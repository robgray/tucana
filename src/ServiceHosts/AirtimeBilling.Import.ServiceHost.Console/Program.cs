﻿using AirtimeBilling.Import;
using AirtimeBilling.Monitor;

namespace AirtimeBilling.ScheduledServices.ServiceHost.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Starting AirtimeBilling System Services Service Host...");

            System.Console.WriteLine("Starting Import Service...");
            Watcher.StartWatching();

            System.Console.WriteLine("Starting Monitor Service");
            JobCoordinator.Start();

            
            System.Console.WriteLine("Press <Enter> to stop the host.");
            System.Console.ReadLine();
            
            Watcher.StopWatching();
            JobCoordinator.Stop();
        }
    }
}
