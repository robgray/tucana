﻿namespace AirtimeBilling.ScheduledServices.ServiceHost.Service
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CallImporterProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.CallImporterServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.ImportServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            this.MonitorServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // CallImporterProcessInstaller
            // 
            this.CallImporterProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.CallImporterProcessInstaller.Password = null;
            this.CallImporterProcessInstaller.Username = null;
            // 
            // CallImporterServiceInstaller
            // 
            this.CallImporterServiceInstaller.Description = "Monitors call data folders and imports when detected.";
            this.CallImporterServiceInstaller.DisplayName = "AirtimeBilling Call Importing Service";
            this.CallImporterServiceInstaller.ServiceName = "AirtimeBillingImportingService";
            this.CallImporterServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ImportServiceInstaller
            // 
            this.ImportServiceInstaller.Description = "Monitors the Call Data Import folder for call data and imports into AirtimeBilling";
            this.ImportServiceInstaller.DisplayName = "Airtime Billing Call Data Import service";
            this.ImportServiceInstaller.ServiceName = "ImportingService";
            this.ImportServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // MonitorServiceInstaller
            // 
            this.MonitorServiceInstaller.Description = "Performs Scheduled Maintenance Jobs on the AirtimeBilling system.  Turning this serv" +
                "ice off will result in database inconsistencies";
            this.MonitorServiceInstaller.DisplayName = "Airtime Billing Monitor Service";
            this.MonitorServiceInstaller.ServiceName = "MonitorService";
            this.MonitorServiceInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.CallImporterProcessInstaller,
            this.CallImporterServiceInstaller,
            this.ImportServiceInstaller,
            this.MonitorServiceInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller CallImporterProcessInstaller;
        private System.ServiceProcess.ServiceInstaller CallImporterServiceInstaller;
        private System.ServiceProcess.ServiceInstaller ImportServiceInstaller;
        private System.ServiceProcess.ServiceInstaller MonitorServiceInstaller;
    }
}