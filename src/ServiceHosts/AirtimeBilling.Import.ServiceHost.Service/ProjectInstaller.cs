﻿using System.ComponentModel;
using System.Configuration.Install;

namespace AirtimeBilling.ScheduledServices.ServiceHost.Service
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
    }
}
