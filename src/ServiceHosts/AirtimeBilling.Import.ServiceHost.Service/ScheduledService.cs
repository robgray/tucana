﻿using System.ServiceProcess;
using AirtimeBilling.Import;
using AirtimeBilling.Monitor;

namespace AirtimeBilling.ScheduledServices.ServiceHost.Service
{
    public partial class ScheduledService : ServiceBase
    {
        public ScheduledService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Watcher.StartWatching();
            JobCoordinator.Start();
        }

        protected override void OnStop()
        {
            JobCoordinator.Stop();
            Watcher.StartWatching();
        }
    }
}
