﻿using System.ServiceProcess;

namespace AirtimeBilling.ScheduledServices.ServiceHost.Service
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new ScheduledService() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
