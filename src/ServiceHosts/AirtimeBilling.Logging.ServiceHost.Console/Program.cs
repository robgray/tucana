﻿using AirtimeBilling.LoggingServices;

namespace AirtimeBilling.Logging.ServiceHost.Console
{
    class Program
    {
        static void Main(string[] args)
        {            
            System.Console.WriteLine("Starting Logging Service Host...");

            var emailHost = new System.ServiceModel.ServiceHost(typeof (EmailService));
            var activityHost = new System.ServiceModel.ServiceHost(typeof(ActivityLoggingService));
            var debugHost = new System.ServiceModel.ServiceHost(typeof(DebugService));
            var errorHost = new System.ServiceModel.ServiceHost(typeof(ErrorService));

            emailHost.Open();
            activityHost.Open();
            debugHost.Open();
            errorHost.Open();
         
            System.Console.WriteLine("Logging and Email service host has started.");
            System.Console.WriteLine("Press <Enter> to stop the host.");
            System.Console.ReadLine();

            emailHost.Close();
            activityHost.Close();
            debugHost.Close();
            errorHost.Close();
            
        }
    }
}
