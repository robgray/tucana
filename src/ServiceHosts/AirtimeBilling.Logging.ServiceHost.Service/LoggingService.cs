﻿using System.ComponentModel;
using System.ServiceProcess;
using AirtimeBilling.LoggingServices;

namespace AirtimeBilling.Logging.ServiceHost.Service
{
    public partial class LoggingService : ServiceBase
    {
        private System.ServiceModel.ServiceHost emailHost;
        private System.ServiceModel.ServiceHost activityHost;
        private System.ServiceModel.ServiceHost debugHost;
        private System.ServiceModel.ServiceHost errorHost;

        public LoggingService()
        {
            InitializeComponent();            
        }

        public LoggingService(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {           
            // Even though the service isn't running yet, the message will sent via MSMQ, 
            // so when the logging service starts it will get this message and log it.
            LoggingUtility.LogDebug("OnStart", "AirtimeBilling.LoggingService.ServiceHost.Service.LoggingService",
                                    "Starting Services");

            if (emailHost != null) emailHost.Close();
            if (activityHost != null) activityHost.Close();
            if (debugHost != null) debugHost.Close();
            if (errorHost != null) errorHost.Close();

            emailHost = new System.ServiceModel.ServiceHost(typeof(EmailService));
            activityHost = new System.ServiceModel.ServiceHost(typeof(ActivityLoggingService));
            debugHost = new System.ServiceModel.ServiceHost(typeof(DebugService));
            errorHost = new System.ServiceModel.ServiceHost(typeof(ErrorService));

            emailHost.Open();
            activityHost.Open();
            debugHost.Open();
            errorHost.Open();

            LoggingUtility.LogDebug("OnStart", "AirtimeBilling.LoggingService.ServiceHost.Service.LoggingService",
                                    "Started Services");                     
        }

        protected override void OnStop()
        {
            LoggingUtility.LogDebug("OnStop", "AirtimeBilling.LoggingService.ServiceHost.Service.LoggingService",
                                    "Stopping Services");            

            if (emailHost != null)
            {
                emailHost.Close();
                emailHost = null;
            }
            
            if (activityHost != null)
            {
                activityHost.Close();
                activityHost = null;
            }

            if (debugHost != null)
            {
                debugHost.Close();
                debugHost = null;
            }

            if (errorHost != null)
            {
                errorHost.Close();
                errorHost = null;
            }

            // Even though the service is no longer running, thanks to MSMQ, the next time it starts it'll get this message.
            LoggingUtility.LogDebug("OnStop", "AirtimeBilling.LoggingService.ServiceHost.Service.LoggingService",
                                    "Stopped Services");                        
        }
    }
}
