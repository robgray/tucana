﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using AirtimeBilling.Common;

namespace RegenChecksum
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var dc = new AirtimeBillingDataContext(ConfigItems.ConnectionString)) {
                
                foreach(var item in dc.SatComDatas) {
                    var fields = new []
                                 {                                     
                                     item.Date,
                                     item.Time,
                                     item.OriginatorNo,                                  
                                     item.DestinationNo,
                                     item.Volume == null ? "" : (item.Volume.Value == (int)item.Volume.Value ? ((int)item.Volume.Value).ToString(): item.Volume.Value.ToString())                                     
                                 };

                    item.Checksum = CalculateCheckSum(fields);
                    dc.SubmitChanges();
                }

                foreach (var item in dc.VizadaDatas) {
                    var fields = new []
                                 {
                                     item.MobileNumber,
                                     item.CallDate,
                                     item.CallTime,
                                     item.CalledNumber,
                                     item.BilledVolume == null ? "" : (item.BilledVolume.Value == (int)item.BilledVolume.Value ? ((int)item.BilledVolume.Value).ToString() : item.BilledVolume.Value.ToString())                           
                                    };

                    item.Checksum = CalculateCheckSum(fields);
                    dc.SubmitChanges();
                }
            }
        }
                       
        public static byte[] CalculateCheckSum(string[] fields)
        {
            var allFields = new StringBuilder();

            foreach (var field in fields)
                allFields.Append(field);

            var md5Hasher = new MD5CryptoServiceProvider();
            var encoder = new UTF8Encoding();
            var hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(allFields.ToString()));

            return hashedBytes;
        }
    }
}
