﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace SharpStudios.AirtimeBilling.Installer.ImageKeys
{
    class Program
    {
        private static string[] filters;

        static void Main(string[] args)
        {
            // scans through the images folder and outputs xml for input into installer.
            // start scan at
            var rootFolderPath = @"f:\source\landwidecms\trunk\src\SharpStudios.AirtimeBilling.Web";
            if (args.Length > 1) rootFolderPath = args[1];

            var extensionFilter = "*.config;*.dll;*.aspx;*.hmtl;*.gif;*.ico;*.jpg;*.jpeg;*.ascx;*.rpt;*.js;*.css;*.Master;*.htm;*.txt;*.png;*.bmp;*.ttf;*.PFB;*.pfm;*.exe;*.xml;*.ashx";            
            if (args.Length > 2) extensionFilter = "*." + args[2];

            filters = extensionFilter.Split(new [] {";"}, StringSplitOptions.RemoveEmptyEntries);

            rootFolderPath += "\\";

            using(var sw = new StreamWriter("files.xml"))
            {
                sw.AutoFlush = true;
                using (var comp = new StreamWriter("components.xml")) {
                    comp.AutoFlush = true; 
                   
                    var di = new DirectoryInfo(rootFolderPath);
                    GetFiles(sw, comp, di);

                    comp.Close();
                }
                sw.Close();
            }

            Console.WriteLine("All Done...");
            Console.ReadLine();
        }

        static void GetFiles(StreamWriter fileWriter, StreamWriter componentWriter, DirectoryInfo directoryInfo)
        {
            Console.WriteLine("Searching " + directoryInfo.Name + "...");

            long tick = DateTime.Now.Ticks;
            fileWriter.WriteLine(string.Format("<Component Id='{0}_FILES{2}' Guid='{1}'>", directoryInfo.Name.ToUpper(), Guid.NewGuid(), tick));
            foreach (var filter in filters)
            {                
                foreach (var file in directoryInfo.GetFiles(filter))
                {                    
                    var path = file.FullName.Replace(@"f:\source\landwidecms\trunk\src\", "..\\");
                    fileWriter.WriteLine(string.Format("<File Id='WEB_{0}_FILES_{1}{4}' Name='{2}' Source='{3}' />",
                                               directoryInfo.Name.ToUpper(), file.Name.Replace("-", "_"), file.Name, path, tick));
                }
            }
            fileWriter.WriteLine(string.Format("</Component>"));

            componentWriter.WriteLine(string.Format("<ComponentRef Id=\"{0}_Files{1}\" />", directoryInfo.Name.ToUpper(), tick));

            foreach(var dir in directoryInfo.GetDirectories())
            {
                if (!dir.IsHidden()) {
                    fileWriter.WriteLine(string.Format("<Directory Id=\"WEB_{0}{2}\" Name=\"{1}\">", dir.Name.ToUpper(), dir.Name, DateTime.Now.Ticks));
                    GetFiles(fileWriter, componentWriter, dir);
                    fileWriter.WriteLine("</Directory>");
                }
            }
        }
    }
}
