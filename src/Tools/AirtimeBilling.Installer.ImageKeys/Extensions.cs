﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace SharpStudios.AirtimeBilling.Installer.ImageKeys
{
    public static class Extensions
    {
        public static bool IsHidden(this DirectoryInfo directory)
        {
            return (directory.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden;
        }
    }
}
