﻿Imports System.Diagnostics

Public Class Log

    Private Shared importEventLog As EventLog

    Shared Sub New()
        ' Event source will be created by the installation programme.
        importEventLog = New EventLog("LandwideCMS")
        importEventLog.Source = "LandwideCMSImportService"
    End Sub

    Public Shared Sub WriteLog(ByVal message As String)
        importEventLog.WriteEntry(message)
        Console.WriteLine(message)
    End Sub

    Public Shared Sub WriteError(ByVal ex As Exception)
        Dim msg As String = "Module: " & ex.Source & vbCrLf & _
                            "Details: " & ex.StackTrace & vbCrLf & vbCrLf & _
                            "Message: " & ex.Message

        WriteError(msg)
    End Sub

    Public Shared Sub WriteError(ByVal message As String)
        importEventLog.WriteEntry(message, EventLogEntryType.Error)
        Console.WriteLine(message)

        If ConfigItems.SendErrorEmails Then
            Email.SendMail(ConfigItems.ErrorEmailAddress, "Importing Error", message)
        End If
    End Sub

    Public Shared Sub WriteWarning(ByVal ex As Exception)
        Dim msg As String = "Module: " & ex.Source & vbCrLf & _
                            "Details: " & ex.StackTrace & vbCrLf & vbCrLf & _
                            "Message: " & ex.Message

        WriteWarning(msg)
    End Sub

    Public Shared Sub WriteWarning(ByVal message As String)
        importEventLog.WriteEntry(message, EventLogEntryType.Warning)
        Console.WriteLine(message)
    End Sub


End Class
