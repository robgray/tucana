﻿Public Interface ICallDataImportManager
    Inherits IImportManager

    ''' <summary>
    ''' This function will attempt to match all the unimported calls to contracts
    ''' in the system.  Useful for matching when adding a contract.
    ''' </summary>
    Sub FindCallDataForContract(ByVal contractId As Integer)

    ''' <summary>
    ''' Invokes a manual rescan of call data looking for contracts.
    ''' </summary>
    ''' <remarks></remarks>
    Sub RescanForContracts()

End Interface
