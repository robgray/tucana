﻿Imports System.IO
Imports AirtimeBilling.Common
Imports System.Reflection
Imports AirtimeBilling.Logging


''' <summary>
''' This class is responsible for managing the importers and ensuring the correct
''' importer gets called for the supplied filename.
''' </summary>
''' <remarks></remarks>
Public Class ImporterManager
    Implements ICallDataImportManager

    Private registeredImporters As IDictionary(Of String, IDataImporter)
    Private _airtimeProviderRepository As IAirtimeProviderRepository

    Public Sub New()
        Me.New(New AirtimeRepository)        
    End Sub

    Public Sub New(ByVal airtimeProviderRepository As IAirtimeProviderRepository)

        Try
            _airtimeProviderRepository = airtimeProviderRepository
            registeredImporters = New Dictionary(Of String, IDataImporter)

            ' Read from Database 
            Using uow = New UnitOfWorkScope()
                Dim providers = _airtimeProviderRepository.FindAll()
                For Each provider In providers
                    Dim importer = DirectCast(Activator.CreateInstance(Type.GetType(provider.ImporterFullName)), IDataImporter)
                    If Not importer Is Nothing Then
                        Register(importer)
                    End If
                Next
            End Using
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

    End Sub

    Private Sub Register(ByVal importer As ICallDataImporter)

        If importer Is Nothing Then
            Return
        End If

        ' Get all the extensions 
        ' Need to compare extensions case insensitive.
        ' This means we need to register with upper case 
        For Each ext As String In importer.RegisteredExtensions()
            If Not registeredImporters.ContainsKey(ext.ToUpper()) Then
                registeredImporters.Add(ext.ToUpper(), importer)
            End If
        Next

    End Sub

    Public Function ProcessDataFile(ByVal fileName As String) As Boolean Implements IImportManager.ProcessDataFile
        Dim fileData = New FileInfo(fileName)

        ' need to be case insensitive.
        Dim ext = fileData.Extension.Substring(1).ToUpper()
        If registeredImporters.ContainsKey(ext) Then
            Dim importer = registeredImporters(ext)            
            LoggingUtility.LogDebug("ProcessCallDataFile", "ImporterManager", "Found " + importer.GetName() + " Importer.  Initiating Import Process ")
            Return importer.Import(fileName)
        Else
            LoggingUtility.LogDebug("ProcessCallDataFile", "ImporterManager", "Could not find an Importer for this extension (" + ext + ").  Cannot import")
        End If

        Return False
    End Function

    ''' <summary>
    ''' This function takes a contractId and attempts to find all calls that could be associated with it.
    ''' </summary>    
    Public Sub FindCallDataForContract(ByVal contractId As Integer) Implements ICallDataImportManager.FindCallDataForContract
        ' Go through each and try.

        For Each provider In registeredImporters.Values
            Dim myType = provider.GetType()
            If myType.GetInterface(GetType(ICallDataImporter).FullName) IsNot Nothing Then
                Dim callDataImporter = DirectCast(provider, ICallDataImporter)
                callDataImporter.FindCallsForContract(contractId)
            End If
        Next
    End Sub

    Public Sub RescanForContracts() Implements ICallDataImportManager.RescanForContracts

        For Each provider In registeredImporters.Values
            Dim myType = provider.GetType()
            If myType.GetInterface(GetType(ICallDataImporter).FullName) IsNot Nothing Then
                Dim callDataImporter = DirectCast(provider, ICallDataImporter)
                Dim ret = callDataImporter.UpdateCallTable()

                Dim msg As New ImportCompletedEmailMessage() With {.CallDataFileName = "* Manual Rescan *", .DataProvider = callDataImporter.GetName()}
                If ret.IsSuccessful Then
                    msg.UnmatchedPhonenumbers = ret.UnmatchedNumbers
                    msg.ImportWarnings = ret.Warnings

                    ' (001-16) Send confirmation email when call data is imported into the system
                    ' (001-102) Make a list of all phone numbers with calls that could not be matched to a contract
                    ' and include in email
                    LoggingUtility.SendEmail(ConfigItems.SystemEmailAddress, "I", "Call Data Manual Scan", msg, True)
                End If
            End If
        Next

    End Sub


End Class
