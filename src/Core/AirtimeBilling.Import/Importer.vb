﻿Imports System.Data.SqlClient
Imports System.IO
Imports Microsoft.VisualBasic.FileIO
Imports System.Transactions
Imports System.Text
Imports RGSS.LandwideCMS.Logging

Public Class Importer

    Private Enum VizadaFields
        Acnt
        InvNo
        ChgTyp
        CrDeb
        MsgRef
        SerNo
        MobNo
        PlatfId
        PlatfNam
        [Date]
        Time
        OceReg
        Sbac
        Orig
        CalledNo
        NumTyp
        ShCod
        Cntry
        SvcCod
        SvcNam
        SvcMain
        SvcSub
        RatPer
        BillV
        ConnV
        SatV
        VolUn
        SatRat
        DestRat
        SatChg
        DestChg
        FacChg
        Disc
        DiscTyp
        TotChg
        Cur
        ISP
    End Enum

    Private mImportFilename As String

    Public Sub New(ByVal importFilename As String)
        mImportFilename = importFilename
    End Sub

    Private Class ContractRec
        Public ContractID As Long
        Public PlanID As Long
        Public ContractNumber As String
        Public TariffID As Long
        Public CallCount As Long
        Public UnitOfTime As Integer
    End Class

    Public Property ImportFilename() As String
        Get
            ImportFilename = mImportFilename
        End Get
        Set(ByVal value As String)
            mImportFilename = value
        End Set
    End Property

    ''' <summary>
    ''' This method imports data...    
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub DoImport()
        If ImportCallFile() Then
            UpdateCallTable()
        End If
    End Sub

    ''' <summary>
    ''' This method imports the call data file into a related table in the system.
    ''' </summary>
    Private Function ImportCallFile() As Boolean
        Try
            Dim repository As IVizadaDataRepository = New VizadataDataRepository

            Dim FolderName As String = Path.GetDirectoryName(mImportFilename)
            Dim Filename As String = Path.GetFileName(mImportFilename)

            Const DELIMITER As String = ","

            Using ts As TransactionScope = New TransactionScope()
                Using parser As New TextFieldParser(mImportFilename)
                    parser.SetDelimiters(DELIMITER)
                    If parser.EndOfData = False Then parser.ReadFields() ' Consume first row (headings)
                    While Not parser.EndOfData
                        Dim fields As String() = parser.ReadFields
                        Dim checksum As Byte() = CalculateCheckSum(fields)
                        Dim entity As New VizadaData
                        With entity
                            .AccountNumber = fields(VizadaFields.Acnt)
                            .InvoiceNumber = fields(VizadaFields.InvNo)
                            .ChargeType = fields(VizadaFields.ChgTyp)
                            .CreditDebit = fields(VizadaFields.CrDeb)
                            .MessageReference = fields(VizadaFields.MsgRef)
                            .SerialNumber = fields(VizadaFields.SerNo)
                            .MobileNumber = fields(VizadaFields.MobNo)
                            .PlatformId = fields(VizadaFields.PlatfId)
                            .PlatformName = fields(VizadaFields.PlatfNam)
                            .CallDate = fields(VizadaFields.Date)
                            .CallTime = fields(VizadaFields.Time)
                            .OceanRegion = fields(VizadaFields.OceReg)
                            .SplitBilingCode = fields(VizadaFields.Sbac)
                            .Origin = fields(VizadaFields.Orig)
                            .CalledNumber = fields(VizadaFields.CalledNo)
                            .NumberType = fields(VizadaFields.NumTyp)
                            .ShortCode = fields(VizadaFields.ShCod)
                            .Country = fields(VizadaFields.Cntry)
                            .ServiceCode = fields(VizadaFields.SvcCod)
                            .ServiceName = fields(VizadaFields.SvcNam)
                            .ServiceMain = fields(VizadaFields.SvcMain)
                            .ServiceSub = fields(VizadaFields.SvcSub)
                            .RatingPeriod = fields(VizadaFields.RatPer)
                            .BilledVolume = fields(VizadaFields.BillV)
                            .ConnectVolume = fields(VizadaFields.ConnV)
                            .SatelliteVolume = fields(VizadaFields.SatV)
                            .VolumeUnit = fields(VizadaFields.VolUn)
                            .SatelliteRate = fields(VizadaFields.SatRat)
                            .DestinationRate = fields(VizadaFields.DestRat)
                            .SatelliteCharge = fields(VizadaFields.SatChg)
                            .DestinationSurCharge = fields(VizadaFields.DestChg)
                            .FacilityCharge = fields(VizadaFields.FacChg)
                            .Discount = fields(VizadaFields.Disc)
                            .DiscountType = fields(VizadaFields.DiscTyp)
                            .TotalCharge = fields(VizadaFields.TotChg)
                            .Currency = fields(VizadaFields.Cur)
                            .ISP = fields(VizadaFields.ISP)
                            .Imported = False
                            .Checksum = checksum
                        End With

                        Try
                            repository.Insert(entity)
                        Catch sqlex As SqlException
                            If sqlex.Message.StartsWith("Violation of UNIQUE KEY constraint 'IX_VizadaData_Checksum'. Cannot insert duplicate key in object 'dbo.VizadaData'.") Then
                                ' Do not allow duplicate phone records to be imported
                                LoggingUtility.LogWarning("ImportCallFile", "Importer", "Duplicate phone record found and ignored. " + vbCrLf + "Data: " + String.Join(",", fields))
                            Else
                                Throw sqlex
                            End If
                        End Try
                    End While
                    repository.Save()
                    parser.Close()
                    ts.Complete()
                End Using
            End Using

            Dim successfulImportMessage = "Calls successfully imported for '" & mImportFilename & "'"
            LoggingUtility.LogDebug("ImportCallFile", "Importer", successfulImportMessage)

            ' (001-16) Send confirmation email when call data is imported into the system
            successfulImportMessage += " At " + DateTime.Now
            LoggingUtility.SendEmail(ConfigItems.SystemEmailAddress, "Landwide", "Call Data Import", successfulImportMessage, True)            
            Return True
        Catch ex As Exception
            ' Log error ???
            LoggingUtility.LogException(ex)
        End Try
        Return False

    End Function

    ''' <summary>
    ''' This method selects all unimported calls and attempts to associate the call
    ''' with a contract.  Entries in the Call table are created and the import data 
    ''' is marked as Imported, if a contract can be associated.  Records not associated
    ''' with a contract will attempt to be associated next import.
    ''' </summary>        
    Private Function UpdateCallTable() As Boolean

        Try
            Dim contracts As IDictionary(Of Integer, ContractRec) = New Dictionary(Of Integer, ContractRec)
            
            Dim vdRepository As IVizadaDataRepository = New VizadataDataRepository
            Dim callRep As ICallRepository = New CallRepository

            Using ts As TransactionScope = New TransactionScope()

                Dim vizadaData = vdRepository.FindAllNotImported()

                For Each vd In vizadaData
                    Dim dtCallTime As Date = ToDateTime(vd.CallDate, vd.CallTime)

                    ' Find matching contract and tariff 
                    Dim crec As ContractRec = GetContractRec(vd.MobileNumber, dtCallTime, vd.ServiceName)
                    Dim c As Contract

                    Dim myCall As New [Call]
                    With myCall
                        .CallTime = dtCallTime
                        .ContractId = crec.ContractID
                        .TariffId = crec.TariffID
                        .ServiceNumber = vd.MobileNumber
                        .CalledNumber = vd.CalledNumber
                        .Location = vd.PlatformName
                        .ImportedCallType = vd.ServiceName
                        Dim callVolume As Double = Double.Parse(vd.BilledVolume)
                        .Volume = callVolume
                        .VolumeUnit = vd.VolumeUnit
                        If vd.VolumeUnit = "MIN" Then
                            ' (Changed 21/10/2009 rag) [001-96] UnitOfTime per plan.
                            .UnitsOfTime = New UnitsOfTimeCalculator().Calculate(crec.UnitOfTime, callVolume)
                        Else
                            .UnitsOfTime = IIf(callVolume > 0, 1, 0)
                        End If
                    End With
                    callRep.Insert(myCall)

                    If contracts.ContainsKey(crec.ContractID) = False Then
                        crec.CallCount = 1
                        contracts.Add(crec.ContractID, crec)
                    Else
                        contracts(crec.ContractID).CallCount += 1
                    End If

                    vd.Imported = True
                Next

                vdRepository.Save()
                callRep.Save()

                ts.Complete()
            End Using

            UpdateActivityLog(contracts)

            ' TODO: Delete or rename import file?            
            Return True

        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try
        Return False

    End Function

    Protected Function ToDateTime(ByVal sDate As String, ByVal sTime As String) As Date

        Return Date.Parse(sDate & " " & sTime)

    End Function

    ''' <summary>
    ''' Retrieves a Contract Entity given a call, given the phone number that made the call, 
    ''' the time of the call, and the tariff.
    ''' </summary>   
    Private Function GetContractRec(ByVal strPhone As String, ByVal dtCallTime As Date, ByVal strServiceName As String) As ContractRec
        Try
            Dim contractRepository As IContractRepository = New ContractRepository()
            Dim tarRepository As ITariffRepository = New TariffRepository()

            Dim contracts = contractRepository.FindAllContracts()

            ' Lookup contract
            Dim crec As New ContractRec
            With crec
                .ContractID = 0
                .ContractNumber = ""
                .PlanID = 0
                .TariffID = 0
            End With

            Dim contract = (From c In contracts _
                           Where (c.PhoneNumber1.Equals(strPhone) Or _
                           c.PhoneNumber2.Equals(strPhone) Or _
                           c.PhoneNumber3.Equals(strPhone) Or _
                           c.PhoneNumber4.Equals(strPhone)) And _
                           (dtCallTime >= c.ActivationDate Or dtCallTime <= c.EndDate) _
                           Select c.ContractId, c.PlanId, c.ContractNumber, c.Plan.UnitOfTime).FirstOrDefault()

            If Not contract Is Nothing Then
                crec.ContractID = contract.ContractId
                crec.PlanID = contract.PlanId
                crec.ContractNumber = contract.ContractNumber
                crec.UnitOfTime = contract.UnitOfTime
                crec.TariffID = 0

                Dim tariff = (From t In tarRepository.FindAllTariffs _
                             Where t.PlanId = contract.PlanId And t.NetworkTariff.Tariff.Equals(strServiceName) _
                             Select t).FirstOrDefault()

                If Not tariff Is Nothing Then
                    crec.TariffID = tariff.TariffId
                Else
                    ' No Tariff found - What do we do here ?
                    LoggingUtility.LogDebug("GetContractRec", "Importer", _
                                    String.Format("Could not Tariff on PlanId={0} with name={2}", crec.PlanID, strServiceName))
                End If
            Else
                LoggingUtility.LogDebug("GetContractRec", "Importer", _
                                        String.Format("Could not find a contract for Phone={0} active on {2}", strPhone, dtCallTime))
            End If
            Return crec

        Catch ex As Exception
            LoggingUtility.LogException(ex)
            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' This route updates the activity log for the contract's recorded 
    ''' col contains a collection of contract records, with the CallCount giving
    ''' the number of calls imported for that contract in this import run.
    ''' </summary>
    ''' <param name="contracts"></param>
    ''' <remarks></remarks>
    Private Sub UpdateActivityLog(ByVal contracts As IDictionary(Of Integer, ContractRec))
        
        For Each crec In contracts.Values            
            Dim activity As String = String.Format("Imported {0} calls.", crec.CallCount)            
            LoggingUtility.WriteActivity(RGSS.LandwideCMS.Common.Constants.TableNames.CONTRACT, crec.ContractID, activity)
        Next crec

    End Sub

    ''' <summary>
    ''' Calculates a checksum that is used to identify a unique record.
    ''' All fields in are concatenated and then hashed, to give the checksum.
    ''' </summary>
    ''' <param name="fields">String array of fields to use as the basis for the checksum</param>
    ''' <returns>A hash representing the record</returns>    
    ''' <remarks>
    ''' Rather than use GetHashCode (or extend it), I thought I'd roll my own, 
    ''' mainly because it's easier to work with the fields array to generate 
    ''' the hash I want, then apply MD5 to that.
    ''' </remarks>
    Private Function CalculateCheckSum(ByVal fields() As String) As Byte()
        Dim allFields As StringBuilder = New StringBuilder

        For Each field As String In fields
            allFields.Append(field)
        Next field

        Dim md5Hasher As New MD5CryptoServiceProvider()
        Dim hashedBytes As Byte()
        Dim encoder As New UTF8Encoding()

        hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(allFields.ToString()))

        Return hashedBytes

    End Function

End Class
