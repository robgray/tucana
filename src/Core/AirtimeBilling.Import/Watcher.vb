﻿Imports System.IO
Imports AirtimeBilling.Common
Imports AirtimeBilling.Logging

Public Class Watcher

    Private Shared watcher As FileSystemWatcher
    Private Shared _importManager As ImporterManager

    Shared Sub New()

        Try
            CheckInitiatedWatcher()

        Catch ex As Exception
            LoggingUtility.LogException(ex)

        End Try

    End Sub

    Private Shared Sub CheckInitiatedWatcher()
        If watcher Is Nothing Then
            ' Check that folder to watch exists
            If Not Directory.Exists(ConfigItems.ImportFolder) Then
                LoggingUtility.LogDebug("New", "Watcher", "Could not find import folder '" + ConfigItems.ImportFolder + "'")
                LoggingUtility.SendEmail(ConfigItems.SystemEmailAddress, "Import Watcher Failure", "Invalid import folder '" + ConfigItems.ImportFolder + "'.", True)
                Return
            End If

            ' Create a new FileSystemWatcher and set its properties[
            watcher = New FileSystemWatcher(ConfigItems.ImportFolder)

            ' Watch for changes in LastAccess and LastWrite times, and
            ' the renaming of files or directories. 
            watcher.NotifyFilter = (NotifyFilters.LastAccess Or NotifyFilters.LastWrite Or NotifyFilters.FileName Or NotifyFilters.DirectoryName)

            ' Watch all files and let the import manager do the work of deciding to do anything.
            watcher.Filter = ""

            _importManager = New ImporterManager()
            '
            ' Add event handlers.
            AddHandler watcher.Changed, AddressOf OnChanged
            AddHandler watcher.Created, AddressOf OnChanged
            AddHandler watcher.Deleted, AddressOf OnChanged
        End If
    End Sub


    Public Shared Sub StartWatching()

        Try
            CheckInitiatedWatcher()
            watcher.EnableRaisingEvents = True
            LoggingUtility.LogDebug("StartWatching", "Watcher", "Watching for new import files in '" + ConfigItems.ImportFolder + "'.")
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

    End Sub

    ' Define the event handlers.
    Private Shared Sub OnChanged(ByVal source As Object, ByVal e As FileSystemEventArgs)

        Try
            ' Specify what is done when a file is changed, created, or deleted.
            If e.ChangeType = WatcherChangeTypes.Created Then
                ' Little bug here when deleting a file and then pasting it back in.
                ' Found in testing and probably only a testing thing, but need to be sure...
                Threading.Thread.Sleep(50)
                LoggingUtility.LogDebug("OnChanged", "Watcher", "Found new call data file '" + e.FullPath + "'")

                _importManager.ProcessDataFile(e.FullPath)

            End If
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

    End Sub

    Public Shared Sub StopWatching()
        Try
            If Not watcher Is Nothing Then
                watcher.EnableRaisingEvents = False
                LoggingUtility.LogDebug("StopWatching", "Watcher", "Monitoring the import folder for new files has been stopped.")
            End If
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try
    End Sub

    Protected Shared Sub WaitForFile(ByVal fileName As String)
        Dim waiter As New FileWaiter(fileName)
        waiter.Wait()
    End Sub

End Class


