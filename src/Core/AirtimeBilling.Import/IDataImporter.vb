﻿Public Interface IDataImporter

    Function RegisteredExtensions() As IList(Of String)
    ReadOnly Property RegisteredFolder() As String
    Function Import(ByVal fileName As String) As Boolean
    Function Exists(ByVal checksum As Byte()) As Boolean
    Function GetName() As String

End Interface
