﻿Imports System.Text

Public Class Warnings
    Private _warnings As IList(Of String)
    Private _info As IList(Of String)

    Public Sub New()
        _warnings = New List(Of String)
        _info = New List(Of String)
    End Sub


    Public Function AddWarning(ByVal message As String) As Boolean
        If Not _warnings.Contains(message) Then
            _warnings.Add(message)
            Return True
        End If
        Return False
    End Function


    Public Function AddInfo(ByVal message As String) As Boolean
        If Not _info.Contains(message) Then
            _info.Add(message)
            Return True
        End If
        Return False
    End Function

    Public ReadOnly Property HasWarnings() As Boolean
        Get
            Return _warnings.Count > 0 Or _info.Count > 0
        End Get
    End Property

    Public Function GetBody() As String
        Dim sb As New StringBuilder()
       
        If _warnings.Count > 0 Then
            sb.Append("<div id='warnings'>")
            sb.Append("The following issues prevented some calls from being imported.")
            sb.Append("<ol>")
            For Each warning In _warnings
                sb.Append(String.Format("<li>{0}</li>", warning))
            Next
            sb.Append("</ol>")
            sb.Append("</div>")
        End If

        If _info.Count > 0 Then
            sb.Append("<div id='warnings'>")
            sb.Append("The following issues did not prevent calls from being imported but should be investigated.")
            sb.Append("<ol>")
            For Each warning In _info
                sb.Append(String.Format("<li>{0}</li>", warning))
            Next
            sb.Append("</ol>")
            sb.Append("</div>")
        End If

        Return sb.ToString()

    End Function

    Public Function GetNoHtml() As String
        Dim sb As New StringBuilder()

        If _warnings.Count > 0 Then
            sb.AppendLine("The following issues prevented some calls from being imported.")
            sb.AppendLine()
            For Each warning In _warnings
                sb.AppendLine(warning)
            Next
        End If

        If _info.Count > 0 Then
            sb.AppendLine("The following issues did not prevent calls from being imported but should be investigated.")
            sb.AppendLine()
            For Each warning In _info
                sb.AppendLine(warning)
            Next
        End If

        Return sb.ToString()
    End Function


End Class
