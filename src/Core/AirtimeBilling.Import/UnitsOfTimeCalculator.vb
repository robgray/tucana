﻿Imports AirtimeBilling.Core.Enums

Public Class UnitsOfTimeCalculator

    Private _unit As CallVolumeUnit

    Public Sub New(ByVal volumeUnit As CallVolumeUnit)
        _unit = volumeUnit
    End Sub
    ''' <summary>
    ''' Determines how many units of time are in the duration of a call.
    ''' Requires the number of seconds per unit 
    ''' </summary>    
    Public Function Calculate(ByVal secondsPerUnit As Integer, ByVal callTimeInDecimalMinutes As Double) As Decimal
        Dim blocksPerMinute As Double

        If callTimeInDecimalMinutes = 0 Then Return 0

        If _unit = CallVolumeUnit.Time Then
            If (callTimeInDecimalMinutes * 60) < secondsPerUnit Then Return 1

            blocksPerMinute = 60 / secondsPerUnit
            Return Math.Ceiling(callTimeInDecimalMinutes * blocksPerMinute)

        ElseIf _unit = CallVolumeUnit.Data Then
            Return Convert.ToDecimal(callTimeInDecimalMinutes)

        Else
            ' Includes Discrete 
            Return Convert.ToDecimal(IIf(callTimeInDecimalMinutes > 0, 1, 0))

        End If


    End Function
End Class
