﻿Public Class UpdateCallTableReturn
    Public Sub New()
        UnmatchedNumbers = New List(Of String)
        Warnings = New Warnings
        IsSuccessful = False
    End Sub

    Public UnmatchedNumbers As IList(Of String)
    Public IsSuccessful As Boolean
    Public Warnings As Warnings
End Class
