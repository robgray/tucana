﻿
Imports System.Text
Imports AirtimeBilling.Common.Logging

    ''' <summary>
    ''' Encapsulates a message containing call matching warnings
    ''' </summary>
    ''' <remarks></remarks>
    Public Class CallMatchWarningsEmailMessage
        Inherits EmailFormat

        Private _warnings As Warnings
        Private _contractNumber As String

        Public Sub New()
            _warnings = New Warnings()

        End Sub

        Public Property ImportWarnings() As Warnings
            Get
                Return _warnings
            End Get
            Set(ByVal value As Warnings)
                _warnings = value
            End Set
        End Property

        Public Property ContractNumber() As String
            Get
                Return _contractNumber
            End Get
            Set(ByVal value As String)
                _contractNumber = value
            End Set
        End Property

        Protected Overrides Function GetBody() As String
            Dim sb As New StringBuilder()
            sb.Append("<div>")
            sb.Append("Airtime Billing Call matching process for Contract Number " + ContractNumber + " has been run.<br/>")
            sb.Append("</div>")
            sb.Append(_warnings.GetBody())

            Return sb.ToString()
        End Function

        Protected Overrides Function GetNoHtml() As String
            Dim sb As New StringBuilder()
            sb.AppendLine("Airtime Billing Call matching process for Contract Number " + ContractNumber + " has been run.")
            sb.AppendLine()
            sb.AppendLine(_warnings.GetNoHtml())

            Return sb.ToString()
        End Function

        Protected Overrides Function GetCustomStyles() As String
            Dim sb As New StringBuilder()
            sb.Append("li { font-style: italic; margin: 20px; }")
            Return sb.ToString()
        End Function

    End Class
