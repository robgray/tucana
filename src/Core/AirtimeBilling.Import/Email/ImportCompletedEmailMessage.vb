﻿
Imports System.Text
Imports AirtimeBilling.Common.Logging


''' <summary>
''' Encapsulates an email sent after the importation process for new call data has been completed.
''' </summary>
''' <remarks></remarks>
Public Class ImportCompletedEmailMessage
    Inherits EmailFormat

    Private _calldataFilename As String
    Private _dataProvider As String
    Private _unmatchedPhonenumbers As IList(Of String)
    Private _warnings As Warnings

    Public Sub New()
        _unmatchedPhonenumbers = New List(Of String)
        _warnings = New Warnings
    End Sub

    Public Property CallDataFileName() As String
        Get
            Return _calldataFilename
        End Get
        Set(ByVal value As String)
            _calldataFilename = value
        End Set
    End Property

    Public Property UnmatchedPhonenumbers() As IList(Of String)
        Get
            Return _unmatchedPhonenumbers
        End Get
        Set(ByVal value As IList(Of String))
            _unmatchedPhonenumbers = value
        End Set
    End Property

    Public Property ImportWarnings() As Warnings
        Get
            Return _warnings
        End Get
        Set(ByVal value As Warnings)
            _warnings = value
        End Set
    End Property

    Public Property DataProvider() As String
        Get
            Return _dataProvider
        End Get
        Set(ByVal value As String)
            _dataProvider = value
        End Set
    End Property

    Protected Overrides Function GetBody() As String
        Dim sb As New StringBuilder()
        sb.Append("<div>")
        sb.Append("Airtime Billing Import process for " + DataProvider() + " has been run.  Data within the " + CallDataFileName + " file has been imported.")
        sb.Append("</div>")

        If Not _unmatchedPhonenumbers Is Nothing AndAlso _unmatchedPhonenumbers.Count > 0 Then
            sb.Append("<div id='unmatched'>")
            sb.Append("Calls for the following phone numbers could not be matched against a Contract.")
            sb.Append("Please check the current contracts and modify as appropriate, or add new Contracts for these phone numbers<br/><br/>")
            sb.Append("<div id='numbers'>")
            sb.Append("<u>Phone Numbers</u><br/>")
            For Each number In UnmatchedPhonenumbers
                sb.Append(number + "<br/>")
            Next
            sb.Append("</div>")
            sb.Append("</div>")
        End If

        sb.Append(_warnings.GetBody())

        Return sb.ToString()
    End Function

    Protected Overrides Function GetNoHtml() As String
        Dim sb As New StringBuilder
        sb.AppendLine("Airtime Billing Import process for " + DataProvider + " has been run.  Data within the " + CallDataFileName + " file has been imported.")
        sb.AppendLine()
        If Not _unmatchedPhonenumbers Is Nothing AndAlso _unmatchedPhonenumbers.Count > 0 Then
            sb.AppendLine("Calls for the following phone numbers could not be matched against a Contract.")
            sb.AppendLine("Please check the current contracts and modify as appropriate, or add new Contracts for these phone numbers<br/><br/>")
            sb.AppendLine()
            For Each unmatched In UnmatchedPhonenumbers
                sb.AppendLine(unmatched)
            Next
        End If

        sb.AppendLine(_warnings.GetNoHtml())

        Return sb.ToString()
    End Function

    Protected Overrides Function GetCustomStyles() As String
        Dim sb As New StringBuilder()
        sb.Append("#unmatched { font-weight: bold; }")
        sb.Append("#numbers { margin: 20px; }")
        sb.Append("li { font-style: italic; margin: 20px; }")
        Return sb.ToString()
    End Function
End Class
