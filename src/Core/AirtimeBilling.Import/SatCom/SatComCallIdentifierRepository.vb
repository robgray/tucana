﻿Public Class SatComCallIdentifierRepository
    Inherits Repository
    Implements ISatComCallIdentifierRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal db As AirtimeBillingDataContext)
        MyBase.New(db)
    End Sub

    Public Function Find(ByVal id As String) As SatComCallIdentifier Implements ISatComCallIdentifierRepository.Find

        Return Db.SatComCallIdentifiers.FirstOrDefault(Function(s) s.CallIdentifierId = id)

    End Function


End Class
