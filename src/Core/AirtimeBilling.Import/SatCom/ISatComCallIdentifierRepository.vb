﻿
Public Interface ISatComCallIdentifierRepository

    Function Find(ByVal callIdentifier As String) As SatComCallIdentifier

End Interface