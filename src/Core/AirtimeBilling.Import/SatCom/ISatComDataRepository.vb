﻿Public Interface ISatComDataRepository

    Function FindAll() As IList(Of SatComData)
    Function FindAllUnimported() As IList(Of SatComData)
    Sub Insert(ByVal data As SatComData)

End Interface
