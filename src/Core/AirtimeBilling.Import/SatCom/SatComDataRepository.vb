﻿
    Public Class SatComDataRepository
        Inherits Repository
        Implements ISatComDataRepository

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal db As AirtimeBillingDataContext)
            MyBase.New(db)
        End Sub

        Public Function FindAll() As IList(Of SatComData) Implements ISatComDataRepository.FindAll

            Return Db.SatComDatas.ToList()

        End Function

        Public Function FindAllUnimported() As IList(Of SatComData) Implements ISatComDataRepository.FindAllUnimported

            Return Db.SatComDatas.Where(Function(s) s.Imported = False).ToList()

        End Function

        Public Sub Insert(ByVal data As SatComData) Implements ISatComDataRepository.Insert

            Db.SatComDatas.InsertOnSubmit(data)

        End Sub

    End Class
