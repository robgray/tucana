﻿Imports System.Collections.Generic
Imports AirtimeBilling.Common
Imports AirtimeBilling.Logging
Imports Microsoft.VisualBasic.FileIO
Imports System.Transactions

Public Class SatComCallDataImporter
    Inherits CallDataImporter

    Private Enum SatComFields
        AccountNo
        InvoiceNo
        OceanRegion
        [Date]
        Time
        OriginatorNo
        Subscriber
        DestinationNo
        Volume
        Unit
        Rate
        TotalCharge
        EquipmentType
        CallToCallType
        CallIdentifierId
        OriginatorCountry
        DestinationCountry
        Provider
        UpstreamRate
        DownstreamRate
        DataSessionId
    End Enum

    Private _dataRepository As ISatComDataRepository
    Private _identifierRepository As ISatComCallIdentifierRepository

    Public Sub New()
        Me.New(New CallRepository, New ContractRepository, New TariffRepository, _
               New SatComDataRepository, New SatComCallIdentifierRepository, New AirtimeRepository)
    End Sub


    Public Sub New(ByVal callRep As ICallRepository, ByVal contRep As IContractRepository, _
                   ByVal tarRep As ITariffRepository, ByVal dataRep As ISatComDataRepository, _
                   ByVal callIdRepo As ISatComCallIdentifierRepository, _
                   ByVal airRep As IAirtimeProviderRepository)
        MyBase.New(callRep, contRep, tarRep, airRep)
        _dataRepository = dataRep
        _identifierRepository = callIdRepo
        _airtimeId = 2  ' AirtimeProvider Primary Key Id.
    End Sub

    Public Overrides Function Exists(ByVal checksum() As Byte) As Boolean




        Return _dataRepository.FindAll.Any(Function(x) x.Checksum = checksum)
    End Function

    Protected Overrides Function ImportCallFile(ByVal fileName As String) As Boolean
        Const DELIMITER As String = ","

        Dim currentChecksumFields = New List(Of String)

        Try
            Using ts As New TransactionScope()
                Using uow = New UnitOfWorkScope()

                    Using parser As New TextFieldParser(fileName)
                        parser.SetDelimiters(DELIMITER)
                        If Not parser.EndOfData Then parser.ReadFields() ' Consume first row (headings)
                        While Not parser.EndOfData
                            Dim fields As String() = parser.ReadFields

                            ' 001-164: I would like to use only Date, Time, OriginatorNo, DestinationNo, Volume for unqiueness
                            ' Also check the overload of CalculateCheckSum field when changing the fields included in uniqueness check.
                            Dim unqiueFields() As String = {fields(SatComFields.Date), fields(SatComFields.Time), _
                                                            fields(SatComFields.OriginatorNo), fields(SatComFields.DestinationNo), _
                                                            fields(SatComFields.Volume)}
                            Dim preHashedFieldValues As String = ""
                            Dim checksum As Byte() = CalculateCheckSum(unqiueFields, preHashedFieldValues)
                            If currentChecksumFields.Contains(preHashedFieldValues) Then
                                Continue While
                            End If
                            currentChecksumFields.Add(preHashedFieldValues)

                            If Exists(checksum) Then Continue While

                            Dim entity As New SatComData
                            With entity
                                .AccountNo = fields(SatComFields.AccountNo)
                                .InvoiceNo = fields(SatComFields.InvoiceNo)
                                .OceanRegion = fields(SatComFields.OceanRegion)
                                .Date = fields(SatComFields.Date)
                                .Time = fields(SatComFields.Time)
                                .OriginatorNo = fields(SatComFields.OriginatorNo)
                                .Subscriber = fields(SatComFields.Subscriber)
                                .DestinationNo = fields(SatComFields.DestinationNo)
                                .Volume = fields(SatComFields.Volume).WhenEmpty("0")
                                .Unit = fields(SatComFields.Unit)
                                .Rate = fields(SatComFields.Rate).WhenEmpty("0")
                                .TotalCharge = fields(SatComFields.TotalCharge).WhenEmpty("0")
                                .EquipmentType = fields(SatComFields.EquipmentType)
                                .CallToCallType = fields(SatComFields.CallToCallType)
                                .CallIdentifierID = fields(SatComFields.CallIdentifierId)
                                .OriginatorCountry = fields(SatComFields.OriginatorCountry)
                                .DestinationCountry = fields(SatComFields.DestinationCountry)
                                .Provider = fields(SatComFields.Provider)
                                .UpstreamRate = fields(SatComFields.UpstreamRate).WhenEmpty("0")
                                .DownstreamRate = fields(SatComFields.DownstreamRate).WhenEmpty("0")
                                .DataSessionId = fields(SatComFields.DataSessionId).WhenEmpty("")
                                .Imported = False
                                .Checksum = checksum
                            End With

                            _dataRepository.Insert(entity)

                        End While

                        parser.Close()
                    End Using
                    uow.Commit()
                End Using
                ts.Complete()
            End Using

            Return True
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try
        Return False

    End Function

    Public Overrides Function RegisteredExtensions() As IList(Of String)
        Dim exts As IList(Of String) = New List(Of String)
        exts.Add("csv")

        Return exts

    End Function


    Public Overrides Function UpdateCallTable() As UpdateCallTableReturn
        Try
            Dim contracts As IDictionary(Of Integer, ContractRecordHelper) = New Dictionary(Of Integer, ContractRecordHelper)
            Dim unmatched As IList(Of String) = New List(Of String)
            Dim warnings As Warnings = New Warnings

            Using ts = New TransactionScope()
                Using uow = New UnitOfWorkScope()
                    Dim satcomData = _dataRepository.FindAllUnimported()

                    For Each sd In satcomData
                        ' Check if there is a SatComCallIdentifier for this callIdentifierId                        
                        'If sd.SatComCallIdentifier Is Nothing Then
                        If _identifierRepository.Find(sd.CallIdentifierID) Is Nothing Then
                            Dim warning = String.Format("Could not find callidentifierid='{0}' with CalledToCallType='{1}' in the SatComCallIdentifier table.", sd.CallIdentifierID, sd.CallToCallType)

                            If warnings.AddWarning(warning) Then
                                LoggingUtility.LogWarning("CreateCallsForContract", "SatComCallDataImporter", warning)
                            End If
                            'Continue For
                        End If

                        Dim crec As ContractRecordHelper = GetContractRec(sd.OriginatorNo, sd.CallDateTime, sd.CallIdentifierID)

                        If crec Is Nothing Then
                            ' Could not find contract 
                            If Not unmatched.Contains(sd.OriginatorNo) Then
                                unmatched.Add(sd.OriginatorNo)
                            End If
                            Continue For
                        End If

                        Dim myCall As New [Call]
                        With myCall
                            .CallTime = sd.CallDateTime
                            .ContractId = crec.ContractID
                            .TariffId = crec.TariffID
                            .ServiceNumber = sd.OriginatorNo
                            .CalledNumber = sd.DestinationNo
                            .Location = sd.OriginatorCountry
                            .ImportedCallType = sd.CallToCallType
                            Dim callVolume As Double = Double.Parse(sd.Volume)
                            .Volume = callVolume
                            .VolumeUnit = MyBase.GetCallVolumeUnit(sd.Unit)

                            ' (Changed 21/10/2009 rag) [001-96] UnitOfTime per plan.
                            .UnitsOfTime = New UnitsOfTimeCalculator(.VolumeUnit).Calculate(crec.UnitOfTime, callVolume)
                        End With
                        _callRepository.Insert(myCall)

                        If contracts.ContainsKey(crec.ContractID) = False Then
                            crec.CallCount = 1
                            contracts.Add(crec.ContractID, crec)
                        Else
                            contracts(crec.ContractID).CallCount += 1
                        End If

                        sd.Imported = True
                    Next

                    UpdateActivityLog(contracts)

                    uow.Commit()
                End Using

                ts.Complete()
            End Using
            Return New UpdateCallTableReturn With {.UnmatchedNumbers = unmatched, .IsSuccessful = True, .Warnings = warnings}

        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

        Return New UpdateCallTableReturn
    End Function

    ''' <summary>
    ''' Finds calls in the import tables for the contract and creates Call entries.
    ''' </summary>
    ''' <param name="cont"></param>
    ''' <remarks></remarks>
    Protected Overrides Function CreateCallsForContract(ByVal cont As Contract) As Warnings
        Dim warnings As Warnings = New Warnings

        Try
            Using ts As New TransactionScope()
                Using uow = New UnitOfWorkScope()

                    Dim calls = (From c In _dataRepository.FindAllUnimported() _
                            Where (c.OriginatorNo = If(cont.PhoneNumber1, String.Empty) Or _
                                   c.OriginatorNo = If(cont.PhoneNumber2, String.Empty) Or _
                                   c.OriginatorNo = If(cont.PhoneNumber3, String.Empty) Or _
                                   c.OriginatorNo = If(cont.PhoneNumber4, String.Empty)) _
                            Select c).ToList()

                    LoggingUtility.LogDebug("CreateCallsForContract", "SatComCallDataImporter", _
                                            String.Format("Found {0} calls for importing against Contract #{1}", _
                                                          calls.Count, cont.ContractNumber))

                    ' Found matched calls
                    For Each sd In calls

                        ' I would like calls to be assigned to this contract as long 
                        ' as it is Active/Suspended and not closed, and the call was made after the
                        ' contract activation date.  This is to disregard the contract end date.
                        'If Not (sd.CallDateTime >= cont.ActivationDate And sd.CallDateTime <= cont.EndDate) Then
                        'Continue For
                        'End If
                        If cont.ActivationDate > sd.CallDateTime AndAlso Not (cont.ContractStatus = Core.Enums.ContractStatus.Active Or cont.ContractStatus = Core.Enums.ContractStatus.Suspended) Then
                            Continue For
                        End If

                        Dim callTypeId As String = sd.CallIdentifierID
                        ' Added Trim() because customers sometimes include spaces at the end of their codes when they add them :(
                        Dim tariff = _tariffRepository.FindAllTariffs().FirstOrDefault(Function(t) t.NetworkTariff.Code.Trim() = callTypeId.Trim())
                        If tariff Is Nothing Then
                            Dim warning = String.Format("Could not find tariff with code='{0}'", sd.CallIdentifierID)

                            If warnings.AddWarning(warning) Then
                                LoggingUtility.LogWarning("CreateCallsForContract", "SatComCallDataImporter", warning)
                            End If
                            Continue For
                        End If

                        Dim newCall = New [Call]()
                        With newCall
                            .CallTime = sd.CallDateTime
                            .ContractId = cont.ContractId
                            .TariffId = tariff.TariffId
                            .ServiceNumber = sd.OriginatorNo
                            .CalledNumber = sd.DestinationNo
                            .Location = sd.OriginatorCountry
                            .ImportedCallType = tariff.NetworkTariff.Tariff
                            Dim callVolume As Double = Double.Parse(If(sd.Volume, 0))
                            .Volume = callVolume
                            .VolumeUnit = MyBase.GetCallVolumeUnit(sd.Unit)

                            ' (Changed 21/10/2009 rag) [001-96] UnitOfTime per plan.
                            .UnitsOfTime = New UnitsOfTimeCalculator(.VolumeUnit).Calculate(cont.Plan.UnitOfTime, callVolume)
                        End With

                        sd.Imported = True

                        _callRepository.Insert(newCall)
                    Next

                    uow.Commit()
                End Using
                ts.Complete()
            End Using

        Catch ex As Exception
            LoggingUtility.LogException(ex)
            ' Reset warnings if the transaction was rolled back.
            warnings = New Warnings()
        End Try

        Return warnings

    End Function

    ''' <summary>
    ''' Retrieves a Contract Entity given a call, given the phone number that made the call, 
    ''' the time of the call, and the tariff.
    ''' </summary>   
    Protected Function GetContractRec(ByVal phoneNumber As String, ByVal callDateTime As Date, ByVal callIdentifierId As String) As ContractRecordHelper
        Try

            Using uow = New UnitOfWorkScope()

                ' I could cache this collection each time, but what if new contracts get added...
                ' Maybe if speed becomes a problem in the future, something can be done in that area...
                Dim contracts = _contractRepository.FindAllContracts(_airtimeId).ToList()

                ' Lookup contract
                Dim crec As New ContractRecordHelper
                Dim contract = (From c In contracts _
                        Where (If(c.PhoneNumber1, String.Empty).Equals(phoneNumber) Or _
                               If(c.PhoneNumber2, String.Empty).Equals(phoneNumber) Or _
                               If(c.PhoneNumber3, String.Empty).Equals(phoneNumber) Or _
                               If(c.PhoneNumber4, String.Empty).Equals(phoneNumber)) And _
                              (c.ActivationDate <= callDateTime) _
                        Select c.ContractId, c.PlanId, c.ContractNumber, c.Plan.UnitOfTime).FirstOrDefault()

                If Not contract Is Nothing Then
                    crec.ContractID = contract.ContractId
                    crec.PlanID = contract.PlanId
                    crec.ContractNumber = contract.ContractNumber
                    crec.UnitOfTime = contract.UnitOfTime
                    crec.TariffID = 0

                    Dim tariff = (From t In _tariffRepository.FindAllTariffs _
                            Where t.PlanId = contract.PlanId And t.NetworkTariff.Code.Trim() = callIdentifierId.Trim() _
                            Select t).FirstOrDefault()

                    If Not tariff Is Nothing Then
                        crec.TariffID = tariff.TariffId
                    Else
                        LoggingUtility.LogWarning("GetContractRec", "Importer", _
                                                  String.Format("Could not find a Tariff on PlanId={0} with code={1}", crec.PlanID, callIdentifierId))

                        ' No Tariff found - What do we do here ?
                        ' We can't continue with no Tariff, as the Call entry needs it.
                        crec = Nothing
                    End If

                    Return crec
                Else
                    LoggingUtility.LogWarning("GetContractRec", "Importer", _
                                              String.Format("Could not find a contract for Phone={0} on a Plan at '{1}'", phoneNumber, callDateTime))
                End If
                uow.Commit()
            End Using
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

        Return Nothing

    End Function

    Public Overrides Function GetName() As String
        Return "SatCom"
    End Function

End Class
