﻿Imports System.Text

Public MustInherit Class DataImporter
    Implements IDataImporter

    Protected _registeredFolder As String = String.Empty

    ''' <summary>
    ''' Describes the file extensions this call importer cares about.
    ''' It can share file extensions with other importers but if so 
    ''' </summary>
    ''' <returns>A list of registered file extensions</returns>    
    Public MustOverride Function RegisteredExtensions() As IList(Of String) Implements IDataImporter.RegisteredExtensions

    ''' <summary>
    ''' Denotes the subfolder in which call data files for this importer 
    ''' will be placed.
    ''' </summary>    
    ''' <returns>The folder that stores call data for importer</returns>    
    Public ReadOnly Property RegisteredFolder() As String Implements IDataImporter.RegisteredFolder
        Get
            Return _registeredFolder
        End Get
    End Property

    ''' <summary>
    ''' This method does the majority of the work opening the particular text file and importing the 
    ''' data into the corresponding sql table.
    ''' </summary>
    ''' <param name="fileName">filename to import</param>        
    Public MustOverride Function Import(ByVal fileName As String) As Boolean Implements IDataImporter.Import

    ''' <summary>
    ''' Determines if the call already exists in the system.
    ''' </summary>
    ''' <param name="checksum"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public MustOverride Function Exists(ByVal checksum As Byte()) As Boolean Implements IDataImporter.Exists

    ''' <summary>
    ''' Calculates a checksum that is used to identify a unique record.
    ''' All fields in are concatenated and then hashed, to give the checksum.
    ''' </summary>
    ''' <param name="fields">String array of fields to use as the basis for the checksum</param>
    ''' <returns>A hash representing the record</returns>    
    ''' <remarks>
    ''' Rather than use GetHashCode (or extend it), I thought I'd roll my own, 
    ''' mainly because it's easier to work with the fields array to generate 
    ''' the hash I want, then apply MD5 to that.
    ''' </remarks>
    Protected Function CalculateCheckSum(ByVal fields() As String, ByRef checksumString As String) As Byte()

        Return CalculateCheckSum(fields, 0, checksumString)

    End Function

    Protected Function CalculateCheckSum(ByVal fields() As String, ByVal startIndex As Integer, ByRef checksumString As String) As Byte()

        Dim allFields As StringBuilder = New StringBuilder

        For index = startIndex To fields.Length - 1
            allFields.Append(fields(index))
        Next index

        Dim md5Hasher As New MD5CryptoServiceProvider()
        Dim hashedBytes As Byte()
        Dim encoder As New UTF8Encoding()

        checksumString = allFields.ToString()
        hashedBytes = md5Hasher.ComputeHash(encoder.GetBytes(checksumString))

        Return hashedBytes

    End Function

    Public MustOverride Function GetName() As String Implements IDataImporter.GetName

End Class
