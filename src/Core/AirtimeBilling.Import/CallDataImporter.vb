﻿Imports System.Globalization
Imports AirtimeBilling.Common
Imports AirtimeBilling.Logging
Imports System.IO
Imports AirtimeBilling.Core.Enums
''' <summary>
''' This class is the base class for importing call data into 
''' the AirtimeBilling system.  It provides logic to convert the 
''' call data files into airtimebilling call data entries.
''' </summary>
''' <remarks>
''' A CallDataImporter gets its data from files with registered extensions.
''' Eg a .traffic file comes from Vizada.
''' A CallDataImporter can also define a folder to search in.
''' This folder is a sub folder of the overall calldata import folder.
''' A folder + extension combination should be unique amongst all 
''' call data importers.
''' </remarks>
Public MustInherit Class CallDataImporter
    Inherits DataImporter
    Implements ICallDataImporter

    Protected Class ContractRecordHelper
        Public ContractID As Long
        Public PlanID As Long
        Public ContractNumber As String
        Public TariffID As Long
        Public CallCount As Long
        Public UnitOfTime As Integer

        Public Sub New()
            ContractID = 0
            PlanID = 0
            ContractNumber = String.Empty
            TariffID = 0
            CallCount = 0
            UnitOfTime = 0
        End Sub
    End Class

    Protected _callRepository As ICallRepository
    Protected _contractRepository As IContractRepository
    Protected _tariffRepository As ITariffRepository
    Protected _airtimeRepository As IAirtimeProviderRepository

    Protected _airtimeId As Integer

    Protected Sub New(ByVal callRep As ICallRepository, ByVal contRep As IContractRepository, ByVal tarRep As ITariffRepository, ByVal airRep As IAirtimeProviderRepository)
        _callRepository = callRep
        _contractRepository = contRep
        _tariffRepository = tarRep
        _airtimeRepository = airRep
    End Sub

    ''' <summary>
    ''' Gives the friendly name of the Call Data Provider
    ''' </summary>    
    Public Function GetDataProviderDescription() As String
        Dim fullName = Me.GetType().FullName
        Using uow = New UnitOfWorkScope()
            Return _airtimeRepository.FindAll().Single(Function(a) a.ImporterFullName = fullName).Name
        End Using
    End Function

    ''' <summary>
    ''' This method does the majority of the work opening the particular text file and importing the 
    ''' data into the corresponding sql table.
    ''' </summary>
    ''' <param name="fileName">filename to import</param>        
    Public Overrides Function Import(ByVal fileName As String) As Boolean
        If Not File.Exists(fileName) Then
            ' Do Nothing - This file should exist!
            LoggingUtility.LogWarning("Import", "CallDataImporter", "Could not import file because file '" _
                                      + fileName + "' could not be found")
            Return False
        End If

        If ImportCallFile(fileName) Then
            Dim successfulImportMessage = "Calls successfully imported for '" + fileName + "'"
            LoggingUtility.LogDebug("ImportCallFile", "Importer", successfulImportMessage)

            Dim msg As New ImportCompletedEmailMessage() With {.CallDataFileName = fileName, .DataProvider = GetDataProviderDescription()}
            Dim ret = UpdateCallTable()
            If ret.IsSuccessful Then
                msg.UnmatchedPhonenumbers = ret.UnmatchedNumbers
                msg.ImportWarnings = ret.Warnings

                ' (001-16) Send confirmation email when call data is imported into the system
                ' (001-102) Make a list of all phone numbers with calls that could not be matched to a contract
                ' and include in email
                LoggingUtility.SendEmail(ConfigItems.SystemEmailAddress, "Airtime Billing", "Call Data Import", msg, True)

                Return True
            Else
                LoggingUtility.LogWarning("Import", "CallDataImporter", "Could not update call table for " + fileName)
                Return False
            End If
        End If
        LoggingUtility.LogWarning("Import", "CallDataImporter", "Could not import calls for " + fileName)
        Return False

    End Function

    ''' <summary>
    ''' This route updates the activity log for the contract's recorded 
    ''' col contains a collection of contract records, with the CallCount giving
    ''' the number of calls imported for that contract in this import run.
    ''' </summary>
    Protected Sub UpdateActivityLog(ByVal contracts As IDictionary(Of Integer, ContractRecordHelper))

        For Each crec In contracts.Values
            Dim activity As String = String.Format("Imported {0} {1} calls.", crec.CallCount, GetName())
            LoggingUtility.WriteActivity(Constants.TableNames.CONTRACT, crec.ContractID, activity)
            LoggingUtility.LogDebug("UpdateActivityLog", "CallDataImporter", activity)
        Next crec

    End Sub

    ''' <summary>
    ''' This method handles importing the call file into the table for the specific
    ''' call data supplier.
    ''' </summary>
    ''' <param name="fileName">file name for the file containing the call data</param>
    ''' <returns>True if call data is successfully imported into the call data 
    ''' supplier table.  The import process can continue if this returns true</returns>    
    Protected MustOverride Function ImportCallFile(ByVal fileName As String) As Boolean

    ''' <summary>
    ''' Takes all unimported data from the call data supplier table and
    ''' creates associated Call table entries, marking the original data as imported
    ''' Making this public gives others the ability to try and match unimported calls with contracts
    ''' for example just before an invoicing run, to ensure all the calls are being billed.
    ''' </summary>    
    ''' <returns>Returns a List of all phone numbers that could not be matched to a Contract</returns>
    Public MustOverride Function UpdateCallTable() As UpdateCallTableReturn Implements ICallDataImporter.UpdateCallTable

    Protected Function ToDateTime(ByVal sDate As String, ByVal sTime As String) As Date

        Dim aussieCulture As New CultureInfo("en-AU")
        Return Date.Parse(sDate & " " & sTime, aussieCulture)

    End Function

    ''' <summary>
    ''' Looks for unimported calls in the system 
    ''' </summary>
    ''' <param name="contractId"></param>
    ''' <remarks></remarks>
    Public Sub FindCallsForContract(ByVal contractId As Integer) Implements ICallDataImporter.FindCallsForContract
        Using uow = New UnitOfWorkScope()
            Dim contract = _contractRepository.FindAllContracts().FirstOrDefault(Function(c) c.ContractId = contractId)

            If Not contract Is Nothing Then
                If contract.Plan.Network.AirtimeProviderId = _airtimeId Then
                    Dim warnings = CreateCallsForContract(contract)

                    If warnings.HasWarnings > 0 Then
                        Dim callEmail As New CallMatchWarningsEmailMessage With {.ContractNumber = contract.ContractNumber, .ImportWarnings = warnings}
                        LoggingUtility.SendEmail(ConfigItems.SystemEmailAddress, "Airtime Billing", "Call Matching", callEmail, True)
                    End If
                End If
            Else
                LoggingUtility.LogWarning("FindCallsForContract", "CallDataImporter", "Could not find a contract with ContractId=" + contractId)
            End If
        End Using
    End Sub

    Protected MustOverride Function CreateCallsForContract(ByVal cont As Contract) As Warnings

    ''' <summary>
    ''' Takes a string Volume unit from an import file and converts it to CallVolumeUnit
    ''' </summary>
    Protected Function GetCallVolumeUnit(ByVal volumeUnit As String) As CallVolumeUnit
        Select Case volumeUnit.ToUpper()
            Case "MIN", "MINS"
                Return CallVolumeUnit.Time

            Case "MBS", "MBYTE"
                Return CallVolumeUnit.Data

            Case Else
                Return CallVolumeUnit.Discrete

        End Select
    End Function

End Class
