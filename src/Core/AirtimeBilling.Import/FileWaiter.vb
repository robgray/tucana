﻿Imports System.Security.Permissions

''' <summary>
''' Use this class to wait for a file to become available
''' Problem cause by FileSystemWatcher
''' </summary>
''' <remarks></remarks>
Friend Class FileWaiter
    Private filename As String

    Public Sub New(ByVal filename As String)
        Me.filename = filename
    End Sub

    Public Sub Wait(ByVal timeout As Integer)
        Wait(New TimeSpan(0, 0, 0, 0, timeout))
    End Sub

    Public Sub Wait()
        Wait(TimeSpan.MinValue)
    End Sub

    Public Sub Wait(ByVal timeout As TimeSpan)
        System.Diagnostics.Debug.Assert(String.IsNullOrEmpty(filename) = False)
        Dim t As Threading.Thread = New Threading.Thread(New Threading.ThreadStart(AddressOf ThreadWait))
        t.Start()
        If (timeout > TimeSpan.MinValue) Then
            t.Join(timeout)
        Else
            t.Join()
        End If

    End Sub

    Public Sub ThreadWait()
        Do
            Threading.Thread.Sleep(100)
        Loop While (System.IO.File.Exists(filename) And IsFileOpen(filename))
    End Sub

    <PermissionSet(SecurityAction.Demand, Name:="FullTrust")> _
    Protected Shared Function IsFileOpen(ByVal filename As String) As Boolean
        Dim s As System.IO.FileStream = Nothing
        Try
            s = System.IO.File.Open(filename, IO.FileMode.Open, IO.FileAccess.ReadWrite, IO.FileShare.None)
            Return False
        Catch ex As System.IO.IOException
            Return True
        Finally
            If Not s Is Nothing Then
                s.Close()
            End If
        End Try
    End Function

End Class
