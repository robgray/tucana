﻿Imports System.Linq


Public Class VizadataDataRepository
    Inherits Repository
    Implements IVizadaDataRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal db As AirtimeBillingDataContext)
        MyBase.New(db)
    End Sub

    Public Sub Insert(ByVal entity As VizadaData) Implements IVizadaDataRepository.Insert

        Db.VizadaDatas.InsertOnSubmit(entity)

    End Sub

    Public Function FindAll() As IList(Of VizadaData) Implements IVizadaDataRepository.FindAll

        Return Db.VizadaDatas.ToList()

    End Function

    Public Function FindAllNotImported() As IList(Of VizadaData) Implements IVizadaDataRepository.FindAllNotImported

        Dim data = (From v In Db.VizadaDatas _
                Where v.Imported = False _
                Select v)

        Return data.ToList()

    End Function

End Class
