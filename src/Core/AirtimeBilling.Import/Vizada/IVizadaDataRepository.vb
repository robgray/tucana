﻿Public Interface IVizadaDataRepository

    Function FindAll() As IList(Of VizadaData)
    Function FindAllNotImported() As IList(Of VizadaData)
    Sub Insert(ByVal entity As VizadaData)

End Interface
