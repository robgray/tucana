﻿Imports System.Collections.Generic
Imports System.Transactions
Imports AirtimeBilling.Logging
Imports System.Linq
Imports Microsoft.VisualBasic.FileIO
Imports AirtimeBilling.Core.Enums

Public Class VizadaCallDataImporter
    Inherits CallDataImporter

    Protected _vizadaRepository As IVizadaDataRepository

    Private Enum VizadaFields
        Acnt
        InvNo
        ChgTyp
        CrDeb
        MsgRef
        SerNo
        MobNo
        PlatfId
        PlatfNam
        [Date]
        Time
        OceReg
        Sbac
        Orig
        CalledNo
        NumTyp
        ShCod
        Cntry
        SvcCod
        SvcNam
        SvcMain
        SvcSub
        RatPer
        BillV
        ConnV
        SatV
        VolUn
        SatRat
        DestRat
        SatChg
        DestChg
        FacChg
        Disc
        DiscTyp
        TotChg
        Cur
        ISP
    End Enum

    Public Sub New()
        Me.New(New CallRepository, New ContractRepository, New TariffRepository, New VizadataDataRepository, New AirtimeRepository)
    End Sub

    Public Sub New(ByVal callRep As ICallRepository, ByVal contRep As IContractRepository, ByVal tarRep As ITariffRepository, ByVal vizRep As IVizadaDataRepository, ByVal airRep As IAirtimeProviderRepository)
        MyBase.New(callRep, contRep, tarRep, airRep)
        _vizadaRepository = vizRep
        _airtimeId = 1
    End Sub

    Public Overrides Function RegisteredExtensions() As IList(Of String)
        Dim exts As IList(Of String) = New List(Of String)
        exts.Add("traffic")

        Return exts
    End Function

    Public Overrides Function UpdateCallTable() As UpdateCallTableReturn
        Try
            Dim contracts As IDictionary(Of Integer, ContractRecordHelper) = New Dictionary(Of Integer, ContractRecordHelper)
            Dim unmatched As New List(Of String)
            Dim foundFlag As Boolean = False

            Using ts As TransactionScope = New TransactionScope()
                Using uow = New UnitOfWorkScope()

                    Dim vizadaData = _vizadaRepository.FindAllNotImported().ToList()

                    For Each vd In vizadaData

                        ' 001-110 - Do not import calls with $0.00 TotChg.      
                        If Not vd.TotalCharge.HasValue OrElse vd.TotalCharge.Value = 0 Then
                            Continue For
                        End If

                        ' Find matching contract and tariff 
                        Dim crec As ContractRecordHelper = GetContractRec(vd.MobileNumber, vd.CallDateTime, vd.ServiceName)

                        If crec Is Nothing Then
                            ' Could not find contract 
                            If Not unmatched.Contains(vd.MobileNumber) Then
                                unmatched.Add(vd.MobileNumber)
                            End If
                            Continue For
                        End If

                        Dim myCall As [Call] = CreateNewCallFromVizadaData(vd, crec)
                        _callRepository.Insert(myCall)

                        If contracts.ContainsKey(crec.ContractID) = False Then
                            crec.CallCount = 1
                            contracts.Add(crec.ContractID, crec)
                        Else
                            contracts(crec.ContractID).CallCount += 1
                        End If

                        vd.Imported = True
                    Next

                    UpdateActivityLog(contracts)

                    uow.Commit()
                End Using
                ts.Complete()
            End Using

            Return New UpdateCallTableReturn With {.UnmatchedNumbers = unmatched, .IsSuccessful = True}

        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

        Return New UpdateCallTableReturn

    End Function

    Private Function CreateNewCallFromVizadaData(ByVal vd As VizadaData, ByVal crec As ContractRecordHelper) As [Call]
        Dim myCall As New [Call]
        With myCall
            .CallTime = vd.CallDateTime
            .ContractId = crec.ContractID
            .TariffId = crec.TariffID
            .ServiceNumber = vd.MobileNumber
            .CalledNumber = vd.CalledNumber
            .Location = vd.PlatformName
            .ImportedCallType = vd.ServiceName
            Dim callVolume As Double = Double.Parse(vd.BilledVolume)
            .Volume = callVolume
            .VolumeUnit = MyBase.GetCallVolumeUnit(vd.VolumeUnit)

            ' (Changed 21/10/2009 rag) [001-96] UnitOfTime per plan.
            .UnitsOfTime = New UnitsOfTimeCalculator(.VolumeUnit).Calculate(crec.UnitOfTime, callVolume)
        End With

        Return myCall
    End Function


    Protected Overrides Function ImportCallFile(ByVal fileName As String) As Boolean
        Const DELIMITER As String = ","

        Dim currentChecksumFields = New List(Of String)

        Try
            Using ts As TransactionScope = New TransactionScope()
                Using uow = New UnitOfWorkScope

                    Using parser As New TextFieldParser(fileName)
                        parser.SetDelimiters(DELIMITER)
                        If parser.EndOfData = False Then parser.ReadFields() ' Consume first row (headings)
                        While Not parser.EndOfData
                            Dim fields As String() = parser.ReadFields

                            ' 001-164: I would like to exclude AccountNo and InvoiceNo from CheckSum calculation
                            Dim preHashedFieldValues As String = ""
                            Dim checksum As Byte() = CalculateCheckSum(fields, 2, preHashedFieldValues)
                            If currentChecksumFields.Contains(preHashedFieldValues) Then
                                Continue While
                            End If
                            currentChecksumFields.Add(preHashedFieldValues)

                            If Exists(checksum) Then Continue While

                            Dim entity As New VizadaData
                            With entity
                                .AccountNumber = fields(VizadaFields.Acnt)
                                .InvoiceNumber = fields(VizadaFields.InvNo)
                                .ChargeType = fields(VizadaFields.ChgTyp)
                                .CreditDebit = fields(VizadaFields.CrDeb)
                                .MessageReference = fields(VizadaFields.MsgRef)
                                .SerialNumber = fields(VizadaFields.SerNo)
                                .MobileNumber = fields(VizadaFields.MobNo)
                                .PlatformId = fields(VizadaFields.PlatfId)
                                .PlatformName = fields(VizadaFields.PlatfNam)
                                .CallDate = fields(VizadaFields.Date)
                                .CallTime = fields(VizadaFields.Time)
                                .OceanRegion = fields(VizadaFields.OceReg)
                                .SplitBilingCode = fields(VizadaFields.Sbac)
                                .Origin = fields(VizadaFields.Orig)
                                .CalledNumber = fields(VizadaFields.CalledNo)
                                .NumberType = fields(VizadaFields.NumTyp)
                                .ShortCode = fields(VizadaFields.ShCod)
                                .Country = fields(VizadaFields.Cntry)
                                .ServiceCode = fields(VizadaFields.SvcCod)
                                .ServiceName = fields(VizadaFields.SvcNam)
                                .ServiceMain = fields(VizadaFields.SvcMain)
                                .ServiceSub = fields(VizadaFields.SvcSub)
                                .RatingPeriod = fields(VizadaFields.RatPer)
                                .BilledVolume = fields(VizadaFields.BillV)
                                .ConnectVolume = fields(VizadaFields.ConnV)
                                .SatelliteVolume = fields(VizadaFields.SatV)
                                .VolumeUnit = fields(VizadaFields.VolUn)
                                .SatelliteRate = fields(VizadaFields.SatRat)
                                .DestinationRate = fields(VizadaFields.DestRat)
                                .SatelliteCharge = fields(VizadaFields.SatChg)
                                .DestinationSurCharge = fields(VizadaFields.DestChg)
                                .FacilityCharge = fields(VizadaFields.FacChg)
                                .Discount = fields(VizadaFields.Disc)
                                .DiscountType = fields(VizadaFields.DiscTyp)
                                .TotalCharge = fields(VizadaFields.TotChg)
                                .Currency = fields(VizadaFields.Cur)
                                .ISP = fields(VizadaFields.ISP)
                                .Imported = False
                                .Checksum = checksum
                            End With

                            _vizadaRepository.Insert(entity)

                        End While

                        parser.Close()
                    End Using
                    uow.Commit()
                End Using
                ts.Complete()
            End Using

            Return True
        Catch ex As Exception
            ' Log error ???
            LoggingUtility.LogException(ex)
        End Try
        Return False
    End Function

    Public Overrides Function Exists(ByVal checksum As Byte()) As Boolean
        Using uow = New UnitOfWorkScope
            Return _vizadaRepository.FindAll().Any(Function(v) v.Checksum = checksum)
        End Using
    End Function

    ''' <summary>
    ''' Finds calls in the import tables for the contract and creates Call entries.
    ''' </summary>
    ''' <param name="cont"></param>
    ''' <remarks></remarks>
    Protected Overrides Function CreateCallsForContract(ByVal cont As Contract) As Warnings

        Dim warnings = New Warnings

        Try
            Using ts = New TransactionScope()
                Using uow = New UnitOfWorkScope
                    ' First get all possible 
                    Dim calls = (From c In _vizadaRepository.FindAllNotImported() _
                            Where (c.MobileNumber = If(cont.PhoneNumber1, String.Empty) Or _
                                   c.MobileNumber = If(cont.PhoneNumber2, String.Empty) Or _
                                   c.MobileNumber = If(cont.PhoneNumber3, String.Empty) Or _
                                   c.MobileNumber = If(cont.PhoneNumber4, String.Empty)) _
                            Select c).ToList()

                    ' Found matched calls
                    For Each vd In calls

                        ' Based on feedback from the user group on 22/02/2012 they would like calls to be assigned to this 
                        ' cotract as long as it is Active/Suspended and not closed, and the call was made after the
                        ' contract activation date.  This is to disregard the contract end date.
                        'If Not (vd.CallDateTime >= cont.ActivationDate And vd.CallDateTime <= cont.EndDate) Then
                        'Continue For
                        'End If
                        If cont.ActivationDate > vd.CallDateTime AndAlso Not (cont.ContractStatus = ContractStatus.Active Or cont.ContractStatus = ContractStatus.Suspended) Then
                            Continue For
                        End If

                        ' 001-110 - Do not import calls with $0.00 TotChg.                    
                        If Not vd.TotalCharge.HasValue OrElse vd.TotalCharge.Value = 0 Then
                            Continue For
                        End If

                        Dim callType As String = vd.ServiceName
                        Dim tariff = _tariffRepository.FindAllTariffs().FirstOrDefault(Function(t) t.NetworkTariff.Tariff = callType)
                        If tariff Is Nothing Then
                            Dim warning = String.Format("Could not find tariff for '{0}' on any Network", callType)

                            If warnings.AddWarning(warning) Then
                                LoggingUtility.LogWarning("CreateCallsForContract", "VizadaCallDataImporter", warning)
                            End If
                            Continue For
                        End If

                        Dim newCall = New [Call]()
                        With newCall
                            .CallTime = vd.CallDateTime
                            .ContractId = cont.ContractId
                            .TariffId = tariff.TariffId
                            .ServiceNumber = vd.MobileNumber
                            .CalledNumber = vd.CalledNumber
                            .Location = vd.PlatformName
                            .ImportedCallType = callType
                            Dim callVolume As Double = Double.Parse(vd.BilledVolume)
                            .Volume = callVolume
                            .VolumeUnit = MyBase.GetCallVolumeUnit(vd.VolumeUnit)

                            ' (Changed 21/10/2009 rag) [001-96] UnitOfTime per plan.
                            .UnitsOfTime = New UnitsOfTimeCalculator(.VolumeUnit).Calculate(cont.Plan.UnitOfTime, callVolume)

                        End With

                        _callRepository.Insert(newCall)
                        vd.Imported = True
                    Next
                    uow.Commit()
                End Using
                ts.Complete()
            End Using

        Catch ex As Exception
            LoggingUtility.LogException(ex)
            warnings = New Warnings()
        End Try

        Return warnings

    End Function

    ''' <summary>
    ''' Retrieves a Contract Entity given a call, given the phone number that made the call, 
    ''' the time of the call, and the tariff.
    ''' </summary>   
    Protected Function GetContractRec(ByVal strPhone As String, ByVal dtCallTime As Date, ByVal strServiceName As String) As ContractRecordHelper
        Try
            Using uow = New UnitOfWorkScope()

                ' We could cache this collection each time, but what if new contracts get added...
                ' Maybe if speed becomes a problem in the future, something can be done in that area...
                Dim contracts = _contractRepository.FindAllContracts(_airtimeId).ToList()

                ' Lookup contract
                Dim crec As New ContractRecordHelper
                Dim contract = (From c In contracts _
                        Where (If(c.PhoneNumber1, "").Equals(strPhone) Or _
                               If(c.PhoneNumber2, "").Equals(strPhone) Or _
                               If(c.PhoneNumber3, "").Equals(strPhone) Or _
                               If(c.PhoneNumber4, "").Equals(strPhone)) And _
                              (dtCallTime >= c.ActivationDate Or dtCallTime <= c.EndDate) _
                        Select c.ContractId, c.PlanId, c.ContractNumber, c.Plan.UnitOfTime).FirstOrDefault()

                If Not contract Is Nothing Then
                    crec.ContractID = contract.ContractId
                    crec.PlanID = contract.PlanId
                    crec.ContractNumber = contract.ContractNumber
                    crec.UnitOfTime = contract.UnitOfTime
                    crec.TariffID = 0

                    Dim tariff = (From t In _tariffRepository.FindAllTariffs _
                            Where t.PlanId = contract.PlanId And t.NetworkTariff.Tariff.Equals(strServiceName) _
                            Select t).FirstOrDefault()

                    If Not tariff Is Nothing Then
                        crec.TariffID = tariff.TariffId
                    Else
                        LoggingUtility.LogWarning("GetContractRec", "Importer", _
                                                  String.Format("Could not find a Tariff on PlanId={0} with name={1}", crec.PlanID, strServiceName))

                        ' No Tariff found - What do we do here ?
                        ' We can't continue with no Tariff, as the Call entry needs it.
                        crec = Nothing
                    End If

                    Return crec
                Else
                    LoggingUtility.LogWarning("GetContractRec", "Importer", _
                                              String.Format("Could not find a contract for Phone={0} active on {1}", strPhone, dtCallTime))
                End If
            End Using
        Catch ex As Exception
            LoggingUtility.LogException(ex)
        End Try

        Return Nothing

    End Function

    Public Overrides Function GetName() As String
        Return "Vizada"
    End Function

End Class