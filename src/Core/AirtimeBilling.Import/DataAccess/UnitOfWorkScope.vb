﻿Imports AirtimeBilling.Common


Public Class UnitOfWorkScope
    Implements IDisposable

    Private _disposed As Boolean
    Private _unitOfWork As IUnitOfWork
    Private Shared _runningScopes As Stack(Of UnitOfWorkScope)

    Public Sub New()

        If UnitOfWorkManager.Current Is Nothing Then
            UnitOfWorkManager.Current = New AirtimeBillingDataContext(ConfigItems.ConnectionString)
        End If
        _unitOfWork = UnitOfWorkManager.Current

        RegisterScope(Me)

    End Sub

    Public Sub Commit()
        _unitOfWork.Commit()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub

    Private Sub Dispose(ByVal disposing As Boolean)

        If Not disposing Then Return
        If _disposed Then Return

        UnregisterScope(Me)
        _disposed = True

    End Sub

    Private ReadOnly Property UnitOfWork() As IUnitOfWork
        Get
            Return _unitOfWork
        End Get
    End Property

    Private Shared ReadOnly Property RunningScopes() As Stack(Of UnitOfWorkScope)
        Get
            If _runningScopes Is Nothing Then
                _runningScopes = New Stack(Of UnitOfWorkScope)
            End If
            Return _runningScopes
        End Get
    End Property

    Private Shared Sub RegisterScope(ByVal scope As UnitOfWorkScope)
        If scope Is Nothing Then Return

        UnitOfWorkManager.Current = scope.UnitOfWork
        RunningScopes.Push(scope)

    End Sub

    Private Shared Sub UnregisterScope(ByVal scope As UnitOfWorkScope)

        RunningScopes.Pop()

        If (RunningScopes.Count > 0) Then
            Dim currentScope As UnitOfWorkScope = RunningScopes.Peek()
            UnitOfWorkManager.Current = currentScope.UnitOfWork
        Else
            UnitOfWorkManager.Current = Nothing
        End If

    End Sub

End Class
