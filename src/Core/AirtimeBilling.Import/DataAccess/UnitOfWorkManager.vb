﻿
Public Class UnitOfWorkManager

    Private Shared _current As IUnitOfWork

    Public Shared Property Current() As IUnitOfWork
        Get
            Return _current
        End Get
        Set(ByVal value As IUnitOfWork)
            _current = value
        End Set
    End Property

End Class
