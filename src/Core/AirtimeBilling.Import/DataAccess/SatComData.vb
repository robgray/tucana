﻿Imports System.Globalization

Partial Public Class SatComData

    Public ReadOnly Property CallDateTime() As DateTime
        Get
            Dim aussieCulture As New CultureInfo("en-AU")
            Return DateTime.Parse(Me.Date & " " & Me.Time, aussieCulture)
        End Get
    End Property

End Class
