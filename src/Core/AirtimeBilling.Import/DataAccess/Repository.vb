﻿
Public MustInherit Class Repository

    Private _db As AirtimeBillingDataContext

    Protected Sub New()

    End Sub

    Protected Sub New(ByVal db As AirtimeBillingDataContext)
        _db = db
    End Sub

    Protected ReadOnly Property Db() As AirtimeBillingDataContext
        Get
            Dim uow As IUnitOfWork = UnitOfWorkManager.Current
            Return If(_db, CType(uow, AirtimeBillingDataContext))
        End Get
    End Property

End Class
