﻿
Public Class PlanRepository
    Inherits Repository
    Implements IPlanRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal db As AirtimeBillingDataContext)
        MyBase.New(db)
    End Sub

    Public Function FindAllPlans() As IList(Of Plan) Implements IPlanRepository.FindAllPlans

        Return Db.Plans.ToList()

    End Function
End Class
