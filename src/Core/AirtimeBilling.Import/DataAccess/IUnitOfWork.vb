﻿Public Interface IUnitOfWork
    Inherits IDisposable

    Sub Commit()

End Interface