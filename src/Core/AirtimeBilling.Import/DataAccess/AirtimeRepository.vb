﻿
Public Class AirtimeRepository
    Inherits Repository
    Implements IAirtimeProviderRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal db As AirtimeBillingDataContext)
        MyBase.New(db)
    End Sub

    Public Function FindAll() As IList(Of AirtimeProvider) Implements IAirtimeProviderRepository.FindAll

        Return Db.AirtimeProviders.ToList()

    End Function
End Class
