﻿
Public Class TariffRepository
    Inherits Repository
    Implements ITariffRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal db As AirtimeBillingDataContext)
        MyBase.New(db)
    End Sub

    Public Function FindAllTariffs() As IList(Of Tariff) Implements ITariffRepository.FindAllTariffs

        Return Db.Tariffs.ToList()

    End Function
End Class
