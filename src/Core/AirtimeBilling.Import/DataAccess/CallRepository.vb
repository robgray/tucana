﻿
Public Class CallRepository
    Inherits Repository
    Implements ICallRepository

    Public Sub New()
        MyBase.New()
    End Sub

    Public Sub New(ByVal db As AirtimeBillingDataContext)
        MyBase.New(db)
    End Sub

    Public Sub Insert(ByVal data As [Call]) Implements ICallRepository.Insert

        Db.Calls.InsertOnSubmit(data)

    End Sub

End Class
