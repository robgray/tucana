﻿Imports System.Data.Linq
Imports SharpStudios.AirtimeBilling.Common

Public Class DataContextFactory

    Private Shared CurrentContext As DataContext

    Public Shared Function Create() As DataContext
        CurrentContext = New LandwideCMSDataContext(ConfigItems.ConnectionString)
        Return CurrentContext
    End Function

    Public Shared ReadOnly Property Current() As DataContext
        Get
            Return CurrentContext
        End Get
    End Property

End Class
