﻿Imports System.Linq

    Public Class ContractRepository
        Inherits Repository
        Implements IContractRepository

        Public Sub New()
            MyBase.New()
        End Sub

        Public Sub New(ByVal db As AirtimeBillingDataContext)
            MyBase.New(db)
        End Sub

    Public Function FindAllContracts() As IList(Of Contract) Implements IContractRepository.FindAllContracts

        Return Db.Contracts.ToList()

    End Function

    Public Function FindAllContracts(ByVal airtimeId As Integer) As IList(Of Contract) Implements IContractRepository.FindAllContracts

        Return Db.Contracts.Where(Function(c) c.Plan.Network.AirtimeProviderId = airtimeId).ToList()

    End Function
    End Class
