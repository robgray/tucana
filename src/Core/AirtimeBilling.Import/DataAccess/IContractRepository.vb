﻿Public Interface IContractRepository

    Function FindAllContracts() As IList(Of Contract)
    Function FindAllContracts(ByVal airtimeId As Integer) As IList(Of Contract)

End Interface
