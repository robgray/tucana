﻿Imports System.Globalization

Partial Public Class VizadaData

    Public ReadOnly Property CallDateTime()
        Get
            Dim aussieCulture As New CultureInfo("en-AU")
            Return DateTime.Parse(Me.CallDate & " " & Me.CallTime, aussieCulture)
        End Get
    End Property

End Class
