﻿using System;
using System.Collections.Generic;
using System.Threading;
using AirtimeBilling.Logging;

namespace AirtimeBilling.Monitor
{
    /// <summary>
    /// Coordinates and manages all jobs.
    /// </summary>
    public static class JobCoordinator
    {
        private readonly static IList<JobManagerItem> jobItems;
        private static Timer jobTimer;

        static JobCoordinator()
        {                        
            // Load all jobs into system.
            // This could be made configurable at some point...
            // BUT... YAGNI....
            jobItems = new List<JobManagerItem>
                       {
                           new JobManagerItem(new ContractStatusMonitor())
                       };            
        }

        static void CheckJobItems(object sender)
        {
            foreach (var job in jobItems)
            {
                if (job.IsExecuting) continue;                
                if (job.Job.NextRunTime > DateTime.Now) continue;

                job.IsExecuting = true;
                ThreadPool.QueueUserWorkItem(RunJobItem, job);
            }
        }

        private static void RunJobItem(object state)
        {
            var item = (JobManagerItem)state;
            try
            {
                item.Job.Execute();
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                item.IsExecuting = false;
            }
        }

        public static void Start()
        {
            LoggingUtility.LogDebug("Start", "AirtimeBilling.Monitor.JobCoordinator",
                                    "Starting the Contract Status Monitor");
            jobTimer = new Timer(CheckJobItems, null, 0, 1000);                        
        }

        public static void Stop()
        {
            LoggingUtility.LogDebug("Stop", "AirtimeBilling.Monitor.JobCoordinator",
                                    "Stopping the Contract Status Monitor");
            jobTimer.Dispose();
        }
    }
}
