﻿namespace AirtimeBilling.Monitor
{
    /// <summary>
    /// Manages the jobs to be performed in the system.
    /// Keeps track of the job executing.
    /// </summary>
    class JobManagerItem
    {
        public JobManagerItem(ScheduledJob job)
        {
            Job = job;
        }

        public ScheduledJob Job { get; private set; }

        public bool IsExecuting { get; set; }
    }
}
