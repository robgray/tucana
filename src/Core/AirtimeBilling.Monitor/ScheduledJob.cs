﻿using System;

namespace AirtimeBilling.Monitor
{
    /// <summary>
    /// Base class for a Scheduled Job.
    /// Scheduled Jobs are actions run at regular intervals by the system.
    /// And used to keep the system in a healthy state.    
    /// </summary>
    public abstract class ScheduledJob
    {
        public DateTime? LastRunTime { get; protected set; }
        
        public DateTime NextRunTime 
        { 
            get
            {
                // If job hasn't been run before, lets run it at startup.
                if (LastRunTime == null) return DateTime.Now;
                DateTime lastRun = LastRunTime.Value;

                switch (ExecutionFrequency)
                {                    
                    case ScheduleFrequency.Seconds:
                        return lastRun.AddSeconds(ExecutionInterval);                        

                    case ScheduleFrequency.Minutes:
                        return lastRun.AddMinutes(ExecutionInterval);
                        
                    case ScheduleFrequency.Hours:
                        return lastRun.AddHours(ExecutionInterval);
                        
                    case ScheduleFrequency.Days:
                        return lastRun.AddDays(ExecutionInterval);
                        
                    case ScheduleFrequency.Months:
                        return lastRun.AddMonths((int)ExecutionInterval);
                        
                    case ScheduleFrequency.Years:
                        return lastRun.AddYears((int)ExecutionInterval);
                        
                    default:        
                        // Also ScheduleFrequency.Off
                        // Put back a day to ensure it never runs.
                        return DateTime.Now.AddDays(-1);
                }
            }
        }

        protected abstract ScheduleFrequency ExecutionFrequency { get; }

        protected abstract double ExecutionInterval { get; }

        /// <summary>
        /// Execution of the task to be performed.
        /// </summary>
        public abstract void Execute();        
    }
}
