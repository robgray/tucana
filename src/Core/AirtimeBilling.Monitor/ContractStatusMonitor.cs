﻿using System;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.DataAccess.Repositories;
using AirtimeBilling.Logging;
using System.Linq;

namespace AirtimeBilling.Monitor
{
    /// <summary>
    /// Periodically checks a Contract to see if the contract status needs changing.
    /// This could occur when a contract comes off Suspension.    
    /// </summary>
    public class ContractStatusMonitor : ScheduledJob 
    {
        private IContractRepository contractRepository;

        public ContractStatusMonitor() : 
            this(RepositoryFactory.GetRepository<IContractRepository>()) { }

        public ContractStatusMonitor(IContractRepository contract)
        {
            contractRepository = contract;
        }


        protected override ScheduleFrequency ExecutionFrequency
        {
            get { return ScheduleFrequency.Hours; }
        }

        protected override double ExecutionInterval
        {
            get { return 1.0; }
        }

        public override void Execute()                
        {            
            try
            {
                var contracts = contractRepository.GetAllContracts();
                foreach (var contract in contracts) {

                    if (contract.ContractStatus != ContractStatus.Suspended) continue;
                    if (contract.SuspendedUntilDate > DateTime.Now) continue;

                    contract.ContractStatus = ContractStatus.Active;
                    if (contract.IsValid) {
                        if (contractRepository.UpdateContract(contract)) {
                            LoggingUtility.WriteSystemActivity(DateTime.Now, "Contract", contract.Id.Value,
                                                               "Contract has come off suspension and is now active.");
                        }
                    }
                    else {
                        throw new InvalidOperationException(contract.GetRuleViolations().First().ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(Constants.SYSTEM_USER_NAME, ex);                
            }

            LastRunTime = DateTime.Now;
        }
    }
}
