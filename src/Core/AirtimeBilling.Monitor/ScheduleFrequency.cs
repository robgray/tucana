namespace AirtimeBilling.Monitor
{
    /// <summary>
    /// Used to indicate the frequency of job execution
    /// </summary>
    public enum ScheduleFrequency
    {
        Off = 0,
        Seconds,
        Minutes,
        Hours,
        Days,
        Months,
        Years
    }
}