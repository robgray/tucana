﻿using System;
using AirtimeBilling.Import.SatComData.Properties;
using Microsoft.VisualBasic.FileIO;
using System.IO;

namespace AirtimeBilling.Import.SatComData
{
    class Program
    {
        static void Main(string[] args)                
        {
            PrintHeading();

            if (!File.Exists(Settings.Default.ImportFile))
            {
                Console.WriteLine("Could not find data file at '" + Settings.Default.ImportFile);
                return;
            }

            if (File.Exists(Settings.Default.OutputFile))
                File.Delete(Settings.Default.OutputFile);

            var parser = new TextFieldParser(Settings.Default.ImportFile);
            using (var sw = new StreamWriter(Settings.Default.OutputFile))
            {
                parser.SetDelimiters(new[] {"\t"});

                // Consume first row.
                if (!parser.EndOfData) parser.ReadFields();
                while (!parser.EndOfData)
                {
                    var fields = parser.ReadFields();

                    var call = new SatComCallIndentifier(fields);                    
                    Console.WriteLine(call.GetSqlInsert());
                    sw.WriteLine(call.GetSqlInsert());
                }
            }
            Console.WriteLine();
            Console.WriteLine("Outputted sql to: " + Settings.Default.OutputFile);
            Console.ReadLine();
        }

        static void PrintHeading()
        {
            Console.WriteLine("(C) 2009 Sharp Studios - SatCom Data importing");
            Console.WriteLine();
        }
    }
}
