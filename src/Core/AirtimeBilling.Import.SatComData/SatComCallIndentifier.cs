﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirtimeBilling.Import.SatComData
{
    public class SatComCallIndentifier
    {
        public SatComCallIndentifier(string[] fields)
        {
            CallIdentifierId = fields[2];
            CalledToCallType = fields[1];
            InitiatorCallType = fields[0];
        }

        public string CallIdentifierId { get; private set; }
        public string InitiatorCallType { get; private set; }
        public string CalledToCallType { get; private set; }

        /// <summary>
        /// Generates a sql insert statement.
        /// </summary>
        /// <returns></returns>
        public string GetSqlInsert()
        {
            return
                string.Format(
                    "INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) " +
                    "VALUES ('{0}','{1}','{2}')", CallIdentifierId, CalledToCallType, InitiatorCallType);
        }
    }
}
