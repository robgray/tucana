﻿using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.DataAccess.Repositories 
{
    internal partial class Call
    {
        private const int SecondsPerMinute = 60;

        public decimal Cost
        {
            get
            {
        
                // [001-121] UnitCost is now per minute not per unit of time.
                decimal unitOfTimePerMinute = SecondsPerMinute/(decimal) Contract.Plan.UnitOfTime;

                decimal flagFall = Tariff.HasFlagfall ? Contract.Plan.Flagfall : 0;
                decimal unitsOfTime = UnitsOfTime ?? 0;
                //decimal unitsOfTime = Units;

                
                decimal costPerUnitOfTime = Tariff.UnitCost == null
                                                    ? 0
                                                    : (Tariff.UnitCost.Value/ (VolumeUnit == 1 ? unitOfTimePerMinute : 1));
                
                
                return flagFall + (unitsOfTime * costPerUnitOfTime);                
            }
        }

        /// <summary>
        /// The volume at which the call was costed (appears in the "Duration")
        /// </summary>
        public decimal CostedVolume
        {
            get
            {
                var timeBlockSize = Contract.Plan.UnitOfTime;
                if (VolumeUnit == (int)CallVolumeUnit.Time)
                    return (UnitsOfTime.HasValue ? timeBlockSize*UnitsOfTime.Value : 0) / 60;                
                return UnitsOfTime.Value;                
            }
        }
    }
}
