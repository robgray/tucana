﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class ContactRepository : IContactRepository
    {
        #region IContactRepository Members

        public void InsertContact(AirtimeBilling.Core.Entities.Contact contact)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var cont = new Contact();                
                cont.PopulateFromEntity(contact);

                db.Contacts.InsertOnSubmit(cont);
                db.SubmitChanges();
                contact.Inserted(cont.ContactId);
            }
        }

        public bool UpdateContact(AirtimeBilling.Core.Entities.Contact contact)
        {
            // Can't update new contacts!
            if (contact.Id == null)
            {
                throw new Exception("Cannot update new Contacts");
            }

            using (var db = DbFactory.GetDataContext())
            {
                Contact cont = db.Contacts.Where(c => c.ContactId == contact.Id.Value).SingleOrDefault();
                if (cont != null)
                {                    
                    cont.PopulateFromEntity(contact);

                    db.SubmitChanges();
                    return true;
                }
                else
                {
                    throw new Exception("Could not find this Contact in the database");
                }
            }
        }

        public AirtimeBilling.Core.Entities.Contact GetContactEntity(int contactId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                Contact contact = db.Contacts.Where(c => c.ContactId == contactId).SingleOrDefault();
                return contact.CreateEntity();
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contact> GetAllContacts()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Contact> contacts = new List<AirtimeBilling.Core.Entities.Contact>();
                foreach (var contact in db.Contacts)
                {
                    contacts.Add(contact.CreateEntity());
                }
                return contacts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contact> FindContactsByContactName(string contactName)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from cont in db.Contacts
                             where cont.Name.Contains(contactName) && cont.Name.Length > 0
                             select cont;

                IList<AirtimeBilling.Core.Entities.Contact> contacts = new List<AirtimeBilling.Core.Entities.Contact>();
                foreach (var contact in search)
                {
                    contacts.Add(contact.CreateEntity());
                }
                return contacts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contact> FindContactsByMobilePhone(string mobilePhone)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from cont in db.Contacts
                             where cont.MobilePhone == mobilePhone && cont.MobilePhone.Length > 0 
                             select cont;

                IList<AirtimeBilling.Core.Entities.Contact> contacts = new List<AirtimeBilling.Core.Entities.Contact>();
                foreach (var contact in search)
                {
                    contacts.Add(contact.CreateEntity());
                }
                return contacts;
            }
        }

        public AirtimeBilling.Core.Entities.Contact this[int id]
        {
            get
            {
                return GetContactEntity(id);
            }
            set
            {
                if (id == value.Id.Value)
                {
                    UpdateContact(value);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public void Delete(AirtimeBilling.Core.Entities.Contact entity)
        {
            using (var db = DbFactory.GetDataContext()) {
                var contact = db.Contacts.FirstOrDefault(c => c.ContactId == entity.Id.Value);
                if (contact != null) {
                    db.Contacts.DeleteOnSubmit(contact);
                    db.SubmitChanges();
                }
            }
        }

        #endregion
    }
}
