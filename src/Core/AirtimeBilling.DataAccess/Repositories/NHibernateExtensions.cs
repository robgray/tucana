using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using NHibernate;
using NHibernate.Type;

namespace Mammoth.BigpondMusic.Data
{
	public static class NHibernateExtensions
	{
		public static ISQLQuery AddScalars(this ISQLQuery query, IEnumerable<KeyValuePair<string, IType>> scalarDefinitions)
		{
			foreach (var scalarDefinition in scalarDefinitions) {
				query = query.AddScalar(scalarDefinition.Key, scalarDefinition.Value);
			}
			return query;
		}

		public static IDictionary<string, object> CreateTuple(this IDictionary<string, IType> fields, IEnumerable<object> values, bool nullsAsDBNull)
		{
			return CreateTuple(fields.Keys, values, nullsAsDBNull);
		}
		public static IDictionary<string, object> CreateTuple(this IEnumerable<string> fields, IEnumerable<object> values, bool nullsAsDBNull)
		{
			IDictionary<string, object> tuple = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

			IEnumerator<object> valueEnumerator = values.GetEnumerator();
			foreach (var field in fields) {
				if (!valueEnumerator.MoveNext()) {
					throw new Exception("Not enough values to create tuple.");
				}
				object value = valueEnumerator.Current;
				if (nullsAsDBNull && null == value) {
					value = DBNull.Value;
				}
				tuple[field] = value;
			}

			return tuple;
		}

		public static IDictionary<string, object> AsDictionary(this IDataRecord record)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
			for (int ii = 0; ii < record.FieldCount; ii++) {
				dictionary[record.GetName(ii)] = record[ii];
			}
			return dictionary;
		}
	}
}
