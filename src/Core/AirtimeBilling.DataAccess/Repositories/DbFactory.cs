﻿using AirtimeBilling.Common;

namespace AirtimeBilling.DataAccess.Repositories
{
    public static class DbFactory
    {        
        /// <summary>
        /// Returns a AirtimeBillingDataContext pointing to the correct database.
        /// </summary>
        /// <returns></returns>
        internal static AirtimeBillingDataContext GetDataContext()
        {
            var dc = new AirtimeBillingDataContext(ConfigItems.ConnectionString);
            
            return dc;
        }
    }
}
