﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class PlanRepository : IPlanRepository 
    {
        #region IPlanRepository Members

        public AirtimeBilling.Core.Entities.Plan GetPlan(int planId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var plan = db.Plans.Where(p => p.PlanId == planId && p.IsActive).SingleOrDefault();
                return PopulatePlanEntityFromPlan(plan);                                       
            }
        }

        #endregion

        #region IRepository<Plan> Members

        public AirtimeBilling.Core.Entities.Plan this[int id]
        {
            get
            {
                return GetPlan(id);                    
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region IPlanRepository Members

        public IList<AirtimeBilling.Core.Entities.Plan> GetPlansByNetworkId(int networkId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var plans = from p in db.Plans
                            where p.NetworkId == networkId && p.IsActive
                            select p;

                IList<AirtimeBilling.Core.Entities.Plan> list = new List<AirtimeBilling.Core.Entities.Plan>();
                foreach (var plan in plans)
                {                    
                    list.Add(PopulatePlanEntityFromPlan(plan));                    
                }

                return list;
            }
        }

        public void InsertPlan(AirtimeBilling.Core.Entities.Plan plan)
        {
            using (var db = DbFactory.GetDataContext())
            {
                Plan p = new Plan();
                PopulatePlanFromPlanEntity(plan, ref p);
                p.IsActive = true;

                db.Plans.InsertOnSubmit(p);                
                db.SubmitChanges();
                plan.Inserted(p.PlanId);
            }
        }

        public bool UpdatePlan(AirtimeBilling.Core.Entities.Plan plan)
        {            
            using (var db = DbFactory.GetDataContext())
            {
                Plan pln = db.Plans.Where(p => p.PlanId == plan.Id.Value && p.IsActive).SingleOrDefault();
                if (pln != null)
                {
                    PopulatePlanFromPlanEntity(plan, ref pln);

                    db.SubmitChanges();
                    return true;
                }
                else
                {
                    throw new UnknownEntityException(string.Format("Could not find Plan Id={0} in the database", plan.Id.Value));
                }
            }
        }

        public bool DeletePlan(int planId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                // Can be active or not.  doesn't matter because it'll be set to inactive.
                var plan = db.Plans.Where(p => p.PlanId == planId).SingleOrDefault();
                if (plan != null)
                {
                    plan.IsActive = false;                    
                    db.SubmitChanges();
                    return true;
                }                
                throw new UnknownEntityException(string.Format("Could not find Plan Id={0} in the database", planId));
                
            }
        }

        public IList<AirtimeBilling.Core.Entities.Call> GetCallsForPlan(int planId)
        {
            using (var db = DbFactory.GetDataContext()) {

                var calls = from call in db.Calls
                            where call.Contract.Plan.PlanId == planId
                            select call;

                IList<global::AirtimeBilling.Core.Entities.Call> callList = new List<global::AirtimeBilling.Core.Entities.Call>();
                foreach(var call in calls) {
                    
                    callList.Add(call.CreateEntity());

                }

                return callList;
            }
        }
        
        #endregion

        #region Helpers

        private AirtimeBilling.Core.Entities.Plan PopulatePlanEntityFromPlan(Plan plan)
        {
            AirtimeBilling.Core.Entities.Plan entity = null;
            if (plan != null)
            {
                entity = new AirtimeBilling.Core.Entities.Plan(plan.PlanId)
                {
                    Duration = plan.Duration,
                    FlagFall = plan.Flagfall,
                    FreeCallAmount = plan.FreeCallAmount,
                    Name = plan.Name,
                    PlanAmount = plan.PlanAmount,
                    NetworkId = plan.NetworkId,
                    DefaultTariffId = plan.DefaultTariffId.Value,
                    UnitOfTime = plan.UnitOfTime
                };
            }
            return entity;
        }

        private void PopulatePlanFromPlanEntity(AirtimeBilling.Core.Entities.Plan entity, ref Plan plan)
        {
            plan.Name = entity.Name;
            plan.Duration = entity.Duration;
            plan.Flagfall = entity.FlagFall;
            plan.FreeCallAmount = entity.FreeCallAmount;
            plan.PlanAmount = entity.PlanAmount;
            plan.NetworkId = entity.NetworkId;
            plan.DefaultTariffId = entity.DefaultTariffId;
            plan.UnitOfTime = entity.UnitOfTime;
        }

        #endregion        
    }
}
