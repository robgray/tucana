﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    /// <summary>
    /// This class returns system log events for display.
    /// </summary>
    internal class LogRepository : ILogRepository
    {

        #region ILogRepository Members

        public LogEntry GetById(int id)
        {
            using (var db = DbFactory.GetDataContext()) {

                var entity = db.Logs.SingleOrDefault(l => l.LogId == id);
                if (entity != null) return entity.CreateEntity();
                return null;
            }
        }

        public IList<LogEntry> Find(DateTime fromDate, DateTime toDate, string username, LoggingLevels? severity)
        {
            using (var db = DbFactory.GetDataContext()) {

                var logEntries = from log in db.Logs
                                 where log.Timestamp >= fromDate && log.Timestamp <= toDate
                                 orderby log.Timestamp descending 
                                 select log;

                IList<LogEntry> logs = new List<LogEntry>();
                foreach (var log in logEntries) {
                    var entity = log.CreateEntity();
                    if (!string.IsNullOrEmpty(username) && entity.Username != username) continue;
                    
                    if (severity.HasValue && entity.Level != severity.Value) continue;

                    logs.Add(entity);
                }

                return logs;
            }
        }

        #endregion
    }
}
