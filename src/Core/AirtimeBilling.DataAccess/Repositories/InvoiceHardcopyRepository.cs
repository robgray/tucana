﻿using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    public class InvoiceHardcopyRepository : IInvoiceHardcopyRepository
    {
        public string Get(Core.Entities.Invoice invoice)
        {
            return string.Empty;
        }

        public string Save(Core.Entities.Invoice invoice)
        {
            return string.Empty;
        }

        public void Delete(Core.Entities.Invoice invoice)
        {
            
        }

        public bool Move(Core.Entities.Invoice invoice)
        {
            return true;
        }
    }
}
