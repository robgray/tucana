﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class AirtimeProviderRepository : IAirtimeProviderRepository
    {

        #region IAirtimeProviderRepository Members

        public IList<AirtimeBilling.Core.Entities.AirtimeProvider> GetAirtimeProviders()
        {
            using(var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.AirtimeProvider> providers = new List<AirtimeBilling.Core.Entities.AirtimeProvider>();
                foreach(AirtimeProvider provider in db.AirtimeProviders)
                {
                    providers.Add(PopulateProviderEntity(provider));
                }
                return providers;
            }            
        }

        #endregion

        #region IRepository<AirtimeProvider> Members

        public AirtimeBilling.Core.Entities.AirtimeProvider this[int id]
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Helpers

        private AirtimeBilling.Core.Entities. AirtimeProvider PopulateProviderEntity(AirtimeProvider provider)
        {
            if (provider != null)
            {
                var entity = new AirtimeBilling.Core.Entities.AirtimeProvider(provider.AirtimeProviderId)
                {                    
                    Name = provider.Name,
                    ImporterFullName = provider.ImporterFullName
                };
                return entity;
            }
            else
            {
                return null;
            }

        }

        #endregion
    }
}
