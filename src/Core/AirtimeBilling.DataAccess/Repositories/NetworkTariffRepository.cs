﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class NetworkTariffRepository : INetworkTariffRepository
    {
        #region INetworkTariffRepository Members

        public AirtimeBilling.Core.Entities.NetworkTariff GetNetworkTariff(int networkTariffId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tariff = db.NetworkTariffs.Where(t => t.NetworkTariffId == networkTariffId).SingleOrDefault();
                return PopulateEntityFromNetworkTariff(tariff);
            }
        }

        public IList<AirtimeBilling.Core.Entities.NetworkTariff> GetNetworkTariffsByNetworkId(int networkId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tars = from t in db.NetworkTariffs
                           where t.NetworkId == networkId
                           select t;

                IList<AirtimeBilling.Core.Entities.NetworkTariff> tariffs = new List<AirtimeBilling.Core.Entities.NetworkTariff>();
                foreach (var tariff in tars)
                {
                    tariffs.Add(PopulateEntityFromNetworkTariff(tariff));
                }

                return tariffs;
            }
        }

        public bool UpdateNetworkTariff(AirtimeBilling.Core.Entities.NetworkTariff entity)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tariff = db.NetworkTariffs.SingleOrDefault(t => t.NetworkTariffId == entity.Id.Value);

                if (tariff != null)
                {
                    PopulateNetworkTariffFromEntity(entity, ref tariff);

                    try
                    {
                        db.SubmitChanges();                        
                    }
                    catch (ChangeConflictException)
                    {
                        db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.NetworkTariffs);
                        db.SubmitChanges();
                    }
                                                            
                    // if it gets to here it must have succeeded.
                    return true;
                }                
                else
                {
                    throw new UnknownEntityException(string.Format("Could not find NetworkTariff with Id={0}", entity.Id.Value));
                }
            }
        }

        public void InsertNetworkTariff(AirtimeBilling.Core.Entities.NetworkTariff entity)
        {
            if (entity == null) return;

            using (var db = DbFactory.GetDataContext())
            {
                var tariff = new NetworkTariff();
                PopulateNetworkTariffFromEntity(entity, ref tariff);

                db.NetworkTariffs.InsertOnSubmit(tariff);
                try 
                {
                    db.SubmitChanges();
                    entity.Inserted(tariff.NetworkTariffId);
                }
                catch (ChangeConflictException)
                {
                    // Try again if there's a concurrency error
                    db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.NetworkTariffs);
                    db.SubmitChanges();
                }                
            }    
        }

        public bool DeleteNetworkTariff(int networkTariffId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tariff = db.NetworkTariffs.SingleOrDefault(t => t.NetworkTariffId == networkTariffId);

                if (tariff != null)
                {
                    db.NetworkTariffs.DeleteOnSubmit(tariff);
                    db.SubmitChanges();
                    return true;
                }
                else
                {
                    throw new UnknownEntityException(string.Format("Could not find NetworkTariff with Id={0}", networkTariffId));
                }
            }
        }

        #endregion

        #region IRepository<NetworkTariff> Members

        public AirtimeBilling.Core.Entities.NetworkTariff this[int id]
        {
            get
            {
                return this.GetNetworkTariff(id);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Helpers

        private void PopulateNetworkTariffFromEntity(AirtimeBilling.Core.Entities.NetworkTariff entity, ref NetworkTariff tariff)
        {
            tariff.Code = entity.Code;
            tariff.IsCountedInFreeCall = entity.IsCountedInFreeCall;
            tariff.NetworkId = entity.NetworkId;
            tariff.Tariff = entity.Name;
            tariff.HasFlagfall = entity.HasFlagfall;
        }

        private AirtimeBilling.Core.Entities.NetworkTariff PopulateEntityFromNetworkTariff(NetworkTariff tariff)
        {
            AirtimeBilling.Core.Entities.NetworkTariff entity = null;
            if (tariff != null)
            {
                entity = new AirtimeBilling.Core.Entities.NetworkTariff(tariff.NetworkTariffId)
                {                    
                    Code = tariff.Code,
                    Name = tariff.Tariff,
                    IsCountedInFreeCall = tariff.IsCountedInFreeCall,
                    NetworkId = tariff.NetworkId,
                    HasFlagfall = tariff.HasFlagfall 
                };
            }

            return entity;
        }

        #endregion
    }
}
