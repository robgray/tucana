using System;
using System.Runtime.Remoting.Messaging;
using System.Web;
using NHibernate;
using NHibernate.Cache;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using log4net;
using log4net.Appender;
using log4net.Repository.Hierarchy;
using log4net.Core;
using System.Data;
using System.Collections.Generic;
using System.IO;

namespace Mammoth.BigpondMusic.Data
{
    /// <summary>
    /// Handles creation and management of sessions and transactions.
    /// </summary>
    public sealed class NHibernateSessionManager
    {
        #region Thread-safe, lazy Singleton

        /// <summary>
        /// This is a thread-safe, lazy singleton.  See http://www.yoda.arachsys.com/csharp/singleton.html
        /// for more details about its implementation.
        /// </summary>
        public static NHibernateSessionManager Instance {
            get {
                return Nested.NHibernateSessionManager;
            }
        }

        /// <summary>
        /// Initializes the NHibernate session factory upon instantiation.
        /// </summary>
        private NHibernateSessionManager() {
            InitSessionFactory();
        }

        /// <summary>
        /// Assists with ensuring thread-safe, lazy singleton
        /// </summary>
        private class Nested
        {
            static Nested() { }
            internal static readonly NHibernateSessionManager NHibernateSessionManager = 
                new NHibernateSessionManager();
        }

        #endregion

		private static readonly Logger _logger = new Logger("NHibernateSessionManager");

        private void InitSessionFactory() {
			Configuration cfg = new Configuration().Configure();
            cfg.SetProperty("hibernate.default_schema", "dbo");
            cfg.AddAssembly("Mammoth.BigpondMusic.Content.Persistence");
			sessionFactory = cfg.BuildSessionFactory();

            //This starts log4net logging.
			log4net.Config.XmlConfigurator.Configure();
        }

        /// <summary>
        /// Allows you to register an interceptor on a new session. This may not be called if there is already
        /// an open session attached to the HttpContext. If you have an interceptor to be used, modify
        /// the HttpModule to call this before calling BeginTransaction().
        /// </summary>
        public void RegisterInterceptor(IInterceptor interceptor) {
            ISession session = ContextSession;

            if (session != null && session.IsOpen) {
                throw new CacheException("You cannot register an interceptor once a session has already been opened");
            }

            GetSession(interceptor);
        }

        public ISession GetSession() {
            return GetSession(null);
        }

        /// <summary>
        /// Gets a session with or without an interceptor. This method is not called directly; instead,
        /// it gets invoked from other public methods.
        /// </summary>
        private ISession GetSession(IInterceptor interceptor) {
            ISession session = ContextSession;

            if (session == null || !session.IsOpen)
            {
                if (interceptor != null) {
                    session = sessionFactory.OpenSession(interceptor);
                }
                else {
                    session = sessionFactory.OpenSession();
                }
            	session.FlushMode = FlushMode.Commit; // AutoFlush scaled back to occurring on Commit
                
                ContextSession = session;
                if (null == session) throw new Exception("A new Session could not be opened.");
				_logger.Debug("Session opened");
            }

            return session;
        }

		// Port of nHibernate's FlushMode enum
		// (so that use of this interface does not require referencing of nHibernate assemblies)
		public enum SessionFlushType
		{
			Auto = FlushMode.Auto,
			Commit = FlushMode.Commit,
			Never = FlushMode.Never,
			Unspecified = FlushMode.Unspecified
		}

		public SessionFlushType SessionFlushMode
    	{
    		get
    		{
				return (SessionFlushType)GetSession().FlushMode;
    		}
			set
			{
				GetSession().FlushMode = (FlushMode)value;
			}
    	}

		/// <summary>
		/// Has the session been opened yet?
		/// </summary>
    	public bool IsSessionOpen
    	{
    		get
    		{
    			ISession session = ContextSession;
    			return session != null && session.IsOpen;
    		}
    	}

        /// <summary>
        /// Flushes anything left in the session and closes the connection.
        /// </summary>
        public void CloseSession() {
        	CloseSession(true);
        }
        
        /// <summary>
        /// Closes the connection, with the option to flush the Session.
        /// </summary>
        public void CloseSession(bool flush) {
            ISession session = ContextSession;

            if (session != null && session.IsOpen) {
            	try {
            		if (flush) session.Flush();
            	}
				catch {
					RollbackTransaction(false);
					throw;
				}
				finally {
            		session.Close();
            	}
				_logger.Debug("Session closed");
            }

            ContextSession = null;
        }

        public void OutputDDL(string path)
        {
    		Configuration config = new Configuration().Configure();
			SchemaExport se = new SchemaExport(config);
			se.SetOutputFile(path);
			se.Execute(false, false, false, true);
        }
        
		public void BeginTransaction()
        {
        	BeginTransaction(IsolationLevel.Unspecified);
        }

		public void BeginTransaction(IsolationLevel isolationLevel)
		{
			ITransaction transaction = ContextTransaction;

            if (transaction == null) {
                transaction = GetSession().BeginTransaction(isolationLevel);
                ContextTransaction = transaction;
            }
		}

        public void CommitTransaction() {
            ITransaction transaction = ContextTransaction;

            try {
                if (HasOpenTransaction()) {
                    transaction.Commit();
                    ContextTransaction = null;
                }
            }
            catch (HibernateException commitException) {
                try {
                	RollbackTransaction();
                }
				catch (Exception rollbackException) {
					throw new BigpondMusicException(
						"CommitTransaction failed and then RollbackTransaction failed. InnerException is the rollback failure. This is the commit failure: \n"
						+ commitException, rollbackException);
				}
                throw;
            }
        }

        public bool HasOpenTransaction() {
            ITransaction transaction = ContextTransaction;

            return transaction != null && !transaction.WasCommitted && !transaction.WasRolledBack;
        }

        public void RollbackTransaction() {
			RollbackTransaction(true);
        }

    	public void RollbackTransaction(bool closeSession)
    	{
    		ITransaction transaction = ContextTransaction;

    		try {
    			if (HasOpenTransaction()) {
    				bool transactionClosed = false;
    				int closeAttempts = 0;
    				while (!transactionClosed) {
    					try {
    						closeAttempts++;
    						transaction.Rollback();
    						transactionClosed = true;
    					}
    					catch {
    						if (closeAttempts >= 5) {
    							try {
    								transaction.Dispose();
    							}
    							catch {}

    							throw;
    						}
    					}
    				}
    			}

    		}
    		finally {
				ContextTransaction = null;
				GetSession().Clear();
    			if (closeSession) CloseSession(false);
    		}
    	}

		/// <summary>
		/// Execute an SQL statement. NHibernate's entity domain has no knowledge of anything run by this command, so any database changes 
		/// made will not be reflected in the current NHibernate Session. This function is best used for entities that the application no longer 
		/// cares about. Can be processed by NHibernate Transactions.
		/// </summary>
		/// <param name="sqlStatement">The statement to execute.</param>
		public void ExecuteSql(string sqlStatement)
		{
			ExecuteSql(sqlStatement, null);
		}

		private static readonly ILog logSql = LogManager.GetLogger("NHibernate.SQL");

		/// <summary>
		/// Execute an SQL statement. NHibernate's entity domain has no knowledge of anything run by this command, so any database changes 
		/// made will not be reflected in the current NHibernate Session. This function is best used for entities that the application no longer 
		/// cares about. Can be processed by NHibernate Transactions.
		/// </summary>
		/// <param name="sqlStatement">The statement to execute.</param>
		/// <param name="parameters">A collection of Parameters. Reference parameters in the SQL statement with a @ prefix. : is PostgreSQL 
		/// syntax but it also recognises @ SQL Server syntax, @ is used for interoperability.</param>
		public void ExecuteSql(string sqlStatement, IList<IDbDataParameter> parameters)
		{
			int parameterCount = 0;
			if (null != parameters) parameterCount = parameters.Count;
			
			IDbCommand command = ContextSession.Connection.CreateCommand();
			command.CommandTimeout = 6000;
			command.CommandText = sqlStatement;
			if ((null != parameters) && (parameters.Count > 0))
				foreach (IDbDataParameter param in parameters)
					command.Parameters.Add(param);
			
			if (HasOpenTransaction())
				ContextTransaction.Enlist(command);
			
			command.ExecuteNonQuery();
			
			logSql.Debug("ExecuteSql with " + parameterCount + " parameter(s): " + sqlStatement);
		}

    	/// <summary>
        /// If within a web context, this uses <see cref="HttpContext" /> instead of the WinForms 
        /// specific <see cref="CallContext" />.
        /// </summary>
        internal ITransaction ContextTransaction {
            private get {
                if (IsInWebContext()) {
                    return (ITransaction)HttpContext.Current.Items[TRANSACTION_KEY];
                }
                else {
                    return (ITransaction)CallContext.GetData(TRANSACTION_KEY);
                }
            }
            set {
                if (IsInWebContext()) {
                    HttpContext.Current.Items[TRANSACTION_KEY] = value;
                }
                else {
                    CallContext.SetData(TRANSACTION_KEY, value);
                }
            }
        }

        /// <summary>
        /// If within a web context, this uses <see cref="HttpContext" /> instead of the WinForms 
        /// specific <see cref="CallContext" />.
        /// </summary>
        private ISession ContextSession {
            get {
                if (IsInWebContext()) {
                    return (ISession)HttpContext.Current.Items[SESSION_KEY];
                }
                else {
                    return (ISession)CallContext.GetData(SESSION_KEY); 
                }
            }
            set {
                if (IsInWebContext()) {
                    HttpContext.Current.Items[SESSION_KEY] = value;
                }
                else {
                    CallContext.SetData(SESSION_KEY, value);
                }
            }
        }

        private bool IsInWebContext() {
            return HttpContext.Current != null;
        }

        public ISessionFactory SessionFactory
        {
            get
            {
                return sessionFactory;
            }
        }

        private const string TRANSACTION_KEY = "CONTEXT_TRANSACTION";
        private const string SESSION_KEY = "CONTEXT_SESSION";
        private ISessionFactory sessionFactory;
    }
}