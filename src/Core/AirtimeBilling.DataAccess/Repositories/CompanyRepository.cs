﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class CompanyRepository : ICompanyRepository 
    {
        #region IRepository<Company> Members

        public AirtimeBilling.Core.Entities.Company this[int id]
        {
            get
            {
                return GetCompany(id);
            }
            set
            {
                if (id == value.Id.Value)
                {
                    UpdateCompany(value);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        #endregion

        #region ICompanyRepository Members

        public IList<AirtimeBilling.Core.Entities.Company> FindCompaniesByCompanyName(string companyName)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from comp in db.Companies
                             where comp.CompanyName.StartsWith(companyName) && comp.CompanyName.Length > 0
                             select comp;

                IList<AirtimeBilling.Core.Entities.Company> companies = new List<AirtimeBilling.Core.Entities.Company>();
                foreach (var company in search)
                {
                    companies.Add(company.CreateEntity());
                }
                return companies;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Company> FindCompaniesByABN(string ABN)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from comp in db.Companies
                             where comp.ABN.StartsWith(ABN) && comp.ABN.Length > 0
                             select comp;

                IList<AirtimeBilling.Core.Entities.Company> companies = new List<AirtimeBilling.Core.Entities.Company>();
                foreach (var company in search)
                {
                    companies.Add(company.CreateEntity());
                }
                return companies;
            }
        }


        public void InsertCompany(AirtimeBilling.Core.Entities.Company company)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var comp = new Company();
                comp.PopulateFromEntity(company);
                
                db.Companies.InsertOnSubmit(comp);                
                db.SubmitChanges();
                company.Inserted(comp.CompanyId);
            }
        }

        public bool UpdateCompany(AirtimeBilling.Core.Entities.Company company)
        {
            try
            {
                using (var db = DbFactory.GetDataContext())
                {
                    var comp = db.Companies.Where(c => c.CompanyId == company.Id.Value).SingleOrDefault();
                    if (comp != null)
                    {                        
                        comp.PopulateFromEntity(company);

                        db.SubmitChanges();
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
            }
            return false;
        }

        public AirtimeBilling.Core.Entities.Company GetCompany(int companyId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var company = db.Companies.Where(c => c.CompanyId == companyId).SingleOrDefault();

                return company.CreateEntity();
            }
        }

        public IList<AirtimeBilling.Core.Entities.Company> GetAllCompanies()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Company> companies = new List<AirtimeBilling.Core.Entities.Company>();
                foreach (var company in db.Companies)
                {
                    companies.Add(company.CreateEntity());
                }
                return companies;
            }
        }

        public void Delete(AirtimeBilling.Core.Entities.Company entity)
        {
            using (var db = DbFactory.GetDataContext()) {

                var company = db.Companies.FirstOrDefault(c => c.CompanyId == entity.Id.Value);
                if (company != null) {                    
                    db.Companies.DeleteOnSubmit(company);
                    db.SubmitChanges();
                }
            }
        }

        #endregion
    }
}
