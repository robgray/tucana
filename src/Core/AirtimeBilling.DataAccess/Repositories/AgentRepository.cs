﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Logging;
using System.Diagnostics;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class AgentRepository : IAgentRepository
    {
        #region IRepository<Agent> Members

        public AirtimeBilling.Core.Entities.Agent this[int id]
        {
            get
            {
                return GetAgent(id);
            }
            set
            {
                if (id != value.Id)
                {
                    throw new InvalidOperationException();

                }
            }
        }

        #endregion

        #region IAgentRepository Members

        public AirtimeBilling.Core.Entities.Agent GetAgent(int agentId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var agent = db.Agents.Where(a => a.AgentId == agentId).SingleOrDefault();

                return PopulateAgentEntity(agent);
            }
        }

        public IList<AirtimeBilling.Core.Entities.Agent> GetAllAgents()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Agent> agents = new List<AirtimeBilling.Core.Entities.Agent>();
                foreach (var agent in db.Agents)
                {
                    agents.Add(PopulateAgentEntity(agent));
                }

                return agents;
            }
        }

        public bool Update(AirtimeBilling.Core.Entities.Agent entity)
        {
            if (entity == null || entity.Id == null)
            {
                throw new InvalidOperationException("Could not update non-existing Agent");
            }

            try
            {
                using (var db = DbFactory.GetDataContext())
                {
                    var agent = db.Agents.Where(a => a.AgentId == entity.Id.Value).SingleOrDefault();
                    if (agent != null)
                    {
                        PopulateAgentFromAgentEntity(entity, ref agent);
                        db.SubmitChanges();

                        return true;
                    }                  
                    throw new Exception("Could not find Agent with Id=" + entity.Id.Value.ToString());                   
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
            }
            return false;
        }

        public bool Insert(AirtimeBilling.Core.Entities.Agent entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("No Agent supplied");                
            }

            if (entity.Id != null)
            {
                throw new ArgumentOutOfRangeException("Agent Id is not null");
            }

            try
            {
                using (var db = DbFactory.GetDataContext())
                {
                    var agent = new Agent();
                    PopulateAgentFromAgentEntity(entity, ref agent);
                    db.Agents.InsertOnSubmit(agent);
                    db.SubmitChanges();

                    entity.Inserted(agent.AgentId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
            }
            return false;
        }

        #endregion

        #region Helpers

        private AirtimeBilling.Core.Entities.Agent PopulateAgentEntity(Agent agent)
        {
            AirtimeBilling.Core.Entities.Agent entity = null;

            if (agent != null)
            {
                var address = new Address
                                  {
                                      Street = agent.Street,
                                      Suburb = agent.Suburb,
                                      State = agent.State,
                                      Postcode = agent.Postcode
                                  };

                entity = new AirtimeBilling.Core.Entities.Agent(agent.AgentId)
                {                    
                    AgentName = agent.AgentName,
                    Address = address,
                    Email = agent.Email,
                    CommissionPercent = (decimal)agent.CommissionPercent
                };                                                               
            }
            return entity;
        }

        private void PopulateAgentFromAgentEntity(AirtimeBilling.Core.Entities.Agent entity, ref Agent agent)
        {
            Debug.Assert(entity != null);
            agent.AgentName = entity.AgentName;
            agent.Street = entity.Address.Street;
            agent.Suburb = entity.Address.Suburb;
            agent.State = entity.Address.State;
            agent.Postcode = entity.Address.Postcode;
            agent.Email = entity.Email;
            agent.CommissionPercent = (double)entity.CommissionPercent;
        }

        #endregion 
         
    }
}
