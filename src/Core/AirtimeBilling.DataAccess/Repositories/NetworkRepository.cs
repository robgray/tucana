﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class NetworkRepository : INetworkRepository 
    {

        #region INetworkRepository Members

        public AirtimeBilling.Core.Entities.Network GetNetwork(int networkId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var network = db.Networks.Where(n => n.NetworkId == networkId).SingleOrDefault();

                return PopulateNetworkEntity(network);
            }
        }

        public IList<AirtimeBilling.Core.Entities.Network> GetAllNetworks()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Network> networks = new List<AirtimeBilling.Core.Entities.Network>();
                foreach (var network in db.Networks)
                {
                    networks.Add(PopulateNetworkEntity(network));
                }
                return networks;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Network> GetAllActiveNetworks()
        {
            using (var db = DbFactory.GetDataContext())
            {
                var nets = db.Networks.Where(n => n.IsActive);

                IList<AirtimeBilling.Core.Entities.Network> networks = new List<AirtimeBilling.Core.Entities.Network>();
                foreach (var network in nets)
                {
                    networks.Add(PopulateNetworkEntity(network));
                }
                return networks;
            }
        }

        public void InsertNetwork(AirtimeBilling.Core.Entities.Network entity)
        {
            using (var db = DbFactory.GetDataContext())
            {
                if (entity != null)
                {
                    Network network = new Network();
                    PopulateNetworkFromNetworkEntity(entity, ref network);
                    network.IsActive = true;

                    db.Networks.InsertOnSubmit(network);
                    db.SubmitChanges();
                    db.Refresh(System.Data.Linq.RefreshMode.OverwriteCurrentValues, db.Networks);
                    entity.Inserted(network.NetworkId);
                    entity.AirtimeProvider = network.AirtimeProvider.Name;
                }
            }
        }

        public bool UpdateNetwork(AirtimeBilling.Core.Entities.Network entity)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var network = db.Networks.Where(n => n.NetworkId == entity.Id.Value).SingleOrDefault();

                if (network != null)
                {                    
                    PopulateNetworkFromNetworkEntity(entity, ref network);

                    db.SubmitChanges();
                    entity.AirtimeProvider = network.AirtimeProvider.Name;                    
                    return true;
                }
                else
                {
                    throw new UnknownEntityException(string.Format("Could not find network with Id={0} in the database", entity.Id.Value));
                }
            }            
        }

        public bool DeleteNetwork(int networkId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                // We don't care that it maybe active or not.
                var network = db.Networks.Where(n => n.NetworkId == networkId).SingleOrDefault();

                if (network != null)
                {
                    network.IsActive = false;
                    db.SubmitChanges();
                    return true;
                }
                else
                {
                    throw new UnknownEntityException(string.Format("Could not find network with Id={0} in the database", networkId));
                }
            }            
        }

        #endregion

        #region IRepository<Network> Members

        public AirtimeBilling.Core.Entities.Network this[int id]
        {
            get
            {
                return this.GetNetwork(id);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        #region Helpers

        private AirtimeBilling.Core.Entities.Network PopulateNetworkEntity(Network network)
        {
            AirtimeBilling.Core.Entities.Network entity = null;
            if (network != null)
            {
                entity = new AirtimeBilling.Core.Entities.Network(network.NetworkId)
                {                    
                    Name = network.Name,
                    AirtimeProviderId = network.AirtimeProviderId,
                    AirtimeProvider = network.AirtimeProvider.Name,
                    AcceptsNewContracts = network.IsActive
                };
            }

            return entity;
        }

        private void PopulateNetworkFromNetworkEntity(AirtimeBilling.Core.Entities.Network entity, ref Network network)
        {
            network.Name = entity.Name;
            network.AirtimeProviderId = entity.AirtimeProviderId;
            network.IsActive = entity.AcceptsNewContracts;
        }

        #endregion 
    }
}
