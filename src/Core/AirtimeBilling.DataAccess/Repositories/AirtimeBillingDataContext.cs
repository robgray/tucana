﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using AirtimeBilling.Common;
using AirtimeBilling.Logging;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal partial class AirtimeBillingDataContext
    {
        public override void SubmitChanges(ConflictMode failureMode)
        {
            try
            {
                this.Audit<Contract>(c => c.ContractId);
                this.Audit<Account>(a => a.AccountId);
                this.Audit<Company>(c => c.CompanyId);
                this.Audit<Contact>(c => c.ContactId);
                this.Audit<Network>(n => n.NetworkId);
                this.Audit<NetworkTariff>(nt => nt.NetworkTariffId);
                this.Audit<Plan>(p => p.PlanId);
                this.Audit<Tariff>(t => t.TariffId);
                this.Audit<Agent>(a => a.AgentId);

                base.SubmitChanges(failureMode);
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                throw;
            }            
        }
    } 
}
