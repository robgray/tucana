﻿using System;
using System.Linq;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    public class EwayProcessedDataRepository : IEwayProcessedDataRepository
    {
        #region IEwayProcessedDataRepository Members

        public AirtimeBilling.Core.Entities.EwayProcessedData GetById(int id)
        {
            using (var db = DbFactory.GetDataContext()) {
                var data = db.EwayProcessedDatas.SingleOrDefault(d => d.EwayProcessedDataId == id);
                return data != null ? data.CreateEntity() : null;
            }
        }

        public AirtimeBilling.Core.Entities.EwayProcessedData GetByEwayTransactionReference(long transactionReference)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var data = db.EwayProcessedDatas.SingleOrDefault(d => d.EwayTransactionReference == transactionReference);
                return data != null ? data.CreateEntity() : null;
            }
        }

        public void Insert(AirtimeBilling.Core.Entities.EwayProcessedData entity)
        {
            using (var db = DbFactory.GetDataContext()) {

                var data = new EwayProcessedData();
                data.PopulateFromEntity(entity);

                db.EwayProcessedDatas.InsertOnSubmit(data);
                db.SubmitChanges();
                entity.Inserted(data.EwayProcessedDataId);
            }
        }

        #endregion

        #region IRepository<EwayProcessedData> Members

        public AirtimeBilling.Core.Entities.EwayProcessedData this[int id]
        {
            get
            {
                return GetById(id);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion
    }
}
