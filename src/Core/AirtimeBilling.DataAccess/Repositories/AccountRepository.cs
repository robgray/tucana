﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class AccountRepository : IAccountRepository
    {
        public AirtimeBilling.Core.Entities.Account GetAccount(int accountId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var accounts = from acc in db.Accounts
                               where acc.AccountId == accountId
                               select acc;

                var account = accounts.SingleOrDefault();

                if (account != null)
                {
                    return account.CreateEntity();
                }                
                return null;                
            }
        }

        public AirtimeBilling.Core.Entities.Account GetAccountByAccountNumber(string accountNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var accounts = from acc in db.Accounts
                               where acc.AccountNumber.Equals(accountNumber)
                               select acc;

                var account = accounts.SingleOrDefault();

                if (account != null)
                {
                    return account.CreateEntity();
                }                
                return null;                
            }            
        }

        public IList<AirtimeBilling.Core.Entities.Account> GetAllAccounts()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Account> accounts = new List<AirtimeBilling.Core.Entities.Account>();
                foreach (Account account in db.Accounts)
                {
                    accounts.Add(account.CreateEntity());
                }
                return accounts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Account> FindAccountsByAccountNumber(string accountNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from acc in db.Accounts
                             where acc.AccountNumber.StartsWith(accountNumber)
                             select acc;

                IList<AirtimeBilling.Core.Entities.Account> accounts = new List<AirtimeBilling.Core.Entities.Account>();
                foreach (var account in search)
                {
                    accounts.Add(account.CreateEntity());
                }
                return accounts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Account> FindAccountsByContact(string contactName)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from acc in db.Accounts
                             where acc.Contact.Name.StartsWith(contactName) 
                             select acc;

                IList<AirtimeBilling.Core.Entities.Account> accounts = new List<AirtimeBilling.Core.Entities.Account>();
                foreach (var account in search)
                {
                    accounts.Add(account.CreateEntity());
                }
                return accounts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Account> FindAccountsByCopmpany(string companyName)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var search = from acc in db.Accounts
                             where acc.CompanyId != null && acc.Company.CompanyName.StartsWith(companyName) 
                             select acc;

                IList<AirtimeBilling.Core.Entities.Account> accounts = new List<AirtimeBilling.Core.Entities.Account>();
                foreach (var account in search)
                {
                    accounts.Add(account.CreateEntity());
                }
                return accounts;
            }
        }

        public bool UpdateAccount(AirtimeBilling.Core.Entities.Account account)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var acc = db.Accounts.Where(a => a.AccountId == account.Id.Value).SingleOrDefault();
                if (acc != null)
                {
                    acc.PopulateFromEntity(account);
                    
                    bool success = true;
                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (Exception ex)
                    {
                        // TODO Log Exception
                        LoggingUtility.LogException(ex);
                        success = false;
                    }

                    return success;
                }
                else
                {
                    throw new UnknownEntityException(string.Format("Could not find Account with Id={0} in the database", account.Id.Value));
                }                
            }
        }

        public void InsertAccount(AirtimeBilling.Core.Entities.Account account)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var acc = new Account();
                acc.PopulateFromEntity(account);
                
                db.Accounts.InsertOnSubmit(acc);
                db.SubmitChanges();
                account.Inserted(acc.AccountId);
            }
        }

        /// <summary>
        /// Get a list of all Root Accounts.          
        /// </summary>
        /// <returns>List of all Root Accounts</returns>
        public IList<AirtimeBilling.Core.Entities.Account> GetAllRootAccounts()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Account> accounts = new List<AirtimeBilling.Core.Entities.Account>();

                var query = db.Accounts.Where(a => a.IsInvoiceRoot);
                foreach(var account in query)
                {
                    accounts.Add(account.CreateEntity());                    
                }
                return accounts;
            }
        }

        /// <summary>
        /// Get all sub accounts of the suppplied master account
        /// </summary>
        /// <param name="masterAccountId">Master Account Id</param>
        /// <returns>List of all Accounts</returns>
        public IList<AirtimeBilling.Core.Entities.Account> GetAllSubAccounts(int rootAccountId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Account> accounts = new List<AirtimeBilling.Core.Entities.Account>();

                // (rag 09/02/2009) # 001-7    Exclude subaccounts that are invoice roots.
                var query = db.Accounts.Where(s => s.ParentAccountId == rootAccountId && !s.IsInvoiceRoot);
                foreach(var account in query)
                {
                    accounts.Add(account.CreateEntity());
                }
                return accounts;
            }
        }

        public AirtimeBilling.Core.Entities.Account this[int id]
        {
            get
            {
                return this.GetAccount(id);
            }
            set
            {
                if (id == value.Id.Value)
                {
                    this.UpdateAccount(value);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public void Delete(AirtimeBilling.Core.Entities.Account entity)
        {
             using (var db = DbFactory.GetDataContext()) {
                 var account = db.Accounts.FirstOrDefault(a => a.AccountId == entity.Id.Value);
                 if (account != null) {
                     db.Accounts.DeleteOnSubmit(account);
                     db.SubmitChanges();
                 }
             }
        }        
    }
}
