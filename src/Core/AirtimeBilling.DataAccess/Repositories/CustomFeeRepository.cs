﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;


namespace AirtimeBilling.DataAccess.Repositories
{
    public class CustomFeeRepository : ICustomFeeRepository
    {

        #region ICustomFeeRepository Members

        public AirtimeBilling.Core.Entities.CustomFee GetById(int id)
        {
            using (var db = DbFactory.GetDataContext()) {
                var fee = db.CustomFees.Where(f => f.CustomFeeId == id).SingleOrDefault();
                if (fee != null) {
                    return fee.CreateEntity();
                }
                return null;
            }
        }

        public IList<AirtimeBilling.Core.Entities.CustomFee> GetAll()
        {
            IList<AirtimeBilling.Core.Entities.CustomFee> fees = new List<AirtimeBilling.Core.Entities.CustomFee>();

            using (var db = DbFactory.GetDataContext())
            {                
                foreach (var fee in db.CustomFees)
                {
                    fees.Add(fee.CreateEntity());
                }
                return fees;
            }
        }

        public void Insert(AirtimeBilling.Core.Entities.CustomFee entity)
        {
            if (entity.Id.HasValue) {
                Update(entity);
                return;
            }

            using (var db = DbFactory.GetDataContext())
            {
                var fee = new CustomFee();
                fee.PopulateFromEntity(entity);
                

                db.CustomFees.InsertOnSubmit(fee);
                db.SubmitChanges();
                entity.Inserted(fee.CustomFeeId);
            }
        }

        public void Delete(AirtimeBilling.Core.Entities.CustomFee entity)
        {
            using (var db = DbFactory.GetDataContext()) {
                var fee = db.CustomFees.SingleOrDefault(f => f.CustomFeeId == entity.Id.Value);
                if (fee != null) {
                    db.CustomFees.DeleteOnSubmit(fee);
                    db.SubmitChanges();
                }
            }
        }

        public void Update(AirtimeBilling.Core.Entities.CustomFee entity)
        {
            // Can't update new contacts!
            if (entity.Id == null)
            {
                Insert(entity);
                return;
            }

            using (var db = DbFactory.GetDataContext())
            {
                var fee = db.CustomFees.SingleOrDefault(f => f.CustomFeeId == entity.Id.Value);
                if (fee != null)
                {
                    fee.PopulateFromEntity(entity);

                    db.SubmitChanges();
                }
                else {
                    throw new Exception("Could not find this Custom Fee in the database");
                }
            }
        }

        #endregion

        #region IRepository<CustomFee> Members

        public AirtimeBilling.Core.Entities.CustomFee this[int id]
        {
            get
            {
                return GetById(id);
            }
            set
            {
                if (value == null) throw new InvalidOperationException("Cannot set to NULL");
                if (!value.Id.HasValue) throw new InvalidOperationException("Use Insert method to create new objects");
                                
                Update(value);                
            }
        }

        #endregion
      
    }
}
