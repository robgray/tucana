﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class InvoiceRepository : IInvoiceRepository, IRepository<Invoice>
    {
        #region IInvoiceRepository Members
               
        public IList<Invoice> GetAllInvoices()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<Invoice> entities = new List<Invoice>();
                foreach(var invoice in db.InvoiceHeaders)
                {
                    entities.Add(invoice.GetInvoiceEntity());
                }
                return entities;
            }
        }

        public Invoice GetByInvoiceNumber(string invoiceNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var invoice = db.InvoiceHeaders.SingleOrDefault(h => h.InvoiceNumber == invoiceNumber);
                return invoice != null ? invoice.GetInvoiceEntity() : null;
            }
        }

        public IList<Invoice> FindInvoicesByRunNumber(int runNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<Invoice> entities = new List<Invoice>();
                var invoices = db.InvoiceHeaders.Where(h => h.InvoiceRunNumber == runNumber);
                foreach(var invoice in invoices)
                {
                    entities.Add(invoice.GetInvoiceEntity());
                }
                return entities;
            }
        }

        public IList<Invoice> FindInvoicesByInvoiceNumber(string pattern)
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<Invoice> entities = new List<Invoice>();
                var invoices = db.InvoiceHeaders.Where(h => h.InvoiceNumber.StartsWith(pattern));
                foreach (var invoice in invoices)
                {
                    entities.Add(invoice.GetInvoiceEntity());
                }
                return entities;
            }
        }

        public Invoice this[string invoiceNumber] 
        { 
            get
            {
                return GetByInvoiceNumber(invoiceNumber);
            }
        
        }

        /// <summary>
        /// Create the InvoiceHeader entry and as many InvoiceLineItem entries as required
        /// Also create an AccountInvoiceEntry for each account that was included on this invoice.
        /// Not sure why we need this :S
        /// </summary>
        /// <param name="entity"></param>
        public void InsertInvoice(Invoice entity)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var header = new InvoiceHeader();
                header.PopulateFromEntity(entity);

                foreach(var line in entity.Lines)
                {
                    var item = new InvoiceLineItem
                                   {                                        
                                       Description = line.Description,
                                       GstAmount = line.GstAmount,
                                       LineAmount = line.LineAmount,
                                       Quantity = line.Quantity,
                                       ContractId = line.ContractId 
                                   };
                    if (line.Call != null) {
                        item.CallId = line.Call.Id.Value;
                    }
                    header.InvoiceLineItems.Add(item);
                    
                    if (line.Call != null)
                    {
                        var callId = line.Call.Id.Value;
                        // Set call date invoiced.
                        var call = db.Calls.Where(c => c.CallId == callId).SingleOrDefault();
                        if (call != null)
                        {
                            call.DateInvoiced = entity.InvoiceDate;
                        }                        
                    }
                }

                foreach(var accountId in entity.AccountIds)
                {
                    var link = new AccountInvoice
                                   {
                                       AccountId = accountId,
                                       InvoiceHeader = header
                                   };
                    header.AccountInvoices.Add(link);
                }                

                db.InvoiceHeaders.InsertOnSubmit(header);
                db.SubmitChanges();
                entity.Inserted(header.InvoiceHeaderId);
            }
        }

        /// <summary>
        /// Will only update invoice header.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public bool UpdateInvoice(Invoice entity)
        {            
            using (var db = DbFactory.GetDataContext()) {

                var header = db.InvoiceHeaders.Single(i => i.InvoiceHeaderId == entity.Id.Value);
                header.PopulateFromEntity(entity);

                db.SubmitChanges();
                return true;
            }            
        }

        public IList<Invoice> GetAllInvoiceForAccount(AirtimeBilling.Core.Entities.Account entity)
        {
            var invoices = new List<Invoice>();

            if (!entity.Id.HasValue) return invoices;

            using (var db = DbFactory.GetDataContext()) {

                var account = db.Accounts.First(a => a.AccountId == entity.Id.Value);
                invoices.AddRange(account.AccountInvoices.Select(ai => ai.InvoiceHeader.GetInvoiceEntity()));

                return invoices;
            }
        }

        public bool DeleteInvoice(Invoice entity)
        {
            using (var db = DbFactory.GetDataContext()) {

                bool? success = false;                
                db.DeleteInvoice(entity.Id, ref success);

                if (success.HasValue) return success.Value;
                return false;

            }
        }
        
        #endregion

        #region IRepository<Invoice> Members

        public Invoice this[int id]
        {
            get
            {
                using (var db = DbFactory.GetDataContext())
                {
                    var invoice = db.InvoiceHeaders.Where(h => h.InvoiceHeaderId == id).SingleOrDefault();
                    return invoice != null ? invoice.GetInvoiceEntity() : null;
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion      
    }
}
