﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;

namespace AirtimeBilling.DataAccess.Repositories
{
    public class NoteRepository : INoteRepository
    {
        #region INoteRepository Members

        public IList<AirtimeBilling.Core.Entities.Note> GetNotesFor(EntityBase entity) 
        {
            using (var db = DbFactory.GetDataContext()) {

                var notes =
                    db.Notes.Where(n => n.Type == entity.GetEntityName() && n.EntityId == entity.Id)
                            .OrderByDescending(n => n.NoteDate);

                return notes.Select(note => note.CreateEntity()).ToList();
            }            
        }

        #endregion

        #region IRepository<Note> Members

        public AirtimeBilling.Core.Entities.Note this[int id]
        {
            get
            {
                using (var db = DbFactory.GetDataContext()) {
                    var entity = db.Notes.FirstOrDefault(n => n.NoteId == id);
                    if (entity != null)
                        return entity.CreateEntity();                    
                    return null;                    
                }
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool Insert(AirtimeBilling.Core.Entities.Note entity)
        {
            if (entity == null)
                throw new ArgumentNullException("No Note supplied.");
            
            if (entity.Id != null )
                throw new ArgumentNullException("Note Id is not null");

            try {
                using (var db = DbFactory.GetDataContext()) {
                    var note = new Note();

                    note.PopulateFromEntity(entity);
                    db.Notes.InsertOnSubmit(note);
                    db.SubmitChanges();

                    entity.Inserted(note.NoteId);
                    return true;
                }
            }
            catch (Exception ex) {
                LoggingUtility.LogException(ex);
                throw;
            }
        }
        
        #endregion

        
    }
}
