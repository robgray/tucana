﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirtimeBilling.DataAccess.Repositories
{
    public static class RepositoryFactory
    {
        // Dictionary to enforce the singleton pattern
        private static IDictionary<string, object> repositories = new Dictionary<string, object>();

        /// <summary>
        /// Gets or creates an instance of the requested interface.  Once a repository is created and initialized, it is cached, 
        /// and all future requests for the repository will come from the cache.
        /// </summary>
        /// <typeparam name="TRepository">The interface of the repository to create.</typeparam>        
        /// <returns>An instance of the interface requested.</returns>
        public static TRepository GetRepository<TRepository>()
            where TRepository : class           
        {
            // Initialize the provider's default value
            TRepository repository = default(TRepository);

            string interfaceShortName = typeof(TRepository).Name;
     
            if (!repositories.ContainsKey(interfaceShortName))
            {
                // Currently, there is only one type of repository and it's derived from the interface name
                // interface = IRepository. Concrete class = Repository.
                repository = Activator.CreateInstance(Type.GetType("AirtimeBilling.DataAccess.Repositories." + interfaceShortName.Substring(1))) as TRepository;
                repositories.Add(interfaceShortName, repository);
            }
            else
            {
                repository = (TRepository)repositories[interfaceShortName];
            }
            return repository;
        }
    }
}
