﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class ContractRepository : IContractRepository
    {
        #region IContractRepository Members

        public IList<AirtimeBilling.Core.Entities.Contract> GetAllContractsWithCustomFee(AirtimeBilling.Core.Entities.CustomFee customFee)
        {
            using (var db = DbFactory.GetDataContext()) {

                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();

                return contracts;
            }            
        }

        public IList<AirtimeBilling.Core.Entities.Contract> GetAllContracts()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();

                var openContracts = db.Contracts.Where(c => c.ContractStatus != (int)ContractStatus.Closed);
                foreach (var contract in openContracts)
                {
                    contracts.Add(contract.CreateEntity());
                } 

                return contracts;
            }
        }

        public IList<global::AirtimeBilling.Core.Entities.Call> GetNonInvoicedCallsByContractId(int contractId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<global::AirtimeBilling.Core.Entities.Call> calls = new List<global::AirtimeBilling.Core.Entities.Call>();

                var callsToInvoice = db.Calls.Where(c => c.DateInvoiced == null && c.ContractId == contractId).OrderBy(o => o.CallTime);
                foreach(var call in callsToInvoice)
                {                    
                    calls.Add(call.CreateEntity());
                }

                return calls;
            }
        }

        public bool UpdateCall(global::AirtimeBilling.Core.Entities.Call entity)
        {
            using (var db = DbFactory.GetDataContext()) {

                var call = db.Calls.SingleOrDefault(c => c.CallId == entity.Id.Value);
                if (call != null) {
                    call.PopulateFromEntity(entity);
                    db.SubmitChanges();
                }
                return true;
            }
        }


        public AirtimeBilling.Core.Entities.Contract GetContract(int contractId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var contract = db.Contracts.Where(c => c.ContractId == contractId).SingleOrDefault();
                return contract.CreateEntity();
            }
        }

        public bool UpdateContract(AirtimeBilling.Core.Entities.Contract contract)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var cont = db.Contracts.Where(c => c.ContractId == contract.Id.Value).SingleOrDefault();

                if (cont != null)
                {
                    cont.PopulateFromEntity(contract);                    
                    db.SubmitChanges();
                }

                return true;
            }
        }

        public void InsertContract(AirtimeBilling.Core.Entities.Contract contract)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var cont = new Contract();
                cont.PopulateFromEntity(contract);

                db.Contracts.InsertOnSubmit(cont);
                db.SubmitChanges();
                contract.Inserted(cont.ContractId);
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contract> GetContractsByAccountId(int accountId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();
                var conts = db.Contracts.Where(c => c.AccountId == accountId);

                foreach (var contract in conts)
                {
                    contracts.Add(contract.CreateEntity());
                }

                return contracts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contract> FindByContractNumber(string contractNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var conts = from c in db.Contracts
                            where c.ContractNumber.StartsWith(contractNumber)
                            orderby c.OrderReceivedDate descending 
                            select c;

                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();
                foreach (var contract in conts)
                {
                    contracts.Add(contract.CreateEntity());
                }

                return contracts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contract> FindByAccountNumber(string accountNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var conts = from c in db.Contracts
                            where c.Account.AccountNumber.StartsWith(accountNumber)
                            select c;

                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();
                foreach (var contract in conts)
                {
                    contracts.Add(contract.CreateEntity());
                }

                return contracts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contract> FindByPhoneNumber(string phoneNumber)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var conts = from c in db.Contracts
                            where c.PhoneNumber1.Equals(phoneNumber) ||
                                    c.PhoneNumber2.Equals(phoneNumber) ||
                                    c.PhoneNumber3.Equals(phoneNumber) ||
                                    c.PhoneNumber4.Equals(phoneNumber)
                            select c;

                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();
                foreach (var contract in conts)
                {
                    contracts.Add(contract.CreateEntity());
                }

                return contracts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contract> FindByContact(string contactName)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var conts = from c in db.Contracts
                            where c.Account.Contact.Name.StartsWith(contactName)
                            select c;

                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();
                foreach (var contract in conts)
                {
                    contracts.Add(contract.CreateEntity());
                }

                return contracts;
            }
        }

        public IList<AirtimeBilling.Core.Entities.Contract> FindByPlanId(int planId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var conts = from c in db.Contracts
                            where c.PlanId == planId
                            select c;

                IList<AirtimeBilling.Core.Entities.Contract> contracts = new List<AirtimeBilling.Core.Entities.Contract>();
                foreach (var contract in conts)
                {
                    contracts.Add(contract.CreateEntity());
                }

                return contracts;
            }
        }

        public int InsertContractPlanVariance(AirtimeBilling.Core.Entities.Contract contract, AirtimeBilling.Core.Entities.Plan newPlan, DateTime startDate, DateTime endDate, 
            string user, decimal cost)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var variance = new ContractPlanVariance
                {
                    CreatedOn = DateTime.Now,
                    Username = user,
                    StartDate = startDate,
                    EndDate = endDate,
                    Cost = cost,
                    PlanId = newPlan.Id.Value,
                    ContractId = contract.Id.Value
                };

                db.ContractPlanVariances.InsertOnSubmit(variance);
                db.SubmitChanges();
                return variance.ContractPlanVarianceId;
            }
        }

        public IList<AirtimeBilling.Core.Entities.CustomFee> GetAllCustomFeesForContract(AirtimeBilling.Core.Entities.Contract contract)
        {
            IList<AirtimeBilling.Core.Entities.CustomFee> customFees = new List<AirtimeBilling.Core.Entities.CustomFee>();

            using (var db = DbFactory.GetDataContext()) {
                var fees = from cont in db.Contracts                           
                           where cont.ContractId == contract.Id.Value
                           select cont.ContractCustomFee;

                foreach (var fee in fees) {
                    var item = new AirtimeBilling.Core.Entities.CustomFee(fee.ContractCustomFeeId)
                                   {
                                       Description = fee.CustomFee.Description,
                                       Amount = fee.CustomFee.Amount,
                                       IsRecurring = fee.CustomFee.IsRecurring
                                   };
                    customFees.Add(item);
                }
            }

            // TODO 
            return customFees;
        }

        public bool AddCustomFee(int contractId, AirtimeBilling.Core.Entities.CustomFee customFee)
        {
            using (var db = DbFactory.GetDataContext()) {

                if (db.ContractCustomFees.SingleOrDefault(f => f.ContractId == contractId && f.CustomFeeId == customFee.Id.Value) != null)
                    return true;

                var contractCustomFee = new ContractCustomFee
                                            {
                                                ContractId = contractId,
                                                CustomFeeId = customFee.Id.Value
                                            };

                db.ContractCustomFees.InsertOnSubmit(contractCustomFee);
                db.SubmitChanges();

                return true;
            }
        }

        public bool RemoveCustomFee(int contractCustomFeeId)
        {
            using (var db = DbFactory.GetDataContext()) {

                var contractCustomFee = db.ContractCustomFees.SingleOrDefault(f => f.ContractCustomFeeId == contractCustomFeeId);
                if (contractCustomFee == null)
                    return true;

                db.ContractCustomFees.DeleteOnSubmit(contractCustomFee);
                db.SubmitChanges();

                return true;
            }
        }
        
        #endregion

        #region IRepository<Contract> Members

        public AirtimeBilling.Core.Entities.Contract this[int id]
        {
            get
            {
                return GetContract(id);
            }
            set
            {
                if (id == value.Id.Value)
                {
                    UpdateContract(value);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }
        }

        #endregion

        #region IContractRepository Members


        public bool Delete(AirtimeBilling.Core.Entities.Contract entity)
        {
            if (entity == null) return false;

            using (var db = DbFactory.GetDataContext()) {

                bool? result = false;
                
                db.DeleteContract(entity.Id, ref result); {}
                
                if (result.HasValue)
                    return result.Value;
                else
                    return false;
            }
        }

        #endregion
    }
}
