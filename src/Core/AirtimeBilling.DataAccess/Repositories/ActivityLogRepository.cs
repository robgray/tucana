﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using System.Data.SqlClient;
using System.Data;

namespace AirtimeBilling.DataAccess.Repositories
{
    public class ActivityLogRepository : IActivityLogRepository
    {

        #region IActivityLogRepository Members

        public void Insert(Activity entity, string tableName, int keyId)
        {
            try
            {
                using (var db = DbFactory.GetDataContext())
                {
                    var log = new ActivityLog
                        {
                            LogDate = entity.LogDate,
                            KeyId = keyId,
                            Activity = entity.ActivityName,
                            TableName = tableName,
                            User = entity.User
                        };

                    db.ActivityLogs.InsertOnSubmit(log);
                    db.SubmitChanges();
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
            }
        }

        public IList<Activity> FindActivitiesByUser(string username)
        {
            if (string.IsNullOrEmpty(username)) throw new Exception("A username must be supplied");

            return FindActivitiesByWhereClause(string.Format("[User] = '{0}'", username));
        }

        public IList<Activity> FindActivitiesByEntity(EntityBase entity)
        {
            if (entity != null)
            {
                if (string.IsNullOrEmpty(entity.GetEntityName())) throw new Exception("A table name must be supplied");
                if (entity.Id == null) throw new Exception("Cannot get Activity Log of unsaved entities");

                return FindActivitiesByWhereClause(string.Format("TableName = '{0}' AND KeyId = {1}", entity.GetEntityName(), entity.Id.Value));
            }
            return new List<Activity>();
        }

        #endregion

        private IList<Activity> FindActivitiesByWhereClause(string whereClause)
        {
            if (string.IsNullOrEmpty(whereClause)) throw new Exception("Must supply selection criteria for activity log");

            string sql = "SELECT ActivityLogId, LogDate, [User], Activity " +
                         "FROM ActivityLog " +
                         "WHERE " + whereClause + " " +
                         "ORDER BY LogDate DESC";

            var logs = new List<Activity>();
            using(var cn = new SqlConnection(ConfigItems.ConnectionString))
            {
                try
                {
                    var cmd = cn.CreateCommand();
                    cmd.CommandText = sql;
                    cmd.CommandType = CommandType.Text;
                    cn.Open();
                    using (var reader = cmd.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                logs.Add(CreateEntityFromReader(reader));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingUtility.LogException(ex);
                    throw ex;
                }
                finally
                {
                    if (cn.State == ConnectionState.Open) cn.Close();
                }
            }

            return logs;
        }

        private Activity CreateEntityFromReader(SqlDataReader reader)
        {
            var entity = new Activity(reader.GetInt32(0), reader.GetDateTime(1), reader.GetString(2), reader.GetString(3));            

            return entity;
        }
    }
}
