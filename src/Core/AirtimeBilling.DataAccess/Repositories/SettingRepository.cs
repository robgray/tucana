﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class SettingRepository : ISettingRepository
    {
        #region ISettingRepository Members

        public Setting GetByName(string name)
        {
            using (var db = DbFactory.GetDataContext()) {
                var setting = db.SystemSettings.SingleOrDefault(s => s.KeyName == name);
                if (setting == null) return null;

                return PopulateEntity(setting);                
            }
        }

        public IList<Setting> GetAll()
        {
            using (var db = DbFactory.GetDataContext()) {
                IList<Setting> settings = new List<Setting>();
                foreach (var setting in db.SystemSettings) {
                    settings.Add(PopulateEntity(setting));
                }

                return settings;
            }
        }

        public bool Commit(Setting entity)
        {
            using (var db = DbFactory.GetDataContext()) {
                var setting = new SystemSetting();
                if (entity.Id != null) {
                    setting = db.SystemSettings.First(x => x.SystemSettingId == entity.Id.Value);
                    if (setting == null) 
                        throw new UnknownEntityException("Could not find System Setting with Id " + entity.Id.Value);                    
                }

                CopySettingEntityToSystemSetting(entity, setting);
                if (entity.Id == null)
                    db.SystemSettings.InsertOnSubmit(setting);
                
                db.SubmitChanges();
                entity.Inserted(setting.SystemSettingId);

                return true;
            }
        }
        

        #endregion

        #region IRepository<Setting> Members

        public Setting this[int id]
        {
            get
            {
                using (var db = DbFactory.GetDataContext()) {
                    var setting = db.SystemSettings.FirstOrDefault(s => s.SystemSettingId == id);
                    return setting == null ? null : PopulateEntity(setting);
                }
            }
            set
            {
                Commit(value);
            }
        }

        public Setting this[string name]
        {
            get
            {
                return GetByName(name);
            }
            set
            {
                Commit(value);
            }
        }

        public IList<Setting> GetAllSystemSettings()
        {
            using (var db = DbFactory.GetDataContext()) {
                IList<Setting> settings = new List<Setting>();                
                foreach (var setting in db.SystemSettings.Where(s => s.IsSystem)) {
                    settings.Add(PopulateEntity(setting));
                }

                return settings;
            }
        }

        public IList<Setting> GetAllUserSettings()
        {
            using (var db = DbFactory.GetDataContext())
            {
                IList<Setting> settings = new List<Setting>();
                foreach (var setting in db.SystemSettings.Where(s => !s.IsSystem))
                {
                    settings.Add(PopulateEntity(setting));
                }

                return settings;
            }
        }

        #endregion

        #region Helpers

         private void CopySettingEntityToSystemSetting(Setting setting, SystemSetting set)
         {
             set.KeyName = setting.Name;
             set.KeyValue = setting.Value;
             set.IsSystem = setting.IsSystem;
         }

        private Setting PopulateEntity(SystemSetting set)
        {
            return new Setting(set.SystemSettingId)
                       {
                           Name = set.KeyName,
                           Value = set.KeyValue,
                           IsSystem = set.IsSystem
                       };
        }

        #endregion 
    }
}
