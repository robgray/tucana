﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Linq;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class PlanTariffRepository : IPlanTariffRepository
    {

        #region IPlanTariffRepository Members

        public PlanTariff GetPlanTariff(int planTariffId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tariff = (from t in db.Tariffs
                             where t.TariffId == planTariffId
                             select new
                             {
                                 t.TariffId, 
                                 t.NetworkTariff.Code,
                                 Name = t.NetworkTariff.Tariff,
                                 t.UnitCost,
                                 t.HasFlagfall,
                                 t.IsCountedInFreeCall,
                                 t.NetworkTariffId,
                                 t.PlanId
                             }).SingleOrDefault();

                if (tariff != null)
                {
                    var planTariff = new PlanTariff(tariff.TariffId, tariff.NetworkTariffId)
                    {                        
                        Code = tariff.Code,
                        Name = tariff.Name,
                        UnitCost = tariff.UnitCost.Value,
                        IsCountedInFreeCall = tariff.IsCountedInFreeCall,                        
                        HasFlagfall = tariff.HasFlagfall,
                        PlanId = tariff.PlanId
                    };

                    return planTariff;
                }                
                return null;                
            }
        }

        public IList<PlanTariff> GetPlanTariffsByPlan(int planId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var taffs = from t in db.Tariffs
                              where t.PlanId == planId
                              select new
                              {
                                  t.TariffId,
                                  t.NetworkTariff.Code,
                                  Name = t.NetworkTariff.Tariff,
                                  t.UnitCost,
                                  t.IsCountedInFreeCall,
                                  t.HasFlagfall,
                                  t.NetworkTariffId,
                                  t.PlanId
                              };

                IList<PlanTariff> tariffs = new List<PlanTariff>();
                foreach(var tariff in taffs)
                {
                    var planTariff = new PlanTariff(tariff.TariffId, tariff.NetworkTariffId)
                    {                        
                        Code = tariff.Code,
                        Name = tariff.Name,
                        UnitCost = tariff.UnitCost.Value,
                        IsCountedInFreeCall = tariff.IsCountedInFreeCall,
                        HasFlagfall = tariff.HasFlagfall,
                        PlanId = tariff.PlanId
                    };

                    tariffs.Add(planTariff);
                }

                return tariffs;
            }
        }

        public void InsertPlanTariff(PlanTariff entity)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tariff = new Tariff()
                {
                    PlanId = entity.PlanId,
                    IsCountedInFreeCall = entity.IsCountedInFreeCall,
                    HasFlagfall = entity.HasFlagfall,
                    NetworkTariffId = entity.NetworkTariffId,
                    UnitCost = entity.UnitCost
                };

                db.Tariffs.InsertOnSubmit(tariff);
                db.SubmitChanges();
                entity.Inserted(tariff.TariffId);
            }
        }

        public bool UpdatePlanTariff(PlanTariff entity)
        {
            if (entity.Id == null) return false;

            using (var db = DbFactory.GetDataContext())
            {
                var tariff = db.Tariffs.Where(t => t.TariffId == entity.Id.Value).SingleOrDefault();
                if (tariff != null)
                {
                    tariff.UnitCost = entity.UnitCost;
                    tariff.IsCountedInFreeCall = entity.IsCountedInFreeCall;
                    tariff.HasFlagfall = entity.HasFlagfall;

                    try
                    {
                        db.SubmitChanges();
                    }
                    catch (ChangeConflictException)
                    {
                        db.Refresh(RefreshMode.OverwriteCurrentValues);
                        db.SubmitChanges();
                    }

                    return true;
                }
                else
                {
                    throw new Exception(string.Format("Plan Tariff with Id={0} was not found in the database", entity.Id.Value));
                }
            }
        }

        public bool DeletePlanTariffsByNetworkTariff(int networkTariffId)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var tariffsToDelete = from t in db.Tariffs
                                      where t.NetworkTariffId == networkTariffId
                                      select t;
                
                db.Tariffs.DeleteAllOnSubmit(tariffsToDelete);
                db.SubmitChanges();

                return true;                
            }
        }

        #endregion

        #region IRepository<PlanTariff> Members

        public PlanTariff this[int id]
        {
            get
            {
                return this.GetPlanTariff(id);
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        #endregion

    }
}
