﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal class UserRepository : IUserRepository
    {
        #region IUserRepository Members

        public AirtimeBilling.Core.Entities.User GetUser(string username, string password)
        {
            using (var db = DbFactory.GetDataContext())
            {
                var user = (from u in db.Users
                            where u.Username == username && u.Password == password
                            select u).SingleOrDefault();

                return PopulateUserEntityFromUser(user);
            }
        }

        private AirtimeBilling.Core.Entities.User PopulateUserEntityFromUser(User user)
        {
            AirtimeBilling.Core.Entities.User entity = null;
            if (user != null)
            {
                if (user.AgentId != null)
                {
                    AgentUser aentity = new AirtimeBilling.Core.Entities.AgentUser()
                    {
                        Username = user.Username,
                        AgentId = user.AgentId.Value,
                        IsActive = user.IsActive,
                        Name = user.Name,
                        IsAuthenticated = true
                    };

                    entity = aentity;
                }
                else if (user.ContactId != null)
                {
                    AirtimeBilling.Core.Entities.ContactUser centity = new AirtimeBilling.Core.Entities.ContactUser()
                    {
                        Username = user.Username,
                        ContactId = user.ContactId.Value,
                        IsActive = user.IsActive,
                        Name = user.Name,
                        IsAuthenticated = true
                    };

                    entity = centity;
                }
                else
                {
                    entity = new AirtimeBilling.Core.Entities.User()
                    {
                        Username = user.Username,
                        Name = user.Name,
                        IsActive = user.IsActive,
                        IsAuthenticated = true
                    };
                }                
            }
            return entity;
        }

        #endregion
    }
}
