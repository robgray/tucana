﻿using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class LogDataMapper
    {
        public static LogEntry CreateEntity(this Log log)
        {
            return new LogEntry(log.LogId)
                       {
                           Username = log.Username,
                           ClassName = log.Source,
                           Message = log.Message,
                           StackTrace = log.StackTrace,
                           MethodName = log.MethodName,
                           Timestamp = log.Timestamp,
                           Level = (LoggingLevels) log.LoggingLevel
                       };
        }
    }
}
