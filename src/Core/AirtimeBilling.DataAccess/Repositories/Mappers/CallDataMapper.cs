﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class CallDataMapper
    {
        public static global::AirtimeBilling.Core.Entities.Call CreateEntity(this Call call)
        {
            if (call == null) return null;
            
            var entity = new global::AirtimeBilling.Core.Entities.Call(call.CallId)
            {                
                CalledFrom = call.Location,
                ContractId = call.ContractId,
                Tariff = call.Tariff.NetworkTariff.Tariff,
                CallTime = call.CallTime,
                Volume = call.CostedVolume,      
                ActualVolume = call.Volume,
                NumberCalled = call.CalledNumber,
                PhoneNumber = call.ServiceNumber,
                UnitsOfTime = call.UnitsOfTime == null ? 0 : call.UnitsOfTime.Value,
                UnitCost = call.Tariff.UnitCost == null ? 0 : call.Tariff.UnitCost.Value,
                Cost = call.Cost,
                HasFreeCallTariff = call.Tariff.IsCountedInFreeCall,
                ImportedCallType = call.ImportedCallType,
                CallType = (CallVolumeUnit)call.VolumeUnit,
            };
            
            entity.Cost = call.Cost;

            return entity;
        }

        public static void PopulateFromEntity(this Call call, global::AirtimeBilling.Core.Entities.Call entity)
        {            
            // only allow units of time to be updated.
            call.UnitsOfTime = entity.UnitsOfTime;                                                
        }
    }
}
