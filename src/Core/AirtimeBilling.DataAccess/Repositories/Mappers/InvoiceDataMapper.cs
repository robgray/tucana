﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class InvoiceDataMapper
    {
        public static Invoice GetInvoiceEntity(this InvoiceHeader header)
        {
            var address = new Address
            {
                Street = header.Street,
                Street2 = header.Street2,
                Suburb = header.Suburb,
                State = header.State,
                Postcode = header.Postcode
            };

            Invoice entity;
            
            if (header.IsInternational.HasValue && header.IsInternational.Value) {
                entity = new NoGstInvoice(header.InvoiceHeaderId);
            } else {
                entity = new Invoice(header.InvoiceHeaderId);
            }
                        
            entity.InvoiceNumber = header.InvoiceNumber;
            entity.InvoiceDate = header.InvoiceDate;
            entity.InvoiceAddress = address;
            entity.FromDate = header.FromDate;
            entity.ToDate = header.ToDate;
            entity.InvoiceRunNumber = header.InvoiceRunNumber;
            entity.To = header.To;
            entity.PreviousBill = header.PreviousBill;
            entity.AmountPaid = header.AmountPaid;
            entity.Outstanding = header.Outstanding;
            entity.NewCharges = header.NewCharges;
            entity.CurrentBill = header.CurrentBill;
            entity.TotalIncGst = header.TotalIncGst;
            entity.TotalGst = header.TotalGst;
            entity.TotalExGst = header.TotalExGst;
            entity.HasFailedPayment = header.HasFailedPayment;
            
            if (header.PaymentDue.HasValue)
                entity.PaymentDue = header.PaymentDue.Value;

            foreach (var item in header.InvoiceLineItems)
            {
                var call = item.Call.CreateEntity();

                var line = new InvoiceLine(item.InvoiceLineItemId)
                {
                    Call = call,
                    Description = item.Description,
                    LineAmount = item.LineAmount,
                    GstAmount = item.GstAmount,
                    Invoice = entity,
                    ContractId = item.ContractId
                };

                entity.AddLine(line);
            }

            foreach (var account in header.AccountInvoices)
            {
                entity.AddAccount(account.AccountId);
                if (account.Account.IsInvoiceRoot)
                {
                    entity.InvoiceRootAccountId = account.AccountId;
                }
            }

            using (var db = DbFactory.GetDataContext())
            {
                var item = (from h in db.InvoiceHeaders
                            join ai in db.AccountInvoices on h.InvoiceHeaderId equals ai.InvoiceHeaderId
                            where h.InvoiceNumber.Equals(header.InvoiceNumber) && ai.Account.IsInvoiceRoot
                            select new { ai.Account.Contact.Email }).FirstOrDefault();

                if (item != null)
                {
                    entity.Email = item.Email;
                }
            }

            return entity;
        }

        public static void PopulateFromEntity(this InvoiceHeader header, Invoice entity)
        {
            
            header.InvoiceNumber = entity.InvoiceNumber;
            header.InvoiceDate = entity.InvoiceDate;
            header.FromDate = entity.FromDate;
            header.ToDate = entity.ToDate;
            header.InvoiceRunNumber = entity.InvoiceRunNumber;
            header.To = entity.To;
            header.Street = entity.InvoiceAddress.Street;
            header.Street2 = entity.InvoiceAddress.Street2;
            header.Suburb = entity.InvoiceAddress.Suburb;
            header.State = entity.InvoiceAddress.State;
            header.Postcode = entity.InvoiceAddress.Postcode;
            header.PreviousBill = entity.PreviousBill;
            header.AmountPaid = entity.AmountPaid;
            header.NewCharges = entity.NewCharges;
            header.TotalIncGst = entity.TotalIncGst;
            header.TotalExGst = entity.TotalExGst;
            header.TotalGst = entity.TotalGst;
            header.Outstanding = entity.Outstanding;
            header.PaymentDue = entity.PaymentDue;
            header.CurrentBill = entity.CurrentBill;
            header.IsInternational = entity is NoGstInvoice ? true : false;
            header.HasFailedPayment = entity.HasFailedPayment;
        }
    }
}
