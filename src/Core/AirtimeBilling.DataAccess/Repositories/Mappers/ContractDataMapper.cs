﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class ContractMapper
    {
        public static void PopulateFromEntity(this Contract contract, AirtimeBilling.Core.Entities.Contract entity)
        {
            contract.PlanId = entity.PlanId;
            contract.AgentId = entity.AgentId;
            contract.ContractNumber = entity.ContractNumber;
            contract.ActivationDate = entity.ActivationDate;
            contract.EndDate = entity.EndDate;
            contract.IMEINumber = entity.IMEINumber;
            contract.SimCard = entity.SimCard;
            contract.PhoneUsedBy = entity.UsedBy;
            contract.PhoneNumber1 = entity.PhoneNumber1;
            contract.PhoneNumber2 = entity.PhoneNumber2;
            contract.PhoneNumber3 = entity.PhoneNumber3;
            contract.PhoneNumber4 = entity.PhoneNumber4;
            contract.Data = entity.Data;
            contract.MessageBank = entity.MessageBank;
            contract.AccountId = entity.AccountId;
            contract.ContractStatus = (int)entity.ContractStatus;
            contract.Puk = entity.Puk;
            contract.Pin = entity.Pin;
            contract.StatusChangedDate = entity.StatusChangedDate;
            contract.SuspensionStartDate = entity.SuspensionStartDate;
            contract.SuspendedUntilDate = entity.SuspendedUntilDate;
            contract.OrderReceivedDate = entity.OrderReceivedDate;
        }

        public static AirtimeBilling.Core.Entities.Contract CreateEntity(this Contract contract)
        {
            AirtimeBilling.Core.Entities.Contract entity = null;
            if (contract != null)
            {
                entity = new AirtimeBilling.Core.Entities.Contract(contract.ContractId, contract.OrderReceivedDate)
                {
                    PlanId = contract.PlanId,
                    AgentId = contract.AgentId,
                    ContractNumber = contract.ContractNumber,
                    ActivationDate = contract.ActivationDate,
                    EndDate = contract.EndDate,
                    IMEINumber = contract.IMEINumber,
                    SimCard = contract.SimCard,
                    UsedBy = contract.PhoneUsedBy,
                    PhoneNumber1 = contract.PhoneNumber1,
                    PhoneNumber2 = contract.PhoneNumber2,
                    PhoneNumber3 = contract.PhoneNumber3,
                    PhoneNumber4 = contract.PhoneNumber4,
                    Data = contract.Data,
                    MessageBank = contract.MessageBank,
                    AccountId = contract.AccountId,
                    ContractStatus =
                        (ContractStatus)
                        Enum.Parse(typeof(ContractStatus), contract.ContractStatus.ToString()),
                    Pin = contract.Pin,
                    Puk = contract.Puk,
                    StatusChangedDate = contract.StatusChangedDate,
                    SuspensionStartDate = contract.SuspensionStartDate,
                    SuspendedUntilDate = contract.SuspendedUntilDate,                                                           
                };
            }

            return entity;
        }
    }
}
