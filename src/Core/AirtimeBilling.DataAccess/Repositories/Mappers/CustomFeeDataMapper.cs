﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class CustomFeeDataMapper
    {
        public static AirtimeBilling.Core.Entities.CustomFee CreateEntity(this CustomFee fee)
        {
            return new AirtimeBilling.Core.Entities.CustomFee(fee.CustomFeeId)
            {
                Description = fee.Description,
                Amount = fee.Amount,
                IsRecurring = fee.IsRecurring
            };
        }

        public static void PopulateFromEntity(this CustomFee fee, AirtimeBilling.Core.Entities.CustomFee entity)
        {
            fee.Description = entity.Description;
            fee.Amount = entity.Amount;
            fee.IsRecurring = entity.IsRecurring;
        }
    }
}
