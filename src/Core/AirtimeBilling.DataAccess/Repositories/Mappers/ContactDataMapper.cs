﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class ContactDataMapper
    {
        public static void PopulateFromEntity(this Contact contact, AirtimeBilling.Core.Entities.Contact entity)
        {
            contact.Name = entity.Name;
            contact.MobilePhone = entity.MobilePhone;
            contact.WorkPhone = entity.WorkPhone;
            contact.FaxNumber = entity.FaxNumber;
            contact.Email = entity.Email;
            contact.CreditCardType = (int)entity.CreditCard.CreditCardType;
            contact.CreditCardNumber = entity.CreditCard.CardNumber;
            contact.CreditCardExpiry = entity.CreditCard.ExpiryDate;
            contact.CreditCardName = entity.CreditCard.NameOnCard;
            contact.DriversLicenseDOB = entity.DriversLicense.DateOfBirth;
            contact.DriversLicenseExpiry = entity.DriversLicense.Expiry;
            contact.DriversLicenseNumber = entity.DriversLicense.LicenseNumber;
            contact.Street = entity.HomeAddress.Street;
            contact.Street2 = entity.HomeAddress.Street2;
            contact.Suburb = entity.HomeAddress.Suburb;
            contact.State = entity.HomeAddress.State;
            contact.Postcode = entity.HomeAddress.Postcode;
            contact.PostalStreet = entity.PostalAddress.Street;
            contact.PostalStreet2 = entity.PostalAddress.Street2;
            contact.PostalSuburb = entity.PostalAddress.Suburb;
            contact.PostalState = entity.PostalAddress.State;
            contact.PostalPostcode = entity.PostalAddress.Postcode;
        }

        public static AirtimeBilling.Core.Entities.Contact CreateEntity(this Contact contact)
        {
            AirtimeBilling.Core.Entities.Contact entity = null;

            if (contact != null)
            {
                var license = new DriversLicense
                {
                    LicenseNumber = contact.DriversLicenseNumber,
                    DateOfBirth = contact.DriversLicenseDOB.Value,
                    Expiry = contact.DriversLicenseExpiry.Value
                };

                var creditCard = new CreditCard
                {
                    CardNumber = contact.CreditCardNumber,
                    NameOnCard = contact.CreditCardName,
                    ExpiryDate = contact.CreditCardExpiry,
                    CreditCardType =
                        (CreditCardType)
                        Enum.Parse(typeof(CreditCardType), contact.CreditCardType.ToString())
                };

                var homeAddress = new Address
                {
                    Street = contact.Street,
                    Street2 = contact.Street2,
                    Suburb = contact.Suburb,
                    State = contact.State,
                    Postcode = contact.Postcode
                };
                var postalAddress = new Address
                {
                    Street = contact.PostalStreet,
                    Street2 = contact.PostalStreet2,
                    Suburb = contact.PostalSuburb,
                    State = contact.PostalState,
                    Postcode = contact.PostalPostcode
                };


                entity = new AirtimeBilling.Core.Entities.Contact(contact.ContactId)
                {
                    Name = contact.Name,
                    WorkPhone = contact.WorkPhone,
                    FaxNumber = contact.FaxNumber,
                    MobilePhone = contact.MobilePhone,
                    Email = contact.Email,
                    DriversLicense = license,
                    HomeAddress = homeAddress,
                    PostalAddress = postalAddress,
                    CreditCard = creditCard
                };
            }

            return entity;
        }
    }
}
