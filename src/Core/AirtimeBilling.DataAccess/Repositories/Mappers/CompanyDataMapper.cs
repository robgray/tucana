﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class CompanyDataMapper
    {
        public static void PopulateFromEntity(this Company company, AirtimeBilling.Core.Entities.Company entity)
        {
            company.CompanyName = entity.CompanyName;
            company.ABN = entity.ABN;
            company.ACN = entity.ACN;
            company.Street = entity.Address.Street;
            company.Street2 = entity.Address.Street2;
            company.Suburb = entity.Address.Suburb;
            company.State = entity.Address.State;
            company.Postcode = entity.Address.Postcode;
            company.PostalStreet = entity.PostalAddress.Street;
            company.PostalStreet2 = entity.PostalAddress.Street2;
            company.PostalSuburb = entity.PostalAddress.Suburb;
            company.PostalState = entity.PostalAddress.State;
            company.PostalPostcode = entity.PostalAddress.Postcode;
        }

        public static AirtimeBilling.Core.Entities.Company CreateEntity(this Company company)
        {
            AirtimeBilling.Core.Entities.Company entity = null;
            if (company != null)
            {
                var add = new Address
                {
                    Street = company.Street,
                    Street2 = company.Street2,
                    Suburb = company.Suburb,
                    State = company.State,
                    Postcode = company.Postcode
                };

                var postal = new Address
                {
                    Street = company.PostalStreet,
                    Street2 = company.PostalStreet2,
                    Suburb = company.PostalSuburb,
                    State = company.PostalState,
                    Postcode = company.PostalPostcode
                };

                entity = new AirtimeBilling.Core.Entities.Company(company.CompanyId)
                {
                    CompanyName = company.CompanyName,
                    ACN = company.ACN,
                    ABN = company.ABN,
                    Address = add,
                    PostalAddress = postal
                };
            }

            return entity;
        }
    }
}
