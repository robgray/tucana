﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class AccountDataMapper
    {
        public static void PopulateFromEntity(this Account acc, AirtimeBilling.Core.Entities.Account entity)
        {
            acc.AccountNumber = entity.AccountNumber;
            acc.AccountName = entity.AccountName;
            acc.ContactId = entity.ContactId;            
            acc.ContactRole = entity.ContactRole;
            acc.BillingMethod = (int)entity.BillingMethod;
            acc.EmailBillDataFile = entity.EmailBillDataFile;
            acc.EmailBill = entity.EmailBill;
            acc.PostBill = entity.PostBill;
            acc.BillingAddressType = (int)entity.BillingAddressType;
            acc.Password = entity.Password;            
            acc.IsInvoiceRoot = entity.IsInvoiceRoot;
            acc.HasRequestedInvoicing = entity.HasRequestedInvoicing;
            acc.IsInternational = entity.IsInternational;

            if (entity is SubAccount) {
                acc.ParentAccountId = ((SubAccount)entity).MasterAccountId;
            }
            else if (entity is MasterAccount) {
                acc.CompanyId = ((MasterAccount)entity).CompanyId;
                acc.AmountPaid = ((MasterAccount)entity).AmountPaid;
                acc.PreviousBill = ((MasterAccount)entity).PreviousBill;
            }
        }

        public static AirtimeBilling.Core.Entities.Account CreateEntity(this Account account)
        {
            if (account.ParentAccountId == null)
            {
                var entity = new MasterAccount(account.AccountId)
                {

                    AccountNumber = account.AccountNumber,
                    AccountName = account.AccountName,
                    ContactId = account.ContactId,
                    ContactRole = account.ContactRole,
                    BillingMethod =
                        (BillingMethod)
                        Enum.Parse(typeof(BillingMethod), account.BillingMethod.ToString()),
                    EmailBillDataFile = account.EmailBillDataFile,
                    EmailBill = account.EmailBill,
                    PostBill = account.PostBill,
                    Password = account.Password,
                    BillingAddressType =
                        (BillingAddressType)
                        Enum.Parse(typeof(BillingAddressType), account.BillingAddressType.ToString()),
                    IsInvoiceRoot = account.IsInvoiceRoot,
                    CompanyId = account.CompanyId,
                    HasRequestedInvoicing = account.HasRequestedInvoicing,
                    IsInternational = account.IsInternational,
                    AmountPaid = account.AmountPaid,
                    PreviousBill = account.PreviousBill
                };

                return entity;
            }
            else
            {
                var entity = new AirtimeBilling.Core.Entities.SubAccount(account.AccountId)
                {
                    AccountNumber = account.AccountNumber,
                    AccountName = account.AccountName,
                    ContactId = account.ContactId,
                    ContactRole = account.ContactRole,
                    BillingMethod =
                        (BillingMethod)
                        Enum.Parse(typeof(BillingMethod),
                                   account.BillingMethod.ToString()),
                    EmailBillDataFile = account.EmailBillDataFile,
                    EmailBill = account.EmailBill,
                    PostBill = account.PostBill,
                    Password = account.Password,
                    BillingAddressType =
                        (BillingAddressType)
                        Enum.Parse(typeof(BillingAddressType),
                                   account.BillingAddressType.ToString()),
                    IsInvoiceRoot = account.IsInvoiceRoot,
                    MasterAccountId = account.ParentAccountId.Value,
                    HasRequestedInvoicing = account.HasRequestedInvoicing,
                    IsInternational = account.IsInternational,
                    AmountPaid = account.AmountPaid,
                    PreviousBill = account.PreviousBill
                };

                return entity;
            }

        }
    }
}
