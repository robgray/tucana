﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class EwayProcessedDataDataMapper
    {
        public static AirtimeBilling.Core.Entities.EwayProcessedData CreateEntity(this EwayProcessedData data)
        {
            return new AirtimeBilling.Core.Entities.EwayProcessedData(data.EwayProcessedDataId)
                       {
                           EwayTransactionReference = data.EwayTransactionReference,
                           EwayInvoiceReference = data.EwayInvoiceReference,
                           Amount = data.Amount,
                           CardHolder = data.CardHolder,
                           InvoiceNumber = data.InvoiceNumber,
                           ResponseCode = data.ResponseCode,
                           ResponseText = data.ResponseText
                       };
        }

        public static void PopulateFromEntity(this EwayProcessedData data, AirtimeBilling.Core.Entities.EwayProcessedData entity)
        {
            data.EwayTransactionReference = entity.EwayTransactionReference;
            data.CardHolder = entity.CardHolder;
            data.Amount = entity.Amount;
            data.EwayInvoiceReference = entity.EwayInvoiceReference;
            data.InvoiceNumber = entity.InvoiceNumber;
            data.ResponseCode = entity.ResponseCode;
            data.ResponseText = entity.ResponseText;            
        }
    }
}
