﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.DataAccess.Repositories
{
    internal static class NoteDataMapper
    {
        public static AirtimeBilling.Core.Entities.Note CreateEntity(this Note note)
        {
            if (note == null) return null;

            var entity = new AirtimeBilling.Core.Entities.Note(note.NoteId)
                             {
                                 Comment = note.Comment,                                 
                                 NoteType = note.Type,
                                 EntityId = note.EntityId,
                                 NoteDate = note.NoteDate,
                                 Username = note.Username
                             };

            return entity;
        }

        public static void PopulateFromEntity(this Note note, AirtimeBilling.Core.Entities.Note entity)
        {
            note.Type = entity.NoteType;            
            note.Comment = entity.Comment;
            note.EntityId = entity.EntityId;
            note.NoteDate = entity.NoteDate;
            note.Username = entity.Username;
        }
    }
}
