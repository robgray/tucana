using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Mammoth.BigpondMusic.Data.NHibernateSpecific;
using Mammoth.BigpondMusic.NHibernateError;
using Mammoth.Library;
using NHibernate;
using NHibernate.Expression;
using Mammoth.BigpondMusic.DataInterfaces;
using NHibernate.Metadata;
using ISession=NHibernate.ISession;
using ML = Mammoth.Library;

namespace Mammoth.BigpondMusic.Data
{
	public abstract class AbstractBroker<T, IdT> : IBroker<T, IdT> {

	    protected readonly INHibernateSessionFactory _nhibernateSessionFactory;
	    protected const int MAXIMUM_ID_LIST_LENGTH = 2000;
	    public const int GETALL_MAX = 10000;

	    public AbstractBroker()
	    {
	        _nhibernateSessionFactory = new HttpNHibernateSessionFactory();
	    }

	    public AbstractBroker(INHibernateSessionFactory sessionFactory)
	    {
	        _nhibernateSessionFactory = sessionFactory;
	    }

	    /// <summary>
		/// Loads an instance of type TypeOfListItem from the DB based on its ID.
		/// </summary>
		public virtual T GetById(IdT id)
		{
			// NHibernate has Load and Get. Get returns null, Load throws on missing row
			return NHibernateSession.Get<T>(id);
		}
        
		/// <summary>
		/// Retrieve a list of T with IDs corresponding to the ID list. The returned list
		/// may not match the order of the supplied ids, only contains an item once
		/// even if the ID list contains it multiple times, and does not do anything
		/// about IDs that are not in the database.
		/// </summary>
		/// <returns></returns>
		protected virtual List<T> GetByIdList_UnsortedWithoutDuplicates(IList<IdT> idList, params ICriterion[] filteringCriterion)
		{
			return GetByIdList_UnsortedWithoutDuplicates(null, idList, filteringCriterion);
		}

		protected List<T> GetByIdList_UnsortedWithoutDuplicates(SkuVisibility? visibility, IList<IdT> idList, params ICriterion[] filteringCriterion)
		{
		    List<T> results = new List<T>();

            if (idList.Count == 0)
                return results;

            int i = 0;
		    IList<IdT> requestIdList = new List<IdT>();
			while (i < idList.Count) {
				requestIdList.Add(idList[i++]);

				if (i % MAXIMUM_ID_LIST_LENGTH == 0 || i == idList.Count) {
				    List<T> requestResults = GetByIdListWithAcceptableLength(visibility, requestIdList, filteringCriterion);

				    results.AddRange(requestResults);

                    requestIdList.Clear();
				}
			}

            return results;
		}

		protected virtual IDictionary<IdT, T> GetByIds(IEnumerable<IdT> ids)
		{
			Dictionary<IdT, T>  results = new Dictionary<IdT, T>();
			foreach (var idsChunk in ids.Split(MAXIMUM_ID_LIST_LENGTH)) {
				InExpression inExpression = new InExpression("Id", idsChunk.Cast<object>().ToArray());
				ICriteria criteria = SetupCriteria(-1, null, new[] {inExpression}, null);
				foreach (var result in criteria.List<T>()) {
					results[(IdT) NHibernateSession.GetIdentifier(result)] = result;
				}
			}
			return results;
		}

		/// <summary>
        /// Returns entities by their identifier
        /// and assumes the method has been supplied
        /// with an acceptable length of identifiers to lookup
        /// </summary>
		protected List<T> GetByIdListWithAcceptableLength(SkuVisibility? visibility, IList<IdT> idList, params ICriterion[] filteringCriterion)
        {
			object[] ids = new object[idList.Count];
			for (int i = 0;i < idList.Count;i++)
				ids[i] = idList[i];

			Conjunction criterium = new Conjunction();
			criterium.Add(new InExpression("Id", ids));
			if (null != filteringCriterion)
				foreach (ICriterion c in filteringCriterion)
					criterium.Add(c);

			return SetupCriteriaWithinRange(-1, null, null, 0, 0, null, null, visibility, criterium).List<T>() as List<T>;
		}

		/// <summary>
		/// Loads every instance of the requested type with no filtering.
		/// </summary>
		public virtual List<T> GetAll()
		{
			return GetAll(null);
		}

		/// <summary>
		/// Loads every instance of the requested type with no filtering, using the 2nd level cache if possible.
		/// </summary>
		public virtual List<T> GetAllWithCache()
		{
			ICriteria criteriaCached = SetupCriteria(-1, null, null, null).SetCacheable(true);
			List<T> result = criteriaCached.List<T>() as List<T>;

			if (result.Count > GETALL_MAX)
				throw new RepositoryException(String.Format("Result of GetAllWithCache() had more than {0} rows; use NHibernate Enumerate for such operations", GETALL_MAX));

			return result;
		}
		

		/// <summary>
		/// Loads every instance of the requested type with no filtering.
		/// </summary>
		public virtual List<T> GetAll(Order order)
		{
			List<T> result = GetByCriteria(null as ICriterion[], order);
			if (result.Count > GETALL_MAX)
				throw new RepositoryException( String.Format("Result of GetAll() had more than {0} rows; use NHibernate Enumerate for such operations", GETALL_MAX) );
			return result;
		}

		/// <summary>
		/// Loads every instance of the requested type using the supplied <see cref="ICriterion" />.
		/// If no <see cref="ICriterion" /> is supplied, this behaves like <see cref="GetAll()" />.
		/// </summary>
		public List<T> GetByCriteria(params ICriterion[] criterion)
		{
		    return GetByCriteria(null, criterion, null);    
		}

		public List<T> GetByCriteria(ICriterion criterion, Order order)
		{
			return GetByCriteria(null, new ICriterion[] {criterion}, order);
		}

		public List<T> GetByCriteria(ICriterion[] criterion, Order order)
		{
			return GetByCriteria(null, criterion, order);
		}

		public List<T> GetByCriteria(Order order, int pageLength, int page, bool overfetch, params ICriterion[] criterion)
		{
			return GetByCriteria(order, pageLength, page, overfetch, null, null, criterion);
		}

		public List<T> GetByCriteria(Order order, int pageLength, int page, bool overfetch, string aliasproperty, string aliasname, params ICriterion[] criterion)
		{
			ICriteria criteria = SetupCriteria(-1, null, criterion, order,pageLength,page, overfetch, aliasproperty, aliasname);
			return criteria.List<T>() as List<T>;
		}

		public List<T> GetByCriteria(LockMode lockMode, ICriterion[] criterion, Order order)
		{
            return GetByCriteria(lockMode, criterion, order, -1);
		}

		public List<T> GetByCriteria(LockMode lockMode, ICriterion[] criterion, Order order, int timeOut)
        {
        	ICriteria criteria = SetupCriteria(timeOut, lockMode, criterion, order);
        	return criteria.List<T>() as List<T>;
        }

		private ICriteria SetupCriteria(int timeOut, LockMode lockMode, ICriterion[] criterion, Order order)
		{
			return SetupCriteria(timeOut, lockMode, criterion, order, 0, 0, false, null, null);
		}

		private ICriteria SetupCriteria(int timeOut, LockMode lockMode, ICriterion[] criterion, Order order, int pageLength, int page, bool overfetch, string aliasproperty, string aliasname)
		{
		    int start = page==0? -1 : (page - 1)*pageLength;
		    int limit = overfetch ? pageLength + 1 : pageLength;
		    return SetupCriteriaWithinRange(timeOut, lockMode, order, start, limit, aliasproperty, aliasname, criterion);
		}
		
        public List<T> GetCriteriaWithinRange(Order order, int start, int limit, string aliasproperty, string aliasname, params ICriterion[] criterion)
        {
            int timeOut = -1;
            LockMode lockMode = null;
            return SetupCriteriaWithinRange(timeOut, lockMode, order, start, limit, aliasproperty, aliasname, criterion).List<T>() as List<T>;
        }
		
		protected virtual ICriteria SetupCriteriaWithinRange(int timeOut, LockMode lockMode, Order order, int start, int limit, string aliasproperty, string aliasname, params ICriterion[] criterion)
		{
			return SetupCriteriaWithinRange(timeOut, lockMode, order, start, limit, aliasproperty, aliasname, null, criterion);
		}

        protected ICriteria SetupCriteriaWithinRange(int timeOut, LockMode lockMode, Order order, int start, int limit, string aliasproperty, string aliasname, SkuVisibility? visibility, params ICriterion[] criterion)
        {
			ISession session;
			if (visibility.HasValue)
				session = GetNHibernateSession(visibility.Value);
			else
				session = NHibernateSession;

			ICriteria criteria = session.CreateCriteria(typeof(T)); ;

            if (!String.IsNullOrEmpty(aliasproperty) && !String.IsNullOrEmpty(aliasname))
            {
                criteria.CreateAlias(aliasproperty, aliasname);
            }

            if (timeOut > -1)
                criteria.SetTimeout(timeOut);

            if (lockMode != null)
                criteria.SetLockMode(lockMode);

                // page values start at 1 for the first page.
            if (start >= 0 && limit != 0)
            {
                criteria.SetFirstResult(start);
                criteria.SetMaxResults(limit);
            }

			if (criterion != null)
            {
                foreach (ICriterion criterium in criterion)
                {
					criteria.Add(criterium);
                }
            }

			if (order != null)
            {
                var multiOrder = order as MultiOrder;

                if (multiOrder != null)
                {
                    foreach (var ord in multiOrder.GetOrders())
                        criteria.AddOrder(ord);
                }
                else
                {
                    criteria.AddOrder(order);
                }
            }

            return criteria;
        }

		protected virtual IQuery CreateQuery(string hql)
		{
			return CreateQuery(null, hql);
		}

		protected virtual IQuery CreateQuery(SkuVisibility? visibility, string hql)
		{
			if (visibility.HasValue)
				return GetNHibernateSession(visibility.Value).CreateQuery(hql);
			return NHibernateSession.CreateQuery(hql);
		}

		public virtual T GetUniqueByCriteria(params ICriterion[] criterion)
        {
	        return GetUniqueByCriteria(null, null, null, criterion);
        }

        public virtual T GetUniqueByCriteria(Order order, params ICriterion[] criterion)
        {
            return GetUniqueByCriteria(null, null, order, criterion);
        }

	    protected T GetUniqueByCriteria(SkuVisibility? visibility, LockMode lockMode, Order order, params ICriterion[] criterion)
		{
	    	ISession session;
			if (visibility.HasValue)
				session = GetNHibernateSession(visibility.Value);
			else
				session = NHibernateSession;

			ICriteria criteria = session.CreateCriteria(typeof(T));;

            if (lockMode != null)
                criteria.SetLockMode(lockMode);

            foreach (ICriterion criterium in criterion) {
				criteria.Add(criterium);
			}
			criteria.SetMaxResults(1);

            if (order != null)
                criteria.AddOrder(order);

            return criteria.UniqueResult<T>();
		}


		public virtual int CountByCriteria(string countfield, params ICriterion[] criterion)
		{
			return CountByCriteria(null, countfield, null, null, criterion);
		}

		public virtual int CountByCriteria(string countfield, string aliasproperty, string aliasname, params ICriterion[] criterion)
		{
			return CountByCriteria(null, countfield, aliasproperty, aliasname, criterion);
		}

		protected virtual int CountByCriteria(SkuVisibility? visibility, string countfield, string aliasproperty, string aliasname, params ICriterion[] criterion)
		{
			ISession session;
			if (visibility.HasValue)
				session = GetNHibernateSession(visibility.Value);
			else
				session = NHibernateSession;

			ICriteria criteria = session.CreateCriteria(typeof(T)); ;

			if (!String.IsNullOrEmpty(aliasproperty) && !String.IsNullOrEmpty(aliasname)) {
				criteria.CreateAlias(aliasproperty, aliasname);
			}

			foreach (ICriterion criterium in criterion) {
				criteria.Add(criterium);
			}
			Object rawcount = criteria.SetProjection(Projections.Count(countfield)).UniqueResult();

			if (rawcount is System.Int32) {
				return (int)rawcount;
			}
			else {
				return 0;
			}
		}

		/// <summary>
		/// Don't use me! I want to be removed from AbstractBroker someday.
		/// </summary>
		public virtual List<T> GetByExample(T exampleInstance, params string[] propertiesToExclude)
		{
			ICriteria criteria = NHibernateSession.CreateCriteria(typeof(T));
			Example example = Example.Create(exampleInstance);

			foreach (string propertyToExclude in propertiesToExclude) {
				example.ExcludeProperty(propertyToExclude);
			}

			criteria.Add(example);

			return criteria.List<T>() as List<T>;
		}

		public enum ChangeType
		{
			Commit,
			Delete, Save
		}

		/// <summary>
		/// For entities with automatatically generated IDs, such as identity, SaveOrUpdate may
		/// be called when saving a new entity.  SaveOrUpdate can also be called to update any
		/// entity, even if its ID is assigned.
		/// </summary>
		public virtual T Commit(T entity)
		{
			return Change(entity, ChangeType.Commit);
		}

        /// <summary>
		/// Complete a change on NHibernateSession, Flush, and wrap any ADOException.
		/// </summary>
		public virtual T Change(T entity, ChangeType changeType)
		{

                try
                {
					switch (changeType)
					{
						case ChangeType.Commit:
							WithinTransaction(() => NHibernateSession.SaveOrUpdate(entity));
							break;
						case ChangeType.Delete:
							WithinTransaction(() => NHibernateSession.Delete(entity));
							break;
                        case ChangeType.Save:
                            WithinTransaction(() => NHibernateSession.Save(entity));
                            break;
                    }
                	//NHibernateSession.Flush();
                }
                catch (ADOException ex)
                {
                    //If exception thrown close connection and re-throw to be handled further up.
                    ISession session = NHibernateSession;
                    //Roll back active transaction. Otherwise attempts to rollback later are broken because
                    //session will be closed.
                    if (session.Transaction != null && session.Transaction.IsActive) 
                    {
                        session.Transaction.Rollback();
                    }
                    session.Close();

                    ISessionFactory sessionfactory = NHibernateSessionManager.Instance.SessionFactory;
                    SqlException sqlEx = ex.InnerException as SqlException;
                    //If inner exception is SqlException we can parse the hbm file to find which fields the error relates to and rethrow as a
                    //more informative NHibernateCommitException.
                    if(sqlEx != null)
                    {
                        IClassMetadata catMeta = null;

						try {
							catMeta = sessionfactory.GetClassMetadata(entity.GetType());
						} catch (MappingException) {
							// We cannot resolve the mapping for this entity (perhaps the Type of this class is an NHibernate Proxy which
							// we do not have an explicit mapping file for, obviously). If so, we can just re-throw the original exception.
						}

						if (catMeta != null) {
							ErrorParser parser = new ErrorParser(catMeta, sqlEx, entity);
							NHibernateCommitException parsedException = new NHibernateCommitException("", ex, parser);
							throw parsedException;
						}

                    	throw;
                    }
                    throw;
                }

		    return entity;
		}

		public virtual void Delete(T entity)
		{
			Change(entity, ChangeType.Delete);
		}

		//Convenience property to get the session without filtering
		protected ISession NHibernateSession {
			get {
				return GetNHibernateSession(SkuVisibility.Default);
			}
		}

		/// <summary>
		/// Exposes the ISession used within the DAO.
		/// </summary>
		protected virtual ISession GetNHibernateSession(SkuVisibility visibility)
		{
			ISession session = _nhibernateSessionFactory.GetSession();
			//disable all visibility filters.
			foreach (FilteredAbstractBroker<T, IdT>.SkuFilter sf in FilteredAbstractBroker<T, IdT>.GetAllFilters()) {
				session.DisableFilter(sf.Name);
			}
			return session;
		}

		public void Refresh(T entity)
		{
			NHibernateSession.Refresh(entity);
		}

		/*
		//this should be a way of getting a collection's size without initializing any entities within.
		//it's broken when using generics.
		public int GetCollectionCount(object list)
		{
			return (int) NHibernateSession.CreateFilter(list, "select count(*)").UniqueResult();
		}
		*/

		protected AbstractCriterion ExpressionEqOrIsNull(string propertyName, object value)
		{
			if (value == null)
				return Expression.IsNull(propertyName);
			return Expression.Eq(propertyName, value);
		}


		/// <summary>
		/// This method is not *guaranteed* to give an accurate account of persistency if something has happened to the 
		/// NHibernate session or the repository. Use this method with caution in places that use a mixture of sessions or 
		/// repositories (like reporting).
		/// </summary>
		public bool IsPersistent(T entity, IdT id)
		{
			if (NHibernateSession.Contains(entity))
				return true;
			if (!object.ReferenceEquals(id, default(IdT)) && !id.Equals(default(IdT)))
				return true;
			return false;
		}

        public virtual T Create()
        {
            return (T)Activator.CreateInstance(typeof(T), null);
        }

	    public virtual T Save(T entity)
	    {
	        return Change(entity, ChangeType.Save);
	    }


	    protected void WithinTransaction(Action transactional)
		{
			WithinTransaction(IsolationLevel.Unspecified, transactional);
		}


		protected void WithinTransaction(IsolationLevel isolationLevel, Action transactional)
		{
			Transaction.WithinTransaction(isolationLevel, transactional);
		}

		
	}

    public abstract class AbstractBrokerWithProxyGroupSupport<T, IdT> : AbstractBroker<T, IdT>
        where T : IDomainObject<IdT>
    {

        protected IList<T> GetByIdList(IList<IdT> idList) {
            IList<T> list = new List<T>();

            if (idList.Count == 0)
                return list;

            IList<T> items = GetByIdList_UnsortedWithoutDuplicates(idList);

            // Build a map showing what index a particular ID should be at
            Hashtable map = new Hashtable();
            foreach (T m in items)
                map[m.Id] = m;

            // Put each item into an array at the correct index
            T[] ordered = new T[idList.Count];
            for (int idx = 0; idx < idList.Count; idx++)
                ordered[idx] = (T)map[idList[idx]];

            // Build the final list from the ordered array
            foreach (int idx in ML.Array.Sequence(0, idList.Count))
            {
                if (ordered[idx] != null)
                    list.Add(ordered[idx]);
                else
                    throw new InvalidProxyException("");
            }

            return list;
        }

    }

    public class MultiOrder : Order
    {
        private readonly IList<Order> _orders = new List<Order>();

        public MultiOrder() : base(null, false) { }

        public MultiOrder WithOrder(string propertyName)
        {
            return WithOrder(propertyName, true);
        }

        public MultiOrder WithOrder(string propertyName, bool asc)
        {
            _orders.Add(new Order(propertyName, asc));

            return this;
        }

        public IList<Order> GetOrders()
        {
            return _orders.ToList().AsReadOnly();
        }

        public override string ToSqlString(ICriteria criteria, ICriteriaQuery criteriaQuery)
        {
            throw new NotSupportedException("MultiOrder is not intended to be passed to NHibernate directly, it should be used only through AbstractBroker.GetByCriteria methods.");
        }
    }

}