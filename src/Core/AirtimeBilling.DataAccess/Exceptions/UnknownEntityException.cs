﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirtimeBilling.DataAccess.Repositories
{
    public class UnknownEntityException : Exception
    {
        public UnknownEntityException() : base("The entity could not be found") { }

        public UnknownEntityException(string message) : base(message) { }

        public UnknownEntityException(string message, Exception innerException) : base(message, innerException) { } 
    }
}
