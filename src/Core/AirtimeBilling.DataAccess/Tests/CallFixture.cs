﻿using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Linq;
using System.Text;
using AirtimeBilling.DataAccess.Repositories;
using NUnit.Framework;


namespace AirtimeBilling.DataAccess.Tests
{
    [TestFixture]
    public class CallFixture
    {        
        private Call _call;

        [SetUp]
        public void SetUp()
        {
            _call = new Call();
            _call.Contract = new Contract();
            _call.Contract.Plan = new Plan()
                                      {
                                          Tariffs = new EntitySet<Tariff>(),
                                          Flagfall = 0M,
                                          FreeCallAmount = 0M,
                                          UnitOfTime = 20
                                      };
            
            Tariff tariff = new Tariff();
            tariff.Calls.Add(_call);
            tariff.PlanId = _call.Contract.Plan.PlanId;
            tariff.Plan = _call.Contract.Plan;
            tariff.UnitCost = 1.20M;
            
            _call.Contract.Plan.Tariffs.Add(tariff);
            _call.Tariff = tariff;
            _call.VolumeUnit = 1;  // Voice Call
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_twenty_seconds()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 20; // 20 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.05M;
            _call.UnitsOfTime = 7;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(7, cost);
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_thirty_seconds()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 30; // 30 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.05M;
            _call.UnitsOfTime = 5;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(7.5, cost);
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_one_second()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 1; // 1 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.08333M;
            _call.UnitsOfTime = 125;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(6.25, cost);
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_one_minute()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 60; // 60 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.05M;
            _call.UnitsOfTime = 3;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(9, cost);
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_seventy_five_seconds()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 75; // 60 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.05M;
            _call.UnitsOfTime = 2;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(7.50, cost);
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_ninety_seconds()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 90; // 90 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.05M;
            _call.UnitsOfTime = 2;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(9, cost);
        }

        [Test]
        public void Call_cost_treats_tarrif_per_minute_blocks_of_one_thousand_seconds()
        {
            // Arrange
            _call.Contract.Plan.UnitOfTime = 1000; // 1000 second blocks
            _call.Tariff.UnitCost = 3M; // $3 per minute of calls.

            _call.Volume = 2.05M;
            _call.UnitsOfTime = 1;

            // Act
            var cost = _call.Cost;

            // Assert 
            Assert.AreEqual(50, cost);
        }

        [Test]
        public void Data_is_costed_using_volume_multiplied_by_quantity()
        {
            _call.Contract.Plan.UnitOfTime = 30; // 30 second call blocks
            _call.VolumeUnit = 2;
            _call.Volume = 5.56M; // 5.56 MBytes
            _call.UnitsOfTime = 5.56M;
            _call.Tariff.UnitCost = 2.55M;

            var cost = _call.Cost;

            Assert.AreEqual(14.178, cost);
        }

        [Test]
        public void SMS_is_costed_using_volume_multiplied_by_quantity()
        {
            _call.Contract.Plan.UnitOfTime = 30; // 30 second call blocks
            _call.VolumeUnit = 3;
            _call.Volume = 1; 
            _call.UnitsOfTime = 1;
            _call.Tariff.UnitCost = 0.55M;

            var cost = _call.Cost;

            Assert.AreEqual(0.55, cost);
        }
    }
}
