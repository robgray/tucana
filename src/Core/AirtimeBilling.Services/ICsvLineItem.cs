﻿namespace AirtimeBilling.Services
{
    public interface ICsvLineItem
    {
        string GetCsvLine();
    }
}
