﻿using System;

namespace AirtimeBilling.Services
{
    public class InvoiceRunInProgressException : Exception
    {
        public InvoiceRunInProgressException() : base("Cannot perform Invoice Run.  Another is already in progress.") { }

        public InvoiceRunInProgressException(string message) : base(message) { }

        public InvoiceRunInProgressException(string message, Exception inner) : base(message, inner) { }
            
    }
}
