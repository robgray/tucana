﻿using System.Collections.Generic;
using System.Text;

namespace AirtimeBilling.Services
{
    public static class CsvBuilder
    {
        public static string CreateCsvFile(IList<ICsvLineItem> items)
        {
            var buffer = new StringBuilder();
            foreach(var line in items) {
                buffer.AppendLine(line.GetCsvLine());
            }

            return buffer.ToString();
        }
    }
}
