﻿using System;

namespace AirtimeBilling.Services
{
    /// <summary>
    /// This exception is thrown if trying to perform an invoice run too often.
    /// </summary>
    public class InvoiceRunFrequencyException : Exception
    {
        public InvoiceRunFrequencyException() : base("Invoice Run already performed this month") { }

        public InvoiceRunFrequencyException(string message) : base(message) { }

        public InvoiceRunFrequencyException(string message, Exception innerException) : base(message, innerException) { }
        
    }
}
