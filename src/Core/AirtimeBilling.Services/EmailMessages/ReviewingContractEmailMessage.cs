﻿using System;
using System.Text;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Services.EmailMessages
{
    public class ReviewingContractEmailMessage : EmailFormat
    {
        public string ContractNumber { get; set; }
        public string ContactName { get; set; }

        #region IEmailFormat Members

        protected override string  GetBody()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div>");
            sb.AppendLine(string.Format("Dear {0},", ContactName));
            sb.AppendLine("</div>");
            sb.AppendLine(
                string.Format("<br/><div>I has begun the Contract activation process for Contract '<b>{0}</b>'<br/>",
                              ContractNumber));
            sb.AppendLine("An activation confirmation email will be sent shortly, once the activation has been completed.</div>");
            return sb.ToString();
        }

        protected override string GetNoHtml()
        {
            var output = string.Format(Environment.NewLine + Environment.NewLine +
                "I have begun the Contract activation process for contract {0} for {1}." + Environment.NewLine +
                "An activation confirmation email will be sent shortly, once the activation has been completed." +
                Environment.NewLine + Environment.NewLine +
                "Kind Regards" + Environment.NewLine + "I Satellite Solutions", ContractNumber, ContactName);

            return output;
        }

        #endregion
    }
}
