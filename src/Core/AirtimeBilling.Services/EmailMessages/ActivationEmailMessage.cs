﻿using System.Text;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Services.EmailMessages
{
    public class ActivationEmailMessage : EmailFormat
    {
        public string ContractNumber { get; set; }
        public string Name { get; set; }
        public string Plan { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber3 { get; set; }
        public string PhoneNumber4 { get; set; }
        
        public string Pin { get; set; }
        public BillingMethod PaymentMethod { get; set; }
        public bool PostBill { get; set; }
        public bool Paperless { get; set; }
        
        #region IEmailFormat Members

        protected override string  GetBody()
        {
            var message = new StringBuilder();
            message.AppendLine("<div>Dear " + Name + ",</div>");
            message.AppendLine("<div style='margin-top:20px;'>");
            message.AppendLine("<span>The following contract has now been Activated on I Satellite Solutions airtime:</span>");
            message.AppendLine("</div>");
            message.AppendLine("<div style='margin-top: 20px; margin-left: 20px;'>");
            message.AppendLine("<table width='600'>");
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Contract:</td>");
            message.AppendLine("<td>" + ContractNumber + "</td>");            
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Name:</td>");
            message.AppendLine("<td>" + Name + "</td>");
            message.AppendLine("</tr>");
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Plan:</td>");
            message.AppendLine("<td>" + Plan + "</td>");
            message.AppendLine("</tr>");
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Phone Number:</td>");
            message.AppendLine("<td>" + PhoneNumber1 + "</td>");
            message.AppendLine("</tr>");
            if (!string.IsNullOrEmpty(PhoneNumber2))
            {
                message.AppendLine("<tr>");
                message.AppendLine("<td width='200'>Phone Number 2:</td>");
                message.AppendLine("<td>" + PhoneNumber2 + "</td>");
                message.AppendLine("</tr>");
            }
            if (!string.IsNullOrEmpty(PhoneNumber3))
            {
                message.AppendLine("<tr>");
                message.AppendLine("<td width='200'>Phone Number 3:</td>");
                message.AppendLine("<td>" + PhoneNumber3 + "</td>");
                message.AppendLine("</tr>");
            }
            if (!string.IsNullOrEmpty(PhoneNumber4))
            {
                message.AppendLine("<tr>");
                message.AppendLine("<td width='200'>Phone Number 4:</td>");
                message.AppendLine("<td>" + PhoneNumber4 + "</td>");
                message.AppendLine("</tr>");
            }
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Pin:</td>");
            message.AppendLine("<td>" + Pin + "</td>");
            message.AppendLine("</tr>");
            message.AppendLine("</table>");
            message.AppendLine("<br/><table>");
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Payment Method:</td>");
            message.AppendLine("<td>" + ((int)PaymentMethod == (int)BillingMethod.CreditCard ? "Credit Card" : "Invoice") + "</td>");
            message.AppendLine("</tr>");
            message.AppendLine("<tr>");
            message.AppendLine("<td width='200'>Invoice Delivery Method:</td>");
            string deliveryMethod = "";
            if (PostBill)
            {
                deliveryMethod = "Post";

                if (Paperless) {
                    deliveryMethod += " and Email";
                }
            }            
            else if (Paperless)
            {
                deliveryMethod = "Email";
            }
            message.AppendLine("<td>" + deliveryMethod + "</td>");
            message.AppendLine("</tr>");
            message.AppendLine("</table>");
            message.AppendLine("</div>");

            var output = message.ToString();

            // replace the labels with style          
            output = output.Replace("<label>", "<label style='width: 150px; float: left;'>");

            return output;
        }

        protected override string GetNoHtml()
        {
            var message = new StringBuilder();
            message.AppendLine("The following contract has now been Activated on I Satellite Solutions airtime: ");
            message.AppendLine();
            message.AppendLine(string.Format("Contract: {0}", ContractNumber));
            message.AppendLine(string.Format("Name: {0}", Name));
            message.AppendLine(string.Format("Plan: {0}", Plan));
            message.AppendLine(string.Format("Phone Number: {0}", PhoneNumber1));
            if (!string.IsNullOrEmpty(PhoneNumber2)) 
                message.AppendLine(string.Format("Phone Number 2: {0}", PhoneNumber2));
            if (!string.IsNullOrEmpty(PhoneNumber3))
                message.AppendLine(string.Format("Phone Number 3: {0}", PhoneNumber3));
            if (!string.IsNullOrEmpty(PhoneNumber4))
                message.AppendLine(string.Format("Phone Number 4: {0}", PhoneNumber4));
            message.AppendLine(string.Format("Pin: {0}", Pin));
            message.AppendLine();
            message.AppendLine(string.Format("Payment Method: {0}",
                                             PaymentMethod == BillingMethod.CreditCard
                                                 ? "Credit Card"
                                                 : "Invoice"));
            message.AppendLine();
            if (PaymentMethod == BillingMethod.Invoice)
                message.AppendLine(string.Format("Invoice Delivery Method: {0}", PhoneNumber4));
            message.AppendLine();
            message.AppendLine("Kind Regards");
            message.AppendLine("I Satellite Solutions");

            return message.ToString();
        }

        #endregion
    }
}
