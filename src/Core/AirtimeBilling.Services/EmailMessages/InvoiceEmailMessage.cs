﻿using System;
using System.Text;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Services.EmailMessages
{
    public class InvoiceEmailMessage : EmailFormat 
    {
        public string Name { get; set; }
        public string InvoiceNumber { get; set; }
        public DateTime InvoiceDate { get; set; }

        #region IEmailFormat Members

        protected override string GetNoHtml()
        {
            var message = new StringBuilder();
            message.AppendLine("Dear " + Name);
            message.AppendLine();
            message.AppendLine("Your account has been processed for this month.");
            message.AppendLine("Please find the associated invoice attached to this email.");
            message.AppendLine();
            message.AppendLine("\tInvoice Number '" + InvoiceNumber + "'");
            message.AppendLine();
            message.AppendLine();
            message.AppendLine("Regards,");
            message.AppendLine("I Satellite Solutions");
            message.AppendLine();

            return message.ToString();
        }

        protected override string GetBody()
        {
            var message = new StringBuilder();
            message.Append("<div>");
            message.Append("Dear " + Name + ",<br /><br />");
            message.Append("</div>");
            message.Append("<div>");
            message.Append("Your account has been processed for this month.<br />");
            message.Append("Please find the associated invoice attached to this email.");
            message.Append("<div id='invoice'>Invoice Number '" + InvoiceNumber + "'</div>");
            message.Append("</div>");            

            return message.ToString(); 
        }

        protected override string GetCustomStyles()
        {
            return "#invoice { padding: 10px 10px 10px 20px }";
        }

        #endregion
    }
}
