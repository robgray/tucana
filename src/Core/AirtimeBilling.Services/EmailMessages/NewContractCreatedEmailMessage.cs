﻿using System;
using System.Text;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Services.EmailMessages 
{
    public class NewContractCreatedEmailMessage : EmailFormat 
    {
        public int ContractId { get; set; }

        #region IEmailFormat Members

        protected override string GetNoHtml()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new InvalidOperationException("Not called from the web");
            }

            string domainName = System.Web.HttpContext.Current.Request.UrlReferrer.Host;
            string application = System.Web.HttpContext.Current.Request.ApplicationPath;
            string url = "http://" + domainName + application + "/Contracts/ViewContract.aspx?cid=" + ContractId.ToString();

            string output =
                "A request for airtime has been submitted. Please open the following link to begin the approval proceess." +
                Environment.NewLine + Environment.NewLine +
                "Link: " + url;

            return output;
        }

        protected override string  GetBody()
        {
            if (System.Web.HttpContext.Current == null)
            {
                throw new InvalidOperationException("Not called from the web");
            }
                        
            var domainName = System.Web.HttpContext.Current.Request.UrlReferrer.Host;
            var application = System.Web.HttpContext.Current.Request.ApplicationPath;
            var url = "http://" + domainName + application + "/Contracts/ViewContract.aspx?cid=" + ContractId.ToString();

            var sb = new StringBuilder();
            sb.AppendLine("<div>");
            sb.AppendLine(string.Format("Please follow <a href='{0}'>this</a> link to continue processing.", url));
            sb.AppendLine("</div>");
            
            return sb.ToString();
        }

        #endregion
    }
}
