﻿using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IUserService
    {
        GetUserResponse GetUserByAgentId(int agentId);

        GetUserResponse GetUserByContactId(int contactId);

        UpdateUserResponse UpdateUser(User user);
    }
}
