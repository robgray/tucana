﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface INetworkService
    {        
        IList<Network> GetAllNetworks();

        IList<Network> GetAllActiveNetworks();
     
        Network GetNetwork(int networkId);

        InsertNetworkResponse InsertNetwork(InsertNetworkRequest request);

        UpdateNetworkResponse UpdateNetwork(UpdateNetworkRequest request);

        DeleteNetworkResponse DeleteNetwork(DeleteNetworkRequest request);

        IList<NetworkTariff> GetNetworkTariffs(int networkId);

        UpdateNetworkTariffResponse UpdateNetworkTariff(UpdateNetworkTariffRequest request);

        DeleteNetworkTariffResponse DeleteNetworkTariff(DeleteNetworkTariffRequest request);

        InsertNetworkTariffResponse InsertNetworkTariff(InsertNetworkTariffRequest request);

        IList<Network> FindByNetworkName(string name);
    }
}
