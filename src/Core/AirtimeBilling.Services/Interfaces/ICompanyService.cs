﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface ICompanyService
    {        
        Company GetCompany(int companyId);
     
        IList<Company> FindCompanies(CompanySearch searchType, string searchValue); 

        SaveCompanyResponse SaveCompany(SaveCompanyRequest request);
    }
}
