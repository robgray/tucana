﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Services.Interfaces
{    
    public interface IInvoicingService
    {
        int InvoiceRun(DateTime callCutoffDate);
           
        IList<InvoiceSearchItem> FindInvoicesByRunNumber(int runNumber);
      
        IList<InvoiceSearchItem> FindInvoicesByInvoiceNumber(string invoiceNumber);

        IList<InvoiceRun> GetAllInvoiceRuns();

        string ExportCreditCardChargeFile(int runNumber);

        void ImportCreditCardChargeFile(string csvFile, string filename);

        IList<CustomFee> GetAllCustomFees();

        void EmailInvoice(string invoiceNumber);

        CustomFee GetCustomFee(int customFeeId);

        ResponseBase SaveCustomFee(CustomFee customFee);

        ResponseBase RemoveCustomFee(CustomFee customFee);

        IInvoiceSettings InvoiceSettings { get; }

        void SendInvoicesInEmail(int invoiceRunNumber);

        string SaveInvoiceHardcopy(Invoice invoice);

        bool HasContractBeenInvoiced(Contract contract);
    }
}
