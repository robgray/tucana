﻿namespace AirtimeBilling.Services.Interfaces
{
    public interface INumberGeneratorService
    {
        string NextInvoiceNumber();

        int NextInvoiceRunNumber();

        string NextContractNumber();

        string NextAccountNumber();
    }
}
