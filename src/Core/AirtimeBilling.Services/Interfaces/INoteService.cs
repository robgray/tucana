﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.Interfaces
{
    public interface INoteService
    {
        IList<Note> GetNotesFor(EntityBase entity);

        void SaveNote(Note note);                       
    }
}
