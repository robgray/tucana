﻿using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{    
    public interface IContractService
    {
        #region Retrieval Operations

        ViewContractResponse ViewContract(ViewContractRequest request);

        ContractSearchResponse FindContracts(ContractSearchRequest request);

        GetUnInvoicedCallsResponse GetUnInvoicedCalls(GetUnInvoicedCallsRequest request);

        #endregion 

        #region Data Insertion Operations
        
        NewContractResponse SubmitNewContract(NewContractRequest request);
        
        NewContractResponse SubmitNewContractForAccount(NewContractForAccountRequest request);
        
        #endregion

        #region Data Modification Opertions
        
        ApproveInvoicingResponse ApproveInvoicing(ApproveInvoicingRequest request);

        UpdateContractResponse UpdateContract(UpdateContractRequest request);
      
        VaryContractPlanResponse VaryContractPlan(VaryContractPlanRequest request);
      
        CloseContractResponse CloseContract(CloseContractRequest request);
        
        SuspendContractResponse SuspendContract(SuspendContractRequest request);

        #endregion

        #region Custom Fees

        ResponseBase AddCustomFee(int contractId, int customFeeId);

        ResponseBase RemoveCustomFee(int contractCustomeFeeId);

        CustomFeesResponse GetCustomFeesForContract(CustomFeesRequest request);

        #endregion
    }
}