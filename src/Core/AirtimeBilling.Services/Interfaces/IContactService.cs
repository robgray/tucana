﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IContactService
    {
        IList<Contact> GetAllContacts();

        IList<Contact> FindContacts(ContactSearch searchType, string searchValue);

        SaveContactResponse SaveContact(SaveContactRequest request);
        
        Contact GetContact(int contactId);
    }
}
