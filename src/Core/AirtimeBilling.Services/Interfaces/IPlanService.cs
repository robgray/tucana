﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IPlanService
    {                
        IList<Plan> GetPlansByNetworkId(int networkId);
        
        Plan GetPlan(int planId);

        InsertPlanResponse InsertPlan(InsertPlanRequest request);

        bool UpdatePlan(Plan plan);

        DeletePlanResponse DeletePlan(DeletePlanRequest request);

        IList<PlanTariff> GetPlanTariffs(int planId);

        bool UpdatePlanTariff(PlanTariff entity);

        CanDeletePlanResponse CanDelete(Plan plan);
        
    }
}
