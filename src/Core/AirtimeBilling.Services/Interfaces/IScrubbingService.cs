﻿using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IScrubbingService
    {
        ResponseBase ScrubContract(Contract contract);

        ResponseBase ScrubCustomer(Account account);

        ResponseBase CanScrubCustomer(Account account);
    }
}
