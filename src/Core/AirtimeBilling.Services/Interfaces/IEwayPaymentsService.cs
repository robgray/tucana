﻿namespace AirtimeBilling.Services.Interfaces
{
    public interface IEwayPaymentsService
    {

        string ExportCreditCardChargeFile(int runNumber);

        void ImportCreditCardChargeFile(string csvFile, string filename);
    }
}
