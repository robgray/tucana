﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.Interfaces
{
    public interface ILogReaderService
    {
        LogEntry ReadLogEntry(int id);

        IList<LogEntry> Find(DateTime fromDate, DateTime toDate);

        IList<LogEntry> Find(DateTime fromDate, DateTime toDate, string username);

        IList<LogEntry> Find(DateTime fromDate, DateTime toDate, string username, LoggingLevels level);
    }
}
