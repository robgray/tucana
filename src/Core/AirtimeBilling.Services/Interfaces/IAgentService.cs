﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IAgentService
    {        
        IList<Agent> GetAllAgents();
     
        Agent GetAgent(int agentId);

        SaveAgentResponse SaveAgent(SaveAgentRequest request);

        IList<Agent> FindAgentsByName(string name);
    }
}
