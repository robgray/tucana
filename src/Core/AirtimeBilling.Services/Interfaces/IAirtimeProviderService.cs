﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IAirtimeProviderService
    {        
        IList<AirtimeProvider> GetAirtimeProviders();
    }
}
