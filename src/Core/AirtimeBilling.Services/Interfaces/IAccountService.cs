﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Services.Interfaces
{
    public interface IAccountService
    {        
        Account GetAccountByAccountNumber(string accountNumber);

        Account GetAccountById(int accountId);
     
        IList<Account> GetAllAccounts();

        IList<Account> FindAccounts(AccountSearch searchType, string searchValue);
      
        SaveAccountResponse SaveAccount(SaveAccountRequest request);

        IList<Account> GetSubAccounts(MasterAccount account);

        ResponseBase MakeInvoiceRoot(int accountId);

        ResponseBase ReAttachToMasterAccount(int subAccountId);

        AccountPaymentResponse MakePayment(int accountId, decimal amount);

    }
}
