﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.Interfaces
{
    [ServiceContract(
     Name = "IActivityLoggingService",
     Namespace = Constants.NAMESPACE_SERVICES_CONTRACT,
     SessionMode = SessionMode.Allowed
     )]
    public interface IActivityLoggingService
    {
        /// <summary>
        /// Log an activity that occurred at the current date and time to the activity log 
        /// </summary>                
        /// <param name="user">User that performed the activity</param>
        /// <param name="activity">Description of the activity that took place</param>        
        [OperationContract]
        void WriteUserActivity(EntityBase entity, string user, string activity);

        /// <summary>
        /// Log an activity that occurred at the current date and time to the activity log 
        /// </summary>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>        
        /// <param name="activity">Description of the activity that took place</param>        
        [OperationContract]
        void WriteActivity(EntityBase entity, string activity);

        /// <summary>
        /// Log an activity that occurred at the specified date and time to the activity log 
        /// </summary>
        /// <param name="activityDate">Date the activity took place</param>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>
        /// <param name="user">User that performed the activity</param>
        /// <param name="activity">Description of the activity that took place</param>        
        [OperationContract]
        void WriteUserActivity(EntityBase entity, DateTime activityDate, string user, string activity);

        /// <summary>
        /// Log an activity that occurred at the specified date and time to the activity log.  
        /// Using the current IIdentity.Name as user.
        /// </summary>        
        /// <param name="activityDate">Date the activity took place</param>        
        /// <param name="activity">Description of the activity that took place</param>
        [OperationContract]
        void WriteActivity(EntityBase entity, DateTime activityDate, string activity);

        /// <summary>
        /// Writes a System activity to the activity log, using the current Date and Time
        /// </summary>        
        /// <param name="activity">Description of the activity that took place</param>
        [OperationContract]
        void WriteSystemActivity(EntityBase entity, string activity);

        /// <summary>
        /// Writes a System activity to the activity log, using the specified Date and Time
        /// </summary>
        /// <param name="activityDate">Time the activity occurred</param>        
        /// <param name="activity">Description of the activity that took place</param>
        [OperationContract]
        void WriteSystemActivity(EntityBase entity, DateTime activityDate, string activity);

        [OperationContract]
        IList<Activity> FindLogEntriesForEntity(EntityBase entity);

        [OperationContract]
        IList<Activity> FindLogEntriesForUser(string username);

    }
}
