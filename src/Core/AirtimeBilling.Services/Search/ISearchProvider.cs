﻿using System.Collections.Generic;

namespace AirtimeBilling.Services.Search
{
    public interface ISearchProvider
    {
        IList<SearchResult> Search(SearchCriteria criteria);
    }
}
