﻿using System.Collections.Generic;

namespace AirtimeBilling.Services.Search
{
    /// <summary>
    /// This class provides generic search services to allow search for a variety of items 
    /// within the system. It uses a search provider interface which various items can implement
    /// to provide different searches.
    /// </summary>
    public class SearchService
    {
        private readonly IList<ISearchProvider> searchProviders;

        public SearchService()
        {
            searchProviders = new List<ISearchProvider>();
        }

        public SearchService(IList<ISearchProvider> providers)
        {
            this.searchProviders = providers;
        }

        public IList<SearchResult> Search(SearchCriteria criteria)
        {
            var result = new List<SearchResult>();
            foreach(var provider in searchProviders)
            {
                result.AddRange(provider.Search(criteria));
            }
            return result;            
        }
    }
}
