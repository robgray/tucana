﻿using System.Collections.Generic;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Search
{
    internal class ContractSearchProvider : ISearchProvider 
    {
        private IContractRepository repository;

        public ContractSearchProvider() : this(RepositoryFactory.GetRepository<IContractRepository>()) { }

        public ContractSearchProvider(IContractRepository repository)
        {
            this.repository = repository;
        }

        #region ISearchProvider Members

        public IList<SearchResult> Search(SearchCriteria criteria)
        {
            var result = new List<SearchResult>();

            repository.FindByAccountNumber(criteria.SearchData);
            repository.FindByContact(criteria.SearchData);
            repository.FindByContractNumber(criteria.SearchData);
            repository.FindByPhoneNumber(criteria.SearchData);

            return result;
        }

        #endregion
    }
}
