﻿namespace AirtimeBilling.Services.Search 
{
    /// <summary>
    /// Represents a result returned by a search initiated by an ISearchProvider
    /// </summary>
    public class SearchResult
    {
        public string Summary { get; set; }
        public string Description { get; set; }        
        public string Url { get; set; }
        public double Rank { get; set; }        
    }
}
