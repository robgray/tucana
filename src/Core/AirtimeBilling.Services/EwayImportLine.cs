﻿using System;
using LINQtoCSV;

namespace AirtimeBilling.Services
{
    public class EwayImportLine
    {
        [CsvColumn(Name = "Date / Time", FieldIndex =  1)]
        public DateTime TransactionDateTime { get; set; }        

        [CsvColumn(Name = "Transaction Number", FieldIndex = 2)]
        public string EwayTransactionReference { get; set; }

        [CsvColumn(Name = "Type", FieldIndex = 3)]
        public string TransactionType { get; set; }

        [CsvColumn(Name = "Card Holder", FieldIndex = 4)]
        public string CardHolder { get; set; }

        [CsvColumn(Name = "Masked Credit Card", FieldIndex = 5)]
        public string CardData { get; set; }

        [CsvColumn(Name = "Response Code", FieldIndex = 6)]
        public string ResponseCode { get; set; }

        [CsvColumn(Name = "Result", FieldIndex = 7)]
        public string Result { get; set;  }

        [CsvColumn(Name = "Response Text", FieldIndex = 8)]
        public string ResponseText { get; set; }

        [CsvColumn(Name = "Currency", FieldIndex = 9)]
        public string Currency { get; set; }
               
        [CsvColumn(Name = "Amount", FieldIndex = 10)]
        public decimal Amount { get; set; }
        
        [CsvColumn(Name = "EmailAddress", FieldIndex = 11)]
        public string EmailAddress { get; set; }       

        [CsvColumn(Name = "Your Reference Number", FieldIndex = 12)]
        public string EwayInvoiceReference { get; set; }

        [CsvColumn(Name = "Invoice Reference", FieldIndex = 13)]
        public string InvoiceNumber{ get; set; }

        public bool IsSuccessful
        {
            get
            {
                int code;
                if (!int.TryParse(ResponseCode, out code)) {
                    return false;
                }
                
                return code == 0 || code == 8 || code == 10 || code == 11 ||
                       code == 16;
            }
        }
    }
}
