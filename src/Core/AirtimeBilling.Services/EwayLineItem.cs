﻿using System;
using AirtimeBilling.Core.Entities;
using LINQtoCSV;

namespace AirtimeBilling.Services
{
    public class EwayLineItem 
    {
        public EwayLineItem()
        {
            TrxnNumber = "N/A";
            TrxnOption1 = "N/A";
            TrxnOption2 = "N/A";
            TrxnOption3 = "N/A";
        }

        /// <summary>
        /// A whole number without AUSTRLAIAN DOLLARS sign
        /// in centrs. eg 10000 for $100.00
        /// </summary>
        [CsvColumn(FieldIndex = 1)]
        public int TotalAmount { get; set; }

        /// <summary>
        ///  The Customers first name
        /// </summary>
        [CsvColumn(FieldIndex = 2)]
        public string CustomerFirstName { get; set; }

        /// <summary>
        /// The Customers last name
        /// </summary>
        [CsvColumn(FieldIndex = 3)]
        public string CustomerLastName { get; set;  }

        /// <summary>
        /// The Customers email address
        /// </summary>
        [CsvColumn(FieldIndex = 4)]
        public string CustomerEmail { get; set; }

        /// <summary>
        /// The Customers postal address
        /// </summary>
        [CsvColumn(FieldIndex = 5)]
        public string CustomerAddress { get; set; }

        /// <summary>
        /// The Customers post code
        /// </summary>
        [CsvColumn(FieldIndex = 6)]
        public string CustomerPostCode { get; set; }

        /// <summary>
        /// Your invoice number
        /// </summary>
        [CsvColumn(FieldIndex = 7)]
        public string CustomerInvoiceRef { get; set; }

        /// <summary>
        /// Your invoice description
        /// </summary>
        [CsvColumn(FieldIndex = 8)]
        public string CustomerInvoiceDescription { get; set; }

        /// <summary>
        /// The card holders name
        /// </summary>
        [CsvColumn(FieldIndex = 9)]
        public string CardHoldersName { get; set; }

        /// <summary>
        /// The card number
        /// </summary>
        [CsvColumn(FieldIndex = 10)]
        public string CardNumber { get; set; }

        /// <summary>
        /// The card expiry month
        /// </summary>
        [CsvColumn(FieldIndex = 11)]
        public string CardExpiryMonth { get; set; }
        
        /// <summary>
        ///  The card expiry year
        /// </summary>
        [CsvColumn(FieldIndex = 12)]
        public string CardExpiryYear { get; set;  }

        /// <summary>
        /// An optional transaction reference 
        /// pass N/A if not used.
        /// </summary>
        [CsvColumn(FieldIndex = 13)]
        public string TrxnNumber { get; set; }

        /// <summary>
        /// An additional field for you to pass and recieve 
        /// information from eWAY. pass N/A if not used.
        /// </summary>
        [CsvColumn(FieldIndex = 14)]
        public string TrxnOption1 { get; set; }

        /// <summary>
        /// An additional field for you to pass and recieve 
        /// information from eWAY. pass N/A if not used.
        /// </summary>
        [CsvColumn(FieldIndex = 15)]
        public string TrxnOption2 { get; set; }

        /// <summary>
        /// An additional field for you to pass and recieve 
        /// information from eWAY. pass N/A if not used.
        /// </summary>
        [CsvColumn(FieldIndex = 16)]
        public string TrxnOption3 { get; set; }        

        public static EwayLineItem Create(Invoice invoice, Contact contact)
        {
            var exportLine = new EwayLineItem
            {
                TotalAmount = ((int)(Math.Round(invoice.TotalIncGst, 2) * 100)),
                CustomerFirstName = contact.FirstName,
                CustomerLastName = contact.LastName,
                CustomerEmail = contact.Email,
                CustomerAddress = contact.HomeAddress.ToString(),
                CustomerPostCode = contact.HomeAddress.Postcode,
                CustomerInvoiceRef = invoice.InvoiceNumber,
                CustomerInvoiceDescription = "I Satellite Airtime",
                CardHoldersName = contact.CreditCard.NameOnCard,
                CardNumber = contact.CreditCard.CardNumber,
                CardExpiryYear = contact.CreditCard.ExpiryDate.Substring(3, 2),
                CardExpiryMonth = contact.CreditCard.ExpiryDate.Substring(0, 2)
            };

            return exportLine;
        }
    }
}
