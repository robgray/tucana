﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.EmailMessages;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{    
    public class ContractService : IContractService
    {
        private readonly IContractRepository contractRepository;
        private readonly IAccountRepository accountRepository;
        private readonly IPlanRepository planRepository;
        private readonly IContactRepository contactRepository;
        private readonly ICompanyRepository companyRepository;
        private readonly IAgentRepository agentRepository;
        private readonly IActivityLoggingService activityService;
        private readonly Import.ICallDataImportManager importManager;
        private readonly INumberGeneratorService generator;
        private readonly ICustomFeeRepository _customFeeRepository;        
        private readonly IDateTimeFacade _dateTime;
        
        public ContractService() :
            this(        
            RepositoryFactory.GetRepository<IContractRepository>(),
            RepositoryFactory.GetRepository<IAccountRepository>(),
            RepositoryFactory.GetRepository<IPlanRepository>(),
            RepositoryFactory.GetRepository<IContactRepository>(),
            RepositoryFactory.GetRepository<ICompanyRepository>(),
            RepositoryFactory.GetRepository<IAgentRepository>(),            
            new ActivityLoggingService(),
            new NumberGeneratorService(),
            new Import.ImporterManager(),            
            RepositoryFactory.GetRepository<ICustomFeeRepository>(),            
            new DateTimeFacade()
        ) { }

        /// <summary>
        /// Use this Constructor for dependancy injection for testing.
        /// </summary>
        public ContractService(IContractRepository contractRepository, IAccountRepository accountRepository,
            IPlanRepository planRepository, IContactRepository contactRepository, 
            ICompanyRepository companyRepository, IAgentRepository agentRepository,
            IActivityLoggingService activityService, INumberGeneratorService generator, Import.ICallDataImportManager importManager, 
            ICustomFeeRepository customFeeRepository, IDateTimeFacade dateTime)
        {
            this.contractRepository = contractRepository;
            this.accountRepository = accountRepository;
            this.planRepository = planRepository;
            this.contactRepository = contactRepository;
            this.companyRepository = companyRepository;
            this.agentRepository = agentRepository;            
            this.activityService = activityService;
            this.generator = generator;
            this.importManager = importManager;
            _customFeeRepository = customFeeRepository;            
            _dateTime = dateTime;
        }

        #region IContractService Members

        public NewContractResponse SubmitNewContract(NewContractRequest request)
        {                          
            var response = new NewContractResponse();
            if (request.User != null)
            {
                System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            }
            else
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            
            var plan = planRepository.GetPlan(request.PlanId);

            #region Validation
           
            if (request.User is ContactUser)
            {
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            if (request.ActivationDate > request.EndDate)
            {
                response.Message = "Contract end date must be after contract activation date.";
                return response;
            }

            if (request.EndDate < _dateTime.Today.AddDays(1))
            {
                response.Message = "Contract end date must be after the current date.";
                return response;
            }

            // TODO: this is network dependent.
            if (string.IsNullOrEmpty(request.IMEINumber))
            {
                response.Message = "An IMEI Number must be supplied.";
                return response;
            }

            if (plan == null)
            {
                response.Message = "A plan must be selected for the Contract.";
                return response;
            }

            if (request.Contact == null)
            {
                // let exception happen.    
                LoggingUtility.LogWarning("SubmitNewContract", "ContractService", "Request does not contain a Contact.  Exception to follow.");
            }

            if (string.IsNullOrEmpty(request.Contact.Name))
            {
                response.Message = "No contact name has been supplied.";
                return response;
            }

            if (request.Contact.PostalAddress == null)
            {
                // let exception happen
                LoggingUtility.LogWarning("SubmitNewContract", "ContractService", "Request does not contain a Contact Postal Address.  Exception to follow.");
            }

            if (string.IsNullOrEmpty(request.Contact.PostalAddress.State) ||
                string.IsNullOrEmpty(request.Contact.PostalAddress.Suburb) ||
                string.IsNullOrEmpty(request.Contact.PostalAddress.Postcode) ||
                string.IsNullOrEmpty(request.Contact.PostalAddress.State))
            {
                response.Message = "Please ensure postal address details have been filled out.\n" +
                    "Including street, suburb, state, and postcode";
                return response;
            }

            // CREDIT CARD INFORMATION MUST BE Supplied
            if (request.Contact.CreditCard == null)
            {
                // Let this exception occurr. This should never happen.
                LoggingUtility.LogWarning("SubmitNewContract", "ContractService", "Request does not contain a Contact Credit Card.  Exception to follow.");
            }

            if (string.IsNullOrEmpty(request.Contact.CreditCard.CardNumber))
            {
                response.Message = "Please ensure a Credit Card Number has been supplied.";
                return response;
            }

            if (request.Contact.CreditCard.CreditCardType == CreditCardType.NotSupplied)
            {
                response.Message = "Please ensure a Credit Card Type has been supplied.";
                return response;
            }

            if (string.IsNullOrEmpty(request.Contact.CreditCard.NameOnCard))
            {

                response.Message = "Please ensure a Credit Card Name has been supplied.";
                return response;
            }

            if (string.IsNullOrEmpty(request.Contact.CreditCard.ExpiryDate))
            {
                response.Message = "Please ensure a Credit Card Expiry Date has been supplied.";
                return response;
            }

            // IF SELECTING PAPERLESS INVOICING/BILLING, AN EMAIL ADDRESS MUST BE SUPPLIED
            if (request.EmailBill && string.IsNullOrEmpty(request.Contact.Email))
            {
                response.Message = "Please ensure an email address is supplied when requesting Invoices to be emailed";
                return response;
            }

            if (request.EmailBillData && string.IsNullOrEmpty(request.Contact.Email))
            {
                response.Message = "Please ensure an email address is supplied when requesting Invoice data to be emailed";
                return response;
            }

            if (!request.PostBill && !request.EmailBill)
            {
                response.Message = "You must select a method to receive an Invoice.  Please selected Post, Email, or both.";
                return response;                    
            }

            #endregion 
                        
            try
            {
                using (var trans = new TransactionScope())
                {
                    var accountNumber = generator.NextAccountNumber();
                    if (string.IsNullOrEmpty(accountNumber))
                    {
                        response.IsSuccessful = false;
                        response.Message = "Failed to generate a account number.  Cannot accept booking at this time";
                        return response;
                    }
                    
                    if (request.Company != null && !string.IsNullOrEmpty(request.Company.CompanyName))
                    {
                        request.Company.Address = request.Contact.HomeAddress;
                        request.Company.PostalAddress = request.Contact.PostalAddress;

                        companyRepository.InsertCompany(request.Company);
                        if (request.Company.Id == null)
                        {
                            response.IsSuccessful = false;
                            response.Message = "Failed to create new Company";
                            return response;
                        }                        
                    }
                                        
                    contactRepository.InsertContact(request.Contact);
                    if (request.Contact.Id == null)
                    {
                        response.IsSuccessful = false;
                        response.Message = "Failed to create new Contract";
                        return response;
                    }

                    request.Contact.LogActivity(string.Format("Create contact with name '{0}'", request.Contact.Name));
                    
                    var account = new MasterAccount
                    {                        
                        AccountName = request.Contact.Name,
                        AccountNumber = accountNumber,
                        BillingMethod = BillingMethod.CreditCard,
                        IsInvoiceRoot = true,
                        BillingAddressType = BillingAddressType.Postal,
                        ContactId = request.Contact.Id.Value,
                        Password = request.AccountPassword,                    
                        PostBill = request.PostBill,                       
                        EmailBill = request.EmailBill,
                        EmailBillDataFile = request.EmailBillData,
                        IsInternational = request.IsInternational
                    };

                    if (request.Company != null && request.Company.Id != null)
                    {
                        account.CompanyId = request.Company.Id.Value;

                        // 001-153 - show company name on invoices.
                        account.AccountName = request.Company.CompanyName;
                    }

                    if (request.BillingMethod == BillingMethod.Invoice)
                    {
                        account.HasRequestedInvoicing = true;
                        account.BillingMethod = BillingMethod.CreditCard;
                    }                    
                                        
                    accountRepository.InsertAccount(account);
                    if (account.Id == null)
                    {
                        response.IsSuccessful = false;
                        response.Message = "Failed to create new Contract";
                        return response;
                    }
                    
                    account.LogActivity(string.Format("Create account with number '{0}'", accountNumber));
                    account.LogActivity("Account with request for invoicing submitted for approval");
                    
                    string contractNumber = generator.NextContractNumber();
                    if (string.IsNullOrEmpty(contractNumber))
                    {
                        response.Message = "Failed to generate a contract number.  Cannot accept Contract at this time";
                        response.IsSuccessful = false;
                        return response;
                    }

                   var contract = new Contract(_dateTime.Now)
                                       {
                                           PlanId = request.PlanId,
                                           AccountId = account.Id.Value,
                                           ActivationDate = request.ActivationDate,
                                           EndDate = request.EndDate,
                                           IMEINumber = request.IMEINumber,
                                           SimCard = request.SimCard,
                                           MessageBank = request.MessageBank,
                                           Data = request.Data,
                                           UsedBy = request.UsedBy,
                                           ContractNumber = contractNumber,

                                           // (Changed 13/04/2009) Starting status depends on the user type who submitted.
                                           // There is no way to enter phone numbers at this stage, so this is as far 
                                           // as the process can go.  
                                           ContractStatus = request.User is AgentUser
                                                                ?
                                                                    ContractStatus.ApplicationSubmitted
                                                                : ContractStatus.ApplicationPending                                                                                  
                                        };

                    var agent = request.User as AgentUser;
                    if (agent != null)
                    {
                        contract.AgentId = agent.AgentId;
                    }

                    contractRepository.InsertContract(contract);
                    if (contract.Id == null)  
                    {
                        response.IsSuccessful = false;
                        response.Message = "Failed to create new Contract";
                        return response;
                    }

                    contract.LogActivity(string.Format("Create new Contract with contract number '{0}'", contractNumber));
                    
                    if (System.Web.HttpContext.Current != null)
                    {
                        var email = new NewContractCreatedEmailMessage {ContractId = contract.Id.Value};

                        LoggingUtility.SendEmail(ConfigItems.NewContractEmailAddress, "New Airtime Contract Application",
                                                 email.GetHtml(), true);

                        contract.LogActivity(string.Format("Sent notification email for contract '{0}' to I Office", contractNumber));
                    }                           
                    
                    trans.Complete();
                    response.ContractId = contract.Id.Value;
                    response.IsSuccessful = true;                    
                }
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }
            
            return response;
        }

        public NewContractResponse SubmitNewContractForAccount(NewContractForAccountRequest request)
        {
            var response = new NewContractResponse();
            if (request.User != null)
            {
                System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            }
            else
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            var plan = planRepository.GetPlan(request.PlanId);
            var account = accountRepository.GetAccountByAccountNumber(
                request.AccountNumber == null ? string.Empty : request.AccountNumber);

            #region Request Validation
            
            if (string.IsNullOrEmpty(request.AccountNumber))
            {
                response.Message = "The Account Number to add the new Contract to has not been supplied.";
                return response;
            }

            if (string.IsNullOrEmpty(request.AccountPassword))
            {
                response.Message = "The Account Password used to verify customer identity has not been supplied.\n" +
                    "Cannot add contracts to accounts without permission."; 
                return response;
            }

            if (request.ActivationDate > request.EndDate)
            {
                response.Message = "Contract end date must be after contract activation date.";
                return response;
            }

            if (request.EndDate < _dateTime.Today.AddDays(1))
            {
                response.Message = "Contract end date must be after the current date.";
                return response;
            }

            // TODO: this is network dependant.
            if (string.IsNullOrEmpty(request.IMEINumber))
            {
                response.Message = "An IMEI Number must be supplied.";
                return response;
            }
            
            if (plan == null)
            {
                response.Message = "A plan must be selected for the Contract.";
                return response;
            }

            if (account == null)
            {
                response.Message = string.Format("Account '{0}' does not exist.", request.AccountNumber);
                return response;
            }

            if (!account.Password.Equals(request.AccountPassword))
            {
                response.Message = "Supplied Account Password does not match.";
                return response;
            }

            #endregion

            string username = request.User.Username;
                        
            using (var trans = new TransactionScope())
            {
                string contractNumber = generator.NextContractNumber();
                if (string.IsNullOrEmpty(contractNumber))
                {
                    response.Message = "Failed to generate a contract number.  Cannot accept booking at this time";
                    return response;                    
                }
                
                var contract = new Contract(_dateTime.Now)
                                            {
                                                AccountId = account.Id.Value,
                                                PlanId = request.PlanId,                    
                                                ActivationDate = request.ActivationDate,
                                                EndDate = request.EndDate,
                                                IMEINumber = request.IMEINumber,
                                                SimCard = request.SimCard,
                                                MessageBank = request.MessageBank,
                                                Data = request.Data,
                                                UsedBy = request.UsedBy,
                                                ContractNumber = contractNumber,
                                                ContractStatus = request.User is AgentUser ? 
                                                    ContractStatus.ApplicationSubmitted : ContractStatus.ApplicationPending                                                                                                
                                            };

                var agent = request.User as AgentUser;
                if (agent != null)
                {
                    contract.AgentId = agent.AgentId;
                }

                if (!contract.IsValid)
                {
                    var violation = contract.GetRuleViolations().First();
                    throw new Exception(violation.PropertyName + ": " + violation.ErrorMessage);
                }

                contractRepository.InsertContract(contract);
                if (contract.Id != null && contract.Id.Value > 0)
                {                                        
                    activityService.WriteActivity(contract, string.Format("Create new Contract with contract number '{0}' against existing account '{1}'", contractNumber, request.AccountNumber));

                    if (System.Web.HttpContext.Current != null)
                    {
                        var email = new NewContractCreatedEmailMessage {ContractId = contract.Id.Value};
                        
                        LoggingUtility.SendEmail(ConfigItems.NewContractEmailAddress, "New Contracts", "New Airtime Contact Application", email.GetHtml(), true);
                        
                        activityService.WriteActivity(contract, string.Format("Sent notification email for contract '{0}' to I Office", contractNumber));
                    }                    

                    trans.Complete();
                    response.ContractId = contract.Id.Value;
                    response.IsSuccessful = true;                   
                }                
            }  
            
            return response;
        }
    
        public ApproveInvoicingResponse ApproveInvoicing(ApproveInvoicingRequest request)
        {
            var response = new ApproveInvoicingResponse();
            // Only allow inserting of networks by I personnel
            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }            
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            
            Account account = accountRepository.GetAccount(request.AccountId);
                
            if (account == null)
            {                
                response.Message = "Account Id does not exist";
                return response;
            }
            
            if (account.BillingMethod == BillingMethod.Invoice)
            {
                response.Message = "Account is already billing by Invoice";
                return response;
            }            
            
            account.BillingMethod = request.IsApprovalGranted ? BillingMethod.Invoice : BillingMethod.CreditCard;            
            account.HasRequestedInvoicing = false;

            try
            {
                using (var ts = new TransactionScope())
                {
                    response.IsSuccessful = accountRepository.UpdateAccount(account);
                    if (response.IsSuccessful)
                    {
                        // TODO: Anything else go here?
                        if (request.IsApprovalGranted)
                        {                            
                            activityService.WriteActivity(account, string.Format("Invoicing Approved for Account '{0}'", account.AccountNumber));
                        }
                        else
                        {                            
                            activityService.WriteActivity(account, string.Format("Invoicing Denied for Account '{0}'", account.AccountNumber));
                        }
                        ts.Complete();
                        response.IsSuccessful = true;
                    }
                    else
                    {
                        response.Message = Constants.Messages.UNKNOWN_FAILURE;
                        response.IsSuccessful = false;
                    }
                }
            }
            catch (Exception ex)
            {                
                response.Message = ex.Message;
                response.IsSuccessful = false;
            }

            return response;
        }

        public ViewContractResponse ViewContract(ViewContractRequest request)
        {            
            var response = new ViewContractResponse();
            if (request.User != null)
            {
                System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            }
            else
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            #region Validate

            Contract contract = contractRepository.GetContract(request.ContractId);
            if (contract == null)
            {
                response.Message = "Could not find the associated Contract.";
                return response;
            }

            Account account = accountRepository.GetAccount(contract.AccountId);
            if (account == null)
            {
                response.Message = "Could not find the associated Account.";
                return response;
            }

            Company company = null;
            if (account is MasterAccount) 
            {
                MasterAccount master = account as MasterAccount;
                if (master.CompanyId > 0)
                {
                    company = companyRepository.GetCompany(master.CompanyId.Value);
                    if (company == null)
                    {
                        response.Message = "Could not find the associated Company.";
                        return response;
                    }
                }
            }

            #endregion 

            try
            {
                using (var ts = new TransactionScope())
                {
                    Contact contact = contactRepository.GetContactEntity(account.ContactId);
                    if (contact == null)
                    {
                        response.Message = "Could not find the associated Contact.";
                        return response;
                    }

                    // (Changed rag 13/04/2009) 001-45, 001-46.  
                    // Send notification to Agent when I starting application process.
                    if (!(request.User is AgentUser) && !(request.User is ContactUser) && 
                        contract.ContractStatus == ContractStatus.ApplicationSubmitted && contract.AgentId != null)
                    {
                        var agent = agentRepository.GetAgent(contract.AgentId.Value);
                        if (agent == null)
                        {
                             LoggingUtility.LogException(new Exception("Agent " + contract.AgentId + " could not be found"));
                            return response;
                        }

                        // Should always have an agent, if in this status.                            
                        activityService.WriteActivity(contract, "Started Application approval process.");

                        var email = new ReviewingContractEmailMessage
                                        {ContractNumber = contract.ContractNumber, ContactName = contact.Name};

                        // Send email to Agent
                        LoggingUtility.SendEmail(agent.Email, agent.AgentName, "I Contract Review", email.GetHtml(), false);

                        activityService.WriteActivity(contract, "Contract Review commenced email sent to Agent (" + agent.Email + ")");

                        contract.ContractStatus = ContractStatus.ApplicationPending;
                        if (!contractRepository.UpdateContract(contract))
                        {
                            throw new Exception("Unable to set viewing mode");
                        }
                    }

                    response.IsReadOnly = (request.User is AgentUser) || (request.User is ContactUser);
                    response.Contract = contract;
                    response.Contact = contact;
                    response.Company = company;
                    response.Account = account;
                    // TODO: Invoice pending.

                    ts.Complete();
                    response.IsSuccessful = true;
                }
            }
            catch (Exception ex)
            {                    
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;                            
        }

        public ContractSearchResponse FindContracts(ContractSearchRequest request)
        {
            ContractSearchResponse response = new ContractSearchResponse();

            if (request.User != null)
            {
                System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            }
            else
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }                        

            #region Validation 

            // (Changed rag: 15/02/2009) #001-19 Allow blank request (all results)
            //if (string.IsNullOrEmpty(request.SearchValue))
            //{
            //    response.IsSuccessful = false;
            //    response.Message = "Please enter one or more characters to search on";
            //    return response;
            //}

            #endregion 

            try
            {
                IList<ContractSearchEntity> searchResults = new List<ContractSearchEntity>();
                IList<Contract> contracts = null;
                switch (request.SearchType)
                {
                    case ContractSearchField.ContractNumber:
                        contracts = contractRepository.FindByContractNumber(request.SearchValue);
                        break;
                    case ContractSearchField.ContactName:
                        contracts = contractRepository.FindByContact(request.SearchValue);
                        break;
                    case ContractSearchField.AccountNumber:
                        contracts = contractRepository.FindByAccountNumber(request.SearchValue);
                        break;
                    case ContractSearchField.PhoneNumber:
                        contracts = contractRepository.FindByPhoneNumber(request.SearchValue);
                        break;
                }
                
                // TODO: This filtering should be done in the data layer.
                if (request.SearchContractStatusType == SearchContractStatusType.UseStatus)
                {
                    contracts = contracts.Where(c => c.ContractStatus == request.SearchStatus).ToList<Contract>();
                }
                else if (request.SearchContractStatusType == SearchContractStatusType.Active)
                {
                    contracts = contracts.Where(c => c.ContractStatus == ContractStatus.ApplicationSubmitted ||
                                                        c.ContractStatus == ContractStatus.ApplicationPending ||
                                                        c.ContractStatus == ContractStatus.Active ||
                                                        c.ContractStatus == ContractStatus.Suspended).ToList<Contract>();
                }
                
                foreach (Contract contract in contracts)
                {                    
                    ContractSearchEntity search = new ContractSearchEntity();

                    Account account = accountRepository.GetAccount(contract.AccountId);
                    Contact contact = contactRepository.GetContactEntity(account.ContactId);

                    // use primary phone number unless the search was by phone number
                    // in that case, use that phone number
                    search.PhoneNumber = contract.PhoneNumber1;
                    if (request.SearchType == ContractSearchField.PhoneNumber && !string.IsNullOrEmpty(request.SearchValue))
                    {
                        search.PhoneNumber = request.SearchValue;
                    }
                    search.ContractStatus = contract.ContractStatus;
                    search.ContractNumber = contract.ContractNumber;
                    search.AccountNumber = account.AccountNumber;
                    search.ContactName = contact.Name;
                    search.ContractId = contract.Id.Value;

                    // Get the company. if this is a sub account, get the master account first.
                    // If master is null, it must be a sub account.
                    // we only go 1 level deep, so just go up a level.
                    MasterAccount master = account as MasterAccount;
                    if (master == null)
                    {
                        // This has to cast.
                        SubAccount sub = account as SubAccount;
                        master = accountRepository.GetAccount(sub.MasterAccountId) as MasterAccount;
                    }

                    // Account can be a master account and not be associated with a company.
                    if (master.CompanyId != null)
                    {
                        Company company = companyRepository.GetCompany(master.CompanyId.Value);
                        if (company != null)
                        {
                            search.CompanyName = company.CompanyName;
                        }
                    }

                    searchResults.Add(search);
                }
                response.Contracts = searchResults;                
                response.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = ex.Message;
            }                        

            return response;
        }

        public UpdateContractResponse UpdateContract(UpdateContractRequest request)
        {            
            var response = new UpdateContractResponse();

            #region Validation 

            // Only allow updating of contracts by I personnel
            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }            
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();            

            // Mandatory fields.
            if (request.ActivationDate > request.EndDate)
            {
                response.Message = "Contract end date must be after contract activation date.";
                return response;
            }
            
            if (planRepository.GetPlan(request.PlanId) == null)
            {
                response.Message = "A Valid Plan must be supplied";
                return response;
            }
            
            // If contract has any phone numbers, the pin and puk should be supplied.
            if ((!string.IsNullOrEmpty(request.PhoneNumber1) || !string.IsNullOrEmpty(request.PhoneNumber2) ||
                        !string.IsNullOrEmpty(request.PhoneNumber3) || !string.IsNullOrEmpty(request.PhoneNumber4)) && 
                (string.IsNullOrEmpty(request.Pin) || string.IsNullOrEmpty(request.Puk)))
            {
                response.Message = "Phone Pin and Puk must be supplied";
                return response;
            }

            #endregion 

            Contract contract = contractRepository.GetContract(request.ContractId);
            if (contract == null)
            {
                // This contract should certainly exist.
                // TODO: Email support with notification of possible database corruption.
                response.Message = Constants.Messages.INTERNAL_ERROR;
                response.IsSuccessful = false;
                return response;
            }           

            try
            {
                using (var ts = new TransactionScope())
                {
                    contract.PlanId = request.PlanId;
                    contract.ActivationDate = request.ActivationDate;
                    contract.EndDate = request.EndDate;
                    contract.UsedBy = request.UsedBy;
                    contract.Pin = request.Pin;
                    contract.Puk = request.Puk;
                    contract.PhoneNumber1 = request.PhoneNumber1;
                    contract.PhoneNumber2 = request.PhoneNumber2;
                    contract.PhoneNumber3 = request.PhoneNumber3;
                    contract.PhoneNumber4 = request.PhoneNumber4;

                    var account = accountRepository.GetAccount(contract.AccountId);
                    if (account == null) return response;
                    var contact = contactRepository.GetContactEntity(account.ContactId);
                    if (contact == null) return response;
                    
                    contact.Name = request.ContactName;
                    contact.Email = request.ContactEmail;
                    contact.DriversLicense.DateOfBirth = request.DateOfBirth;
                    contact.DriversLicense.LicenseNumber = request.LicenseNumber;
                    contact.DriversLicense.Expiry = request.LicenseExpiry;
                    contact.MobilePhone = request.MobilePhoneNumber;
                    contact.WorkPhone = request.WorkNumber;
                    contact.FaxNumber = request.FaxNumber;
                    contact.HomeAddress.Street = request.HomeAddress.Street;
                    contact.HomeAddress.Suburb = request.HomeAddress.Suburb;
                    contact.HomeAddress.State = request.HomeAddress.State;
                    contact.HomeAddress.Postcode = request.HomeAddress.Postcode;
                    contact.PostalAddress.Street = request.DeliveryAddress.Street;
                    contact.PostalAddress.Suburb = request.DeliveryAddress.Suburb;
                    contact.PostalAddress.State = request.DeliveryAddress.State;
                    contact.PostalAddress.Postcode = request.DeliveryAddress.Postcode;
                    contact.CreditCard.NameOnCard = request.CreditCardName;
                    contact.CreditCard.CardNumber = request.CreditCardNumber;
                    contact.CreditCard.ExpiryDate = request.CreditCardExpiry;
                    contact.CreditCard.CreditCardType = request.CreditCardType;                                        

                    #region Contract Activation
                    // If any of the phone numbers are complete, the contract is demeed to be in an activated state
                    if ((!string.IsNullOrEmpty(contract.PhoneNumber1) || !string.IsNullOrEmpty(contract.PhoneNumber2) ||
                        !string.IsNullOrEmpty(contract.PhoneNumber3) || !string.IsNullOrEmpty(contract.PhoneNumber4)) &&
                        (int)contract.ContractStatus < (int)ContractStatus.Active)
                    {
                        contract.ContractStatus = ContractStatus.Active;                        
                        activityService.WriteActivity(contract, "Contract is now in an Active Status.");

                        // (Changed rag 13/04/2009) 001-50 - Send notification email to customer
                        if (!string.IsNullOrEmpty(contact.Email))
                        {
                            var plan = planRepository.GetPlan(contract.PlanId);
                            var message = new ActivationEmailMessage
                                              {
                                                  ContractNumber = contract.ContractNumber,
                                                  Name = contact.Name,
                                                  Paperless = account.EmailBill,
                                                  Plan = plan.Name,
                                                  PhoneNumber1 = contract.PhoneNumber1,
                                                  PhoneNumber2 = contract.PhoneNumber2,
                                                  PhoneNumber3 = contract.PhoneNumber3,
                                                  PhoneNumber4 = contract.PhoneNumber4,
                                                  Pin = contract.Pin,
                                                  PaymentMethod = account.BillingMethod,
                                                  PostBill = account.PostBill
                                              };

                            LoggingUtility.SendEmail(contact.Email, contact.Name, "I Contract Activated", message.GetHtml(), false);

                            activityService.WriteActivity(contract, "I Contract Activated email sent to '" + contact.Email + "'");                            
                        }
                    }
                    #endregion
                    
                    if (!contact.IsValid)
                    {
                        response.IsSuccessful = false;
                        response.Message = contact.GetRuleViolationsErrorString();
                        return response;
                    }

                    if (!contract.IsValid)
                    {
                        response.IsSuccessful = false;
                        response.Message = contract.GetRuleViolationsErrorString();
                        return response;
                    }

                    response.IsSuccessful = contractRepository.UpdateContract(contract) &&                                            
                                            contactRepository.UpdateContact(contact);
                    if (response.IsSuccessful)
                    {
                        response.Contract = contract;                         
                        activityService.WriteActivity(contract, "Updated contract information.");
                        ts.Complete();
                    }
                    else                    
                    {
                        // TODO: Log descriptive message
                        response.Message = Constants.Messages.INTERNAL_ERROR;                        
                    }
                }

                if (response.IsSuccessful)
                {
                    // Must do this outside the Transaction or the importManager won't see the new Phone Number!
                    // [001-101] Check for imported calls that might match this contract
                    importManager.FindCallDataForContract(contract.Id.Value);
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                // TODO : Email support with exception.
                response.Message = Constants.Messages.UNKNOWN_FAILURE;
            }

            return response;
        }

        public VaryContractPlanResponse VaryContractPlan(VaryContractPlanRequest request)
        {
            VaryContractPlanResponse response = new VaryContractPlanResponse();

            #region Validation
            // Only allow updating of contracts by I personnel
            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            else
            {
                System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            }

            if (request.StartDate > request.EndDate)
            {
                response.Message = "Variance Start Date cannot be after Variance End Date";
                return response;
            }
            if (request.EndDate < _dateTime.Now)
            {
                response.Message = "Cannot create a Plan Variance in the past";
                return response;
            }
            if (request.Cost < 0)
            {
                response.Message = "Variance Cost cannot be negative";
                return response;
            }
            Contract contract = contractRepository.GetContract(request.ContractId);
            if (contract == null)
            {
                response.Message = "Contract with Contract Id = " + request.ContractId.ToString() + " does not exist";
                return response;
            }
            Plan plan = planRepository.GetPlan(request.NewPlanId);
            if (plan == null)
            {
                response.Message = "Plan with Plan Id = " + request.NewPlanId.ToString() + " does not exist";
                return response;
            }
            #endregion 

            using (TransactionScope ts = new TransactionScope())
            {                
                int varianceId = contractRepository.InsertContractPlanVariance(contract, plan, request.StartDate, request.EndDate, request.User.Username, request.Cost);
                if (varianceId > 0)
                {                    
                    activityService.WriteActivity(contract, string.Format("Contract Plan Varied from Plan Id={0} to Plan Id={1} from {2:dd/MM/yyyy} until {3:dd/MM/yyyy}",
                            contract.PlanId, request.NewPlanId, request.StartDate, request.EndDate));

                    ts.Complete();
                    response.IsSuccessful = true;
                }
                else
                {
                    response.Message = Constants.Messages.INTERNAL_ERROR;                    
                }
            }

            return response;
        }

        public CloseContractResponse CloseContract(CloseContractRequest request)
        {            
            var response = new CloseContractResponse();

            // Only allow closing of contracts by I personnel
            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            else
            {
                System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();
            }

            var contract = contractRepository.GetContract(request.ContractId);
            if (contract == null)
            {
                response.Message = "Contact " + request.ContractId + " not found.  Cannot close.";
                return response;
            }

            try
            {
                using (var ts = new TransactionScope())
                {
                    if (contract.EndDate.Date > _dateTime.Today)
                    {
                        // Contract is closed early.
                        activityService.WriteActivity(contract, "Contract cancelled before contract end date ('" + contract.EndDate.Date.ToShortDateString() + "')");
                    }
                    else
                    {
                        activityService.WriteActivity(contract, "Contract closed");
                    }

                    contract.ContractStatus = ContractStatus.Closed;                    
                    if (contractRepository.UpdateContract(contract))
                    {
                        response.IsSuccessful = true;                        
                        ts.Complete();
                    }
                    else
                    {
                        response.IsSuccessful = false;
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        /// <summary>
        /// Gets all uninvoiced calls for a particular contract
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public GetUnInvoicedCallsResponse GetUnInvoicedCalls(GetUnInvoicedCallsRequest request)
        {
            var response = new GetUnInvoicedCallsResponse();

            var contract = contractRepository.GetContract(request.ContractId);
            if (contract == null)
            {
                response.Message = "Contract " + request.ContractId + " not found.";
                return response;
            }

            try
            {
                response.UninvoicedCalls = contractRepository.GetNonInvoicedCallsByContractId(request.ContractId);
                response.IsSuccessful = true;
                return response;
            }
            catch (Exception ex)
            {                
                LoggingUtility.LogException(request.User.Username, ex);
                response.Message = Constants.Messages.INTERNAL_ERROR;
                return response;
            }
        }

        /// <summary>
        /// Suspends a contract until the date 
        /// </summary>
        /// <param name="contract"></param>
        /// <param name="suspendedUntil"></param>
        /// <returns></returns>
        public SuspendContractResponse SuspendContract(SuspendContractRequest request)
        {
            var response = new SuspendContractResponse();

            #region Validate

            if (request.SuspendUntil < _dateTime.Now)
            {                
                response.Message = "Cannot suspend a contract until a day in the past";                
                return response;
            }

            var contract = contractRepository.GetContract(request.ContractId);
            if (contract == null)
            {                
                response.Message = "Could not find contract with contractId=" + request.ContractId.ToString();
                return response;
            }

            if (contract.ContractStatus != ContractStatus.Active)
            {                
                response.Message = "Cannot suspend a contract that is not active.  Contract is " +
                                   contract.ContractStatus.ToString();
                return response;
            }

            #endregion

            try
            {
                using (var ts = new TransactionScope())
                {
                    contract.ContractStatus = ContractStatus.Suspended;
                    contract.SuspendedUntilDate = request.SuspendUntil;
                    if (contractRepository.UpdateContract(contract))
                    {
                        // Send suspended email? - not a requirement.                    

                        contract.LogActivity(string.Format("Suspended Contract until {0}", contract.SuspendedUntilDate.Value.ToString("dd/MM/yyyy")));
                        
                        ts.Complete();

                        response.IsSuccessful = true;
                    }
                    else
                    {
                        LoggingUtility.LogDebug("SuspendContract", "ContractService",
                                                "Failed to update Contract in the database.");
                        response.IsSuccessful = false;
                        response.Message = "The suspension could not be completed.";
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(request.User.Username, ex);
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        public ResponseBase AddCustomFee(int contractId, int customFeeId)
        {
            var response = new ResponseBase();

            var customFee = _customFeeRepository.GetById(customFeeId);

            if (customFee == null) {
                response.IsSuccessful = false;
                response.Message = "Could not find Custom Fee";
                return response;
            }

            var contract = contractRepository.GetContract(contractId);

            if (contract == null) {
                response.IsSuccessful = false;
                response.Message = "Could not find Contract";
                return response;
            }

            if (ContractContainsCustomFee(contractId, customFee)) {
                response.IsSuccessful = false;
                response.Message = "This contract already contains this Custom Fee";
                return response;
            }

            if (contract.ContractStatus == ContractStatus.Closed)
            {
                response.IsSuccessful = false;
                response.Message = "Cannot add Custom Fees to closed Contracts";
                return response;
            }

            // TODO            
            try {

                if (contractRepository.AddCustomFee(contractId, customFee)) {
                    response.IsSuccessful = true;
                    contract.LogActivity(string.Format("Added Custom Fee '{0}'", customFee.Description));
                } else {
                    LoggingUtility.LogDebug("AddCustomFee", "ContractService", string.Format("Failed to add Custom Fee '{0}' to Contract '{1}'", customFee.Description, contract.ContractNumber));
                    response.IsSuccessful = false;
                    response.Message = Constants.Messages.INTERNAL_ERROR;     
                }
                
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        protected bool ContractContainsCustomFee(int contractId, CustomFee customFee)
        {
            var contracts = contractRepository.GetAllContractsWithCustomFee(customFee);

            return contracts.Any(c => c.Id == contractId);
        }

        public ResponseBase RemoveCustomFee(int contractCustomFeeId)
        {
            var response = new ResponseBase();

            try {
                if (contractRepository.RemoveCustomFee(contractCustomFeeId)) {
                    

                    response.IsSuccessful = true;
                }
                else {
                    response.IsSuccessful = false;
                    response.Message = Constants.Messages.INTERNAL_ERROR;
                }
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        public CustomFeesResponse GetCustomFeesForContract(CustomFeesRequest request)
        {
            var response = new CustomFeesResponse();
            try {
                var contract = contractRepository.GetContract(request.ContractId);
                if (contract == null) {
                    response.IsSuccessful = false;
                    response.Message = "Could not find Contract with Id=" + request.ContractId;
                    return response;
                }

                response.CustomFees = contractRepository.GetAllCustomFeesForContract(contract);
                response.IsSuccessful = true;
                
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }
            return response;
        }

        #endregion
    }   
}
