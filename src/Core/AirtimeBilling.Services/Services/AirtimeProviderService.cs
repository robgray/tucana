﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    internal class AirtimeProviderService :  ServiceBase, IAirtimeProviderService 
    {
        private IAirtimeProviderRepository _providerRepository;   

        public AirtimeProviderService() : this (
            RepositoryFactory.GetRepository<IAirtimeProviderRepository>()) { }

        public AirtimeProviderService(IAirtimeProviderRepository providerRepository)
        {
            _providerRepository = providerRepository;
        }

        public IList<AirtimeProvider> GetAirtimeProviders()
        {
            return _providerRepository.GetAirtimeProviders();
        }
       
    }
}
