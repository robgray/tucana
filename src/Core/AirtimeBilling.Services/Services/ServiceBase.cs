﻿using AirtimeBilling.Services.Interfaces;

namespace AirtimeBilling.Services.Services
{
    /// <summary>
    /// Defines behaviours common to all services.
    /// </summary>
    public abstract class ServiceBase
    {
        private IActivityLoggingService _activityLoggingService;

        protected ServiceBase() : this(ServiceFactory.GetService<IActivityLoggingService>()) { }

        protected ServiceBase(IActivityLoggingService activityLoggingService)
        {
            _activityLoggingService = activityLoggingService;   
        }

        protected bool PermitOnlyI()
        {
            return System.Threading.Thread.CurrentPrincipal != null && System.Threading.Thread.CurrentPrincipal.IsInRole("I");
        }

        protected IActivityLoggingService Activity
        {
            get { return _activityLoggingService; }
        }
    }
}
