﻿using System;
using LINQtoCSV;

namespace AirtimeBilling.Services.Services
{
    /// <summary>
    /// Contains the flattened invoice structure, ready for exporting.
    /// </summary>
    public class InvoiceDataExportLine
    {        
        [CsvColumn(FieldIndex = 1)]
        public string InvoiceNumber { get; set; }
                        
        [CsvColumn(FieldIndex = 2, OutputFormat="dd/MM/yyyy")]
        public DateTime InvoiceDate { get; set; }
        
        [CsvColumn(FieldIndex = 3)]
        public string MasterAccountNumber { get; set; }

        [CsvColumn(FieldIndex = 4)]
        public string AccountNumber { get; set; }

        [CsvColumn(FieldIndex = 5)]
        public string ContractNumber { get; set; }

        [CsvColumn(FieldIndex = 6)]
        public string Description { get; set; }

        [CsvColumn(FieldIndex = 7)]
        public string OriginatingNumber { get; set; }

        [CsvColumn(FieldIndex = 8)]
        public string CalledNumber { get; set; }

        [CsvColumn(FieldIndex = 9)]
        public string CallType { get; set; }

        [CsvColumn(FieldIndex = 10)]
        public decimal Duration { get; set; }

        [CsvColumn(FieldIndex = 11, OutputFormat = "C")]
        public decimal Cost { get; set; }                
    }
}
