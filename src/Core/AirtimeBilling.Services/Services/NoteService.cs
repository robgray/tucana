﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class NoteService : INoteService
    {
        private INoteRepository _noteRepository;
                                  
        public NoteService() : this (RepositoryFactory.GetRepository<INoteRepository>()) { }

        public NoteService(INoteRepository noteRepository)
        {
            _noteRepository = noteRepository;
        }
        
        #region INoteService Members

        public IList<Note> GetNotesFor(EntityBase entity)
        {
            return _noteRepository.GetNotesFor(entity);
        }
        

        public void SaveNote(Note note)
        {

            try {
                if (!note.Id.HasValue)
                    _noteRepository.Insert(note);
                else
                    throw new InvalidOperationException("Cannot update existing note");
            }
            catch (Exception ex) {
                LoggingUtility.LogException(ex);
                throw;
            }
        }

        #endregion
    }
}
