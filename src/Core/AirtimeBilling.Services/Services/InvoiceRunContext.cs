﻿using System;

namespace AirtimeBilling.Services.Services
{
    public class InvoiceRunContext
    {
        private readonly int _runNumber;
        private readonly DateTime _fromDate;
        private readonly DateTime _toDate;

        public InvoiceRunContext(int runNumber, DateTime fromDate, DateTime toDate)
        {            
            _runNumber = runNumber;
            _fromDate = fromDate;
            _toDate = toDate.AddDays(1).AddSeconds(-1);
        }

        public int RunNumber { get { return _runNumber;  } }

        public DateTime FromDate { get { return _fromDate; } }

        public DateTime ToDate { get { return _toDate; }}
        
    }
}
