﻿using System;
using System.Collections.Generic;
using System.IO;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using LINQtoCSV;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class InvoiceDataFileCreator
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IContractRepository _contractRepository;

        public InvoiceDataFileCreator() : this(
            RepositoryFactory.GetRepository<IAccountRepository>(), 
            RepositoryFactory.GetRepository<IContractRepository>()) 
        { }

        public InvoiceDataFileCreator(IAccountRepository accountRepository, IContractRepository contractRepository)
        {
            _accountRepository = accountRepository;
            _contractRepository = contractRepository;
        }

        /// <summary>
        /// Creates a datafile based on the supplied invoice.          
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns>The filename of the created file.</returns>
        public string CreateFile(Invoice invoice)
        {
            IDictionary<int, Account> accountCache = new Dictionary<int, Account>();
            IDictionary<int, Contract> contractCache = new Dictionary<int, Contract>();

            var exportLines = new List<InvoiceDataExportLine>();

            try
            {

                var masterAccount = _accountRepository.GetAccount(invoice.InvoiceRootAccountId);
                if (!accountCache.ContainsKey(masterAccount.Id.Value))
                {
                    accountCache.Add(masterAccount.Id.Value, masterAccount);
                }

  
                foreach (var line in invoice.Lines)
                {
                    var exportLine = new InvoiceDataExportLine
                    {
                        InvoiceNumber = invoice.InvoiceNumber,
                        InvoiceDate = invoice.InvoiceDate,
                        MasterAccountNumber = masterAccount.AccountNumber,
                        Description = line.Description,
                        Cost = line.LineAmount
                    };

                    if (line.ContractId.HasValue)
                    {
                        if (!contractCache.ContainsKey(line.ContractId.Value))
                        {
                            contractCache.Add(line.ContractId.Value, _contractRepository.GetContract(line.ContractId.Value));
                        }
                        var contract = contractCache[line.ContractId.Value];

                        if (!accountCache.ContainsKey(contract.AccountId))
                        {
                            accountCache.Add(contract.AccountId, _accountRepository.GetAccount(contract.AccountId));
                        }
                        var account = accountCache[contract.AccountId];

                        exportLine.AccountNumber = account.AccountNumber;
                        exportLine.ContractNumber = contract.ContractNumber;
                    }

                    if (line.Call != null)
                    {
                        exportLine.Duration = line.Call.Volume;
                        exportLine.OriginatingNumber = line.Call.PhoneNumber;
                        exportLine.CalledNumber = line.Call.NumberCalled;
                        exportLine.CallType = line.Call.Tariff;
                    }

                    exportLines.Add(exportLine);
                }

                return ExportLinesToFile(exportLines, invoice);
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                return null;
            }
            
            
        }

        /// <summary>
        /// Exports the given invoice data lines to file
        /// </summary>
        /// <param name="lines">Lines to export</param>
        /// <param name="invoice"></param>
        /// <returns>The filename of the exported file</returns>
        private string ExportLinesToFile(IEnumerable<InvoiceDataExportLine> lines, Invoice invoice)
        {
            // Now export to something or other.
            var filename = string.Format(@"{0}\{1}.csv", ConfigItems.InvoicesFolder, invoice.InvoiceNumber);

            try {
                if (File.Exists(filename)) File.Delete(filename);

                var fileDescription = new CsvFileDescription
                                          {
                                              SeparatorChar = ',',
                                              FirstLineHasColumnNames = true,
                                              FileCultureName = "en-AU"
                                          };

                var context = new CsvContext();
                context.Write(lines, filename, fileDescription);
                return filename;

            } catch (Exception ex) {                
                LoggingUtility.LogException(ex);
                return null;
            }            
        }
    }
}
