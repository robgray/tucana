﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class LogReaderService : ILogReaderService 
    {
        private ILogRepository _logRepository;

        public LogReaderService() : this(RepositoryFactory.GetRepository<ILogRepository>()) { }

        public LogReaderService(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        #region ILogReaderService Members

        public LogEntry ReadLogEntry(int id)
        {
            return _logRepository.GetById(id);
        }

        public IList<LogEntry> Find(DateTime fromDate, DateTime toDate)
        {
            return _logRepository.Find(fromDate, toDate, null, null);
        }

        public IList<LogEntry> Find(DateTime fromDate, DateTime toDate, string username)
        {
            return _logRepository.Find(fromDate, toDate, username, null);
        }

        public IList<LogEntry> Find(DateTime fromDate, DateTime toDate, string username, LoggingLevels level)
        {
            return _logRepository.Find(fromDate, toDate, username, level);
        }

        #endregion
    }
}
