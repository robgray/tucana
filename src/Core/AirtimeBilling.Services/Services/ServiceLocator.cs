﻿using System.Collections;
using AirtimeBilling.Common;
using AirtimeBilling.Services.Interfaces;

namespace AirtimeBilling.Services.Services
{
    /// <summary>
    /// Factory class for creating services
    /// </summary>
    public static class ServiceFactory
    {
        private readonly static Hashtable services = new Hashtable();

        static ServiceFactory()
        {
            // register all services            
            services.Add(typeof(INumberGeneratorService), new NumberGeneratorService());
            services.Add(typeof(IActivityLoggingService), new ActivityLoggingService());

            services.Add(typeof(IContractService), new ContractService());            
            services.Add(typeof(IAccountService), new AccountService());
            services.Add(typeof(IAgentService), new AgentService());
            services.Add(typeof(IAirtimeProviderService), new AirtimeProviderService());
            services.Add(typeof(ICompanyService), new CompanyService());
            services.Add(typeof(IContactService), new ContactService());
            services.Add(typeof(INetworkService), new NetworkService());            
            services.Add(typeof(IPlanService), new PlanService());            
            services.Add(typeof(IUserService), new UserService());                        
            services.Add(typeof(IDateTimeFacade), new DateTimeFacade());            
            services.Add(typeof(IEwayPaymentsService), new EwayPaymentsService());
            services.Add(typeof(ILogReaderService), new LogReaderService());
            services.Add(typeof(INoteService), new NoteService());
            services.Add(typeof(IScrubbingService), new ScrubbingService());

            // Put at bottom.  Beware: services that depend on other services must be registered after those services!
            services.Add(typeof(IInvoicingService), new InvoicingService());
        }

        /// <summary>
        /// Retrieves the appropriate service implementing the interface specified by T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static T GetService<T>() 
        {
            return (T)services[typeof(T)];
        }
    }
}
