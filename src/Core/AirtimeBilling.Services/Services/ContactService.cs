﻿using System;
using System.Collections.Generic;
using System.Transactions;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    internal class ContactService : IContactService
    {
        private IContactRepository _contactRepository;

        public ContactService() : this(
            RepositoryFactory.GetRepository<IContactRepository>()) { }

        public ContactService(IContactRepository contactRepository)
        {
            _contactRepository = contactRepository;
        }

        public Contact GetContact(int contactId)
        {
            return _contactRepository.GetContactEntity(contactId);
        }

        public IList<Contact> GetAllContacts()
        {
            return _contactRepository.GetAllContacts();
        }

        public IList<Contact> FindContacts(ContactSearch searchType, string searchValue)
        {
            try {
                if (searchType == ContactSearch.Name)
                    return _contactRepository.FindContactsByContactName(searchValue);

                return searchType == ContactSearch.MobilePhone
                           ? _contactRepository.FindContactsByMobilePhone(searchValue)
                           : new List<Contact>();
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                throw;
            }            
        }

        public SaveContactResponse SaveContact(SaveContactRequest request)
        {
            var response = new SaveContactResponse();
            if (request.User == null || request.User is AgentUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            // TOOD: move validation to a single source.
            // Validation is cut and pasted from IContractService.SubmitContract.
            if (request.Contact == null)
            {
                throw new Exception("No Contact has been supplied.");
            }

            if (string.IsNullOrEmpty(request.Contact.Name))
            {
                response.Message = "No contact name has been supplied.";
                return response;
            }

            if (request.Contact.PostalAddress == null)
            {
                throw new Exception("No Postal Address was supplied.");
            }

            if (string.IsNullOrEmpty(request.Contact.PostalAddress.State) ||
                string.IsNullOrEmpty(request.Contact.PostalAddress.Suburb) ||
                string.IsNullOrEmpty(request.Contact.PostalAddress.Postcode) ||
                string.IsNullOrEmpty(request.Contact.PostalAddress.State))
            {
                response.Message = "Please ensure postal address details have been filled out.\r" +
                    "Including street, suburb, state, and postcode";
                return response;
            }

            try
            {
                using (var ts = new TransactionScope())
                {
                    if (request.Contact.Id == null)
                    {
                        _contactRepository.InsertContact(request.Contact);
                        if (request.Contact.Id != null)
                        {
                            request.Contact.LogActivity("New Contact added");                            
                            response.IsSuccessful = true;
                            ts.Complete();
                        }
                        else
                        {
                            response.Message = Constants.Messages.INTERNAL_ERROR;
                        }
                    }
                    else
                    {
                        if (_contactRepository.UpdateContact(request.Contact))
                        {
                            request.Contact.LogActivity("Updated Contact information");                            
                            response.IsSuccessful = true;
                            ts.Complete();
                        }
                        else
                        {
                            response.Message = Constants.Messages.INTERNAL_ERROR;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //  TODO: Log Exception  
                LoggingUtility.LogException(request.User.Username, ex);
                response.Message = Constants.Messages.INTERNAL_ERROR;
                response.IsSuccessful = false;
            }

            return response;
        }
    }
}
