﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.EmailMessages;
using AirtimeBilling.Services.Interfaces;
using System.Transactions;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class InvoicingService : IInvoicingService
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IPlanRepository _planRepository;
        private readonly INetworkRepository _networkRepository;
        private readonly IContractRepository _contractRepository;
        private readonly IContactRepository _contactRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly INumberGeneratorService _numberGeneratorService;
        private readonly ISettingRepository _settingRepository;
        private readonly ICustomFeeRepository _customFeeRepository;
        private readonly IContractService _contractService;
        private readonly IDateTimeFacade _dateTime;
        private readonly IInvoiceSettings _invoiceSettings;
        private readonly IEwayPaymentsService _ewayPaymentsService;
        private readonly IInvoiceHardcopyRepository _invoiceHardcopyRepository;

        private static bool runInProgress = false;

        public InvoicingService() :
            this(
            RepositoryFactory.GetRepository<IInvoiceRepository>(),
            RepositoryFactory.GetRepository<IAccountRepository>(),
            RepositoryFactory.GetRepository<IPlanRepository>(),
            RepositoryFactory.GetRepository<INetworkRepository>(),
            RepositoryFactory.GetRepository<IContractRepository>(),
            RepositoryFactory.GetRepository<IContactRepository>(),
            RepositoryFactory.GetRepository<ICompanyRepository>(),
            ServiceFactory.GetService<INumberGeneratorService>(),
            RepositoryFactory.GetRepository<ISettingRepository>(),
            RepositoryFactory.GetRepository<ICustomFeeRepository>(),
            RepositoryFactory.GetRepository<IInvoiceHardcopyRepository>(),
            ServiceFactory.GetService<IContractService>(),
            new InvoiceSettings(),
            ServiceFactory.GetService<IEwayPaymentsService>(),
            new DateTimeFacade()
        ) { }

        public InvoicingService(IInvoiceRepository invoiceRepository, IAccountRepository accountRepository, 
                                IPlanRepository planRepository, INetworkRepository networkRepository, IContractRepository contractRepository, IContactRepository contactRepository,
                                ICompanyRepository companyRepository, INumberGeneratorService numberGeneratorService, 
                                ISettingRepository settingRepository, ICustomFeeRepository customFeeRepository, IInvoiceHardcopyRepository hardcopyRepository, IContractService contractService,
                                IInvoiceSettings invoiceSettings, IEwayPaymentsService ewayPaymentsService, IDateTimeFacade dateTime)
        {            
            _invoiceRepository = invoiceRepository;
            _accountRepository = accountRepository;
            _planRepository = planRepository;
            _networkRepository = networkRepository;
            _contractRepository = contractRepository;
            _contactRepository = contactRepository;
            _companyRepository = companyRepository;
            _numberGeneratorService = numberGeneratorService;
            _settingRepository = settingRepository;
            _contractService = contractService;            
            _customFeeRepository = customFeeRepository;
            _invoiceHardcopyRepository = hardcopyRepository;
            _invoiceSettings = invoiceSettings;
            _ewayPaymentsService = ewayPaymentsService;            
            _dateTime = dateTime;
        }
        
        #region IInvoicingService Members

        public IInvoiceSettings InvoiceSettings
        {
            get { return _invoiceSettings; }
        }
        

        
        public int InvoiceRun(DateTime callCutoffDate)
        {
            if (runInProgress)
            {
                throw new InvoiceRunInProgressException();
            }
            runInProgress = true;

            try
            {            
                // SRD-PCMS-40 (001-62) Only invoice once per month
                DateTime? lastInvoiceDate = _invoiceSettings.LastInvoiceRunPerformedDate;
                if (lastInvoiceDate != null && lastInvoiceDate.Value.Month == callCutoffDate.Month && lastInvoiceDate.Value.Year == callCutoffDate.Year)
                {
                    LoggingUtility.LogDebug("InvoiceRun", "AirtimeBilling.Services.InvoicingService",
                                            string.Format(
                                                "Attempted Invoice Run more than once in the same month. Last Run Date={0}.  Current Date={1}",
                                                lastInvoiceDate.Value, _dateTime.Now));

                    throw new InvoiceRunFrequencyException();
                }

                int runNumber = 0;
                using (var ts = new TransactionScope())
                {
                    var rootAccounts = _accountRepository.GetAllRootAccounts();                           
                    if (rootAccounts.Count == 0)
                    {
                        LoggingUtility.LogDebug("InvoiceRun", "InvoicingService",
                                                "An Invoice Run was Started but no accounts were found.");
                        return 0;
                    }

                    runNumber = _numberGeneratorService.NextInvoiceRunNumber();

                    DateTime fromDate = DateTime.MinValue;
                    if (lastInvoiceDate.HasValue)
                    {
                        fromDate = lastInvoiceDate.Value.AddDays(1);
                    }

                    var context = new InvoiceRunContext(runNumber, fromDate, callCutoffDate);
                    var factory = new InvoiceFactory(_invoiceSettings, _dateTime, _contractRepository,
                                                                _accountRepository,
                                                                _planRepository, _networkRepository, _contractService, _companyRepository,
                                                                _contactRepository,
                                                                _numberGeneratorService, context);

                    foreach (var rootAccount in rootAccounts)
                    {
                        var invoice = factory.GenerateInvoice(rootAccount);
                        if (invoice == null) continue;

                        _invoiceRepository.InsertInvoice(invoice);
                        if (invoice.Id == null)
                        {
                            // TODO Return failure reason and log.
                            return 0;
                        }

                        rootAccount.LogActivity("Invoice Number '" + invoice.InvoiceNumber + "' created in Invoice Run " + runNumber);

                        rootAccount.AmountPaid = 0;
                        rootAccount.PreviousBill = invoice.CurrentBill;

                        if (!_accountRepository.UpdateAccount(rootAccount)) {
                            return 0;
                        }                        
                    }

                    _invoiceSettings.LastInvoiceRunPerformedDate = callCutoffDate;
                    
                    ts.Complete();
                }

                // Report generation times out the transaction.  
                // Report generation doesn't participate in the ambient transaction so 
                // there's no real reason to do it in the transaction.
                var invoices = _invoiceRepository.FindInvoicesByRunNumber(runNumber);
                foreach (var invoice in invoices)
                    _invoiceHardcopyRepository.Save(invoice);

                // SRD-PCMS-54 (001-76) Invoice data file emailed
                // Now make a manual process to cope with credit cards.
                //SendCustomerInvoiceInEmail(runNumber);                
                
                return runNumber;
            }            
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                throw;
                //return 0;
            }
            finally
            {
                runInProgress = false;
            }
        }

       
        /// <summary>
        /// Finds all invoices given an invoice run number.      
        /// </summary>
        /// <param name="runNumber">Invoice Run Number</param>
        /// <returns>A summary of all invoices with matching Run Number</returns>
        /// <remarks>
        /// Each invoice run has a run number, so finding all invoices with a particular run number
        /// Finds all invoices calculated in the same batch.
        /// </remarks>
        public IList<InvoiceSearchItem> FindInvoicesByRunNumber(int runNumber)
        {
            IList<InvoiceSearchItem> items = new List<InvoiceSearchItem>();

            var results = _invoiceRepository.FindInvoicesByRunNumber(runNumber);
            
            foreach (var item in results)
            {
                var searchItem = new InvoiceSearchItem
                                     {
                                         InvoiceHeaderId = item.Id.Value,
                                         InvoiceNumber = item.InvoiceNumber,
                                         InvoiceDate = item.ToDate,
                                         To = item.To,
                                         RunNumber = item.InvoiceRunNumber,
                                         CanBeEmailed = !string.IsNullOrEmpty(item.Email)
                                    };

                items.Add(searchItem);
            }

            return items;            
        }

       /// <summary>
        /// Finds invoices given a particular partial invoice number.  
        /// </summary>
        /// <param name="invoiceNumber">The left most characters of the invoice number.</param>
        /// <returns>A list of all invoices with matching invoice number fragment.</returns>
        public IList<InvoiceSearchItem> FindInvoicesByInvoiceNumber(string invoiceNumber)
        {
            IList<InvoiceSearchItem> items = new List<InvoiceSearchItem>();

            var results = _invoiceRepository.FindInvoicesByInvoiceNumber(invoiceNumber);
            foreach (var item in results)
            {
                var searchItem = new InvoiceSearchItem
                                     {
                                        InvoiceHeaderId = item.Id.Value,
                                        InvoiceNumber = item.InvoiceNumber,
                                        InvoiceDate = item.ToDate,
                                        To = item.To,
                                        RunNumber = item.InvoiceRunNumber,
                                    };

                items.Add(searchItem);
            }
            return items;
            
        }

        /// <summary>
        /// Retrieve all invoice runs from the system.        
        /// </summary>
        /// <returns>List of all invoice runs</returns>
        /// <remarks>
        /// This returns a list of all Invoice Runs performed, include the date the run was performed.
        /// </remarks>
        public IList<InvoiceRun> GetAllInvoiceRuns()
        {
            IList<Invoice> invoices = _invoiceRepository.GetAllInvoices();
            IList<InvoiceRun> runs = new List<InvoiceRun>();

            var groups = invoices.GroupBy(i => i.InvoiceRunNumber);
            
            foreach (var group in groups)            
            {
                // Every invoice with the same run number will have the same invoice date, 
                // as they're all invoiced at the same time (same run).
                // We can use the first invoice (there must be one or the group would not exist)
                // to get the Invoice Date.
                var run = new InvoiceRun 
                            {
                                RunNumber = group.Key,
                                RunDate = group.ElementAt(0).ToDate,
                                NumberOfInvoices = group.Count()
                            };
                runs.Add(run);                
            }

            return runs;
        }

        #endregion
     
        public void EmailInvoice(string invoiceNumber)
        {
            var invoice = _invoiceRepository.GetAllInvoices().FirstOrDefault(i => i.InvoiceNumber == invoiceNumber && !i.HasFailedPayment);
            if (invoice != null) {
                EmailInvoice(invoice, true);
            }
        }

        /// <summary>
        /// Save invoice as PDF to file system
        /// </summary>
        /// <param name="invoice">Invoice to save</param>
        public string SaveInvoiceHardcopy(Invoice invoice)
        {
            try {

                return _invoiceHardcopyRepository.Save(invoice);
            }
            catch (Exception ex) {
                LoggingUtility.LogException(ex);
            }
            return null;
        }

        protected string GetInvoiceHardcopyFilename(Invoice invoice)
        {
            return _invoiceHardcopyRepository.Get(invoice);                
        }

        protected void EmailInvoice(Invoice invoice, bool overrideRestrictions)
        {
            try
            {
                if (invoice == null) {
                    return;
                }

                var rootAccount = _accountRepository.GetAccount(invoice.InvoiceRootAccountId);
                if (rootAccount == null) {
                    LoggingUtility.LogDebug("EmailInvoice", "AirtimeBilling.Service.InvoicingService",
                                            "Could not find a root account for this invoice");
                    return;
                }
                
                var invoiceFile = SaveInvoiceHardcopy(invoice);
                
                IList<string> emailAttachmentFiles = new List<string>();

                // SRD-PCMS-51 (001-73) Invoice pdf and emailed to customer      
                if ((rootAccount.EmailBill && !string.IsNullOrEmpty(invoice.Email)) || overrideRestrictions)
                {
                    emailAttachmentFiles.Add(invoiceFile);
                }

                // SRD-PCMS-54 (001-76) Invoice export file emailed
                if ((rootAccount.EmailBillDataFile && !string.IsNullOrEmpty(invoice.Email)) || overrideRestrictions)
                {
                    var creator = new InvoiceDataFileCreator();
                    var exportFilename = creator.CreateFile(invoice);

                    if (!string.IsNullOrEmpty(exportFilename))
                    {
                        emailAttachmentFiles.Add(exportFilename);
                    }
                }

                if (!string.IsNullOrEmpty(invoice.Email) && emailAttachmentFiles.Count > 0)
                {
                    // Get the contact for the InvoiceRoot.
                    var contact = GetContactForInvoice(invoice);

                    var emailMsg = new InvoiceEmailMessage
                    {
                        InvoiceDate = invoice.InvoiceDate,
                        InvoiceNumber = invoice.InvoiceNumber,
                        Name = contact.Name
                    };

                    LoggingUtility.SendEmailWithAttachments(invoice.Email, invoice.To,
                                                            "I Airtime Invoice " + invoice.InvoiceNumber,
                                                            emailMsg,
                                                            false, emailAttachmentFiles);

                    // SRD-PCMS-52 (001-74) Invoice emailed to Customer Activity.
                    var emailMsgText = string.Format("Sent Invoice '{0}' via Email.", invoice.InvoiceNumber);
                    contact.LogActivity(emailMsgText);
                    rootAccount.LogActivity(emailMsgText);
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
            }
        }

        protected void EmailInvoice(Invoice invoice)
        {
            EmailInvoice(invoice, false);            
        }

        // SRD-PCMS-54 (001-76) Invoice data file emailed
        public void SendInvoicesInEmail(int invoiceRunNumber)
        {
            var invoices = _invoiceRepository.FindInvoicesByRunNumber(invoiceRunNumber);
            if (invoices.Count == 0) return;
                          
            foreach (var invoice in invoices)
            {
               EmailInvoice(invoice);
            }                            
        }

        /// <summary>
        /// Finds the Contact for the Account that is the InvoiceRoot for this invoice 
        /// </summary>        
        private Contact GetContactForInvoice(Invoice invoice)
        {
            if (invoice == null) return null;

            var rootAccount = _accountRepository.GetAccount(invoice.InvoiceRootAccountId);
            return _contactRepository.GetContactEntity(rootAccount.ContactId);
            
        }

        private void WriteActivity(EntityBase entity, string message)
        {
            LoggingUtility.WriteActivity(entity.GetEntityName(), entity.Id.Value, message);
        }
       
        /// <summary>
        /// void ExportCreditCardChargeFile(int runNumber);                
        /// </summary>        
        /// <returns>A string representation of the eway file</returns>
        public string ExportCreditCardChargeFile(int runNumber)
        {
            return _ewayPaymentsService.ExportCreditCardChargeFile(runNumber);
        }
        
        public IList<CustomFee> GetAllCustomFees()
        {
            return _customFeeRepository.GetAll();            
        }

        public CustomFee GetCustomFee(int customFeeId)
        {
            return _customFeeRepository.GetById(customFeeId);
        }

        public ResponseBase SaveCustomFee(CustomFee customFee)
        {
            var response = new ResponseBase();

            try {
                if (customFee.Id.HasValue) {
                    _customFeeRepository.Update(customFee);                    
                } else {
                    _customFeeRepository.Insert(customFee);
                    if (!customFee.Id.HasValue)
                    {
                        response.IsSuccessful = false;
                        response.Message = "Addition of new Custom Fee has failed";
                        return response;
                    }
                    
                }
                response.IsSuccessful = true;                
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = "Could not " + (customFee.Id.HasValue ? "add" : "update") + " the custom fee";
            }
            return response;
        }

        public ResponseBase RemoveCustomFee(CustomFee customFee)
        {
            var response = new ResponseBase();

            // Check no contracts are using this fee.
            var contracts = _contractRepository.GetAllContractsWithCustomFee(customFee);
            if (contracts.Count > 0) {
                response.IsSuccessful = false;
                response.Message = "Cannot delete Custom Fees still associated with contracts";
                return response;
            }

            try {
                _customFeeRepository.Delete(customFee);
                response.IsSuccessful = true;
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = "Could not remove the custom fee";
            }
            return response;
        }              
        
        public void ImportCreditCardChargeFile(string csvFile, string filename)
        {
            _ewayPaymentsService.ImportCreditCardChargeFile(csvFile, filename);
        }

        public bool HasContractBeenInvoiced(Contract contract)
        {
            var invoices = _invoiceRepository.GetAllInvoices();
            
            foreach (var invoice in invoices) {

                var count = invoice.Lines.Count(line => line.ContractId == contract.Id);
                if (count > 0)
                    return true;
            }

            return false;
        }     
    }
}
