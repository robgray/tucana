﻿using System;

namespace AirtimeBilling.Services.Services
{
    public interface IInvoiceSettings
    {
        string ContractSuspensionDescription { get; }

        string FreeCallComponentDescription { get; }

        string EnvironmentalLevyDescription { get; }

        string NetworkAccessFeeDescription { get; }

        DateTime? LastInvoiceRunPerformedDate { get; set; }

        int LastInvoiceRunNumber { get; }

        decimal ContractSuspensionFee { get; }

        decimal EnvironmentalLevy { get; }
    }
}
