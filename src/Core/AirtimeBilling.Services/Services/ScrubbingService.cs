﻿using System;
using System.Transactions;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    /// <summary>
    /// Scrubbing service handles purging of data.  
    /// Normally data should not be purged and this separates the purging 
    /// from normal business activities.
    /// </summary>
    public class ScrubbingService : IScrubbingService 
    {        
        private IInvoicingService _invoicingService;
        private readonly IContractRepository _contractRepository;        
        private readonly Import.ICallDataImportManager _importManager;
        private readonly IAccountRepository _accountRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        
        public ScrubbingService() : this(ServiceFactory.GetService<IInvoicingService>(),                                         
                                        RepositoryFactory.GetRepository<IContractRepository>(),
                                        new Import.ImporterManager(),
                                        RepositoryFactory.GetRepository<IAccountRepository>(), 
                                        RepositoryFactory.GetRepository<IInvoiceRepository>()) { }

        public ScrubbingService(IInvoicingService invoicingService, IContractRepository contractRepository,                                
                                Import.ICallDataImportManager importManager, IAccountRepository accountRepository,
                                IInvoiceRepository invoiceRepository)
        {
            _invoicingService = invoicingService;
            _contractRepository = contractRepository;            
            _importManager = importManager;
            _accountRepository = accountRepository;
            _invoiceRepository = invoiceRepository;
        }

        #region IScrubbingService Members

        public ResponseBase ScrubContract(Contract contract)
        {
            if (_invoicingService == null) _invoicingService = ServiceFactory.GetService<IInvoicingService>(); // yuk hack            

            var response = new ResponseBase();
            if (_invoicingService.HasContractBeenInvoiced(contract)) {
                response.IsSuccessful = false;
                response.Message = "This Contract has been invoiced.  Cannot scrub data.";

                return response;
            }

            try 
            {
                LoggingUtility.LogDebug("ScrubContract", "ScrubbingService", string.Format("Scrubbing Contract Number {0}.  Phone 1: {1}", contract.ContractNumber, contract.PhoneNumber1));

                response.IsSuccessful = _contractRepository.Delete(contract);
                _importManager.RescanForContracts();

                LoggingUtility.LogDebug("ScrubContract", "ScrubbingService", string.Format("Scrubbing completed for Contract Number {0}.  Phone 1: {1}", contract.ContractNumber, contract.PhoneNumber1));
            
            } catch (Exception ex) {
                
                LoggingUtility.LogException(ex);
                LoggingUtility.LogDebug("ScrubContract", "ScrubbingService", string.Format("Scrubbing Failed for Contract Number {0}.  Phone 1: {1}", contract.ContractNumber, contract.PhoneNumber1));
            }

            return response;
        }

        public ResponseBase CanScrubCustomer(Account account)
        {
            var response = new ResponseBase();

            // Does account have more than one contract?
            var contracts = _contractRepository.GetContractsByAccountId(account.Id.Value);

            var master = account as MasterAccount;
            if (master != null)
            {
                var subAccounts = _accountRepository.GetAllSubAccounts(master.Id.Value);
                if (subAccounts.Count > 0)
                {
                    response.IsSuccessful = false;
                    response.Message = "This Account has sub-accounts.  Cannot Delete";
                    return response;
                }
            } else {
                response.IsSuccessful = false;
                response.Message = "This Account is a sub-account. Cannot Delete";
            }

            if (contracts.Count > 1)
            {
                response.IsSuccessful = false;
                response.Message = "This Customer has multiple Contracts.  Cannot Delete.";
                return response;
            }

            response.IsSuccessful = true;
            return response;
        }

        /// <summary>
        /// Delete's all evidence of a Customer having been in the sytem.
        /// </summary>
        /// <param name="account">Single account to delete</param>        
        public ResponseBase ScrubCustomer(Account account)
        {
            if (_invoicingService == null) _invoicingService = ServiceFactory.GetService<IInvoicingService>(); // yuk hack

            var response = new ResponseBase();

            // Does account have more than one contract?
            var contracts = _contractRepository.GetContractsByAccountId(account.Id.Value);

            if (contracts.Count == 0)
            {
                response.IsSuccessful = false;
                return response;
            }

            response = CanScrubCustomer(account);
            if (!response.IsSuccessful) return response;

            // Delete Invoices
            if (!DeleteInvoices(account)) {
                response.IsSuccessful = false;
                response.Message = "Could not delete Invoices";
                return response;
            }
            

            // Call normal Scrub Contract
            return ScrubContract(contracts[0]);
        }
       
        #endregion

        private bool DeleteInvoices(Account account)
        {
            using (var ts = new TransactionScope()) {
                var invoices = _invoiceRepository.GetAllInvoiceForAccount(account);

                foreach (var invoice in invoices) {

                    var success = _invoiceRepository.DeleteInvoice(invoice);
                    if (!success) {
                        return false;
                    }

                }

                ts.Complete();
                return true;
            }
        }
    }
}
