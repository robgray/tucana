﻿using System;
using System.Collections.Generic;
using System.Transactions;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    internal class CompanyService : ServiceBase, ICompanyService
    {
        private ICompanyRepository _companyRepository;

        public CompanyService() : this(
            RepositoryFactory.GetRepository<ICompanyRepository>()) { }

        public CompanyService(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }


        public Company GetCompany(int companyId)
        {
            return _companyRepository.GetCompany(companyId);
        }

        public IList<Company> FindCompanies(CompanySearch searchType, string searchValue)
        {
            switch (searchType)
            {
                case CompanySearch.CompanyName:
                    return _companyRepository.FindCompaniesByCompanyName(searchValue);
                case CompanySearch.ABN:
                    return _companyRepository.FindCompaniesByABN(searchValue);
                default:
                    return new List<Company>();
            }
        }

        public SaveCompanyResponse SaveCompany(SaveCompanyRequest request)
        {
            var response = new SaveCompanyResponse();

            #region Validation

            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            if (string.IsNullOrEmpty(request.Company.CompanyName))
            {
                response.Message = "Company Name cannot be blank";
                return response;
            }

            #endregion

            try
            {
                using (var ts = new TransactionScope())
                {
                    if (request.Company.Id == null)
                    {
                        _companyRepository.InsertCompany(request.Company);
                        if (request.Company.Id != null)
                        {
                            request.Company.LogActivity("New Company added");                            
                            response.IsSuccessful = true;
                            ts.Complete();
                        }
                        else
                        {
                            response.Message = Constants.Messages.INTERNAL_ERROR;
                        }
                    }
                    else
                    {
                        if (_companyRepository.UpdateCompany(request.Company))
                        {                            
                            request.Company.LogActivity("Updated Company with new information");
                            response.IsSuccessful = true;
                            ts.Complete();
                        }
                        else
                        {
                            response.Message = Constants.Messages.INTERNAL_ERROR;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                response.Message = Constants.Messages.INTERNAL_ERROR;
                response.IsSuccessful = false;
            }

            return response;
        }
    }
}
