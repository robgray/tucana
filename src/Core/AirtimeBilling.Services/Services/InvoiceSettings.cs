﻿using System;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class InvoiceSettings : IInvoiceSettings 
    {
        private readonly ISettingRepository _settingRepository;

        public InvoiceSettings() : this(
            RepositoryFactory.GetRepository<ISettingRepository>()) 
        { }

        public InvoiceSettings(ISettingRepository settingRepository)
        {
            _settingRepository = settingRepository;
        }

        #region IInvoiceSettings Members

        public string ContractSuspensionDescription
        {
            get { return "Contract Suspension Fee"; }
        }

        public string FreeCallComponentDescription
        {
            get { return "Less free call component"; }
        }

        public string EnvironmentalLevyDescription
        {
            get { return "Environmental Levy"; }
        }

        public string NetworkAccessFeeDescription
        {
            get { return "Network Access Fee"; }
        }

        public DateTime? LastInvoiceRunPerformedDate 
        { 
            get
            {
                var setting = _settingRepository["LastInvoicedDate"];
                if (setting != null)
                {
                    return DateTime.Parse(setting.Value);
                }
                return null;
            }
            set
            {
                try
                {
                    var setting = _settingRepository["LastInvoicedDate"];
                    if (setting != null)
                    {
                        if (value.HasValue) {
                            setting.Value = value.ToString();
                        } else
                            setting.Value = string.Empty;

                        _settingRepository.Commit(setting);
                    }
                    else
                    {
                        throw new Exception("LastInvoicedDate setting is not present in SystemSettings");
                    }
                }
                catch
                {
                    // TODO Log exception
                    throw new Exception("Failed to update Last Invoiced Date");
                }            
            }
        }
        
        public int LastInvoiceRunNumber
        {
            get
            {
                var setting = _settingRepository["NextInvoiceRunNumber"];
                if (setting != null)
                {
                    return Convert.ToInt32(setting.Value) - 1;
                }
                else
                    return 0;
            }
        }

        public decimal ContractSuspensionFee
        {
            get
            {
                var setting = _settingRepository["ContractSuspensionFee"];
                if (setting != null)
                {
                    return setting.Value.ToDecimalCurrency();
                }
                throw new MissingFieldException("ContractSuspensionFee setting Is missing");
            }
        }

        public decimal EnvironmentalLevy
        {
            get
            {
                var setting = _settingRepository["EnvironmentalLevy"];
                if (setting != null)
                {
                    return setting.Value.ToDecimalCurrency();
                }
                throw new MissingFieldException("EnvironmentalLevy setting Is missing");
            }


        }

        #endregion
    }
}
