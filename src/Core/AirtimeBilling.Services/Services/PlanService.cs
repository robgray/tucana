﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Import;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;
using IContractRepository = AirtimeBilling.Core.Repositories.IContractRepository;
using IPlanRepository = AirtimeBilling.Core.Repositories.IPlanRepository;
using Plan = AirtimeBilling.Core.Entities.Plan;

namespace AirtimeBilling.Services.Services
{
    internal class PlanService : ServiceBase, IPlanService 
    {
        private IPlanRepository _planRepository;
        private IPlanTariffRepository _planTariffRepository;
        private IContractRepository _contractRepository;
        private INetworkRepository _networkRepository;
        private INetworkTariffRepository _networkTariffRepository;

        public PlanService() : this(
            RepositoryFactory.GetRepository<IPlanRepository>(),
            RepositoryFactory.GetRepository<IPlanTariffRepository>(),
            RepositoryFactory.GetRepository<IContractRepository>(),
            RepositoryFactory.GetRepository<INetworkRepository>(),
            RepositoryFactory.GetRepository<INetworkTariffRepository>()) { }
            

        public PlanService(IPlanRepository planRepository, IPlanTariffRepository planTariffRepository, IContractRepository contractRepository, INetworkRepository networkRepository, INetworkTariffRepository networkTariffRepository)
        {
            _planRepository = planRepository;
            _planTariffRepository = planTariffRepository;
            _contractRepository = contractRepository;
            _networkTariffRepository = networkTariffRepository;
            _networkRepository = networkRepository;
        }

        public IList<Plan> GetPlansByNetworkId(int networkId)
        {
            return _planRepository.GetPlansByNetworkId(networkId);
        }

        public Plan GetPlan(int planId)
        {
            return _planRepository.GetPlan(planId);
        }

        public InsertPlanResponse InsertPlan(InsertPlanRequest request)
        {
            var response = new InsertPlanResponse();
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            #region Validation

            if (!PermitOnlyI())
            {
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            // Only allow inserting of plans by I personnel               
            var validationErrors = ValidateNewPlan(request.Plan);
            if (validationErrors.Count > 0)
            {
                response.Message = validationErrors[0];
                return response;
            }

            #endregion

            // Creating a new plan involves:
            // 1. Creating the new plan.
            // 2. Create a plan tariff for each network tariff that exists for the network of this plan           
            // 3. Setting the default tariff for the plan.
            using (var ts = new TransactionScope())
            {
                _planRepository.InsertPlan(request.Plan);
                if (request.Plan.Id == null)
                {
                    response.IsSuccessful = false;
                    return response;
                }

                var networkTariffs = _networkTariffRepository.GetNetworkTariffsByNetworkId(request.Plan.NetworkId);
                foreach (var netTariff in networkTariffs)
                {
                    var tariff = new PlanTariff(netTariff.Id.Value)
                    {
                        UnitCost = 0,
                        IsCountedInFreeCall = netTariff.IsCountedInFreeCall,
                        PlanId = request.Plan.Id.Value
                    };
                    _planTariffRepository.InsertPlanTariff(tariff);
                    if (tariff.Id == null)
                    {
                        response.IsSuccessful = false;
                        return response;
                    }

                    if (tariff.NetworkTariffId != request.DefaultNetworkTariffId) continue;
                    request.Plan.DefaultTariffId = tariff.Id.Value;

                    if (!request.Plan.IsValid)
                    {
                        response.Message = request.Plan.GetRuleViolations().First().ErrorMessage;
                        return response;
                    }

                    if (_planRepository.UpdatePlan(request.Plan)) continue;
                    response.IsSuccessful = false;
                    return response;
                }

                ts.Complete();
                response.IsSuccessful = true;
            }

            return response;
        }

        /// <summary>
        /// Validates a new plan for entry.        
        /// </summary>        
        /// <returns>Returns a string List of all validation error messages</returns>
        private IList<string> ValidateNewPlan(Plan plan)
        {
            IList<string> messages = new List<string>();
            if (plan == null)
            {
                messages.Add("No Plan was supplied.");
                return messages;
            }

            if (plan.Id != null) messages.Add("Please supply a new plan.");

            if (plan.NetworkId > 0)
            {
                var network = _networkRepository.GetNetwork(plan.NetworkId);
                if (network == null) messages.Add("No Network was found.");
            }
            else
            {
                messages.Add("No Network was found.");
            }

            if (string.IsNullOrEmpty(plan.Name))
            {
                messages.Add("A plan name was not supplied.");
            }

            if (plan.FlagFall < 0)
            {
                messages.Add("Plan flag fall is negative.");
            }

            if (plan.FreeCallAmount < 0)
            {
                messages.Add("Plan free call amount is negative.");
            }

            if (plan.PlanAmount < 0)
            {
                messages.Add("Plan amount is negative.");
            }

            if (plan.FreeCallAmount > plan.PlanAmount)
            {
                messages.Add("Plan free call amount is greater than the plan amount.");
            }

            if (plan.Duration < 0)
            {
                messages.Add("Plan duration is negative.");
            }

            return messages;
        }

        public bool UpdatePlan(Plan plan)
        {
            // When a plan is updated we may need to update the units of time on uninvoiced calls.
            var existingPlan = _planRepository.GetPlan(plan.Id.Value);
            if (existingPlan != null) {
                if (existingPlan.UnitOfTime != plan.UnitOfTime) {
                    // Recalculate unit of time on calls for this plan.
                    var calls = _planRepository.GetCallsForPlan(plan.Id.Value);                    
                    foreach (var call in calls) {

                        call.UnitsOfTime = new UnitsOfTimeCalculator(call.CallType).Calculate(plan.UnitOfTime,
                                                                                              (double)call.ActualVolume);
                        
                        _contractRepository.UpdateCall(call);
                    }
                }
            }

            return _planRepository.UpdatePlan(plan);
        }

        public DeletePlanResponse DeletePlan(DeletePlanRequest request)
        {
            var response = new DeletePlanResponse();
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            #region Validation

            if (!PermitOnlyI())
            {
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            var contracts = _contractRepository.FindByPlanId(request.PlanId);
            if (contracts.Count(c => c.ContractStatus != ContractStatus.Closed) > 0)
            {
                response.Message = "Cannot delete Plan that has active contracts";
                return response;
            }

            #endregion

            try
            {
                using (var ts = new TransactionScope())
                {
                    if (_planRepository.DeletePlan(request.PlanId))
                    {
                        response.IsSuccessful = true;
                        ts.Complete();
                    }
                }
            }
            catch (Exception)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        public IList<PlanTariff> GetPlanTariffs(int planId)
        {
            return _planTariffRepository.GetPlanTariffsByPlan(planId);
        }

        public bool UpdatePlanTariff(PlanTariff entity)
        {
            return _planTariffRepository.UpdatePlanTariff(entity);
        }

        public CanDeletePlanResponse CanDelete(Plan plan)
        {
            var response = new CanDeletePlanResponse();

            if (plan == null) {
                response.IsSuccessful = false;
                response.Message = "No plan supplied";
                return response;
            }

            try {
                var contracts = _contractRepository.FindByPlanId(plan.Id.Value);
                response.OpenContractCount = contracts.Count(c => c.ContractStatus != ContractStatus.Closed);                
                response.IsSuccessful = true;
            } catch (Exception) {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }        
    }
}
