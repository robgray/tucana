﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.Interfaces;
using LINQtoCSV;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class EwayPaymentsService : IEwayPaymentsService 
    {
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IContactRepository _contactRepository;
        private readonly IEwayProcessedDataRepository _ewayDataRepository;
        private readonly IInvoiceHardcopyRepository _invoiceHardcopyRepository;

        public EwayPaymentsService() : this (
            RepositoryFactory.GetRepository<IInvoiceRepository>(),
            RepositoryFactory.GetRepository<IAccountRepository>(),
            RepositoryFactory.GetRepository<IContactRepository>(),
            RepositoryFactory.GetRepository<IEwayProcessedDataRepository>(),
            RepositoryFactory.GetRepository<IInvoiceHardcopyRepository>()) 
        { }

        public EwayPaymentsService(IInvoiceRepository invoiceRepository, IAccountRepository accountRepository,
                                   IContactRepository contactRepository,
                                   IEwayProcessedDataRepository ewayDataRepository,
                                   IInvoiceHardcopyRepository invoiceHardcopyRepository)
        {
            _invoiceRepository = invoiceRepository;
            _accountRepository = accountRepository;
            _contactRepository = contactRepository;
            _ewayDataRepository = ewayDataRepository;
            _invoiceHardcopyRepository = invoiceHardcopyRepository;
        }

        /// <summary>
        /// Creates a CSV string representation of the Eway credit card charge file
        /// </summary>        
        public string ExportCreditCardChargeFile(int runNumber)
        {
            try
            {
                var invoices = _invoiceRepository.FindInvoicesByRunNumber(runNumber);

                var ewayExport = new List<EwayLineItem>();

                foreach (var invoice in invoices)
                {
                    var rootAccount = _accountRepository.GetAccount(invoice.InvoiceRootAccountId);
                    if (rootAccount.BillingMethod != BillingMethod.CreditCard)
                        continue;

                    var rootContact = _contactRepository.GetContactEntity(rootAccount.ContactId);

                    // 13/05/2012 - Moved eway line item creation to factory method.
                    ewayExport.Add(EwayLineItem.Create(invoice, rootContact));                    
                }

                CsvFileDescription fileDescription = new CsvFileDescription
                {
                    SeparatorChar = ',',
                    FirstLineHasColumnNames = false
                };

                CsvContext context = new CsvContext();
                using (Stream csvStream = new MemoryStream())
                {
                    var sw = new StreamWriter(csvStream);
                    context.Write(ewayExport, sw, fileDescription);
                    sw.Flush();

                    csvStream.Position = 0;
                    var sr = new StreamReader(csvStream);
                    string result = sr.ReadToEnd();
                    sr.Dispose();

                    return result;
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
            }

            return string.Empty;
        }

        public void ImportCreditCardChargeFile(string csvFile, string filename)
        {
            var fileDescription = new CsvFileDescription
            {
                SeparatorChar = ','
            };

            var context = new CsvContext();
            IEnumerable<EwayImportLine> importFile;
            using (var csvStream = new MemoryStream())
            {
                var sw = new StreamWriter(csvStream);
                sw.Write(csvFile);
                sw.Flush();

                csvStream.Position = 0;
                var sr = new StreamReader(csvStream);
                importFile = context.Read<EwayImportLine>(sr, fileDescription);
                
                var unsuccessfulPayments = importFile.ToList();     // initially all are unsuccessful.

                foreach (var line in importFile)
                {
                    long transactionReference = Convert.ToInt64(line.EwayTransactionReference);

                    // Check has not already been processed
                    if (_ewayDataRepository.GetByEwayTransactionReference(transactionReference) != null) {
                        LoggingUtility.LogDebug("ImportCreditCardChargeFile", "EwayPaymentsService", string.Format("Eway Transaction Reference '{0}' has already been imported.  Skipping processing of this payment", transactionReference));
                        continue;
                    }

                    using (var ts = new TransactionScope()) {

                        var invoice = _invoiceRepository.GetByInvoiceNumber(line.InvoiceNumber);
                        
                        if (invoice != null) {

                            var account = _accountRepository.GetAccount(invoice.InvoiceRootAccountId);                           

                            if (line.IsSuccessful) {

                                unsuccessfulPayments.Remove(line);

                                var gap = invoice.MakePayment(line.Amount);
                                account.MakePayment(line.Amount + gap, invoice.InvoiceNumber);                                        

                            } else {
                                account.LogActivity(string.Format("Failed to process payment for invoice '{0}'.  Reason: {1} - {2}.", line.InvoiceNumber, line.ResponseCode, line.ResponseText));
                                
                                invoice.HasFailedPayment = true;
                                invoice.LogActivity(string.Format("Failed to process payment from cardholder {0}. Reason: {1} - {2}.", line.CardHolder, line.ResponseCode, line.ResponseText));

                                if (_invoiceHardcopyRepository.Move(invoice))
                                    invoice.LogActivity("Invoice moved to Failed Invoices folder");
                            }

                            _invoiceRepository.UpdateInvoice(invoice);
                            _accountRepository.UpdateAccount(account);
                        }
                        else {
                            LoggingUtility.LogWarning("ImportCreditCardChargeFile", "EwayPaymentsService",
                                                      string.Format(
                                                          "Could not find Invoice '{0}'.  Cannot process payment",
                                                          line.InvoiceNumber));
                        }
                        

                        var processed = new EwayProcessedData
                                            {
                                                Amount = line.Amount,
                                                CardHolder = line.CardHolder,
                                                EwayInvoiceReference = line.EwayInvoiceReference,
                                                EwayTransactionReference = transactionReference,
                                                InvoiceNumber = line.InvoiceNumber,
                                                ResponseCode = line.ResponseCode,
                                                ResponseText = line.ResponseText
                                            };

                        _ewayDataRepository.Insert(processed);                        
                        ts.Complete();
                    }
                }                
            }
            LoggingUtility.LogDebug("ImportCreditCardChargeFile", "EwayPaymentsService", string.Format("Finished Importing and Processing '{0}'", filename));
        }
    }
}
