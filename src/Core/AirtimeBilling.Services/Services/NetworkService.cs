﻿using System;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Transactions;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    internal class NetworkService : ServiceBase, INetworkService     
    {
        private INetworkRepository _networkRepository;
        private IPlanRepository _planRepository;
        private INetworkTariffRepository _networkTariffRepository;
        private IPlanTariffRepository _planTariffRepository;
        private IContractRepository _contractRepository;

        public NetworkService() : 
            this(RepositoryFactory.GetRepository<INetworkRepository>(), 
            RepositoryFactory.GetRepository<IPlanRepository>(),
            RepositoryFactory.GetRepository<INetworkTariffRepository>(),
            RepositoryFactory.GetRepository<IPlanTariffRepository>(),
            RepositoryFactory.GetRepository<IContractRepository>()) { }

        public NetworkService(INetworkRepository networkRepository, IPlanRepository planRepository, INetworkTariffRepository networkTariffRepository, IPlanTariffRepository planTariffRepository, IContractRepository contractRepository) 
        {
            _networkRepository = networkRepository;
            _planRepository = planRepository;
            _networkTariffRepository = networkTariffRepository;
            _planTariffRepository = planTariffRepository;
            _contractRepository = contractRepository;
        }
     
        public IList<Network> GetAllNetworks()
        {
            return _networkRepository.GetAllNetworks();
        }

        public IList<Network> GetAllActiveNetworks()
        {
            return _networkRepository.GetAllActiveNetworks();
        }

        public Network GetNetwork(int networkId)
        {
            return _networkRepository[networkId];
        }

        /// <summary>
        /// Validates a new network to be added or updated in the system.
        /// </summary>        
        /// <returns>Returns a string list of validation errors</returns>
        protected IList<string> ValidateNetwork(Network network)
        {
            IList<string> messages = new List<string>();
            if (network == null)
            {
                messages.Add("Missing Network");
                return messages;
            }

            if (string.IsNullOrEmpty(network.Name)) messages.Add("Missing Network Name");

            return messages;
        }

        public InsertNetworkResponse InsertNetwork(InsertNetworkRequest request)
        {
            var response = new InsertNetworkResponse();
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            #region Validation

            // Only allow inserting of networks by I personnel
            if (!PermitOnlyI())
            {
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            var validationErrors = ValidateNetwork(request.Network);
            if (validationErrors.Count > 0)
            {
                response.Message = validationErrors[0];
                return response;
            }

            #endregion

            using (var ts = new TransactionScope())
            {
                try
                {
                    _networkRepository.InsertNetwork(request.Network);
                    if (request.Network.Id != null)
                    {
                        response.NetworkId = request.Network.Id.Value;
                        response.IsSuccessful = true;
                        ts.Complete();
                    }
                    else
                    {
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                    }

                }
                catch (Exception)
                {
                    response.Message = Constants.Messages.INTERNAL_ERROR;
                }
            }

            return response;
        }

        public UpdateNetworkResponse UpdateNetwork(UpdateNetworkRequest request)
        {
            var response = new UpdateNetworkResponse();
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            #region Validation

            if (!PermitOnlyI())
            {
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            var validationErrors = ValidateNetwork(request.Network);
            if (validationErrors.Count > 0)
            {
                response.Message = validationErrors[0];
                return response;
            }

            #endregion

            try
            {
                using (var ts = new TransactionScope())
                {              
                    if (_networkRepository.UpdateNetwork(request.Network))
                    {
                        response.IsSuccessful = true;                 
                    }
                    else
                    {
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                    }
                    ts.Complete();
                }                
            } catch (Exception ex)
                {
                    LoggingUtility.LogException(ex);
                    response.Message = Constants.Messages.INTERNAL_ERROR;
                }

            return response;
        }

        public DeleteNetworkResponse DeleteNetwork(DeleteNetworkRequest request)
        {
            var response = new DeleteNetworkResponse();
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            #region Validation

            if (!PermitOnlyI())
            {
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }

            // There must be no active Contracts for this network
            IList<Plan> plans = _planRepository.GetPlansByNetworkId(request.NetworkId);
            foreach (Plan plan in plans)
            {
                IList<Contract> contracts = _contractRepository.FindByPlanId(plan.Id.Value);
                if (contracts.Count(c => c.ContractStatus != ContractStatus.Closed) > 0)
                {                    
                    response.Message = "Cannot delete networks with active contracts.";
                    LoggingUtility.LogDebug("DeleteNetwork", "NetworkService", response.Message);
                    return response;
                }
            }

            #endregion

            try {
                using (var ts = new TransactionScope()) {
                    if (_networkRepository.DeleteNetwork(request.NetworkId)) {
                        response.IsSuccessful = true;
                        
                    }
                    else {
                        LoggingUtility.LogDebug("DeleteNetwork", "NetworkService", "Could not delete network '" + request.NetworkId + "'");
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                    }
                    ts.Complete();
                }
                
            } catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        public IList<NetworkTariff> GetNetworkTariffs(int networkId)
        {
            return _networkTariffRepository.GetNetworkTariffsByNetworkId(networkId);
        }

        public UpdateNetworkTariffResponse UpdateNetworkTariff(UpdateNetworkTariffRequest request)
        {
            var response = new UpdateNetworkTariffResponse();

            #region Validation

            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            if (request.Tariff == null)
            {
                response.Message = "No Network Tariff has been supplied";
                return response;
            }

            if (!request.Tariff.IsValid)
            {
                response.Message = request.Tariff.GetRuleViolations().First().ErrorMessage;
                return response;
            }

            #endregion

            try
            {
                using (var ts = new TransactionScope())
                {                    
                    var success = _networkTariffRepository.UpdateNetworkTariff(request.Tariff);
                    if (success)
                    {
                        request.Tariff.LogActivity("Updated Tariff");                        
                        response.IsSuccessful = true;                        
                    }
                    else
                    {
                        LoggingUtility.LogDebug("UpdateNetworkTariff", "NetworkService", "Could not updat network tariff");
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                    }
                    ts.Complete();
                }                                    
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                response.Message = Constants.Messages.INTERNAL_ERROR;
            }

            return response;
        }

        public DeleteNetworkTariffResponse DeleteNetworkTariff(DeleteNetworkTariffRequest request)
        {
            var response = new DeleteNetworkTariffResponse();

            #region Validation

            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            var tariff = _networkTariffRepository[request.TariffId];
            if (tariff == null)
            {
                response.Message = string.Format("Invalid Network Tariff. Id={0} does not exist.", request.TariffId);
                return response;
            }

            #endregion

            using (var ts = new TransactionScope())
            {
                try
                {
                    if (!_planTariffRepository.DeletePlanTariffsByNetworkTariff(tariff.Id.Value))
                    {
                        LoggingUtility.LogDebug("DeleteNetworkTariff", "NetworkService", "Could not delete associated plan tariffs for network tariff");
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                        return response;
                    }

                    if (_networkTariffRepository.DeleteNetworkTariff(request.TariffId))
                    {
                        LoggingUtility.WriteActivity(Constants.TableNames.NETWORK_TARIFF, request.TariffId, "Deleted NetworkTariff and associated Plan Tariffs");
                        ts.Complete();
                        response.IsSuccessful = true;
                    }
                    else
                    {
                        LoggingUtility.LogDebug("DeleteNetworkTariff", "NetworkService","Could not delete network tariff");
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                        return response;
                    }
                }
                catch (Exception ex)
                {
                    LoggingUtility.LogException(ex);
                    response.Message = Constants.Messages.INTERNAL_ERROR;
                }
            }

            return response;
        }

        public InsertNetworkTariffResponse InsertNetworkTariff(InsertNetworkTariffRequest request)
        {
            var response = new InsertNetworkTariffResponse();

            #region Validation

            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            if (request.Tariff == null)
            {
                response.Message = "No Network Tariff has been supplied";
                return response;
            }

            if (string.IsNullOrEmpty(request.Tariff.Code))
            {
                response.Message = "A Tariff Code must be supplied";
                return response;
            }

            if (string.IsNullOrEmpty(request.Tariff.Name))
            {
                response.Message = " A Tariff Name must be supplied";
                return response;
            }

            #endregion

            using (var ts = new TransactionScope())
            {
                try
                {
                    _networkTariffRepository.InsertNetworkTariff(request.Tariff);
                    if (request.Tariff.Id == null)
                    {
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                        return response;
                    }

                    LoggingUtility.WriteActivity(Constants.TableNames.NETWORK_TARIFF, request.Tariff.Id.Value, "Insert Network Tariff");

                    // Insert a Plan Tariff for every plan on this network
                    var plans = _planRepository.GetPlansByNetworkId(request.Tariff.NetworkId);
                        
                    foreach (var plan in plans)
                    {
                        var tariff = new PlanTariff(request.Tariff.Id.Value)
                        {
                            IsCountedInFreeCall = request.Tariff.IsCountedInFreeCall,
                            PlanId = plan.Id.Value,
                            UnitCost = 0M
                        };

                        _planTariffRepository.InsertPlanTariff(tariff);
                        if (tariff.Id != null) continue;
                        LoggingUtility.LogDebug("InsertNetworkTariff", "NetworkService", "Count not insert Plan Tariff");
                        response.Message = Constants.Messages.INTERNAL_ERROR;
                        return response;
                    }

                    ts.Complete();
                    response.IsSuccessful = true;
                }
                catch (Exception ex)
                {
                    LoggingUtility.LogException(ex);
                    response.Message = Constants.Messages.INTERNAL_ERROR;
                }
            }

            return response;
        }

        public IList<Network> FindByNetworkName(string name)
        {
            return GetAllNetworks().Where(n => n.Name.StartsWith(name,true,CultureInfo.CurrentCulture)).ToList();
        }
    }
}
