﻿using System;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class UserService : IUserService 
    {
        private IAgentRepository agentRepository;
        private IContactRepository contactRepository;

        public UserService(IAgentRepository agentRepository, IContactRepository contactRepository)
        {
            this.agentRepository = agentRepository;
            this.contactRepository = contactRepository;
        }

        public UserService()
        {
            this.agentRepository = RepositoryFactory.GetRepository<IAgentRepository>();
            this.contactRepository = RepositoryFactory.GetRepository<IContactRepository>();
        }

        #region IUserService Members

        public GetUserResponse GetUserByAgentId(int agentId)
        {
            var response = new GetUserResponse();
            try
            {
                var agent = agentRepository.GetAgent(agentId);
                if (agent != null)
                {
                    response.Email = agent.Email;
                }
                response.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
                LoggingUtility.LogException(ex);
            }
            
            return response;
        }

        public GetUserResponse GetUserByContactId(int contactId)
        {
            var response = new GetUserResponse();
            try
            {
                var contact = contactRepository.GetContactEntity(contactId);
                if (contact != null)
                {
                    response.Email = contact.Email;
                }
                response.IsSuccessful = true;
            }
            catch (Exception ex)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;
                LoggingUtility.LogException(ex);
            }

            return response;
        }

        public UpdateUserResponse UpdateUser(User user)
        {
            var response = new UpdateUserResponse();

            if (user == null)
            {
                response.IsSuccessful = false;
                response.Message = "User was not supplied with update";
                return response;
            }

            try
            {
                if (user is AgentUser)
                {
                    var agentUser = (user as AgentUser);
                    var agent = agentRepository.GetAgent(agentUser.AgentId);
                    if (agent != null)
                    {
                        agent.Email = user.Email;
                        agentRepository.Update(agent);
                    }
                }
                else if (user is ContactUser)
                {
                    var contactUser = (user as ContactUser);
                    var contact = contactRepository.GetContactEntity(contactUser.ContactId);
                    if (contact != null)
                    {
                        contact.Email = user.Email;
                        contactRepository.UpdateContact(contact);
                    }
                }

                response.IsSuccessful = true;                
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                response.IsSuccessful = false;
                response.Message = Constants.Messages.INTERNAL_ERROR;                
            }
            return response;
        }
       
        #endregion
    }
}
