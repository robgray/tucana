﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    public class ActivityLoggingService : IActivityLoggingService
    {
        private IActivityLogRepository repository;

        public ActivityLoggingService() :
            this(RepositoryFactory.GetRepository<IActivityLogRepository>())
        {
        }

        public ActivityLoggingService(IActivityLogRepository repository)
        {
            this.repository = repository;
        }

        #region IActivityLoggingService Members

        public void WriteUserActivity(EntityBase entity, string user, string activity) 
        {
            WriteUserActivity(entity, DateTime.Now, user, activity);
        }

        public void WriteActivity(EntityBase entity, string activity)
        {
            WriteActivity(entity, DateTime.Now, activity);
        }

        public void WriteUserActivity(EntityBase entity, DateTime activityDate, string user, string activity)
        {
            var log = new Activity(activityDate, user, activity);

            repository.Insert(log, entity.GetEntityName(), entity.Id.Value);
        }

        public void WriteActivity(EntityBase entity, DateTime activityDate, string activity)
        {
            WriteUserActivity(entity, activityDate, System.Threading.Thread.CurrentPrincipal.Identity.Name, activity);
        }

        public void WriteSystemActivity(EntityBase entity, string activity)
        {
            WriteSystemActivity(entity, DateTime.Now, activity);
        }

        public void WriteSystemActivity(EntityBase entity, DateTime activityDate, string activity)
        {
            WriteUserActivity(entity, activityDate, "System", activity);
        }

        public IList<Activity> FindLogEntriesForEntity(EntityBase entity)
        {
            return repository.FindActivitiesByEntity(entity);
        }

        public IList<Activity> FindLogEntriesForUser(string username)
        {
            return repository.FindActivitiesByUser(username);
        }

        #endregion
    }
}
