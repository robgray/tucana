﻿using System;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.DataAccess.Repositories;
using AirtimeBilling.Services.Interfaces;

namespace AirtimeBilling.Services.Services
{
    public class NumberGeneratorService : INumberGeneratorService
    {
        private static object invoiceNumberLock = new object();
        private static object invoiceRunNumberLock = new object();
        private static object contractNumberLock = new object();
        private static object accountNumberLock = new object();

        private ISettingRepository _settingRepository;

        public NumberGeneratorService() : this (RepositoryFactory.GetRepository<ISettingRepository>()) { }

        public NumberGeneratorService(ISettingRepository settingRepository)
        {
            _settingRepository = settingRepository;
        }

        #region INumberGeneratorService Members

        public string NextInvoiceNumber()
        {
            lock (invoiceNumberLock)
            {
                var setting = _settingRepository["NextInvoiceNumber"];
                
                if (setting == null) 
                    throw new Exception("Unable to find Invoice Number");
                
                var number = Convert.ToInt32(setting.Value);
                setting.Value = (number + 1).ToString();
                
                _settingRepository.Commit(setting);

                return number.ToString(); ;
            }
        }

        public int NextInvoiceRunNumber()
        {
            lock (invoiceRunNumberLock)
            {
                var setting = _settingRepository["NextInvoiceRunNumber"];
                
                if (setting == null)
                    throw new Exception("Unable to find Invoice Run Number");                
                
                var number = Convert.ToInt32(setting.Value);
                setting.Value = (number + 1).ToString();
                
                _settingRepository.Commit(setting);
                return number;                                
            }
        }

        public string NextContractNumber()
        {
            lock (contractNumberLock)
            {
                var setting = _settingRepository["NextContractNumber"];
                var prefixSetting = _settingRepository["ContractNumberPrefix"];
                var formatSetting = _settingRepository["ContractNumberFormat"];

                if (setting == null) throw new Exception("Unable to find Contract Number");
                if (prefixSetting == null) throw new Exception("Unable to find Contract Number Prefix");
                if (formatSetting == null) throw new Exception("Unable to find Contract Number Format");

                var number = Convert.ToInt32(setting.Value);
                
                setting.Value = (number + 1).ToString();
                _settingRepository.Commit(setting);

                return string.Format("{0}{1:" + formatSetting.Value + "}", prefixSetting.Value, number);
            }
        }

        public string NextAccountNumber()
        {
            lock (accountNumberLock)
            {
                var setting = _settingRepository["NextAccountNumber"];
                var prefixSetting = _settingRepository["AccountNumberPrefix"];
                var formatSetting = _settingRepository["AccountNumberFormat"];

                if (setting == null) throw new Exception("Unable to find Account Number");
                if (prefixSetting == null) throw new Exception("Unable to find Account Number Prefix");
                if (formatSetting == null) throw new Exception("Unable to find Account Number Format");

                var number = Convert.ToInt32(setting.Value);

                setting.Value = (number + 1).ToString();
                _settingRepository.Commit(setting);

                return string.Format("{0}{1:" + formatSetting.Value + "}", prefixSetting.Value, number);
            }
        }

        #endregion
    }
}
