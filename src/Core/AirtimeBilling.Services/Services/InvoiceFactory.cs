﻿using System;
using System.Linq;
using System.ServiceModel;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    /// <summary>
    /// Generates invoices 
    /// </summary>
    public class InvoiceFactory
    {
        private readonly IDateTimeFacade _dateTime;        
        private readonly IContractRepository _contractRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IContactRepository _contactRepository;
        private readonly IPlanRepository _planRepository;
        private readonly INetworkRepository _networkRepository;
        private readonly IContractService _contractService;
        private readonly IInvoiceSettings _invoiceSettings;
        private readonly INumberGeneratorService _numberGeneratorService;

        private readonly InvoiceRunContext _context;

        public InvoiceFactory(InvoiceRunContext context) : this(
            new InvoiceSettings(), 
            new DateTimeFacade(), 
            RepositoryFactory.GetRepository<IContractRepository>(),
            RepositoryFactory.GetRepository<IAccountRepository>(),
            RepositoryFactory.GetRepository<IPlanRepository>(),
            RepositoryFactory.GetRepository<INetworkRepository>(),
            ServiceFactory.GetService<IContractService>(), 
            RepositoryFactory.GetRepository<ICompanyRepository>(),
            RepositoryFactory.GetRepository<IContactRepository>(),
            ServiceFactory.GetService<INumberGeneratorService>(),
            context) 
        { }

        public InvoiceFactory(IInvoiceSettings invoiceSettings, IDateTimeFacade dateTimeFacade, IContractRepository contractRepository,
                                IAccountRepository accountRepository, IPlanRepository planRepository, INetworkRepository networkRepository, 
                                IContractService contractService, ICompanyRepository companyRepository, IContactRepository contactRepository, 
                                INumberGeneratorService numberGeneratorService, InvoiceRunContext context)
        {
            _invoiceSettings = invoiceSettings;
            _dateTime = dateTimeFacade;
            _contractRepository = contractRepository;
            _accountRepository = accountRepository;
            _planRepository = planRepository;
            _networkRepository = networkRepository;
            _contractService = contractService;
            _companyRepository = companyRepository;
            _contactRepository = contactRepository;
            _numberGeneratorService = numberGeneratorService;

            _context = context;        
        }
        
        public Invoice GenerateInvoice(Account rootAccount)
        {
            if (!rootAccount.IsInvoiceRoot) 
                throw new ActionNotSupportedException("Cannot create an Invoice from a non-root Account");
            
            var rootContact = _contactRepository.GetContactEntity(rootAccount.ContactId);

            // Each Root Account is an Invoice Root.
            // This means we create an Invoice Header based on it.
            var invoiceNumber = _numberGeneratorService.NextInvoiceNumber();
            var invoice = rootAccount.CreateInvoice();

            invoice.InvoiceDate = _dateTime.Today;
            invoice.InvoiceNumber = invoiceNumber;
            invoice.FromDate = _context.FromDate;
            invoice.ToDate = _context.ToDate;
            invoice.AmountPaid = 0;
            invoice.To = rootAccount.AccountName;
            invoice.InvoiceRunNumber = _context.RunNumber;
            invoice.InvoiceAddress = new Address
                                         {
                                             Street = rootAccount.BillingAddressType == BillingAddressType.Physical ? rootContact.HomeAddress.Street : rootContact.PostalAddress.Street,
                                             Street2 = rootAccount.BillingAddressType == BillingAddressType.Physical ? rootContact.HomeAddress.Street2 : rootContact.PostalAddress.Street2,
                                             Suburb = rootAccount.BillingAddressType == BillingAddressType.Physical ? rootContact.HomeAddress.Suburb : rootContact.PostalAddress.Suburb,
                                             State = rootAccount.BillingAddressType == BillingAddressType.Physical ? rootContact.HomeAddress.State : rootContact.PostalAddress.State,
                                             Postcode = rootAccount.BillingAddressType == BillingAddressType.Physical ? rootContact.HomeAddress.Postcode : rootContact.PostalAddress.Postcode
                                         };

            ProcessCalls(invoice, rootAccount);
                        
            // Environmental levy 
            if (rootAccount.PostBill && (_invoiceSettings.EnvironmentalLevy > 0))
            {
                var environmentalLevy = new InvoiceLine
                {
                    Description = _invoiceSettings.EnvironmentalLevyDescription,
                    Quantity = 1,
                    LineAmount = _invoiceSettings.EnvironmentalLevy,
                    GstAmount = _invoiceSettings.EnvironmentalLevy / 11,
                    Invoice = invoice
                };

                invoice.AddLine(environmentalLevy);
            }                        

            // Update total fields on header
            invoice.PreviousBill = rootAccount.PreviousBill;
            invoice.AmountPaid = rootAccount.AmountPaid;  
            invoice.NewCharges = invoice.Lines.Sum(l => l.LineAmount);
            invoice.PaymentDue = invoice.InvoiceDate.AddMonths(1);
            invoice.CalculateTotals();
                        
            // HasData can return false when there are no contracts to be invoiced for this account.
            // Measured by there being no accounts associated with this invoice.  this could be clearer.
            if (invoice.HasData)
                return invoice;
                                    
            return null;
        }
        
        /// <summary>
        /// This (recursive) call calculates the invoice line items for all accounts and contracts under the root account.
        /// </summary>
        /// <param name="invoice">Invoice for the root account</param>
        /// <param name="account">Current account to process the calls for</param>
        /// <param name="accessFees">The invoice line for the network access fees</param>
        /// <param name="freeCalls">The invoice line for the free calls</param>
        /// <param name="suspendedContracts">The invoice line for contract suspended in this invoice period</param>
        /// <remarks>
        /// Only one access fees line and one free calls lines per invoice, so we pass the line instance and
        /// for every new access fee or free call component we increase the quantity and line amount accordingingly.
        /// </remarks>
        private void ProcessCalls(Invoice invoice, Account account)
        {                       
            // Get all the contracts associated with the current Account
            var contracts = _contractRepository.GetContractsByAccountId(account.Id.Value);

            if (contracts.Count(invoice.IsContractBillable) > 0) {
                // Associate the invoice with the accounts that were invoiced on this invoice.
                invoice.AddAccount(account); 
            }
            else {
                return;
            }

            // Process all contracts for this account
            foreach (var contract in contracts)
            {
                if (!invoice.IsContractBillable(contract))                
                    continue;

                // Need the plan to calculate the free call amount and plan amount.
                var plan = _planRepository.GetPlan(contract.PlanId);

                var customFees = _contractRepository.GetAllCustomFeesForContract(contract);
                foreach (var fee in customFees)
                {
                    invoice.AddLine(fee.Description, 1, fee.Amount, null, contract);

                    if (!fee.IsRecurring)
                    {
                        _contractService.RemoveCustomFee(fee.Id.Value);
                    }
                }

                // Network Access fee - everyone gets this                
                // Network access fee is pro-rata
                var accessFee = plan.PlanAmount * invoice.CalculateContractStartProrataRate(contract);
                
                invoice.AddLine(_invoiceSettings.NetworkAccessFeeDescription, 1, accessFee , null, contract);

                // Get all imported calls
                var uninvoicedCalls = _contractRepository.GetNonInvoicedCallsByContractId(contract.Id.Value);

                // If the contract started a suspension since the last invoice period, charge the suspension fee
                // we still want to calculate everything else, as if it's active.
                // Doesn't matter if it's no longer suspended.
                if (contract.SuspensionStartDate >= invoice.FromDate)
                {                    
                    invoice.AddLine(_invoiceSettings.ContractSuspensionDescription, 1, _invoiceSettings.ContractSuspensionFee, null, contract);
                }

                if (invoice.HasContractBeenClosedInThisInvoicePeriod(contract) && contract.IsTerminated)
                {                                        
                    var remainingContractValue = plan.RemainingContractValue(contract);

                    invoice.AddLine(GetEarlyTerminationFeeDescription(contract), 1, remainingContractValue, null, contract);
                }
                else
                {                    
                    decimal freeCallAmount = 0;
                    decimal availableFreeCall = plan.FreeCallAmount > accessFee ? accessFee : plan.FreeCallAmount;
                    foreach (var call in uninvoicedCalls)
                    {
                        // 001-152 - do not invoice calls made after the invoice date.
                        if (call.CallTime > invoice.ToDate) continue;

                        if (call.HasFreeCallTariff)
                        {
                            freeCallAmount += call.Cost;
                            if (freeCallAmount > availableFreeCall)
                            {
                                freeCallAmount = availableFreeCall;
                            }                            
                        }
                        
                        invoice.AddLine(GetCallLine(call));
                    }

                    // If no calls were made in this period, insert a $0 Call line with the Network as the description.
                    if (uninvoicedCalls.Count == 0) {
                        var network = _networkRepository.GetNetwork(plan.NetworkId);
                        
                        invoice.AddLine(network.Name, 0, 0, null, contract);
                    }

                    // Add the free call amount to the total thus far.
                    if (freeCallAmount > 0)
                    {                        
                        invoice.AddLine(_invoiceSettings.FreeCallComponentDescription, 1, -freeCallAmount, null, contract);
                    }                 
                }
           
                contract.LogActivity(string.Format("Contract has been invoiced against Invoice Number '{0}' in Run Number '{1}'",
                        invoice.InvoiceNumber, invoice.InvoiceRunNumber));
            }

            var subAccounts = _accountRepository.GetAllSubAccounts(account.Id.Value);
            foreach (var subAccount in subAccounts)
            {
                ProcessCalls(invoice, subAccount);
            }

            account.LogActivity(string.Format("Account has been invoiced against Invoice Number '{0}' in Run Number '{1}'",
                invoice.InvoiceNumber, invoice.InvoiceRunNumber));
        }

        public static string GetEarlyTerminationFeeDescription(Contract contract)
        {
            var description = string.Format("Early contract termination fee for contract '{0}'.  {1:0} days early.",
                                        contract.ContractNumber, contract.RemainingTimeOnContract.TotalDays);

            return description;
        }     

        public InvoiceLine GetCallLine(Call call)
        {
            var line = new InvoiceLine
                        {
                            Call = call,
                            Quantity = Convert.ToDouble(call.Volume),
                            LineAmount = call.Cost,
                            Description = GetCallDescription(call),
                            GstAmount = call.Cost / 11M,
                            ContractId = call.ContractId,
                        };

            return line;
        }

        public static string GetCallDescription(Call call)
        {
            if (string.IsNullOrEmpty(call.NumberCalled))            
                return call.ImportedCallType;
            
            return string.Format("{0} to {1}", call.ImportedCallType, call.NumberCalled);            
        }
    }
}
