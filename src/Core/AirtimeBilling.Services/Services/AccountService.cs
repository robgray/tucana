﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Transactions;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    internal class AccountService : ServiceBase, IAccountService 
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IContactRepository _contactRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IInvoiceRepository _invoiceRepository;
        private readonly IInvoicingService _invoicingService;
        private readonly INumberGeneratorService _generator;

        public AccountService() : this(
            RepositoryFactory.GetRepository<IAccountRepository>(),
            RepositoryFactory.GetRepository<IContactRepository>(),
            RepositoryFactory.GetRepository<ICompanyRepository>(), 
            ServiceFactory.GetService<INumberGeneratorService>(),            
            RepositoryFactory.GetRepository<IInvoiceRepository>(),
            ServiceFactory.GetService<IInvoicingService>()) { }

        public AccountService(IAccountRepository accountRepository, IContactRepository contactRepository, ICompanyRepository companyRepository, 
            INumberGeneratorService generatorService, IInvoiceRepository invoiceRepository, IInvoicingService invoicingService)
        {
            _accountRepository = accountRepository;
            _contactRepository = contactRepository;
            _companyRepository = companyRepository;
            _generator = generatorService;
            _invoiceRepository = invoiceRepository;
            _invoicingService = invoicingService;
        }

        public Account GetAccountById(int accountId)
        {
            return _accountRepository.GetAccount(accountId);
        }

        public Account GetAccountByAccountNumber(string accountNumber)
        {
            return _accountRepository.GetAccountByAccountNumber(accountNumber);
        }

        public IList<Account> GetAllAccounts()
        {
            return _accountRepository.GetAllAccounts();
        }

        public IList<Account> FindAccounts(AccountSearch searchType, string searchValue)
        {
            IList<Account> accounts = new List<Account>();
            switch (searchType)
            {
                case AccountSearch.AccountNumber:
                    accounts = _accountRepository.FindAccountsByAccountNumber(searchValue);
                    break;

                case AccountSearch.Company:
                    accounts = _accountRepository.FindAccountsByCopmpany(searchValue);
                    break;

                case AccountSearch.Contact:
                    accounts = _accountRepository.FindAccountsByContact(searchValue);
                    break;
            }
            return accounts;
        }

        public SaveAccountResponse SaveAccount(SaveAccountRequest request)
        {
            var response = new SaveAccountResponse();

            #region Validation

            if (request.User == null || request.User is AgentUser || request.User is ContactUser)
            {
                response.IsSuccessful = false;
                response.Message = Constants.Messages.NO_PERMISSIONS;
                return response;
            }
            System.Threading.Thread.CurrentPrincipal = request.User.GetUserPrincipal();

            var contact = _contactRepository.GetContactEntity(request.Account.ContactId);
            if (contact == null)
            {
                response.Message = "A valid Contact must be given for this Account";
                return response;
            }

            if (request.Account.Id != null && string.IsNullOrEmpty(request.Account.AccountNumber))
            {
                response.Message = "An Account Number must be given for this Account";
                return response;
            }
            else if (request.Account.Id == null)
            {
                // get account number
                request.Account.AccountNumber = _generator.NextAccountNumber();
                if (string.IsNullOrEmpty(request.Account.AccountNumber))
                {
                    response.Message = "Failed to generate Account Number";
                    return response;
                }
            }

            if (string.IsNullOrEmpty(request.Account.Password))
            {
                response.Message = "A Passwod must be given for this Account";
                return response;
            }

            // Master Accounts Don't need to have Companies.
            //if (request.Account is MasterAccount)
            //{            
            //    var master = (MasterAccount)request.Account;
            //    if (!master.CompanyId.HasValue)
            //    {
            //        response.Message = "A Company must be given for this Master Account";
            //        return response;
            //    }
            //    var company = _companyRepository.GetCompany(master.CompanyId.Value);
            //    if (company == null)
            //    {
            //        response.Message = "A valid Company must be given for this Master Account";
            //        return response;
            //    }
            //}

            if (request.Account is SubAccount)
            {
                var sub = (SubAccount)request.Account;
                if (sub.MasterAccountId == 0)
                {
                    response.Message = "This Sub Account must be associated with a parent Master Account";
                    return response;
                }
                else
                {
                    MasterAccount master = (MasterAccount)_accountRepository.GetAccount(sub.MasterAccountId);
                    if (master == null)
                    {
                        response.Message = "A valid Master Account must be given for this Account";
                        return response;
                    }
                }
            }

            // Cannot allow emailing of bill if no email provided on Account Contact
            if (string.IsNullOrEmpty(contact.Email) && (request.Account.EmailBill || request.Account.EmailBillDataFile))
            {
                response.Message = "A contact email must be supplied if recieving paperless bills";
                return response;
            }

            // IF SELECTING PAPERLESS INVOICING/BILLING, AN EMAIL ADDRESS MUST BE SUPPLIED
            if (request.Account.EmailBill && string.IsNullOrEmpty(contact.Email))
            {
                response.Message = "Please ensure an email address is supplied when requesting Invoices to be emailed";
                return response;
            }

            if (request.Account.EmailBillDataFile && string.IsNullOrEmpty(contact.Email))
            {
                response.Message = "Please ensure an email address is supplied when requesting Invoice data to be emailed";
                return response;
            }


            #endregion

            // Ensure HasRequestedInvoice is false.  
            // Invoice Requesting is only valid when creating a new contract.
            request.Account.HasRequestedInvoicing = false;

            try
            {
                using (var ts = new TransactionScope())
                {
                    if (request.Account.Id == null)
                    {
                        _accountRepository.InsertAccount(request.Account);
                        if (request.Account.Id != null)
                        {
                            request.Account.LogActivity("New Account Addded");                            
                            response.IsSuccessful = true;
                            ts.Complete();
                        }
                        else
                        {
                            response.Message = Constants.Messages.INTERNAL_ERROR;
                        }
                    }
                    else
                    {
                        if (_accountRepository.UpdateAccount(request.Account))
                        {
                            request.Account.LogActivity("Account Updated");                            
                            response.IsSuccessful = true;
                            ts.Complete();
                        }
                        else
                        {
                            response.Message = Constants.Messages.INTERNAL_ERROR;
                        }
                    }
                }
            }
            catch (Exception)
            {
                // TODO: Log the exception
                response.Message = Constants.Messages.INTERNAL_ERROR;
                response.IsSuccessful = false;
            }

            return response;
        }

        public IList<Account> GetSubAccounts(MasterAccount masterAccount)
        {
            return _accountRepository.GetAllSubAccounts(masterAccount.Id.Value);
        }

        public ResponseBase MakeInvoiceRoot(int accountId)
        {
            var response = new ResponseBase();
            
            var account = _accountRepository.GetAccount(accountId);
            if (account == null) {
                response.SetFailure("Could not find Account to updated");
                return response;
            }

            if (account.IsInvoiceRoot) {
                response.IsSuccessful = true;
                return response;
            }

            try {
                account.IsInvoiceRoot = true;
                if (_accountRepository.UpdateAccount(account)) {
                    account.LogActivity("Dettached from Master Account to be an Invoice Root");
                    response.IsSuccessful = true;
                    return response;
                }
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.SetFailure(Constants.Messages.INTERNAL_ERROR);
            }
            return response;
        }

        public ResponseBase ReAttachToMasterAccount(int subAccountId)
        {
            var response = new ResponseBase();
            var subAccount = _accountRepository.GetAccount(subAccountId);

            // verify this is an invoice root account
            if (subAccount is MasterAccount) {
                response.SetFailure("Cannot add the Master Account as a sub Account of another Account.");
                return response;
            }

            if (!subAccount.IsInvoiceRoot) {
                response.SetFailure("Accounts that are already sub-accounts cannot be added as a sub-account");
                return response;
            }

            subAccount.IsInvoiceRoot = false;

            try {
                using (var ts = new TransactionScope()) {

                    if (_accountRepository.UpdateAccount(subAccount)) {

                        subAccount.LogActivity("Re-attached as Sub-Account");
                        response.IsSuccessful = true;
                    }

                    ts.Complete();
                }
            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                response.SetFailure(Constants.Messages.INTERNAL_ERROR);
            }
            return response;
        }

        public AccountPaymentResponse MakePayment(int accountId, decimal amount)
        {
            var response = new AccountPaymentResponse();

            var account = _accountRepository.GetAccount(accountId);
            if (account != null) {
                response.Account = account;

                if (!account.IsInvoiceRoot) {
                    response.IsSuccessful = false;
                    response.Message = "Payments can only be made against Invoice Root accounts";
                    return response;
                }

                using (var ts = new TransactionScope())
                {
                    account.MakePayment(amount);

                    // Get Last Invoice for this account - this will be inefficient!!
                    var invoices = _invoiceRepository.GetAllInvoiceForAccount(account);
                    var failedPaymentInvoice = invoices.Where(i => i.HasFailedPayment).OrderByDescending(i => i.InvoiceDate).FirstOrDefault();
                    if (failedPaymentInvoice != null)
                    {
                        failedPaymentInvoice.MakeCatchupPayment(amount);

                        var invoicingService = new InvoicingService();
                        invoicingService.SaveInvoiceHardcopy(failedPaymentInvoice);
                        _invoiceRepository.UpdateInvoice(failedPaymentInvoice);
                    }

                    _accountRepository.UpdateAccount(account);
                    response.IsSuccessful = true;
                    ts.Complete();
                }

            } else {
                response.IsSuccessful = false;
                response.Message = "Could not find Account";
            }

            return response;
        }
    }
}
