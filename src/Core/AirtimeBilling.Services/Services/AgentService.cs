﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Services.Services
{
    internal class AgentService : ServiceBase, IAgentService 
    {
        private IAgentRepository _agentRepository;

        public AgentService() :this(RepositoryFactory.GetRepository<IAgentRepository>()) { }

        public AgentService(IAgentRepository agentRepository)
        {
            _agentRepository = agentRepository;
        }

        public IList<Agent> GetAllAgents()
        {
            return _agentRepository.GetAllAgents();
        }

        public Agent GetAgent(int agentId)
        {
            return _agentRepository.GetAgent(agentId);
        }

        public SaveAgentResponse SaveAgent(SaveAgentRequest request)
        {
            var response = new SaveAgentResponse();

            #region Validation

            if (!request.Agent.IsValid)
            {
                response.Message = request.Agent.GetRuleViolations().First().ErrorMessage;
                return response;
            }

            #endregion

            try
            {
                using (var ts = new TransactionScope())
                {
                    if (request.Agent.Id == null)
                    {
                        if (_agentRepository.Insert(request.Agent))
                        {                            
                            Activity.WriteActivity(request.Agent, "New Agent added");
                        }
                    }
                    else
                    {
                        if (_agentRepository.Update(request.Agent))
                        {
                            Activity.WriteActivity(request.Agent, "Agent updated");
                        }
                    }
                    response.IsSuccessful = true;
                    ts.Complete();
                }
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(ex);
                response.Message = "Failed to Update Agent.";
            }

            return response;
        }

        public IList<Agent> FindAgentsByName(string name)
        {
            return GetAllAgents().Where(a => a.AgentName.StartsWith(name)).ToList();            
        }
    }
}
