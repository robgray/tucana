﻿namespace AirtimeBilling.Services.DTO
{
    public class CloseContractRequest : RequestBase 
    {
        public int ContractId { get; set; }
    }
}
