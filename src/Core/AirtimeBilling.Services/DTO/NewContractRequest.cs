﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Services.DTO
{        
    public class NewContractRequest : RequestBase 
    {
        public NewContractRequest()
        {
            Contact = new Contact();
            Company = new Company();
        }

        public Contact Contact { get; set; }        
        public Company Company { get; set; }               
        public int PlanId { get; set; }        
        public DateTime ActivationDate { get; set; }        
        public DateTime EndDate { get; set; }        
        public BillingAddressType BillingAddressType { get; set; }        
        public BillingMethod BillingMethod { get; set; }        
        public string IMEINumber { get; set; }        
        public string SimCard { get; set; }
        public bool IsBusinessContract { get; set; }        
        public bool Data { get; set; }        
        public bool MessageBank { get; set; }        
        public string UsedBy { get; set; }        
        public string AccountPassword { get; set; }
        public bool EmailBill { get; set; }
        public bool PostBill { get; set; }
        public bool EmailBillData { get; set; }
        public bool IsInternational { get; set; }
    }
}
