﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{    
    public class InsertNetworkRequest : RequestBase 
    { 
        public Network Network { get; set; }
    }
}
