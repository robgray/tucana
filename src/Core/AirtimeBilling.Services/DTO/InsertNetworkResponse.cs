﻿namespace AirtimeBilling.Services.DTO
{  
    public class InsertNetworkResponse : ResponseBase 
    {
        public int NetworkId { get; set; }
    }
}
