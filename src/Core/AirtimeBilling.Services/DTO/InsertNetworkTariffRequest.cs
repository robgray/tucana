﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{    
    public class InsertNetworkTariffRequest : RequestBase 
    {
        public NetworkTariff Tariff { get; set; }
    }
}
