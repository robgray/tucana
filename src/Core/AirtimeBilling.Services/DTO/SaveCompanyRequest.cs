﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{    
    public class SaveCompanyRequest : RequestBase 
    {
        public Company Company { get; set; }
    }
}
