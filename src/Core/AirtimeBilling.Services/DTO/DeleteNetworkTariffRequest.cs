﻿namespace AirtimeBilling.Services.DTO
{    
    public class DeleteNetworkTariffRequest : RequestBase
    { 
        public int TariffId { get; set; }
    }
}
