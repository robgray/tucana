﻿namespace AirtimeBilling.Services.DTO
{
    public class GetUserResponse : ResponseBase 
    {
        public string Email { get; set; }
    }
}
