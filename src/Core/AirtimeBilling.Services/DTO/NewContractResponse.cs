﻿namespace AirtimeBilling.Services.DTO
{    
    public class NewContractResponse : ResponseBase
    { 
        public int ContractId { get; set;  }
    }
}
