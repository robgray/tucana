﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public  class CustomFeesResponse : ResponseBase 
    {
        public CustomFeesResponse()
        {
            CustomFees = new List<CustomFee>();
        }

        public IList<CustomFee> CustomFees { get; set; }
    }
}
