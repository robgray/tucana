﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{   
    public class UpdateContractResponse : ResponseBase 
    {
        public Contract Contract { get; set; }
    }
}
