﻿using System;

namespace AirtimeBilling.Services.DTO
{
    public class SuspendContractRequest : RequestBase
    {
        public int ContractId { get; set; }
        public DateTime SuspendUntil { get; set; }
    }
}
