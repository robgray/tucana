﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class GetUnInvoicedCallsResponse : ResponseBase      
    {
        public IList<Call> UninvoicedCalls { get; set; }
    }
}
