﻿using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Services.DTO
{    
    public class ContractSearchEntity
    {
        public int ContractId { get; set; }
 
        public string ContractNumber { get; set; }
 
        public string AccountNumber { get; set; }
 
        public string PhoneNumber { get; set; }
 
        public string ContactName { get; set; }
 
        public string CompanyName { get; set; }
 
        public ContractStatus ContractStatus { get; set; }
    }
}
