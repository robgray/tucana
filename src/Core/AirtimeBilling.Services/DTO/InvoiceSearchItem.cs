﻿using System;

namespace AirtimeBilling.Services.DTO
{        
    public class InvoiceSearchItem
    {     
        public int InvoiceHeaderId { get; set; }
     
        public string InvoiceNumber { get; set; }

        public DateTime InvoiceDate { get; set; }

        public string To { get; set; }

        public int RunNumber { get; set; }

        public bool CanBeEmailed { get; set; }
    }
}