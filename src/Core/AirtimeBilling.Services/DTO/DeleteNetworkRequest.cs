﻿namespace AirtimeBilling.Services.DTO
{    
    public class DeleteNetworkRequest : RequestBase 
    {
        public int NetworkId { get; set; }
    }
}
