﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class ViewContractResponse : ResponseBase 
    {
        public Account Account { get; set; }
        public Contract Contract { get; set; }        
        public Contact Contact { get; set; }        
        public Company Company { get; set; }        
        public bool IsInvoicingPending { get; set; }        
        public bool IsReadOnly { get; set; }
    }
}
