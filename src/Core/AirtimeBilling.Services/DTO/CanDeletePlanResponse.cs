﻿namespace AirtimeBilling.Services.DTO
{
    public class CanDeletePlanResponse : ResponseBase 
    {
        public int OpenContractCount { get; set; }
        public bool IsDeleteable { get { return OpenContractCount == 0;  } }
    }
}
