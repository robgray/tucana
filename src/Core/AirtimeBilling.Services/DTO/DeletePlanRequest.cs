﻿namespace AirtimeBilling.Services.DTO
{
    public class DeletePlanRequest : RequestBase
    { 
        public int PlanId { get; set; }
    }
}
