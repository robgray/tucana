﻿namespace AirtimeBilling.Services.DTO
{
    public class CustomFeesRequest : RequestBase
    {
        public int ContractId { get; set; }
    }
}
