﻿namespace AirtimeBilling.Services.DTO
{
    public class ViewContractRequest : RequestBase 
    {
        public int ContractId { get; set; }        
    }
}
