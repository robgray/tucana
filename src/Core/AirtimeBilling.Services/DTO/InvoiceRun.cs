﻿using System;

namespace AirtimeBilling.Services.DTO
{    
    public class InvoiceRun
    {         
        public int RunNumber { get; set; }
        
        public DateTime RunDate { get; set;  }

        public int NumberOfInvoices { get; set; }        
    }
}


