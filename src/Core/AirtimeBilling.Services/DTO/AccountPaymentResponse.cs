﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class AccountPaymentResponse : ResponseBase
    {
        public Account Account { get; set; }
    }
}
