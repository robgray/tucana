﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class UpdateNetworkTariffRequest : RequestBase 
    {
        public NetworkTariff Tariff { get; set; }
    }
}
