﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class SaveContactRequest : RequestBase 
    {        
        public Contact Contact { get; set; }
    }
}
