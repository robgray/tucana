﻿using System;

namespace AirtimeBilling.Services.DTO
{
    public class VaryContractPlanRequest : RequestBase
    {
        public int ContractId { get; set; }
        public DateTime StartDate { get; set; }     
        public DateTime EndDate { get; set; }        
        public decimal Cost { get; set; }        
        public int NewPlanId { get; set; }
    }
}
