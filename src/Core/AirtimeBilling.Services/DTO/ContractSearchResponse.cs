﻿using System.Collections.Generic;

namespace AirtimeBilling.Services.DTO
{    
    public class ContractSearchResponse : ResponseBase
    { 
        public IList<ContractSearchEntity> Contracts { get; set; }
    }
}
