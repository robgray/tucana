﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class SaveAccountRequest : RequestBase 
    {
        public Account Account { get; set; }
    }
}
