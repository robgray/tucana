﻿using System;

namespace AirtimeBilling.Services.DTO
{
    public class CallDTO
    {
        public CallDTO(int id, DateTime callTime, string calledNumber, string location, string tariff, decimal volume, decimal cost)
        {
            Id = id;
            CallTime = callTime;
            CalledNumber = calledNumber;
            Location = location;
            Tariff = tariff;
            Volume = volume;
            Cost = cost;
        }

        public int Id { get; private set; }
        public DateTime CallTime { get; private set; }
        public string CalledNumber { get; private set; }
        public string Location { get; private set; }
        public string Tariff { get; private set; }
        public decimal Volume { get; private set; }
        public decimal Cost { get; private set; }
    }
}
