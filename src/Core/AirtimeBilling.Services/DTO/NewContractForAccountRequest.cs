﻿using System;

namespace AirtimeBilling.Services.DTO
{
    public class NewContractForAccountRequest : RequestBase 
    {
        public string AccountNumber { get; set; }  
        public string AccountPassword { get; set; } 
        public int PlanId { get; set; }        
        public DateTime ActivationDate { get; set; }        
        public DateTime EndDate { get; set; }        
        public string IMEINumber { get; set; }        
        public string SimCard { get; set; }        
        public bool Data { get; set; }        
        public bool MessageBank { get; set; }        
        public string UsedBy { get; set; }       
    }
}
