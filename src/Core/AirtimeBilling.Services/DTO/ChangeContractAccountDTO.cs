﻿namespace AirtimeBilling.Services.DTO
{
    public class ChangeContractAccountDTO
    {
        private int contractId;
        private string contractNumber;
        private int accountId;
        private string accountNumber;
        private string contactName;
        private string plan;

        public ChangeContractAccountDTO(int contractId, string contractNumber, int accountId, string accountNumber, string contactName, string plan)
        {
            this.contractId = contractId;
            this.contractNumber = contractNumber;
            this.accountId = accountId;
            this.accountNumber = accountNumber;
            this.contactName = contactName;
            this.plan = plan;            
        }

        public int ContractId { get { return contractId; } }
        public string ContractNumber { get { return contractNumber;  } }
        public int AccountId { get { return accountId; } }
        public string AccountNumber { get { return accountNumber; } }
        public string ContactName { get { return contactName; } }
        public string Plan { get { return plan; } }
    }
}
