﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{    
    public class InsertPlanRequest : RequestBase 
    { 
        public Plan Plan { get; set; } 
        public int DefaultNetworkTariffId { get; set; }
    }
}
