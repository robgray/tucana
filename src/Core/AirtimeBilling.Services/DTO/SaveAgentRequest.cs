﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class SaveAgentRequest : RequestBase 
    {
        public Agent Agent { get; set; }
    }
}
