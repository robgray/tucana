﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Services.DTO
{
    public class UpdateContractRequest : RequestBase 
    {
        public int ContractId { get; set; }
        public int PlanId { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime EndDate { get; set; }
        public string UsedBy { get; set; }        
        public string Pin { get; set; }
        public string Puk { get; set; }
        public string PhoneNumber1 { get; set; }
        public string PhoneNumber2 { get; set; }
        public string PhoneNumber3 { get; set; }
        public string PhoneNumber4 { get; set; }

        // Can also update the other items on the contract.
        public string ContactEmail { get; set;  }
        public string ContactName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string LicenseNumber { get; set; }
        public DateTime LicenseExpiry { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string WorkNumber { get; set; }
        public string FaxNumber { get; set; }
        public Address HomeAddress { get; set; }
        public Address DeliveryAddress { get; set; }
        public string CreditCardName { get; set; }
        public string CreditCardExpiry { get; set; }
        public CreditCardType CreditCardType { get; set; }
        public string CreditCardNumber { get; set; }

        // Account
        public string AccountName { get; set; }
        public string AccountPassword { get; set; }
        public BillingMethod BillingMethod { get; set; }
        public BillingAddressType BillingAddress { get; set; }
        public bool EmailDataFile { get; set; }
        public bool PaperlessBill { get; set; }
        public bool PostBill { get; set;  }
        public bool IsInternational { get; set; }

    }
}
