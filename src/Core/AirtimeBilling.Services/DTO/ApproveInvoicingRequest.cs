﻿namespace AirtimeBilling.Services.DTO
{
    public class ApproveInvoicingRequest : RequestBase
    { 
        public int AccountId { get; set; } 
        public bool IsApprovalGranted { get; set; }
    }
}
