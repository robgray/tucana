﻿using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Services.DTO
{   
    public class ContractSearchRequest : RequestBase 
    { 
        public ContractStatus SearchStatus { get; set; } 
        public string SearchValue { get; set; } 
        public ContractSearchField SearchType { get; set; } 
        public SearchContractStatusType SearchContractStatusType { get; set; }
    }
}
