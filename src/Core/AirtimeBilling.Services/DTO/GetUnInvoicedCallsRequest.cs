﻿namespace AirtimeBilling.Services.DTO
{
    public class GetUnInvoicedCallsRequest : RequestBase
    {
        public int ContractId { get; set; }
    }
}
