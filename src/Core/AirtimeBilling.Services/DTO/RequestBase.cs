﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class RequestBase
    { 
        public User User { get; set; }
    }
}
