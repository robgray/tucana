﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Services.DTO
{
    public class UpdateNetworkRequest : RequestBase 
    {
        public Network Network { get; set; }        
    }
}
