﻿namespace AirtimeBilling.Services.DTO
{    
    public class ResponseBase
    {
        public bool IsSuccessful { get; set;  }
        public string Message { get; set;  }
        
        public void SetFailure(string message)
        {
            IsSuccessful = false;
            Message = message;
        }
    }
}
