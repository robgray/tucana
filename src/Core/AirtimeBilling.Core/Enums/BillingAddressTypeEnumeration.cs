﻿namespace AirtimeBilling.Core.Enums
{
    /// <summary>
    /// Contact's address type to post the bill to
    /// </summary>  
    public enum BillingAddressType
    {        
        Physical = 1,        
        Postal = 2
    }
}
