﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{    
    public enum ContactSearch
    { 
        Name = 0,
        [EnumDescription("Mobile Phone")]
        MobilePhone = 1
    };
}
