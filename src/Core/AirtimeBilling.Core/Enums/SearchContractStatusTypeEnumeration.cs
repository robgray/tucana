﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{    
    public enum SearchContractStatusType
    {                
        All = 0,    
        Active = 1,   
        [EnumDescription("Use Status")]
        UseStatus = 2
    };
}
