﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{
    public enum CompanySearch : int
    {
        [EnumDescription("Company Name")]
        CompanyName = 0,
        ABN = 1
    };
}
