﻿namespace AirtimeBilling.Core.Enums
{
    public enum CallVolumeUnit 
    {
        Time = 1,
        Data, 
        Discrete // SMS
    }
}
