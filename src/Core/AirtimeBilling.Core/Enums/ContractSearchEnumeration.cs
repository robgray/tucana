﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{
    /// <summary>
    /// How to find the Contracts
    /// </summary>    
    public enum ContractSearchField 
    {        
        [EnumDescription("Contract Number")]
        ContractNumber = 2,
        [EnumDescription("Account Number")]
        AccountNumber = 3,
        [EnumDescription("Contact Number")]
        ContactName = 4,
        [EnumDescription("Phone Number")]
        PhoneNumber = 5     
    }
}
        