﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{    
    /// <summary>
    /// How to bill for the contract
    /// </summary>    
    public enum BillingMethod
    {        
        None = 0,     
        [EnumDescription("Credit Card")]
        CreditCard = 1,        
        Invoice = 2
    }
}
