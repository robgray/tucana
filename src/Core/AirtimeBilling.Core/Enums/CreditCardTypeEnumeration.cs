﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{    
    public enum CreditCardType
    {
        [EnumDescription("Not Supplied")]
        NotSupplied = 0,     
        Visa = 1,
        [EnumDescription("American Express")]
        AmericanExpress = 2,     
        Mastercard = 3
    }
}
