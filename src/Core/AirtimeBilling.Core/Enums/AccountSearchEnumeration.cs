﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{
    public enum AccountSearch 
    {
        [EnumDescription("Account Number")]
        AccountNumber,        
        Contact,        
        Company
    };
}
