﻿using AirtimeBilling.Common;

namespace AirtimeBilling.Core.Enums
{
    /// <summary>
    /// The contract statuses    
    /// </summary>
    /// <remarks>
    /// A contract should only be in the NewContract status before it has been saved for the first time
    /// If a database record has this status, an exception occurred when creating the contract    
    /// 
    /// Application Submitted contracts have been sumbitted by the Agent but not yet viewed by I staff.
    /// Contracts created by Agents are initially in this state.
    /// 
    /// Application Pending contracts have not yet had a signed Airtime Contract returned but the process 
    /// has been started by I.  Contracts created by I are initially in this status.
    /// 
    /// Active contracts have a signed and returned Airtime Contract.
    /// 
    /// Suspended contracts have been placed on temporary suspension.
    /// 
    /// Closed contracts are no longer in use.  Either terminated early or past their contract end date and cancelled by the customer.
    /// </remarks>        
    public enum ContractStatus
    {
        [EnumDescription("New Contract")]
        NewContract = 0,
        [EnumDescription("Application Submitted")]
        ApplicationSubmitted = 1,
        [EnumDescription("Application Pending")]
        ApplicationPending = 2,        
        Active = 3,        
        Suspended = 4,        
        Closed = 5
    }
}
