﻿using System;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class AirtimeProvider : EntityBase 
    {        
        public AirtimeProvider() { }

        public AirtimeProvider(int id) : base(id) { }

        public virtual string Name { get; set; }

        public virtual string ImporterFullName { get; set; }        
    }
}
