﻿namespace AirtimeBilling.Core.Entities
{ 
    public class NetworkTariff : Tariff 
    {
        public NetworkTariff() { }

        public NetworkTariff(int id) : base(id) { } 

        public int NetworkId { get; set; }       
    }
}
