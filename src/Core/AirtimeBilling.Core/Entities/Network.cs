﻿
using System.Collections.Generic;

namespace AirtimeBilling.Core.Entities
{    
    public class Network : EntityBase
    {
        public Network() { }

        public Network(int id) : base(id) { } 

        public string Name { get; set; }
     
        public int AirtimeProviderId { get; set; }
     
        public string AirtimeProvider { get; set; }

        public bool AcceptsNewContracts { get; set; }
        
        public override IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(Name))
                yield return new RuleViolation("Network name required", "Name");

            yield break;
        }
    }
}
