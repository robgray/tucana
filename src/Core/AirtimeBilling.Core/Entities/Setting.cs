﻿namespace AirtimeBilling.Core.Entities
{
    public class Setting : EntityBase 
    {
        public Setting() { }

        public Setting(int id) : base(id) { }

        public virtual string Name { get; set; }

        public virtual string Value { get; set; }

        public virtual bool IsSystem { get; set; }        
    }
}
