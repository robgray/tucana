﻿namespace AirtimeBilling.Core.Entities
{   
    public class AgentUser : User 
    { 
        public int AgentId { get; set; }
		public override string HomePage
		{
			get
			{
			    return "~/Agent/Welcome.aspx";
			}
		}
        public override string MenuFile
        {
            get
            {
                return "~/AgentMenu.xml";
            }
        }
    }
}
