﻿namespace AirtimeBilling.Core.Entities
{
    public class EwayProcessedData : EntityBase
    {
        public EwayProcessedData() { }
        public EwayProcessedData(int id) : base(id) { }

        public long EwayTransactionReference { get; set; }
        public string CardHolder { get; set; }
        public decimal Amount { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string InvoiceNumber { get; set; }
        public string EwayInvoiceReference { get; set; }
    }
}
