﻿using System;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Core.Entities
{    
    [Serializable]
    public class Contract : EntityBase 
    {
        private ContractStatus _contractStatus;
        
        public Contract(DateTime orderRecievedDate) 
        {
            OrderReceivedDate = orderRecievedDate;
            _contractStatus = ContractStatus.NewContract;
            StatusChangedDate = orderRecievedDate;
        }

        public Contract(int id, DateTime orderReceivedDate) : base(id)
        {
            OrderReceivedDate = orderReceivedDate;
        }

        public virtual string ContractNumber { get; set; }

        public virtual int PlanId { get; set; }

        public virtual DateTime ActivationDate { get; set; }

        public virtual DateTime EndDate { get; set; }

        public virtual string IMEINumber { get; set; }

        public virtual string SimCard { get; set; }

        public virtual bool Data { get; set; }

        public virtual bool MessageBank { get; set; }

        public virtual string UsedBy { get; set; }

        public virtual int AccountId { get; set; }

        public virtual DateTime OrderReceivedDate { get; private set; }

        public virtual ContractStatus ContractStatus 
        {
            get
            {
                return _contractStatus;
            }
            set
            {
                if (_contractStatus != value)
                {
                    _contractStatus = value;
                    StatusChangedDate = DateTime.Now;                    
                }
            }
        }

        public virtual int? AgentId { get; set; }

        public virtual string PhoneNumber1 { get; set; }

        public virtual string PhoneNumber2 { get; set; }

        public virtual string PhoneNumber3 { get; set; }

        public virtual string PhoneNumber4 { get; set; }

        public virtual string Pin { get; set; }

        public virtual string Puk { get; set; }

        public virtual DateTime StatusChangedDate { get; set; }

        public virtual DateTime? SuspensionStartDate { get; set; }

        public virtual DateTime? SuspendedUntilDate { get; set; }

        public virtual bool IsTerminated
        {
            get { return ContractStatus == ContractStatus.Closed && StatusChangedDate < EndDate;}
        }

        public virtual TimeSpan RemainingTimeOnContract
        {
            get { return EndDate - StatusChangedDate; }
        }

       
        public override System.Collections.Generic.IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(ContractNumber))
                yield return new RuleViolation("Contract Number required", "ContractNumber");

            if (AgentId <= 0)
                yield return new RuleViolation("Agent required", "AgentId");

            if (AccountId <= 0)
                yield return new RuleViolation("Account required", "AccountId");

            if (ActivationDate > EndDate)
                yield return new RuleViolation("Contract cannot start after it has ended", "Activation Date");
            
            if (string.IsNullOrEmpty(IMEINumber))
                yield return new RuleViolation("An IMEI Number must be supplied");

            if (string.IsNullOrEmpty(PhoneNumber1) && (ContractStatus == ContractStatus.Active ||
                ContractStatus == ContractStatus.Suspended || ContractStatus == ContractStatus.Closed))
                yield return new RuleViolation("The first phone number is required", "PhoneNumber1");

            if (PlanId <= 0)
                yield return new RuleViolation("Plan required", "PlanId");

            if (SuspendedUntilDate <= ActivationDate)
                yield return
                    new RuleViolation("Cannot suspend the contract before the activation date", "SuspendUntilDate");

            if (SuspendedUntilDate > EndDate)
                yield return
                    new RuleViolation("Cannot suspend the contract after the contract end date", "SuspendUntilDate");

            if (ContractStatus == ContractStatus.Suspended && SuspendedUntilDate < StatusChangedDate)
                yield return new RuleViolation("Suspend until date must be after the status changed date", "SuspendUntilDate");

            yield break;
        }
    }
}
