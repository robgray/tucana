﻿namespace AirtimeBilling.Core.Entities
{
    public class ContactUser : User 
    {
        public int ContactId { get; set; }

		public override string HomePage
		{
			get
			{
				return "~/Customer/Welcome.aspx";
			}
		}

        public override string MenuFile
        {
            get
            {
                return "~/CustomerMenu.xml";
            }
        }
    }
}
