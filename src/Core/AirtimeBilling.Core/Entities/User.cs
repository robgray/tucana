﻿using System.Collections.Generic;
using System.Linq;

namespace AirtimeBilling.Core.Entities
{    
    public class User 
    {        
        public string Username { get; set; }

        public virtual bool IsAuthenticated { get; set; }

        public virtual bool IsActive { get; set; }

        public virtual string Name { get; set; }

        public virtual string Email { get; set; }

        public virtual bool IsValid
        {
            get { return GetRuleViolations().Count() == 0; }
        }		

    	public virtual string HomePage
    	{
			get { return "~/BackOffice/Welcome.aspx"; }
    	}

        public virtual string MenuFile
        {
            get { return "~/IMenu.xml"; }
        }

        public IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(Username))
                yield return new RuleViolation("Username required", "Username");

            if (string.IsNullOrEmpty(Email))
                yield return new RuleViolation("Email required", "Email");

            yield break;   
        }        
    }
}
