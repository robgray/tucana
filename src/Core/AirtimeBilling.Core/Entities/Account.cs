﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class Account : EntityBase
    {
        public Account() { }

        public Account(int id) : base(id) { }

        public virtual string AccountNumber { get; set; }

        public virtual string AccountName { get; set; }

        public virtual string ContactRole { get; set; }

        public virtual bool EmailBillDataFile { get; set; }

        public virtual bool EmailBill { get; set; }

        public virtual bool PostBill { get; set; }

        public virtual BillingMethod BillingMethod { get; set; }

        public virtual BillingAddressType BillingAddressType { get; set; }
        
        //public Contact Contact { get; set; }
        public virtual int ContactId { get; set; }

        public virtual bool IsInvoiceRoot { get; set; }

        public virtual string Password { get; set; }

        public virtual bool HasRequestedInvoicing { get; set; }

        public virtual bool IsInternational { get; set; }

        public virtual decimal AmountPaid { get; set; }

        public virtual decimal PreviousBill { get; set; }

        public void MakePayment(decimal amount)
        {
            MakePayment(amount, null);
        }

        public void MakePayment(decimal amount, string invoiceNumber)
        {
            AmountPaid += amount;

            if (string.IsNullOrEmpty(invoiceNumber))
                LogActivity(string.Format("Made payment of {0:c}", amount));
            else {
                LogActivity(string.Format("Made payment of {0:c} on Invoice '{1}'", amount, invoiceNumber));
            }
        }

        public Invoice CreateInvoice()
        {
            Invoice invoice;
            invoice = IsInternational ? new NoGstInvoice() : new Invoice();

            // Can update invoice properties that rely on account data here.


            return invoice;
        }

        #region Business Rules

        public override IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(AccountName))
                yield return new RuleViolation("Account Name required", "AccountName");

            if (string.IsNullOrEmpty(AccountNumber))
                yield return new RuleViolation("Acocunt Number required", "AccountNumber");

            if (string.IsNullOrEmpty(Password))
                yield return new RuleViolation("Password required", "Password");
            
            yield break;
        }

        #endregion 

    }
}
