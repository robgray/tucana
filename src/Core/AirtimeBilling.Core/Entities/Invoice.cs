﻿using System;
using System.Linq;
using System.Collections.Generic;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Core.Entities
{    
    public class Invoice : EntityBase 
    {
        protected IList<InvoiceLine> _lines;
        protected IList<int> _accountIds;

        public Invoice()
        {
            _lines = new List<InvoiceLine>();
            _accountIds = new List<int>();
        }

        public Invoice(int id) : base(id)
        {
            _lines = new List<InvoiceLine>();
            _accountIds = new List<int>();
        }

        public Invoice(int id, IList<InvoiceLine> lines, IList<int> accountIds)
            : base(id)
        {
            _lines = lines;
            _accountIds = accountIds;
        }

        public virtual string InvoiceNumber { get; set; }
 
        public virtual DateTime InvoiceDate { get; set; }
 
        public virtual string To { get; set; }
 
        public virtual Address InvoiceAddress { get; set; }
 
        public virtual DateTime FromDate { get; set; }
 
        public virtual DateTime ToDate { get; set; }
 
        public virtual int InvoiceRunNumber { get; set; }
        
        public virtual IEnumerable<int> AccountIds 
        {
            get { return _accountIds.AsEnumerable(); }
        }

        public virtual void AddAccount(Account account)
        {
            _accountIds.Add(account.Id.Value);
        }

        public virtual void AddAccount(int accountId)
        {
            _accountIds.Add(accountId);
        }

        public virtual bool HasData { get { return _accountIds.Count > 0;  } }

        // Only exists as a helper for emailing invoices.        
        public virtual string Email { get; set; }

        public virtual int InvoiceRootAccountId { get; set; }
       
        public virtual IEnumerable<InvoiceLine> Lines
        {
            get
            {
                return _lines.AsEnumerable();
            }
        }

        public virtual void AddLine(InvoiceLine line)
        {            
            line.Invoice = this;
            _lines.Add(line);
        }
        
        public virtual void AddLine(string description, double quantity, decimal lineAmount, Call call, Contract contract)
        {
            var line = new InvoiceLine
                           {
                               Description = description,
                               Quantity = quantity,
                               LineAmount = lineAmount,
                               GstAmount = lineAmount/11M,
                               Call = call,
                               Invoice = this
                           };

            if (contract != null)
                line.ContractId = contract.Id.Value;

            _lines.Add(line);
        }

        public virtual bool HasFailedPayment { get; set; }

        public virtual decimal AmountPaid { get; set; }

        public virtual decimal PreviousBill { get; set; }

        public virtual decimal Outstanding { get; set; }

        public virtual decimal NewCharges { get; set; }
        
        public virtual decimal CurrentBill { get; set; }

        public virtual DateTime PaymentDue { get; set; }

        public virtual decimal TotalIncGst { get; set; }

        public virtual decimal TotalGst { get; set; }

        public virtual decimal TotalExGst { get; set; }

        public virtual bool IsContractBillable(Contract contract)
        {
            if ((contract.ContractStatus == ContractStatus.Active || contract.ContractStatus == ContractStatus.Suspended) 
                && contract.ActivationDate <= ToDate)
                return true;

            if (HasContractBeenClosedInThisInvoicePeriod(contract))
                return true;

            // As far as this invoice is concerned, this contract is billable, 
            // as it was not closed before the end of the invoice period.
            if (HasContractBeenClosedAfterThisInvoicePeriod(contract))
                return true;

            return false;
        }

        public virtual bool HasContractBeenClosedInThisInvoicePeriod(Contract contract)
        {
            if (contract.ContractStatus == ContractStatus.Closed &&
                    contract.StatusChangedDate >= this.FromDate &&
                    contract.StatusChangedDate <= this.ToDate)
                return true;

            return false;
        }

        public virtual bool HasContractBeenClosedAfterThisInvoicePeriod(Contract contract)
        {
            if (contract.ContractStatus == ContractStatus.Closed &&
                    contract.StatusChangedDate > this.ToDate)
                return true;

            return false;
        }

        /// <summary>
        /// Makes a payment on this Invoice, increasing the Amount Paid and reducing the Current Balance amount.
        /// </summary>
        /// <param name="amount">Amount to pay</param>
        /// <remarks>
        /// Ideally this would also mark the payment on the Root Account... 
        /// </remarks>
        public virtual decimal MakePayment(decimal amount)
        {
            LogActivity(string.Format("Made payment of {0:c}", amount));

            AmountPaid += amount;            
            CalculateTotals();

            return 0;
        }

        public virtual void MakeCatchupPayment(decimal amount)
        {
            if (amount > 0) {
                AmountPaid += amount;
                CalculateTotals();

                LogActivity(string.Format("Made catchup payment of {0:c}", amount));
            }

            HasFailedPayment = false;            
        }

        public virtual decimal CalculateTotals()
        {
            decimal gap = 0;
            Outstanding = PreviousBill - AmountPaid;
            if (Outstanding <= 0.01M)
            {
                if (Outstanding > 0)
                {
                    gap = Outstanding;
                    AmountPaid += Outstanding;
                }            
                Outstanding = 0;
            }

            CurrentBill = PreviousBill + NewCharges - AmountPaid;

            // 001-155 : Total is "total for this invoice" ie. new charges
            TotalIncGst = NewCharges;
            TotalGst = CalculateTotalGst(TotalIncGst);
            TotalExGst = TotalIncGst - TotalGst;

            return gap;
        }

        public virtual decimal CalculateTotalGst(decimal totalIncGst)
        {
            return totalIncGst / 11M;
        }
               
        public virtual decimal CalculateContractStartProrataRate(Contract contract)
        {
            const decimal TotalDays = 365.25M/12M;

            if (contract.ActivationDate <= FromDate || contract.ActivationDate > ToDate)
            {
                return 1;
            }

            var numberOfDays = (int)(contract.ActivationDate - FromDate).TotalDays;
            if (numberOfDays >= 365.25M / 12M) return 1;
            
            return 1 - numberOfDays/TotalDays;            
        }
    }
}
