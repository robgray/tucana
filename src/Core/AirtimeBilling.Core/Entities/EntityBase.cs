﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Logging;

namespace AirtimeBilling.Core.Entities
{    
    [Serializable]
    public abstract class EntityBase
    {                
        protected EntityBase() { } 
        
        protected EntityBase(int id)
        {
            Id = id;
        }

        public int? Id { get; private set; }
     
        public void Inserted(int keyId)
        {
            Id = keyId;
        }

        public bool IsValid
        {
            get { return GetRuleViolations().Count() == 0; }
        }

        public virtual IEnumerable<RuleViolation> GetRuleViolations()
        {
            yield break;
        }

        /// <summary>
        /// Returns a string list of all errors on this object.
        /// </summary>
        /// <returns></returns>
        public string GetRuleViolationsErrorString()
        {
            if (IsValid) return string.Empty;

            var message = new StringBuilder();
            foreach (var violation in GetRuleViolations())
            {
                message.AppendLine(violation.ToString());
            }

            return message.ToString();
        }

        /// <summary>
        /// Entity name indicates what table the entity is stored in 
        /// </summary>
        /// <returns></returns>
        public virtual string GetEntityName()
        {
            return GetType().Name;
        }

        public void LogActivity(string activityMessage)
        {
            if (Id.HasValue) {
                LoggingUtility.WriteActivity(GetEntityName(), Id.Value, activityMessage);
            }
        }

        public Note CreateNote()
        {
            if (!Id.HasValue)
                return null;

            return new Note
                       {
                           NoteType = GetEntityName(),
                           EntityId = Id.Value
                       };
        }
    }
}
