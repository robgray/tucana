﻿using System;
using System.Collections.Generic;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class Contact : EntityBase
    {
        private string _firstName;
        private string _lastName;

        public Contact()
        {
            _firstName = string.Empty;
            _lastName = string.Empty;
            Initialize();
        }

        public Contact(int id) : base(id)
        {
            Initialize();
        }

        public virtual string Name 
        { 
            get
            {
                return (_firstName + " " + _lastName).Trim();
            }
            set
            {
                if (!string.IsNullOrEmpty(value)) {
                    var names = value.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);

                    _firstName = string.Empty;
                    _lastName = string.Empty;

                    if (names.Length == 0) return;
                    _lastName = names[names.Length - 1];
                    if (names.Length > 1) {
                        for (var i = 0; i < names.Length - 1; i++) {
                            if (_firstName.Length > 0) _firstName += " ";
                            _firstName += names[i];
                        }
                    }
                }
                else {
                    _firstName = string.Empty;
                    _lastName = string.Empty;
                }
            }
        }

        public virtual string FirstName
        {
            get { return _firstName; }            
        }

        public virtual string LastName
        {
            get { return _lastName; }
        }

        public virtual string Email { get; set; }

        public virtual string MobilePhone { get; set; }

        public virtual string FaxNumber { get; set; }

        public virtual string WorkPhone { get; set; }

        public virtual CreditCard CreditCard { get; set; }

        public virtual DriversLicense DriversLicense { get; set; }

        public virtual Address HomeAddress { get; set; }

        public virtual Address PostalAddress { get; set; }

        protected void Initialize()
        {
            this.HomeAddress = Address.Empty;
            this.PostalAddress = Address.Empty;
            this.DriversLicense = new DriversLicense();
            this.CreditCard = new CreditCard();
        }

        public override IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(Name))
                yield return new RuleViolation("Contact Name required", "Name");

            if (string.IsNullOrEmpty(Email))
                yield return new RuleViolation("Contact Email required", "Email");

            if (CreditCard == null)
                yield return new RuleViolation("Credit Card Information required", "CreditCard");

            
                

            yield break;
        }
    }
}
