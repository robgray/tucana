﻿using System;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Core.Entities
{    
    public class Call : EntityBase
    {
        private string _displayVolume;
        private string _numberCalled;

        public Call() { }
     
        public Call(int id) : base(id) { }

        public virtual int ContractId { get; set; }

        public virtual string Tariff { get; set; }

        public virtual DateTime CallTime { get; set; }
        
        public virtual string PhoneNumber { get; set; }
        
        public virtual string NumberCalled 
        { 
            get
            {
                if (CallType == CallVolumeUnit.Data)
                    return "Data";
                if (CallType == CallVolumeUnit.Discrete)
                    return "SMS";
                return _numberCalled;  
            }
            set
            {
                _numberCalled = value;
            }
        }
        
        public virtual string CalledFrom { get; set; }

        /// <summary>
        /// AKA Duration property.  Should reflect the Duration 
        /// formula from the Invoice.  This gets filled by the DAL
        /// But will be calcualted, in the NHibernate version!
        /// </summary>
        public virtual decimal Volume { get; set; }

        public virtual string DisplayVolume
        { 
            get
            {
                if (CallType == CallVolumeUnit.Time) {
                    // format
                    var hours = Math.Floor(Volume);
                    var minutes = Math.Round((Volume - hours)*60);
                    return string.Format("{0}:{1:00}", hours, minutes);                    
                }
                if (CallType == CallVolumeUnit.Data) {
                    return string.Format("{0:0.00} MB", Volume);
                }                   
                return string.Format("{0:0}", Volume);                
            }
            set { _displayVolume = value; }
        }

        /// <summary>
        /// The Volume as it was imported from the Call Data
        /// </summary>
        public virtual decimal ActualVolume { get; set; }
        
        public virtual decimal UnitsOfTime { get; set; }

        public virtual decimal UnitCost { get; set; }

        public virtual decimal Cost { get; set; }
        
        public virtual bool HasFreeCallTariff { get; set; }
        
        public virtual string ImportedCallType { get; set; }

        public virtual CallVolumeUnit CallType { get; set; }               
    }
}
