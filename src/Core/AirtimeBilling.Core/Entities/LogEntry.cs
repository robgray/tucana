﻿using System;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Core.Entities
{
    public class LogEntry : EntityBase
    {
        public LogEntry() { }

        public LogEntry(int id) : base(id) { }
        
        public DateTime Timestamp { get; set; }

        public string Username { get; set; }

        public LoggingLevels Level { get; set; }

        public string MethodName { get; set; }

        public string ClassName { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }

    }
}
