﻿namespace AirtimeBilling.Core.Entities
{    
    public class PlanTariff : Tariff
    {
        private readonly int networkTariffId;
        
        public PlanTariff(int networkTariffId) : base()
        {
            this.networkTariffId = networkTariffId;
        }

        public PlanTariff(int id, int networkTariffId)
            : base(id)
        {
            this.networkTariffId = networkTariffId;
        }

        public virtual decimal UnitCost { get; set; }

        public virtual int PlanId { get; set; }

        public virtual int NetworkTariffId 
        {
            get { return this.networkTariffId; }
        }        
    }
}
