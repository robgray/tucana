﻿using System;

namespace AirtimeBilling.Core.Entities
{    
    [Serializable]
    public class MasterAccount : Account 
    {
        public MasterAccount() { }

        public MasterAccount(int id) : base(id) { }

        public virtual int? CompanyId { get; set; }

        public SubAccount CreateSubAccount(string accountNumber, string accountName)
        {
            var subAccount = new SubAccount()
                                        {
                                            AccountName = accountName,
                                            AccountNumber = accountNumber,
                                            ContactId = this.ContactId,
                                            ContactRole = this.ContactRole,
                                            PreviousBill = 0,
                                            AmountPaid = 0,
                                            BillingAddressType = this.BillingAddressType,
                                            BillingMethod = this.BillingMethod,
                                            IsInternational = this.IsInternational,
                                            IsInvoiceRoot = false,
                                            EmailBill = this.EmailBill,
                                            EmailBillDataFile = this.EmailBillDataFile,
                                            Password = this.Password,
                                            PostBill = this.PostBill,
                                            MasterAccountId = this.Id.Value
                                        };

            return subAccount;
        }
    }
}
