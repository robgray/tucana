﻿using System;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class Company : EntityBase
    {
        public Company()
        {
            Initialize();
        }

        public Company(int id) :  base(id)
        {
            Initialize();
        }

        public virtual string CompanyName { get; set; }

        public virtual string ABN { get; set; }

        public virtual string ACN { get; set; }

        public virtual Address Address { get; set; }

        public virtual Address PostalAddress { get; set; }        

        protected void Initialize()
        {
            Address = Address.Empty;
            PostalAddress = Address.Empty;
        }

        public override System.Collections.Generic.IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(CompanyName))
                yield return new RuleViolation("Company Name required", "CompanyName");

            // TODO check for valid ABN and ACN.

            yield break;
        }
    }
}
