﻿using System.Collections.Generic;

namespace AirtimeBilling.Core.Entities
{    
    public abstract class Tariff : EntityBase
    {
        protected Tariff() { }

        protected Tariff(int id) : base(id) { } 

        public virtual string Code { get; set; }

        public virtual string Name { get; set; }

        public virtual bool IsCountedInFreeCall { get; set; }

        public virtual bool HasFlagfall { get; set; }

        public override IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(Code))
                yield return new RuleViolation("Tariff Code required", "Code");

            if (string.IsNullOrEmpty(Name))
                yield return new RuleViolation("Tariff Name required", "Name");

            yield break;
        }
    }
}
