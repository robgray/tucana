﻿using System;

namespace AirtimeBilling.Core.Entities
{    
    [Serializable]
    public class Agent : EntityBase
    {
        public Agent()
        {
            Address = Address.Empty;
            CommissionPercent = 0;            
        }

        public Agent(int id) : base(id) { } 

        public string AgentName { get; set; }
 
        public Address Address { get; set; }
 
        public decimal CommissionPercent { get; set; }

        public string Email { get; set; }
       
        public override System.Collections.Generic.IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(AgentName))
                yield return new RuleViolation("Agent Name required", "AgentName");

            if (Address == null)
                yield return new RuleViolation("Agent Address required", "Address");

            if (Address != null && string.IsNullOrEmpty(Address.Street))
                yield return new RuleViolation("Agent Street Address required", "Address.Street");

            if (Address != null && string.IsNullOrEmpty(Address.Suburb))
                yield return new RuleViolation("Agent Suburb required", "Address.Suburb");

            if (string.IsNullOrEmpty(Email))
                yield return new RuleViolation("Email required", "Email");

            if (CommissionPercent < 0 || CommissionPercent > 100)
                yield return new RuleViolation("Commission must be between 0 and 100");
                    
            yield break;
        }
    }
}
