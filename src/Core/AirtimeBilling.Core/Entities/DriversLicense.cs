﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class DriversLicense 
    {
        public virtual string LicenseNumber { get; set; }

        public virtual DateTime DateOfBirth { get; set; }

        public virtual DateTime Expiry { get; set; }

        public virtual bool IsValid
        {
            get { return GetRuleViolations().Count() == 0; }
        }

        public virtual IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (DateOfBirth >= DateTime.Now)
                yield return new RuleViolation("Cannot be born in the future", "DateOfBirth");

            if (Expiry <= DateTime.Now)
                yield return new RuleViolation("License has expired");

            yield break;
        }
    }
}
