﻿using System;

namespace AirtimeBilling.Core.Entities
{    
    [Serializable]
    public class Address
    {
        public virtual string Street { get; set; }

        public virtual string Street2 { get; set; }

        public virtual string Suburb { get; set; }

        public virtual string State { get; set; }

        public virtual string Postcode { get; set; }

        public static Address Empty
        {
            get
            {
                return new Address {Street = "", Street2 = "", Postcode = "", State = "", Suburb = ""};
            }
        }

        public override string ToString()
        {                
            return (((((Street ?? "") + " " + (Street2 ?? "")).Trim() + " " + (Suburb ?? "")).Trim() + " " + (State ?? "")).Trim() + " " + (Postcode ?? "")).Trim();
        }

        public virtual string Display { get { return ToString(); } }
    }
}
