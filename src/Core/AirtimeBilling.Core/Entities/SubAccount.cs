﻿using System;

namespace AirtimeBilling.Core.Entities
{    
    [Serializable]
    public class SubAccount : Account
    {
        public SubAccount() { }

        public SubAccount(int id) : base(id) { } 

        public virtual int MasterAccountId { get; set; }
    }
}
