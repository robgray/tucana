﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class CreditCard
    {
        public virtual string CardNumber { get; set; }

        public virtual string NameOnCard { get; set; }

        public virtual string ExpiryDate { get; set; }

        public virtual CreditCardType CreditCardType { get; set; }

        public virtual bool IsValid
        {
            get { return GetRuleViolations().Count() == 0;  }
        }

        public virtual IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(CardNumber))
                yield return new RuleViolation("Credit Card Number required", "CardNumber");

            if (string.IsNullOrEmpty(NameOnCard))
                yield return new RuleViolation("Credit Card holder name required", "NameOnCard");

            if (string.IsNullOrEmpty(ExpiryDate))
                yield return new RuleViolation("Credit Card Expiry Date required (format mm/yy)", "ExpiryDate");

            DateTime expiry = DateTime.Parse("01/" + ExpiryDate).AddMonths(1);
            if (expiry <= DateTime.Now)
                yield return new RuleViolation("Credit Card has expired");

            if (CreditCardType == CreditCardType.NotSupplied)
                yield return new RuleViolation("Credit Card Type required", "CreditCardType");

            

            yield break;
        }

    }
}
