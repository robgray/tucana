﻿using System;
using System.Collections.Generic;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class Plan : EntityBase
    {
        public Plan() { }

        public Plan(int id) : base(id) { } 
        
        public string Name { get; set; }
     
        public int Duration { get; set; }
     
        public decimal FlagFall { get; set; }
     
        public decimal FreeCallAmount { get; set; }
     
        public decimal PlanAmount { get; set; }
     
        public int NetworkId { get; set; }
     
        public int DefaultTariffId { get; set; }

        public int UnitOfTime { get; set; }
      
        public virtual decimal RemainingContractValue(Contract contract)
        {
            var remainingContractValue = ((PlanAmount * 12) / 365.25M) * (int)contract.RemainingTimeOnContract.TotalDays;

            return remainingContractValue;
        }

        public override IEnumerable<RuleViolation> GetRuleViolations()
        {
            if (string.IsNullOrEmpty(Name))
                yield return new RuleViolation("Plan Name required", "Name");

            if (Duration <= 0)
                yield return new RuleViolation("Plan duration must be greater than 0", "Duration");

            if (FlagFall < 0)
                yield return new RuleViolation("Flagfall must be at least 0", "FlagFall");

            if (FreeCallAmount < 0)
                yield return new RuleViolation("Free call amount must be at least 0", "FreeCallAmount");

            if (PlanAmount < 0)
                yield return new RuleViolation("Plan Amount must be at least 0", "PlanAmount");

            if (NetworkId <= 0)
                yield return new RuleViolation("Network required", "NetworkId");

            if (DefaultTariffId <= 0)
                yield return new RuleViolation("Default tariff required", "NetworkTariffId");

            if (FreeCallAmount > PlanAmount)
                yield return new RuleViolation("Plan free call amount is greater than the plan amount", "FreeCallAmount");

            if (UnitOfTime <= 0)
                yield return new RuleViolation("Units of Time must be greater than 0", "UnitOfTime");

            yield break;
        }
    }
}
