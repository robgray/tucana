﻿
namespace AirtimeBilling.Core.Entities
{
    public class InvoiceLine : EntityBase 
    {
        public InvoiceLine() { }

        public InvoiceLine(int id) : base(id) { }

        public virtual Invoice Invoice { get; set; }

        public virtual string Description { get; set; }

        public virtual double Quantity { get; set; }

        public virtual decimal GstAmount { get; set; }

        public virtual decimal LineAmount { get; set; }

        public virtual int? ContractId { get; set; }

        public virtual Call Call { get; set; }        
    }
}
