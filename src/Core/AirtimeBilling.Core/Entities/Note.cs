﻿using System;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class Note : EntityBase
    {
        public Note() { }

        public Note(int id) : base(id) { }

        // BAD BUT I JUST DON'T CARE ANYMORE - STUPID CUSTOMER! :(
        public virtual string NoteType { get; set; }

        public virtual int EntityId { get; set; }

        public virtual string Comment { get; set; }

        public virtual string Username { get; set; }

        public virtual DateTime NoteDate { get; set; }

        
    }
}
