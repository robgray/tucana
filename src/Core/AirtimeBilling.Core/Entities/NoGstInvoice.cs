﻿using System.Collections.Generic;

namespace AirtimeBilling.Core.Entities
{
    // This invoice is used for international customers.
    // See 001-158
    public class NoGstInvoice : Invoice
    {
        public NoGstInvoice() : base() { }
        
        public NoGstInvoice(int id) : base(id) { }
        
        public NoGstInvoice(int id, IList<InvoiceLine> lines, IList<int> accountIds)
            : base(id)
        {
            _lines = lines;
            _accountIds = accountIds;
        }

        public override decimal CalculateTotalGst(decimal totalIncGst)
        {
            return 0;
        }
    }
}
