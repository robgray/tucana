﻿using System;

namespace AirtimeBilling.Core.Entities
{       
    public class Activity : EntityBase
    {
        private DateTime _logDate;
        private string _user;
        private string _activity;

        public Activity(DateTime logDate, string user, string activity) : base()
        {
            this._logDate = logDate;
            this._user = user;
            this._activity = activity;
        }

        public Activity(int id, DateTime logDate, string user, string activity) : base(id)
        {
            this._logDate = logDate;
            this._user = user;
            this._activity = activity;
        }

        public virtual DateTime LogDate { get { return _logDate; } }

        public virtual string User { get { return _user; } }

        public virtual string ActivityName { get { return _activity; } }        
    }
}
