﻿using System;

namespace AirtimeBilling.Core.Entities
{
    [Serializable]
    public class CustomFee : EntityBase
    {
        public CustomFee() { }

        public CustomFee(int id) : base(id) { }

        public virtual string Description { get; set; }

        public virtual decimal Amount { get; set; }

        public virtual bool IsRecurring { get; set; }              
    }
}
