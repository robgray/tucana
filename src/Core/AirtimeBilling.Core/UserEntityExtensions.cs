﻿using System.Security.Principal;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core
{
    public static class UserEntityExtensions
    {
        public static IPrincipal GetUserPrincipal(this User user)        
        {            
            string role = "";
            if (user is AgentUser) role = "Agent";
            else if (user is ContactUser) role = "Customer";
            else role = "I";

            var identity = new GenericIdentity(user.Username);
            var principal = new GenericPrincipal(identity, new [] { role });
            return principal;         
        }
    }
}
