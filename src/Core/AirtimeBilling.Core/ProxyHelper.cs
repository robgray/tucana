﻿using System;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core
{
    public static class ProxyHelper
    {
        public static EntityBase GetProxyEntity(int id, string entityName)
        {
            if (entityName == "Contract")
                return new Contract(id, DateTime.Now);
            return null;
        }
    }
}
