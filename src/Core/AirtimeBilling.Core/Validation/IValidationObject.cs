﻿namespace AirtimeBilling.Core.Validation
{
    public interface IValidationObject
    {
        bool IsValid { get; }
        IBusinessRuleSet Validate();
    }
}
