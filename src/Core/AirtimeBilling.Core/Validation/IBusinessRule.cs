﻿namespace AirtimeBilling.Core.Validation
{
    /// <summary>
    /// IBusinessRule defines a basic business rule that provides a method to test the rule.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBusinessRule<T> : IRule
    {
        bool IsSatisfiedBy(T item);        
    }
}
