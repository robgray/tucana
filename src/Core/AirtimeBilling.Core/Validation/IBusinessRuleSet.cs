﻿using System.Collections.Generic;

namespace AirtimeBilling.Core.Validation
{
    public interface IBusinessRuleSet
    {
        IBusinessRuleSet BrokenBy(IValidationObject item);
        bool Contains(IRule rule);
        int Count { get; }
        IList<string> Message { get; }
        bool IsEmpty { get; }
    }
}
