﻿using System.Collections.Generic;

namespace AirtimeBilling.Core.Validation
{
    public class BusinessRuleSet<T> : IBusinessRuleSet where T : IValidationObject
    {
        private IList<IBusinessRule<T>> rules;

        public BusinessRuleSet(params IBusinessRule<T>[] rules) : this (new List<IBusinessRule<T>>(rules))
        {
        }

        public BusinessRuleSet(IList<IBusinessRule<T>> rules)
        {
            this.rules = rules;
        }

        public IBusinessRuleSet BrokenBy(IValidationObject item)
        {
            IList<IBusinessRule<T>> brokenRules = new List<IBusinessRule<T>>();

            foreach(IBusinessRule<T> rule in rules)
            {
                if (!rule.IsSatisfiedBy((T)item))
                {
                    brokenRules.Add(rule);
                }
            }
            return new BusinessRuleSet<T>(brokenRules);
        }

        public int Count
        {
            get { return rules.Count; }
        }

        public IList<string> Message
        {
            get
            {
                return new List<IBusinessRule<T>>(rules).ConvertAll<string>(rule => rule.Description);
            }
        }

        public bool Contains(IRule rule)
        {
            foreach (IBusinessRule<T> businessRule in rules)
            {
                if (rule.Name.EndsWith(businessRule.Name))
                {
                    return true;
                }
            }
            return false;
        }

        public bool IsEmpty
        {
            get { return Count == 0; }
        }
    }
}
