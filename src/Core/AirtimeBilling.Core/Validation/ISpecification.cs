﻿namespace AirtimeBilling.Core.Validation
{
    public interface ISpecification<T>
    {
        bool IsSatisfiedBy(T item);
    }
}
