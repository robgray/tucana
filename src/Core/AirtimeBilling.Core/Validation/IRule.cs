﻿namespace AirtimeBilling.Core.Validation
{
    public interface IRule
    {
        string Name { get; }
        string Description { get; }
    }
}
