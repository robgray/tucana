﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RGSS.LandwideCMS.Services.Entities
{
    [DataContract]
    [Serializable]
    public class Address
    {
        [DataMember]
        public string Street { get; set; }
        [DataMember]
        public string Suburb { get; set; }
        [DataMember]
        public string State { get; set; }
        [DataMember]
        public string Postcode { get; set; }
    }
}
