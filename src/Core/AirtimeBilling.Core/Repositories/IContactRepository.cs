﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IContactRepository : IRepository<Contact>
    {
        void InsertContact(Contact contact);

        bool UpdateContact(Contact contact);

        Contact GetContactEntity(int contactId);
        
        IList<Contact> GetAllContacts();

        IList<Contact> FindContactsByContactName(string contactName);

        IList<Contact> FindContactsByMobilePhone(string mobilePhone);

        void Delete(Contact contact);
    }
}
