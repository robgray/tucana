﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface INetworkRepository : IRepository<Network>
    {
        Network GetNetwork(int networkId);

        IList<Network> GetAllNetworks();

        IList<Network> GetAllActiveNetworks();

        void InsertNetwork(Network entity);

        bool UpdateNetwork(Network entity);

        bool DeleteNetwork(int networkId);
    }
}
