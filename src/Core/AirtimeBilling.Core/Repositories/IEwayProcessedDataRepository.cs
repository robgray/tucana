﻿
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IEwayProcessedDataRepository : IRepository<EwayProcessedData>
    {
        EwayProcessedData GetById(int id);

        EwayProcessedData GetByEwayTransactionReference(long transactionReference);

        void Insert(EwayProcessedData entity);
    }
}
