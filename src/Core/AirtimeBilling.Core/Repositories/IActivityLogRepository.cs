﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IActivityLogRepository
    {
        void Insert(Activity entity, string tableName, int keyId);

        IList<Activity> FindActivitiesByUser(string username);

        IList<Activity> FindActivitiesByEntity(EntityBase entity);
    }
}
