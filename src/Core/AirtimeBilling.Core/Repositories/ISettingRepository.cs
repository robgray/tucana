﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface ISettingRepository : IRepository<Setting>
    {
        Setting GetByName(string mame);

        IList<Setting> GetAll();

        IList<Setting> GetAllSystemSettings();

        IList<Setting> GetAllUserSettings();

        bool Commit(Setting entity);

        Setting this[string name] { get; set; }
    }
}
