﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IAccountRepository : IRepository<Account>
    {
        IList<Account> GetAllAccounts();

        Account GetAccount(int accountId);

        Account GetAccountByAccountNumber(string accountNumber);

        IList<Account> FindAccountsByAccountNumber(string accountNumber);

        IList<Account> FindAccountsByContact(string contactName);

        IList<Account> FindAccountsByCopmpany(string companyName);

        IList<Account> GetAllRootAccounts();

        IList<Account> GetAllSubAccounts(int masterAccountId);

        bool UpdateAccount(Account account);

        void InsertAccount(Account account);

        void Delete(Account account);
    }
}
