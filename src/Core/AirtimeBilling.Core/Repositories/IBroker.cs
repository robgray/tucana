using System.Collections.Generic;

namespace Mammoth.BigpondMusic.DataInterfaces
{
	public interface IReadOnlyBroker<T, IdT>
	{
		T GetById(IdT id);
	}
    public interface IBroker<T, IdT> : IReadOnlyBroker<T, IdT>
    {
        List<T> GetAll();
        List<T> GetByExample(T exampleInstance, params string[] propertiesToExclude);
        void Delete(T entity);
        T Commit(T entity);
    	void Refresh(T entity);
    	bool IsPersistent(T entity, IdT id);
        T Create();
    }
}
