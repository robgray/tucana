﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface ICustomFeeRepository : IRepository<CustomFee>
    {
        CustomFee GetById(int id);

        IList<CustomFee> GetAll();

        void Insert(CustomFee entity);

        void Delete(CustomFee entity);

        void Update(CustomFee entity);
    }
}
