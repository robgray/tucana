﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IPlanTariffRepository : IRepository<PlanTariff>
    {
        PlanTariff GetPlanTariff(int planTariffId);

        IList<PlanTariff> GetPlanTariffsByPlan(int planId);

        void InsertPlanTariff(PlanTariff entity);

        bool UpdatePlanTariff(PlanTariff entity);

        bool DeletePlanTariffsByNetworkTariff(int networkTariffId);
    }
}
