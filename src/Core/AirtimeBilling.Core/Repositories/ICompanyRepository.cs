﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface ICompanyRepository : IRepository<Company>
    {
        void InsertCompany(Company company);

        bool UpdateCompany(Company company);

        void Delete(Company entity);

        Company GetCompany(int companyId);

        IList<Company> GetAllCompanies();

        IList<Company> FindCompaniesByCompanyName(string companyName);

        IList<Company> FindCompaniesByABN(string ABN);
    }
}
