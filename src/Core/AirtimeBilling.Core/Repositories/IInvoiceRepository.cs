﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IInvoiceRepository
    {
        IList<Invoice> GetAllInvoices();

        Invoice GetByInvoiceNumber(string invoiceNumber);
        
        IList<Invoice> FindInvoicesByRunNumber(int runNumber);

        IList<Invoice> FindInvoicesByInvoiceNumber(string pattern);

        void InsertInvoice(Invoice entity);

        Invoice this[string invoiceNumber] { get; }

        bool UpdateInvoice(Invoice entity);   

        IList<Invoice> GetAllInvoiceForAccount(Account account);

        bool DeleteInvoice(Invoice entity);
    }
}
