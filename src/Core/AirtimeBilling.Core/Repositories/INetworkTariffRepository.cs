﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface INetworkTariffRepository : IRepository<NetworkTariff>
    {
        NetworkTariff GetNetworkTariff(int networkTariffId);

        IList<NetworkTariff> GetNetworkTariffsByNetworkId(int networkId);

        bool UpdateNetworkTariff(NetworkTariff entity);

        void InsertNetworkTariff(NetworkTariff entity);

        bool DeleteNetworkTariff(int networkTariffId);
    }
}
