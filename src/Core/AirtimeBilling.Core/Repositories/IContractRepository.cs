﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IContractRepository : IRepository<Contract>
    {
        IList<Call> GetNonInvoicedCallsByContractId(int contractId);

        bool UpdateCall(Call call);

        IList<Contract> GetAllContractsWithCustomFee(CustomFee customFee);

        Contract GetContract(int contractId);

        IList<Contract> GetAllContracts();

        IList<Contract> GetContractsByAccountId(int accountId);

        IList<Contract> FindByContractNumber(string contractNumber);

        IList<Contract> FindByAccountNumber(string accountNumber);

        IList<Contract> FindByPhoneNumber(string phoneNumber);

        IList<Contract> FindByContact(string contactName);

        IList<Contract> FindByPlanId(int planId);

        bool UpdateContract(Contract contract);

        void InsertContract(Contract contract);

        int InsertContractPlanVariance(Contract contract, Plan newPlan, DateTime startDate, DateTime endDate, string user, decimal cost);

        IList<CustomFee> GetAllCustomFeesForContract(Contract contract);

        bool AddCustomFee(int contractId, CustomFee customfee);

        bool RemoveCustomFee(int contractCustomFeeId);

        bool Delete(Contract contract);
    }
}
