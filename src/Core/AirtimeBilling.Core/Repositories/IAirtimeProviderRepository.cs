﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IAirtimeProviderRepository : IRepository<AirtimeProvider>
    {
        IList<AirtimeProvider> GetAirtimeProviders();
    }
}
