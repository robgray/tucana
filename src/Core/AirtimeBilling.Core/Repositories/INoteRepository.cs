﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface INoteRepository : IRepository<Note>
    {
        IList<Note> GetNotesFor(EntityBase entity);

        bool Insert(Note entity);       
    }
}
