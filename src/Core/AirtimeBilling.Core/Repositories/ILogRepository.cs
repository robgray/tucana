﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface ILogRepository
    {
        LogEntry GetById(int id);

        IList<LogEntry> Find(DateTime fromDate, DateTime toDate, string username, LoggingLevels? severity);
    }
}
