﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IInvoiceHardcopyRepository 
    {
        string Get(Invoice invoice);

        string Save(Invoice invoice);

        void Delete(Invoice invoice);

        bool Move(Invoice invoice);
    }
}
