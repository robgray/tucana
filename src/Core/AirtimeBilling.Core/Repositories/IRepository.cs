﻿namespace AirtimeBilling.Core.Repositories
{
    public interface IRepository<T>
    {
        T this[int id] { get; set; }        
    }
}
