﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IPlanRepository : IRepository<Plan>
    {
        Plan GetPlan(int planId);

        IList<Plan> GetPlansByNetworkId(int networkId);

        void InsertPlan(Plan plan);

        bool UpdatePlan(Plan plan);

        bool DeletePlan(int planId);

        IList<Call> GetCallsForPlan(int planId);
    }
}
