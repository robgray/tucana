﻿using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IUserRepository
    {
        User GetUser(string username, string password);
    }
}
