﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core.Repositories
{
    public interface IAgentRepository : IRepository<Agent>
    {
        Agent GetAgent(int agentId);
        
        IList<Agent> GetAllAgents();

        bool Update(Agent entity);

        bool Insert(Agent entity);
    }
}
