﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Core
{
    /// <summary>
    /// Compares InvoiceLine items in a collection.        
    /// </summary>
    public class InvoiceLineEqualityComparer : IEqualityComparer<InvoiceLine>
    {
        #region IEqualityComparer<InvoiceLine> Members

        public bool Equals(InvoiceLine x, InvoiceLine y)
        {
            if (x.Id.HasValue && y.Id.HasValue) return x.Id.Value == y.Id.Value;

            return x.Description == y.Description &&
                   x.ContractId == y.ContractId &&
                   x.GstAmount == y.GstAmount &&
                   x.LineAmount == y.LineAmount &&
                   x.Call == y.Call &&
                   x.Invoice == y.Invoice;            
        }

        public int GetHashCode(InvoiceLine obj)
        {
            return obj.GetHashCode();
        }

        #endregion
    }
}
