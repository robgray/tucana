﻿using System;
using System.Text;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Logging.Email
{
    internal class ExceptionEmail : EmailFormat
    {
        public string Username { get; set; }
        public Exception Ex { get; set; }

        protected override string GetNoHtml()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Exception received from AirtimeBilling");
            sb.AppendLine();
            sb.AppendLine();
            
            sb.AppendLine(string.Format("User: {0}", Username));
            sb.AppendLine();
            sb.AppendLine(string.Format("Source: {0}", Ex.Source));
            sb.AppendLine();
            sb.AppendLine(string.Format("Target Site: {0}", (Ex.TargetSite == null ? "Unknown" : Ex.TargetSite.Name)));
            sb.AppendLine();
            sb.AppendLine(string.Format("Message: {0}", Ex.Message));
            sb.AppendLine();
            sb.AppendLine(string.Format("Stack Trace: {0}", Ex.StackTrace));
            sb.AppendLine();

            return sb.ToString();
        }

        protected override string GetBody()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<div style='margin-bottom: 30px'>");
            sb.AppendLine("<b>Exception received from AirtimeBilling</b>");
            sb.AppendLine("</div>");
            sb.AppendLine("<table>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='tableheading'>");
            sb.AppendLine("User");
            sb.AppendLine("</td>");            
            sb.AppendLine("<td>");
            sb.AppendLine(Username);
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='tableheading'>");
            sb.AppendLine("Source");
            sb.AppendLine("</td>");
            sb.AppendLine("<td>");
            sb.AppendLine(Ex.Source);
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='tableheading'>");
            sb.AppendLine("Target Site");
            sb.AppendLine("</td>");
            sb.AppendLine("<td>");
            sb.AppendLine((Ex.TargetSite == null ? "Unknown" : Ex.TargetSite.Name));
            sb.AppendLine("</td>");            
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='tableheading'>");
            sb.AppendLine("Message");
            sb.AppendLine("</td>");
            sb.AppendLine("<td>");
            sb.AppendLine(Ex.Message);
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td class='tableheading'>");
            sb.AppendLine("Stack Trace");
            sb.AppendLine("</td>");
            sb.AppendLine("<td>");
            sb.AppendLine(Ex.StackTrace);
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("</table>");

            return sb.ToString();
        }

        protected override string GetCustomStyles()
        {
            return ".tableheading { width: 110px; text-weight: bold; }";
        }
    }
}
