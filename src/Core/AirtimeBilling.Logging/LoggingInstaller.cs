﻿using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using System.Messaging;
using AirtimeBilling.Common;

namespace AirtimeBilling.Logging
{
    /// <summary>
    /// This class handles the installation and uninstallation of the message queues relating to logging, email, etc.
    /// </summary>
    [RunInstaller(true)]
    public partial class LoggingInstaller : Installer
    {
        #region Overrides

        public override void Install(IDictionary stateSaver)
        {
            CreateMessageQueues();
            base.Install(stateSaver);
        }

        public override void Uninstall(IDictionary savedState)
        {
            if (MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.DebugMessageQueue)))
                MessageQueue.Delete(ConstructMessageQueueName(ConfigItems.DebugMessageQueue));
            if (MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.ActivityMessageQueue)))
                MessageQueue.Delete(ConstructMessageQueueName(ConfigItems.ActivityMessageQueue));
            if (MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.EmailMessageQueue)))
                MessageQueue.Delete(ConstructMessageQueueName(ConfigItems.EmailMessageQueue));
            if (MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.ExceptionMessageQueue)))
                MessageQueue.Delete(ConstructMessageQueueName(ConfigItems.ExceptionMessageQueue));
            base.Uninstall(savedState);
        }

        #endregion 

        private void CreateMessageQueues()
        {
            if (!MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.ExceptionMessageQueue)))
                MessageQueue.Create(ConstructMessageQueueName(ConfigItems.ExceptionMessageQueue), true);
            if (!MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.DebugMessageQueue)))
                MessageQueue.Create(ConstructMessageQueueName(ConfigItems.DebugMessageQueue),true);
            if (!MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.ActivityMessageQueue)))
                MessageQueue.Create(ConstructMessageQueueName(ConfigItems.ActivityMessageQueue),true);
            if (!MessageQueue.Exists(ConstructMessageQueueName(ConfigItems.EmailMessageQueue)))
                MessageQueue.Create(ConstructMessageQueueName(ConfigItems.EmailMessageQueue),true);
        }

        private string ConstructMessageQueueName(string queueName)
        {
            return string.Format(".\\Private$\\{0}", queueName);
        }
    }
}
