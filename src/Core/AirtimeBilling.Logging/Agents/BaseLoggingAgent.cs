﻿using System.ServiceModel;

namespace AirtimeBilling.Logging.Agents
{
    public abstract class BaseLoggingAgent<T> where T : class
    {
        #region Private fields

        private EndpointAddress _addr = null;
        private NetMsmqBinding _binding = new NetMsmqBinding(NetMsmqSecurityMode.None);
        private string _contractName = typeof(T).ToString();
        private ChannelFactory<T> _factory = new ChannelFactory<T>();
        
        #endregion 

        public BaseLoggingAgent()
        {
            Initialise();
        }

        public BaseLoggingAgent(string queueName)
        {
            _addr = new EndpointAddress(string.Format("net.msmq://localhost/private/{0}",queueName));
            Initialise();
        }

        private void Initialise()
        {
            _factory.Endpoint.Address = _addr;
            _factory.Endpoint.Binding = _binding;
            _factory.Endpoint.Contract.ConfigurationName = _contractName;
            Channel = _factory.CreateChannel();
        }

        protected EndpointAddress Address
        {
            get { return _addr; }
            set { _addr = value; }
        }

        protected NetMsmqBinding Binding
        {
            get { return _binding; }
            set { _binding = value; }
        }

        protected string ContractName
        {
            get { return _contractName; }
            set { _contractName = value; }
        }

        protected ChannelFactory<T> ChannelFactory
        {
            get { return _factory; }
            set { _factory = value; }
        }

        protected T Channel { get; set; }

        public void Close()
        {
            _factory.Close();
        }
    }
}
