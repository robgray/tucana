﻿using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Logging.Agents
{
    public class ActivityMessageAgent : BaseLoggingAgent<IActivityMessage>, IActivityMessage
    {
        public ActivityMessageAgent() : base(ConfigItems.ActivityMessageQueue) { }

        #region IActivityMessage Members

        public void DispatchActivityLogMessage(ActivityLogMessage entry)
        {
            Channel.DispatchActivityLogMessage(entry);
        }
    
        #endregion
    }
}
