﻿using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Logging.Agents
{
    public class DebugMessageAgent : BaseLoggingAgent<IDebugMessage>, IDebugMessage
    {
        public DebugMessageAgent() : base(ConfigItems.DebugMessageQueue) { }
    
        #region IDebugMessage Members

        public void DispatchDebugMessage(DebugLogMessage msg)
        {
            Channel.DispatchDebugMessage(msg);
        }

        #endregion
    }
}
