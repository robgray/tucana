﻿using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Logging.Agents
{
    public class EmailMessageAgent : BaseLoggingAgent<IEmailMessage>, IEmailMessage
    {
        public EmailMessageAgent() : base(ConfigItems.EmailMessageQueue) { }

        #region IEmailMessage Members

        public void DispatchEmailMessage(EmailMessage email)
        {
            Channel.DispatchEmailMessage(email);
        }

        #endregion
    }
}
