﻿using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.Logging.Agents
{
    public class ExceptionMessageAgent : BaseLoggingAgent<IExceptionMessage>, IExceptionMessage
    {
        public ExceptionMessageAgent() : base(ConfigItems.ExceptionMessageQueue) { }

        #region IExceptionMessage Members

        public void DispatchExceptionLogMessage(ExceptionLogMessage entry)
        {
            Channel.DispatchExceptionLogMessage(entry);
        }

        #endregion
    }
}
