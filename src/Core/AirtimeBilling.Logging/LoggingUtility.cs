﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Logging.Agents;
using AirtimeBilling.Logging.Email;

namespace AirtimeBilling.Logging
{
    /// <summary>
    /// The central interface to send emails, log exceptions, and log activities.
    /// </summary>
    public static class LoggingUtility
    {        
        #region Debug Logging

        public static void LogDebug(string user, string method, string source, string message)
        {
            DebugMessageAgent agent = null;

            try
            {
                agent = new DebugMessageAgent();
                var msg = new DebugLogMessage
                              {
                                  Username = user,
                                  LoggingLevel = LoggingLevels.DebugInfo,
                                  MethodName = method,
                                  Source = source,
                                  Message = message
                              };

                agent.DispatchDebugMessage(msg);
            }
            catch (Exception ex)
            {
                LogException(user, ex);
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }

        public static void LogDebug(string method, string source, string message)
        {
            DebugMessageAgent agent = null;
            if (!string.IsNullOrEmpty(message)) Console.WriteLine(message);

            try
            {
                agent = new DebugMessageAgent();
                var msg = new DebugLogMessage
                {
                    Username = Constants.SYSTEM_USER_NAME,
                    LoggingLevel = LoggingLevels.DebugInfo,
                    MethodName = method,
                    Source = source,
                    Message = message
                };

                agent.DispatchDebugMessage(msg);
            }
            catch (Exception ex)
            {
                LogException(Constants.SYSTEM_USER_NAME, ex);
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }

        public static void LogWarning(string user, string method, string source, string message)
        {
            DebugMessageAgent agent = null;

            try
            {
                agent = new DebugMessageAgent();
                var msg = new DebugLogMessage
                {
                    LoggingLevel = LoggingLevels.Warning,
                    Username = user,
                    MethodName = method,
                    Source = source,
                    Message = message
                };

                agent.DispatchDebugMessage(msg);
            }
            catch (Exception ex)
            {
                LogException(user, ex);
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }

        public static void LogWarning(string method, string source, string message)
        {
            DebugMessageAgent agent = null;

            try
            {
                agent = new DebugMessageAgent();
                var msg = new DebugLogMessage
                                {
                                    Username = Constants.SYSTEM_USER_NAME,
                                    LoggingLevel = LoggingLevels.Warning,
                                    MethodName = method,
                                    Source = source,
                                    Message = message
                                };

                agent.DispatchDebugMessage(msg);
            }
            catch (Exception ex)
            {
                LogException(Constants.SYSTEM_USER_NAME, ex);
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }

        #endregion 

        #region Activity Logging

        /// <summary>
        /// Log an activity that occurred at the current date and time to the activity log 
        /// </summary>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>
        /// <param name="user">User that performed the activity</param>
        /// <param name="activity">Description of the activity that took place</param>        
        public static void WriteUserActivity(string tableName, int keyId, string user, string activity)
        {
            WriteUserActivity(DateTime.Now, tableName, keyId, user, activity);
        }

        /// <summary>
        /// Log an activity that occurred at the current date and time to the activity log 
        /// </summary>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>
        /// <param name="user">User that performed the activity</param>
        /// <param name="activity">Description of the activity that took place</param>        
        public static void WriteActivity(string tableName, int keyId, string activity)
        {
            WriteActivity(DateTime.Now, tableName, keyId, activity);
        }

        /// <summary>
        /// Log an activity that occurred at the specified date and time to the activity log 
        /// </summary>
        /// <param name="activityDate">Date the activity took place</param>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>
        /// <param name="user">User that performed the activity</param>
        /// <param name="activity">Description of the activity that took place</param>        
        public static void WriteUserActivity(DateTime activityDate, string tableName, int keyId, string user, string activity)
        {
            ActivityMessageAgent agent = null;

            try
            {
                var msg = new ActivityLogMessage
                              {
                                  Timestamp = activityDate,
                                  TableName = tableName,
                                  TableKeyId = keyId,
                                  Username = user,
                                  Activity = activity
                              };

                agent = new ActivityMessageAgent();
                agent.DispatchActivityLogMessage(msg);
            }
            catch (Exception ex) 
            {
                LogException(user, ex);
                throw;
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }            
        }

        /// <summary>
        /// Log an activity that occurred at the specified date and time to the activity log.  
        /// Using the current IIdentity.Name as user.
        /// </summary>
        /// <param name="activityDate">Date the activity took place</param>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>        
        /// <param name="activity">Description of the activity that took place</param>        
        public static void WriteActivity(DateTime activityDate, string tableName, int keyId, string activity)
        {
            ActivityMessageAgent agent = null;

            var userName = System.Threading.Thread.CurrentPrincipal != null
                               ? System.Threading.Thread.CurrentPrincipal.Identity.Name
                               : Constants.SYSTEM_USER_NAME;

            try
            {
                var msg = new ActivityLogMessage
                {
                    Timestamp = activityDate,
                    TableName = tableName,
                    TableKeyId = keyId,
                    Username = userName,
                    Activity = activity
                };

                agent = new ActivityMessageAgent();
                agent.DispatchActivityLogMessage(msg);
            }
            catch (Exception ex)
            {
                LogException(Constants.SYSTEM_USER_NAME, ex);
                throw;
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }


        /// <summary>
        /// Writes a System activity to the activity log, using the current Date and Time
        /// </summary>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>
        /// <param name="activity">Description of the activity that took place</param>        
        public static void WriteSystemActivity(string tableName, int keyId, string activity)
        {
            WriteSystemActivity(DateTime.Now, tableName, keyId, activity);
        }

        /// <summary>
        /// Writes a System activity to the activity log, using the specified Date and Time
        /// </summary>
        /// <param name="activityDate">Time the activity occurred</param>
        /// <param name="tableName">Name of the table associated with the activity (if any)</param>
        /// <param name="keyId">Primary key of the record associated with the activity (if any)</param>
        /// <param name="activity">Description of the activity that took place</param>        
        public static void WriteSystemActivity(DateTime activityDate, string tableName, int keyId, string activity)
        {
            WriteUserActivity(activityDate, tableName, keyId, "System", activity);
        }

        #endregion 

        #region Exception Logging

        public static void LogException(Exception ex)
        {
            LogException(Constants.SYSTEM_USER_NAME, ex);
        }

        public static void LogException(string user, Exception ex)
        {
            LogException(user, ex, true);
        }

        public static void LogException(Exception ex, bool sendEmail)
        {
            LogException(System.Threading.Thread.CurrentPrincipal.Identity.Name, ex, sendEmail);
        }

        public static void LogException(string user, Exception ex, bool sendEmail)
        {
            ExceptionMessageAgent agent = null;
            var msg = new ExceptionLogMessage(ex)
            {
                Username = user
            };

            try
            {
                Console.WriteLine(msg.ToString());
                agent = new ExceptionMessageAgent();
                agent.DispatchExceptionLogMessage(msg);

                if (ConfigItems.SendErrorEmails && sendEmail)
                {
                    var email = new ExceptionEmail
                                    {
                                        Ex = ex,
                                        Username = user
                                    };
                    SendEmail(ConfigItems.ErrorEmailAddress, "AirtimeBilling Error", email, false);
                }

                // After send of initial exception email, log the other exceptions
                if (ex is SqlException)
                {
                    var sex = ex as SqlException;
                    foreach (SqlError error in sex.Errors)
                    {
                        var sqlExMsg = new ExceptionLogMessage(sex)
                                           {
                                               Message = error.Message,
                                               StackTrace =
                                                   string.Format("server={0}, Procedure={1}", error.Server,
                                                                 error.Procedure),
                                               Username = user
                                           };
                        Console.WriteLine(msg.ToString());
                        agent.DispatchExceptionLogMessage(sqlExMsg);
                    }
                }
            }
            catch (Exception ex1)
            {
                // Can't do anything here or we'll recurse to death!
                Console.WriteLine(ex1.Message);
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }
        
        #endregion 

        #region Email Logging

        public static void SendEmailWithAttachments(string toAddress, string toName, string subject, IEmailFormat message, bool highPriority, IList<string> attachmentFilenames)
        {
            var msg = new EmailMessage
            {
                ToEmailAddress = toAddress,
                ToName = toName,
                Subject = subject,
                Body = message.GetHtml(),
                IsBodyHtml = true,
                IsHighPriority = highPriority,
                FilesToAttach = attachmentFilenames ?? new List<string>()
            };

            EmailMessageAgent agent = null;
            try
            {
                agent = new EmailMessageAgent();
                agent.DispatchEmailMessage(msg); 
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }

        public static void SendEmail(string toAddress, string subject, IEmailFormat message, bool highPriority)
        {
            SendEmail(toAddress, null, subject, message, highPriority);
        }

        public static void SendEmail(string toAddress, string toName, string subject, IEmailFormat message, bool highPriority)
        {
            var msg = new EmailMessage
                          {
                              ToEmailAddress = toAddress,
                              ToName = toName,
                              Subject = subject,
                              Body = message.GetHtml(),
                              IsBodyHtml = true,
                              IsHighPriority = highPriority,        
                            };

            EmailMessageAgent agent = null;
            try
            {
                agent = new EmailMessageAgent();
                agent.DispatchEmailMessage(msg);
            }
            finally
            {
                if (agent != null)
                    agent.Close();
            }
        }

        public static void SendEmail(string toAddress, string subject, string message, bool highPriority)
        {
            SendEmail(toAddress, null, subject, message, highPriority);
        }

        public static void SendEmail(string toAddress, string toName, string subject, string message, bool highPriority)
        {   
            SendEmail(toAddress, toName, subject, new EmailFormat { Message = message }, highPriority);            
        }

        #endregion 

        class EmailFormat : IEmailFormat
        {
            public string Message { get; set; }

            #region IEmailFormat Members

            public string GetHtml()
            {
                return Message;
            }

            public string GetPlainText()
            {
                return Message;
            }

            #endregion
        }
    }

    
}
