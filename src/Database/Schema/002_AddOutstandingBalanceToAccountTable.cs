﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(2)]
    /// This got renamed from OutstandingBalance
    /// to AmountPaid and PreviousBill
    /// OutstandingBalance = PreviousBill - AmountPaid       
    public class _002_AddOutstandingBalanceToAccountTable : Migration
    {
        public override void Down()
        {
            Database.RemoveColumn("Account", "PreviousBill");
            Database.RemoveColumn("Account", "AmountPaid");
        }

        public override void Up()
        {
            Database.AddColumn("Account", new Column("PreviousBill", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("Account", new Column("AmountPaid", DbType.Currency, ColumnProperty.NotNull, 0));
        }
    }
}
