﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(3)]
    public class _003_AddContractIdToInvoiceLineItemTable : Migration 
    {
        public override void Down()
        {
            Database.RemoveColumn("InvoiceLineItem", "ContractId");
        }

        public override void Up()
        {
            Database.AddColumn("InvoiceLineItem", new Column("ContractId", DbType.Int32, ColumnProperty.Null));            
        }
    }
}
