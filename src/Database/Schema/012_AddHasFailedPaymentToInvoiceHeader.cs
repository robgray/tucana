﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(12)]
    public class _012_AddHasFailedPaymentToInvoiceHeader : Migration
    {

        public override void Down()
        {
            Database.RemoveColumn("InvoiceHeader", "HasFailedPayment");
        }

        public override void Up()
        {
            Database.AddColumn("InvoiceHeader", "HasFailedPayment", DbType.Boolean, 1, ColumnProperty.NotNull, false);
        }
    }
}
