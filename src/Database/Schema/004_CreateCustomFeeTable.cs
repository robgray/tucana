﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(4)]
    public class _004_CreateCustomFeeTable : Migration 
    {
        public override void Down()
        {
            Database.RemoveTable("ContractCustomFee");
            Database.RemoveTable("CustomFee");
        }

        public override void Up()
        {            
            Database.AddTable("CustomFee", new []
                                {
                                  new Column("CustomFeeId", DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                                  new Column("Description", DbType.String, 50, ColumnProperty.NotNull),
                                  new Column("Amount", DbType.Currency, ColumnProperty.NotNull, 0),
                                  new Column("IsRecurring", DbType.Boolean, ColumnProperty.NotNull, 0)
                                });

            Database.AddTable("ContractCustomFee", new []
                                    {
                                        new Column("ContractCustomFeeId", DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                                        new Column("ContractId", DbType.Int32, ColumnProperty.NotNull), 
                                        new Column("CustomFeeId", DbType.Int32, ColumnProperty.NotNull)
                                    });         
        }
    }
}
