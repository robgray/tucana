﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(6)]
    public class _006_EwayImportedDataTable : Migration 
    {
        public override void Down()
        {
            Database.RemoveTable("EwayProcessedData");
        }

        public override void Up()
        {
            var columns = new[]
                              {
                                  new Column("EwayProcessedDataId", DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                                  new Column("EwayTransactionReference", DbType.Int64, ColumnProperty.Unique | ColumnProperty.NotNull),
                                  new Column("CardHolder", DbType.String, 50, ColumnProperty.NotNull),
                                  new Column("Amount", DbType.Decimal, ColumnProperty.NotNull),
                                  new Column("ResponseCode", DbType.String, 2, ColumnProperty.NotNull),
                                  new Column("ResponseText", DbType.String, 50, ColumnProperty.NotNull),
                                  new Column("InvoiceNumber", DbType.String, 50),
                                  new Column("EwayInvoiceReference", DbType.String, 50)
                              };

            Database.AddTable("EwayProcessedData", columns);
        }
    }
}
