﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(1)]
    public class _001_AddIsInternationalToAccountTable : Migration
    {
        public override void Down()
        {
            Database.RemoveColumn("Account", "IsInternational");            
        }

        public override void Up()
        {
            Database.AddColumn("Account", new Column("IsInternational", DbType.Boolean, ColumnProperty.NotNull, false));
        }
    }
}
