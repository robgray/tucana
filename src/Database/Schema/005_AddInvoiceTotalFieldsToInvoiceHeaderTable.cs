﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(5)]
    public class _005_AddInvoiceTotalFieldsToInvoiceHeaderTable : Migration
    {
        public override void Down()
        {
            Database.RemoveColumn("InvoiceHeader", "PreviousBill");
            Database.RemoveColumn("InvoiceHeader", "AmountPaid");
            Database.RemoveColumn("InvoiceHeader", "Outstanding");
            Database.RemoveColumn("InvoiceHeader", "NewCharges");
            Database.RemoveColumn("InvoiceHeader", "CurrentBill");
            Database.RemoveColumn("InvoiceHeader", "PaymentDue");
            Database.RemoveColumn("InvoiceHeader", "TotalIncGst");
            Database.RemoveColumn("InvoiceHeader", "TotalGst");
            Database.RemoveColumn("InvoiceHeader", "TotalExGst");
        }

        public override void Up()
        {
            Database.AddColumn("InvoiceHeader", new Column("PreviousBill", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("AmountPaid", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("Outstanding", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("NewCharges", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("CurrentBill", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("PaymentDue", DbType.DateTime));
            Database.AddColumn("InvoiceHeader", new Column("TotalIncGst", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("TotalGst", DbType.Currency, ColumnProperty.NotNull, 0));
            Database.AddColumn("InvoiceHeader", new Column("TotalExGst", DbType.Currency, ColumnProperty.NotNull, 0));
        }
    }
}
