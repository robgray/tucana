﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(11)]
    public class _011_AddIsInternationalToInvoiceHeaderTable : Migration
    {
        public override void Down()
        {
            Database.RemoveColumn("InvoiceHeader", "IsInternational");
        }

        public override void Up()
        {
            Database.AddColumn("InvoiceHeader", "IsInternational", DbType.Boolean);
        }
    }
}
