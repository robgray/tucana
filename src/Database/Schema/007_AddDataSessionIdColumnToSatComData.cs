﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(7)]
    public class _007_AddDataSessionIdColumnToSatComData : Migration
    {
        public override void Down()
        {
            Database.RemoveColumn("SatComData", "DataSessionId");
        }

        public override void Up()
        {
            Database.AddColumn("SatComData", "DataSessionId", DbType.String, 50);
        }
    }
}
