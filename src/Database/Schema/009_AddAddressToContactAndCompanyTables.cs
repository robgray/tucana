﻿using Migrator.Framework;
using System.Data;

namespace AirtimeBilling.Schema
{
    [Migration(9)]
    public class _009_AddAddressToContactAndCompanyTables : Migration
    {

        public override void Down()
        {
            Database.RemoveColumn("Company", "Street2");
            Database.RemoveColumn("Company", "PostalStreet2");

            Database.RemoveColumn("ContactCompany", "Street2");
            Database.RemoveColumn("ContactCompany", "PostalStreet2");
        }

        public override void Up()
        {
            Database.AddColumn("Contact", "Street2", DbType.String, 50);
            Database.AddColumn("Contact", "PostalStreet2", DbType.String, 50);

            Database.AddColumn("Company", "Street2", DbType.String, 50);
            Database.AddColumn("Company", "PostalStreet2", DbType.String, 50);

        }
    }
}
