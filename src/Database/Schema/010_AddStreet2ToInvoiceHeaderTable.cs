﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(10)]
    public class _010_AddStreet2ToInvoiceHeaderTable : Migration
    {
        public override void Down()
        {
            Database.RemoveColumn("InvoiceHeader", "Street2");
        }

        public override void Up()
        {
            Database.AddColumn("InvoiceHeader", "Street2", DbType.String, 50);
        }
    }
}
