﻿using System.Data;
using Migrator.Framework;

namespace AirtimeBilling.Schema
{
    [Migration(8)]
    public class _008_AddNotesTable : Migration
    {
        public override void Down()
        {
            Database.RemoveTable("Note");
        }

        public override void Up()
        {
            Database.AddTable("Note", new[]
                                {
                                  new Column("NoteId", DbType.Int32, ColumnProperty.PrimaryKeyWithIdentity),
                                  new Column("EntityId", DbType.Int32, ColumnProperty.NotNull, 0), 
                                  new Column("Type", DbType.String, 50, ColumnProperty.NotNull),      
                                  new Column("NoteDate", DbType.DateTime, ColumnProperty.NotNull),    
                                  new Column("Username", DbType.String, 50, ColumnProperty.NotNull),    
                                  new Column("Comment", DbType.String, 1073741823, ColumnProperty.NotNull, 0) // NVARCHAR(MAX)
                                });
        }
    }
}
