/****** Object:  View [dbo].[InvoiceSummaryView]    Script Date: 12/24/2010 08:32:15 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceSummaryView]'))
DROP VIEW [dbo].[InvoiceSummaryView]
GO
/****** Object:  View [dbo].[InvoiceServicesSummaryView]    Script Date: 12/24/2010 08:32:11 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceServicesSummaryView]'))
DROP VIEW [dbo].[InvoiceServicesSummaryView]
GO
/****** Object:  View [dbo].[InvoiceCallDetailView]    Script Date: 12/24/2010 08:32:06 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceCallDetailView]'))
DROP VIEW [dbo].[InvoiceCallDetailView]
GO
/****** Object:  View [dbo].[AirtimeContractView]    Script Date: 12/24/2010 08:32:06 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AirtimeContractView]'))
DROP VIEW [dbo].[AirtimeContractView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW [dbo].[InvoiceCallDetailView]
AS
SELECT	AI.AccountInvoiceId,
		ContractNetwork.Name 'NetworkName', 
		lineContract.PhoneNumber1,
		lineContract.PhoneUsedBy,
		header.InvoiceHeaderId,
		line.InvoiceLineItemId,
		line.CallId,
		line.ContractId,
		CASE WHEN line.CallId IS NULL THEN line.[Description] ELSE NetworkTariff.Tariff END 'CallType',
		InvoiceNumber, 
		InvoiceDate,
		[To],
		Street,
		Suburb,
		Postcode,
		[State], 
		AmountPaid, 
		FromDate,
		ToDate, 
		[Description] 'LineDescription',
		Quantity,
		GstAmount,
		LineAmount,		
		CASE WHEN VolumeUnit=1 THEN c.CalledNumber ELSE CASE WHEN VolumeUnit=2 THEN 'Data' ELSE CASE WHEN VolumeUnit IS NOT NULL THEN 'SMS' ELSE NULL END END END'CalledNumber',
		c.CallTime,		
		c.UnitsOfTime,
		c.VolumeUnit,
		c.Volume,
		p.UnitOfTime,
		CASE WHEN line.CallId IS NOT NULL THEN 1 ELSE 0 END 'IsCall',
		CASE WHEN line.CallId IS NULL AND LineAmount < 0 THEN 1 ELSE 0 END 'IsCreditAmount'		
FROM AccountInvoice AI INNER JOIN 
	InvoiceHeader header ON header.InvoiceHeaderId = AI.InvoiceHeaderId INNER JOIN 
	InvoiceLineItem line ON header.InvoiceHeaderId=line.InvoiceHeaderId INNER JOIN 	
	[Contract] lineContract ON line.ContractId=lineContract.ContractId INNER JOIN 
	[Plan] ContractPlan ON lineContract.PlanId=ContractPlan.PlanId INNER JOIN 
	[Network] ContractNetwork ON ContractPlan.NetworkId=ContractNetwork.NetworkId LEFT JOIN 
	[Call] c ON line.CallId=c.CallId LEFT JOIN 
	[Contract] cont ON c.ContractId=cont.ContractId LEFT JOIN 
	Tariff ON c.TariffId=Tariff.TariffId LEFT JOIN 
	NetworkTariff ON Tariff.NetworkTariffId=NetworkTariff.NetworkTariffId LEFT JOIN 
	[Plan] p ON cont.PlanId=p.PlanId LEFT JOIN 
	Network net ON p.NetworkId=net.NetworkId
WHERE line.ContractId IS NOT NULL AND line.Quantity > 0 

GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE View [dbo].[InvoiceServicesSummaryView]
AS
SELECT	InvoiceNumber,		
		CASE WHEN line.CallId IS NOT NULL THEN net.Name ELSE CASE WHEN line.CallId IS NULL THEN line.[Description] ELSE NetworkTariff.Tariff END END 'SummaryDescription',
		CASE WHEN line.CallId IS NOT NULL OR line.Quantity = 0 THEN 1 ELSE 0 END 'IsCall',
		CASE WHEN line.CallId IS NULL AND LineAmount < 0 THEN 1 ELSE 0 END 'IsCreditAmount',	 
		COUNT(DISTINCT CASE WHEN line.CallId IS NOT NULL OR line.Quantity = 0 THEN line.ContractId ELSE NULL END) 'Services',  
		SUM(LineAmount) 'Amount'		
FROM InvoiceHeader header INNER JOIN 
	InvoiceLineItem line ON header.InvoiceHeaderId=line.InvoiceHeaderId LEFT JOIN 
	[Contract] lineContract ON line.ContractId=lineContract.ContractId LEFT JOIN 
	[Call] c ON line.CallId=c.CallId LEFT JOIN 
	[Contract] cont ON c.ContractId=cont.ContractId LEFT JOIN 
	Tariff ON c.TariffId=Tariff.TariffId LEFT JOIN 
	NetworkTariff ON Tariff.NetworkTariffId=NetworkTariff.NetworkTariffId LEFT JOIN 
	[Plan] p ON cont.PlanId=p.PlanId LEFT JOIN 
	Network net ON NetworkTariff.NetworkId=net.NetworkId
GROUP BY InvoiceNumber, CASE WHEN line.CallId IS NOT NULL THEN net.Name ELSE CASE WHEN line.CallId IS NULL THEN line.[Description] ELSE NetworkTariff.Tariff END END, CASE WHEN line.CallId IS NOT NULL OR line.Quantity = 0 THEN 1 ELSE 0 END, CASE WHEN line.CallId IS NULL AND LineAmount < 0 THEN 1 ELSE 0 END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE View [dbo].[InvoiceSummaryView]
AS
SELECT	header.InvoiceHeaderId,
		AccountNumber,
		AccountName,
		header.[To],
		cont.Name 'Contact',
		CompanyName,
		header.Street,
		header.Street2,
		header.Suburb,
		header.State,
		header.Postcode,		
		header.AmountPaid, 
		header.PreviousBill,
		header.Outstanding,
		header.NewCharges,
		header.CurrentBill,
		header.PaymentDue,
		header.FromDate,
		header.ToDate,
		header.TotalIncGst,
		header.TotalGst,
		header.TotalExGst,		
		PostBill 'IsPostOnly'
FROM InvoiceHeader header INNER JOIN 
	InvoiceLineItem line ON header.InvoiceHeaderId=line.InvoiceHeaderId INNER JOIN
	AccountInvoice ainv ON header.InvoiceHeaderId=ainv.InvoiceHeaderId INNER JOIN 
	Account acc ON ainv.AccountId=acc.AccountId INNER JOIN 
	Contact cont ON acc.ContactId=cont.ContactId LEFT JOIN 
	Company comp ON acc.CompanyId=comp.CompanyId
WHERE acc.IsInvoiceRoot=1	
GROUP BY header.InvoiceHeaderId, AccountNumber, AccountName, header.[To], cont.Name, comp.CompanyName, header.Street, header.Street2, header.Suburb, header.State, header.Postcode, 
		header.AmountPaid, header.PreviousBill, header.Outstanding, header.NewCharges, header.CurrentBill, header.PaymentDue, FromDate, ToDate, 
		header.TotalIncGst, header.TotalGst, header.TotalExGst, PostBill
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW AirtimeContractView
AS
SELECT	Account.AccountNumber, 
	Account.AccountName, 
	Company.CompanyName, 
	Company.ABN, 
	Contact.Name 'ContactName', 
	Contact.MobilePhone, 
	Contact.WorkPhone, 
	Contact.FaxNumber, 
	Contact.Email, 
	Contact.Street, 
	Contact.Street2, 
	Contact.Suburb, 
	Contact.[State], 
	Contact.Postcode, 
	Contact.DriversLicenseNumber, 
	Contact.DriversLicenseDOB, 
	Contact.DriversLicenseExpiry, 
	Contact.CreditCardNumber, 
	Contact.CreditCardExpiry, 
	[Contract].EndDate, 
	[Plan].Name 'PlanName', 
	Contact.CreditCardName, 
	Contract.ContractNumber, 
	parentcompany.CompanyName 'ParentCompanyName', 
	parentcompany.ABN 'ParentCompanyABN', 
	Contract.IMEINumber, 
	Contract.SimCard, 
	Contact.CreditCardType, 
	Account.BillingMethod, 
	Contract.ContractId, 
	Contract.ActivationDate, 	
	Account.HasRequestedInvoicing,
	Account.Password	
FROM   Contract Contract 
	INNER JOIN Account ON [Contract].AccountId=Account.AccountId
	INNER JOIN [Plan] ON [Contract].PlanId=[Plan].PlanId
	LEFT OUTER JOIN Company ON Account.CompanyId=Company.CompanyId
	LEFT OUTER JOIN Account parentaccount ON Account.ParentAccountId=parentaccount.AccountId
	INNER JOIN Contact ON Account.ContactId=Contact.ContactId
	LEFT OUTER JOIN Company parentcompany ON parentaccount.CompanyId=parentcompany.CompanyId
GO

