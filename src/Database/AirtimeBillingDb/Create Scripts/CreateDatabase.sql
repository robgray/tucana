/****** Object:  ForeignKey [FK_Account_CompanyId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_CompanyId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_CompanyId]
GO
/****** Object:  ForeignKey [FK_Account_ContactId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_ContactId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_ContactId]
GO
/****** Object:  ForeignKey [FK_Account_ParentAccountId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_ParentAccountId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_ParentAccountId]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__6359AB88]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__6359AB88]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__Appli__6359AB88]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__644DCFC1]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__644DCFC1]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [FK__aspnet_Me__UserI__644DCFC1]
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__14F1071C]   Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__14F1071C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths] DROP CONSTRAINT [FK__aspnet_Pa__Appli__14F1071C]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__1AA9E072]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__1AA9E072]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] DROP CONSTRAINT [FK__aspnet_Pe__PathI__1AA9E072]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__1E7A7156]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__1E7A7156]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [FK__aspnet_Pe__PathI__1E7A7156]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__1F6E958F]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__1F6E958F]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [FK__aspnet_Pe__UserI__1F6E958F]
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__7854C86E]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__7854C86E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile] DROP CONSTRAINT [FK__aspnet_Pr__UserI__7854C86E]
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__01DE32A8]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__01DE32A8]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles] DROP CONSTRAINT [FK__aspnet_Ro__Appli__01DE32A8]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__532343BF]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__532343BF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [FK__aspnet_Us__Appli__532343BF]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__06A2E7C5]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__06A2E7C5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] DROP CONSTRAINT [FK__aspnet_Us__RoleI__06A2E7C5]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__05AEC38C]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__05AEC38C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] DROP CONSTRAINT [FK__aspnet_Us__UserI__05AEC38C]
GO
/****** Object:  ForeignKey [FK_Call_ContractId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Call_ContractId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Call]'))
ALTER TABLE [dbo].[Call] DROP CONSTRAINT [FK_Call_ContractId]
GO
/****** Object:  ForeignKey [FK_Call_TariffId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Call_TariffId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Call]'))
ALTER TABLE [dbo].[Call] DROP CONSTRAINT [FK_Call_TariffId]
GO
/****** Object:  ForeignKey [FK_Contract_AccountId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_AccountId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract] DROP CONSTRAINT [FK_Contract_AccountId]
GO
/****** Object:  ForeignKey [FK_Contract_AgentId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_AgentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract] DROP CONSTRAINT [FK_Contract_AgentId]
GO
/****** Object:  ForeignKey [FK_Contract_PlanId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract] DROP CONSTRAINT [FK_Contract_PlanId]
GO
/****** Object:  ForeignKey [FK_ContractPlanVariance_ContractId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContractPlanVariance_ContractId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
ALTER TABLE [dbo].[ContractPlanVariance] DROP CONSTRAINT [FK_ContractPlanVariance_ContractId]
GO
/****** Object:  ForeignKey [FK_ContractPlanVariance_PlanId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContractPlanVariance_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
ALTER TABLE [dbo].[ContractPlanVariance] DROP CONSTRAINT [FK_ContractPlanVariance_PlanId]
GO
/****** Object:  ForeignKey [FK_NetworkTariff_NetworkId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NetworkTariff_NetworkId]') AND parent_object_id = OBJECT_ID(N'[dbo].[NetworkTariff]'))
ALTER TABLE [dbo].[NetworkTariff] DROP CONSTRAINT [FK_NetworkTariff_NetworkId]
GO
/****** Object:  ForeignKey [FK_Plan_NetworkId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Plan_NetworkId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
ALTER TABLE [dbo].[Plan] DROP CONSTRAINT [FK_Plan_NetworkId]
GO
/****** Object:  ForeignKey [FK_Tariff_NetworkTariffId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tariff_NetworkTariffId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
ALTER TABLE [dbo].[Tariff] DROP CONSTRAINT [FK_Tariff_NetworkTariffId]
GO
/****** Object:  ForeignKey [FK_Tariff_PlanId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tariff_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
ALTER TABLE [dbo].[Tariff] DROP CONSTRAINT [FK_Tariff_PlanId]
GO
/****** Object:  ForeignKey [FK_InvoiceLineItem_CallId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InvoiceLineItem_CallId]') AND parent_object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]'))
ALTER TABLE [dbo].[InvoiceLineItem] DROP CONSTRAINT [FK_InvoiceLineItem_CallId]
GO
/****** Object:  ForeignKey [FK_InvoiceLineItem_InvoiceHeaderId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InvoiceLineItem_InvoiceHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]'))
ALTER TABLE [dbo].[InvoiceLineItem] DROP CONSTRAINT [FK_InvoiceLineItem_InvoiceHeaderId]
GO
/****** Object:  ForeignKey [FK_Network_AirtimeProviderId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Network_AirtimeProviderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Network]'))
ALTER TABLE [dbo].[Network] DROP CONSTRAINT [FK_Network_AirtimeProviderId]
GO
/****** Object:  ForeignKey [FK_AccountInvoice_AccountId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccountInvoice_Account]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccountInvoice]'))
ALTER TABLE [dbo].[AccountInvoice] DROP CONSTRAINT [FK_AccountInvoice_Account]
GO
/****** Object:  ForeignKey [FK_AccountInvoice_InvoiceHeaderId]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccountInvoice_InvoiceHeader]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccountInvoice]'))
ALTER TABLE [dbo].[AccountInvoice] DROP CONSTRAINT [FK_AccountInvoice_InvoiceHeader]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomFee__Amoun__36139A73]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomFee] DROP CONSTRAINT [DF__CustomFee__Amoun__36139A73]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__CustomFee__IsRec__3707BEAC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[CustomFee] DROP CONSTRAINT [DF__CustomFee__IsRec__3707BEAC]
END

/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_CreateRole]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_SetProperties]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_CreateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
DROP VIEW [dbo].[vw_aspnet_Roles]
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
DROP VIEW [dbo].[vw_aspnet_Profiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_CreateUser]
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
DROP VIEW [dbo].[vw_aspnet_Users]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
DROP VIEW [dbo].[vw_aspnet_MembershipUsers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Profile_GetProperties]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_RoleExists]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_GetAllRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_SetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPassword]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
DROP VIEW [dbo].[vw_aspnet_Applications]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_AnyDataInTables]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Users_DeleteUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_User]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_Shared]
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
DROP VIEW [dbo].[vw_aspnet_WebPartState_Paths]
GO

/****** Object:  View [dbo].[InvoiceCallDetailView]    Script Date: 12/19/2010 09:42:44 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceCallDetailView]'))
DROP VIEW [dbo].[InvoiceCallDetailView]
GO

/****** Object:  View [dbo].[InvoiceCallDetailView]    Script Date: 12/19/2010 09:42:44 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceServicesSummaryView]'))
DROP VIEW [dbo].[InvoiceServicesSummaryView]
GO

/****** Object:  View [dbo].[InvoiceCallDetailView]    Script Date: 12/19/2010 09:42:44 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceSummaryView]'))
DROP VIEW [dbo].[InvoiceSummaryView]
GO

/****** Object:  View [dbo].[AirtimeContractView]    Script Date: 12/24/2010 08:32:06 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[AirtimeContractView]'))
DROP VIEW [dbo].[AirtimeContractView]
GO

/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Paths_CreatePath]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
DROP VIEW [dbo].[vw_aspnet_UsersInRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
GO


/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_UsersInRoles]
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Profile]
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Roles]
GO
/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Membership]
GO
/****** Object:  Table [dbo].[EwayProcessedData]    Script Date: 01/03/2011 09:58:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EwayProcessedData]') AND type in (N'U'))
DROP TABLE [dbo].[EwayProcessedData]
GO
/****** Object:  Table [dbo].[AccountInvoice]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountInvoice]') AND type in (N'U'))
DROP TABLE [dbo].[AccountInvoice]
GO
/****** Object:  Table [dbo].[Call]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Call]') AND type in (N'U'))
DROP TABLE [dbo].[Call]
GO
/****** Object:  Table [dbo].[ContractPlanVariance]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]') AND type in (N'U'))
DROP TABLE [dbo].[ContractPlanVariance]
GO
/****** Object:  Table [dbo].[Tariff]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tariff]') AND type in (N'U'))
DROP TABLE [dbo].[Tariff]
GO
/****** Object:  Table [dbo].[NetworkTariff]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NetworkTariff]') AND type in (N'U'))
DROP TABLE [dbo].[NetworkTariff]
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_PersonalizationPerUser]
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Users]
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_PersonalizationAllUsers]
GO
/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Paths]
GO
/****** Object:  Table [dbo].[Contract]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND type in (N'U'))
DROP TABLE [dbo].[Contract]
GO
/****** Object:  Table [dbo].[Plan]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Plan]') AND type in (N'U'))
DROP TABLE [dbo].[Plan]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND type in (N'U'))
DROP TABLE [dbo].[Account]
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_SchemaVersions]
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserProfile]') AND type in (N'U'))
DROP TABLE [dbo].[UserProfile]
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_Applications]
GO
/****** Object:  Table [dbo].[EwayImportedData]    Script Date: 12/24/2010 08:30:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EwayImportedData]') AND type in (N'U'))
DROP TABLE [dbo].[EwayImportedData]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
GO
/****** Object:  Table [dbo].[ContractCustomFee]    Script Date: 12/21/2010 17:33:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractCustomFee]') AND type in (N'U'))
DROP TABLE [dbo].[ContractCustomFee]
GO
/****** Object:  Table [dbo].[CustomFee]    Script Date: 12/21/2010 17:33:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomFee]') AND type in (N'U'))
DROP TABLE [dbo].[CustomFee]
GO
/****** Object:  Table [dbo].[VizadaSubServiceCode]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaSubServiceCode]') AND type in (N'U'))
DROP TABLE [dbo].[VizadaSubServiceCode]
GO
/****** Object:  Table [dbo].[VizadaMainServiceCode]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaMainServiceCode]') AND type in (N'U'))
DROP TABLE [dbo].[VizadaMainServiceCode]
GO
/****** Object:  Table [dbo].[VizadaServiceCode]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaServiceCode]') AND type in (N'U'))
DROP TABLE [dbo].[VizadaServiceCode]
GO
/****** Object:  Table [dbo].[VizadaData]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaData]') AND type in (N'U'))
DROP TABLE [dbo].[VizadaData]
GO
/****** Object:  Table [dbo].[VizadaData]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SatComData]') AND type in (N'U'))
DROP TABLE [dbo].[SatComData]
GO
/****** Object:  Table [dbo].[VizadaData]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SatComCallIdentifier]') AND type in (N'U'))
DROP TABLE [dbo].[SatComCallIdentifier]
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND type in (N'U'))
DROP TABLE [dbo].[Audit]
GO
/****** Object:  Table [dbo].[ActivityLog]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityLog]') AND type in (N'U'))
DROP TABLE [dbo].[ActivityLog]
GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemSettings]') AND type in (N'U'))
DROP TABLE [dbo].[SystemSettings]
GO
/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
DROP TABLE [dbo].[aspnet_WebEvent_Events]
GO
/****** Object:  Table [dbo].[AirtimeProvider]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AirtimeProvider]') AND type in (N'U'))
DROP TABLE [dbo].[AirtimeProvider]
GO
/****** Object:  Table [dbo].[Network]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Network]') AND type in (N'U'))
DROP TABLE [dbo].[Network]
GO
/****** Object:  Table [dbo].[Agent]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Agent]') AND type in (N'U'))
DROP TABLE [dbo].[Agent]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND type in (N'U'))
DROP TABLE [dbo].[Contact]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Company]') AND type in (N'U'))
DROP TABLE [dbo].[Company]
GO
/****** Object:  Table [dbo].[InvoiceHeader]    Script Date: 12/28/2008 08:59:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceHeader]') AND type in (N'U'))
DROP TABLE [dbo].[InvoiceHeader]
GO
/****** Object:  Table [dbo].[InvoiceLineItem]    Script Date: 12/28/2008 08:59:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]') AND type in (N'U'))
DROP TABLE [dbo].[InvoiceLineItem]
GO
/****** Object:  Table [dbo].[LogLevel]    Script Date: 12/28/2008 08:59:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogLevel]') AND type in (N'U'))
DROP TABLE [dbo].[LogLevel]
GO
/****** Object:  Table [dbo].[Log]    Script Date: 12/28/2008 08:59:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log]') AND type in (N'U'))
DROP TABLE [dbo].[Log]
GO
/****** Object:  Default [DF_Account_BillingMethod]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_BillingMethod]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_BillingMethod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_BillingMethod]
END


End
GO
/****** Object:  Default [DF_Account_EmailBillDataFile]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_EmailBillDataFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_EmailBillDataFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_EmailBillDataFile]
END


End
GO
/****** Object:  Default [DF_Account_EmailBill]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_EmailBill]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_EmailBill]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_EmailBill]
END


End
GO
/****** Object:  Default [DF_Account_PostBill]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_PostBill]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_PostBill]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_PostBill]
END


End
GO
/****** Object:  Default [DF_Account_BillingAddressType]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_BillingAddressType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_BillingAddressType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_BillingAddressType]
END


End
GO
/****** Object:  Default [DF_Account_IsInvoiceRoot]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_IsInvoiceRoot]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_IsInvoiceRoot]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_IsInvoiceRoot]
END


End
GO
/****** Object:  Default [DF_Account_HasRequestedInvoicing]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_HasRequestedInvoicing]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_HasRequestedInvoicing]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] DROP CONSTRAINT [DF_Account_HasRequestedInvoicing]
END


End
GO
/****** Object:  Default [DF_Agent_CommissionPercent]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Agent_CommissionPercent]') AND parent_object_id = OBJECT_ID(N'[dbo].[Agent]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Agent_CommissionPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Agent] DROP CONSTRAINT [DF_Agent_CommissionPercent]
END


End
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__5046D714]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__5046D714]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ap__Appli__5046D714]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] DROP CONSTRAINT [DF__aspnet_Ap__Appli__5046D714]
END


End
GO
/****** Object:  Default [DF__aspnet_Me__Passw__6541F3FA]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__6541F3FA]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Me__Passw__6541F3FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] DROP CONSTRAINT [DF__aspnet_Me__Passw__6541F3FA]
END


End
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__15E52B55]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Pa__PathI__15E52B55]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Pa__PathI__15E52B55]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Paths] DROP CONSTRAINT [DF__aspnet_Pa__PathI__15E52B55]
END


End
GO
/****** Object:  Default [DF__aspnet_Perso__Id__1D864D1D]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Perso__Id__1D864D1D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Perso__Id__1D864D1D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] DROP CONSTRAINT [DF__aspnet_Perso__Id__1D864D1D]
END


End
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__02D256E1]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ro__RoleI__02D256E1]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ro__RoleI__02D256E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Roles] DROP CONSTRAINT [DF__aspnet_Ro__RoleI__02D256E1]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__UserI__541767F8]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__541767F8]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__UserI__541767F8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__UserI__541767F8]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__550B8C31]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__550B8C31]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__Mobil__550B8C31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__Mobil__550B8C31]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__55FFB06A]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__55FFB06A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__IsAno__55FFB06A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] DROP CONSTRAINT [DF__aspnet_Us__IsAno__55FFB06A]
END


End
GO
/****** Object:  Default [DF_Contact_CreditCardType]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contact_CreditCardType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contact]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contact_CreditCardType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contact] DROP CONSTRAINT [DF_Contact_CreditCardType]
END


End
GO
/****** Object:  Default [DF_Contract_ContractStatus]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contract_ContractStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contract_ContractStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contract] DROP CONSTRAINT [DF_Contract_ContractStatus]
END


End
GO
/****** Object:  Default [DF_Contract_Data]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contract_Data]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contract_Data]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contract] DROP CONSTRAINT [DF_Contract_Data]
END


End
GO
/****** Object:  Default [DF_Contract_MessageBank]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contract_MessageBank]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contract_MessageBank]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contract] DROP CONSTRAINT [DF_Contract_MessageBank]
END


End
GO
/****** Object:  Default [DF_ContractPlanVariance_Cost]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ContractPlanVariance_Cost]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ContractPlanVariance_Cost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ContractPlanVariance] DROP CONSTRAINT [DF_ContractPlanVariance_Cost]
END


End
GO
/****** Object:  Default [DF_NetworkTariff_IsCountedInFreeCall]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_NetworkTariff_IsCountedInFreeCall]') AND parent_object_id = OBJECT_ID(N'[dbo].[NetworkTariff]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_NetworkTariff_IsCountedInFreeCall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NetworkTariff] DROP CONSTRAINT [DF_NetworkTariff_IsCountedInFreeCall]
END


End
GO
/****** Object:  Default [DF_PLan_Duration]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_Duration]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_Duration]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] DROP CONSTRAINT [DF_PLan_Duration]
END


End
GO
/****** Object:  Default [DF_PLan_Flagfall]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_Flagfall]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_Flagfall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] DROP CONSTRAINT [DF_PLan_Flagfall]
END


End
GO
/****** Object:  Default [DF_PLan_FreeCallAmount]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_FreeCallAmount]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_FreeCallAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] DROP CONSTRAINT [DF_PLan_FreeCallAmount]
END


End
GO
/****** Object:  Default [DF_PLan_PlanAmount]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_PlanAmount]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_PlanAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] DROP CONSTRAINT [DF_PLan_PlanAmount]
END


End
GO
/****** Object:  Default [DF_Tariff_IsCountedInFreeCall]    Script Date: 10/22/2008 16:06:20 ******/
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Tariff_IsCountedInFreeCall]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Tariff_IsCountedInFreeCall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Tariff] DROP CONSTRAINT [DF_Tariff_IsCountedInFreeCall]
END


End
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_BasicAccess')
DROP SCHEMA [aspnet_Membership_BasicAccess]
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_FullAccess')
DROP SCHEMA [aspnet_Membership_FullAccess]
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_ReportingAccess')
DROP SCHEMA [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_BasicAccess')
DROP SCHEMA [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_FullAccess')
DROP SCHEMA [aspnet_Personalization_FullAccess]
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_ReportingAccess')
DROP SCHEMA [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_BasicAccess')
DROP SCHEMA [aspnet_Profile_BasicAccess]
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_FullAccess')
DROP SCHEMA [aspnet_Profile_FullAccess]
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_ReportingAccess')
DROP SCHEMA [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_BasicAccess')
DROP SCHEMA [aspnet_Roles_BasicAccess]
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_FullAccess')
DROP SCHEMA [aspnet_Roles_FullAccess]
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_ReportingAccess')
DROP SCHEMA [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF  EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_WebEvent_FullAccess')
DROP SCHEMA [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_BasicAccess]
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_FullAccess]
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Membership_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Membership_ReportingAccess]
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Personalization_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Personalization_BasicAccess]
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Personalization_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Personalization_FullAccess]
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Personalization_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Personalization_ReportingAccess]
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Profile_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Profile_BasicAccess]
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Profile_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Profile_FullAccess]
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Profile_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Profile_ReportingAccess]
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Roles_BasicAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess' AND type = 'R')
DROP ROLE [aspnet_Roles_BasicAccess]
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Roles_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_FullAccess' AND type = 'R')
DROP ROLE [aspnet_Roles_FullAccess]
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_Roles_ReportingAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess' AND type = 'R')
DROP ROLE [aspnet_Roles_ReportingAccess]
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
DECLARE @RoleName sysname
set @RoleName = N'aspnet_WebEvent_FullAccess'
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = @RoleName AND type = 'R')
Begin
	DECLARE @RoleMemberName sysname
	DECLARE Member_Cursor CURSOR FOR
	select [name]
	from dbo.sysusers 
	where uid in ( 
		select member_principal_id 
		from sys.database_role_members 
		where role_principal_id in (
			select principal_id
			FROM sys.database_principals where [name] = @RoleName  AND type = 'R' ))

	OPEN Member_Cursor;

	FETCH NEXT FROM Member_Cursor
	into @RoleMemberName

	WHILE @@FETCH_STATUS = 0
	BEGIN

		exec sp_droprolemember @rolename=@RoleName, @membername= @RoleMemberName

		FETCH NEXT FROM Member_Cursor
		into @RoleMemberName
	END;

	CLOSE Member_Cursor;
	DEALLOCATE Member_Cursor;
End
GO
IF  EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_WebEvent_FullAccess' AND type = 'R')
DROP ROLE [aspnet_WebEvent_FullAccess]
GO
/****** Object:  Role [AirtimeBilling]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'airtimebilling')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'airtimebilling' AND type = 'R')
CREATE ROLE [AirtimeBilling]

END
GO
/****** Object:  Role [aspnet_Membership_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Membership_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Membership_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Membership_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Personalization_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Personalization_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Personalization_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Personalization_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Personalization_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Personalization_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Profile_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Profile_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Profile_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Profile_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Profile_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Profile_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Roles_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess' AND type = 'R')
CREATE ROLE [aspnet_Roles_BasicAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Roles_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_Roles_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_Roles_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess' AND type = 'R')
CREATE ROLE [aspnet_Roles_ReportingAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Role [aspnet_WebEvent_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_WebEvent_FullAccess')
BEGIN
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_WebEvent_FullAccess' AND type = 'R')
CREATE ROLE [aspnet_WebEvent_FullAccess] AUTHORIZATION [dbo]

END
GO
/****** Object:  Schema [aspnet_Membership_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_BasicAccess] AUTHORIZATION [aspnet_Membership_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Membership_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_FullAccess] AUTHORIZATION [aspnet_Membership_FullAccess]'
GO
/****** Object:  Schema [aspnet_Membership_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Membership_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Membership_ReportingAccess] AUTHORIZATION [aspnet_Membership_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_Personalization_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Personalization_BasicAccess] AUTHORIZATION [aspnet_Personalization_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Personalization_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Personalization_FullAccess] AUTHORIZATION [aspnet_Personalization_FullAccess]'
GO
/****** Object:  Schema [aspnet_Personalization_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Personalization_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Personalization_ReportingAccess] AUTHORIZATION [aspnet_Personalization_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_Profile_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Profile_BasicAccess] AUTHORIZATION [aspnet_Profile_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Profile_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Profile_FullAccess] AUTHORIZATION [aspnet_Profile_FullAccess]'
GO
/****** Object:  Schema [aspnet_Profile_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Profile_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Profile_ReportingAccess] AUTHORIZATION [aspnet_Profile_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_Roles_BasicAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_BasicAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Roles_BasicAccess] AUTHORIZATION [aspnet_Roles_BasicAccess]'
GO
/****** Object:  Schema [aspnet_Roles_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Roles_FullAccess] AUTHORIZATION [aspnet_Roles_FullAccess]'
GO
/****** Object:  Schema [aspnet_Roles_ReportingAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_Roles_ReportingAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_Roles_ReportingAccess] AUTHORIZATION [aspnet_Roles_ReportingAccess]'
GO
/****** Object:  Schema [aspnet_WebEvent_FullAccess]    Script Date: 10/22/2008 16:06:21 ******/
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = N'aspnet_WebEvent_FullAccess')
EXEC sys.sp_executesql N'CREATE SCHEMA [aspnet_WebEvent_FullAccess] AUTHORIZATION [aspnet_WebEvent_FullAccess]'
GO
/****** Object:  Table [dbo].[EwayProcessedData]    Script Date: 01/03/2011 09:58:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EwayProcessedData](
	[EwayProcessedDataId] [int] identity(1,1),
	[EwayTransactionReference] [bigint] NOT NULL,
	[CardHolder] [nvarchar](50) NOT NULL,
	[Amount] [decimal](19, 5) NOT NULL,
	[ResponseCode] [nvarchar](2) NOT NULL,
	[ResponseText] [nvarchar](50) NOT NULL,
	[InvoiceNumber] [nvarchar](50) NULL,
	[EwayInvoiceReference] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[EwayProcessedDataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Company]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Company]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Company](
	[CompanyId] [int] IDENTITY(1,1) NOT NULL,
	[CompanyName] [nvarchar](50)  NOT NULL,
	[ABN] [char](11)  NULL,
	[ACN] [char](9)  NULL,
	[Street] [nvarchar](50)  NULL,
	[Suburb] [nvarchar](50)  NULL,
	[State] [nvarchar](3)  NULL,
	[Postcode] [char](4)  NULL,
	[PostalStreet] [nvarchar](50)  NULL,
	[PostalSuburb] [nvarchar](50)  NULL,
	[PostalState] [nvarchar](3)  NULL,
	[PostalPostcode] [char](4)  NULL,
 CONSTRAINT [PK__Company__02133CD2] PRIMARY KEY CLUSTERED 
(
	[CompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Company]') AND name = N'IX_Company_ABN')
CREATE NONCLUSTERED INDEX [IX_Company_ABN] ON [dbo].[Company] 
(
	[ABN] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Company]') AND name = N'IX_Company_CompanyName')
CREATE NONCLUSTERED INDEX [IX_Company_CompanyName] ON [dbo].[Company] 
(
	[CompanyName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO

/****** Object:  Table [dbo].[Contact]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Contact](
	[ContactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100)  NOT NULL,
	[MobilePhone] [nvarchar](15)  NULL,
	[WorkPhone] [nvarchar](15)  NULL,
	[FaxNumber] [nvarchar](15)  NULL,
	[Email] [nvarchar](255)  NULL,
	[Street] [nvarchar](50)  NULL,
	[Suburb] [nvarchar](50)  NULL,
	[State] [nvarchar](10)  NULL,
	[Postcode] [nvarchar](10)  NULL,
	[PostalStreet] [nvarchar](50)  NULL,
	[PostalSuburb] [nvarchar](50)  NULL,
	[PostalState] [nvarchar](10)  NULL,
	[PostalCountry] [nvarchar](50)  NULL,
	[PostalPostcode] [nvarchar](10)  NULL,
	[DriversLicenseNumber] [char](8)  NULL,
	[DriversLicenseDOB] [datetime] NULL,
	[DriversLicenseExpiry] [datetime] NULL,
	[CreditCardName] [nvarchar](50)  NULL,
	[CreditCardNumber] [nvarchar](20)  NULL,
	[CreditCardExpiry] [char](5)  NULL,
	[CreditCardType] [int] NOT NULL,
	[CompanyId] [int] NULL,
 CONSTRAINT [PK__Contact__03FB8544] PRIMARY KEY CLUSTERED 
(
	[ContactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND name = N'IX_Contact_CompanyId')
CREATE NONCLUSTERED INDEX [IX_Contact_CompanyId] ON [dbo].[Contact] 
(
	[CompanyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND name = N'IX_Contact_FaxNumber')
CREATE NONCLUSTERED INDEX [IX_Contact_FaxNumber] ON [dbo].[Contact] 
(
	[FaxNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND name = N'IX_Contact_MobilePhone')
CREATE NONCLUSTERED INDEX [IX_Contact_MobilePhone] ON [dbo].[Contact] 
(
	[MobilePhone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND name = N'IX_Contact_Name')
CREATE NONCLUSTERED INDEX [IX_Contact_Name] ON [dbo].[Contact] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contact]') AND name = N'IX_Contact_WorkPhone')
CREATE NONCLUSTERED INDEX [IX_Contact_WorkPhone] ON [dbo].[Contact] 
(
	[WorkPhone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO

/****** Object:  Table [dbo].[Agent]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Agent]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Agent](
	[AgentId] [int] IDENTITY(1,1) NOT NULL,
	[AgentName] [nvarchar](50)  NOT NULL,
	[Street] [nvarchar](50)  NULL,
	[Suburb] [nvarchar](50)  NULL,
	[State] [nvarchar](3)  NULL,
	[Postcode] [nvarchar](4)  NULL,
	[Email] [nvarchar](100) NULL,
	[CommissionPercent] [float] NOT NULL,
 CONSTRAINT [PK__Agent__06D7F1EF] PRIMARY KEY CLUSTERED 
(
	[AgentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[Network]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Network]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Network](
	[NetworkId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50)  NOT NULL,
	[AirtimeProviderId] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK__Network__15261146] PRIMARY KEY CLUSTERED 
(
	[NetworkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[AirtimeProvider]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AirtimeProvider]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AirtimeProvider](
	[AirtimeProviderId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50)  NOT NULL,
	[ImporterFullName] [nvarchar](255)  NOT NULL,
 CONSTRAINT [PK__AirtimeProvider__170E59B8] PRIMARY KEY CLUSTERED 
(
	[AirtimeProviderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[aspnet_WebEvent_Events]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_Events]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_WebEvent_Events](
	[EventId] [char](32)  NOT NULL,
	[EventTimeUtc] [datetime] NOT NULL,
	[EventTime] [datetime] NOT NULL,
	[EventType] [nvarchar](256)  NOT NULL,
	[EventSequence] [decimal](19, 0) NOT NULL,
	[EventOccurrence] [decimal](19, 0) NOT NULL,
	[EventCode] [int] NOT NULL,
	[EventDetailCode] [int] NOT NULL,
	[Message] [nvarchar](1024)  NULL,
	[ApplicationPath] [nvarchar](256)  NULL,
	[ApplicationVirtualPath] [nvarchar](256)  NULL,
	[MachineName] [nvarchar](256)  NOT NULL,
	[RequestUrl] [nvarchar](1024)  NULL,
	[ExceptionType] [nvarchar](256)  NULL,
	[Details] [ntext]  NULL,
 CONSTRAINT [PK__aspnet_WebEvent___2EB0D91F] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[SystemSettings]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SystemSettings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SystemSettings](
	[SystemSettingId] [int] IDENTITY(1,1) NOT NULL,
	[KeyName] [nvarchar](100) NOT NULL,
	[KeyValue] [nvarchar](100) NULL,		
	[IsSystem] [bit] NOT NULL	
 CONSTRAINT [PK_SystemSettings] PRIMARY KEY CLUSTERED 
(
	[SystemSettingId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [IX_SystemSettings_KeyName] UNIQUE NONCLUSTERED 
(
	[KeyName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[ActivityLog]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ActivityLog]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ActivityLog](
	[ActivityLogId] [int] IDENTITY(1,1) NOT NULL,
	[LogDate] [datetime] NOT NULL,
	[TableName] [nvarchar](60)  NULL,
	[KeyId] [int] NULL,
	[User] [nvarchar](256)  NOT NULL,
	[Activity] [nvarchar](500)  NOT NULL,
 CONSTRAINT [PK__ActivityLog__3D3402A0] PRIMARY KEY CLUSTERED 
(
	[ActivityLogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ActivityLog]') AND name = N'IX_ActivityLog_LogDate')
CREATE NONCLUSTERED INDEX [IX_ActivityLog_LogDate] ON [dbo].[ActivityLog] 
(
	[LogDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ActivityLog]') AND name = N'IX_ActivityLog_User')
CREATE NONCLUSTERED INDEX [IX_ActivityLog_User] ON [dbo].[ActivityLog] 
(
	[User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[Audit]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Audit](
	[AuditId] [int] IDENTITY(1,1) NOT NULL,
	[AuditDate] [datetime] NOT NULL,
	[User] [nvarchar](30)  NOT NULL,
	[TableName] [nvarchar](60)  NOT NULL,
	[FieldName] [nvarchar](60)  NOT NULL,
	[KeyId] [int] NOT NULL,
	[OldValue] [nvarchar](80)  NULL,
	[NewValue] [nvarchar](80)  NULL,
 CONSTRAINT [PK__Audit__3F1C4B12] PRIMARY KEY CLUSTERED 
(
	[AuditId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND name = N'IX_Audit_EditDate')
CREATE NONCLUSTERED INDEX [IX_Audit_EditDate] ON [dbo].[Audit] 
(
	[AuditDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Audit]') AND name = N'IX_Audit_User')
CREATE NONCLUSTERED INDEX [IX_Audit_User] ON [dbo].[Audit] 
(
	[User] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO

/****** Object:  Table [dbo].[VizadaData]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VizadaData](
	[VizadaDataId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [nvarchar](10)  NULL,
	[InvoiceNumber] [nvarchar](10)  NULL,
	[ChargeType] [nvarchar](40)  NULL,
	[CreditDebit] [nvarchar](1)  NULL,
	[MessageReference] [nvarchar](12)  NULL,
	[SerialNumber] [nvarchar](20)  NULL,
	[MobileNumber] [nvarchar](20)  NULL,
	[PlatformId] [nvarchar](20)  NULL,
	[PlatformName] [nvarchar](40)  NULL,
	[CallDate] [nvarchar](10)  NULL,
	[CallTime] [nvarchar](8)  NULL,
	[OceanRegion] [nvarchar](15)  NULL,
	[SplitBilingCode] [nvarchar](10)  NULL,
	[Origin] [nvarchar](100)  NULL,
	[CalledNumber] [nvarchar](20)  NULL,
	[NumberType] [nvarchar](15)  NULL,
	[ShortCode] [nvarchar](10)  NULL,
	[Country] [nvarchar](100)  NULL,
	[ServiceCode] [nvarchar](14)  NULL,
	[ServiceName] [nvarchar](40)  NULL,
	[ServiceMain] [nvarchar](2)  NULL,
	[ServiceSub] [nvarchar](2)  NULL,
	[RatingPeriod] [nvarchar](2)  NULL,
	[BilledVolume] [decimal](10, 2) NULL,
	[ConnectVolume] [decimal](10, 2) NULL,
	[SatelliteVolume] [decimal](10, 2) NULL,
	[VolumeUnit] [nvarchar](5)  NULL,
	[SatelliteRate] [decimal](5, 3) NULL,
	[DestinationRate] [decimal](5, 3) NULL,
	[SatelliteCharge] [decimal](7, 2) NULL,
	[DestinationSurCharge] [decimal](7, 2) NULL,
	[FacilityCharge] [decimal](7, 2) NULL,
	[Discount] [decimal](7, 2) NULL,
	[DiscountType] [nvarchar](1)  NULL,
	[TotalCharge] [decimal](7, 2) NULL,
	[Currency] [nvarchar](3)  NULL,
	[ISP] [nvarchar](10)  NULL,	
	[Imported] [bit] NOT NULL, 
	[Checksum] [varbinary](30) NOT NULL,
 CONSTRAINT [PK__VizadaData__43E1002F] PRIMARY KEY CLUSTERED 
(
	[VizadaDataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [IX_VizadaData_Checksum] UNIQUE NONCLUSTERED 
(
	[Checksum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[VizadaServiceCode]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaServiceCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VizadaServiceCode](
	[ServiceCode] [nvarchar](15)  NOT NULL,
	[ServiceName] [nvarchar](40)  NOT NULL,
 CONSTRAINT [PK__VizadaServiceCod__45C948A1] PRIMARY KEY CLUSTERED 
(
	[ServiceCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1000', N'Mini-M Voice')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1026', N'Mini-M Voice PP')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1100', N'Mini-M Fax')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1126', N'Mini-M Fax PP')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1200', N'Mini-M Data')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1226', N'Mini-M Data PP')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1500', N'Mini-M Gain Voice')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1526', N'Mini-M Gain Voice PP')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1600', N'Mini-M Gain Fax')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1626', N'Mini-M Gain Fax PP')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1700', N'Mini-M Gain Data')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1726', N'Mini-M Gain Data PP')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-1800', N'Mini-M Voice Mail')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-LH00', N'Mini-M Leased HSD')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'MM-LV00', N'Mini-M Leased Voice')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I0', N'Iridium No Charge')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I1', N'Iridium Voice')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I2', N'Iridium 4.8 kbps Data')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I3', N'Iridium 9.6 kbps Data')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I4', N'Iridium Forward')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I5', N'Iridium Sat. Direct')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-I6', N'Iridium SMS')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'RD-IA', N'Iridium Incoming')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T1', N'Thuraya Voice')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T2', N'Thuraya GSM Outbound')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T3', N'Thuraya GSM Inbound')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T4', N'Thuraya Forward')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T5', N'Thuraya SMS')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T6', N'Thuraya GSM SMS')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T7', N'ThurayaDSL Light')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T8', N'ThurayaDSL Plenty')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-T9', N'ThurayaDSL Extra')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-TA', N'ThurayaDSL Unlimited')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-TB', N'Thuraya Fax')
INSERT [dbo].[VizadaServiceCode] ([ServiceCode], [ServiceName]) VALUES (N'TH-TC', N'Thuraya Data')
/****** Object:  Table [dbo].[VizadaMainServiceCode]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaMainServiceCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VizadaMainServiceCode](
	[ServiceCode] [nvarchar](2)  NOT NULL,
	[MainService] [nvarchar](40)  NOT NULL,
 CONSTRAINT [PK__VizadaMainServic__47B19113] PRIMARY KEY CLUSTERED 
(
	[ServiceCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I0', N'Iridium Free of Charge')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I1', N'Iridium Voice')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I2', N'Iridium Data 4.8')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I3', N'Iridium Data 9.6')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I4', N'Iridium Forwarding')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I5', N'Iridium Satellite Direct')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'I6', N'Iridium SMS')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'IA', N'Iridium Incoming (To Mobile)')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'P0', N'Inmarsat SPS Voice')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'P1', N'Inmarsat SPS SMS')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'P2', N'Inmarsat SPS Fax 2.4')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'P3', N'Inmarsat SPS Data 2.4')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T1', N'Thuraya Voice')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T2', N'Thuraya GSM Outbound')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T3', N'Thuraya GSM Inbound')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T4', N'Thuraya Forward')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T5', N'Thuraya SMS')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T6', N'Thuraya GMS SMS')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T7', N'ThurayaDSL Light')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T8', N'ThurayaDSL Plenty')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'T9', N'ThurayaDSL Extra')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'TA', N'ThurayaDSL Unlimited')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'TB', N'Thuraya Fax')
INSERT [dbo].[VizadaMainServiceCode] ([ServiceCode], [MainService]) VALUES (N'TC', N'Thuraya Data')
/****** Object:  Table [dbo].[VizadaSubServiceCode]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[VizadaSubServiceCode]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[VizadaSubServiceCode](
	[ServiceCode] [nvarchar](2)  NOT NULL,
	[SubService] [nvarchar](40)  NOT NULL,
 CONSTRAINT [PK__VizadaSubService__4999D985] PRIMARY KEY CLUSTERED 
(
	[ServiceCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RestorePermissions]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RestorePermissions]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RestorePermissions]
    @name   sysname
AS
BEGIN
    DECLARE @object sysname
    DECLARE @protectType char(10)
    DECLARE @action varchar(60)
    DECLARE @grantee sysname
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT Object, ProtectType, [Action], Grantee FROM #aspnet_Permissions where Object = @name

    OPEN c1

    FETCH c1 INTO @object, @protectType, @action, @grantee
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = @protectType + '' '' + @action + '' on '' + @object + '' TO ['' + @grantee + '']''
        EXEC (@cmd)
        FETCH c1 INTO @object, @protectType, @action, @grantee
    END

    CLOSE c1
    DEALLOCATE c1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Setup_RemoveAllRoleMembers]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Setup_RemoveAllRoleMembers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Setup_RemoveAllRoleMembers]
    @name   sysname
AS
BEGIN
    CREATE TABLE #aspnet_RoleMembers
    (
        Group_name      sysname,
        Group_id        smallint,
        Users_in_group  sysname,
        User_id         smallint
    )

    INSERT INTO #aspnet_RoleMembers
    EXEC sp_helpuser @name

    DECLARE @user_id smallint
    DECLARE @cmd nvarchar(500)
    DECLARE c1 cursor FORWARD_ONLY FOR
        SELECT User_id FROM #aspnet_RoleMembers

    OPEN c1

    FETCH c1 INTO @user_id
    WHILE (@@fetch_status = 0)
    BEGIN
        SET @cmd = ''EXEC sp_droprolemember '' + '''''''' + @name + '''''', '''''' + USER_NAME(@user_id) + ''''''''
        EXEC (@cmd)
        FETCH c1 INTO @user_id
    END

    CLOSE c1
    DEALLOCATE c1
END' 
END
GO
/****** Object:  Table [dbo].[aspnet_Applications]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Applications](
	[ApplicationName] [nvarchar](256)  NOT NULL,
	[LoweredApplicationName] [nvarchar](256)  NOT NULL,
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[Description] [nvarchar](256)  NULL,
 CONSTRAINT [PK__aspnet_Applicati__4D6A6A69] PRIMARY KEY NONCLUSTERED 
(
	[ApplicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [UQ__aspnet_Applicati__4E5E8EA2] UNIQUE NONCLUSTERED 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [UQ__aspnet_Applicati__4F52B2DB] UNIQUE NONCLUSTERED 
(
	[ApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]') AND name = N'aspnet_Applications_Index')
CREATE CLUSTERED INDEX [aspnet_Applications_Index] ON [dbo].[aspnet_Applications] 
(
	[LoweredApplicationName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[UserProfile]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserProfile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserProfile](
	[UserId] [uniqueidentifier] NOT NULL,
	[AgentId] [int] NULL,
	[ContactId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
 CONSTRAINT [PK__User__41049384] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[aspnet_SchemaVersions]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_SchemaVersions]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_SchemaVersions](
	[Feature] [nvarchar](128)  NOT NULL,
	[CompatibleSchemaVersion] [nvarchar](128)  NOT NULL,
	[IsCurrentVersion] [bit] NOT NULL,
 CONSTRAINT [PK__aspnet_SchemaVer__57E7F8DC] PRIMARY KEY CLUSTERED 
(
	[Feature] ASC,
	[CompatibleSchemaVersion] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'common', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'health monitoring', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'membership', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'personalization', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'profile', N'1', 1)
INSERT [dbo].[aspnet_SchemaVersions] ([Feature], [CompatibleSchemaVersion], [IsCurrentVersion]) VALUES (N'role manager', N'1', 1)
/****** Object:  Table [dbo].[Account]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Account]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Account](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [nvarchar](10)  NOT NULL,
	[AccountName] [nvarchar](50)  NOT NULL,
	[ContactId] [int] NOT NULL,
	[CompanyId] [int] NULL,
	[ParentAccountId] [int] NULL,
	[ContactRole] [nvarchar](50)  NULL,
	[BillingMethod] [int] NOT NULL,
	[EmailBillDataFile] [bit] NOT NULL,
	[EmailBill] [bit] NOT NULL,
	[PostBill] [bit] NOT NULL,
	[BillingAddressType] [int] NOT NULL,
	[Password] [nvarchar](20)  NOT NULL,
	[IsInvoiceRoot] [bit] NOT NULL,
	[IsInternational] [bit] NOT NULL,	
	[HasRequestedInvoicing] [bit] NOT NULL,	
	[AmountPaid] [money] NOT NULL,
	[PreviousBill] [money] NOT NULL,
 CONSTRAINT [PK__Account__09B45E9A] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[Plan]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Plan]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Plan](
	[PlanId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50)  NOT NULL,
	[Duration] [int] NOT NULL,
	[Flagfall] [money] NOT NULL,
	[FreeCallAmount] [money] NOT NULL,
	[PlanAmount] [money] NOT NULL,
	[NetworkId] [int] NOT NULL,
	[DefaultTariffId] [int] NULL,
	[UnitOfTime] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK__Plan__1CC7330E] PRIMARY KEY CLUSTERED 
(
	[PlanId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Plan]') AND name = N'IX_Plan_Name')
CREATE NONCLUSTERED INDEX [IX_Plan_Name] ON [dbo].[Plan] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Plan]') AND name = N'IX_Plan_NetworkId')
CREATE NONCLUSTERED INDEX [IX_Plan_NetworkId] ON [dbo].[Plan] 
(
	[NetworkId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO

/****** Object:  Table [dbo].[Contract]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Contract](
	[ContractId] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NOT NULL,
	[AccountId] [int] NOT NULL,
	[ContractNumber] [nvarchar](10)  NOT NULL,
	[ActivationDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[IMEINumber] [nvarchar](50)  NULL,
	[SimCard] [nvarchar](50)  NULL,
	[PhoneNumber1] [nvarchar](20)  NULL,
	[PhoneNumber2] [nvarchar](20)  NULL,
	[PhoneNumber3] [nvarchar](20)  NULL,
	[PhoneNumber4] [nvarchar](20)  NULL,
	[AgentId] [int] NULL,
	[ContractStatus] [int] NOT NULL,
	[Data] [bit] NOT NULL,
	[MessageBank] [bit] NOT NULL,
	[PhoneUsedBy] [nvarchar](50)  NULL,
	[OrderReceivedDate] [datetime] NOT NULL,
	[Phone] [nvarchar](50)  NULL,
	[Pin] [nvarchar](10)  NULL,
	[Puk] [nvarchar](10)  NULL,
	[StatusChangedDate] [datetime] NOT NULL,
	[SuspensionStartDate] [datetime] NULL,
	[SuspendedUntilDate] [datetime] NULL,	
 CONSTRAINT [PK__Contract__2838E5BA] PRIMARY KEY CLUSTERED 
(
	[ContractId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [IX_Contract_CntractNumber] UNIQUE NONCLUSTERED 
(
	[ContractNumber] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND name = N'IX_Contract_AccountId')
CREATE NONCLUSTERED INDEX [IX_Contract_AccountId] ON [dbo].[Contract] 
(
	[AccountId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND name = N'IX_Contract_ActivationDate')
CREATE NONCLUSTERED INDEX [IX_Contract_ActivationDate] ON [dbo].[Contract] 
(
	[ActivationDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND name = N'IX_Contract_AgentId')
CREATE NONCLUSTERED INDEX [IX_Contract_AgentId] ON [dbo].[Contract] 
(
	[AgentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND name = N'IX_Contract_EndDate')
CREATE NONCLUSTERED INDEX [IX_Contract_EndDate] ON [dbo].[Contract] 
(
	[EndDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND name = N'IX_Contract_PhoneNumber1')
CREATE NONCLUSTERED INDEX [IX_Contract_PhoneNumber1] ON [dbo].[Contract] 
(
	[PhoneNumber1] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Contract]') AND name = N'IX_Contract_PlanId')
CREATE NONCLUSTERED INDEX [IX_Contract_PlanId] ON [dbo].[Contract] 
(
	[PlanId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO


/****** Object:  Table [dbo].[CustomFee]    Script Date: 12/21/2010 17:33:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomFee](
	[CustomFeeId] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
	[Amount] [money] NOT NULL,
	[IsRecurring] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CustomFeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CustomFee] ADD  DEFAULT ((0)) FOR [Amount]
GO
ALTER TABLE [dbo].[CustomFee] ADD  DEFAULT ((0)) FOR [IsRecurring]
GO

/****** Object:  Table [dbo].[ContractCustomFee]    Script Date: 12/21/2010 17:33:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContractCustomFee](
	[ContractCustomFeeId] [int] IDENTITY(1,1) NOT NULL,
	[ContractId] [int] NOT NULL,
	[CustomFeeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ContractCustomFeeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[aspnet_Paths]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Paths](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NOT NULL,
	[Path] [nvarchar](256)  NOT NULL,
	[LoweredPath] [nvarchar](256)  NOT NULL,
 CONSTRAINT [PK__aspnet_Paths__13FCE2E3] PRIMARY KEY NONCLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]') AND name = N'aspnet_Paths_index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Paths_index] ON [dbo].[aspnet_Paths] 
(
	[ApplicationId] ASC,
	[LoweredPath] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationAllUsers]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationAllUsers](
	[PathId] [uniqueidentifier] NOT NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Personali__19B5BC39] PRIMARY KEY CLUSTERED 
(
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[aspnet_Users]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Users](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](256)  NOT NULL,
	[LoweredUserName] [nvarchar](256)  NOT NULL,
	[MobileAlias] [nvarchar](16)  NULL,
	[IsAnonymous] [bit] NOT NULL,
	[LastActivityDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Users__522F1F86] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Users_Index] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LoweredUserName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users]') AND name = N'aspnet_Users_Index2')
CREATE NONCLUSTERED INDEX [aspnet_Users_Index2] ON [dbo].[aspnet_Users] 
(
	[ApplicationId] ASC,
	[LastActivityDate] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[aspnet_PersonalizationPerUser]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_PersonalizationPerUser](
	[Id] [uniqueidentifier] NOT NULL,
	[PathId] [uniqueidentifier] NULL,
	[UserId] [uniqueidentifier] NULL,
	[PageSettings] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Personali__1C9228E4] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_index1')
CREATE UNIQUE CLUSTERED INDEX [aspnet_PersonalizationPerUser_index1] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[PathId] ASC,
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]') AND name = N'aspnet_PersonalizationPerUser_ncindex2')
CREATE UNIQUE NONCLUSTERED INDEX [aspnet_PersonalizationPerUser_ncindex2] ON [dbo].[aspnet_PersonalizationPerUser] 
(
	[UserId] ASC,
	[PathId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[NetworkTariff]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NetworkTariff]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[NetworkTariff](
	[NetworkTariffId] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](10)  NOT NULL,
	[Tariff] [nvarchar](50)  NOT NULL,
	[IsCountedInFreeCall] [bit] NOT NULL,
	[HasFlagfall] [bit] NOT NULL,
	[NetworkId] [int] NOT NULL,
 CONSTRAINT [PK__NetworkTariff__18F6A22A] PRIMARY KEY CLUSTERED 
(
	[NetworkTariffId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[Tariff]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Tariff]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Tariff](
	[TariffId] [int] IDENTITY(1,1) NOT NULL,
	[UnitCost] [money] NULL,
	[Markup] [float] NULL,
	[PlanId] [int] NOT NULL,
	[NetworkTariffId] [int] NOT NULL,	
	[IsCountedInFreeCall] [bit] NOT NULL,
	[HasFlagfall] [bit] NOT NULL,
 CONSTRAINT [PK__Tariff__2374309D] PRIMARY KEY CLUSTERED 
(
	[TariffId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

/****** Object:  Table [dbo].[ContractPlanVariance]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContractPlanVariance](
	[ContractPlanVarianceId] [int] IDENTITY(1,1) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Username] [nvarchar](30)  NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[Cost] [money] NOT NULL,
	[PlanId] [int] NOT NULL,
	[ContractId] [int] NOT NULL,
 CONSTRAINT [PK__ContractPlanVari__30CE2BBB] PRIMARY KEY CLUSTERED 
(
	[ContractPlanVarianceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[Call]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Call]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Call](
	[CallId] [int] IDENTITY(1,1) NOT NULL,
	[ContractId] [int] NOT NULL,
	[TariffId] [int] NOT NULL,
	[ServiceNumber] [nvarchar](20)  NULL,
	[CallTime] [datetime] NOT NULL,
	[CalledNumber] [nvarchar](20)  NULL,
	[Location] [nvarchar](50)  NULL,
	[ImportedCallType] [nvarchar](50)  NULL,
	[Volume] [decimal](10, 2) NOT NULL,
	[VolumeUnit] [tinyint] NOT NULL,
	[UnitsOfTime] [decimal] (10,2) NULL,	
	[DateInvoiced] [datetime] NULL,
 CONSTRAINT [PK__Call__3592E0D8] PRIMARY KEY CLUSTERED 
(
	[CallId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Call]') AND name = N'IX_Call_PlanType')
CREATE NONCLUSTERED INDEX [IX_Call_PlanType] ON [dbo].[Call] 
(
	[TariffId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceHeader]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InvoiceHeader](
	[InvoiceHeaderId] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceNumber] [nvarchar](10) NOT NULL,
	[InvoiceDate] [datetime] NOT NULL,	
	[To] [nvarchar](50) NOT NULL,
	[Street] [nvarchar](50) NOT NULL,
	[Suburb] [nvarchar](50) NOT NULL,
	[Postcode] [nvarchar](10) NOT NULL,
	[State] [nvarchar](50) NOT NULL,	
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[InvoiceRunNumber] [int] NOT NULL,
	[PreviousBill] [money] NOT NULL,
	[AmountPaid] [money] NOT NULL,
	[Outstanding] [money] NOT NULL,
	[NewCharges] [money] NOT NULL,
	[CurrentBill] [money] NOT NULL,
	[PaymentDue] [datetime] NULL,
	[TotalIncGst] [money] NOT NULL,
	[TotalGst] [money] NOT NULL,
	[TotalExGst] [money] NULL,
 CONSTRAINT [PK_InvoiceHeader] PRIMARY KEY CLUSTERED 
(
	[InvoiceHeaderId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  Index [IX_InvoiceHeader]    Script Date: 12/28/2008 09:25:15 ******/
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceHeader]') AND name = N'IX_InvoiceHeader')
CREATE UNIQUE NONCLUSTERED INDEX [IX_InvoiceHeader] ON [dbo].[InvoiceHeader] 
(
	[InvoiceNumber] ASC
) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LogLevel]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[LogLevel] (
	[LevelId] [int] NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_LevelId] PRIMARY KEY CLUSTERED 
(
	[LevelId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

INSERT INTO [dbo].[LogLevel] (LevelId, Description) VALUES (0, 'None');
INSERT INTO [dbo].[LogLevel] (LevelId, Description) VALUES (1, 'Debug');
INSERT INTO [dbo].[LogLevel] (LevelId, Description) VALUES (2, 'Warning');
INSERT INTO [dbo].[LogLevel] (LevelId, Description) VALUES (3, 'Exception');
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Log](
	[LogId] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] DATETIME NOT NULL,
	[Username] [nvarchar](256) NOT NULL,
	[LoggingLevel] [int] NOT NULL,
	[MethodName] [nvarchar](500) NOT NULL,
	[Source] [nvarchar](1000) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[StackTrace] [nvarchar](max) NULL,	
 CONSTRAINT [PK_LogId] PRIMARY KEY CLUSTERED 
(
	[LogId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[InvoiceLineItem](
	[InvoiceLineItemId] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceHeaderId] [int] NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Quantity] [float] NOT NULL,
	[GstAmount] [money] NOT NULL,
	[LineAmount] [money] NOT NULL,
	[CallId] [int] NULL,
	[ContractId] [int] NULL,
 CONSTRAINT [PK_InvoiceLineItem] PRIMARY KEY CLUSTERED 
(
	[InvoiceLineItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InvoiceLineItem_CallId]') AND parent_object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]'))
ALTER TABLE [dbo].[InvoiceLineItem]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceLineItem_CallId] FOREIGN KEY([CallId])
REFERENCES [dbo].[Call] ([CallId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InvoiceLineItem_CallId]') AND parent_object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]'))
ALTER TABLE [dbo].[InvoiceLineItem] CHECK CONSTRAINT [FK_InvoiceLineItem_CallId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InvoiceLineItem_InvoiceHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]'))
ALTER TABLE [dbo].[InvoiceLineItem]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceLineItem_InvoiceHeaderId] FOREIGN KEY([InvoiceHeaderId])
REFERENCES [dbo].[InvoiceHeader] ([InvoiceHeaderId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_InvoiceLineItem_InvoiceHeaderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[InvoiceLineItem]'))
ALTER TABLE [dbo].[InvoiceLineItem] CHECK CONSTRAINT [FK_InvoiceLineItem_InvoiceHeaderId]
GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_InvoiceLineItem_Quantity]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[InvoiceLineItem] ADD  CONSTRAINT [DF_InvoiceLineItem_Quantity]  DEFAULT ((0)) FOR [Quantity]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_InvoiceLineItem_GstAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[InvoiceLineItem] ADD  CONSTRAINT [DF_InvoiceLineItem_GstAmount]  DEFAULT ((0)) FOR [GstAmount]
END

GO
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_InvoiceLineItem_LineAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[InvoiceLineItem] ADD  CONSTRAINT [DF_InvoiceLineItem_LineAmount]  DEFAULT ((0)) FOR [LineAmount]
END

GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AccountInvoice]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AccountInvoice](
	[AccountInvoiceId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NOT NULL,
	[InvoiceHeaderId] [int] NOT NULL,
 CONSTRAINT [PK_AccountInvoice] PRIMARY KEY CLUSTERED 
(
	[AccountInvoiceId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccountInvoice_Account]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccountInvoice]'))
ALTER TABLE [dbo].[AccountInvoice]  WITH CHECK ADD  CONSTRAINT [FK_AccountInvoice_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([AccountId])
GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccountInvoice_Account]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccountInvoice]'))
--ALTER TABLE [dbo].[AccountInvoice] CHECK CONSTRAINT [FK_AccountInvoice_Account]
--GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccountInvoice_InvoiceHeader]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccountInvoice]'))
ALTER TABLE [dbo].[AccountInvoice]  WITH CHECK ADD  CONSTRAINT [FK_AccountInvoice_InvoiceHeader] FOREIGN KEY([InvoiceHeaderId])
REFERENCES [dbo].[InvoiceHeader] ([InvoiceHeaderId])
GO
--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AccountInvoice_InvoiceHeader]') AND parent_object_id = OBJECT_ID(N'[dbo].[AccountInvoice]'))
--ALTER TABLE [dbo].[AccountInvoice] CHECK CONSTRAINT [FK_AccountInvoice_InvoiceHeader]
--GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SatComData]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SatComData] (
	[SatComDataId] [int] IDENTITY(1,1) NOT NULL,
	[AccountNo] [nvarchar](10)  NULL,
	[InvoiceNo] [nvarchar](10)  NULL,
	[OceanRegion] [nvarchar](40)  NULL,
	[Date] [nvarchar](10)  NULL,
	[Time] [nvarchar](8)  NULL,
	[OriginatorNo] [nvarchar](20) NULL,
	[Subscriber] [nvarchar](50)  NULL,
	[DestinationNo] [nvarchar](20)  NULL,
	[Volume] [decimal](10,2)  NULL,
	[Unit] [nvarchar](5)  NULL,
	[Rate] [decimal](10,2)  NULL,
	[TotalCharge] [decimal](10,2)  NULL,
	[EquipmentType] [nvarchar](50)  NULL,
	[CallToCallType] [nvarchar](50) NULL,
	[CallIdentifierID] [nvarchar](10) NULL,
	[OriginatorCountry] [nvarchar](50) NULL,
	[DestinationCountry] [nvarchar](50) NULL,
	[Provider] [nvarchar](10) NULL, 
	[UpstreamRate] [int] NULL,
	[DownstreamRate] [int] NULL,
	[Imported] [bit] NOT NULL, 
	[Checksum] [varbinary](30) NOT NULL,
 CONSTRAINT [PK__SatComData__43E1002F] PRIMARY KEY CLUSTERED 
(
	[SatComDataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON),
 CONSTRAINT [IX_SatComData_Checksum] UNIQUE NONCLUSTERED 
(
	[Checksum] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SatComCallIdentifier]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SatComCallIdentifier](
	[CallIdentifierId] [nvarchar](4)  NOT NULL,
	[InitiatorCallType] [nvarchar](50)  NOT NULL,
	[CalledToCallType] [nvarchar](50)  NOT NULL,
 CONSTRAINT [PK__SatComCallIdentifierId__45C948A1] PRIMARY KEY CLUSTERED 
(
	[CallIdentifierId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO

-- NEED TO ADD ALL THOSE EFFING ENTRIES!!!!! VOMITS -> :'(
-- See AirtimeBilling.Import.SatComData helper application to generate these entries.
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5454','BGAN','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5465','BGAN (CalledTo only)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5460','BGAN SMS','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5455','BGAN Standard IP','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5462','BGAN Streaming IP','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5463','Cellular','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5459','Cellular (BGAN Only)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5410','F77/F55/F33/M4 Low Speed','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('545A','Fax (BGAN Only)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5403','Fixed','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5457','Fixed (BGAN Only)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('547A','Fleet High Speed (BGAN Only)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('540F','Inmarsat A','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5404','Inmarsat B (HSD)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5402','Inmarsat B Voice','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5406','Inmarsat C','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5405','Inmarsat M','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5412','Iridium Postpaid Crew Calling','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5408','Iridium Postpaid Standard','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5413','Iridium Prepaid','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5458','ISDN (BGAN Only)','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5401','Mini-M','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('541A','Thuraya','BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6554','BGAN','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6565','BGAN (CalledTo only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6570','BGAN ISDN','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6560','BGAN SMS','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6555','BGAN Standard IP','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6562','BGAN Streaming IP','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6563','Cellular','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6559','Cellular (BGAN Only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6510','F77/F55/F33/M4 Low Speed','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('655A','Fax (BGAN Only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6503','Fixed','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6557','Fixed (BGAN Only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('657A','Fleet High Speed (BGAN Only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6573','Inmarsat','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('650F','Inmarsat A','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6567','Inmarsat B','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6504','Inmarsat B (HSD)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6553','Inmarsat B Data','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6580','Inmarsat B HSD (BGAN Only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6502','Inmarsat B Voice','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6505','Inmarsat M','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6512','Iridium Postpaid Crew Calling','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6508','Iridium Postpaid Standard','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6513','Iridium Prepaid','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6558','ISDN (BGAN Only)','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6501','Mini-M','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('651A','Thuraya','BGAN (CalledTo only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6055','BGAN Standard IP','BGAN SMS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4003','Fixed','F33 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('400F','Inmarsat A','F33 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4004','Inmarsat B (HSD)','F33 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4002','Inmarsat B Voice','F33 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4005','Inmarsat M','F33 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4001','Mini-M','F33 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4140','F33 2.4k Fax','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4103','Fixed','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('410F','Inmarsat A','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4167','Inmarsat B','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4104','Inmarsat B (HSD)','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4102','Inmarsat B Voice','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4105','Inmarsat M','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4112','Iridium Postpaid Crew Calling','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4108','Iridium Postpaid Standard','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4101','Mini-M','F33 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E03','Fixed','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E57','Fixed (BGAN Only)','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E0F','Inmarsat A','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E04','Inmarsat B (HSD)','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E02','Inmarsat B Voice','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E05','Inmarsat M','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2E01','Mini-M','F33 9.6k Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4203','Fixed','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('420F','Inmarsat A','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4267','Inmarsat B','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4204','Inmarsat B (HSD)','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4202','Inmarsat B Voice','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4205','Inmarsat M','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4201','Mini-M','F33 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4E03','Fixed','F33 MPDS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4303','Fixed','F55 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4403','Fixed','F55 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4402','Inmarsat B Voice','F55 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('443E','M4 High Speed','F55 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4401','Mini-M','F55 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4503','Fixed','F55 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3B03','Fixed','F55 HSD 128k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4C03','Fixed','F55 HSD ISDN 56k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3A03','Fixed','F55 HSD ISDN 64k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4F03','Fixed','F55 MPDS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4607','F77/F55/F33/M4 High Speed','F77 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4603','Fixed','F77 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4657','Fixed (BGAN Only)','F77 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('463E','M4 High Speed','F77 2.4k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4747','F77 4.8k Voice','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4703','Fixed','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('470F','Inmarsat A','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4767','Inmarsat B','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4702','Inmarsat B Voice','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4705','Inmarsat M','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4712','Iridium Postpaid Crew Calling','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4708','Iridium Postpaid Standard','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('473E','M4 High Speed','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4701','Mini-M','F77 4.8k Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4807','F77/F55/F33/M4 High Speed','F77 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4803','Fixed','F77 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4867','Inmarsat B','F77 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4802','Inmarsat B Voice','F77 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4801','Mini-M','F77 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('481D','MPDS','F77 9.6k Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3D03','Fixed','F77 HSD 128k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4D03','Fixed','F77 HSD ISDN 56k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4D57','Fixed (BGAN Only)','F77 HSD ISDN 56k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4D67','Inmarsat B','F77 HSD ISDN 56k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('4D01','Mini-M','F77 HSD ISDN 56k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3C03','Fixed','F77 HSD ISDN 64k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3C57','Fixed (BGAN Only)','F77 HSD ISDN 64k')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5003','Fixed','F77 MPDS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3203','Fixed','F77 Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3201','Mini-M','F77 Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0707','F77/F55/F33/M4 High Speed','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0710','F77/F55/F33/M4 Low Speed','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0703','Fixed','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0704','Inmarsat B (HSD)','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0702','Inmarsat B Voice','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0708','Iridium Postpaid Standard','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0701','Mini-M','F77/F55/F33/M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1007','F77/F55/F33/M4 High Speed','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1010','F77/F55/F33/M4 Low Speed','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1003','Fixed','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1002','Inmarsat B Voice','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1012','Iridium Postpaid Crew Calling','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1008','Iridium Postpaid Standard','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('101C','Iridium SMS','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1001','Mini-M','F77/F55/F33/M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0308','Iridium Postpaid Standard','Fixed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0B03','Fixed','Free 32')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0F03','Fixed','Inmarsat A')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2403','Fixed','Inmarsat A Telex')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('240F','Inmarsat A','Inmarsat A Telex')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2452','Inmarsat B Fax','Inmarsat A Telex')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2401','Mini-M','Inmarsat A Telex')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2503','Fixed','Inmarsat A Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('250F','Inmarsat A','Inmarsat A Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2525','Inmarsat A Voice','Inmarsat A Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2502','Inmarsat B Voice','Inmarsat A Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2501','Mini-M','Inmarsat A Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6703','Fixed','Inmarsat B')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0403','Fixed','Inmarsat B (HSD)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5303','Fixed','Inmarsat B Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5371','Fixed HMCMail','Inmarsat B Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5203','Fixed','Inmarsat B Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5201','Mini-M','Inmarsat B Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('521D','MPDS','Inmarsat B Fax')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0903','Fixed','Inmarsat B Telex')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0203','Fixed','Inmarsat B Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('020F','Inmarsat A','Inmarsat B Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0267','Inmarsat B','Inmarsat B Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0202','Inmarsat B Voice','Inmarsat B Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0201','Mini-M','Inmarsat B Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('021D','MPDS','Inmarsat B Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('066A','Email','Inmarsat C')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0603','Fixed','Inmarsat C')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0503','Fixed','Inmarsat M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7603','Fixed','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('760F','Inmarsat A','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7602','Inmarsat B Voice','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7606','Inmarsat C','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7605','Inmarsat M','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('762A','Iridium Analogue Data','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7611','Iridium Data','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('762B','Iridium Digital Data','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7674','Iridium Digital Data (881600006)','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7629','Iridium Internet Data','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7612','Iridium Postpaid Crew Calling','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7608','Iridium Postpaid Standard','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7613','Iridium Prepaid','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('766D','Iridium Voice','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('766F','Iridium Voicemail','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7601','Mini-M','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('761A','Thuraya','Iridium +1')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2A74','Iridium Digital Data (881600006)','Iridium Analogue Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2A29','Iridium Internet Data','Iridium Analogue Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2A08','Iridium Postpaid Standard','Iridium Analogue Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B6E','Email (Iridium SMS only)','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B03','Fixed','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B73','Inmarsat','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B0F','Inmarsat A','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B67','Inmarsat B','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B02','Inmarsat B Voice','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B06','Inmarsat C','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B05','Inmarsat M','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B2A','Iridium Analogue Data','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B1B','Iridium Australia Originated','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B11','Iridium Data','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B2B','Iridium Digital Data','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B74','Iridium Digital Data (881600006)','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B29','Iridium Internet Data','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B12','Iridium Postpaid Crew Calling','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B08','Iridium Postpaid Standard','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B13','Iridium Prepaid','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B1C','Iridium SMS','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B17','Iridium Two Stage','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B6D','Iridium Voice','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B6F','Iridium Voicemail','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B01','Mini-M','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B1E','R-BGAN','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1B1A','Thuraya','Iridium Australia Originated')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1103','Fixed','Iridium Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2B03','Fixed','Iridium Digital Data')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1207','F77/F55/F33/M4 High Speed','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1203','Fixed','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('127A','Fleet High Speed (BGAN Only)','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1273','Inmarsat','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('120F','Inmarsat A','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1225','Inmarsat A Voice','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1267','Inmarsat B','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1209','Inmarsat B Telex','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1202','Inmarsat B Voice','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1206','Inmarsat C','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1205','Inmarsat M','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('122A','Iridium Analogue Data','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('121B','Iridium Australia Originated','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('127F','Iridium Crew Calling Scratch Car','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1211','Iridium Data','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('122B','Iridium Digital Data','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1274','Iridium Digital Data (881600006)','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1229','Iridium Internet Data','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1212','Iridium Postpaid Crew Calling','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1208','Iridium Postpaid Standard','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1213','Iridium Prepaid','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('121C','Iridium SMS','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('126D','Iridium Voice','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('126F','Iridium Voicemail','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1201','Mini-M','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('121E','R-BGAN','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('121A','Thuraya','Iridium Postpaid Crew Calling')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0865','BGAN (CalledTo only)','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('086E','Email (Iridium SMS only)','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0810','F77/F55/F33/M4 Low Speed','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0803','Fixed','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0873','Inmarsat','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('080F','Inmarsat A','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0825','Inmarsat A Voice','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0867','Inmarsat B','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0804','Inmarsat B (HSD)','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0809','Inmarsat B Telex','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0802','Inmarsat B Voice','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0806','Inmarsat C','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0805','Inmarsat M','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('082A','Iridium Analogue Data','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('081B','Iridium Australia Originated','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0811','Iridium Data','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('082B','Iridium Digital Data','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0874','Iridium Digital Data (881600006)','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0829','Iridium Internet Data','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0812','Iridium Postpaid Crew Calling','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0808','Iridium Postpaid Standard','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0813','Iridium Prepaid','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('081C','Iridium SMS','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0817','Iridium Two Stage','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('086D','Iridium Voice','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('086F','Iridium Voicemail','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0801','Mini-M','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('081E','R-BGAN','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('081A','Thuraya','Iridium Postpaid Standard')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1303','Fixed','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('130F','Inmarsat A','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1302','Inmarsat B Voice','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1306','Inmarsat C','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1305','Inmarsat M','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('132A','Iridium Analogue Data','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1311','Iridium Data','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('132B','Iridium Digital Data','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1374','Iridium Digital Data (881600006)','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1329','Iridium Internet Data','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1312','Iridium Postpaid Crew Calling','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1308','Iridium Postpaid Standard','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1313','Iridium Prepaid','Iridium Prepaid')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2203','Fixed','Iridium SBD')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('226D','Iridium Voice','Iridium SBD')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1C6E','Email (Iridium SMS only)','Iridium SMS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1C03','Fixed','Iridium SMS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1C6D','Iridium Voice','Iridium SMS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1703','Fixed','Iridium Two Stage')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1708','Iridium Postpaid Standard','Iridium Two Stage')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6D11','Iridium Data','Iridium Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6F03','Fixed','Iridium Voicemail')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('2303','Fixed','IridiumSBDMailboxCheck')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5854','BGAN','ISDN (BGAN Only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('587A','Fleet High Speed (BGAN Only)','ISDN (BGAN Only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5804','Inmarsat B (HSD)','ISDN (BGAN Only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5858','ISDN (BGAN Only)','ISDN (BGAN Only)')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E46','F77 2.4k Fax','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E47','F77 4.8k Voice','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E48','F77 9.6k Fax','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E3D','F77 HSD 128k','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E3C','F77 HSD ISDN 64k','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E50','F77 MPDS','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E07','F77/F55/F33/M4 High Speed','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E03','Fixed','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E7A','Fleet High Speed (BGAN Only)','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E04','Inmarsat B (HSD)','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E02','Inmarsat B Voice','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E05','Inmarsat M','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E08','Iridium Postpaid Standard','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E3E','M4 High Speed','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E35','M4 Low Speed','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E01','Mini-M','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3E1A','Thuraya','M4 High Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3507','F77/F55/F33/M4 High Speed','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3503','Fixed','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3502','Inmarsat B Voice','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3505','Inmarsat M','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3512','Iridium Postpaid Crew Calling','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3508','Iridium Postpaid Standard','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('351C','Iridium SMS','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('356D','Iridium Voice','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('353E','M4 High Speed','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3535','M4 Low Speed','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('3501','Mini-M','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('351A','Thuraya','M4 Low Speed')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('5103','Fixed','M4 MPDS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('016A','Email','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0143','F55 2.4k Fax','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0147','F77 4.8k Voice','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0148','F77 9.6k Fax','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('014D','F77 HSD ISDN 56k','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('013C','F77 HSD ISDN 64k','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0132','F77 Voice','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0107','F77/F55/F33/M4 High Speed','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0110','F77/F55/F33/M4 Low Speed','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0103','Fixed','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0157','Fixed (BGAN Only)','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0171','Fixed HMCMail','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0172','Fixed HMCMail (6551030)','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('017A','Fleet High Speed (BGAN Only)','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('010B','Free 32','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('010C','Free 33','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('010D','Free 38','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('010E','Free 68','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0173','Inmarsat','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('010F','Inmarsat A','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0125','Inmarsat A Voice','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0167','Inmarsat B','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0104','Inmarsat B (HSD)','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0109','Inmarsat B Telex','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0102','Inmarsat B Voice','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0106','Inmarsat C','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0105','Inmarsat M','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('011B','Iridium Australia Originated','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0112','Iridium Postpaid Crew Calling','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0108','Iridium Postpaid Standard','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0113','Iridium Prepaid','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('011C','Iridium SMS','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('016D','Iridium Voice','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('013E','M4 High Speed','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0135','M4 Low Speed','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('0101','Mini-M','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('011D','MPDS','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('011A','Thuraya','Mini-M')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('6C03','Fixed','Mini-M 2 Stage')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1D03','Fixed','MPDS')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1E03','Fixed','R-BGAN')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7965','BGAN (CalledTo only)','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7959','Cellular (BGAN Only)','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7910','F77/F55/F33/M4 Low Speed','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7957','Fixed (BGAN Only)','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7902','Inmarsat B Voice','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7905','Inmarsat M','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7912','Iridium Postpaid Crew Calling','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7908','Iridium Postpaid Standard','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7913','Iridium Prepaid','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('7901','Mini-M','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('791A','Thuraya','SPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('1A03','Fixed','Thuraya')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('8186','FBB StandardIP','FBB')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('8199','Fixed (FBB Only)','FBB')
GO





/*********************************************/
/*********************************************/
/*********************************************/
/*********************************************/
/***** ASPNET MEMBERSHIP STUFF ***************/
/*********************************************/
/*********************************************/
/*********************************************/





/****** Object:  Table [dbo].[aspnet_Membership]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Membership](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[UserId] [uniqueidentifier] NOT NULL,
	[Password] [nvarchar](128)  NOT NULL,
	[PasswordFormat] [int] NOT NULL,
	[PasswordSalt] [nvarchar](128)  NOT NULL,
	[MobilePIN] [nvarchar](16)  NULL,
	[Email] [nvarchar](256)  NULL,
	[LoweredEmail] [nvarchar](256)  NULL,
	[PasswordQuestion] [nvarchar](256)  NULL,
	[PasswordAnswer] [nvarchar](128)  NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAttemptWindowStart] [datetime] NOT NULL,
	[FailedPasswordAnswerAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttemptWindowStart] [datetime] NOT NULL,
	[Comment] [ntext]  NULL,
 CONSTRAINT [PK__aspnet_Membershi__6265874F] PRIMARY KEY NONCLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]') AND name = N'aspnet_Membership_index')
CREATE CLUSTERED INDEX [aspnet_Membership_index] ON [dbo].[aspnet_Membership] 
(
	[ApplicationId] ASC,
	[LoweredEmail] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[aspnet_Roles]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Roles](
	[ApplicationId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256)  NOT NULL,
	[LoweredRoleName] [nvarchar](256)  NOT NULL,
	[Description] [nvarchar](256)  NULL,
 CONSTRAINT [PK__aspnet_Roles__00EA0E6F] PRIMARY KEY NONCLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]') AND name = N'aspnet_Roles_index1')
CREATE UNIQUE CLUSTERED INDEX [aspnet_Roles_index1] ON [dbo].[aspnet_Roles] 
(
	[ApplicationId] ASC,
	[LoweredRoleName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  Table [dbo].[aspnet_Profile]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_Profile](
	[UserId] [uniqueidentifier] NOT NULL,
	[PropertyNames] [ntext]  NOT NULL,
	[PropertyValuesString] [ntext]  NOT NULL,
	[PropertyValuesBinary] [image] NOT NULL,
	[LastUpdatedDate] [datetime] NOT NULL,
 CONSTRAINT [PK__aspnet_Profile__7760A435] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
/****** Object:  Table [dbo].[aspnet_UsersInRoles]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[aspnet_UsersInRoles](
	[UserId] [uniqueidentifier] NOT NULL,
	[RoleId] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK__aspnet_UsersInRo__04BA9F53] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
)
END
GO
IF NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]') AND name = N'aspnet_UsersInRoles_index')
CREATE NONCLUSTERED INDEX [aspnet_UsersInRoles_index] ON [dbo].[aspnet_UsersInRoles] 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_IsUserInRole]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_IsUserInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_IsUserInRole]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(2)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    DECLARE @RoleId uniqueidentifier
    SELECT  @RoleId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(2)

    SELECT  @RoleId = RoleId
    FROM    dbo.aspnet_Roles
    WHERE   LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
        RETURN(3)

    IF (EXISTS( SELECT * FROM dbo.aspnet_UsersInRoles WHERE  UserId = @UserId AND RoleId = @RoleId))
        RETURN(1)
    ELSE
        RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetRolesForUser]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetRolesForUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetRolesForUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT  @UserId = UserId
    FROM    dbo.aspnet_Users
    WHERE   LoweredUserName = LOWER(@UserName) AND ApplicationId = @ApplicationId

    IF (@UserId IS NULL)
        RETURN(1)

    SELECT r.RoleName
    FROM   dbo.aspnet_Roles r, dbo.aspnet_UsersInRoles ur
    WHERE  r.RoleId = ur.RoleId AND r.ApplicationId = @ApplicationId AND ur.UserId = @UserId
    ORDER BY r.RoleName
    RETURN (0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_DeleteRole]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_DeleteRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_DeleteRole]
    @ApplicationName            nvarchar(256),
    @RoleName                   nvarchar(256),
    @DeleteOnlyIfRoleIsEmpty    bit
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    DECLARE @RoleId   uniqueidentifier
    SELECT  @RoleId = NULL
    SELECT  @RoleId = RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId

    IF (@RoleId IS NULL)
    BEGIN
        SELECT @ErrorCode = 1
        GOTO Cleanup
    END
    IF (@DeleteOnlyIfRoleIsEmpty <> 0)
    BEGIN
        IF (EXISTS (SELECT RoleId FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId))
        BEGIN
            SELECT @ErrorCode = 2
            GOTO Cleanup
        END
    END


    DELETE FROM dbo.aspnet_UsersInRoles  WHERE @RoleId = RoleId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DELETE FROM dbo.aspnet_Roles WHERE @RoleId = RoleId  AND ApplicationId = @ApplicationId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_AddUsersToRoles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_AddUsersToRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_AddUsersToRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000),
	@CurrentTimeUtc   datetime
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)
	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames	table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles	table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers	table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num		int
	DECLARE @Pos		int
	DECLARE @NextPos	int
	DECLARE @Name		nvarchar(256)

	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		SELECT TOP 1 Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END

	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1

	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	IF (@@ROWCOUNT <> @Num)
	BEGIN
		DELETE FROM @tbNames
		WHERE LOWER(Name) IN (SELECT LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE au.UserId = u.UserId)

		INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
		  SELECT @AppId, NEWID(), Name, LOWER(Name), 0, @CurrentTimeUtc
		  FROM   @tbNames

		INSERT INTO @tbUsers
		  SELECT  UserId
		  FROM	dbo.aspnet_Users au, @tbNames t
		  WHERE   LOWER(t.Name) = au.LoweredUserName AND au.ApplicationId = @AppId
	END

	IF (EXISTS (SELECT * FROM dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr WHERE tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId))
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 dbo.aspnet_UsersInRoles ur, @tbUsers tu, @tbRoles tr, aspnet_Users u, aspnet_Roles r
		WHERE		u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND tu.UserId = ur.UserId AND tr.RoleId = ur.RoleId

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	INSERT INTO dbo.aspnet_UsersInRoles (UserId, RoleId)
	SELECT UserId, RoleId
	FROM @tbUsers, @tbRoles

	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     ' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles]
	@ApplicationName  nvarchar(256),
	@UserNames		  nvarchar(4000),
	@RoleNames		  nvarchar(4000)
AS
BEGIN
	DECLARE @AppId uniqueidentifier
	SELECT  @AppId = NULL
	SELECT  @AppId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
	IF (@AppId IS NULL)
		RETURN(2)


	DECLARE @TranStarted   bit
	SET @TranStarted = 0

	IF( @@TRANCOUNT = 0 )
	BEGIN
		BEGIN TRANSACTION
		SET @TranStarted = 1
	END

	DECLARE @tbNames  table(Name nvarchar(256) NOT NULL PRIMARY KEY)
	DECLARE @tbRoles  table(RoleId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @tbUsers  table(UserId uniqueidentifier NOT NULL PRIMARY KEY)
	DECLARE @Num	  int
	DECLARE @Pos	  int
	DECLARE @NextPos  int
	DECLARE @Name	  nvarchar(256)
	DECLARE @CountAll int
	DECLARE @CountU	  int
	DECLARE @CountR	  int


	SET @Num = 0
	SET @Pos = 1
	WHILE(@Pos <= LEN(@RoleNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @RoleNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@RoleNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@RoleNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbRoles
	  SELECT RoleId
	  FROM   dbo.aspnet_Roles ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredRoleName AND ar.ApplicationId = @AppId
	SELECT @CountR = @@ROWCOUNT

	IF (@CountR <> @Num)
	BEGIN
		SELECT TOP 1 N'''', Name
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT ar.LoweredRoleName FROM dbo.aspnet_Roles ar,  @tbRoles r WHERE r.RoleId = ar.RoleId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(2)
	END


	DELETE FROM @tbNames WHERE 1=1
	SET @Num = 0
	SET @Pos = 1


	WHILE(@Pos <= LEN(@UserNames))
	BEGIN
		SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @Pos)
		IF (@NextPos = 0 OR @NextPos IS NULL)
			SELECT @NextPos = LEN(@UserNames) + 1
		SELECT @Name = RTRIM(LTRIM(SUBSTRING(@UserNames, @Pos, @NextPos - @Pos)))
		SELECT @Pos = @NextPos+1

		INSERT INTO @tbNames VALUES (@Name)
		SET @Num = @Num + 1
	END

	INSERT INTO @tbUsers
	  SELECT UserId
	  FROM   dbo.aspnet_Users ar, @tbNames t
	  WHERE  LOWER(t.Name) = ar.LoweredUserName AND ar.ApplicationId = @AppId

	SELECT @CountU = @@ROWCOUNT
	IF (@CountU <> @Num)
	BEGIN
		SELECT TOP 1 Name, N''''
		FROM   @tbNames
		WHERE  LOWER(Name) NOT IN (SELECT au.LoweredUserName FROM dbo.aspnet_Users au,  @tbUsers u WHERE u.UserId = au.UserId)

		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(1)
	END

	SELECT  @CountAll = COUNT(*)
	FROM	dbo.aspnet_UsersInRoles ur, @tbUsers u, @tbRoles r
	WHERE   ur.UserId = u.UserId AND ur.RoleId = r.RoleId

	IF (@CountAll <> @CountU * @CountR)
	BEGIN
		SELECT TOP 1 UserName, RoleName
		FROM		 @tbUsers tu, @tbRoles tr, dbo.aspnet_Users u, dbo.aspnet_Roles r
		WHERE		 u.UserId = tu.UserId AND r.RoleId = tr.RoleId AND
					 tu.UserId NOT IN (SELECT ur.UserId FROM dbo.aspnet_UsersInRoles ur WHERE ur.RoleId = tr.RoleId) AND
					 tr.RoleId NOT IN (SELECT ur.RoleId FROM dbo.aspnet_UsersInRoles ur WHERE ur.UserId = tu.UserId)
		IF( @TranStarted = 1 )
			ROLLBACK TRANSACTION
		RETURN(3)
	END

	DELETE FROM dbo.aspnet_UsersInRoles
	WHERE UserId IN (SELECT UserId FROM @tbUsers)
	  AND RoleId IN (SELECT RoleId FROM @tbRoles)
	IF( @TranStarted = 1 )
		COMMIT TRANSACTION
	RETURN(0)
END
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        ' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_GetUsersInRoles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_GetUsersInRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_GetUsersInRoles]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId
    ORDER BY u.UserName
    RETURN(0)
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_UsersInRoles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_UsersInRoles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_UsersInRoles]
  AS SELECT [dbo].[aspnet_UsersInRoles].[UserId], [dbo].[aspnet_UsersInRoles].[RoleId]
  FROM [dbo].[aspnet_UsersInRoles]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UsersInRoles_FindUsersInRole]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles_FindUsersInRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UsersInRoles_FindUsersInRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256),
    @UserNameToMatch  nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(1)
     DECLARE @RoleId uniqueidentifier
     SELECT  @RoleId = NULL

     SELECT  @RoleId = RoleId
     FROM    dbo.aspnet_Roles
     WHERE   LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId

     IF (@RoleId IS NULL)
         RETURN(1)

    SELECT u.UserName
    FROM   dbo.aspnet_Users u, dbo.aspnet_UsersInRoles ur
    WHERE  u.UserId = ur.UserId AND @RoleId = ur.RoleId AND u.ApplicationId = @ApplicationId AND LoweredUserName LIKE LOWER(@UserNameToMatch)
    ORDER BY u.UserName
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Paths_CreatePath]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Paths_CreatePath]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Paths_CreatePath]
    @ApplicationId UNIQUEIDENTIFIER,
    @Path           NVARCHAR(256),
    @PathId         UNIQUEIDENTIFIER OUTPUT
AS
BEGIN
    BEGIN TRANSACTION
    IF (NOT EXISTS(SELECT * FROM dbo.aspnet_Paths WHERE LoweredPath = LOWER(@Path) AND ApplicationId = @ApplicationId))
    BEGIN
        INSERT dbo.aspnet_Paths (ApplicationId, Path, LoweredPath) VALUES (@ApplicationId, @Path, LOWER(@Path))
    END
    COMMIT TRANSACTION
    SELECT @PathId = PathId FROM dbo.aspnet_Paths WHERE LOWER(@Path) = LoweredPath AND ApplicationId = @ApplicationId
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Paths]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Paths]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Paths]
  AS SELECT [dbo].[aspnet_Paths].[ApplicationId], [dbo].[aspnet_Paths].[PathId], [dbo].[aspnet_Paths].[Path], [dbo].[aspnet_Paths].[LoweredPath]
  FROM [dbo].[aspnet_Paths]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_Shared]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_Shared]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_Shared]
  AS SELECT [dbo].[aspnet_PersonalizationAllUsers].[PathId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationAllUsers].[PageSettings]), [dbo].[aspnet_PersonalizationAllUsers].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationAllUsers]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_WebPartState_User]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_WebPartState_User]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_WebPartState_User]
  AS SELECT [dbo].[aspnet_PersonalizationPerUser].[PathId], [dbo].[aspnet_PersonalizationPerUser].[UserId], [DataSize]=DATALENGTH([dbo].[aspnet_PersonalizationPerUser].[PageSettings]), [dbo].[aspnet_PersonalizationPerUser].[LastUpdatedDate]
  FROM [dbo].[aspnet_PersonalizationPerUser]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_WebEvent_LogEvent]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_WebEvent_LogEvent]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_WebEvent_LogEvent]
        @EventId         char(32),
        @EventTimeUtc    datetime,
        @EventTime       datetime,
        @EventType       nvarchar(256),
        @EventSequence   decimal(19,0),
        @EventOccurrence decimal(19,0),
        @EventCode       int,
        @EventDetailCode int,
        @Message         nvarchar(1024),
        @ApplicationPath nvarchar(256),
        @ApplicationVirtualPath nvarchar(256),
        @MachineName    nvarchar(256),
        @RequestUrl      nvarchar(1024),
        @ExceptionType   nvarchar(256),
        @Details         ntext
AS
BEGIN
    INSERT
        dbo.aspnet_WebEvent_Events
        (
            EventId,
            EventTimeUtc,
            EventTime,
            EventType,
            EventSequence,
            EventOccurrence,
            EventCode,
            EventDetailCode,
            Message,
            ApplicationPath,
            ApplicationVirtualPath,
            MachineName,
            RequestUrl,
            ExceptionType,
            Details
        )
    VALUES
    (
        @EventId,
        @EventTimeUtc,
        @EventTime,
        @EventType,
        @EventSequence,
        @EventOccurrence,
        @EventCode,
        @EventDetailCode,
        @Message,
        @ApplicationPath,
        @ApplicationVirtualPath,
        @MachineName,
        @RequestUrl,
        @ExceptionType,
        @Details
    )
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_DeleteUser]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_DeleteUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Users_DeleteUser]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @TablesToDeleteFrom int,
    @NumTablesDeletedFrom int OUTPUT
AS
BEGIN
    DECLARE @UserId               uniqueidentifier
    SELECT  @UserId               = NULL
    SELECT  @NumTablesDeletedFrom = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    DECLARE @ErrorCode   int
    DECLARE @RowCount    int

    SET @ErrorCode = 0
    SET @RowCount  = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   u.LoweredUserName       = LOWER(@UserName)
        AND u.ApplicationId         = a.ApplicationId
        AND LOWER(@ApplicationName) = a.LoweredApplicationName

    IF (@UserId IS NULL)
    BEGIN
        GOTO Cleanup
    END

    -- Delete from Membership table if (@TablesToDeleteFrom & 1) is set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        DELETE FROM dbo.aspnet_Membership WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
               @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_UsersInRoles table if (@TablesToDeleteFrom & 2) is set
    IF ((@TablesToDeleteFrom & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_UsersInRoles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_UsersInRoles WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Profile table if (@TablesToDeleteFrom & 4) is set
    IF ((@TablesToDeleteFrom & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_Profile WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_PersonalizationPerUser table if (@TablesToDeleteFrom & 8) is set
    IF ((@TablesToDeleteFrom & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    -- Delete from aspnet_Users table if (@TablesToDeleteFrom & 1,2,4 & 8) are all set
    IF ((@TablesToDeleteFrom & 1) <> 0 AND
        (@TablesToDeleteFrom & 2) <> 0 AND
        (@TablesToDeleteFrom & 4) <> 0 AND
        (@TablesToDeleteFrom & 8) <> 0 AND
        (EXISTS (SELECT UserId FROM dbo.aspnet_Users WHERE @UserId = UserId)))
    BEGIN
        DELETE FROM dbo.aspnet_Users WHERE @UserId = UserId

        SELECT @ErrorCode = @@ERROR,
                @RowCount = @@ROWCOUNT

        IF( @ErrorCode <> 0 )
            GOTO Cleanup

        IF (@RowCount <> 0)
            SELECT  @NumTablesDeletedFrom = @NumTablesDeletedFrom + 1
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:
    SET @NumTablesDeletedFrom = 0

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
	    ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_AnyDataInTables]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_AnyDataInTables]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_AnyDataInTables]
    @TablesToCheck int
AS
BEGIN
    -- Check Membership table if (@TablesToCheck & 1) is set
    IF ((@TablesToCheck & 1) <> 0 AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_MembershipUsers'') AND (type = ''V''))))
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Membership))
        BEGIN
            SELECT N''aspnet_Membership''
            RETURN
        END
    END

    -- Check aspnet_Roles table if (@TablesToCheck & 2) is set
    IF ((@TablesToCheck & 2) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Roles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 RoleId FROM dbo.aspnet_Roles))
        BEGIN
            SELECT N''aspnet_Roles''
            RETURN
        END
    END

    -- Check aspnet_Profile table if (@TablesToCheck & 4) is set
    IF ((@TablesToCheck & 4) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_Profiles'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Profile))
        BEGIN
            SELECT N''aspnet_Profile''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 8) is set
    IF ((@TablesToCheck & 8) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''vw_aspnet_WebPartState_User'') AND (type = ''V''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_PersonalizationPerUser))
        BEGIN
            SELECT N''aspnet_PersonalizationPerUser''
            RETURN
        END
    END

    -- Check aspnet_PersonalizationPerUser table if (@TablesToCheck & 16) is set
    IF ((@TablesToCheck & 16) <> 0  AND
        (EXISTS (SELECT name FROM sysobjects WHERE (name = N''aspnet_WebEvent_LogEvent'') AND (type = ''P''))) )
    BEGIN
        IF (EXISTS(SELECT TOP 1 * FROM dbo.aspnet_WebEvent_Events))
        BEGIN
            SELECT N''aspnet_WebEvent_Events''
            RETURN
        END
    END

    -- Check aspnet_Users table if (@TablesToCheck & 1,2,4 & 8) are all set
    IF ((@TablesToCheck & 1) <> 0 AND
        (@TablesToCheck & 2) <> 0 AND
        (@TablesToCheck & 4) <> 0 AND
        (@TablesToCheck & 8) <> 0 AND
        (@TablesToCheck & 32) <> 0 AND
        (@TablesToCheck & 128) <> 0 AND
        (@TablesToCheck & 256) <> 0 AND
        (@TablesToCheck & 512) <> 0 AND
        (@TablesToCheck & 1024) <> 0)
    BEGIN
        IF (EXISTS(SELECT TOP 1 UserId FROM dbo.aspnet_Users))
        BEGIN
            SELECT N''aspnet_Users''
            RETURN
        END
        IF (EXISTS(SELECT TOP 1 ApplicationId FROM dbo.aspnet_Applications))
        BEGIN
            SELECT N''aspnet_Applications''
            RETURN
        END
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Applications_CreateApplication]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Applications_CreateApplication]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Applications_CreateApplication]
    @ApplicationName      nvarchar(256),
    @ApplicationId        uniqueidentifier OUTPUT
AS
BEGIN
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName

    IF(@ApplicationId IS NULL)
    BEGIN
        DECLARE @TranStarted   bit
        SET @TranStarted = 0

        IF( @@TRANCOUNT = 0 )
        BEGIN
	        BEGIN TRANSACTION
	        SET @TranStarted = 1
        END
        ELSE
    	    SET @TranStarted = 0

        SELECT  @ApplicationId = ApplicationId
        FROM dbo.aspnet_Applications WITH (UPDLOCK, HOLDLOCK)
        WHERE LOWER(@ApplicationName) = LoweredApplicationName

        IF(@ApplicationId IS NULL)
        BEGIN
            SELECT  @ApplicationId = NEWID()
            INSERT  dbo.aspnet_Applications (ApplicationId, ApplicationName, LoweredApplicationName)
            VALUES  (@ApplicationId, @ApplicationName, LOWER(@ApplicationName))
        END


        IF( @TranStarted = 1 )
        BEGIN
            IF(@@ERROR = 0)
            BEGIN
	        SET @TranStarted = 0
	        COMMIT TRANSACTION
            END
            ELSE
            BEGIN
                SET @TranStarted = 0
                ROLLBACK TRANSACTION
            END
        END
    END
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Applications]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Applications]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Applications]
  AS SELECT [dbo].[aspnet_Applications].[ApplicationName], [dbo].[aspnet_Applications].[LoweredApplicationName], [dbo].[aspnet_Applications].[ApplicationId], [dbo].[aspnet_Applications].[Description]
  FROM [dbo].[aspnet_Applications]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByName]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByName]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier

    IF (@UpdateLastActivity = 1)
    BEGIN
        -- select user ID from aspnet_users table
        SELECT TOP 1 @UserId = u.UserId
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1

        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        WHERE    @UserId = UserId

        SELECT m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut, m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  @UserId = u.UserId AND u.UserId = m.UserId 
    END
    ELSE
    BEGIN
        SELECT TOP 1 m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
                m.CreateDate, m.LastLoginDate, u.LastActivityDate, m.LastPasswordChangedDate,
                u.UserId, m.IsLockedOut,m.LastLockoutDate
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE    LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                LOWER(@UserName) = u.LoweredUserName AND u.UserId = m.UserId

        IF (@@ROWCOUNT = 0) -- Username not found
            RETURN -1
    END

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByEmail]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByEmail]
    @ApplicationName  nvarchar(256),
    @Email            nvarchar(256)
AS
BEGIN
    IF( @Email IS NULL )
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                m.LoweredEmail IS NULL
    ELSE
        SELECT  u.UserName
        FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
                u.ApplicationId = a.ApplicationId    AND
                u.UserId = m.UserId AND
                LOWER(@Email) = m.LoweredEmail

    IF (@@rowcount = 0)
        RETURN(1)
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPasswordWithFormat]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPasswordWithFormat]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPasswordWithFormat]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @UpdateLastLoginActivityDate    bit,
    @CurrentTimeUtc                 datetime
AS
BEGIN
    DECLARE @IsLockedOut                        bit
    DECLARE @UserId                             uniqueidentifier
    DECLARE @Password                           nvarchar(128)
    DECLARE @PasswordSalt                       nvarchar(128)
    DECLARE @PasswordFormat                     int
    DECLARE @FailedPasswordAttemptCount         int
    DECLARE @FailedPasswordAnswerAttemptCount   int
    DECLARE @IsApproved                         bit
    DECLARE @LastActivityDate                   datetime
    DECLARE @LastLoginDate                      datetime

    SELECT  @UserId          = NULL

    SELECT  @UserId = u.UserId, @IsLockedOut = m.IsLockedOut, @Password=Password, @PasswordFormat=PasswordFormat,
            @PasswordSalt=PasswordSalt, @FailedPasswordAttemptCount=FailedPasswordAttemptCount,
		    @FailedPasswordAnswerAttemptCount=FailedPasswordAnswerAttemptCount, @IsApproved=IsApproved,
            @LastActivityDate = LastActivityDate, @LastLoginDate = LastLoginDate
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF (@UserId IS NULL)
        RETURN 1

    IF (@IsLockedOut = 1)
        RETURN 99

    SELECT   @Password, @PasswordFormat, @PasswordSalt, @FailedPasswordAttemptCount,
             @FailedPasswordAnswerAttemptCount, @IsApproved, @LastLoginDate, @LastActivityDate

    IF (@UpdateLastLoginActivityDate = 1 AND @IsApproved = 1)
    BEGIN
        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @CurrentTimeUtc
        WHERE   UserId = @UserId

        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @CurrentTimeUtc
        WHERE   @UserId = UserId
    END


    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUserInfo]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUserInfo]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUserInfo]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @IsPasswordCorrect              bit,
    @UpdateLastLoginActivityDate    bit,
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @LastLoginDate                  datetime,
    @LastActivityDate               datetime
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @IsApproved                             bit
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @IsApproved = m.IsApproved,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        GOTO Cleanup
    END

    IF( @IsPasswordCorrect = 0 )
    BEGIN
        IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAttemptWindowStart ) )
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = 1
        END
        ELSE
        BEGIN
            SET @FailedPasswordAttemptWindowStart = @CurrentTimeUtc
            SET @FailedPasswordAttemptCount = @FailedPasswordAttemptCount + 1
        END

        BEGIN
            IF( @FailedPasswordAttemptCount >= @MaxInvalidPasswordAttempts )
            BEGIN
                SET @IsLockedOut = 1
                SET @LastLockoutDate = @CurrentTimeUtc
            END
        END
    END
    ELSE
    BEGIN
        IF( @FailedPasswordAttemptCount > 0 OR @FailedPasswordAnswerAttemptCount > 0 )
        BEGIN
            SET @FailedPasswordAttemptCount = 0
            SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @FailedPasswordAnswerAttemptCount = 0
            SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
        END
    END

    IF( @UpdateLastLoginActivityDate = 1 )
    BEGIN
        UPDATE  dbo.aspnet_Users
        SET     LastActivityDate = @LastActivityDate
        WHERE   @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END

        UPDATE  dbo.aspnet_Membership
        SET     LastLoginDate = @LastLoginDate
        WHERE   UserId = @UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END


    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
        FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
        FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
        FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
        FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
    WHERE @UserId = UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetPassword]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetPassword]
    @ApplicationName                nvarchar(256),
    @UserName                       nvarchar(256),
    @MaxInvalidPasswordAttempts     int,
    @PasswordAttemptWindow          int,
    @CurrentTimeUtc                 datetime,
    @PasswordAnswer                 nvarchar(128) = NULL
AS
BEGIN
    DECLARE @UserId                                 uniqueidentifier
    DECLARE @PasswordFormat                         int
    DECLARE @Password                               nvarchar(128)
    DECLARE @passAns                                nvarchar(128)
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId,
            @Password = m.Password,
            @passAns = m.PasswordAnswer,
            @PasswordFormat = m.PasswordFormat,
            @IsLockedOut = m.IsLockedOut,
            @LastLockoutDate = m.LastLockoutDate,
            @FailedPasswordAttemptCount = m.FailedPasswordAttemptCount,
            @FailedPasswordAttemptWindowStart = m.FailedPasswordAttemptWindowStart,
            @FailedPasswordAnswerAttemptCount = m.FailedPasswordAnswerAttemptCount,
            @FailedPasswordAnswerAttemptWindowStart = m.FailedPasswordAnswerAttemptWindowStart
    FROM    dbo.aspnet_Applications a, dbo.aspnet_Users u, dbo.aspnet_Membership m WITH ( UPDLOCK )
    WHERE   LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.ApplicationId = a.ApplicationId    AND
            u.UserId = m.UserId AND
            LOWER(@UserName) = u.LoweredUserName

    IF ( @@rowcount = 0 )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    IF ( NOT( @PasswordAnswer IS NULL ) )
    BEGIN
        IF( ( @passAns IS NULL ) OR ( LOWER( @passAns ) <> LOWER( @PasswordAnswer ) ) )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
        ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    IF( @ErrorCode = 0 )
        SELECT @Password, @PasswordFormat

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_SetPassword]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_SetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_SetPassword]
    @ApplicationName  nvarchar(256),
    @UserName         nvarchar(256),
    @NewPassword      nvarchar(128),
    @PasswordSalt     nvarchar(128),
    @CurrentTimeUtc   datetime,
    @PasswordFormat   int = 0
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    UPDATE dbo.aspnet_Membership
    SET Password = @NewPassword, PasswordFormat = @PasswordFormat, PasswordSalt = @PasswordSalt,
        LastPasswordChangedDate = @CurrentTimeUtc
    WHERE @UserId = UserId
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Personalization_GetApplicationId]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Personalization_GetApplicationId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Personalization_GetApplicationId] (
    @ApplicationName NVARCHAR(256),
    @ApplicationId UNIQUEIDENTIFIER OUT)
AS
BEGIN
    SELECT @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_GetAllRoles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_GetAllRoles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_GetAllRoles] (
    @ApplicationName           nvarchar(256))
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN
    SELECT RoleName
    FROM   dbo.aspnet_Roles WHERE ApplicationId = @ApplicationId
    ORDER BY RoleName
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_RoleExists]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_RoleExists]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Roles_RoleExists]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN(0)
    IF (EXISTS (SELECT RoleName FROM dbo.aspnet_Roles WHERE LOWER(@RoleName) = LoweredRoleName AND ApplicationId = @ApplicationId ))
        RETURN(1)
    ELSE
        RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProperties]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProperties]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)

    IF (@UserId IS NULL)
        RETURN
    SELECT TOP 1 PropertyNames, PropertyValuesString, PropertyValuesBinary
    FROM         dbo.aspnet_Profile
    WHERE        UserId = @UserId

    IF (@@ROWCOUNT > 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetProfiles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @PageIndex              int,
    @PageSize               int,
    @UserNameToMatch        nvarchar(256) = NULL,
    @InactiveSinceDate      datetime      = NULL
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT  u.UserId
        FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
        WHERE   ApplicationId = @ApplicationId
            AND u.UserId = p.UserId
            AND (@InactiveSinceDate IS NULL OR LastActivityDate <= @InactiveSinceDate)
            AND (     (@ProfileAuthOptions = 2)
                   OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                   OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                 )
            AND (@UserNameToMatch IS NULL OR LoweredUserName LIKE LOWER(@UserNameToMatch))
        ORDER BY UserName

    SELECT  u.UserName, u.IsAnonymous, u.LastActivityDate, p.LastUpdatedDate,
            DATALENGTH(p.PropertyNames) + DATALENGTH(p.PropertyValuesString) + DATALENGTH(p.PropertyValuesBinary)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p, #PageIndexForUsers i
    WHERE   u.UserId = p.UserId AND p.UserId = i.UserId AND i.IndexId >= @PageLowerBound AND i.IndexId <= @PageUpperBound

    SELECT COUNT(*)
    FROM   #PageIndexForUsers

    DROP TABLE #PageIndexForUsers
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteInactiveProfiles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT  0
        RETURN
    END

    DELETE
    FROM    dbo.aspnet_Profile
    WHERE   UserId IN
            (   SELECT  UserId
                FROM    dbo.aspnet_Users u
                WHERE   ApplicationId = @ApplicationId
                        AND (LastActivityDate <= @InactiveSinceDate)
                        AND (
                                (@ProfileAuthOptions = 2)
                             OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                             OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
                            )
            )

    SELECT  @@ROWCOUNT
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles]
    @ApplicationName        nvarchar(256),
    @ProfileAuthOptions     int,
    @InactiveSinceDate      datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
    BEGIN
        SELECT 0
        RETURN
    END

    SELECT  COUNT(*)
    FROM    dbo.aspnet_Users u, dbo.aspnet_Profile p
    WHERE   ApplicationId = @ApplicationId
        AND u.UserId = p.UserId
        AND (LastActivityDate <= @InactiveSinceDate)
        AND (
                (@ProfileAuthOptions = 2)
                OR (@ProfileAuthOptions = 0 AND IsAnonymous = 1)
                OR (@ProfileAuthOptions = 1 AND IsAnonymous = 0)
            )
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ResetPassword]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ResetPassword]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ResetPassword]
    @ApplicationName             nvarchar(256),
    @UserName                    nvarchar(256),
    @NewPassword                 nvarchar(128),
    @MaxInvalidPasswordAttempts  int,
    @PasswordAttemptWindow       int,
    @PasswordSalt                nvarchar(128),
    @CurrentTimeUtc              datetime,
    @PasswordFormat              int = 0,
    @PasswordAnswer              nvarchar(128) = NULL
AS
BEGIN
    DECLARE @IsLockedOut                            bit
    DECLARE @LastLockoutDate                        datetime
    DECLARE @FailedPasswordAttemptCount             int
    DECLARE @FailedPasswordAttemptWindowStart       datetime
    DECLARE @FailedPasswordAnswerAttemptCount       int
    DECLARE @FailedPasswordAnswerAttemptWindowStart datetime

    DECLARE @UserId                                 uniqueidentifier
    SET     @UserId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    SELECT @IsLockedOut = IsLockedOut,
           @LastLockoutDate = LastLockoutDate,
           @FailedPasswordAttemptCount = FailedPasswordAttemptCount,
           @FailedPasswordAttemptWindowStart = FailedPasswordAttemptWindowStart,
           @FailedPasswordAnswerAttemptCount = FailedPasswordAnswerAttemptCount,
           @FailedPasswordAnswerAttemptWindowStart = FailedPasswordAnswerAttemptWindowStart
    FROM dbo.aspnet_Membership WITH ( UPDLOCK )
    WHERE @UserId = UserId

    IF( @IsLockedOut = 1 )
    BEGIN
        SET @ErrorCode = 99
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Membership
    SET    Password = @NewPassword,
           LastPasswordChangedDate = @CurrentTimeUtc,
           PasswordFormat = @PasswordFormat,
           PasswordSalt = @PasswordSalt
    WHERE  @UserId = UserId AND
           ( ( @PasswordAnswer IS NULL ) OR ( LOWER( PasswordAnswer ) = LOWER( @PasswordAnswer ) ) )

    IF ( @@ROWCOUNT = 0 )
        BEGIN
            IF( @CurrentTimeUtc > DATEADD( minute, @PasswordAttemptWindow, @FailedPasswordAnswerAttemptWindowStart ) )
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = 1
            END
            ELSE
            BEGIN
                SET @FailedPasswordAnswerAttemptWindowStart = @CurrentTimeUtc
                SET @FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount + 1
            END

            BEGIN
                IF( @FailedPasswordAnswerAttemptCount >= @MaxInvalidPasswordAttempts )
                BEGIN
                    SET @IsLockedOut = 1
                    SET @LastLockoutDate = @CurrentTimeUtc
                END
            END

            SET @ErrorCode = 3
        END
    ELSE
        BEGIN
            IF( @FailedPasswordAnswerAttemptCount > 0 )
            BEGIN
                SET @FailedPasswordAnswerAttemptCount = 0
                SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )
            END
        END

    IF( NOT ( @PasswordAnswer IS NULL ) )
    BEGIN
        UPDATE dbo.aspnet_Membership
        SET IsLockedOut = @IsLockedOut, LastLockoutDate = @LastLockoutDate,
            FailedPasswordAttemptCount = @FailedPasswordAttemptCount,
            FailedPasswordAttemptWindowStart = @FailedPasswordAttemptWindowStart,
            FailedPasswordAnswerAttemptCount = @FailedPasswordAnswerAttemptCount,
            FailedPasswordAnswerAttemptWindowStart = @FailedPasswordAnswerAttemptWindowStart
        WHERE @UserId = UserId

        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN @ErrorCode

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UnlockUser]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UnlockUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UnlockUser]
    @ApplicationName                         nvarchar(256),
    @UserName                                nvarchar(256)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF ( @UserId IS NULL )
        RETURN 1

    UPDATE dbo.aspnet_Membership
    SET IsLockedOut = 0,
        FailedPasswordAttemptCount = 0,
        FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        FailedPasswordAnswerAttemptCount = 0,
        FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 ),
        LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )
    WHERE @UserId = UserId

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_UpdateUser]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_UpdateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_UpdateUser]
    @ApplicationName      nvarchar(256),
    @UserName             nvarchar(256),
    @Email                nvarchar(256),
    @Comment              ntext,
    @IsApproved           bit,
    @LastLoginDate        datetime,
    @LastActivityDate     datetime,
    @UniqueEmail          int,
    @CurrentTimeUtc       datetime
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId, @ApplicationId = a.ApplicationId
    FROM    dbo.aspnet_Users u, dbo.aspnet_Applications a, dbo.aspnet_Membership m
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId

    IF (@UserId IS NULL)
        RETURN(1)

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership WITH (UPDLOCK, HOLDLOCK)
                    WHERE ApplicationId = @ApplicationId  AND @UserId <> UserId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            RETURN(7)
        END
    END

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
	SET @TranStarted = 0

    UPDATE dbo.aspnet_Users WITH (ROWLOCK)
    SET
         LastActivityDate = @LastActivityDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    UPDATE dbo.aspnet_Membership WITH (ROWLOCK)
    SET
         Email            = @Email,
         LoweredEmail     = LOWER(@Email),
         Comment          = @Comment,
         IsApproved       = @IsApproved,
         LastLoginDate    = @LastLoginDate
    WHERE
       @UserId = UserId

    IF( @@ERROR <> 0 )
        GOTO Cleanup

    IF( @TranStarted = 1 )
    BEGIN
	SET @TranStarted = 0
	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN -1
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer]
    @ApplicationName       nvarchar(256),
    @UserName              nvarchar(256),
    @NewPasswordQuestion   nvarchar(256),
    @NewPasswordAnswer     nvarchar(128)
AS
BEGIN
    DECLARE @UserId uniqueidentifier
    SELECT  @UserId = NULL
    SELECT  @UserId = u.UserId
    FROM    dbo.aspnet_Membership m, dbo.aspnet_Users u, dbo.aspnet_Applications a
    WHERE   LoweredUserName = LOWER(@UserName) AND
            u.ApplicationId = a.ApplicationId  AND
            LOWER(@ApplicationName) = a.LoweredApplicationName AND
            u.UserId = m.UserId
    IF (@UserId IS NULL)
    BEGIN
        RETURN(1)
    END

    UPDATE dbo.aspnet_Membership
    SET    PasswordQuestion = @NewPasswordQuestion, PasswordAnswer = @NewPasswordAnswer
    WHERE  UserId=@UserId
    RETURN(0)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetAllUsers]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetAllUsers]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetAllUsers]
    @ApplicationName       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0


    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
    SELECT u.UserId
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u
    WHERE  u.ApplicationId = @ApplicationId AND u.UserId = m.UserId
    ORDER BY u.UserName

    SELECT @TotalRecords = @@ROWCOUNT

    SELECT u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName
    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetNumberOfUsersOnline]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetNumberOfUsersOnline]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetNumberOfUsersOnline]
    @ApplicationName            nvarchar(256),
    @MinutesSinceLastInActive   int,
    @CurrentTimeUtc             datetime
AS
BEGIN
    DECLARE @DateActive datetime
    SELECT  @DateActive = DATEADD(minute,  -(@MinutesSinceLastInActive), @CurrentTimeUtc)

    DECLARE @NumOnline int
    SELECT  @NumOnline = COUNT(*)
    FROM    dbo.aspnet_Users u(NOLOCK),
            dbo.aspnet_Applications a(NOLOCK),
            dbo.aspnet_Membership m(NOLOCK)
    WHERE   u.ApplicationId = a.ApplicationId                  AND
            LastActivityDate > @DateActive                     AND
            a.LoweredApplicationName = LOWER(@ApplicationName) AND
            u.UserId = m.UserId
    RETURN(@NumOnline)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByName]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByName]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByName]
    @ApplicationName       nvarchar(256),
    @UserNameToMatch       nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    INSERT INTO #PageIndexForUsers (UserId)
        SELECT u.UserId
        FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
        WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND u.LoweredUserName LIKE LOWER(@UserNameToMatch)
        ORDER BY u.UserName


    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY u.UserName

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_FindUsersByEmail]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_FindUsersByEmail]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_FindUsersByEmail]
    @ApplicationName       nvarchar(256),
    @EmailToMatch          nvarchar(256),
    @PageIndex             int,
    @PageSize              int
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL
    SELECT  @ApplicationId = ApplicationId FROM dbo.aspnet_Applications WHERE LOWER(@ApplicationName) = LoweredApplicationName
    IF (@ApplicationId IS NULL)
        RETURN 0

    -- Set the page bounds
    DECLARE @PageLowerBound int
    DECLARE @PageUpperBound int
    DECLARE @TotalRecords   int
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table TO store the select results
    CREATE TABLE #PageIndexForUsers
    (
        IndexId int IDENTITY (0, 1) NOT NULL,
        UserId uniqueidentifier
    )

    -- Insert into our temp table
    IF( @EmailToMatch IS NULL )
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.Email IS NULL
            ORDER BY m.LoweredEmail
    ELSE
        INSERT INTO #PageIndexForUsers (UserId)
            SELECT u.UserId
            FROM   dbo.aspnet_Users u, dbo.aspnet_Membership m
            WHERE  u.ApplicationId = @ApplicationId AND m.UserId = u.UserId AND m.LoweredEmail LIKE LOWER(@EmailToMatch)
            ORDER BY m.LoweredEmail

    SELECT  u.UserName, m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate,
            m.LastLoginDate,
            u.LastActivityDate,
            m.LastPasswordChangedDate,
            u.UserId, m.IsLockedOut,
            m.LastLockoutDate
    FROM   dbo.aspnet_Membership m, dbo.aspnet_Users u, #PageIndexForUsers p
    WHERE  u.UserId = p.UserId AND u.UserId = m.UserId AND
           p.IndexId >= @PageLowerBound AND p.IndexId <= @PageUpperBound
    ORDER BY m.LoweredEmail

    SELECT  @TotalRecords = COUNT(*)
    FROM    #PageIndexForUsers
    RETURN @TotalRecords
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_MembershipUsers]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_MembershipUsers]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_MembershipUsers]
  AS SELECT [dbo].[aspnet_Membership].[UserId],
            [dbo].[aspnet_Membership].[PasswordFormat],
            [dbo].[aspnet_Membership].[MobilePIN],
            [dbo].[aspnet_Membership].[Email],
            [dbo].[aspnet_Membership].[LoweredEmail],
            [dbo].[aspnet_Membership].[PasswordQuestion],
            [dbo].[aspnet_Membership].[PasswordAnswer],
            [dbo].[aspnet_Membership].[IsApproved],
            [dbo].[aspnet_Membership].[IsLockedOut],
            [dbo].[aspnet_Membership].[CreateDate],
            [dbo].[aspnet_Membership].[LastLoginDate],
            [dbo].[aspnet_Membership].[LastPasswordChangedDate],
            [dbo].[aspnet_Membership].[LastLockoutDate],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAttemptWindowStart],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptCount],
            [dbo].[aspnet_Membership].[FailedPasswordAnswerAttemptWindowStart],
            [dbo].[aspnet_Membership].[Comment],
            [dbo].[aspnet_Users].[ApplicationId],
            [dbo].[aspnet_Users].[UserName],
            [dbo].[aspnet_Users].[MobileAlias],
            [dbo].[aspnet_Users].[IsAnonymous],
            [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Membership] INNER JOIN [dbo].[aspnet_Users]
      ON [dbo].[aspnet_Membership].[UserId] = [dbo].[aspnet_Users].[UserId]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_GetUserByUserId]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_GetUserByUserId]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_GetUserByUserId]
    @UserId               uniqueidentifier,
    @CurrentTimeUtc       datetime,
    @UpdateLastActivity   bit = 0
AS
BEGIN
    IF ( @UpdateLastActivity = 1 )
    BEGIN
        UPDATE   dbo.aspnet_Users
        SET      LastActivityDate = @CurrentTimeUtc
        FROM     dbo.aspnet_Users
        WHERE    @UserId = UserId

        IF ( @@ROWCOUNT = 0 ) -- User ID not found
            RETURN -1
    END

    SELECT  m.Email, m.PasswordQuestion, m.Comment, m.IsApproved,
            m.CreateDate, m.LastLoginDate, u.LastActivityDate,
            m.LastPasswordChangedDate, u.UserName, m.IsLockedOut,
            m.LastLockoutDate
    FROM    dbo.aspnet_Users u, dbo.aspnet_Membership m
    WHERE   @UserId = u.UserId AND u.UserId = m.UserId

    IF ( @@ROWCOUNT = 0 ) -- User ID not found
       RETURN -1

    RETURN 0
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Users]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Users]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Users]
  AS SELECT [dbo].[aspnet_Users].[ApplicationId], [dbo].[aspnet_Users].[UserId], [dbo].[aspnet_Users].[UserName], [dbo].[aspnet_Users].[LoweredUserName], [dbo].[aspnet_Users].[MobileAlias], [dbo].[aspnet_Users].[IsAnonymous], [dbo].[aspnet_Users].[LastActivityDate]
  FROM [dbo].[aspnet_Users]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Users_CreateUser]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Users_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Users_CreateUser]
    @ApplicationId    uniqueidentifier,
    @UserName         nvarchar(256),
    @IsUserAnonymous  bit,
    @LastActivityDate DATETIME,
    @UserId           uniqueidentifier OUTPUT
AS
BEGIN
    IF( @UserId IS NULL )
        SELECT @UserId = NEWID()
    ELSE
    BEGIN
        IF( EXISTS( SELECT UserId FROM dbo.aspnet_Users
                    WHERE @UserId = UserId ) )
            RETURN -1
    END

    INSERT dbo.aspnet_Users (ApplicationId, UserId, UserName, LoweredUserName, IsAnonymous, LastActivityDate)
    VALUES (@ApplicationId, @UserId, @UserName, LOWER(@UserName), @IsUserAnonymous, @LastActivityDate)

    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_UnRegisterSchemaVersion]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_UnRegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_UnRegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    DELETE FROM dbo.aspnet_SchemaVersions
        WHERE   Feature = LOWER(@Feature) AND @CompatibleSchemaVersion = CompatibleSchemaVersion
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_RegisterSchemaVersion]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_RegisterSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_RegisterSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128),
    @IsCurrentVersion          bit,
    @RemoveIncompatibleSchema  bit
AS
BEGIN
    IF( @RemoveIncompatibleSchema = 1 )
    BEGIN
        DELETE FROM dbo.aspnet_SchemaVersions WHERE Feature = LOWER( @Feature )
    END
    ELSE
    BEGIN
        IF( @IsCurrentVersion = 1 )
        BEGIN
            UPDATE dbo.aspnet_SchemaVersions
            SET IsCurrentVersion = 0
            WHERE Feature = LOWER( @Feature )
        END
    END

    INSERT  dbo.aspnet_SchemaVersions( Feature, CompatibleSchemaVersion, IsCurrentVersion )
    VALUES( LOWER( @Feature ), @CompatibleSchemaVersion, @IsCurrentVersion )
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_CheckSchemaVersion]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_CheckSchemaVersion]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_CheckSchemaVersion]
    @Feature                   nvarchar(128),
    @CompatibleSchemaVersion   nvarchar(128)
AS
BEGIN
    IF (EXISTS( SELECT  *
                FROM    dbo.aspnet_SchemaVersions
                WHERE   Feature = LOWER( @Feature ) AND
                        CompatibleSchemaVersion = @CompatibleSchemaVersion ))
        RETURN 0

    RETURN 1
END' 
END
GO
/****** Object:  View [dbo].[vw_aspnet_Profiles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Profiles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Profiles]
  AS SELECT [dbo].[aspnet_Profile].[UserId], [dbo].[aspnet_Profile].[LastUpdatedDate],
      [DataSize]=  DATALENGTH([dbo].[aspnet_Profile].[PropertyNames])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesString])
                 + DATALENGTH([dbo].[aspnet_Profile].[PropertyValuesBinary])
  FROM [dbo].[aspnet_Profile]
  '
GO
/****** Object:  View [dbo].[vw_aspnet_Roles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_aspnet_Roles]'))
EXEC dbo.sp_executesql @statement = N'
  CREATE VIEW [dbo].[vw_aspnet_Roles]
  AS SELECT [dbo].[aspnet_Roles].[ApplicationId], [dbo].[aspnet_Roles].[RoleId], [dbo].[aspnet_Roles].[RoleName], [dbo].[aspnet_Roles].[LoweredRoleName], [dbo].[aspnet_Roles].[Description]
  FROM [dbo].[aspnet_Roles]
  '
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationAllUsers p WHERE p.PathId = @PathId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path              NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    DELETE FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationAllUsers WHERE PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationAllUsers SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationAllUsers(PathId, PageSettings, LastUpdatedDate) VALUES (@PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_GetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_GetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    SELECT p.PageSettings FROM dbo.aspnet_PersonalizationPerUser p WHERE p.PathId = @PathId AND p.UserId = @UserId
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_ResetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        RETURN
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        RETURN
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    DELETE FROM dbo.aspnet_PersonalizationPerUser WHERE PathId = @PathId AND UserId = @UserId
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetSharedState]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetSharedState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] (
    @Count int OUT,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256))
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationAllUsers
        WHERE PathId IN
            (SELECT AllUsers.PathId
             FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
             WHERE Paths.ApplicationId = @ApplicationId
                   AND AllUsers.PathId = Paths.PathId
                   AND Paths.LoweredPath = LOWER(@Path))

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_ResetUserState]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_ResetUserState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_ResetUserState] (
    @Count                  int                 OUT,
    @ApplicationName        NVARCHAR(256),
    @InactiveSinceDate      DATETIME            = NULL,
    @UserName               NVARCHAR(256)       = NULL,
    @Path                   NVARCHAR(256)       = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        DELETE FROM dbo.aspnet_PersonalizationPerUser
        WHERE Id IN (SELECT PerUser.Id
                     FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
                     WHERE Paths.ApplicationId = @ApplicationId
                           AND PerUser.UserId = Users.UserId
                           AND PerUser.PathId = Paths.PathId
                           AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
                           AND (@UserName IS NULL OR Users.LoweredUserName = LOWER(@UserName))
                           AND (@Path IS NULL OR Paths.LoweredPath = LOWER(@Path)))

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_GetCountOfState]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_GetCountOfState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] (
    @Count int OUT,
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN

    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
        IF (@AllUsersScope = 1)
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND AllUsers.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
        ELSE
            SELECT @Count = COUNT(*)
            FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
            WHERE Paths.ApplicationId = @ApplicationId
                  AND PerUser.UserId = Users.UserId
                  AND PerUser.PathId = Paths.PathId
                  AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
                  AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
                  AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationPerUser_SetPageSettings]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser_SetPageSettings]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] (
    @ApplicationName  NVARCHAR(256),
    @UserName         NVARCHAR(256),
    @Path             NVARCHAR(256),
    @PageSettings     IMAGE,
    @CurrentTimeUtc   DATETIME)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    DECLARE @PathId UNIQUEIDENTIFIER
    DECLARE @UserId UNIQUEIDENTIFIER

    SELECT @ApplicationId = NULL
    SELECT @PathId = NULL
    SELECT @UserId = NULL

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    SELECT @PathId = u.PathId FROM dbo.aspnet_Paths u WHERE u.ApplicationId = @ApplicationId AND u.LoweredPath = LOWER(@Path)
    IF (@PathId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Paths_CreatePath @ApplicationId, @Path, @PathId OUTPUT
    END

    SELECT @UserId = u.UserId FROM dbo.aspnet_Users u WHERE u.ApplicationId = @ApplicationId AND u.LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
    BEGIN
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CurrentTimeUtc, @UserId OUTPUT
    END

    UPDATE   dbo.aspnet_Users WITH (ROWLOCK)
    SET      LastActivityDate = @CurrentTimeUtc
    WHERE    UserId = @UserId
    IF (@@ROWCOUNT = 0) -- Username not found
        RETURN

    IF (EXISTS(SELECT PathId FROM dbo.aspnet_PersonalizationPerUser WHERE UserId = @UserId AND PathId = @PathId))
        UPDATE dbo.aspnet_PersonalizationPerUser SET PageSettings = @PageSettings, LastUpdatedDate = @CurrentTimeUtc WHERE UserId = @UserId AND PathId = @PathId
    ELSE
        INSERT INTO dbo.aspnet_PersonalizationPerUser(UserId, PathId, PageSettings, LastUpdatedDate) VALUES (@UserId, @PathId, @PageSettings, @CurrentTimeUtc)
    RETURN 0
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_DeleteAllState]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_DeleteAllState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @Count int OUT)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        SELECT @Count = 0
    ELSE
    BEGIN
        IF (@AllUsersScope = 1)
            DELETE FROM aspnet_PersonalizationAllUsers
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)
        ELSE
            DELETE FROM aspnet_PersonalizationPerUser
            WHERE PathId IN
               (SELECT Paths.PathId
                FROM dbo.aspnet_Paths Paths
                WHERE Paths.ApplicationId = @ApplicationId)

        SELECT @Count = @@ROWCOUNT
    END
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_PersonalizationAdministration_FindState]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAdministration_FindState]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_PersonalizationAdministration_FindState] (
    @AllUsersScope bit,
    @ApplicationName NVARCHAR(256),
    @PageIndex              INT,
    @PageSize               INT,
    @Path NVARCHAR(256) = NULL,
    @UserName NVARCHAR(256) = NULL,
    @InactiveSinceDate DATETIME = NULL)
AS
BEGIN
    DECLARE @ApplicationId UNIQUEIDENTIFIER
    EXEC dbo.aspnet_Personalization_GetApplicationId @ApplicationName, @ApplicationId OUTPUT
    IF (@ApplicationId IS NULL)
        RETURN

    -- Set the page bounds
    DECLARE @PageLowerBound INT
    DECLARE @PageUpperBound INT
    DECLARE @TotalRecords   INT
    SET @PageLowerBound = @PageSize * @PageIndex
    SET @PageUpperBound = @PageSize - 1 + @PageLowerBound

    -- Create a temp table to store the selected results
    CREATE TABLE #PageIndex (
        IndexId int IDENTITY (0, 1) NOT NULL,
        ItemId UNIQUEIDENTIFIER
    )

    IF (@AllUsersScope = 1)
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT Paths.PathId
        FROM dbo.aspnet_Paths Paths,
             ((SELECT Paths.PathId
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND AllUsers.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT DISTINCT Paths.PathId
               FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Paths Paths
               WHERE Paths.ApplicationId = @ApplicationId
                      AND PerUser.PathId = Paths.PathId
                      AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path,
               SharedDataPerPath.LastUpdatedDate,
               SharedDataPerPath.SharedDataLength,
               UserDataPerPath.UserDataLength,
               UserDataPerPath.UserCount
        FROM dbo.aspnet_Paths Paths,
             ((SELECT PageIndex.ItemId AS PathId,
                      AllUsers.LastUpdatedDate AS LastUpdatedDate,
                      DATALENGTH(AllUsers.PageSettings) AS SharedDataLength
               FROM dbo.aspnet_PersonalizationAllUsers AllUsers, #PageIndex PageIndex
               WHERE AllUsers.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
              ) AS SharedDataPerPath
              FULL OUTER JOIN
              (SELECT PageIndex.ItemId AS PathId,
                      SUM(DATALENGTH(PerUser.PageSettings)) AS UserDataLength,
                      COUNT(*) AS UserCount
               FROM aspnet_PersonalizationPerUser PerUser, #PageIndex PageIndex
               WHERE PerUser.PathId = PageIndex.ItemId
                     AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
               GROUP BY PageIndex.ItemId
              ) AS UserDataPerPath
              ON SharedDataPerPath.PathId = UserDataPerPath.PathId
             )
        WHERE Paths.PathId = SharedDataPerPath.PathId OR Paths.PathId = UserDataPerPath.PathId
        ORDER BY Paths.Path ASC
    END
    ELSE
    BEGIN
        -- Insert into our temp table
        INSERT INTO #PageIndex (ItemId)
        SELECT PerUser.Id
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths
        WHERE Paths.ApplicationId = @ApplicationId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND (@Path IS NULL OR Paths.LoweredPath LIKE LOWER(@Path))
              AND (@UserName IS NULL OR Users.LoweredUserName LIKE LOWER(@UserName))
              AND (@InactiveSinceDate IS NULL OR Users.LastActivityDate <= @InactiveSinceDate)
        ORDER BY Paths.Path ASC, Users.UserName ASC

        SELECT @TotalRecords = @@ROWCOUNT

        SELECT Paths.Path, PerUser.LastUpdatedDate, DATALENGTH(PerUser.PageSettings), Users.UserName, Users.LastActivityDate
        FROM dbo.aspnet_PersonalizationPerUser PerUser, dbo.aspnet_Users Users, dbo.aspnet_Paths Paths, #PageIndex PageIndex
        WHERE PerUser.Id = PageIndex.ItemId
              AND PerUser.UserId = Users.UserId
              AND PerUser.PathId = Paths.PathId
              AND PageIndex.IndexId >= @PageLowerBound AND PageIndex.IndexId <= @PageUpperBound
        ORDER BY Paths.Path ASC, Users.UserName ASC
    END

    RETURN @TotalRecords
END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Membership_CreateUser]    Script Date: 10/22/2008 16:06:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Membership_CreateUser]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Membership_CreateUser]
    @ApplicationName                        nvarchar(256),
    @UserName                               nvarchar(256),
    @Password                               nvarchar(128),
    @PasswordSalt                           nvarchar(128),
    @Email                                  nvarchar(256),
    @PasswordQuestion                       nvarchar(256),
    @PasswordAnswer                         nvarchar(128),
    @IsApproved                             bit,
    @CurrentTimeUtc                         datetime,
    @CreateDate                             datetime = NULL,
    @UniqueEmail                            int      = 0,
    @PasswordFormat                         int      = 0,
    @UserId                                 uniqueidentifier OUTPUT
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @NewUserId uniqueidentifier
    SELECT @NewUserId = NULL

    DECLARE @IsLockedOut bit
    SET @IsLockedOut = 0

    DECLARE @LastLockoutDate  datetime
    SET @LastLockoutDate = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAttemptCount int
    SET @FailedPasswordAttemptCount = 0

    DECLARE @FailedPasswordAttemptWindowStart  datetime
    SET @FailedPasswordAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @FailedPasswordAnswerAttemptCount int
    SET @FailedPasswordAnswerAttemptCount = 0

    DECLARE @FailedPasswordAnswerAttemptWindowStart  datetime
    SET @FailedPasswordAnswerAttemptWindowStart = CONVERT( datetime, ''17540101'', 112 )

    DECLARE @NewUserCreated bit
    DECLARE @ReturnValue   int
    SET @ReturnValue = 0

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
	    BEGIN TRANSACTION
	    SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    SET @CreateDate = @CurrentTimeUtc

    SELECT  @NewUserId = UserId FROM dbo.aspnet_Users WHERE LOWER(@UserName) = LoweredUserName AND @ApplicationId = ApplicationId
    IF ( @NewUserId IS NULL )
    BEGIN
        SET @NewUserId = @UserId
        EXEC @ReturnValue = dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, 0, @CreateDate, @NewUserId OUTPUT
        SET @NewUserCreated = 1
    END
    ELSE
    BEGIN
        SET @NewUserCreated = 0
        IF( @NewUserId <> @UserId AND @UserId IS NOT NULL )
        BEGIN
            SET @ErrorCode = 6
            GOTO Cleanup
        END
    END

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @ReturnValue = -1 )
    BEGIN
        SET @ErrorCode = 10
        GOTO Cleanup
    END

    IF ( EXISTS ( SELECT UserId
                  FROM   dbo.aspnet_Membership
                  WHERE  @NewUserId = UserId ) )
    BEGIN
        SET @ErrorCode = 6
        GOTO Cleanup
    END

    SET @UserId = @NewUserId

    IF (@UniqueEmail = 1)
    BEGIN
        IF (EXISTS (SELECT *
                    FROM  dbo.aspnet_Membership m WITH ( UPDLOCK, HOLDLOCK )
                    WHERE ApplicationId = @ApplicationId AND LoweredEmail = LOWER(@Email)))
        BEGIN
            SET @ErrorCode = 7
            GOTO Cleanup
        END
    END

    IF (@NewUserCreated = 0)
    BEGIN
        UPDATE dbo.aspnet_Users
        SET    LastActivityDate = @CreateDate
        WHERE  @UserId = UserId
        IF( @@ERROR <> 0 )
        BEGIN
            SET @ErrorCode = -1
            GOTO Cleanup
        END
    END

    INSERT INTO dbo.aspnet_Membership
                ( ApplicationId,
                  UserId,
                  Password,
                  PasswordSalt,
                  Email,
                  LoweredEmail,
                  PasswordQuestion,
                  PasswordAnswer,
                  PasswordFormat,
                  IsApproved,
                  IsLockedOut,
                  CreateDate,
                  LastLoginDate,
                  LastPasswordChangedDate,
                  LastLockoutDate,
                  FailedPasswordAttemptCount,
                  FailedPasswordAttemptWindowStart,
                  FailedPasswordAnswerAttemptCount,
                  FailedPasswordAnswerAttemptWindowStart )
         VALUES ( @ApplicationId,
                  @UserId,
                  @Password,
                  @PasswordSalt,
                  @Email,
                  LOWER(@Email),
                  @PasswordQuestion,
                  @PasswordAnswer,
                  @PasswordFormat,
                  @IsApproved,
                  @IsLockedOut,
                  @CreateDate,
                  @CreateDate,
                  @CreateDate,
                  @LastLockoutDate,
                  @FailedPasswordAttemptCount,
                  @FailedPasswordAttemptWindowStart,
                  @FailedPasswordAnswerAttemptCount,
                  @FailedPasswordAnswerAttemptWindowStart )

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
	    SET @TranStarted = 0
	    COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_SetProperties]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_SetProperties]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_SetProperties]
    @ApplicationName        nvarchar(256),
    @PropertyNames          ntext,
    @PropertyValuesString   ntext,
    @PropertyValuesBinary   image,
    @UserName               nvarchar(256),
    @IsUserAnonymous        bit,
    @CurrentTimeUtc         datetime
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
       BEGIN TRANSACTION
       SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    DECLARE @UserId uniqueidentifier
    DECLARE @LastActivityDate datetime
    SELECT  @UserId = NULL
    SELECT  @LastActivityDate = @CurrentTimeUtc

    SELECT @UserId = UserId
    FROM   dbo.aspnet_Users
    WHERE  ApplicationId = @ApplicationId AND LoweredUserName = LOWER(@UserName)
    IF (@UserId IS NULL)
        EXEC dbo.aspnet_Users_CreateUser @ApplicationId, @UserName, @IsUserAnonymous, @LastActivityDate, @UserId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    UPDATE dbo.aspnet_Users
    SET    LastActivityDate=@CurrentTimeUtc
    WHERE  UserId = @UserId

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS( SELECT *
               FROM   dbo.aspnet_Profile
               WHERE  UserId = @UserId))
        UPDATE dbo.aspnet_Profile
        SET    PropertyNames=@PropertyNames, PropertyValuesString = @PropertyValuesString,
               PropertyValuesBinary = @PropertyValuesBinary, LastUpdatedDate=@CurrentTimeUtc
        WHERE  UserId = @UserId
    ELSE
        INSERT INTO dbo.aspnet_Profile(UserId, PropertyNames, PropertyValuesString, PropertyValuesBinary, LastUpdatedDate)
             VALUES (@UserId, @PropertyNames, @PropertyValuesString, @PropertyValuesBinary, @CurrentTimeUtc)

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END

    RETURN 0

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Roles_CreateRole]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Roles_CreateRole]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'CREATE PROCEDURE [dbo].[aspnet_Roles_CreateRole]
    @ApplicationName  nvarchar(256),
    @RoleName         nvarchar(256)
AS
BEGIN
    DECLARE @ApplicationId uniqueidentifier
    SELECT  @ApplicationId = NULL

    DECLARE @ErrorCode     int
    SET @ErrorCode = 0

    DECLARE @TranStarted   bit
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
        SET @TranStarted = 0

    EXEC dbo.aspnet_Applications_CreateApplication @ApplicationName, @ApplicationId OUTPUT

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF (EXISTS(SELECT RoleId FROM dbo.aspnet_Roles WHERE LoweredRoleName = LOWER(@RoleName) AND ApplicationId = @ApplicationId))
    BEGIN
        SET @ErrorCode = 1
        GOTO Cleanup
    END

    INSERT INTO dbo.aspnet_Roles
                (ApplicationId, RoleName, LoweredRoleName)
         VALUES (@ApplicationId, @RoleName, LOWER(@RoleName))

    IF( @@ERROR <> 0 )
    BEGIN
        SET @ErrorCode = -1
        GOTO Cleanup
    END

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        COMMIT TRANSACTION
    END

    RETURN(0)

Cleanup:

    IF( @TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
        ROLLBACK TRANSACTION
    END

    RETURN @ErrorCode

END' 
END
GO
/****** Object:  StoredProcedure [dbo].[aspnet_Profile_DeleteProfiles]    Script Date: 10/22/2008 16:06:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[aspnet_Profile_DeleteProfiles]') AND type in (N'P', N'PC'))
BEGIN
EXEC dbo.sp_executesql @statement = N'
CREATE PROCEDURE [dbo].[aspnet_Profile_DeleteProfiles]
    @ApplicationName        nvarchar(256),
    @UserNames              nvarchar(4000)
AS
BEGIN
    DECLARE @UserName     nvarchar(256)
    DECLARE @CurrentPos   int
    DECLARE @NextPos      int
    DECLARE @NumDeleted   int
    DECLARE @DeletedUser  int
    DECLARE @TranStarted  bit
    DECLARE @ErrorCode    int

    SET @ErrorCode = 0
    SET @CurrentPos = 1
    SET @NumDeleted = 0
    SET @TranStarted = 0

    IF( @@TRANCOUNT = 0 )
    BEGIN
        BEGIN TRANSACTION
        SET @TranStarted = 1
    END
    ELSE
    	SET @TranStarted = 0

    WHILE (@CurrentPos <= LEN(@UserNames))
    BEGIN
        SELECT @NextPos = CHARINDEX(N'','', @UserNames,  @CurrentPos)
        IF (@NextPos = 0 OR @NextPos IS NULL)
            SELECT @NextPos = LEN(@UserNames) + 1

        SELECT @UserName = SUBSTRING(@UserNames, @CurrentPos, @NextPos - @CurrentPos)
        SELECT @CurrentPos = @NextPos+1

        IF (LEN(@UserName) > 0)
        BEGIN
            SELECT @DeletedUser = 0
            EXEC dbo.aspnet_Users_DeleteUser @ApplicationName, @UserName, 4, @DeletedUser OUTPUT
            IF( @@ERROR <> 0 )
            BEGIN
                SET @ErrorCode = -1
                GOTO Cleanup
            END
            IF (@DeletedUser <> 0)
                SELECT @NumDeleted = @NumDeleted + 1
        END
    END
    SELECT @NumDeleted
    IF (@TranStarted = 1)
    BEGIN
    	SET @TranStarted = 0
    	COMMIT TRANSACTION
    END
    SET @TranStarted = 0

    RETURN 0

Cleanup:
    IF (@TranStarted = 1 )
    BEGIN
        SET @TranStarted = 0
    	ROLLBACK TRANSACTION
    END
    RETURN @ErrorCode
END' 
END
GO
/****** Object:  Default [DF_Account_BillingMethod]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_BillingMethod]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_BillingMethod]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_BillingMethod]  DEFAULT ((1)) FOR [BillingMethod]
END


End
GO
/****** Object:  Default [DF_Account_EmailBillDataFile]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_EmailBillDataFile]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_EmailBillDataFile]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_EmailBillDataFile]  DEFAULT ((0)) FOR [EmailBillDataFile]
END
End
GO

/****** Object:  Default [DF_VizadaData_Imported]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_VizadaData_Imported]') AND parent_object_id = OBJECT_ID(N'[dbo].[VizadaData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_VizadaData_Imported]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[VizadaData] ADD  CONSTRAINT [DF_VizadaData_Imported]  DEFAULT ((0)) FOR [Imported]
END
End
GO

/****** Object:  Default [DF_SatComData_Imported]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_SatComData_Imported]') AND parent_object_id = OBJECT_ID(N'[dbo].[SatComData]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_SatComData_Imported]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[SatComData] ADD  CONSTRAINT [DF_SatComData_Imported]  DEFAULT ((0)) FOR [Imported]
END
End
GO

/****** Object:  Default [DF_Account_EmailBill]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_EmailBill]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_EmailBill]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_EmailBill]  DEFAULT ((0)) FOR [EmailBill]
END


End
GO
/****** Object:  Default [DF_Account_PostBill]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_PostBill]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_PostBill]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_PostBill]  DEFAULT ((0)) FOR [PostBill]
END


End
GO
/****** Object:  Default [DF_Account_BillingAddressType]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_BillingAddressType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_BillingAddressType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_BillingAddressType]  DEFAULT ((1)) FOR [BillingAddressType]
END


End
GO
/****** Object:  Default [DF_Account_IsInvoiceRoot]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_IsInvoiceRoot]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_IsInvoiceRoot]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_IsInvoiceRoot]  DEFAULT ((1)) FOR [IsInvoiceRoot]
END


End
GO
/****** Object:  Default [DF_Account_HasRequestedInvoicing]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Account_HasRequestedInvoicing]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Account_HasRequestedInvoicing]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_HasRequestedInvoicing]  DEFAULT ((1)) FOR [HasRequestedInvoicing]
END

End
GO
/****** Object:  Default [DF_Agent_CommissionPercent]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Agent_CommissionPercent]') AND parent_object_id = OBJECT_ID(N'[dbo].[Agent]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Agent_CommissionPercent]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Agent] ADD  CONSTRAINT [DF_Agent_CommissionPercent]  DEFAULT ((0)) FOR [CommissionPercent]
END


End
GO
/****** Object:  Default [DF__aspnet_Ap__Appli__5046D714]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ap__Appli__5046D714]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Applications]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ap__Appli__5046D714]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Applications] ADD  CONSTRAINT [DF__aspnet_Ap__Appli__5046D714]  DEFAULT (newid()) FOR [ApplicationId]
END


End
GO
/****** Object:  Default [DF__aspnet_Me__Passw__6541F3FA]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Me__Passw__6541F3FA]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Me__Passw__6541F3FA]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Membership] ADD  CONSTRAINT [DF__aspnet_Me__Passw__6541F3FA]  DEFAULT ((0)) FOR [PasswordFormat]
END


End
GO
/****** Object:  Default [DF__aspnet_Pa__PathI__15E52B55]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Pa__PathI__15E52B55]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Pa__PathI__15E52B55]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Paths] ADD  CONSTRAINT [DF__aspnet_Pa__PathI__15E52B55]  DEFAULT (newid()) FOR [PathId]
END


End
GO
/****** Object:  Default [DF__aspnet_Perso__Id__1D864D1D]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Perso__Id__1D864D1D]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Perso__Id__1D864D1D]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] ADD  CONSTRAINT [DF__aspnet_Perso__Id__1D864D1D]  DEFAULT (newid()) FOR [Id]
END


End
GO
/****** Object:  Default [DF__aspnet_Ro__RoleI__02D256E1]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Ro__RoleI__02D256E1]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Ro__RoleI__02D256E1]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Roles] ADD  CONSTRAINT [DF__aspnet_Ro__RoleI__02D256E1]  DEFAULT (newid()) FOR [RoleId]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__UserI__541767F8]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__UserI__541767F8]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__UserI__541767F8]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__UserI__541767F8]  DEFAULT (newid()) FOR [UserId]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__Mobil__550B8C31]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__Mobil__550B8C31]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__Mobil__550B8C31]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__Mobil__550B8C31]  DEFAULT (NULL) FOR [MobileAlias]
END


End
GO
/****** Object:  Default [DF__aspnet_Us__IsAno__55FFB06A]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF__aspnet_Us__IsAno__55FFB06A]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__aspnet_Us__IsAno__55FFB06A]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[aspnet_Users] ADD  CONSTRAINT [DF__aspnet_Us__IsAno__55FFB06A]  DEFAULT ((0)) FOR [IsAnonymous]
END


End
GO
/****** Object:  Default [DF_Contact_CreditCardType]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contact_CreditCardType]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contact]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contact_CreditCardType]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contact] ADD  CONSTRAINT [DF_Contact_CreditCardType]  DEFAULT ((0)) FOR [CreditCardType]
END


End
GO
/****** Object:  Default [DF_Contract_ContractStatus]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contract_ContractStatus]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contract_ContractStatus]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contract] ADD  CONSTRAINT [DF_Contract_ContractStatus]  DEFAULT ((1)) FOR [ContractStatus]
END


End
GO
/****** Object:  Default [DF_Contract_Data]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contract_Data]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contract_Data]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contract] ADD  CONSTRAINT [DF_Contract_Data]  DEFAULT ((0)) FOR [Data]
END


End
GO
/****** Object:  Default [DF_Contract_MessageBank]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Contract_MessageBank]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Contract_MessageBank]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Contract] ADD  CONSTRAINT [DF_Contract_MessageBank]  DEFAULT ((0)) FOR [MessageBank]
END


End
GO
/****** Object:  Default [DF_ContractPlanVariance_Cost]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_ContractPlanVariance_Cost]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_ContractPlanVariance_Cost]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[ContractPlanVariance] ADD  CONSTRAINT [DF_ContractPlanVariance_Cost]  DEFAULT ((0)) FOR [Cost]
END


End
GO
/****** Object:  Default [DF_NetworkTariff_IsCountedInFreeCall]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_NetworkTariff_IsCountedInFreeCall]') AND parent_object_id = OBJECT_ID(N'[dbo].[NetworkTariff]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_NetworkTariff_IsCountedInFreeCall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NetworkTariff] ADD  CONSTRAINT [DF_NetworkTariff_IsCountedInFreeCall]  DEFAULT ((1)) FOR [IsCountedInFreeCall]
END


End
GO
/****** Object:  Default [DF_NetworkTariff_HasFlagfall]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_NetworkTariff_HasFlagfall]') AND parent_object_id = OBJECT_ID(N'[dbo].[NetworkTariff]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_NetworkTariff_HasFlagfall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[NetworkTariff] ADD  CONSTRAINT [DF_NetworkTariff_HasFlagfall]  DEFAULT ((1)) FOR [HasFlagfall]
END


End
GO
/****** Object:  Default [DF_PLan_Duration]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Plan_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Plan_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_Plan_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO
/****** Object:  Default [DF_PLan_Duration]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_Duration]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_Duration]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_PLan_Duration]  DEFAULT ((1)) FOR [Duration]
END


End
GO
/****** Object:  Default [DF_PLan_Flagfall]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_Flagfall]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_Flagfall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_PLan_Flagfall]  DEFAULT ((0)) FOR [Flagfall]
END


End
GO
/****** Object:  Default [DF_PLan_FreeCallAmount]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_FreeCallAmount]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_FreeCallAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_PLan_FreeCallAmount]  DEFAULT ((0)) FOR [FreeCallAmount]
END


End
GO
/****** Object:  Default [DF_PLan_PlanAmount]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_PLan_PlanAmount]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_PLan_PlanAmount]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Plan] ADD  CONSTRAINT [DF_PLan_PlanAmount]  DEFAULT ((0)) FOR [PlanAmount]
END


End
GO
/****** Object:  Default [DF_Network_IsActive]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Network_IsActive]') AND parent_object_id = OBJECT_ID(N'[dbo].[Network]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Network_IsActive]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Network] ADD  CONSTRAINT [DF_Network_IsActive]  DEFAULT ((1)) FOR [IsActive]
END


End
GO

/****** Object:  Default [DF_Tariff_IsCountedInFreeCall]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Tariff_IsCountedInFreeCall]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Tariff_IsCountedInFreeCall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Tariff] ADD  CONSTRAINT [DF_Tariff_IsCountedInFreeCall]  DEFAULT ((1)) FOR [IsCountedInFreeCall]
END


End
GO
/****** Object:  Default [DF_Tariff_HasFlagfall]    Script Date: 10/22/2008 16:06:20 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[dbo].[DF_Tariff_HasFlagfall]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Tariff_HasFlagfall]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Tariff] ADD  CONSTRAINT [DF_Tariff_HasFlagfall]  DEFAULT ((1)) FOR [HasFlagfall]
END


End
GO
/****** Object:  ForeignKey [FK_Account_CompanyId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_CompanyId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_CompanyId] FOREIGN KEY([CompanyId])
REFERENCES [dbo].[Company] ([CompanyId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_CompanyId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_CompanyId]
GO
/****** Object:  ForeignKey [FK_Account_ContactId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_ContactId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_ContactId] FOREIGN KEY([ContactId])
REFERENCES [dbo].[Contact] ([ContactId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_ContactId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_ContactId]
GO
/****** Object:  ForeignKey [FK_Account_ParentAccountId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_ParentAccountId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_ParentAccountId] FOREIGN KEY([ParentAccountId])
REFERENCES [dbo].[Account] ([AccountId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Account_ParentAccountId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Account]'))
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_ParentAccountId]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__Appli__6359AB88]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__6359AB88]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Me__Appli__6359AB88] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__Appli__6359AB88]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__Appli__6359AB88]
GO
/****** Object:  ForeignKey [FK__aspnet_Me__UserI__644DCFC1]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__644DCFC1]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Me__UserI__644DCFC1] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Me__UserI__644DCFC1]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Membership]'))
ALTER TABLE [dbo].[aspnet_Membership] CHECK CONSTRAINT [FK__aspnet_Me__UserI__644DCFC1]
GO
/****** Object:  ForeignKey [FK__aspnet_Pa__Appli__14F1071C]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__14F1071C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pa__Appli__14F1071C] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pa__Appli__14F1071C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Paths]'))
ALTER TABLE [dbo].[aspnet_Paths] CHECK CONSTRAINT [FK__aspnet_Pa__Appli__14F1071C]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__1AA9E072]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__1AA9E072]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__PathI__1AA9E072] FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__1AA9E072]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationAllUsers]'))
ALTER TABLE [dbo].[aspnet_PersonalizationAllUsers] CHECK CONSTRAINT [FK__aspnet_Pe__PathI__1AA9E072]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__PathI__1E7A7156]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__1E7A7156]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__PathI__1E7A7156] FOREIGN KEY([PathId])
REFERENCES [dbo].[aspnet_Paths] ([PathId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__PathI__1E7A7156]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] CHECK CONSTRAINT [FK__aspnet_Pe__PathI__1E7A7156]
GO
/****** Object:  ForeignKey [FK__aspnet_Pe__UserI__1F6E958F]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__1F6E958F]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pe__UserI__1F6E958F] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pe__UserI__1F6E958F]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_PersonalizationPerUser]'))
ALTER TABLE [dbo].[aspnet_PersonalizationPerUser] CHECK CONSTRAINT [FK__aspnet_Pe__UserI__1F6E958F]
GO
/****** Object:  ForeignKey [FK__aspnet_Pr__UserI__7854C86E]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__7854C86E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Pr__UserI__7854C86E] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Pr__UserI__7854C86E]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Profile]'))
ALTER TABLE [dbo].[aspnet_Profile] CHECK CONSTRAINT [FK__aspnet_Pr__UserI__7854C86E]
GO
/****** Object:  ForeignKey [FK__aspnet_Ro__Appli__01DE32A8]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__01DE32A8]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Ro__Appli__01DE32A8] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Ro__Appli__01DE32A8]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Roles]'))
ALTER TABLE [dbo].[aspnet_Roles] CHECK CONSTRAINT [FK__aspnet_Ro__Appli__01DE32A8]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__Appli__532343BF]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__532343BF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__Appli__532343BF] FOREIGN KEY([ApplicationId])
REFERENCES [dbo].[aspnet_Applications] ([ApplicationId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__Appli__532343BF]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_Users]'))
ALTER TABLE [dbo].[aspnet_Users] CHECK CONSTRAINT [FK__aspnet_Us__Appli__532343BF]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__RoleI__06A2E7C5]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__06A2E7C5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__RoleI__06A2E7C5] FOREIGN KEY([RoleId])
REFERENCES [dbo].[aspnet_Roles] ([RoleId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__RoleI__06A2E7C5]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] CHECK CONSTRAINT [FK__aspnet_Us__RoleI__06A2E7C5]
GO
/****** Object:  ForeignKey [FK__aspnet_Us__UserI__05AEC38C]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__05AEC38C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK__aspnet_Us__UserI__05AEC38C] FOREIGN KEY([UserId])
REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__aspnet_Us__UserI__05AEC38C]') AND parent_object_id = OBJECT_ID(N'[dbo].[aspnet_UsersInRoles]'))
ALTER TABLE [dbo].[aspnet_UsersInRoles] CHECK CONSTRAINT [FK__aspnet_Us__UserI__05AEC38C]
GO
/****** Object:  ForeignKey [FK_Call_ContractId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Call_ContractId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Call]'))
ALTER TABLE [dbo].[Call]  WITH CHECK ADD  CONSTRAINT [FK_Call_ContractId] FOREIGN KEY([ContractId])
REFERENCES [dbo].[Contract] ([ContractId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Call_ContractId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Call]'))
ALTER TABLE [dbo].[Call] CHECK CONSTRAINT [FK_Call_ContractId]
GO
/****** Object:  ForeignKey [FK_Call_TariffId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Call_TariffId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Call]'))
ALTER TABLE [dbo].[Call]  WITH CHECK ADD  CONSTRAINT [FK_Call_TariffId] FOREIGN KEY([TariffId])
REFERENCES [dbo].[Tariff] ([TariffId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Call_TariffId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Call]'))
ALTER TABLE [dbo].[Call] CHECK CONSTRAINT [FK_Call_TariffId]
GO
/****** Object:  ForeignKey [FK_Contract_AccountId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_AccountId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_AccountId] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([AccountId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_AccountId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_AccountId]
GO
/****** Object:  ForeignKey [FK_Contract_AgentId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_AgentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_AgentId] FOREIGN KEY([AgentId])
REFERENCES [dbo].[Agent] ([AgentId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_AgentId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_AgentId]
GO
/****** Object:  ForeignKey [FK_Contract_PlanId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract]  WITH CHECK ADD  CONSTRAINT [FK_Contract_PlanId] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([PlanId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Contract_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Contract]'))
ALTER TABLE [dbo].[Contract] CHECK CONSTRAINT [FK_Contract_PlanId]
GO
/****** Object:  ForeignKey [FK_ContractPlanVariance_ContractId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContractPlanVariance_ContractId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
ALTER TABLE [dbo].[ContractPlanVariance]  WITH CHECK ADD  CONSTRAINT [FK_ContractPlanVariance_ContractId] FOREIGN KEY([ContractId])
REFERENCES [dbo].[Contract] ([ContractId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContractPlanVariance_ContractId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
ALTER TABLE [dbo].[ContractPlanVariance] CHECK CONSTRAINT [FK_ContractPlanVariance_ContractId]
GO
/****** Object:  ForeignKey [FK_ContractPlanVariance_PlanId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContractPlanVariance_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
ALTER TABLE [dbo].[ContractPlanVariance]  WITH CHECK ADD  CONSTRAINT [FK_ContractPlanVariance_PlanId] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([PlanId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_ContractPlanVariance_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[ContractPlanVariance]'))
ALTER TABLE [dbo].[ContractPlanVariance] CHECK CONSTRAINT [FK_ContractPlanVariance_PlanId]
GO
/****** Object:  ForeignKey [FK_Network_AirtimeProvider]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Network_AirtimeProviderId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Network]'))
ALTER TABLE [dbo].[Network]  WITH CHECK ADD  CONSTRAINT [FK_Network_AirtimeProviderId] FOREIGN KEY([AirtimeProviderId])
REFERENCES [dbo].[AirtimeProvider] ([AirtimeProviderId])
GO

/****** Object:  ForeignKey [FK_NetworkTariff_NetworkId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NetworkTariff_NetworkId]') AND parent_object_id = OBJECT_ID(N'[dbo].[NetworkTariff]'))
ALTER TABLE [dbo].[NetworkTariff]  WITH CHECK ADD  CONSTRAINT [FK_NetworkTariff_NetworkId] FOREIGN KEY([NetworkId])
REFERENCES [dbo].[Network] ([NetworkId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_NetworkTariff_NetworkId]') AND parent_object_id = OBJECT_ID(N'[dbo].[NetworkTariff]'))
ALTER TABLE [dbo].[NetworkTariff] CHECK CONSTRAINT [FK_NetworkTariff_NetworkId]
GO
/****** Object:  ForeignKey [FK_Plan_NetworkId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Plan_NetworkId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
ALTER TABLE [dbo].[Plan]  WITH CHECK ADD  CONSTRAINT [FK_Plan_NetworkId] FOREIGN KEY([NetworkId])
REFERENCES [dbo].[Network] ([NetworkId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Plan_NetworkId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Plan]'))
ALTER TABLE [dbo].[Plan] CHECK CONSTRAINT [FK_Plan_NetworkId]
GO
/****** Object:  ForeignKey [FK_Tariff_NetworkTariffId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tariff_NetworkTariffId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
ALTER TABLE [dbo].[Tariff]  WITH CHECK ADD  CONSTRAINT [FK_Tariff_NetworkTariffId] FOREIGN KEY([NetworkTariffId])
REFERENCES [dbo].[NetworkTariff] ([NetworkTariffId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tariff_NetworkTariffId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
ALTER TABLE [dbo].[Tariff] CHECK CONSTRAINT [FK_Tariff_NetworkTariffId]
GO
/****** Object:  ForeignKey [FK_Tariff_PlanId]    Script Date: 10/22/2008 16:06:20 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tariff_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
ALTER TABLE [dbo].[Tariff]  WITH CHECK ADD  CONSTRAINT [FK_Tariff_PlanId] FOREIGN KEY([PlanId])
REFERENCES [dbo].[Plan] ([PlanId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Tariff_PlanId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Tariff]'))
ALTER TABLE [dbo].[Tariff] CHECK CONSTRAINT [FK_Tariff_PlanId]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_CheckSchemaVersion] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_ChangePasswordQuestionAndAnswer] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_CreateUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_FindUsersByEmail] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_FindUsersByName] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetAllUsers] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetNumberOfUsersOnline] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetPassword] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetPasswordWithFormat] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByEmail] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByName] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_GetUserByUserId] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_ResetPassword] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_SetPassword] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UnlockUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UpdateUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Membership_UpdateUserInfo] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Paths_CreatePath] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Personalization_GetApplicationId] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_DeleteAllState] TO [aspnet_Personalization_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_FindState] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_GetCountOfState] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_ResetSharedState] TO [aspnet_Personalization_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAdministration_ResetUserState] TO [aspnet_Personalization_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAllUsers_GetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAllUsers_ResetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationAllUsers_SetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationPerUser_GetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationPerUser_ResetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_PersonalizationPerUser_SetPageSettings] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_DeleteInactiveProfiles] TO [aspnet_Profile_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_DeleteProfiles] TO [aspnet_Profile_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_GetNumberOfInactiveProfiles] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_GetProfiles] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_GetProperties] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Profile_SetProperties] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_RegisterSchemaVersion] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_CreateRole] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_DeleteRole] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_GetAllRoles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Roles_RoleExists] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Personalization_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Profile_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UnRegisterSchemaVersion] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_Users_DeleteUser] TO [aspnet_Membership_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_AddUsersToRoles] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_FindUsersInRole] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_GetRolesForUser] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_GetRolesForUser] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_GetUsersInRoles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_BasicAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_IsUserInRole] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_UsersInRoles_RemoveUsersFromRoles] TO [aspnet_Roles_FullAccess] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[aspnet_WebEvent_LogEvent] TO [aspnet_WebEvent_FullAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Applications] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_MembershipUsers] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Profiles] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Roles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Membership_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Profile_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_Users] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_UsersInRoles] TO [aspnet_Roles_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_WebPartState_Paths] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_WebPartState_Shared] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
GRANT SELECT ON [dbo].[vw_aspnet_WebPartState_User] TO [aspnet_Personalization_ReportingAccess] AS [dbo]
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_BasicAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Membership_BasicAccess', @membername=N'aspnet_Membership_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Membership_ReportingAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Membership_ReportingAccess', @membername=N'aspnet_Membership_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_BasicAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Personalization_BasicAccess', @membername=N'aspnet_Personalization_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Personalization_ReportingAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Personalization_ReportingAccess', @membername=N'aspnet_Personalization_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_BasicAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Profile_BasicAccess', @membername=N'aspnet_Profile_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Profile_ReportingAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Profile_ReportingAccess', @membername=N'aspnet_Profile_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_BasicAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Roles_BasicAccess', @membername=N'aspnet_Roles_FullAccess'

END
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE name = N'aspnet_Roles_ReportingAccess')
BEGIN
EXEC sys.sp_addrolemember @rolename=N'aspnet_Roles_ReportingAccess', @membername=N'aspnet_Roles_FullAccess'

END
GO

/* INSERT ADMIN USER */
INSERT INTO [dbo].[aspnet_applications] (ApplicationName, LoweredApplicationName, ApplicationId, Description)
VALUES ('Airtime Billing', 'Airtime Billing', '4F20F1EF-2806-41BF-8778-58CC48D84961', NULL)
GO

INSERT INTO [dbo].[aspnet_users] (ApplicationId, UserId, UserName, LoweredUserName, MobileAlias, IsAnonymous, LastActivityDate)
VALUES ('4F20F1EF-2806-41BF-8778-58CC48D84961', 'AD6FFCA3-1775-49DF-93A2-03EB6C880595', 'admin', 'admin', NULL, 0, getdate())
GO

INSERT INTO [dbo].[aspnet_membership] (ApplicationId, UserId, Password, PasswordFormat, PasswordSalt, MobilePIN, Email, LoweredEmail, PasswordQuestion, PasswordAnswer, IsApproved, IsLockedOut, CreateDate, LastLoginDate, LastPasswordChangedDate, LastLockoutDate, FailedPasswordAttemptCount, FailedPasswordAttemptWindowStart, FailedPasswordAnswerAttemptCount, FailedPasswordAnswerAttemptWindowStart,Comment)
VALUES ('4F20F1EF-2806-41BF-8778-58CC48D84961','AD6FFCA3-1775-49DF-93A2-03EB6C880595','3QQTBbeYejtilpdfo5ikRulpzhQ=',1,'diTR8q5R2x2+0QWzJbNSbQ==',NULL,'admin@ragware.net','admin@ragware.net','a','x1c9yalo7FE7NF9eBXIlIY5jyoI=',1,0,getdate(),getdate(),getdate(),'1754-01-01',0,'1754-01-01',0,'1754-01-01',NULL)
GO

INSERT INTO [dbo].[UserProfile] (UserId, AgentId, ContactId, LastUpdatedDate) 
VALUES ('AD6FFCA3-1775-49DF-93A2-03EB6C880595', 0, 0, getdate())
GO

SET IDENTITY_INSERT dbo.AirtimeProvider ON
INSERT INTO dbo.AirtimeProvider (AirtimeProviderId, Name, ImporterFullName) VALUES( 1, 'Vizada', 'AirtimeBilling.Import.VizadaCallDataImporter')
INSERT INTO dbo.AirtimeProvider (AirtimeProviderId, Name, ImporterFullName) VALUES( 2, 'SatCom', 'AirtimeBilling.Import.SatComCallDataImporter')
SET IDENTITY_INSERT dbo.AirtimeProvider OFF

SET IDENTITY_INSERT dbo.Network ON
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 1, 'Iridium', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 2, 'Thuraya', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 3, 'Inmasat', 1, 1)
SET IDENTITY_INSERT dbo.Network OFF

SET IDENTITY_INSERT dbo.NetworkTariff ON
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 1, 'I0', 'Iridium Free of Charge', 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 2, 'I1', 'Iridium Voice', 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 3, 'I2', 'Iridium Data 4.8', 0, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 4, 'I3', 'Iridium Data 9.6', 0, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 5, 'I4', 'Iridium Forwarding', 0, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 6, 'I6', 'Iridium Satellite Direct', 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 7, 'I6', 'Iridium SMS', 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 8, 'IA', 'Iridium Incoming (To Mobile)', 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 9, 'P0', 'Inmarsat SPS Voice', 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 10, 'P1', 'Inmarsat SPS SMS', 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 11, 'P2', 'Inmarsat SPS Fax 2.4', 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 12, 'P3', 'Inmarsat SPS Data 2.4', 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 13, 'T1', 'Thuraya Voice', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 14, 'T2', 'Thuraya GSM Outbound', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 15, 'T3', 'Thuraya GSM Inbound', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 16, 'T4', 'Thuraya Forward', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 17, 'T5', 'Thuraya SMS', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 18, 'T6', 'Thuraya GMS SMS', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 19, 'T7', 'ThurayaDSL Light', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 20, 'T8', 'ThurayaDSL Plenty', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 21, 'T9', 'ThurayaDSL Extra', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 22, 'TA', 'ThurayaDSL Unlimited', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 23, 'TB', 'Thuraya Fax', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 24, 'TC', 'Thuraya Data', 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, NetworkId) VALUES( 25, 'MM-1000', 'Mini-M Voice', 1, 3)
SET IDENTITY_INSERT dbo.NetworkTariff OFF

SET IDENTITY_INSERT dbo.SystemSettings ON
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (1, 'NextInvoiceNumber', '1', 1);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (2, 'NextContractNumber', '1', 1);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (3, 'NextAccountNumber', '1', 1);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (4, 'LastInvoicedDate', '2008-08-01', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (5, 'NextInvoiceRunNumber', '1', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (6, 'EnvironmentalLevy', '2.50', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (7, 'ContractSuspensionFee', '10.00', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (8, 'ContractNumberFormat', '00000', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (9, 'ContractNumberPrefix', 'CT', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (10, 'AccountNumberFormat', '00000', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (11, 'AccountNumberPrefix', 'LW', 0);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (12, 'MatchCallsBeforeInvoicing', 'False', 1);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (13, 'PrintClosedContractLetter', 'False', 1);
INSERT INTO SystemSettings (SystemSettingId, KeyName, KeyValue, IsSystem) VALUES (14, 'PrintSuspendedContractLetter', 'False', 1);

SET IDENTITY_INSERT dbo.SystemSettings OFF
GO

-- ADD EwayImport Table



CREATE VIEW [dbo].[InvoiceCallDetailView]
AS
SELECT	header.InvoiceHeaderId,
		line.InvoiceLineItemId,
		line.CallId,
		line.ContractId,
		CASE WHEN line.CallId IS NULL THEN line.[Description] ELSE NetworkTariff.Tariff END 'CallType',
		InvoiceNumber, 
		InvoiceDate,
		[To],
		Street,
		Suburb,
		Postcode,
		[State], 
		AmountPaid, 
		FromDate,
		ToDate, 
		[Description] 'LineDescription',
		Quantity,
		GstAmount,
		LineAmount,
		lineContract.PhoneNumber1,
		c.CalledNumber,
		c.CallTime,		
		c.UnitsOfTime,
		c.VolumeUnit,
		c.Volume,
		p.UnitOfTime,
		CASE WHEN line.CallId IS NOT NULL THEN 1 ELSE 0 END 'IsCall',
		CASE WHEN line.CallId IS NULL AND LineAmount < 0 THEN 1 ELSE 0 END 'IsCreditAmount',
		net.Name 'NetworkName'
FROM InvoiceHeader header INNER JOIN 
	InvoiceLineItem line ON header.InvoiceHeaderId=line.InvoiceHeaderId LEFT JOIN 
	[Contract] lineContract ON line.ContractId=lineContract.ContractId LEFT JOIN 
	[Call] c ON line.CallId=c.CallId LEFT JOIN 
	[Contract] cont ON c.ContractId=cont.ContractId LEFT JOIN 
	Tariff ON c.TariffId=Tariff.TariffId LEFT JOIN 
	NetworkTariff ON Tariff.NetworkTariffId=NetworkTariff.NetworkTariffId LEFT JOIN 
	[Plan] p ON cont.PlanId=p.PlanId LEFT JOIN 
	Network net ON NetworkTariff.NetworkId=net.NetworkId
WHERE line.ContractId IS NOT NULL
GO

CREATE View InvoiceServicesSummaryView
AS
SELECT	InvoiceNumber,		
		CASE WHEN line.CallId IS NOT NULL THEN net.Name ELSE CASE WHEN line.CallId IS NULL THEN line.[Description] ELSE NetworkTariff.Tariff END END 'SummaryDescription',
		CASE WHEN line.CallId IS NOT NULL THEN 1 ELSE 0 END 'IsCall',
		CASE WHEN line.CallId IS NULL AND LineAmount < 0 THEN 1 ELSE 0 END 'IsCreditAmount',	 
		COUNT(DISTINCT CASE WHEN line.CallId IS NOT NULL THEN line.ContractId ELSE NULL END) 'Services',  
		SUM(LineAmount) 'Amount'		
FROM InvoiceHeader header INNER JOIN 
	InvoiceLineItem line ON header.InvoiceHeaderId=line.InvoiceHeaderId LEFT JOIN 
	[Contract] lineContract ON line.ContractId=lineContract.ContractId LEFT JOIN 
	[Call] c ON line.CallId=c.CallId LEFT JOIN 
	[Contract] cont ON c.ContractId=cont.ContractId LEFT JOIN 
	Tariff ON c.TariffId=Tariff.TariffId LEFT JOIN 
	NetworkTariff ON Tariff.NetworkTariffId=NetworkTariff.NetworkTariffId LEFT JOIN 
	[Plan] p ON cont.PlanId=p.PlanId LEFT JOIN 
	Network net ON NetworkTariff.NetworkId=net.NetworkId
GROUP BY InvoiceNumber, CASE WHEN line.CallId IS NOT NULL THEN net.Name ELSE CASE WHEN line.CallId IS NULL THEN line.[Description] ELSE NetworkTariff.Tariff END END, CASE WHEN line.CallId IS NOT NULL THEN 1 ELSE 0 END, CASE WHEN line.CallId IS NULL AND LineAmount < 0 THEN 1 ELSE 0 END
GO

CREATE View InvoiceSummaryView
AS
SELECT	header.InvoiceHeaderId,
		cont.Name 'Contact',
		CompanyName,
		header.Street,
		header.Suburb,
		header.State,
		header.Postcode,		
		header.AmountPaid, 
		header.PreviousBill,
		header.Outstanding,
		header.NewCharges,
		header.CurrentBill,
		header.PaymentDue,
		header.FromDate,
		header.ToDate,
		header.TotalIncGst,
		header.TotalGst,
		header.TotalExGst,
		AccountNumber		
FROM InvoiceHeader header INNER JOIN 
	InvoiceLineItem line ON header.InvoiceHeaderId=line.InvoiceHeaderId INNER JOIN
	AccountInvoice ainv ON header.InvoiceHeaderId=ainv.InvoiceHeaderId INNER JOIN 
	Account acc ON ainv.AccountId=acc.AccountId INNER JOIN 
	Contact cont ON acc.ContactId=cont.ContactId LEFT JOIN 
	Company comp ON acc.CompanyId=comp.CompanyId
WHERE acc.IsInvoiceRoot=1	
GROUP BY header.InvoiceHeaderId, cont.Name, comp.CompanyName, header.Street, header.Suburb, header.State, header.Postcode, 
		header.AmountPaid, header.PreviousBill, header.Outstanding, header.NewCharges, header.CurrentBill, header.PaymentDue, FromDate, ToDate, 
		header.TotalIncGst, header.TotalGst, header.TotalExGst, AccountNumber
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE VIEW AirtimeContractView
AS
SELECT	Account.AccountNumber, 
	Account.AccountName, 
	Company.CompanyName, 
	Company.ABN, 
	Contact.Name 'ContactName', 
	Contact.MobilePhone, 
	Contact.WorkPhone, 
	Contact.FaxNumber, 
	Contact.Email, 
	Contact.Street, 
	Contact.Suburb, 
	Contact.[State], 
	Contact.Postcode, 
	Contact.DriversLicenseNumber, 
	Contact.DriversLicenseDOB, 
	Contact.DriversLicenseExpiry, 
	Contact.CreditCardNumber, 
	Contact.CreditCardExpiry, 
	[Contract].EndDate, 
	[Plan].Name 'PlanName', 
	Contact.CreditCardName, 
	Contract.ContractNumber, 
	parentcompany.CompanyName 'ParentCompanyName', 
	parentcompany.ABN 'ParentCompanyABN', 
	Contract.IMEINumber, 
	Contract.SimCard, 
	Contact.CreditCardType, 
	Account.BillingMethod, 
	Contract.ContractId, 
	Contract.ActivationDate, 	
	Account.HasRequestedInvoicing,
	Account.Password
FROM   Contract Contract 
	INNER JOIN Account ON [Contract].AccountId=Account.AccountId
	INNER JOIN [Plan] ON [Contract].PlanId=[Plan].PlanId
	LEFT OUTER JOIN Company ON Account.CompanyId=Company.CompanyId
	LEFT OUTER JOIN Account parentaccount ON Account.ParentAccountId=parentaccount.AccountId
	INNER JOIN Contact ON Account.ContactId=Contact.ContactId
	LEFT OUTER JOIN Company parentcompany ON parentaccount.CompanyId=parentcompany.CompanyId
GO

DELETE FROM SchemaInfo
INSERT INTO SchemaInfo VALUES (1)
INSERT INTO SchemaInfo VALUES (2)
INSERT INTO SchemaInfo VALUES (3)
INSERT INTO SchemaInfo VALUES (4)
INSERT INTO SchemaInfo VALUES (5)
INSERT INTO SchemaInfo VALUES (6)
GO