﻿IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'uspDeleteContract')
	BEGIN
		DROP  Procedure  uspDeleteContract
	END

GO

IF EXISTS (SELECT * FROM sysobjects WHERE type = 'P' AND name = 'uspDeleteInvoice')
	BEGIN
		DROP  Procedure  uspDeleteInvoice
	END

GO

CREATE Procedure uspDeleteContract
(
	@ContractId INT,
	@success BIT OUTPUT
)
AS
DECLARE @accountId INT,
	@masterAccountId INT,
	@companyId INT,
	@contactid INT,
	@errornum INT

SELECT @success = 0 
SELECT @accountId = AccountId FROM [Contract] WHERE ContractId=@ContractId

IF NOT @accountId IS NULL 
BEGIN		
	-- DELETE THE CONTRACT AND DEPENDANT CHILD
	BEGIN TRANSACTION 
	BEGIN TRY 				
		DELETE FROM Note WHERE EntityId=@ContractId AND Type='Contract'
						
		DELETE FROM ActivityLog WHERE KeyId=@ContractId AND TableName='Contract'
			
		DELETE FROM Audit WHERE KeyId=@ContractId AND TableName='Contract'
				
		DELETE FROM ContractCustomFee WHERE ContractId=@ContractId
				
		DELETE FROM ContractPlanVariance WHERE ContractId=@ContractId
				
		UPDATE V SET Imported=0
		FROM VizadaData V, [Call] C
		WHERE V.MobileNumber = C.ServiceNumber
			AND (V.CallDate + ' ' + V.CallTime + '.000') = C.CallTime	
			AND C.ContractId=@ContractId
				
		UPDATE S SET Imported = 0
		FROM SatComData S, [Call] C 
		WHERE S.OriginatorNo = C.ServiceNumber
			AND (S.Date + ' ' + S.Time + '.000') = C.CallTime	
			AND C.ContractId=@ContractId
				
		DELETE FROM [Call] WHERE ContractId=@ContractId
		
		DELETE FROM [Contract] WHERE ContractId=@ContractId
		
		COMMIT 
		SELECT @success=1		
	END TRY
	BEGIN CATCH 
		ROLLBACK 
		SELECT @success = 0
	END CATCH 
			
	IF @success = 1
	BEGIN	
		BEGIN TRANSACTION 
		BEGIN TRY 
			DELETE FROM Account WHERE ParentAccountId=@AccountId
						
			SELECT @CompanyId=CompanyId FROM Account WHERE AccountId=@AccountId					
			SELECT @ContactId=ContactId FROM Account WHERE AccountId=@AccountId
			DELETE FROM Account WHERE AccountId=@AccountId
			
			DELETE FROM ActivityLog WHERE KeyId=@ContactId AND TableName='Contact'			
			DELETE FROM Contact WHERE ContactId=@ContactId
			
						
			DELETE FROM ActivityLog WHERE KeyId=@CompanyId AND TableName='Company'			
			DELETE FROM Company WHERE CompanyId=@CompanyId
			
			COMMIT 		
		END TRY 					
		BEGIN CATCH 
			ROLLBACK 			
		END CATCH 	
	END
END

GRANT EXEC ON uspDeleteContract TO PUBLIC
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[uspDeleteInvoice]
(
	@InvoiceHeaderId INT,
	@success BIT OUTPUT
)
AS

SELECT @success = 0

BEGIN TRANSACTION
BEGIN TRY	
	DELETE FROM AccountInvoice WHERE InvoiceHeaderId = @InvoiceHeaderId
	DELETE FROM InvoiceLineItem WHERE InvoiceHeaderId = @InvoiceHeaderId
	DELETE FROM InvoiceHeader WHERE InvoiceHeaderId = @InvoiceHeaderId

	COMMIT TRANSACTION
	SELECT @success = 1
END TRY 
BEGIN CATCH 
	ROLLBACK TRANSACTION
	SELECT @success = 0 
END CATCH 

GRANT EXEC ON uspDeleteInvoice TO PUBLIC