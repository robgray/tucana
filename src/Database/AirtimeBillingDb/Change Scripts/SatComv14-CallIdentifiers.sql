﻿INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('12B9','Voicemail','Iridium')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('BC9A','Fixed','GSPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('BCB9','Voicemail','GSPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('BCBC','GSPS Voice','GSPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('BCBD','GSPS SMS','GSPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('BC9B','Cellular','GSPS Voice')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('8183','Cellular','FBB')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('81B9','Voicemail','FBB')
INSERT [dbo].[SatComCallIdentifier] (CallIdentifierId, CalledToCallType, InitiatorCallType) VALUES ('65B9','Voicemail','BGAN')