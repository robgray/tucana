DELETE FROM AccountInvoice
DELETE FROM InvoiceLineItem
DELETE FROM InvoiceHeader
DELETE FROM Call
DELETE FROM ContractPlanVariance
DELETE FROM Contract
DELETE FROM Agent
DELETE FROM Tariff
DELETE FROM [Plan]
DELETE FROM Account
DELETE FROM Contact
DELETE FROM Company
DELETE FROM ActivityLog
DELETE FROM Audit
DELETE FROM VizadaData
DELETE FROM NetworkTariff
DELETE FROM Network
GO

SET IDENTITY_INSERT dbo.Network ON
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 1, 'Iridium', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 2, 'Thuraya', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 3, 'Inmasat', 1, 1)
SET IDENTITY_INSERT dbo.Network OFF

SET IDENTITY_INSERT dbo.NetworkTariff ON
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 1, 'I0', 'Iridium Free of Charge', 1, 0, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 2, 'I1', 'Iridium Voice', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 3, 'I2', 'Iridium Data 4.8', 0, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 4, 'I3', 'Iridium Data 9.6', 0, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 5, 'I4', 'Iridium Forwarding', 0, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 6, 'I6', 'Iridium Satellite Direct', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 7, 'I6', 'Iridium SMS', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 8, 'IA', 'Iridium Incoming (To Mobile)', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 9, 'P0', 'Inmarsat SPS Voice', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 10, 'P1', 'Inmarsat SPS SMS', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 11, 'P2', 'Inmarsat SPS Fax 2.4', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 12, 'P3', 'Inmarsat SPS Data 2.4', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 13, 'T1', 'Thuraya Voice', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 14, 'T2', 'Thuraya GSM Outbound', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 15, 'T3', 'Thuraya GSM Inbound', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 16, 'T4', 'Thuraya Forward', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 17, 'T5', 'Thuraya SMS', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 18, 'T6', 'Thuraya GMS SMS', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 19, 'T7', 'ThurayaDSL Light', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 20, 'T8', 'ThurayaDSL Plenty', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 21, 'T9', 'ThurayaDSL Extra', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 22, 'TA', 'ThurayaDSL Unlimited', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 23, 'TB', 'Thuraya Fax', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 24, 'TC', 'Thuraya Data', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 25, 'MM-1000', 'Mini-M Voice', 1, 1, 3)
SET IDENTITY_INSERT dbo.NetworkTariff OFF

SET IDENTITY_INSERT [dbo].[Plan] ON
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 1, 'Airtime Billing 30 (12 Months)', 12, 0, 10, 30, 1, 1, 1, 30)
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 2, 'Airtime Billing 30 (24 Months)', 24, 0, 10, 30, 1, 9, 1, 30)
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 3, 'Airtime Billing Mini 50 (24 Months)', 24, 0, 10, 50, 3, 25, 1, 30)
SET IDENTITY_INSERT [dbo].[Plan] OFF

SET IDENTITY_INSERT [dbo].[Tariff] ON
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 1, 1, NULL, 1, 1, 1)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 2, 3, NULL, 1, 1, 2)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 3, 4, NULL, 1, 1, 3)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 4, 3, NULL, 1, 1, 4)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 5, 6, NULL, 1, 1, 5)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 6, 6, NULL, 1, 1, 6)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 7, 1.5, NULL, 2, 1, 1)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 8, 3.55, NULL, 2, 1, 2)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 9, 4.85, NULL, 2, 1, 3)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 10, 7.00, NULL, 2, 1, 4)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 11, 6.25, NULL, 2, 1, 5)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 12, 6.15, NULL, 2, 1, 6)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 13, 1, NULL, 3, 1, 9)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 14, 3, NULL, 3, 1, 10)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 15, 4, NULL, 3, 1, 11)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 16, 3, NULL, 3, 1, 12)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 17, 6, NULL, 3, 1, 25)
SET IDENTITY_INSERT [dbo].[Tariff] OFF
