DELETE FROM CustomFee
DELETE FROM ContractCustomFee
DELETE FROM AccountInvoice
DELETE FROM InvoiceLineItem
DELETE FROM InvoiceHeader
DELETE FROM Call
DELETE FROM ContractPlanVariance
DELETE FROM Contract
DELETE FROM Agent
DELETE FROM Tariff
DELETE FROM [Plan]
DELETE FROM Account
DELETE FROM Contact
DELETE FROM Company
DELETE FROM ActivityLog
DELETE FROM Audit
DELETE FROM VizadaData
DELETE FROM NetworkTariff
DELETE FROM Network
DELETE FROM EwayProcessedData


SET IDENTITY_INSERT dbo.Network ON
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 1, 'Iridium', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 2, 'Thuraya', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 3, 'Inmasat', 1, 1)
INSERT INTO dbo.Network (NetworkId, Name, AirtimeProviderId, IsActive) VALUES( 4, 'BGAN (SatCom)', 2, 1)
SET IDENTITY_INSERT dbo.Network OFF

SET IDENTITY_INSERT dbo.NetworkTariff ON
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 1, 'I0', 'Iridium Free of Charge', 1, 0, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 2, 'I1', 'Iridium Voice', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 3, 'I2', 'Iridium Data 4.8', 0, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 4, 'I3', 'Iridium Data 9.6', 0, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 5, 'I4', 'Iridium Forwarding', 0, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 6, 'I6', 'Iridium Satellite Direct', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 7, 'I6', 'Iridium SMS', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 8, 'IA', 'Iridium Incoming (To Mobile)', 1, 1, 1)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 9, 'P0', 'Inmarsat SPS Voice', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 10, 'P1', 'Inmarsat SPS SMS', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 11, 'P2', 'Inmarsat SPS Fax 2.4', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 12, 'P3', 'Inmarsat SPS Data 2.4', 1, 1, 3)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 13, 'T1', 'Thuraya Voice', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 14, 'T2', 'Thuraya GSM Outbound', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 15, 'T3', 'Thuraya GSM Inbound', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 16, 'T4', 'Thuraya Forward', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 17, 'T5', 'Thuraya SMS', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 18, 'T6', 'Thuraya GMS SMS', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 19, 'T7', 'ThurayaDSL Light', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 20, 'T8', 'ThurayaDSL Plenty', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 21, 'T9', 'ThurayaDSL Extra', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 22, 'TA', 'ThurayaDSL Unlimited', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 23, 'TB', 'Thuraya Fax', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 24, 'TC', 'Thuraya Data', 1, 1, 2)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 25, 'MM-1000', 'Mini-M Voice', 1, 1, 3)

INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 26, '6555', 'Standard IP', 1, 0, 4)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 27, '6557', 'Fixed', 1, 1, 4)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 28, '6559', 'Voice to Caller', 1, 1, 4)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 29, '655A', 'Fax', 1, 0, 4)
INSERT INTO dbo.NetworkTariff (NetworkTariffId, Code, Tariff, IsCountedInFreeCall, HasFlagfall, NetworkId) VALUES( 30, '6560', 'SMS', 1, 0, 4)
SET IDENTITY_INSERT dbo.NetworkTariff OFF

SET IDENTITY_INSERT [dbo].[Plan] ON
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 1, 'Airtime Billing 30', 12, 0, 10, 30, 1, 1, 1, 30)
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 2, 'Airtime Billing 30', 24, 0, 10, 30, 1, 9, 1, 30)
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 3, 'Airtime Billing Mini 50 (24 Months)', 24, 0, 10, 50, 3, 25, 1, 30)
INSERT INTO [dbo].[Plan] (PlanId, [Name], Duration, Flagfall, FreeCallAmount, PlanAmount, NetworkId, DefaultTariffId, IsActive, UnitOfTime) VALUES( 4, 'Airtime Billing Mini 50 (24 Months)', 24, 0.55, 10, 50, 4, 28, 1, 15)
SET IDENTITY_INSERT [dbo].[Plan] OFF

SET IDENTITY_INSERT [dbo].[Tariff] ON
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 1, 1, NULL, 1, 1, 1)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 2, 3, NULL, 1, 1, 2)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 3, 4, NULL, 1, 1, 3)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 4, 3, NULL, 1, 1, 4)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 5, 6, NULL, 1, 1, 5)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 6, 6, NULL, 1, 1, 6)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 7, 1.5, NULL, 2, 1, 1)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 8, 3.55, NULL, 2, 1, 2)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 9, 4.85, NULL, 2, 1, 3)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 10, 7.00, NULL, 2, 1, 4)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 11, 6.25, NULL, 2, 1, 5)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 12, 6.15, NULL, 2, 1, 6)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 13, 1, NULL, 3, 1, 9)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 14, 3, NULL, 3, 1, 10)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 15, 4, NULL, 3, 1, 11)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 16, 3, NULL, 3, 1, 12)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 17, 6, NULL, 3, 1, 25)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 18, 1.88, NULL, 4, 0, 26)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 19, 1.88, NULL, 4, 1, 27)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 20, 1.88, NULL, 4, 1, 28)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 21, 1.88, NULL, 4, 0, 29)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 22, 1.88, NULL, 4, 0, 30)
INSERT INTO dbo.Tariff (TariffId, UnitCost, Markup, PlanId, HasFlagfall, NetworkTariffId) VALUES( 23, 0.55, NULL, 1, 0, 7)
SET IDENTITY_INSERT [dbo].[Tariff] OFF

SET IDENTITY_INSERT dbo.Company ON
INSERT INTO Company (CompanyId, CompanyName, ABN, ACN, Street, Suburb, State, Postcode, PostalStreet, PostalSuburb, PostalState, PostalPostcode)
VALUES (1, 'Mikel', NULL, NULL, '434 Ripley Rd', 'Ripley', 'QLD', '4305', NULL, NULL, NULL, NULL);
INSERT INTO Company (CompanyId, CompanyName, ABN, ACN, Street, Suburb, State, Postcode, PostalStreet, PostalSuburb, PostalState, PostalPostcode)
VALUES (2, 'RG Software Solutions', '68664344985', '123456789', '22 Turrbal St', 'Bellbowrie', 'QLD', '4070', '139 Coronation Dr', 'Milton', 'QLD', '4064');
INSERT INTO Company (CompanyId, CompanyName, ABN, ACN, Street, Suburb, State, Postcode, PostalStreet, PostalSuburb, PostalState, PostalPostcode)
VALUES (3, 'SDSI', NULL, NULL, '139 Coronation Dr', 'Milton', 'QLD', '4064', NULL, NULL, NULL, NULL); 
SET IDENTITY_INSERT dbo.Company OFF

INSERT INTO ActivityLog (LogDate, TableName, KeyId, [User], Activity)
SELECT getdate(), 'Company', CompanyId, 'admin', 'Created Company'
FROM Company
 
SET IDENTITY_INSERT dbo.Contact ON 
INSERT INTO Contact (ContactId, [Name], MobilePhone, WorkPhone, FaxNumber, Email, Street, Suburb, State, Postcode, PostalStreet, PostalSuburb, PostalState, PostalPostcode, DriversLicenseNumber, DriversLicenseExpiry, DriversLicenseDOB, CreditCardName, CreditCardNumber, CreditCardExpiry, CreditCardType, CompanyId)
 VALUES (1, 'Michael Moran', NULL, NULL, NULL, 'test@test.com', 'Ripley Rd', 'Ripley', 'QLD', '4305', NULL, NULL, NULL, NULL, '32323535', '2009-01-01', '1966-04-02', 'M J Moran', '23232323042323', '03/11', 1, NULL); 
INSERT INTO Contact (ContactId, [Name], MobilePhone, WorkPhone, FaxNumber, Email, Street, Suburb, State, Postcode, PostalStreet, PostalSuburb, PostalState, PostalPostcode, DriversLicenseNumber, DriversLicenseExpiry, DriversLicenseDOB, CreditCardName, CreditCardNumber, CreditCardExpiry, CreditCardType, CompanyId)
 VALUES (2, 'Robert Gray', '0411331273', '0732361040', '0732361070', 'test@test.com', '22 Turrbal St', 'Bellbowrie', 'QLD', '4070', 'PO Box 134', 'Bellbowrie', 'QLD', '4070', '37545958', '20-JAN-2012', '21-AUG-1976', 'Mr Robert Gray', '45799033033003921', '01/10', 1, 2);
INSERT INTO Contact (ContactId, [Name], MobilePhone, WorkPhone, FaxNumber, Email, Street, Suburb, State, Postcode, PostalStreet, PostalSuburb, PostalState, PostalPostcode, DriversLicenseNumber, DriversLicenseExpiry, DriversLicenseDOB, CreditCardName, CreditCardNumber, CreditCardExpiry, CreditCardType, CompanyId)
 VALUES (3, 'David Leggett', NULL, '0732361040', NULL, 'test@test.com', '139 Coronation Dr', 'Milton', 'QLD', '4064', '139 Coronation Dr', 'Milton', 'QLD', '4064', '30239584', '2010-02-01', '1960-03-18', 'D J Leggett', '239585435', '04/09', 2, NULL);
SET IDENTITY_INSERT dbo.Contact OFF

INSERT INTO ActivityLog (LogDate, TableName, KeyId, [User], Activity)
SELECT getdate(), 'Contact', ContactId, 'admin', 'Created Contact'
FROM Contact

SET IDENTITY_INSERT dbo.Account ON 
INSERT INTO Account (AccountId, AccountNumber, AccountName, ContactId, CompanyId, ParentAccountId, ContactRole, BillingMethod, EmailBillDataFile, EmailBill, PostBill, BillingAddressType, Password, IsInvoiceRoot, AmountPaid, PreviousBill, IsInternational) 
VALUES (1, 'MK0001', 'Mikel Master Account', 1, 1, NULL, 'Owner', 1, 1, 1, 1, 1, 'Account', 1, 400, 500, 0);
INSERT INTO Account (AccountId, AccountNumber, AccountName, ContactId, CompanyId, ParentAccountId, ContactRole, BillingMethod, EmailBillDataFile, EmailBill, PostBill, BillingAddressType, Password, IsInvoiceRoot, AmountPaid, PreviousBill, IsInternational) 
VALUES (2, 'MK0002', 'Mikel Account 1', 1, NULL, 1, 'Owner', 1, 0, 0, 0, 1, 'Account', 0, 0, 0, 0);
INSERT INTO Account (AccountId, AccountNumber, AccountName, ContactId, CompanyId, ParentAccountId, ContactRole, BillingMethod, EmailBillDataFile, EmailBill, PostBill, BillingAddressType, Password, IsInvoiceRoot, HasRequestedInvoicing, AmountPaid, PreviousBill, IsInternational) 
VALUES (3, 'RG0001', 'RGSS Account 1', 2, 2, NULL, 'Director', 2, 0, 0, 0, 1, 'Account', 1, 0, 0, 0, 0);
INSERT INTO Account (AccountId, AccountNumber, AccountName, ContactId, CompanyId, ParentAccountId, ContactRole, BillingMethod, EmailBillDataFile, EmailBill, PostBill, BillingAddressType, Password, IsInvoiceRoot, AmountPaid, PreviousBill, IsInternational) 
VALUES (4, 'DL0001', 'SDSI Account 1', 3, NULL, NULL, 'CEO & President', 1, 0, 0, 1, 2, 'Account', 1, 0, 0, 0);
SET IDENTITY_INSERT dbo.Account OFF

INSERT INTO ActivityLog (LogDate, TableName, KeyId, [User], Activity)
SELECT getdate(), 'Account', AccountId, 'admin', 'Created Account'
FROM Account

SET IDENTITY_INSERT dbo.Agent ON
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 1, 'Britz - Darwin', '44-46 Stuart Highway', 'Darwin', 'NT', '0820', 0.15, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 2, 'Britz - Adelaide', '376 Sir Donald Bradman Drive', 'Brooklyn Park', 'SA', '5032', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 3, 'Britz - Perth', '471 Great Eastern Highway', 'Redcliffe', 'WA', '6104', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 4, 'Britz - Alice Springs', 'Cnr Stuart Highway & Power Streets', 'Alice Springs', 'NT', '0870', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 5, 'Britz - Sydney', '653 Gardeners Road', 'Mascot', 'NSW', '2020', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 6, 'Britz - Cairns', '411 Sheridan Street', 'Cairns', 'QLD', '4870', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 7, 'Britz - Melbourne', 'Building 24 South Rd', 'Braybrook', 'VIC', '3019', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 8, 'Britz - Brisbane', '647 Kingsford Smith Drive', 'Eagle Farm', 'QLD', '4009', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 9, 'Britz - Hobart', '102 Holyman Avenue', 'Cambridge', 'TAS', '7170', 0, 'test@test.com')
INSERT INTO dbo.Agent (AgentId, AgentName, Street, Suburb, State, Postcode, CommissionPercent, Email) VALUES( 10, 'Britz - Broome', '10 Livingstone Street', 'Broome', 'WA', '6725', 0, 'test@test.com')
SET IDENTITY_INSERT dbo.Agent OFF

SET IDENTITY_INSERT dbo.Contract ON  
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate , IMEINumber, SimCard, AgentId, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, StatusChangedDate, PhoneNumber1) 
VALUES (1, 4, 1, 'CT0001', '13-MAR-2008', '13-MAR-2009', '1234232323', '2323232323', 1, 3, 0, 0, 'Mike', '13-MAR-2008 14:00:00', '13-MAR-2008 14:00:00', '870772522610');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (2, 1, 1, 'CT0002', '13-MAR-2008', '13-MAR-2009', '1232323465', '343435', 2, 0, 0, 'Kelly', '13-MAR-2008 14:30:00', 'Iridium 8550i', '1234', '1234', '13-MAR-2008 14:30:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (3, 1, 2, 'CT0003', '13-MAR-2008', '13-MAR-2009', '0893827896', '3902660233', '2392139232', 3, 0, 1, 'Cameron', '13-MAR-2008 14:45:00', 'Iridium 8550i', '1234', '1234', '13-MAR-2008 14:45:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate , IMEINumber, SimCard, ContractStatus, Data, MessageBank, OrderReceivedDate, StatusChangedDate)
VALUES (4, 1, 2, 'CT0004', '13-MAR-2008', '13-MAR-2009', '958499584', '43434656', 2, 0, 0, '13-MAR-2008 14:50:00', '13-MAR-2008 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (5, 1, 3, 'CT0005', '18-APR-2008', '18-APR-2009', '0823897228', '3928323234', '3232332442', 3, 1, 1, 'Robert', '18-APR-2008 14:50:00', 'Iridium 8850j', '1234', '1234', '18-APR-2008');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate, SuspensionStartDate, SuspendedUntilDate) 
VALUES (6, 1, 3, 'CT0006', '01-JAN-2008', '01-JAN-2010', '3029302930', '6665455334', '34343434343', 4, 0, 0, '01-JAN-2008 14:50:00', 'Iridium 8850j', '2344', '423235', '11-OCT-2009 13:25:11', '11-OCT-2009', '16-OCT-2009');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, StatusChangedDate) 
VALUES (7, 1, 4, 'CT0007', '1-JUN-2008', '1-JUN-2009', '3928999234', '3232338872', 2, 1, 1, 'David', '1-JUN-2008 14:50:00', '1-JUN-2008 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate, PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (8, 1, 4, 'CT0008', '1-MAR-2007', '1-MAR-2008', '0823999299', '1128999234', '1132338872', 3, 1, 1, 'Kevin', '1-03-2007 14:50:00', 'Iridium 8850j', '1234', '1234', '1-MAR-2007 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate, PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate) 
VALUES (9, 1, 4, 'CT0009', '1-MAR-2007', '1-MAR-2008', '0823999299', '1128999234', '1132338872', 3, 1, 1, 'Dave', '1-MAR-2007 14:51:00', 'Iridium 8850a', '1234', '1234', '1-MAR-2007 14:51:00');
INSERT INTO Contract (ContractId ,PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (10, 1, 3, 'CT0010', '18-APR-2006', '18-APR-2007', '0147150051', '3928323234', '3232332442', 3, 1, 1, 'Robert', '18-APR-2006 14:50:00', 'Iridium 8850j', '1234', '1234', 'F18-APR-2006 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate , IMEINumber, SimCard, ContractStatus, Data, MessageBank, OrderReceivedDate, StatusChangedDate)
VALUES (11, 1, 2, 'CT0011', '28-AUG-2008', '28-AUG-2009', '12343233', '12342323', 2, 0, 0, '13-MAR-2008 14:50:00', '13-MAR-2008 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (12, 3, 3, 'CT0012', '18-APR-2008', '18-APR-2009', '764876622', '3928323231', '3232332411', 3, 1, 1, 'Sharon', '18-APR-2008 14:50:00', 'Iridium 8850j', '1234', '1234', '18-APR-2008 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (13, 3, 3, 'CT0013', '18-APR-2008', '18-APR-2009', '764876626', '3928323232', '3232332412', 3, 1, 1, 'Isla', '18-APR-2008 14:50:00', 'Iridium 8850j', '1234', '1234', '18-APR-2008 14:50:00');
INSERT INTO Contract (ContractId, PlanId, AccountId, ContractNumber, ActivationDate, EndDate ,PhoneNumber1, IMEINumber, SimCard, ContractStatus, Data, MessageBank, PhoneUsedBy, OrderReceivedDate, Phone, Pin, Puk, StatusChangedDate)
VALUES (14, 3, 3, 'CT0014', '18-APR-2008', '18-APR-2009', '764876630', '3928323233', '3232332413', 3, 1, 1, 'Stella', '18-APR-2008 14:50:00', 'Iridium 8850j', '1234', '1234', '18-APR-2008 14:50:00');
SET IDENTITY_INSERT dbo.Contract OFF

INSERT INTO ActivityLog (LogDate, TableName, KeyId, [User], Activity)
SELECT getdate(), 'Contract', ContractId, 'admin', 'Created Contract'
FROM Contract

INSERT INTO ActivityLog (LogDate, TableName, KeyId, [User], Activity)
VALUES ('11-OCT-2009','Contract',6,'admin','Suspended Contract until ''16-OCT-2009''')

SET IDENTITY_INSERT dbo.Call ON
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 1, 1, 2, '870772522610', '2008-07-17 15:45:00', '0891927567', 'SATELLITE', 'National Direct', 1, 1, 2, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 2, 1, 2, '870772522610', '2008-07-17 15:47:00', '0891937386', 'SATELLITE', 'National Direct', 4.5, 1, 9, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 3, 1, 2, '870772522610', '2008-07-16 16:23:00', '0883535108', 'SATELLITE', 'National Direct', 0.5, 1, 1, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 4, 5, 2, '0823897228', '2008-07-17 20:02:00', '0427811251', 'SATELLITE', 'National Direct', 1.5, 1, 3, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 5, 5, 2, '0823897228', '2008-07-17 20:04:00', '0883535108', 'SATELLITE', 'National Direct', 2, 1, 4, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 6, 5, 2, '0823897228', '2008-07-18 15:10:00', '0880913872', 'SATELLITE', 'National Direct', 1.5, 1, 3, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 7, 5, 1, '0823897228', '2008-07-18 19:17:00', '0427811251', 'SATELLITE', 'National Direct', 4,1,  8, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 8, 5, 2, '0823897228', '2008-07-18 19:22:00', '0881547226', 'SATELLITE', 'National Direct', 0.5, 1, 1, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 9, 5, 1, '0823897228', '2008-07-18 19:23:00', '0881547226', 'SATELLITE', 'National Direct', 0.5, 1, 1, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 10, 5, 2, '0823897228', '2008-07-18 20:54:00', '0883535108', 'SATELLITE', 'National Direct', 1.5, 1, 3, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 11, 5, 2, '0823897228', '2008-07-19 09:53:00', '0886756627', 'SATELLITE', 'National Direct', 1.5, 1, 3, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 12, 1, 2, '870772522610', '2008-07-18 15:45:00', '0891927561', 'SATELLITE', 'National Direct', 1, 1, 2, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 13, 1, 2, '870772522610', '2008-07-19 16:43:00', '0891937382', 'SATELLITE', 'National Direct', 4.5, 1, 9, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 14, 1, 2, '870772522610', '2008-07-20 19:13:00', '0883535103', 'SATELLITE', 'National Direct', 0.5, 1, 1, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 15, 1, 1, '870772522610', '2008-07-12 11:45:00', '0891927561', 'SATELLITE', 'National Direct', 1, 1, 2, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 16, 1, 2, '870772522610', '2008-07-13 13:43:00', '0891937382', 'SATELLITE', 'National Direct', 4.5, 1, 9, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 17, 1, 2, '870772522610', '2008-07-24 12:13:00', '0883535103', 'SATELLITE', 'National Direct', 0.5, 1, 1, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 18, 1, 2, '870772522610', '2008-07-15 11:45:00', '0891927561', 'SATELLITE', 'National Direct', 1, 1, 2, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 19, 1, 2, '870772522610', '2008-07-16 09:43:00', '0891937382', 'SATELLITE', 'National Direct', 4.5, 1, 9, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 20, 1, 2, '870772522610', '2008-07-27 11:13:00', '0883535103', 'SATELLITE', 'National Direct', 0.5, 1, 1, '2008-08-01 08:00:00')
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 21, 1, 2, '870772522610', '2008-08-03 12:13:00', '0883535103', 'SATELLITE', 'National Direct', 1, 1, 2, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 22, 1, 2, '870772522610', '2008-08-06 03:04:12', '0883535103', 'SATELLITE', 'National Direct', 1.32, 1, 4, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 23, 1, 2, '870772522610', '2008-08-06 14:13:42', '0883535103', 'SATELLITE', 'National Direct', 2.1, 1, 5, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 24, 3, 2, '0893827896', '2008-08-06 14:13:42', '0883535113', 'SATELLITE', 'Instant Connect', 9.15, 1, 19, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 25, 3, 2, '0893827896', '2008-08-07 16:43:14', '0883535114', 'SATELLITE', 'National Direct', 0.15, 1, 1, NULL)
-- DATA AND SMS --
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 26, 3, 4, '870772522610', '2008-08-06 03:04:12', '0883535103', 'SATELLITE', 'Data', 13.43, 2, 13.43, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 27, 3, 4, '870772522610', '2008-08-16 14:13:42', '0883535103', 'SATELLITE', 'Data', 3.77, 2, 3.77, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 28, 3, 23, '0893827896', '2008-08-08 10:07:42', '0883535113', 'SATELLITE', 'SMS', 1, 3, 1, NULL)
INSERT INTO dbo.Call (CallId, ContractId, TariffId, Servicenumber, CallTime, CalledNumber, Location, ImportedCallType, Volume, VolumeUnit, UnitsOfTime, DateInvoiced) 
VALUES( 29, 3, 23, '0893827896', '2008-08-12 19:41:14', '0883535114', 'SATELLITE', 'SMS', 1, 3, 1, NULL)
SET IDENTITY_INSERT dbo.Call OFF

SET IDENTITY_INSERT dbo.InvoiceHeader ON  
INSERT INTO InvoiceHeader (InvoiceHeaderId, InvoiceNumber, InvoiceDate, [To], Street, Suburb, State, Postcode, FromDate, ToDate, InvoiceRunNumber, PreviousBill, AmountPaid, Outstanding, NewCharges, CurrentBill, PaymentDue, TotalIncGst, TotalGst, TotalExGst)
VALUES (1, 'INV0001', '2008-08-01 08:00:00', 'Michael Moran', '123 ABC Street', 'Ripley', 'QLD', '4305', '2008-07-01', '2008-07-31 23:59:59', 1, 500.00, 400.00, 100.00, 378.78, 478.78, '2008-09-01', 478.78, 43.4355, 435.3445)
INSERT INTO InvoiceHeader (InvoiceHeaderId, InvoiceNumber, InvoiceDate, [To], Street, Suburb, State, Postcode, FromDate, ToDate, InvoiceRunNumber, PreviousBill, AmountPaid, Outstanding, NewCharges, CurrentBill, PaymentDue, TotalIncGst, TotalGst, TotalExGst)
VALUES (2, 'INV0002', '2008-08-01 08:00:00', 'Robert Gray','22 Turrbal St', 'Bellbowrie', 'QLD', '4070', '2008-07-01', '2008-07-31 23:59:59', 1, 0, 0, 0, 173.00, 173.00, '2008-09-01', 173.00, 15.6363636363, 157.363636363636)
SET IDENTITY_INSERT dbo.InvoiceHeader OFF 

SET IDENTITY_INSERT dbo.InvoiceLineItem ON  
-- INVOICE INV0001 --
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (1, 1, 'National Direct to 0891927567', 60, 0.909, 10.00, 1, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (2, 1, 'National Direct to 0891937386', 270, 4.0909, 45, 2, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (3, 1, 'National Direct to 0883535108', 30, 0.4545, 5.00, 3, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (4, 1, 'National Direct to 0891927561', 60, 0.909, 10.00, 12, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (5, 1, 'National Direct to 0891937382', 270, 4.0909, 45.00, 13, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (6, 1, 'National Direct to 0883535103', 30, 0.4545, 5.00, 14, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (7, 1, 'National Direct to 0891927561', 60, 0.909, 10.00, 15, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (8, 1, 'National Direct to 0891937382', 270, 4.0909, 45.00, 16, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (9, 1, 'National Direct to 0883535103', 30, 0.4545, 5.00, 17, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (10, 1, 'National Direct to 0891927561', 60, 0.909, 10.00, 18, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (11, 1, 'National Direct to 0891937382', 270, 4.0909, 45.00, 19, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (12, 1, 'National Direct to 0883535103', 30, 0.4545, 5.00, 20, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (13, 1, 'National Direct to 0893827896', 270, 4.0909, 45.00, 24, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (14, 1, 'National Direct to 0893827896', 30, 0.4545, 5.00, 25, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (15, 1, 'Network Access Fees', 1, 2.7272, 30.00, NULL, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (16, 1, 'Data Access Fees', 1, 1.1818, 13.00, NULL, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (27, 1, 'Less free call component', 1, -1.00, -11.00, NULL, 1)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (28, 1, 'Less free call component', 1, -1.00, -11.00, NULL, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (29, 1, 'Network Access Fees', 1, 2.7272, 30.00, NULL, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (30, 1, 'Data Access Fees', 1, 1.1818, 13.00, NULL, 3)
-- DATA AND SMS --
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (31, 1, 'Iridium Data 9,6', 13.43, 2.02272, 22.25, 26, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (32, 1, 'Iridium Data 9,6', 3.77, 0.13181818, 1.43, 27, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (33, 1, 'Iridium SMS', 1, 0.05, 0.55, 28, 3)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (34, 1, 'Iridium SMS', 1, 0.05, 0.55, 29, 3)


-- REMEMBER TO UPDATE THE INVOICE TOTAL IF ADDING NEW LINES!!!!


-- INVOICE INV0002 --
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (17, 2, 'National Direct to 0427811251', 90, 1.3636, 15.00, 4, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (18, 2, 'National Direct to 0883535108', 120, 1.8181, 20.00, 5, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (19, 2, 'National Direct to 0880913872', 90, 1.3636, 15.00, 6, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (20, 2, 'National Direct to 0427811251', 240, 3.6363, 40.00, 7, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (21, 2, 'National Direct to 0881547226', 30, 0.4545, 5.00, 8, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (22, 2, 'National Direct to 0881547226', 30, 0.4545, 5.00, 9, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (23, 2, 'National Direct to 0883535108', 90, 1.3636, 15.00, 10, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (24, 2, 'National Direct to 0886756627', 90, 1.3636, 15.00, 11, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (25, 2, 'Network Access Fees', 1, 2.7272, 30.00, NULL, 5)
INSERT INTO InvoiceLineItem (InvoiceLineItemId, InvoiceHeaderId, Description, Quantity, GstAmount, LineAmount, CallId, ContractId)
VALUES (26, 2, 'Data Access Fees', 1, 1.1818, 13.00, NULL, 5)

SET IDENTITY_INSERT dbo.InvoiceLineItem OFF 

SET IDENTITY_INSERT dbo.AccountInvoice ON
INSERT INTO AccountInvoice (AccountInvoiceId, AccountId, InvoiceHeaderId) VALUES (1, 1, 1)
INSERT INTO AccountInvoice (AccountInvoiceId, AccountId, InvoiceHeaderId) VALUES (2, 2, 1)
INSERT INTO AccountInvoice (AccountInvoiceId, AccountId, InvoiceHeaderId) VALUES (3, 3, 2) 
SET IDENTITY_INSERT dbo.AccountInvoice OFF

Update SystemSettings SET KeyValue = 2 WHERE SystemSettingId = 5 AND KeyName = 'NextInvoiceRunNumber'
Update SystemSettings SET KeyValue = 2 WHERE SystemSettingId = 1 AND KeyName = 'NextInvoiceNumber'
Update SystemSettings SET KeyValue = 15 WHERE SystemSettingId = 2 AND KeyName = 'NextContractNumber'
Update SystemSettings SET KeyValue = '2009-12-01' WHERE SystemSettingId = 4 AND KeyName = 'LastInvoicedDate'

SET IDENTITY_INSERT dbo.CustomFee ON
INSERT INTO CustomFee (CustomFeeId, Description, Amount, IsRecurring) VALUES (1, 'Standard IP', 30.00, 1);
SET IDENTITY_INSERT dbo.CustomFee OFF

SET IDENTITY_INSERT dbo.ContractCustomFee ON
INSERT INTO ContractCustomFee (ContractCustomFeeId, ContractId, CustomFeeId) VALUES (1, 1, 1);
SET IDENTITY_INSERT dbo.ContractCustomFee OFF