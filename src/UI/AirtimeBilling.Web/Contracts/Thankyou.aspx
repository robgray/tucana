<%@Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Thankyou.aspx.cs" Inherits="AirtimeBilling.Web.Contracts.ThankyouPage" %>
<%@ Import Namespace="AirtimeBilling.Web"%>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<br />
<table class="MessageContainer Success" style="width: 95%;">
    <tr>
        <td class="Message">
            <div class="h1">New Contract Submitted!</div>                                
        </td>
    </tr>    
</table>
<div style="padding:10px">
    Thank you for submitting your airtime application to I.<br/><br />
    Your request has been sent to I for processing and an email will be sent to&nbsp;
    <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>&nbsp;
    once the application has been completed.
    <br /><br /><br />
    Kind Regards,<br />
    I Satellite Solutions.
</div>
<div style="text-align: center; margin:10px auto;">
    Click <asp:HyperLink ID="HomePageLink" runat="server">here</asp:HyperLink>&nbsp;to continue.
</div>
</asp:Content>
