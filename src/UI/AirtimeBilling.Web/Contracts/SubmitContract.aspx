<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.SubmitContractPage" Codebehind="SubmitContractPage.aspx.cs" %>
<%@ Register Src="~/UserControls/Contract.ascx" TagName="contract" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/CompanyInfo.ascx" TagName="company" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="address" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Account.ascx" TagName="account" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Contact.ascx" TagName="contact" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ContractTerms.ascx" TagName="terms" TagPrefix="sharp" %>

<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">                
<h1>Contract For New Account</h1>

    <asp:Panel ID="Panel1" runat="server">
        <sharp:MessagePanel ID="UserMessage" runat="server" />
        <asp:ValidationSummary  ID="valSummary" 
                                runat="server"                                 
                                HeaderText="Submission Errors"   
                                CssClass="ValidationSummary"                              
                                ShowSummary="True" />
    </asp:Panel>
    <div class="Form">
        <sharp:contract ID="ucContract" runat="server" ReadOnly="false" NewContract="true" />       
        <sharp:company ID="ucCompany" runat="server" ReadOnly="false" />                   
        <sharp:contact ID="ucContact" runat="server" />                            
        <sharp:FormGroup ID="BillingInformation" runat="server" GroupingText="Billing Information">
            <div class="control-group">
                <label>Payment Method</label>
                <asp:RadioButton ID="rbCreditCard" runat="server"  
                    Text="" GroupName="PaymentMethod" />Credit Card
                <asp:RadioButton ID="rbInvoice" runat="server"  Text="" GroupName="PaymentMethod" />Invoice
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= chkPostBill.ClientID %>">Post Bill</label>
                <asp:CheckBox ID="chkPostBill" runat="server" Checked="True"  />
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= chkEmailBill.ClientID %>">Paperless Billing</label>
                <asp:CheckBox ID="chkEmailBill" runat="server"  />
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= chkEmailBillData.ClientID %>">Email Bill Data</label>
                <asp:CheckBox ID="chkEmailBillData" runat="server"  />
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= chkInternational.ClientID %>">International Customer</label>
                <asp:CheckBox ID="chkInternational" runat="server"  />
            </div>
        </sharp:FormGroup>
        
        <div class="Row">
            <sharp:FormGroup ID="Security" runat="server" GroupingText="Security" CssClass="full">            
                <div class="control-group">
                    <label class="control-label" for="<%= txtPassword.ClientID %>">Password</label>
                    <table>
                        <tr>
                            <td style="vertical-align:top">
                                <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                            </td>
                            <td style="vertical-align:top">
                                <div id="passwordInfo" class="info" style="margin: 0;min-height: 50px;padding-top: 10px; display:none;">
                                    A password is collected as an extra security measure for when customers call to ask about Accounts.
                                    Verification of the password is also required when adding new airtime contracts to existing Accounts.
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </sharp:FormGroup>    
        </div>
    </div>     
    <sharp:terms ID="ContractTerms" runat="server" OnContractSubmitted="SubmitNewContract" />       
</asp:Content>
