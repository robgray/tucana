﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Web
{
    public partial class SubmitContractPage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            UserMessage.Clear();

            if (!IsPostBack)
            {                				
                chkPostBill.Checked = true;
                rbCreditCard.Checked = true;                

                txtPassword.Attributes.Add("onfocus", "$('#passwordInfo').show();");
                txtPassword.Attributes.Add("onblur", "$('#passwordInfo').hide();");
            }
        }

        void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ucContract.ReadOnly = false;
                ucContract.SetNew();
            }
        }    

        protected void SubmitNewContract(object sender, EventArgs e)
        {
            if (!IsValid)
            {
                // Insert some client side popup here?
                return;
            }

            UserMessage.Clear();

            var contact = ucContact.Contact;

            var company = new Company
            {
                CompanyName = ucCompany.CompanyName,
                ABN = ucCompany.ABN,
                ACN = ucCompany.ACN
            };

            var request = new NewContractRequest
            {
                Contact = contact,
                Company = company,
                IMEINumber = ucContract.IMEINumber,
                SimCard = ucContract.SimCard,
                ActivationDate = ucContract.ActivationDate,
                EndDate = ucContract.EndDate,
                MessageBank = ucContract.MessageBank,
                Data = ucContract.Data,
                PlanId = ucContract.PlanId,
                AccountPassword = txtPassword.Text,
                EmailBill = chkEmailBill.Checked,
                PostBill = chkPostBill.Checked,
                EmailBillData = chkEmailBillData.Checked,           
                IsInternational = chkInternational.Checked,
                User = Users.Current
            };

            request.BillingMethod = rbCreditCard.Checked ? BillingMethod.CreditCard : BillingMethod.Invoice;

            var service = ServiceFactory.GetService<IContractService>();
            var response = service.SubmitNewContract(request);

            if (response.IsSuccessful)
            {                
                NavigationController.Thankyou();                
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }
    }
}