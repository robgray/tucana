﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ViewContractPage" Codebehind="ViewContract.aspx.cs" %>
<%@ Register Src="~/UserControls/Contract.ascx" TagName="contract" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/CompanyInfoReadOnly.ascx" TagName="company" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ActivityLogViewer.ascx" TagName="activitylog" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/CallDataViewer.ascx" TagName="calldata" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/AccountReadOnly.ascx" TagName="account" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Contact.ascx" TagName="contact" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="address" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ContractTabSummary.ascx" TagName="contractsummary" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ContractCustomFees.ascx" TagName="customfees" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/EntityNotes.ascx" TagName="notes" TagPrefix="sharp" %>

<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">            
    <h1>Airtime Contract</h1>    
    <hr />
<% if (!CannotDisplayPage)
   { %>
    <div class="well">                    
        <asp:LinkButton CssClass="btn" ID="DetailsViewButton" Text="Details" runat="server" OnClick="DetailsView" CausesValidation="False"  />        
        <asp:LinkButton CssClass="btn" ID="ActivityLogButton" Text="Activity Log" runat="server" OnClick="ActivityLogViewer" CausesValidation="false"  />
        <asp:LinkButton CssClass="btn" ID="ViewCallDataButton" Text="Call Data" runat="server" OnClick="ViewCallData" CausesValidation="false"  />
        <asp:LinkButton CssClass="btn" ID="ViewCustomFeesButton" Text="Custom Fees" runat="server" OnClick="ViewCustomFees" CausesValidation="false"  />
        <asp:LinkButton CssClass="btn" ID="SuspendPhoneUsagePopupButton" Text="Suspend Phone" runat="server" CausesValidation="false"  />
        <asp:LinkButton CssClass="btn" ID="CloseContractPopupButton" Text="Close Contract" runat="server" CausesValidation="false"  />
        <asp:LinkButton CssClass="btn" ID="NotesButton" Text="Notes" runat="server" CausesValidation="false" OnClick="ViewNotesPage"  />
        <asp:LinkButton CssClass="btn btn-danger" ID="ScrubContractPopupButton" Text="Delete" runat="server" CausesValidation="false" />         
    </div>
<% } %>
    
    <sharp:MessagePanel Id="UserMessage" runat="server" />    

<% if (!CannotDisplayPage)
   { %>   
    
    <asp:Panel ID="pnlContractDetails" runat="server">   
        <asp:Panel ID="pnlInvoicingApproval" runat="server" CssClass="alert alert-info">                              
            <h3 >Invoicing Approval</h3>
            <p>The customer has request to pay by Invoice.<br />Does I accept payment by invoice from this customer?</p>
                        
            <div class="form-actions">
                <asp:LinkButton CssClass="btn btn-success" ID="ApproveInvoicingButton" Text="Approve" Icon="accept" OnClick="ApproveInvoicing" runat="server" />
                <asp:LinkButton CssClass="btn btn-danger" ID="DenyInvoicingButton" Text="Deny" Icon="cancel" OnClick="ApproveInvoicing" runat="server" />
            </div>                    
        </asp:Panel>    
    
        
        <sharp:contract ID="Contract" runat="server" ReadOnly="true" NewContract="false" />        

        <div class="row">
            <sharp:company ID="Company" runat="server" ReadOnly="true" />   
        </div>
        
        <sharp:account ID="Account" runat="server" />     
                  
        <sharp:contact ID="Contact" runat="server" ReadOnly="true" />           
        
        <div class="form-actions">                                  
            <asp:LinkButton CssClass="btn btn-success" ID="SaveContractButton" OnClick="UpdateContract" runat="server">
                <i class="icon-ok icon-white"></i> Save Contract
            </asp:LinkButton>
        </div>
    </asp:Panel>   
    <asp:Panel ID="OtherTabsPanel" runat="server">
        <sharp:contractsummary runat="server" ID="ContractSummary"  />
        <asp:Panel ID="pnlActivityLog" runat="server" Visible="False" CssClass="othertabs">        
            <sharp:activitylog ID="LogViewer" runat="server" />    
        </asp:Panel>
        <asp:Panel ID="pnlCallData" runat="server" Visible="False" CssClass="othertabs">        
            <sharp:calldata ID="CallData" runat="server" />
        </asp:Panel>    
        <asp:Panel ID="CustomFeesPanel" runat="server" Visible="false" CssClass="othertabs">
            <div class="Form">
                <sharp:FormGroup ID="SelectFeesFG" runat="server" GroupingText="Add Custom Fee" CssClass="full">
                    <div class="form-inline">
                        <label class="control-label" for="<%= CustomFeesDropDown.ClientID %>">Custom Fee</label>
                        <asp:DropDownList ID="CustomFeesDropDown" runat="server"></asp:DropDownList>
                        <asp:LinkButton CssClass="btn btn-primary" ID="AddCustomFeeButton" Text="Add Fee" Icon="add" OnClick="AddCustomFee" runat="server" />
                    </div>
                </sharp:FormGroup>
            </div>            
            <sharp:customfees ID="CustomFees" runat="server" />                             
        </asp:Panel>
        <asp:Panel ID="NotesPanel" runat="server" CssClass="othertabs">            
            <sharp:notes runat="server" ID="Notes" />
        </asp:Panel>
    </asp:Panel>    
    
    <!-- Modal Popups -->
    <asp:Panel ID="ScrubContractConfirmPanel" runat="server" CssClass="modal" style="display:none;min-height:0px">
        <div class="modal-header">Delete Contract</div>        
        <sharp:Tip Id="Tip1" runat="server" Text="This will completely remove the Contract from the system, as if it were never entered." style="width:500px" />        
        <div class="modal-footer">
            <asp:LinkButton CssClass="btn btn-danger" ID="DeleteContractButton" runat="server" Text="Delete" CausesValidation="false" OnClick="DeleteContract">
                <i class="icon-trash icon-white"></i> Delete Contract
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" ID="CancelDeleteContractButton" runat="server" Text="Cancel" CausesValidation="false" />                             
        </div>       
    </asp:Panel>
     <atk:ModalPopupExtender ID="ScrubContractExtender" runat="server"
        Enabled="True" 
        TargetControlID="ScrubContractPopupButton"
        CancelControlID="CancelDeleteContractButton"
        PopupControlID="ScrubContractConfirmPanel"
        BackgroundCssClass="modal-backdrop">
    </atk:ModalPopupExtender>
    
    <asp:Panel ID="pnlSuspendContract" runat="server" CssClass="modal" style="display:none;min-height:0px;">                                      
        <div class="modal-header"><h3>Suspend Contract</h3></div>
        <div class="modal-body">Suspending the Contract suspends charging access fees for the contract.  A suspension fee will be incurred.  It is assumed the phone has been prevented from making calls whilst suspended.
            <div class="Form" style="width:425px;margin:auto;">
                <div class="control-group verticalcenter" style="text-align: center;">
                    <label class="control-label" for="<%= txtSuspendUntil.ClientID %>" class="rowheader">Suspend Until</label>
                    <asp:TextBox ID="txtSuspendUntil" runat="server" Width="110px"></asp:TextBox>
                    <asp:Image ID="imgSuspendUntilDate" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
                    <span class="tip">Format is dd/mm/yyyy</span>
                    <atk:CalendarExtender ID="txtSuspendUntilCalendarExtender" runat="server" 
                        TargetControlID="txtSuspendUntil" 
                        PopupButtonID="imgSuspendUntilDate"
                        Format="dd/MM/yyyy">                        
                    </atk:CalendarExtender>                                                         
                </div> 
            </div>        
        </div>        
        <div class="modal-footer">
            <asp:LinkButton CssClass="btn btn-success" ID="SuspendPhoneUsageButton" runat="server" Text="Suspend" CausesValidation="false" OnClick="SuspendPhoneUsage">
                <i class="icon-ok icon-white"></i> Suspend
            </asp:LinkButton>      
            <asp:LinkButton CssClass="btn" ID="StopSupendButton" runat="server" Text="Cancel" CausesValidation="false" />                                     
        </div>       
        <div>
            <asp:UpdatePanel ID="SuspendContractUpdatePanel" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="updProgess" runat="server" AssociatedUpdatePanelID="SuspendContractUpdatePanel">
                        <ProgressTemplate>
                            <div class="SearchUpdateProgress">
                                <asp:Image ID="SuspendUpdateProgressAjaxImage" runat="server" ImageUrl="~/images/ajax-loader.gif" />Suspending...
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>                                                    
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="SuspendPhoneUsageButton" />
                </Triggers>
            </asp:UpdatePanel>
        </div>            
    </asp:Panel>
    <atk:ModalPopupExtender ID="SuspendContractPopup" runat="server"
        Enabled="True" 
        TargetControlID="SuspendPhoneUsagePopupButton"
        CancelControlID="StopSupendButton"
        PopupControlID="pnlSuspendContract"
        BackgroundCssClass="modal-backdrop">
    </atk:ModalPopupExtender>    
    <!-- Close Contract Confirmation -->
    <asp:Panel ID="pnlConfirmCancel" runat="server" CssClass="modal Information" style="display: none;">                    
        <div class="modal-header">
            <h3>Close Contract</h3>
        </div>
        <div class="modal-body">                
            Please ensure all invoicing for this contract as been run before closing.<br />
            Click 'Yes' to close this contract.
        </div>        
        <div class="modal-footer"> 
            <asp:LinkButton CssClass="btn btn-danger" ID="CloseContractButton" runat="server" Text="Yes" CausesValidation="false" OnClick="CloseContract">
                <i class="icon-ok icon-white"></i> Close
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn" ID="CancelCloseButton" runat="server" Text="Cancel" CausesValidation="false" />
        </div>                                                                            
    </asp:Panel>    
    <atk:ModalPopupExtender ID="ConfirmCancelPopup" runat="server" 
        Enabled="True" 
        TargetControlID="CloseContractPopupButton"
        CancelControlID="CancelCloseButton"
        PopupControlID="pnlConfirmCancel"
        BackgroundCssClass="modal-backdrop">
    </atk:ModalPopupExtender>
  <% } %>
</asp:Content>

