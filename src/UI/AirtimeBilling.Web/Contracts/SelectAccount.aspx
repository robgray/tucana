<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="SelectAccount.aspx.cs" Inherits="AirtimeBilling.Web.Contracts.SelectAccountPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">    
    <h1>Select Account for new Contract</h1>
    <hr />
    <p>Enter the Account Number for the account this contract will be added to</p>
    <div class="row">
        <sharp:FormGroup ID="AccountXYZ12" runat="server" CssClass="span12">
            <div>
                <label class="control-label">Account Number</label>
                <div class="controls">
                    <asp:TextBox ID="txtAccountNumber" runat="server" AutoPostBack="True" 
                        ontextchanged="txtAccountNumber_TextChanged"></asp:TextBox>
                </div>
            </div>
        </sharp:FormGroup>
    </div>            
    <div class="row" style="margin-top: 10px;">
        <asp:UpdatePanel ID="upnlAccount" runat="server">
            <ContentTemplate>                        
                <asp:Panel ID="SearchResultsPanel" runat="server" CssClass="span6">                                                            
                    <div class="alert alert-info">  
                        <h4 style="text-align: center; margin-bottom:10px">Found Account</h4>                        
                    
                        <sharp:FormGroup ID="AccountXYZ" runat="server">
                            <div class="control-group">
                                <label class="control-label">Account Number</label>
                                <div class="controls">
                                    <asp:Label ID="lblAccountNumber" runat="server" Text="" ></asp:Label>
                                </div>
                            </div>
                            <div class="control-group">                            
                                <label class="control-label">Account Name</label>
                                <div class="controls">
                                    <asp:Label ID="lblAccountName" runat="server" Text="" ></asp:Label>
                                </div>
                            </div>
                        </sharp:FormGroup>                                                                           
                    </div>                    
                </asp:Panel>   
                                                                        
                <div class="span6">
                    <sharp:MessagePanel ID="SearchResult" runat="server" />                            
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="txtAccountNumber" EventName="TextChanged" />
            </Triggers>
        </asp:UpdatePanel>        
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="upnlAccount" DisplayAfter="1">
        <ProgressTemplate>
            <div style="display:inline;left:250px">                    
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/ajax-loader.gif" />
            </div>
        </ProgressTemplate>        
    </asp:UpdateProgress>      
    
    <div class="form-actions">
        <asp:LinkButton CssClass="btn btn-primary" ID="ExistingAccountContractButton" 
            Text="Add to Account" runat="server" onclick="ExistingAccountContractButton_Click" />
        <asp:LinkButton CssClass="btn btn-primary" ID="NeAccountContractButton" Text="New Account" runat="server" onclick="NeAccountContractButton_Click" />            
    </div> 
</asp:Content>
