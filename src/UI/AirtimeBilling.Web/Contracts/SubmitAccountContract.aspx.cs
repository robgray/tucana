﻿using System;
using System.Web.UI;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Web
{
    public partial class SubmitAccountContractPage : BasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            UserMessage.Clear();
            if (!IsPostBack)
            {                
                Page.LoadComplete += new EventHandler(Page_LoadComplete);

                if (CurrentSession.Account == null)
                {
                    Response.Redirect("~/Contracts/SubmitContract.aspx");
                }                                
            }
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var account = CurrentSession.Account;

                if (account == null) {
                    UserMessage.SetFailure(
                        "No account has been selected.  Please select an account.  Click <a href='/Contracts/SelectAccount.aspx'>here</a> to continue");
                    return;
                }
                
                ucAccount.SetAccountNumberAndId(account.Id.Value, account.AccountNumber);
                ucAccount.AmountPaid = account.AmountPaid;
                ucAccount.PreviousBill = account.PreviousBill;                
                ucAccount.AccountName = account.AccountName;                
                ucAccount.BillingAddressType = account.BillingAddressType;
                ucAccount.BillingMethod = account.BillingMethod;
                ucAccount.EmailBill = account.EmailBill;
                ucAccount.EmailBillDataFile = account.EmailBillDataFile;
                ucAccount.PostBill = account.PostBill;
                ucAccount.IsInvoiceRoot = account.IsInvoiceRoot;
                ucAccount.Password = account.Password;
                ucAccount.IsInternational  = account.IsInternational;

                ucAccount.Company = "";                
                if (account is MasterAccount)
                {
                    MasterAccount master = account as MasterAccount;
                    if (master.CompanyId != null)
                    {
                        ucAccount.Company = CompanyService.GetCompany(master.CompanyId.Value).CompanyName;
                    }
                }
                ucAccount.Contact = ContactService.GetContact(account.ContactId).Name;

                ucContract.SetNew();
            }
        }

        protected void SubmitNewContract(object sender,EventArgs e)
        {
            var service = ServiceFactory.GetService<IContractService>();
            var request = new NewContractForAccountRequest
                              {

                                  AccountNumber = ucAccount.AccountNumber,
                                  AccountPassword = txtPassword.Text,
                                  ActivationDate = ucContract.ActivationDate,
                                  EndDate = ucContract.EndDate,
                                  Data = ucContract.Data,
                                  MessageBank = ucContract.MessageBank,
                                  PlanId = ucContract.PlanId,
                                  SimCard = ucContract.SimCard,
                                  IMEINumber = ucContract.IMEINumber,
                                  UsedBy = ucContract.PhoneUsedBy,
                                  User = Users.Current
                              };
            var response = service.SubmitNewContractForAccount(request);
            if (!response.IsSuccessful)
            {
                // lblErrorMessage.Text = response.Message;
                UserMessage.SetFailure(response.Message);
            }
            else
            {                
                NavigationController.Thankyou();
            }
        }
    }
}