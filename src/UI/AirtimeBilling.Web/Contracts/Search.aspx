﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ContractSearchPage" Codebehind="Search.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1 class="h1">Find Contract</h1>
<div class="well form-inline">
    <table>
        <tbody>
            <tr>
                <td>
                    <label>Search:</label>
                    <asp:DropDownList ID="ddlStatus" runat="server">
                        <asp:ListItem Text="(All)" Value="-1"></asp:ListItem>        
                        <asp:ListItem Text="(Open)" Value="0" Selected="true"></asp:ListItem>
                        <asp:ListItem Text="Application Submitted" Value="1"></asp:ListItem>        
                        <asp:ListItem Text="Application Pending" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Active" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Suspended" Value="4"></asp:ListItem>
                        <asp:ListItem Text="Closed" Value="5"></asp:ListItem>        
                    </asp:DropDownList>    
                    <asp:TextBox ID="txtSearch" runat="server" Width="300px"></asp:TextBox>      
                </td>
                <td>
                    <asp:DropDownList ID="ddlSearchField" runat="server">    
                        <asp:ListItem Text="Contract Number" Value="Contract Number" Selected="true"></asp:ListItem>        
                        <asp:ListItem Text="Account Number" Value="Account Number"></asp:ListItem>
                        <asp:ListItem Text="Contact Name" Value="Contact Name"></asp:ListItem>
                        <asp:ListItem Text="Phone Number" Value="Phone Number"></asp:ListItem>                
                    </asp:DropDownList>                      
                </td>
                <td>                    
                    <asp:LinkButton CssClass="btn btn-primary" ID="SearchContractsButton" 
                        Text="Search" runat="server" CausesValidation="false"                
                        onclick="SearchContracts">
                        <i class="icon-search icon-white"></i> Search
                    </asp:LinkButton>
                        </td>
            </tr>
        </tbody>    
    </table>
</div> 
<sharp:ClickableGridView ID="gvContracts" runat="server"
    OnRowDataBound="gvContracts_RowDataBound" 
    OnRowCommand="gvContracts_RowCommand" 
    AutoGenerateColumns="False"       
    DataKeyNames="ContractId" 
    AllowPaging="True"
    PageSize="50"
    OnPageIndexChanging="gvContracts_PageIndexChanging"   
    EditNavigateUrl="~/Contracts/ViewContracts.aspx" 
    onrowediting="gvContracts_RowEditing"
    OnRowClicked="gvContracts_RowClicked">
    <Columns>                
        <asp:BoundField DataField="ContractNumber" HeaderText="Contract Number" ReadOnly="True" SortExpression="ContractNumber">
            <HeaderStyle Width="60px" />
            <ItemStyle Width="60px" />
        </asp:BoundField>
        <asp:BoundField DataField="AccountNumber" HeaderText="Account Number" ReadOnly="True" SortExpression="AccountNumber">  
            <HeaderStyle Width="60px" />
            <ItemStyle Width="60px" />
        </asp:BoundField>
        <asp:BoundField DataField="ContactName" HeaderText="Contact" ReadOnly="True" SortExpression="ContactName">  
            <HeaderStyle Width="130px" />
            <ItemStyle Width="130px" />
        </asp:BoundField>
        <asp:BoundField DataField="PhoneNumber" HeaderText="Phone Number" ReadOnly="True" SortExpression="PhoneNumber">  
            <HeaderStyle Width="100px" />
            <ItemStyle Width="100px" />
        </asp:BoundField>
        <asp:BoundField DataField="CompanyName" HeaderText="Company" ReadOnly="True" SortExpression="CompanyName" />
        <asp:BoundField DataField="ContractStatus" HeaderText="Status" ReadOnly="True" SortExpression="ContractStatus">
            <HeaderStyle Width="80px" />
            <ItemStyle Width="80px" />
        </asp:BoundField>
    </Columns>
    <EmptyDataTemplate>        
        <sharp:MessagePanel runat="server" ID="NoResultsMessage" MessageType="alert-info" Message="The search was performed but no results were found." IncludeClose="False"/>                                
    </EmptyDataTemplate>
</sharp:ClickableGridView>
</asp:Content>


