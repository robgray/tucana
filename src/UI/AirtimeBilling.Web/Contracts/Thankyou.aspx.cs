﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web.Contracts
{
    public partial class ThankyouPage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                lblEmail.Text = Users.Current.Email;
                HomePageLink.NavigateUrl = Users.Current.HomePage;
            }

            // Airtime Contract should be included in Session object.
            if (Session["Rep"] != null)
            {
                Response.Redirect("~/Reports/ReportViewer.aspx", "_blank", "");
            }
        }
    }
}
