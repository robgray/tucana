<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.SubmitAccountContractPage" Codebehind="SubmitAccountContractPage.aspx.cs" %>
<%@ Register Src="~/UserControls/Contract.ascx" TagName="contract" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/AccountReadOnly.ascx" TagName="account" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ContractTerms.ascx" TagName="terms" TagPrefix="sharp" %>

<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Contract For Existing Account</h1>
<hr />
    <sharp:MessagePanel ID="UserMessage" runat="server" />    
    <asp:ValidationSummary  ID="valSummary" 
                                runat="server"                                 
                                HeaderText="Submission Errors"   
                                CssClass="ValidationSummary"                              
                                ShowSummary="True" />
    <div class="Form">        
        <sharp:Account ID="ucAccount" runat="server" />            
        <sharp:Contract ID="ucContract" runat="server" NewContract="true" />     
        <div class="Row">
            <sharp:FormGroup ID="Security" runat="server" GroupingText="Security" CssClass="full">            
                <div class="control-group">
                    <label class="control-label" for="<%= txtPassword.ClientID %>" class="rowheader">Password</label>
                    <div class="controls">
                        <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvPassword" runat="server" 
                            ErrorMessage="Missing Password Check" Display="None"
                            ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                    </div>                                                                                    
                </div>
            </sharp:FormGroup>    
        </div>
    </div>             
    
    <sharp:terms ID="ContractTerms" runat="server" OnContractSubmitted="SubmitNewContract" />  
</asp:Content>

