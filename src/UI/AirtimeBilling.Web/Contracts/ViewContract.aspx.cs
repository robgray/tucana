﻿using System;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using System.Web.UI;

namespace AirtimeBilling.Web
{


    public partial class ViewContractPage : BackOfficeBasePage
    {
        protected bool CannotDisplayPage 
        { 
            get { return (bool) (ViewState["nodisplay"] ?? false);}
            set { ViewState["nodisplay"] = value; } 
        }

        private enum ViewContractTab
        {
            Details,
            ActivityLog,
            CallData,
            CustomFees,
            Notes
        } ;

        protected override void OnLoad(EventArgs e)
        {
           base.OnLoad(e);

           UserMessage.Clear();

           if (!IsPostBack)
           {
               if (Request.QueryString["cid"] == null)
               {
                   Response.Redirect(Users.Current.HomePage);
               }               
           }
        }        

        private bool IsReadOnly
        {
            get
            {
                return CurrentSession.SearchReadOnly;
            }
        }

        void Page_LoadComplete(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ClientScript.RegisterStartupScript(GetType(), "startups", GetPageStartupScript());

                if (Request.QueryString["cid"] != null)
                {
                    int contractId;
                    if (int.TryParse(Request.QueryString["cid"], out contractId))
                    {
                        var user = Users.Current;
                        var request = new ViewContractRequest { ContractId = contractId, User = user };

                        var response = ContractService.ViewContract(request);
                        if (response.IsSuccessful)
                        {
                            PopulateView(response);
                        }
                        else
                        {
                            UserMessage.SetFailure(response.Message);
                            CannotDisplayPage = true;
                        }
                    }
                }                
            }
        }

        public void PopulateView(ViewContractResponse response)
        {
            ContractSummary.ContractNumber = response.Contract.ContractNumber;
            ContractSummary.ContactName = response.Contact.Name;
            
            // The next two are now done in the code behind the button.
            // LogViewer.Entity = response.Contract;
            // CallData.ContractId = response.Contract.Id.Value;
            
            Contract.Contract = response.Contract;
            Contract.ContractId = response.Contract.Id.Value;

            Account.SetAccountNumberAndId(response.Account.Id.Value, response.Account.AccountNumber);            
            Account.EmailBill = response.Account.EmailBill;
            Account.EmailBillDataFile = response.Account.EmailBillDataFile;
            Account.PostBill = response.Account.PostBill;
            Account.BillingMethod = response.Account.BillingMethod;
            Account.BillingAddressType = response.Account.BillingAddressType;            
            Account.AccountName = response.Account.AccountName;
            Account.Password = response.Account.Password;            
            Account.IsInvoiceRoot = response.Account.IsInvoiceRoot;
            Account.IsInternational = response.Account.IsInternational;
            Account.AmountPaid = response.Account.AmountPaid;
            Account.PreviousBill = response.Account.PreviousBill;
            Account.Contact = response.Contact.Name;

            Contact.Contact = response.Contact;

            if (response.Company != null)
            {
                Account.Company = response.Company.CompanyName;
                Company.ABN = response.Company.ABN;
                Company.ACN = response.Company.ACN;
                Company.CompanyName = response.Company.CompanyName;
                Company.Visible = true;
            }
            else
            {
                Account.Company = null;
                Company.Visible = false;
            }

            pnlInvoicingApproval.Visible = response.Account.HasRequestedInvoicing &&
                                            Users.IsInRole(Users.Current, Roles.BackOffice) &&
                                            !IsReadOnly;

            SetStatusDependantControls(response.Contract.ContractStatus);

            SetTab(ViewContractTab.Details);

            ScrubContractPopupButton.Visible = !InvoicingService.HasContractBeenInvoiced(response.Contract);

            var viewReadOnly = response.Account.HasRequestedInvoicing || (!Users.IsInRole(Users.Current, Roles.BackOffice)) || IsReadOnly;
            SetControlReadOnly(viewReadOnly);

            // (Changed 14/10/2009 rag) Report handled in button press now.
            //var rep = ReportFactory.GetAirtimeContract(Contract.ContractId);
            //Session["Rep"] = rep;
        }

        private void SetStatusDependantControls(ContractStatus status)
        {         
            SuspendPhoneUsagePopupButton.Visible = status == ContractStatus.Active && !IsReadOnly;

            CloseContractPopupButton.Visible = (status == ContractStatus.Active || status == ContractStatus.Suspended) && !IsReadOnly;

            SaveContractButton.Visible = status != ContractStatus.Closed && !IsReadOnly;            
        }

        private void SetControlReadOnly(bool readOnly)
        {
            Contract.ReadOnly = readOnly;
            Company.ReadOnly = readOnly;
            Contact.ReadOnly = readOnly;
            Account.ReadOnly = readOnly;
        }

        protected void ApproveInvoicing(object sender, EventArgs e)
        {
            if (Account.AccountId != null)
            {
                bool approval = ((Control)sender).ID == "ApproveInvoicingButton" ? true : false;
                var service = ServiceFactory.GetService<IContractService>();
                var user = new User { Username = User.Identity.Name, IsAuthenticated = User.Identity.IsAuthenticated };
                var request = new ApproveInvoicingRequest { AccountId = Account.AccountId.Value, IsApprovalGranted = approval, User = user };
                var response = service.ApproveInvoicing(request);
                if (response.IsSuccessful)
                {
                    pnlInvoicingApproval.Visible = false;
                    Account.BillingMethod = BillingMethod.Invoice;

                    // Enable controls.
                    SetControlReadOnly(false);
                }
                else
                {                    
                    UserMessage.SetFailure(response.Message);
                }
            }
            else
            {
                UserMessage.SetFailure("No account associated with this contact.  Cannot approve");                
            }
        }
        
        protected void UpdateContract(object sender, EventArgs e)
        {
            var request = new UpdateContractRequest
                              {
                                  ContractId = Contract.ContractId,
                                  ActivationDate = Contract.ActivationDate,
                                  EndDate = Contract.EndDate,                                  
                                  Pin = Contract.Pin,
                                  PlanId = Contract.PlanId,
                                  Puk = Contract.Puk,                                  
                                  UsedBy = Contract.PhoneUsedBy,
                                  PhoneNumber1 = Contract.PhoneNumber1,
                                  PhoneNumber2 = Contract.PhoneNumber2,
                                  PhoneNumber3 = Contract.PhoneNumber3,
                                  PhoneNumber4 = Contract.PhoneNumber4,

                                  ContactName = Contact.Name,
                                  ContactEmail = Contact.Email,
                                  CreditCardExpiry = Contact.CreditCardExpiry,
                                  CreditCardName = Contact.CreditCardName,
                                  CreditCardNumber = Contact.CreditCardNumber,
                                  CreditCardType = Contact.CreditCardType,
                                  DateOfBirth = Contact.DriversLicenseDateOfBirth.Value,
                                  DeliveryAddress =
                                      new Address
                                          {
                                              Street = Contact.DeliveryStreet,
                                              Suburb = Contact.DeliverySuburb,
                                              State = Contact.DeliveryState,
                                              Postcode = Contact.DeliveryPostcode
                                          },
                                  HomeAddress =
                                      new Address
                                          {
                                              Street = Contact.HomeStreet,
                                              Suburb = Contact.HomeSuburb,
                                              State = Contact.HomeState,
                                              Postcode = Contact.HomePostcode
                                          },
                                    FaxNumber = Contact.FaxNumber,
                                    MobilePhoneNumber = Contact.MobilePhone,
                                    WorkNumber = Contact.WorkPhone,
                                    LicenseExpiry = Contact.DriversLicenseExpiry.Value,
                                    LicenseNumber = Contact.DriversLicenseNumber,                                    
                                    User = Users.Current
                            };

            var response = ContractService.UpdateContract(request);
            if (response.IsSuccessful)
            {
                Contract.Contract = response.Contract;
                SetStatusDependantControls(response.Contract.ContractStatus);

                UserMessage.SetSuccess("Successful Contract Update.");
            }
            else
            {
                UserMessage.SetFailure(response.Message);                
            }
        }
               

        private void SetTab(ViewContractTab tab)
        {
            pnlActivityLog.Visible = tab == ViewContractTab.ActivityLog;
            pnlContractDetails.Visible = tab == ViewContractTab.Details;
            pnlCallData.Visible = tab == ViewContractTab.CallData;
            CustomFeesPanel.Visible = tab == ViewContractTab.CustomFees;
            NotesPanel.Visible = tab == ViewContractTab.Notes;
            OtherTabsPanel.Visible = tab != ViewContractTab.Details;            
        }

        protected void ActivityLogViewer(object sender, EventArgs e)
        {
            SetTab(ViewContractTab.ActivityLog);

            // Load the activity log
            LogViewer.Entity = Contract.Contract;
        }
        protected void ViewCallData(object sender, EventArgs e)
        {
            SetTab(ViewContractTab.CallData);

            // Load the call data.
            CallData.ContractId = Contract.ContractId;
        }

        protected void DetailsView(object sender, EventArgs e)
        {
            SetTab(ViewContractTab.Details);
            // should I requery here?
        }

        protected void ViewNotesPage(object sender, EventArgs e)
        {
            SetTab(ViewContractTab.Notes);

            Notes.SetEntity(Contract.Contract);
        }        

        protected void SuspendPhoneUsage(object sender, EventArgs e)
        {
            var contractId = Contract.ContractId;
            if (contractId == 0)
            {
                UserMessage.SetFailure("Invalid Contract Id in request to Suspend contract.");               
                return;
            }

            DateTime dteSuspend;
            if (!DateTime.TryParse(txtSuspendUntil.Text, out dteSuspend))
            {
                UserMessage.SetFailure("Invalid Suspension Date/Time");                
                return;
            }
            
            var request = new SuspendContractRequest { ContractId = contractId, SuspendUntil = dteSuspend, User = Users.Current };
            var response = ContractService.SuspendContract(request);
            if (response.IsSuccessful)
            {
                var responseContract = ContractService.ViewContract(new ViewContractRequest { ContractId = contractId, User = Users.Current });
                if (responseContract.IsSuccessful)
                {
                    // Set Contract
                    PopulateView(responseContract);

                    SuspendContractPopup.Hide();

                    UserMessage.SetSuccess("Contract has been suspended");
                }
                else
                {
                    UserMessage.SetFailure(responseContract.Message);                    
                }
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void CloseContract(object sender, EventArgs e)
        {
            var contractId = Contract.ContractId;
            if (contractId == 0) return;
            
            var request = new CloseContractRequest { ContractId = contractId, User = Users.Current };
            var response = ContractService.CloseContract(request);

            if (response.IsSuccessful)
            {
                var responseContract = ContractService.ViewContract(new ViewContractRequest { ContractId = contractId, User = Users.Current });
                if (responseContract.IsSuccessful)
                {
                    // Set Contract
                    PopulateView(responseContract);

                    ConfirmCancelPopup.Hide();

                    UserMessage.SetSuccess("Contract has been closed");
                }
                else
                {
                    UserMessage.SetFailure(responseContract.Message);                    
                }
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void ViewCustomFees(object sender, EventArgs e)
        {
            CustomFees.LoadCustomFees(Contract.ContractId);

            var fees = InvoicingService.GetAllCustomFees();
            CustomFeesDropDown.DataSource = fees;
            CustomFeesDropDown.DataValueField = "Id";
            CustomFeesDropDown.DataTextField = "Description";
            CustomFeesDropDown.DataBind();

            SetTab(ViewContractTab.CustomFees);            
        }

        /// <summary>
        /// Contains JQuery script to toggle panels.
        /// </summary>
        /// <returns></returns>
        public static string GetPageStartupScript()
        {
            return "";
        }    

        protected void AddCustomFee(object sender, EventArgs e)
        {
            if (CustomFeesDropDown.SelectedItem != null) {
                var customFeeId = Convert.ToInt32(CustomFeesDropDown.SelectedItem.Value);

                var response = ContractService.AddCustomFee(Contract.ContractId, customFeeId);
                if (response.IsSuccessful) {  
                    CustomFees.LoadCustomFees(Contract.ContractId);
                }
                else {
                    UserMessage.SetFailure(response.Message);
                }
            }
        }

        protected void DeleteContract(object sender, EventArgs e)
        {
            var response = ScrubbingService.ScrubContract(Contract.Contract);

            if (response.IsSuccessful) {
                NavigationController.ContractSearch();
            } else {
                UserMessage.SetFailure(response.Message);
            }
        }
    }
}