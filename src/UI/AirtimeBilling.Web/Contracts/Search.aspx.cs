﻿using System;
using System.Web.UI.WebControls;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using System.Web.UI;

namespace AirtimeBilling.Web
{
    public partial class ContractSearchPage : BackOfficeBasePage
    {
        private string _EditNavigateUrl;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            
            _EditNavigateUrl = "~/Contracts/ViewContract.aspx";

            if (!IsPostBack) {
                ddlSearchField.DataSource = Enum<ContractSearchField>.GetAllDescriptions();
                ddlSearchField.DataBind();
            }            
        }

        protected void gvContracts_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            if (args.HasKey) {
                CurrentSession.SearchReadOnly = false;
                Response.Redirect(_EditNavigateUrl + "?cid=" + args.Key);
            }
        }

        /// <summary>
        /// Gets or sets the edit navigate URL.
        /// </summary>
        /// <value>The edit navigate URL.</value>
        public string EditNavigateUrl
        {
            get { return _EditNavigateUrl; }
            set { _EditNavigateUrl = value; }
        }

        /// <summary>
        /// Handles the RowCommand event of the gvContracts control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void gvContracts_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Set in Session to prevent changing in URL 
            switch (e.CommandName)
            {
                case "Edit":
                    CurrentSession.SearchReadOnly = false;
                    Response.Redirect(_EditNavigateUrl + "?cid=" + e.CommandArgument);
                    break;

                case "View":
                    CurrentSession.SearchReadOnly = true;                    
                    Response.Redirect(_EditNavigateUrl + "?cid=" + e.CommandArgument);
                    break;

                default:
                    CurrentSession.SearchReadOnly = false;
                    break;
            }
        }



        /// <summary>
        /// Gets or sets the search filter.
        /// </summary>
        /// <value>The search filter.</value>
        protected SearchParameters SearchParams
        {
            get { return ViewState["SearchParameters"] as SearchParameters; }
            set { ViewState["SearchParameters"] = value; }
        }

        /// <summary>
        /// Binds the data.
        /// </summary>
        private void BindData(SearchParameters searchParameters)
        {
            try
            {
                var service = ServiceFactory.GetService<IContractService>();

                var response = service.FindContracts(searchParameters.GetSearchRequest());
                if (response.IsSuccessful)
                {
                    var contracts = response.Contracts;
                    gvContracts.DataSource = contracts;
                    gvContracts.DataBind();
                }
                else
                {
                    // TODO: display message           
                    ClientSideUtils.CreateMessageAlert(this, response.Message, "requestError");
                }
            } catch (TypeInitializationException ex)
            {
                CurrentSession.ExceptionMessage = ex.InnerException.ToString();                
                Response.Redirect("~/Error/ErrorDetail.aspx");
            }

        }
        /// <summary>
        /// Filters the URL.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="currentPage">The current page.</param>
        protected string FilterUrl(object filter, string currentPage)
        {
            string f = (string)filter;
            string url = Page.TemplateControl.AppRelativeVirtualPath;
            if (!String.IsNullOrEmpty(f))
            {
                url = string.Format("{0}?Filter={1}", url, f);
            }
            return ResolveUrl(url);
        }

        /// <summary>
        /// Handles the RowDataBound event of the gvContracts control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvContracts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the gvContracts control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void gvContracts_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvContracts.PageIndex = e.NewPageIndex;
            BindData(SearchParams);
        }

        [Serializable]
        protected class SearchParameters
        {
            private readonly SearchContractStatusType statusType;
            private readonly ContractStatus contractStatus;
            private readonly ContractSearchField searchType;
            private readonly string searchValue;

            public SearchParameters(SearchContractStatusType statusType, ContractStatus contractStatus,
                ContractSearchField searchType, string searchValue)
            {
                this.statusType = statusType;
                this.contractStatus = contractStatus;
                this.searchType = searchType;
                this.searchValue = searchValue;
            }

            public SearchContractStatusType StatusType
            {
                get
                {
                    return statusType;
                }
            }

            public ContractStatus ContractStatus
            {
                get { return contractStatus; }
            }

            public ContractSearchField SearchType
            {
                get { return searchType; }
            }

            public string SearchValue
            {
                get { return searchValue; }
            }

            public ContractSearchRequest GetSearchRequest()
            {
                var searchRequest = new ContractSearchRequest
                                        {
                                            SearchContractStatusType = StatusType,
                                            SearchValue = SearchValue,
                                            SearchType = SearchType,
                                            SearchStatus = ContractStatus,
                                            User = Users.Current
                                        };
                return searchRequest;
            }
        }

        protected void gvContracts_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void SearchContracts(object sender, EventArgs e)
        {
            var searchType = Enum<ContractSearchField>.GetEnumFromDescription(ddlSearchField.SelectedValue);
            SearchContractStatusType statusType;
            if (ddlStatus.SelectedValue == "-1")
            {
                statusType = SearchContractStatusType.All;
            }
            else if (ddlStatus.SelectedValue == "0")
            {
                statusType = SearchContractStatusType.Active;
            }
            else
            {
                statusType = SearchContractStatusType.UseStatus;
            }
            var status = ContractStatus.Active;
            if (int.Parse(ddlStatus.SelectedValue) > 0)
            {
                status = Enum<ContractStatus>.Parse(ddlStatus.SelectedValue);
            }

            var searchParams = new SearchParameters(statusType, status, searchType, txtSearch.Text);

            SearchParams = searchParams;
            BindData(SearchParams);
        }
    }
}