﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web.Contracts
{
    public partial class SelectAccountPage : BasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            SearchResultsPanel.Visible = false;
            SearchResult.Clear();
            if (!IsPostBack)
            {
                CurrentSession.Account = null;
            }
        }

        protected void txtAccountNumber_TextChanged(object sender, EventArgs e)
        {
            if (txtAccountNumber.Text.Length > 0)
            {
                try
                {
                    Account account = AccountService.GetAccountByAccountNumber(txtAccountNumber.Text);

                    if (account != null)
                    {                        
                        SearchResultsPanel.Visible = true;
                        lblAccountNumber.Text = account.AccountNumber;
                        lblAccountName.Text = account.AccountName;
                        CurrentSession.Account = account;
                    }
                    else
                    {                     
                        SearchResultsPanel.Visible = false;
                        SearchResult.SetInformation("Could not find Account");
                    }
                }
                catch (Exception)
                {                    
                    SearchResultsPanel.Visible = false;
                    SearchResult.SetFailure("Error finding Account");
                }
            }
        }        

        protected void ExistingAccountContractButton_Click(object sender, EventArgs e)
        {
            if (CurrentSession.Account != null)
            {
                Response.Redirect("~/Contracts/SubmitAccountContract.aspx");
            }
            else
            {
                Response.Redirect("~/Contracts/SubmitContract.aspx");
            }
        }

        protected void NeAccountContractButton_Click(object sender, EventArgs e)
        {
            CurrentSession.Account = null;
            Response.Redirect("~/Contracts/SubmitContract.aspx");
        }
    }
}
