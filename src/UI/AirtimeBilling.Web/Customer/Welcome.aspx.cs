﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class CustomerWelcomePage : BasePage
    {
        private enum CustomerTab
        {
            Details,
            UnbilledCallData, 
            PreviousInvoices,            
        }

        protected override void OnLoad(EventArgs e)
        {
            Security.RedirectToWelcomeIfNotInRole(Roles.Customer);

            base.OnLoad(e);

            UserMessage.Clear();

            if (!IsPostBack)
            {
                var user = Users.GetUser(HttpContext.Current.User.Identity.Name);
                var contactUser = user as ContactUser;

                if (contactUser != null) {
                    var contact = ContactService.GetContact(contactUser.ContactId);
                    if (contact != null) {
                        Session["Contact"] = contact;
                        PopulateContact(contact);
                    }
                    else {
                        throw new InvalidOperationException("Invalid Contact Id. Cannot Continue.");
                    }
                }
                else {
                    throw new InvalidOperationException("Cannot login to Customer section. Not a valid Customer");
                }
            }
        }

        protected void SaveCustomer(object sender, ImageClickEventArgs e)
        {
            var request = new SaveContactRequest
            {
                Contact = Contact.Contact,
                User = Users.Current
            };

            var response = ContactService.SaveContact(request);
            if (response.IsSuccessful)
            {
                UserMessage.SetSuccess("Contact Saved");
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void PopulateContact(Contact contact)
        {
            Contact.Contact = contact;
        }

        private void SetTab(CustomerTab tab)
        {
            UnbilledCallDataPanel.Visible = tab == CustomerTab.UnbilledCallData;
            DetailsPanel.Visible = tab == CustomerTab.Details;
            InvoicesPanel.Visible = tab == CustomerTab.PreviousInvoices;            
        }

        protected void ShowPreviousInvoices(object sender, ImageClickEventArgs e)
        {
            SetTab(CustomerTab.PreviousInvoices);

            // Do invoice load here.
            var contact = Session["Contact"] as Contact;
            if (contact != null) {
                lblInvoicesContactName.Text = contact.Name;
            }
        }

        protected void ViewCallData(object sender, ImageClickEventArgs e)
        {
            SetTab(CustomerTab.UnbilledCallData);

            // Do invoice load here.
            var contact = Session["Contact"] as Contact;
            if (contact != null)
            {
                lblContactName.Text = contact.Name;
            }

            // Load the call data.
            //CallData.ContractId = Contract.ContractId;
        }

        protected void DetailsView(object sender, ImageClickEventArgs e)
        {
            SetTab(CustomerTab.Details);
        }
    }
}