﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.CustomerWelcomePage" Codebehind="Welcome.aspx.cs" %>
<%@ Register Src="~/UserControls/Contact.ascx" TagName="contact" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/CallDataViewer.ascx" TagName="calldata" TagPrefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Your Details</h1>
<asp:Panel ID="pnlUserActions" runat="server" CssClass="submenu">                
    <sharp:DynamicHoverImageButton ID="DetailsButton" runat="server" Text="Details"  onclick="DetailsView" CausesValidation="false" />
    <sharp:DynamicHoverImageButton ID="CallDataButton" runat="server" Text="Unbilled Calls"  onclick="ViewCallData" CausesValidation="false" />
    <sharp:DynamicHoverImageButton ID="PreviousInvoicesButton" runat="server" Text="Previous Invoices"  onclick="ShowPreviousInvoices" CausesValidation="false" />        
</asp:Panel>
<sharp:MessagePanel ID="UserMessage" runat="server" />
<asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" runat="server" ShowSummary="True"  />
<asp:Panel ID="DetailsPanel" runat="server">
    <div class="Form">
    
        <sharp:contact ID="Contact" runat="server" />
    
         <div class="crudbarfloat">                                
            <sharp:DynamicHoverImageButton ID="lnkSave" runat="server" Text="Save" Icon="disk"  onclick="SaveCustomer" />             
        </div>
        
    </div>
</asp:Panel>    
<asp:Panel ID="UnbilledCallDataPanel" runat="server" Visible="false">        
    <div class="Form">
    
        <sharp:FormGroup ID="ContactFG" runat="server" GroupingText="Contact" CssClass="full">                
            <div class="control-group">
                <label class="control-label" for="<%= lblContactName.ClientID %>" class="rowheader">Contact Name</label>
                <asp:Label ID="lblContactName" runat="server" Text=""></asp:Label>                        
            </div>            
        </sharp:FormGroup>
        <div class="clear" style="width:700px;margin:auto;font-size:1.2em;">Provide ability to select Contract to calls for from either a list or a drop down.</div>
        <sharp:calldata ID="CallData" runat="server" />
    </div>                                 
</asp:Panel>
<asp:Panel ID="InvoicesPanel" runat="server" Visible="false">        
    <div class="Form">    
        <sharp:FormGroup ID="InvoicingContactFG" runat="server" GroupingText="Contact" CssClass="full">                
            <div class="control-group">
                <label class="control-label" for="<%= lblInvoicesContactName.ClientID %>" class="rowheader">Contact Name</label>
                <asp:Label ID="lblInvoicesContactName" runat="server" Text=""></asp:Label>                        
            </div>            
        </sharp:FormGroup>       
        <div style="width:700px;margin:auto;font-size:1.2em;">Invoice Panel same as existing Invoice Panel goes here.  Containing all Invoices for this Customer/Contact based on the Accounts the contact controls.  Must be the Contact on a Invoice Account.</div>
    </div>                                 
</asp:Panel>


</asp:Content>

