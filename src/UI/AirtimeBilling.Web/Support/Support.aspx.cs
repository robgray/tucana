﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.UI;
using AirtimeBilling.Common;
using AirtimeBilling.Logging;

namespace AirtimeBilling.Web
{
    public partial class SupportPage : BackOfficeBasePage
    {
        private static List<ResetCommand> DeleteCommands;
        private static List<ResetCommand> SystemSettingCommands;

        public int NumberOfCalls;

        protected override void OnLoad(EventArgs e)
        {
            Info.SetWarning("This page is intended for use by System Support staff to diagnose problems.");
            NumberOfCalls = GetNumberOfCallDataRows();
        }
        
        protected string ConnectionString
        {
            get { return ConfigItems.ConnectionString;  }
        }        

        protected void ExceptionEmail(object sender, EventArgs e)
        {
            LoggingUtility.SendEmail(ConfigItems.ErrorEmailAddress, "Airtime Billing Support", "Test Exception",
                                     "TEST EXCEPTION", true);
        }

        protected void NewContractEmail(object sender, EventArgs e)
        {
            LoggingUtility.SendEmail(ConfigItems.NewContractEmailAddress, "Airtime Billing", "Test Email", "Test Email Msg from Airtime Billing", false);
        }

        protected void SystemEmail(object sender, EventArgs e)
        {
            LoggingUtility.SendEmail(ConfigItems.SystemEmailAddress, "Airtime Billing", "Test: Please Ignore", "Test from AirtimeBilling", false);
        }

        protected void LogException(object sender, EventArgs e)
        {
            LoggingUtility.LogException(new Exception("Test Exception"));            
        }

        protected void LogDebug(object sender, EventArgs e)
        {
            LoggingUtility.LogDebug("Test", "Test", "Test Debug");
        }

        protected void LogWarning(object sender, EventArgs e)
        {
            LoggingUtility.LogWarning("Test", "Test", "Test Warning");
        }

        protected void ClearInvoicing(object sender, EventArgs e)
        {
            PerformReset(ResetLevel.Invoicing);
        }

        protected void ClearInvoicingAndCalls(object sender, EventArgs e)
        {
            PerformReset(ResetLevel.InvoicingAndCalls);
        }

        protected void ClearContracts(object sender, EventArgs e)
        {
            PerformReset(ResetLevel.ContractsAndCustomers);
        }

        protected void ClearDatabase(object sender, EventArgs e)
        {
            PerformReset(ResetLevel.All);
        }

        private void PerformReset(ResetLevel level)
        {
            using (var cn = new SqlConnection(ConfigItems.ConnectionString)) {
                cn.Open();
                var trans = cn.BeginTransaction("CleanTransaction");

                try
                {
                    foreach (var delete in GetDeleteCommands(level))
                    {
                        var cmd = delete.Command;
                        cmd.Connection = cn;
                        cmd.Transaction = trans;
                        
                        cmd.ExecuteNonQuery();
                    }

                    // reset the system settings
                    foreach (var setting in GetSystemSettingCommands(level))
                    {
                        var cmd = setting.Command;
                        cmd.Connection = cn;
                        cmd.Transaction = trans;

                        cmd.ExecuteNonQuery();
                    }

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    try
                    {
                        trans.Rollback();
                    }
                    catch { }
                    throw ex;
                }
            }

            NumberOfCalls = GetNumberOfCallDataRows();       
            Info.SetSuccess("Database has been Reset : " + Enum<ResetLevel>.GetDescription(level) + " cleared");
        }

        protected int GetNumberOfCallDataRows()
        {
            using (var cn = new SqlConnection(ConfigItems.ConnectionString)) {
                
                var cmd = new SqlCommand("SELECT Count(*) FROM Call", cn);
                cn.Open();

                return (int)cmd.ExecuteScalar();                
            }
        }

        private enum ResetLevel
        {
            [EnumDescription("Invoicing tables")]
            Invoicing,
            [EnumDescription("Invoicing and Call tables")]
            InvoicingAndCalls,
            [EnumDescription("Contracts, Customers, and Invoices.")]
            ContractsAndCustomers,
            [EnumDescription("All database tables")]
            All                        
        }

        private class ResetCommand
        {
            private string _command;
            private ResetLevel _level;

            public ResetCommand(string command) : this(command, ResetLevel.All) { }

            public ResetCommand(string command, ResetLevel level)
            {
                _command = command;
                _level = level;
            }

            public ResetLevel Level 
            { 
                get { return _level; }
            }
            
            public SqlCommand Command
            {
                get { return new SqlCommand(_command); }
            }
        }

        private static List<ResetCommand> GetDeleteCommands(ResetLevel level)
        {
            if (DeleteCommands == null) {

                // Order is important. Must allow for referential integrity.  No Deletes are cascading.
                DeleteCommands = new List<ResetCommand>
                                     {
                                         new ResetCommand("DELETE FROM [Log]", ResetLevel.Invoicing),
                                         new ResetCommand("DELETE FROM AccountInvoice", ResetLevel.Invoicing),
                                         new ResetCommand("DELETE FROM InvoiceLineItem", ResetLevel.Invoicing),
                                         new ResetCommand("DELETE FROM InvoiceHeader", ResetLevel.Invoicing),
                                         new ResetCommand("UPDATE Call SET DateInvoiced = NULL", ResetLevel.Invoicing),
                                         new ResetCommand("UPDATE Account SET AmountPaid = 0, PreviousBill = 0", ResetLevel.Invoicing),
                                         new ResetCommand("DELETE FROM Call", ResetLevel.InvoicingAndCalls),
                                         new ResetCommand("DELETE FROM ContractPlanVariance", ResetLevel.ContractsAndCustomers),
                                         new ResetCommand("DELETE FROM Contract", ResetLevel.ContractsAndCustomers),                                                                           
                                         new ResetCommand("DELETE FROM Agent", ResetLevel.ContractsAndCustomers),                                         
                                         new ResetCommand("DELETE FROM Account", ResetLevel.ContractsAndCustomers),
                                         new ResetCommand("DELETE FROM Contact", ResetLevel.ContractsAndCustomers),
                                         new ResetCommand("DELETE FROM ActivityLog", ResetLevel.ContractsAndCustomers),
                                         new ResetCommand("DELETE FROM Audit", ResetLevel.ContractsAndCustomers),
                                         new ResetCommand("DELETE FROM VizadaData", ResetLevel.InvoicingAndCalls),
                                         new ResetCommand("DELETE FROM SatComData", ResetLevel.InvoicingAndCalls),                                         
                                         new ResetCommand("DELETE FROM Company", ResetLevel.ContractsAndCustomers),                                  
                                         new ResetCommand("DELETE FROM ContractCustomFee", ResetLevel.ContractsAndCustomers),                                  
                                         new ResetCommand("DELETE FROM NetworkTariff"),
                                         new ResetCommand("DELETE FROM Network"),
                                         new ResetCommand("DELETE FROM Tariff"),
                                         new ResetCommand("DELETE FROM [Plan]"),                                         
                                         new ResetCommand("DELETE FROM CustomFee"),                                  
                                     };

            }

            if (level == ResetLevel.All)
                return DeleteCommands;

            return DeleteCommands.FindAll(cmd => cmd.Level <= level);
        }

        private static List<ResetCommand> GetSystemSettingCommands(ResetLevel level)
        {
            if (SystemSettingCommands == null) {
                SystemSettingCommands = new List<ResetCommand>
                                            {
                                                new ResetCommand(
                                                    "UPDATE SystemSettings SET KeyValue='1' WHERE SystemSettingId=1",
                                                    ResetLevel.Invoicing),
                                                new ResetCommand(
                                                    "UPDATE SystemSettings SET KeyValue='1' WHERE SystemSettingId=2"),
                                                new ResetCommand(
                                                    "UPDATE SystemSettings SET KeyValue='1' WHERE SystemSettingId=3"),
                                                new ResetCommand(
                                                    "UPDATE SystemSettings SET KeyValue='2001-01-01' WHERE SystemSettingId=4",
                                                    ResetLevel.Invoicing),
                                                new ResetCommand(
                                                    "UPDATE SystemSettings SET KeyValue='1' WHERE SystemSettingId=5",
                                                    ResetLevel.Invoicing),                                                
                                            };
            }

            if (level == ResetLevel.All)
                return SystemSettingCommands;

            return SystemSettingCommands.FindAll(cmd => cmd.Level <= level);
        }
    }
}
