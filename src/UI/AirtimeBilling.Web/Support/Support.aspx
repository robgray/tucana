<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Support.aspx.cs" Inherits="AirtimeBilling.Web.SupportPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<sharp:MessagePanel runat="server" ID="Info" IncludeClose="False" />
<div class="Form">
<div class="Row">
    <sharp:FormGroup ID="ConfigurationInfo" runat="server" GroupingText="Configuration Information" CssClass="full">        
    <div class="control-group">
            <label>Database Connection String</label>
            <span><%= ConnectionString %></span>
        </div>                
    </sharp:FormGroup>
</div>
<div class="Row">
    <sharp:FormGroup ID="EmailTesting" runat="server" GroupingText="Email Testing" CssClass="well">        
        <div class="control-group">
            <label><asp:LinkButton CssClass="btn" ID="DynamicHoverImageButton1" runat="server" Text="Send Exception Email" OnClick="ExceptionEmail" /></label>            
        </div>
        <div class="control-group">            
            <label><asp:LinkButton CssClass="btn" ID="DynamicHoverImageButton2" runat="server" Text="Send New Contract Email" OnClick="NewContractEmail" /></label>            
        </div>
        <div class="control-group">            
            <label><asp:LinkButton CssClass="btn" ID="DynamicHoverImageButton3" runat="server" Text="Send System Email" OnClick="SystemEmail" /></label>            
        </div>        
    </sharp:FormGroup>
    <sharp:FormGroup ID="Logging" runat="server" GroupingText="Logging" CssClass="well">        
        <div class="control-group">            
            <label><asp:LinkButton CssClass="btn" ID="LogExceptionButton" runat="server" Text="Log Exception" OnClick="LogException" /></label>
        </div>
        <div class="control-group">            
             <label><asp:LinkButton CssClass="btn" ID="LogDebugButton" runat="server" Text="Log Debug" OnClick="LogDebug" /></label>            
        </div>
        <div class="control-group">
             <label><asp:LinkButton CssClass="btn" ID="LogWarningButton" runat="server" Text="Log Warning" OnClick="LogWarning" /></label>
        </div>
    </sharp:FormGroup>
</div>
<div class="Row">
    <sharp:FormGroup ID="TestingReset" runat="server" GroupingText="Testing Info" CssClass="well">        
        <div class="control-group">
            <label>Call Data Rows</label>
            <span><%= NumberOfCalls %></span>
        </div>
        <div class="control-group">
            <label><b>Clear</b></label>
        </div>
        <div class="control-group">                        
            <label><asp:LinkButton CssClass="btn" ID="ClearInvoicingButton" runat="server" Text="Invoicing" OnClick="ClearInvoicing" /></label>            
            <span style="margin-bottom: 10px"><b>Use this when testing invoicing</b><br />Clears all invoicing data. Customers/Contracts and Call data remain in the system.</span>            
        </div>        
        <div class="control-group">                        
            <label><asp:LinkButton CssClass="btn" ID="ClearInvoicesAndCallsButton" runat="server" Text="Inv & Calls" OnClick="ClearInvoicingAndCalls" /></label>            
            <span style="margin-bottom: 10px"><b>Use this when testing invoicing</b><br />Clears all invoicing related data including calls and invoices.  Customers/Contracts etc remain in the system.</span>            
        </div>        
        <div class="control-group" style="margin-bottom:20px">                        
            <label><asp:LinkButton CssClass="btn" ID="ClearContractsButton" runat="server" Text="Contracts" OnClick="ClearContracts" /></label>            
            <span style="margin-bottom: 10px"><b>Use this to clear everything but Networks and Plans and associated Tariffs</b><br />Clears all Contract data including accounts, companies, contacts, invoices.  Does not delete Networks and Plans.</span>            
        </div>                
        <div class="control-group" style="margin-bottom:20px">                        
            <label><asp:LinkButton CssClass="btn" ID="ClearDatabaseButton" runat="server" Text="Database" OnClick="ClearDatabase" /></label>            
            <span style="margin-bottom: 10px"><b>Use this to completely clear the system</b><br />Clears all data including Customers/Contracts, Networks, Plans, Tariffs, and more.</span>            
        </div>                
    </sharp:FormGroup>
</div>

</div>
<br class="clear" />
</asp:Content>
