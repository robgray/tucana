﻿///<reference path="jquery-1.2.6.js" />

var AddressManager = {

    Copy: function(sourceCtrl, destCtrl) {

        var sourceStreet = $('#' + sourceCtrl + '_txtStreet');
        var sourceStreet2 = $('#' + sourceCtrl + '_txtStreet2');
        var sourceSuburb = $('#' + sourceCtrl + '_txtSuburb');
        var sourceState = $('#' + sourceCtrl + '_txtState');
        var sourcePostcode = $('#' + sourceCtrl + '_txtPostcode');

        var destStreet = $('#' + destCtrl + '_txtStreet');
        var destStreet2 = $('#' + destCtrl + '_txtStreet2');
        var destSuburb = $('#' + destCtrl + '_txtSuburb');
        var destState = $('#' + destCtrl + '_txtState');
        var destPostcode = $('#' + destCtrl + '_txtPostcode');


        destStreet.val(sourceStreet.val());
        destStreet2.val(sourceStreet2.val());
        destSuburb.val(sourceSuburb.val());
        destState.val(sourceState.val());
        destPostcode.val(sourcePostcode.val());

        return false;
    }

}
