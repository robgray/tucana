﻿///<reference path="jquery-1.2.6.js" />

// Some jquery helpers to make life easier.

// from: http://john-sheehan.com/blog/custom-jquery-selector-for-aspnet-webforms/
String.prototype.endsWith = function(str) {
    return (this.match(str + '$') == str);
}

jQuery.extend(
    jQuery.expr[":"],
    {
        asp: "jQuery(a).attr('id').endsWith(m[3]);"
    }
);
// end from