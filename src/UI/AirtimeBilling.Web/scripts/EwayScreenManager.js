﻿//<reference path="jquery-1.2.6.js" />

var EwayManager = {

    Init: function(tabs) {

        $('#' + tabs[0].Button).click(function() {
            $('#' + tabs[0].Tab).show();
            $('#' + tabs[1].Tab).hide();
        });

        $('#' + tabs[1].Button).click(function() {
            $('#' + tabs[0].Tab).hide();
            $('#' + tabs[1].Tab).show();
        });
    }
}