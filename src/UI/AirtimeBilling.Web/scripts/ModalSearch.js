﻿/**********************************
** Modal search related functions
***********************************/
function mouseSearchHover(item) {        
    $common.removeCssClasses(item, ["searchItem"]);
    $common.addCssClasses(item, ["searchItemHighlight"]);
}
function mouseSearchUnHover(item) {
    $common.removeCssClasses(item, ["searchItemHighlight"]);
    $common.addCssClasses(item, ["searchItem"]);
}