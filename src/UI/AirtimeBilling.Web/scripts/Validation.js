﻿///<reference path="jquery-1.2.6.js" />


function ValidateCheckboxSelected(val)
{
    var control = $(val.controltovalidate);
    var mustBeChecked = Boolean(val.mustBeChecked == 'true');

    return control.checked == mustBeChecked;
}