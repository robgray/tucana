﻿///<reference path="jquery-1.2.6.js" />

//function InvoiceRun() {
//    var lastInvoiceDate = $get('lblLastInvoiceDate').innerText;    
//    var invoiceDate = $get('txtInvoiceDate').value;
//    alert(invoiceDate);
//    var popup = $find('mdlPopup');
//    if (popup != null) popup.show();
//    PageMethods.PerformInvoiceRun(lastInvoiceDate, invoiceDate, this.OnSuccessful, this.OnError);
//} 

//function OnSuccessful() {   
//    var popup=$find('mdlPopup');
//    if (popup != null) popup.hide();
//}

//function OnError(error) {
//    var popup = $find('mdlPopup');
//    if (popup != null) popup.hide();
//    alert(error.get_message());
//}

//function SelectInvoice(parentid, value) {
//    
//    $('input:checkbox[id*=chkSelect]').attr('checked', value);

//}

var InvoiceRunManager = {

    Init: function(lastCtrl, invoiceDateCtrl, submit, calldatacheck) {

        var button = $('#' + submit);
        button.click(function() {

            $('#InvoiceFail').hide();

            var lastInvoiceDate = $('#' + lastCtrl).text();
            var invoiceDate = $('#' + invoiceDateCtrl).val();

            if (lastInvoiceDate == '') lastInvoiceDate = '1900-01-01';

            var popup = $find('mdlPopup');
            if (popup != null) popup.show();

            PageMethods.PerformInvoiceRun(lastInvoiceDate, invoiceDate,
                function(runNumber) {

                    var popup = $find('mdlPopup');
                    if (popup != null) popup.hide();

                    window.location = location.protocol + "//" + location.hostname + "/BackOffice/Invoicing/InvoiceRun.aspx?Code=SA2342";

                }, function(error) {
                    var popup = $find('mdlPopup');
                    if (popup != null) popup.hide();

                    $('#InvoiceFail').show();
                    $('#FailMessage').text(error.get_message());
                });

            return false;
        });

        var calldatabutton = $('#' + calldatacheck);
        calldatabutton.click(function() {
        
            var popup = $find('mdlPopupCheck');
            if (popup != null) popup.show();

            PageMethods.CheckForNewCallsToImport(
                    function() {

                        var popup = $find('mdlPopupCheck');
                        if (popup != null) popup.hide();

                    }, function(error) {
                        var popup = $find('mdlPopupCheck');
                        if (popup != null) popup.hide();
                        
                        $('#FailMessage').text(error.get_message());
                    });

            return false;   
        });
    },

    SelectInvoice: function(parentid, value) {

        $('input:checkbox[id*=chkSelect]').attr('checked', value);

    }
}

function changeSearch(searchType) {
    if (searchType.value == 1) {
        $('#RunNumberSearch').show();
        $('#InvoiceNumberSearch').hide();
    }
    else {
        $('#RunNumberSearch').hide();
        $('#InvoiceNumberSearch').show();
    }
}