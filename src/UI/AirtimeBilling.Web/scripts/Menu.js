﻿///<reference path="jquery-1.2.6.js" />

var Menu = {

    Selected: function(menuItem) {
        $(menuItem).addClass('menuItemHighlight');
        $(menuItem).removeClass('menuItem');
    },

    UnSelected: function(menuItem) {
        $(menuItem).removeClass('menuItemHighlight');
        $(menuItem).addClass('menuItem');
    },

    Click: function(menuItem) {
        var location = $(menuItem).find("a")[0].href;
        window.location.replace(location);        
    }
}