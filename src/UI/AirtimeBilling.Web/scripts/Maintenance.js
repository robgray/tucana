﻿///<reference path="jquery-1.2.6.js" />

//***********************************************
// This is a recursive function to extract the inner input node from a given node. If 
// checkNestedContent is true, then the function ercursively searches all child nodes until
// an INPUT is found. If we supply an ignoreNode, then the routine will only return an INPUT
// node that is NOT the ignoreNode, effectively ignoring the supplied node if found. This is kind
// of a hack to enable us to iterate through a RadioItemList where it renders 2 input types. In the
// initiall call, we want the first input node to see if its value is what we want, if not we make
// a second call to locate the next input, but passing in the previous input the routine found
// as the ignore parameter.
function GetInputChildNode(node, checkNestedContent, ignoreNode, excludeHiddenControls) {
    var childNode = null;
    var t = new String(typeof (node.childNodes[c]));
    var typ = t.toLowerCase();
    for (var c = 0; c < node.childNodes.length; c++) {
        if (checkNestedContent == true && typeof (node.childNodes[c]) != 'undefined' && node.childNodes[c].childNodes.length > 0) {
            var innerChildNode = GetInputChildNode(node.childNodes[c], true, ignoreNode, excludeHiddenControls);
            if (innerChildNode != null && innerChildNode != ignoreNode)
                return innerChildNode;
        }

        if (typ == 'input' || (typeof (node.childNodes[c].tagName) != 'undefined' && node.childNodes[c].tagName == 'INPUT')) {
            if ((excludeHiddenControls != true) || (excludeHiddenControls == true && node.childNodes[c].type != 'hidden')) {
                if (node.childNodes[c] != ignoreNode) {
                    childNode = node.childNodes[c];
                    //data = item.childNodes[c].value;
                    break;
                }
            }
        }
    }
    return childNode;
}

function isTrue(condition)
{
    return condition == 'True' ? true : false;
}