﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.CallDataViewerControl" Codebehind="CallDataViewer.ascx.cs" %>
<div>
    <asp:GridView ID="gvCallData" runat="server" AutoGenerateColumns="False" SkinID="NoHoverGrid">        
    <Columns>        
        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
        <asp:BoundField DataField="CallTime" HeaderText="Call Time" Visible="True" 
            DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="CallTime">
            <ItemStyle Width="120px" />
            <HeaderStyle Width="120px" />
        </asp:BoundField>
        <asp:BoundField DataField="NumberCalled" HeaderText="Number" Visible="True">
            <ItemStyle Width="100px" />
            <HeaderStyle Width="100px" />
        </asp:BoundField>
        <asp:BoundField DataField="CalledFrom" HeaderText="Called From" Visible="True">
            <ItemStyle Width="100px" />
            <HeaderStyle Width="100px" />
        </asp:BoundField>            
        <asp:BoundField DataField="Tariff" HeaderText="Tariff" Visible="True">
            <ItemStyle Width="200px" />
            <HeaderStyle Width="200px" />
        </asp:BoundField>
        <asp:BoundField DataField="DisplayVolume" HeaderText="Volume" Visible="True">
            <ItemStyle Width="70px" />
            <HeaderStyle Width="70px" />
        </asp:BoundField>
        <asp:BoundField DataField="Cost" HeaderText="Cost" Visible="True" DataFormatString="{0:c}">
            <ItemStyle Width="70px" />
            <HeaderStyle Width="70px" />
        </asp:BoundField>
    </Columns>            
    <EmptyDataTemplate>        
        <div class="alert alert-info">            
            No unpaid calls have been made
        </div>
    </EmptyDataTemplate>
    </asp:GridView>
</div>
