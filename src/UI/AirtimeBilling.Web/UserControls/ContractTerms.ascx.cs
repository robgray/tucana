﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    public partial class ContractTerms : BaseUserControl
    {
        public event EventHandler ContractSubmitted;
               
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack) {
                
                try
                {
                    txtTermsConditions.Text = System.IO.File.ReadAllText(Server.MapPath("../terms.txt"));
                    txtTermsConditions.Visible = true;
                }
                catch (Exception)
                {
                    txtTermsConditions.Visible = false;                                      
                }
            }
        }

        protected void SubmitNewContract(object sender, EventArgs e)
        {
            OnContractSubmitted(e);
        }

        protected void OnContractSubmitted(EventArgs e)
        {
            if (ContractSubmitted != null) {
                ContractSubmitted(this, e);
            }
        }
    }
}