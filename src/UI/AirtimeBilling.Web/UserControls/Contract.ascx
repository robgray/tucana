﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ContractControl" Codebehind="Contract.ascx.cs" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="address" TagPrefix="sharp" %>

<div class="row">
    <sharp:FormGroup ID="PlanFG" runat="server" GroupingText="Plan Information" CssClass="span6">    
        <div class="control-group">
            <label class="control-label"  for="<%= lblContractNumber.ClientID %>">Contract Number</label>
            <div class="controls">
                <asp:Label ID="lblContractNumber" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= lblContractStatus.ClientID %>">Contract Status</label>
            <div class="controls">
                <asp:Label ID="lblContractStatus" runat="server" Text=""></asp:Label>
             </div>
        </div>
        
        <asp:UpdatePanel ID="apnlNetwork" runat="server" UpdateMode="Conditional">
            <ContentTemplate>           
                <div class="control-group">
                    <label class="control-label">Network</label>                    
                    <div class="controls">
                        <asp:DropDownList ID="ddlNetwork" runat="server" 
                            onselectedindexchanged="NetworkChange" AutoPostBack="True" >
                        </asp:DropDownList>
                        <asp:Label ID="NetworkLabel" runat="server" />
                    </div>
                </div>                     
                <div class="control-group">
                    <label class="control-label" for="<%= ddlPlan.ClientID %>">Plan</label>                    
                    <div class="controls">
                        <asp:DropDownList ID="ddlPlan" runat="server" Width="270px" 
                            onselectedindexchanged="ConfigureForNewPlan" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="<%= txtActivationDate.ClientID %>">Activation Date</label>                    
                    <div class="controls">
                        <asp:TextBox ID="txtActivationDate" runat="server" Width="110px"></asp:TextBox>
                        <asp:Image ID="imgActivationDate" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
                        <atk:CalendarExtender ID="txtActivationDate_CalendarExtender" runat="server" 
                            TargetControlID="txtActivationDate" 
                            PopupButtonID="imgActivationDate"
                            Format="dd/MM/yyyy">
                        </atk:CalendarExtender>                    
                        <span class="tip">Format is dd/mm/yyyy</span>
                        <asp:RequiredFieldValidator ID="rfvActivationDate" runat="server" 
                            ControlToValidate="txtActivationDate"  SetFocusOnError="True"
                            ErrorMessage="Missing Activation Date" Display="None" />
                        <asp:CompareValidator ID="cvtxtActivationDate" runat="server" ErrorMessage="Invalid Start Date - Expected format dd/mm/yyyy" 
                            Type="Date" Operator="DataTypeCheck" ControlToValidate="txtActivationDate"  
                            Text="" Display="None"></asp:CompareValidator>                                    
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="<%= txtEndDate.ClientID %>">End Date</label>                    
                    <div class="controls">
                        <asp:TextBox ID="txtEndDate" runat="server" Width="110px"></asp:TextBox>
                        <asp:Image ID="imgEndDate" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon"/>
                        <atk:CalendarExtender ID="txtEndDate_CalendarExtender" runat="server" 
                            Enabled="True" TargetControlID="txtEndDate"
                            PopupButtonID="imgEndDate"
                            Format="dd/MM/yyyy">
                        </atk:CalendarExtender>
                        <span class="tip">Format is dd/mm/yyyy</span>
                        <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" 
                            ControlToValidate="txtEndDate" ErrorMessage="Missing End Date" SetFocusOnError="True" Display="None"></asp:RequiredFieldValidator>                
                        <asp:CompareValidator ID="cvEndDate" runat="server" ErrorMessage="Invalid End Date - Expected format dd/mm/yyyy" 
                            Type="Date" Operator="DataTypeCheck" ControlToValidate="txtEndDate" 
                            Display="None"></asp:CompareValidator>                                    
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
            <asp:AsyncPostBackTrigger ControlID="ddlNetwork" EventName="SelectedIndexChanged" />                                       
            <asp:AsyncPostBackTrigger ControlID="ddlPlan" EventName="SelectedIndexChanged" />                    
        </Triggers> 
        </asp:UpdatePanel>
    </sharp:FormGroup>   

    <sharp:FormGroup ID="PhoneOptionsFG" runat="server" GroupingText="Phone Options" CssClass="span6">    
        <div class="control-group">
            <label class="control-label" for="<%= txtIMEINumber.ClientID %>">IMEI Number</label>            
            <div class="controls">
                <asp:TextBox ID="txtIMEINumber" runat="server" CssClass="EntryBox" MaxLength="20"></asp:TextBox>     
                <asp:RequiredFieldValidator ID="rfvIMEINumber" runat="server" 
                    ControlToValidate="txtIMEINumber" ErrorMessage="Missing IMEI Number" 
                    SetFocusOnError="True" Display="None" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtSimCard.ClientID %>">Sim Card</label>            
            <div class="controls">
                <asp:TextBox ID="txtSimCard" runat="server" CssClass="EntryBox" MaxLength="20"></asp:TextBox>            
            </div>
        </div>
        <div class="control-group">            
            <label class="control-label" for="<%= txtUsedBy.ClientID %>">Used By</label>            
            <div class="controls">
                <asp:TextBox ID="txtUsedBy" runat="server" MaxLength="30"></asp:TextBox>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= chkData.ClientID %>">Data</label>
            <div class="controls">
                <asp:CheckBox ID="chkData" runat="server" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label"  for="<%= chkMessageBank.ClientID %>">Message Bank</label>
            <div class="controls">
                <asp:CheckBox ID="chkMessageBank" runat="server" />
            </div>
        </div>
    </sharp:FormGroup>
</div>
<div class="row">
    <asp:Panel ID="pnlPhoneNumbers" runat="server" >
        <sharp:FormGroup ID="PhoneNumbersFG" runat="server" GroupingText="Phone Numbers" CssClass="span6">        
            <div class="control-group">
                <label class="control-label" for="<%= txtPhoneNumber1.ClientID %>">Phone Number 1</label>                
                <div class="controls">
                    <asp:TextBox ID="txtPhoneNumber1" runat="server" MaxLength="20"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= txtPhoneNumber2.ClientID %>">Phone Number 2</label>                
                <div class="controls">
                    <asp:TextBox ID="txtPhoneNumber2" runat="server" MaxLength="20"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= txtPhoneNumber3.ClientID %>">Phone Number 3</label>                
                <div class="controls">
                    <asp:TextBox ID="txtPhoneNumber3" runat="server" MaxLength="20"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= txtPhoneNumber4.ClientID %>">Phone Number 4</label>                
                <div class="controls">
                    <asp:TextBox ID="txtPhoneNumber4" runat="server" MaxLength="20"></asp:TextBox>
                </div>
            </div>
        </sharp:FormGroup>

        <sharp:FormGroup ID="PhoneDetailsFG" runat="server" GroupingText="Phone Security" style="height:132px" CssClass="span6">            
            <div class="control-group">
                <label class="control-label" for="<%= txtPin.ClientID %>">Pin</label>                
                <div class="controls">
                    <asp:TextBox ID="txtPin" runat="server" MaxLength="10"></asp:TextBox>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label"  for="<%= txtPuk.ClientID %>">Puk</label>                
                <div class="controls">
                    <asp:TextBox ID="txtPuk" runat="server" MaxLength="10"></asp:TextBox>
                </div>
            </div>
        </sharp:FormGroup>
    </asp:Panel>
 </div>