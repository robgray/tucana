﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.CompanyInfoReadOnlyControl" Codebehind="CompanyInfoReadOnly.ascx.cs" %>
<sharp:FormGroup ID="CompanyInfo" runat="server" GroupingText="Company Information" CssClass="well span12">    
    <table cellpadding="1" cellspacing="0" class="companyInfo">
        <tbody>
            <tr>
                <th style="width: 120px; padding-left: 5px; padding-right: 5px; text-align:right;" valign="top">                    
                    Company Name
                </th>
                <td style="width: 275px;" valign="top"><%= CompanyName %></td>
                <th style="padding-right: 5px">    
                    ABN                               
                </th>
                <td style="width: 120px" valign="top">
                    <% if (string.IsNullOrEmpty(ABN) || ABN.Trim().Length == 0) { %>
                        <%= "<i>&lt;NO ABN&gt;</i>" %>    
                    <% } else {%>
                        <%= ABN %>
                    <% } %>
                </td>
                <th style="padding-right: 5px" valign="top">      
                    ACN
                </th>
                <td style="width:120px" valign="top">
                    <% if (string.IsNullOrEmpty(ACN) || ACN.Trim().Length == 0) { %>
                        <%= "<i>&lt;NO ACN&gt;</i>" %>    
                    <% } else {%>
                        <%= ACN %>
                    <% } %>
                </td>                
            </tr>
        </tbody>
    </table>        
</sharp:FormGroup>
