﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.AddressControl" Codebehind="Address.ascx.cs" %>
<sharp:FormGroup ID="AddressFG" runat="server" CssClass="span6">
    <div class="verticalcenter" style="display:inline-table; height: 35px">
        <asp:HyperLink ID="CopyFromAddressLink" runat="server" Visible="false" ImageUrl="~/images/icons/home.png" ToolTip="Copy the Home Address to the Postal Address" style="padding-right: 5px" NavigateUrl="javascript:void(0)" />
        <b><%= CopyFromAddressLink.Text %></b>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtStreet.ClientID %>">Street</label>            
        <div class="controls">
            <asp:TextBox ID="txtStreet" runat="server" CssClass="EntryBox" MaxLength="50"></asp:TextBox>
         </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtStreet2.ClientID %>">Street 2</label>            
        <div class="controls">
            <asp:TextBox ID="txtStreet2" runat="server" CssClass="EntryBox" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"  for="<%= txtSuburb.ClientID %>">Suburb</label>            
        <div class="controls">
            <asp:TextBox ID="txtSuburb" runat="server" CssClass="EntryBox" MaxLength="50"></asp:TextBox>
        </div>
    </div>            
    <div class="control-group">
        <label class="control-label" for="<%= txtState.ClientID %>">State</label>                
        <div class="controls">
            <asp:TextBox ID="txtState" runat="server" MaxLength="3" CssClass="EntryBox" Width="70px"></asp:TextBox>            
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtPostcode.ClientID %>">Postcode</label>                
        <div class="controls">
            <asp:TextBox ID="txtPostcode" runat="server" MaxLength="4" Width="70px"></asp:TextBox>        
        </div>
    </div>        
</sharp:FormGroup>
