﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteHeading.ascx.cs" Inherits="AirtimeBilling.Web.UserControls.SiteHeading" %>
<%@ Import Namespace="AirtimeBilling.Web" %>

<div class="siteheading">
<% if (Users.Current.IsInRole(AirtimeBilling.Web.Roles.Agent)) { %>
Dealer Airtime Activations
<% } else if (Users.Current.IsInRole(AirtimeBilling.Web.Roles.Customer)) {%>
Airtime Activations
<% } %>
</div>