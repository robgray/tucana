﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.CompanyInfoControl" Codebehind="CompanyInfo.ascx.cs" %>
<sharp:FormGroup ID="CompanyInfo" runat="server" GroupingText="Company Information" CssClass="well span12">    
    <div class="control-group">
        <label class="control-label" for="<%= txtCompanyName.ClientID %>">Company Name</label>            
        <div class="controls">
            <asp:TextBox ID="txtCompanyName" runat="server" CssClass="EntryBox" MaxLength="50"></asp:TextBox>        
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtABN.ClientID %>" >ABN</label>             
        <div class="controls">
            <asp:TextBox ID="txtABN" runat="server" Width="100px" MaxLength="11"></asp:TextBox>
         </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtACN.ClientID %>" >ACN</label>            
        <div class="controls">
            <asp:TextBox ID="txtACN" runat="server" Width="100px" MaxLength="9"></asp:TextBox>
        </div>
    </div>
</sharp:FormGroup>
