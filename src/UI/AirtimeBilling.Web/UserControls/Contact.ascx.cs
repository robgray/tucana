﻿using System;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Web
{
    public partial class ContactControl : BaseUserControl
    {
        private bool _readOnly;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                                
                ddlCreditCardType.DataSource = Enum<CreditCardType>.GetAllDescriptions();
                ddlCreditCardType.DataBind();

                ucDeliveryAddress.CopyFromPanel(ucHomeAddress, "Copy from " + ucHomeAddress.Text);
            }
        }

        public void Clear()
        {
            txtCreditCardExpiry.Text = string.Empty;
            txtCreditCardName.Text = string.Empty;
            txtCreditCardNumber.Text = string.Empty;
            txtDriversLicenseDateOfBirth.Text = string.Empty;
            txtDriversLicenseExpiryDate.Text = string.Empty;
            txtDriversLicenseNumber.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtFaxNumber.Text = string.Empty;
            txtMobilePhone.Text = string.Empty;
            txtWorkNumber.Text = string.Empty;

            ucDeliveryAddress.Clear();
            ucHomeAddress.Clear();
        }

        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;
                ViewState.Remove("ContactId");

                txtCreditCardExpiry.Enabled = !value;
                txtCreditCardName.Enabled = !value;
                txtCreditCardNumber.Enabled = !value;
                txtDriversLicenseDateOfBirth.Enabled = !value;
                imgDateOfBirth.Enabled = !value;
                txtDriversLicenseExpiryDate.Enabled = !value;
                imgDriversLicenseExpiryDate.Enabled = !value;
                txtDriversLicenseNumber.Enabled = !value;
                txtEmail.Enabled = !value;
                txtFaxNumber.Enabled = !value;
                txtMobilePhone.Enabled = !value;
                txtName.Enabled = !value;
                txtWorkNumber.Enabled = !value;
                ddlCreditCardType.Enabled = !value;

                ucDeliveryAddress.ReadOnly = value;
                ucHomeAddress.ReadOnly = value;
            }
        }

        #region Properties

        public Contact Contact
        {
            get
            {
                Contact contact;
                if (ContactId > 0)
                {
                    contact = new Contact(ContactId);
                }
                else
                {
                    contact = new Contact();
                }

                contact.Name = this.Name;
                contact.DriversLicense = this.DriversLicense;
                contact.Email = this.Email;
                contact.FaxNumber = this.FaxNumber;
                contact.CreditCard = this.CreditCard;
                contact.HomeAddress = this.HomeAddress;
                contact.PostalAddress = this.DeliveryAddress;
                contact.MobilePhone = this.MobilePhone;
                contact.WorkPhone = this.WorkPhone;

                return contact;
            }
            set
            {
                if (value != null)
                {
                    this.ContactId = value.Id.Value;
                    this.Name = value.Name;
                    this.MobilePhone = value.MobilePhone;
                    this.Email = value.Email;
                    this.FaxNumber = value.FaxNumber;
                    this.WorkPhone = value.WorkPhone;
                    this.DriversLicense = value.DriversLicense;
                    this.CreditCard = value.CreditCard;
                    this.HomeAddress = value.HomeAddress;
                    this.DeliveryAddress = value.PostalAddress;
                    if (value.Id != null)
                    {
                        ContactId = value.Id.Value;
                    }
                    else
                    {
                        hdnContactId.Value = "";
                    }
                }
                else
                {
                    this.Clear();
                }
            }
        }

        public int ContactId
        {
            get
            {
                return hdnContactId.Value.Length > 0 ? int.Parse(hdnContactId.Value) : 0;
            }
            set { hdnContactId.Value = value.ToString(); }
        }

        public string Name
        {
            get { return txtName.Text; }
            set
            {
                txtName.Text = value;
            }
        }

        public string DriversLicenseNumber
        {
            get { return txtDriversLicenseNumber.Text; }
            set
            {
                txtDriversLicenseNumber.Text = value;
            }
        }

        public DateTime? DriversLicenseExpiry
        {
            get
            {
                DateTime expiry;
                if (DateTime.TryParse(txtDriversLicenseExpiryDate.Text, out expiry))
                {
                    return new DateTime?(expiry);
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    txtDriversLicenseExpiryDate.Text = value.Value.ToShortDateString();
                }
                else
                {
                    txtDriversLicenseExpiryDate.Text = string.Empty;
                }
            }
        }

        public DateTime? DriversLicenseDateOfBirth
        {
            get
            {
                DateTime dob;
                return DateTime.TryParse(txtDriversLicenseDateOfBirth.Text, out dob) ? (DateTime?)dob : null;
            }
            set
            {
                if (value != null)
                {
                    txtDriversLicenseDateOfBirth.Text = value.Value.ToShortDateString();
                }
                else
                {
                    txtDriversLicenseDateOfBirth.Text = string.Empty;
                }
            }
        }

        public DriversLicense DriversLicense
        {
            get
            {
                return new DriversLicense
                           {
                               LicenseNumber = this.DriversLicenseNumber,
                               DateOfBirth = this.DriversLicenseDateOfBirth.Value,
                               Expiry = this.DriversLicenseExpiry.Value
                           };
            }
            set
            {
                this.DriversLicenseNumber = value.LicenseNumber;
                this.DriversLicenseExpiry = new DateTime?(value.Expiry);
                this.DriversLicenseDateOfBirth = new DateTime?(value.DateOfBirth);
            }
        }

        public string CreditCardName
        {
            get { return txtCreditCardName.Text; }
            set
            {
                txtCreditCardName.Text = value;
            }
        }

        public string CreditCardExpiry
        {
            get { return txtCreditCardExpiry.Text; }
            set
            {
                txtCreditCardExpiry.Text = value;
            }
        }

        public CreditCardType CreditCardType
        {
            get
            {                
                return Enum<CreditCardType>.GetEnumFromDescription(ddlCreditCardType.SelectedValue);
            }
            set
            {                
                var name = value.GetDescription();                
                var item = ddlCreditCardType.Items.FindByValue(name);
                if (item != null)
                {
                    ddlCreditCardType.SelectedItem.Selected = false;
                    item.Selected = true;
                }
            }
        }

        public string CreditCardNumber
        {
            get { return txtCreditCardNumber.Text; }
            set
            {
                txtCreditCardNumber.Text = value;
            }
        }

        public CreditCard CreditCard
        {
            get
            {
                var cc = new CreditCard
                {
                    CardNumber = CreditCardNumber,
                    NameOnCard = CreditCardName,
                    ExpiryDate = CreditCardExpiry,
                    CreditCardType = CreditCardType
                };
                return cc;
            }
            set
            {
                if (value != null)
                {
                    this.CreditCardExpiry = value.ExpiryDate;
                    this.CreditCardName = value.NameOnCard;
                    this.CreditCardNumber = value.CardNumber;
                    this.CreditCardType = value.CreditCardType;
                }
                else
                {                    
                    txtCreditCardExpiry.Text = string.Empty;
                    txtCreditCardName.Text = string.Empty;                    
                    txtCreditCardNumber.Text = string.Empty;
                }
            }
        }

        public string HomeStreet
        {
            get { return ucHomeAddress.Street; }
            set { ucHomeAddress.Street = value; }
        }

        public string HomeStreet2
        {
            get { return ucHomeAddress.Street2; }
            set { ucHomeAddress.Street2 = value; }
        }

        public string HomeSuburb
        {
            get { return ucHomeAddress.Suburb; }
            set { ucHomeAddress.Suburb = value; }
        }

        public string HomeState
        {
            get { return ucHomeAddress.State; }
            set { ucHomeAddress.State = value; }
        }

        public string HomePostcode
        {
            get { return ucHomeAddress.Postcode; }
            set { ucHomeAddress.Postcode = value; }
        }

        public string DeliveryStreet
        {
            get { return ucDeliveryAddress.Street; }
            set { ucDeliveryAddress.Street = value; }
        }

        public string DeliveryStreet2
        {
            get { return ucDeliveryAddress.Street2; }
            set { ucDeliveryAddress.Street2 = value; }
        }

        public string DeliverySuburb
        {
            get { return ucDeliveryAddress.Suburb; }
            set { ucDeliveryAddress.Suburb = value; }
        }

        public string DeliveryState
        {
            get { return ucDeliveryAddress.State; }
            set { ucDeliveryAddress.State = value; }
        }

        public string DeliveryPostcode
        {
            get { return ucDeliveryAddress.Postcode; }
            set { ucDeliveryAddress.Postcode = value; }
        }

        public Address HomeAddress
        {
            get
            {
                return new Address
                           {
                               Street = this.HomeStreet,        
                               Street2 = this.HomeStreet2,
                               Suburb = this.HomeSuburb,
                               State = this.HomeState,
                               Postcode = this.HomePostcode
                           };
            }
            set
            {
                if (value != null)
                {
                    this.HomeStreet = value.Street;
                    this.HomeStreet2 = value.Street2;
                    this.HomeSuburb = value.Suburb;
                    this.HomeState = value.State;
                    this.HomePostcode = value.Postcode;
                }
                else
                {
                    ucHomeAddress.Clear();
                }
            }
        }

        public Address DeliveryAddress
        {
            get
            {
                return new Address
                           {
                               Street = this.DeliveryStreet,
                               Street2 = this.DeliveryStreet2,
                               Suburb = this.DeliverySuburb,
                               State = this.DeliveryState,
                               Postcode = this.DeliveryPostcode
                           };
            }
            set
            {
                if (value != null)
                {
                    this.DeliveryStreet = value.Street;
                    this.DeliveryStreet2 = value.Street2;
                    this.DeliverySuburb = value.Suburb;
                    this.DeliveryState = value.State;
                    this.DeliveryPostcode = value.Postcode;
                }
                else
                {
                    ucDeliveryAddress.Clear();
                }
            }
        }

        public string Email
        {
            get { return txtEmail.Text; }
            set
            {
                txtEmail.Text = value;
            }
        }

        public string FaxNumber
        {
            get { return txtFaxNumber.Text; }
            set
            {
                txtFaxNumber.Text = value;
            }
        }

        public string MobilePhone
        {
            get { return txtMobilePhone.Text; }
            set
            {
                txtMobilePhone.Text = value;
            }
        }

        public string WorkPhone
        {
            get { return txtWorkNumber.Text; }
            set
            {
                txtWorkNumber.Text = value;
            }
        }

        #endregion
    }
}