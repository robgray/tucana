﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractTabSummary.ascx.cs" Inherits="AirtimeBilling.Web.UserControls.ContractTabSummary" %>
<div class="Form">
    <sharp:FormGroup ID="ContractSummaryFG" runat="server" GroupingText="Contract" CssClass="full">                
        <table cellpadding="1" cellspacing="0" class="companyInfo">
        <tbody>
            <tr>
                <td style="padding-left: 5px;">
                    <div class="control-group">
                        <label>Contract Number</label>
                    </div>                       
                </td>
                <td style="width:200px;" valign="top"><asp:Label ID="ContractNumberLabel" runat="server" Text="" ></asp:Label></td>
                <td>
                    <div class="control-group">                    
                        <label>Contact Name</label>
                    </div>
                </td>
                <td valign="top"><asp:Label ID="ContactNameLabel" runat="server" Text="" ></asp:Label></td>
            </tr>                                                
        </tbody>
        </table>
    </sharp:FormGroup>  
</div>    