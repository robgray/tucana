﻿using System;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class CompanyInfoReadOnlyControl : BaseUserControl
    {
        private bool _readOnly;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Clear()
        {
            ClearInputs(this.Controls);            
        }

        public bool ReadOnly
        {
            get { return _readOnly;  }
            set { _readOnly = value; }
        }
        
        public string CompanyName
        {
            get { return (string)ViewState["CompanyName"]; }
            set
            {
                ViewState["CompanyName"] = value;
            }
        }

        public string ABN
        {
            get { return (string)ViewState["ABN"]; }
            set
            {
                ViewState["ABN"] = value;            
            }
        }

        public string ACN
        {
            get { return (string)ViewState["ACN"]; }
            set
            {
                ViewState["ACN"] = value;
            }
        }

        public void SetCompany(Company company)
        {
            Clear();
            CompanyName = company.CompanyName;
            ABN = company.ABN;
            ACN = company.ACN;
        }
    }
}