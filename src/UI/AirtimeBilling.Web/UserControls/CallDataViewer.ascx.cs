﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Logging;
using System.Collections.Generic;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class CallDataViewerControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public int ContractId
        {
            set
            {                
                var request = new GetUnInvoicedCallsRequest { ContractId = value, User = Users.Current };

                try
                {
                    var response = ContractService.GetUnInvoicedCalls(request);

                    IList<Call> uninvoicedCalls = new List<Call>();
                    if (response.IsSuccessful) {
                        uninvoicedCalls = response.UninvoicedCalls;
                    }

                    // Perform the figure modification.

                    gvCallData.DataSource = uninvoicedCalls;
                    gvCallData.DataBind();
                }
                catch (Exception ex)
                {
                    LoggingUtility.LogException(Users.Current.Username, ex);
                }
            }
        }        
    }    
}