﻿using System;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class AddressControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public bool ReadOnly
        {
            get
            {
                return (bool)(ViewState["readonly"] ?? false);
            }
            set
            {
                ViewState["readonly"] = value;                

                txtStreet.Enabled = !value;
                txtStreet2.Enabled = !value;
                txtSuburb.Enabled = !value;
                txtState.Enabled = !value;
                txtPostcode.Enabled = !value;
            }
        }

        public void Clear()
        {
            ClearInputs(Controls);            
        }

        public AddressControlType AddressType
        {
            get { return (AddressControlType) (ViewState["controltype"] ?? AddressControlType.Home); }
            set { ViewState["controltype"] = value;  }
        }

        public string Text
        {
            get { return AddressFG.GroupingText; }
            set 
            {
                AddressFG.GroupingText = value;               
            }
        }

        public string Street
        {
            get { return txtStreet.Text; }
            set
            {
                txtStreet.Text = value;
            }
        }

        public string Street2
        {
            get { return txtStreet2.Text;  }
            set { txtStreet2.Text = value; }
        }

        public string Suburb
        {
            get { return txtSuburb.Text; }
            set
            {
                txtSuburb.Text = value;
            }
        }

        public string State
        {
            get { return txtState.Text; }
            set
            {
                txtState.Text = value;
            }
        }

        public string Postcode
        {
            get { return txtPostcode.Text; }
            set
            {
                txtPostcode.Text = value;
            }
        }

        public enum AddressControlType
        {
            Home,
            Delivery
        }

        public void SetAddress(Address address)
        {
            Clear();
            Street = address.Street;
            Street2 = address.Street2;
            Suburb = address.Suburb;
            Postcode = address.Postcode;
            State = address.State;
        }

        public void CopyFromPanel(AddressControl control, string linkText)
        {
            if (control == null) return;

            CopyFromAddressLink.ToolTip = linkText;                     
            CopyFromAddressLink.Visible = true;
            CopyFromAddressLink.Text = linkText;                        
            CopyFromAddressLink.Attributes.Add("onclick", string.Format("javascript:AddressManager.Copy('{0}', '{1}')", control.ClientID, this.ClientID));
        }
    }
}