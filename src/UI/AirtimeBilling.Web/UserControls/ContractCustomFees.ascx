﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ContractCustomFees.ascx.cs" Inherits="AirtimeBilling.Web.UserControls.ContractCustomFees" %>
<div>
    <asp:GridView ID="gvCustomFees" runat="server" AutoGenerateColumns="False" DataKeyNames="Id"     
        onrowdeleting="gvCustomFees_RowDeleting">        
    <Columns>        
        <asp:CommandField ButtonType="Image" ShowEditButton="False" ShowDeleteButton="True" 
                        DeleteImageUrl="~/images/cross.gif" DeleteText="Delete"                                         
                        HeaderStyle-Width="20px" ItemStyle-Width="20px" /> 
        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />        
        <asp:BoundField DataField="Description" HeaderText="Custom Fee" Visible="True">
            <ItemStyle Width="400px" />
        </asp:BoundField>        
        <asp:BoundField DataField="Amount" HeaderText="Amount" Visible="True" DataFormatString="{0:c}">
            <ItemStyle Width="80px" />
        </asp:BoundField>
        <asp:BoundField DataField="IsRecurring" HeaderText="Recurring?" Visible="True">
            <ItemStyle Width="40px" />
        </asp:BoundField>
    </Columns>                
    </asp:GridView>
    <div class="clear">
        <sharp:MessagePanel ID="EmptyMessage" runat="server" IncludeClose="False" />
    </div>
</div>