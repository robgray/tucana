﻿using System;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class CompanyInfoControl : BaseUserControl
    {
        private bool _readOnly;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Clear()
        {
            txtABN.Text = string.Empty;
            txtACN.Text = string.Empty;
            txtCompanyName.Text = string.Empty;
        }

        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;

                txtCompanyName.Enabled = !value;
                txtABN.Enabled = !value;
                txtACN.Enabled = !value;
            }
        }

        public string CompanyName
        {
            get { return txtCompanyName.Text; }
            set
            {
                txtCompanyName.Text = value;
            }
        }

        public string ABN
        {
            get { return txtABN.Text; }
            set
            {
                txtABN.Text = value;
                txtACN.Text = value;
            }
        }

        public string ACN
        {
            get { return txtACN.Text; }
            set
            {
                txtACN.Text = value;
            }
        }

        public void SetCompany(Company company)
        {
            Clear();
            CompanyName = company.CompanyName;
            ABN = company.ABN;
            ACN = company.ACN;
        }
    }
}