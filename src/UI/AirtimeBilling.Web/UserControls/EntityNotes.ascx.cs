﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web.UserControls
{
    public partial class EntityNotes : BaseUserControl
    {      
        public void SetEntity(EntityBase entity)
        {
            EntityId = entity.Id.Value;
            EntityType = entity.GetEntityName();

            PopulateNotes();
        }
        
        public int EntityId
        {
            get
            {
                return (int) ViewState["EntityId"];
            }
            private set
            {
                ViewState["EntityId"] = value;
            }
        }

        public string EntityType
        {
            get
            {
                return (string) ViewState["EntityType"];
            }
            private set
            {
                ViewState["EntityType"] = value;
            }
        }

        public void AddNote(object sender, EventArgs e)
        {     
            var note = new Note()
                           {
                               Comment = Comment.Text,
                               EntityId = EntityId,
                               NoteDate = DateTime.Now,
                               NoteType = EntityType,
                               Username = Users.Current.Username
                           };

            NoteService.SaveNote(note);

            PopulateNotes();

            Comment.Text = string.Empty;
        }

        protected void PopulateNotes()
        {
            var notes = NoteService.GetNotesFor(ProxyHelper.GetProxyEntity(EntityId, EntityType));
            NotesGrid.DataSource = notes;
            NotesGrid.DataBind();
        }
    }
}