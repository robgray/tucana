﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.AccountReadOnlyControl" Codebehind="AccountReadOnly.ascx.cs" %>
<div class="row">
    <asp:HiddenField ID="hdnAccountId" runat="server" />
    <sharp:FormGroup ID="AccountInfo" runat="server" GroupingText="Account Information" CssClass="span6"> 
        <div class="control-group">
            <label class="control-label" for="<%= lblAccountNumber.ClientID %>">Account Number</label>
            <div class="controls">
                <asp:Label ID="lblAccountNumber" runat="server" Text="<SYSTEM GENERATED>"></asp:Label>        
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= lblAccountName.ClientID %>">Account Name</label>
            <div class="controls">
                <asp:Label ID="lblAccountName" runat="server" Text=""></asp:Label>        
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= lblContactName.ClientID %>">Contact</label>
            <div class="controls">
                <asp:Label ID="lblContactName" runat="server" Text=""></asp:Label>        
            </div>
        </div>
        <asp:Panel ID="pnlCompany" runat="server" CssClass="control-group">
            <label class="control-label" for="<%= lblCompany.ClientID %>">Company</label>
            <div class="controls">
                <asp:Label ID="lblCompany" runat="server" Text=""></asp:Label>        
            </div>
        </asp:Panel>
        <div class="control-group">
            <label class="control-label">Password</label>
            <div class="controls">
                <asp:Label ID="lblPassword" runat="server"></asp:Label>     
            </div>   
        </div>
    </sharp:FormGroup>

    <sharp:FormGroup ID="AccountBilling" runat="server" GroupingText="Account Billing" CssClass="span6">   
        <asp:Panel ID="InvoiceRootPanel" runat="server">
            <asp:Panel runat="server" ID="OutstandingBalancePanel" Style="border: 1px solid #b0b0b0;">   
                <br />  
                <table cellpadding="0">
                <tr>
                    <td style="width:225px">
                        <div class="control-group">
                            <label class="control-label">Previous Bill</label>
                            <div class="controls">
                            <asp:Label ID="PreviousBillLabel" runat="server" Text="$0.00" />
                            </div>
                        </div>
                    </td>
                    <td>
                        <div class="control-group">
                            <label class="control-label">Amount Paid</label>
                            <div class="controls">
                                <asp:Label ID="AmountPaidLabel" runat="server" />
                            </div>
                        </div> 
                    </td>
                </tr>
                </table>                                                                                                             
            </asp:Panel>
            <br />
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                    <div class="control-group">
                        <label class="control-label">Billing Method</label>                    
                        <div class="controls">
                            <asp:Label ID="lblBillingMethod" runat="server"></asp:Label>
                        </div>
                    </div>            
                    <div class="control-group" style="display:none;">
                        <label class="control-label">Billing Address</label>                    
                        <div class="controls">
                            <asp:Label ID="lblBillingAddressType" runat="server"></asp:Label>
                        </div>
                    </div>    
                    </td>        
                    <td>
                    <div class="control-group" style="display:none">                                    
                        <label class="control-label">Invoice Root</label>
                        <div class="controls">
                            <asp:Image ID="InvoiceRootImage" runat="server" />                    
                        </div>
                    </div>    
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div class="control-group">
                        <label class="control-label">Email Data File</label>
                        <div class="controls">
                            <asp:Image ID="EmailBillDataFileImage" runat="server" />
                        </div>
                    </div>
                    </td>
                    <td>
                    <div class="control-group">
                        <label class="control-label">Paperless Bill</label>
                        <div class="controls">
                            <asp:Image ID="PaperlessBillImage" runat="server" />
                        </div>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div class="control-group">
                        <label class="control-label">Post Bill</label>
                        <div class="controls">
                            <asp:Image ID="PostBillImage" runat="server" />
                        </div>
                    </div>
                    </td>
                    <td>
                    <div class="control-group">
                        <label class="control-label">International Customer</label>
                        <div class="controls">
                            <asp:Image ID="InternationalImage" runat="server" />
                        </div>
                    </div>
                    </td>
                </tr>                
            </table>
        </asp:Panel>
        <asp:Panel ID="NotInvoiceRootPanel" runat="server">
        <sharp:MessagePanel ID="InvoiceRootMessage" runat="server" MessageType="Information"
                Message="" />
        </asp:Panel>
    </sharp:FormGroup>
</div>