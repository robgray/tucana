﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;


namespace AirtimeBilling.Web.UserControls
{
	public partial class MainMenu : BaseUserControl
	{	    
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack) {
                var login = lvMenu.FindControl("LoginStatus1") as LoginStatus;
                if (login != null) {
                    login.LoggingOut += (LoggingOut);         
                }
            }
		}
      
        protected void LoggingOut(object sender, LoginCancelEventArgs e)
        {
            FormsAuthentication.SignOut();            
            Session.Abandon();

            // clear the authentication cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie1);

            // clear session cookie
            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.Now.AddYears(-1);
            Response.Cookies.Add(cookie2);

            FormsAuthentication.RedirectToLoginPage();
        } 
	}
}