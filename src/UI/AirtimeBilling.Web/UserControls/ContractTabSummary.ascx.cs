﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web.UserControls
{
    public partial class ContractTabSummary : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string ContractNumber
        {
            get { return ContractNumberLabel.Text; }
            set { ContractNumberLabel.Text = value; }
        }

        public string ContactName
        {
            get { return ContactNameLabel.Text; }
            set { ContactNameLabel.Text = value; }
        }

        public string Title
        {
            get { return ContractSummaryFG.GroupingText;  }
            set { ContractSummaryFG.GroupingText = value; }
        }
    }
}