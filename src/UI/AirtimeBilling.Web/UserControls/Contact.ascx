﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ContactControl" Codebehind="Contact.ascx.cs" %>
<%@ Register src="~/UserControls/Address.ascx" tagname="Address" tagprefix="sharp" %>
<div class="row">
    <asp:HiddenField ID="hdnContactId" runat="server" />
    <!-- Personal/Identity Information -->
    <sharp:FormGroup ID="PersonalInfo" runat="server" GroupingText="Personal Information" CssClass="span6">
        <div class="control-group">
            <label class="control-label" for="<%= txtName.ClientID %>">Name</label>
            <div class="controls">
                <asp:Label ID="lblName" runat="server" Text="" Visible="false" ></asp:Label>
                <asp:TextBox ID="txtName" runat="server" CssClass="EntryBox" MaxLength="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
                    ControlToValidate="txtName" 
                    ErrorMessage="Missing Contact Name" Display="None"></asp:RequiredFieldValidator>        
            </div>                
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtDriversLicenseDateOfBirth.ClientID %>">Date Of Birth</label>            
            <div class="controls">
                <asp:TextBox ID="txtDriversLicenseDateOfBirth" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                <asp:Image ID="imgDateOfBirth" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
                <atk:CalendarExtender ID="txtDriversLicenseDateOfBirth_CalendarExtender" 
                    runat="server" Enabled="True" TargetControlID="txtDriversLicenseDateOfBirth" PopupButtonID="imgDateOfBirth" Format="dd/MM/yyyy">
                </atk:CalendarExtender>
                <span class="tip">Format is dd/mm/yyyy</span>
                <asp:RequiredFieldValidator ID="rfvDateOfBirth" runat="server" 
                    ErrorMessage="Missing Date of Birth" 
                    ControlToValidate="txtDriversLicenseDateOfBirth" Display="None"></asp:RequiredFieldValidator>        
                <asp:CompareValidator ID="cvDateOfBirth" runat="server" ErrorMessage="Invalid D.O.B." 
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtDriversLicenseDateOfBirth" 
                    Text=""  Display="None"></asp:CompareValidator>            
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtDriversLicenseNumber.ClientID %>">License Number</label>            
            <div class="controls">
                <asp:TextBox ID="txtDriversLicenseNumber" runat="server" Width="250px" 
                    MaxLength="8"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvLicenseNumber" runat="server" 
                    ErrorMessage="Missing Drivers License Number"
                    ControlToValidate="txtDriversLicenseNumber" Display="None"></asp:RequiredFieldValidator>                    
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtDriversLicenseExpiryDate.ClientID %>">License Expiry</label>            
            <div class="controls">
                <asp:TextBox ID="txtDriversLicenseExpiryDate" runat="server" MaxLength="10" Width="100px"></asp:TextBox>
                <asp:Image ID="imgDriversLicenseExpiryDate" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
                <atk:CalendarExtender ID="txtDriversLicenseExpiryDate_CalendarExtender" runat="server" 
                    TargetControlID="txtDriversLicenseExpiryDate" 
                    PopupButtonID="imgDriversLicenseExpiryDate"
                    Format="dd/MM/yyyy">
                </atk:CalendarExtender>
                <span class="tip">Format is dd/mm/yyyy</span>            
                <asp:RequiredFieldValidator ID="rfvLicenseExpiryDate" runat="server" 
                    ErrorMessage="Missing License Expiry Date"
                    ControlToValidate="txtDriversLicenseExpiryDate" Display="None"></asp:RequiredFieldValidator>        
                <asp:CompareValidator ID="cvDriversLicenseExpiry" runat="server" ErrorMessage="Invalid Date" 
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtDriversLicenseExpiryDate" 
                    Display="None"></asp:CompareValidator>            
            </div>
        </div>
    </sharp:FormGroup>
    
    <!-- Contact Phone Numbers, Email, etc -->
    <sharp:FormGroup ID="MaintainPlan" runat="server" GroupingText="Contact Details" CssClass="span6">  
        <div class="control-group">
            <label class="control-label" for="<%=  txtEmail.ClientID %>">Email</label>
            <div class="controls">
                <asp:TextBox ID="txtEmail" runat="server" Width="250px" MaxLength="255"></asp:TextBox>        
                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                    ErrorMessage="Missing Email Address" Display="None"
                    ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtMobilePhone.ClientID %>">Mobile Number</label> 
            <div class="controls">
                <asp:TextBox ID="txtMobilePhone" runat="server" MaxLength="15"></asp:TextBox>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtWorkNumber.ClientID %>">Work Number</label>
            <div class="controls">
                <asp:TextBox ID="txtWorkNumber" runat="server" MaxLength="15"></asp:TextBox>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtFaxNumber.ClientID %>">Fax Number</label>
            <div class="controls">
                <asp:TextBox ID="txtFaxNumber" runat="server" MaxLength="15"></asp:TextBox>
                <br />
            </div>
        </div>
    </sharp:FormGroup>  
</div>
<div class="row">
    <!-- Address Information -->
    <sharp:address ID="ucHomeAddress" runat="server" Text="Home Address" />
    <sharp:address ID="ucDeliveryAddress" runat="server" Text="Postal Address" />                
 </div>
<div class="row">
    <!-- Credit Card Information -->
    <sharp:FormGroup ID="CCInfo" runat="server" GroupingText="Credit Card Information" CssClass="span6">
        <div class="alert alert-info">
            Credit Card Information must be collected to establish identity.  The supplied credit card will not be charged on Invoice accounts.  
            All other accounts will incur a charge to the provided Credit Card.        
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtCreditCardName.ClientID %>">Name on Card</label>
            <div class="controls">
                <asp:TextBox ID="txtCreditCardName" runat="server" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCreditCardName" runat="server" 
                    ErrorMessage="Missing Credit Card Name"
                    ControlToValidate="txtCreditCardName" Display="None"></asp:RequiredFieldValidator>        
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtCreditCardExpiry.ClientID %>">Expiry Date</label>
            <div class="controls">
                <asp:Label ID="lblCreditCardExpiry" runat="server" Text="" Visible="false" ></asp:Label>
                <asp:TextBox ID="txtCreditCardExpiry" runat="server" Width="75px" MaxLength="5"></asp:TextBox>
                <span class="tip">Format is mm/yy</span>
                <asp:RequiredFieldValidator ID="rfvCreditCardExpiry" runat="server" 
                    ErrorMessage="Missing Credit Card Expiry" 
                    ControlToValidate="txtCreditCardExpiry" Display="None"></asp:RequiredFieldValidator>        
                <asp:RegularExpressionValidator ID="rgvCreditCardExpiry" runat="server" ErrorMessage="Invalid Expiry Date.  Must be in format __/__" ControlToValidate="txtCreditCardExpiry" 
                     Enabled="True" ValidationExpression="\d{2}/\d{2}" Display="None"></asp:RegularExpressionValidator>                
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= ddlCreditCardType.ClientID %>">Card Type</label>
            <div class="controls">
                <asp:DropDownList ID="ddlCreditCardType" runat="server" Width="150px"></asp:DropDownList>
                <asp:RegularExpressionValidator ID="rgvCreditCardType" runat="server" ErrorMessage="Invalid Credit Card Type" ControlToValidate="ddlCreditCardType" 
                     Enabled="True" ValidationExpression="^(?!.*?Not).*" Display="None"></asp:RegularExpressionValidator>  
            </div>                               
        </div>        
        <div class="control-group">
            <label class="control-label" for="<%= txtCreditCardNumber.ClientID %>">Card Number</label>
            <div class="controls">
                <asp:TextBox ID="txtCreditCardNumber" runat="server" Width="250px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvCreditCardNumber" runat="server" 
                    ErrorMessage="Missing Credit Card Number"
                    ControlToValidate="txtCreditCardNumber" Display="None"></asp:RequiredFieldValidator>       
            </div>
        </div>
    </sharp:FormGroup>
</div>