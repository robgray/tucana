﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="MainMenu.ascx.cs" Inherits="AirtimeBilling.Web.UserControls.MainMenu" %>
<%@ Import Namespace="AirtimeBilling.Web"%>
<asp:LoginView ID="lvMenu" runat="server">
<LoggedInTemplate>                                            
	<div class="navbar navbar-fixed-top">
	    <div class="navbar-inner">
	        <div class="container">
	            <a class="brand" href="#">Airtime Billing</a>
                <ul class="nav">	        
	                <% if (Users.Current.IsInRole(AirtimeBilling.Web.Roles.BackOffice)) { %>		                
                    <li><a href="/BackOffice/Welcome.aspx">Home</a></li>
		            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Contracts<b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a href="<%= NavigationController.GetNewContractLink() %>">New Contract</a></li>
			                <li><a href="<%= NavigationController.GetContractSearchLink() %>">Find Contract</a></li>                    
			            </ul>
		            </li>
		            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Invoicing<b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a href="<%= NavigationController.GetInvoiceRunLink() %>">Perform Invoicing</a></li>
			                <li><a href="<%= NavigationController.GetInvoiceSearchLink() %>">Existing Invoices</a></li>
			            </ul>
		            </li>
		            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Maintenance<b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a href="<%= NavigationController.GetAccountSearchLink() %>">Accounts</a></li>
			                <li><a href="<%= NavigationController.GetAgentSearchLink() %>">Agents</a></li>
			                <li><a href="<%= NavigationController.GetCompanySearchLink() %>">Companies</a></li>
				            <li><a href="<%= NavigationController.GetContactSearchLink() %>">Contacts</a></li>
				            <li><a href="<%= NavigationController.GetNetworkSearchLink() %>">Networks</a></li>				    
				            <li><a href="<%= NavigationController.GetPlanSearchLink() %>">Plans</a></li>	
				            <li><a href="<%= NavigationController.GetCustomFeesLink() %>">Custom Fees</a></li>
			            </ul>
		            </li>
		            <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Control Panel<b class="caret"></b></a>
			            <ul class="dropdown-menu">
			                <li><a href="<%= NavigationController.GetUserListLink() %>">Users</a></li>
			                <li><a href="<%= NavigationController.GetSystemSettingsLink() %>">Settings</a></li>
			                <li><a href="<%= NavigationController.GetLogViewerLink() %>">Log Viewer</a></li>
			                <li><a href="<%= NavigationController.GetAboutLink() %>">About</a></li>
			            </ul>
		            </li>                
	                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Support<b class="caret"></b></a>
	                    <ul class="dropdown-menu">
	                        <li><a href="<%= NavigationController.GetSupportLink() %>">Testing Services</a></li>
	                    </ul>
	                </li>		      
		            <% } else if (Users.Current.IsInRole(AirtimeBilling.Web.Roles.Agent)) { %>

                    <li><a id="Welcome" href="/Agent/Welcome.aspx">Home</a></li>		    		        
		            <li><a id="Contracts" href="<%= NavigationController.GetNewContractLink() %>">New Contract</a></li>		    
		            <li><a id="Maintenance" href="<%= NavigationController.GetEditAgentLink() %>">Agency Details</a></li>		    
		            <% } %>
		            <li><asp:LoginStatus ID="LoginStatus1" runat="server" OnLoggingOut="LoggingOut" /></li>
	            </ul>
                <div id="topstatusbar">                        
                    <asp:Image ID="Image1" ImageUrl="~/images/user.gif" runat="server" AlternateText="User"  style="vertical-align: middle;" />                    
                    <asp:LoginName ID="LoginName1" runat="server" style="vertical-align: middle;" />                                            
                </div>                                            
                <div class="clear"></div>
	        </div>        
	    </div>
    </div>	   	
</LoggedInTemplate>
</asp:LoginView>