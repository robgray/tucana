﻿

<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ActivityLogViewerControl" Codebehind="ActivityLogViewer.ascx.cs" %>
<div>
    <asp:GridView ID="gvActivityLog" runat="server"           
        AutoGenerateColumns="False" SkinID="NoHoverGrid">    
        <Columns>        
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />        
            <asp:BoundField DataField="LogDate" HeaderText="Date/Time" Visible="True" 
                DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="LogDate">
                <HeaderStyle Width="130px" />
                <ItemStyle Width="130px" />
            </asp:BoundField>
            <asp:BoundField DataField="User" HeaderText="User" Visible="True">
                <ItemStyle Width="100px" />
            </asp:BoundField>
            <asp:BoundField DataField="ActivityName" HeaderText="Activity" Visible="True">
                <ItemStyle Width="600px" />
            </asp:BoundField>
        </Columns>                            
    </asp:GridView>
    <div class="crudbarfloat"></div>
</div>