﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web.UserControls
{    

    public partial class ContractCustomFees : BaseUserControl 
    {        
        protected int ContractId
        {
            get
            {
                return (int) (ViewState["ContractId"] ?? 0);
            }
            set
            {
                ViewState["ContractId"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        public void LoadCustomFees(int contractId)
        {
            ContractId = contractId;

            var request = new CustomFeesRequest() { ContractId = contractId };
            var response = ContractService.GetCustomFeesForContract(request);

            gvCustomFees.DataSource = response.CustomFees;
            gvCustomFees.DataBind();

            if (response.CustomFees.Count == 0) {
                EmptyMessage.SetInformation("No Custom Fees are currently active on this Contract");
            }
            else {
                EmptyMessage.Clear();
            }
        }

        protected void gvCustomFees_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (Request["x"] != null && Request["y"] != null)
            {
                gvCustomFees.EditIndex = -1;
                LoadCustomFees(ContractId);
            }
            else
            {

                int customFeeId = (int)gvCustomFees.DataKeys[e.RowIndex].Value;


                var response = ContractService.RemoveCustomFee(customFeeId);
                if (response.IsSuccessful)
                {
                    gvCustomFees.EditIndex = -1;
                    LoadCustomFees(ContractId);
                }               
            }
        }
    }
}