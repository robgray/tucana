﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EntityNotes.ascx.cs" Inherits="AirtimeBilling.Web.UserControls.EntityNotes" %>

<div class="Form">
    <div>
        <label>Comment/Note</label>
        <asp:TextBox ID="Comment" runat="server" TextMode="MultiLine" Rows="4" Width="600" />
        <asp:LinkButton CssClass="btn btn-primary" ID="AddNoteButton" Text="Add Note" OnClick="AddNote" runat="server" />
    </div>
</div>

<div>
    <br />
    <asp:GridView ID="NotesGrid" runat="server" AutoGenerateColumns="False" SkinID="NoHoverGrid">        
    <Columns>        
        <asp:BoundField DataField="Id" HeaderText="Id" Visible="False" />
        <asp:BoundField DataField="NoteDate" HeaderText="Note Date" Visible="True" 
            DataFormatString="{0:dd/MM/yyyy HH:mm:ss}">
            <ItemStyle Width="140px" />
            <HeaderStyle Width="140px" />
        </asp:BoundField>
        <asp:BoundField DataField="Username" HeaderText="User" Visible="True">
            <ItemStyle Width="100px" />
            <HeaderStyle Width="100px" />
        </asp:BoundField>
        <asp:BoundField DataField="Comment" HeaderText="Comment/Note" Visible="True">            
        </asp:BoundField>                    
    </Columns>            
    <EmptyDataTemplate>        
    </EmptyDataTemplate>
    </asp:GridView>
    <div class="crudbarfloat"></div>
</div>