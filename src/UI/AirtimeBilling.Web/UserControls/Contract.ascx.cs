﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Logging;

namespace AirtimeBilling.Web
{
    public partial class ContractControl : BaseUserControl
    {
        private int _planId;
        private bool _readOnly;

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            try
            {                                
                if (!IsPostBack)
                {
                    if (NewContract)
                    {
                                                
                        var networks = NetworkService.GetAllActiveNetworks();

                        ddlNetwork.DataSource = networks;
                        ddlNetwork.DataTextField = "Name";
                        ddlNetwork.DataValueField = "Id";
                        ddlNetwork.DataBind();

                        if (networks.Count > 0) NetworkId = networks[0].Id.Value;
                    }                    
                }

                ddlNetwork.Visible = NewContract;
                NetworkLabel.Visible = !ddlNetwork.Visible;
            }
            catch (Exception ex)
            {
                CurrentSession.ExceptionMessage = ex.Message;
                LoggingUtility.LogException(ex);
            }
        }
        protected void ConfigureForNewPlan(object sender, EventArgs e)
        {            
            Plan plan = PlanService.GetPlan(PlanId);
            if (plan != null)
            {
                DateTime activationDate = DateTime.Parse(txtActivationDate.Text);
                txtEndDate.Text = activationDate.AddMonths(plan.Duration).ToShortDateString();
            }
        }

        public string ContractNumber
        {
            get { return lblContractNumber.Text; }
            set { lblContractNumber.Text = value; }
        }

        public DateTime ActivationDate
        {
            get { return DateTime.Parse(txtActivationDate.Text); }
            set
            {
                txtActivationDate.Text = value.ToShortDateString();
            }
        }

        public DateTime EndDate
        {
            get { return DateTime.Parse(txtEndDate.Text); }
            set
            {
                txtEndDate.Text = value.ToShortDateString();
            }
        }

        public int NetworkId
        {
            get
            {
                return ddlNetwork.SelectedIndex > -1 ? int.Parse(ddlNetwork.SelectedValue) : 0;
            }
            set
            {
                if (NewContract) {
                    var item = ddlNetwork.Items.FindByValue(value.ToString());
                    if (item != null) {
                        if (ddlNetwork.SelectedIndex > -1) ddlNetwork.SelectedItem.Selected = false;
                        item.Selected = true;
                    }
                } else {
                    var network = NetworkService.GetNetwork(value);
                    NetworkLabel.Text = network != null ? network.Name : "<ERR NET UNKNOWN>";
                }

                
                var plans = PlanService.GetPlansByNetworkId(value);

                ddlPlan.DataSource = plans;
                ddlPlan.DataTextField = "Name";
                ddlPlan.DataValueField = "Id";
                ddlPlan.DataBind();

                if (_planId > 0)
                {
                    var planItem = ddlPlan.Items.FindByValue(_planId.ToString());
                    if (planItem != null)
                    {
                        if (ddlPlan.SelectedIndex > -1) ddlPlan.SelectedItem.Selected = false;
                        planItem.Selected = true;
                    }
                }
            }
        }

        public int PlanId
        {
            get
            {
                if (ddlPlan.SelectedIndex > -1)
                {
                    return int.Parse(ddlPlan.SelectedValue);
                }                
                return 0;
                
            }
            set
            {
                _planId = value;
                var item = ddlPlan.Items.FindByValue(value.ToString());
                if (item != null)
                {
                    if (ddlPlan.SelectedIndex > -1) ddlPlan.SelectedItem.Selected = false;
                    item.Selected = true;
                }
            }
        }

        public int AccountId
        {
            get { return (int)ViewState["AccountId"]; }
            set { ViewState["AccountId"] = value; }
        }

        public int ContractId
        {
            get { return (int)ViewState["ContractId"]; }
            set { ViewState["ContractId"] = value; }
        }

        public string SimCard
        {
            get { return txtSimCard.Text; }
            set
            {
                txtSimCard.Text = value;
            }
        }

        public string IMEINumber
        {
            get { return txtIMEINumber.Text; }
            set
            {
                txtIMEINumber.Text = value;
            }
        }

        public string PhoneNumber1
        {
            get { return txtPhoneNumber1.Text; }
            set
            {
                txtPhoneNumber1.Text = value;
            }
        }

        public string PhoneNumber2
        {
            get { return txtPhoneNumber2.Text; }
            set
            {
                txtPhoneNumber2.Text = value;
            }
        }

        public string PhoneNumber3
        {
            get { return txtPhoneNumber3.Text; }
            set
            {
                txtPhoneNumber3.Text = value;
            }
        }

        public string PhoneNumber4
        {
            get { return txtPhoneNumber4.Text; }
            set
            {
                txtPhoneNumber4.Text = value;
            }
        }

        public int AgentId
        {
            get { return (int)ViewState["AgentId"]; }
            set { ViewState["AgentId"] = value; }
        }

        public ContractStatus ContractStatus
        {
            get
            {
                return Enum<ContractStatus>.Parse(ViewState["ContractStatus"].ToString());
            }
            set
            {
                var statusName = value.GetDescription();
                lblContractStatus.Text = statusName;
                pnlPhoneNumbers.Visible = value != ContractStatus.NewContract;                
                ViewState["ContractStatus"] = value;
            }
        }

        public bool Data
        {
            get { return chkData.Checked; }
            set { chkData.Checked = value; }
        }

        public bool MessageBank
        {
            get { return chkMessageBank.Checked; }
            set { chkMessageBank.Checked = value; }
        }

        public string PhoneUsedBy
        {
            get { return txtUsedBy.Text; }
            set
            {
                txtUsedBy.Text = value;
            }
        }

        public string Pin
        {
            get { return txtPin.Text; }
            set
            {
                txtPin.Text = value;
            }
        }

        public string Puk
        {
            get { return txtPuk.Text; }
            set
            {
                txtPuk.Text = value;
            }
        }

        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;

                chkData.Enabled = !value;
                chkMessageBank.Enabled = !value;

                ddlNetwork.Enabled = !value;
                ddlPlan.Enabled = !value;
                txtActivationDate.Enabled = !value;
                imgActivationDate.Enabled = !value;
                txtEndDate.Enabled = !value;
                imgEndDate.Enabled = !value;
                txtIMEINumber.Enabled = !value;
                //txtPhone.Enabled = !value;
                txtPhoneNumber1.Enabled = !value;
                txtPhoneNumber2.Enabled = !value;
                txtPhoneNumber3.Enabled = !value;
                txtPhoneNumber4.Enabled = !value;
                txtPin.Enabled = !value;
                txtPuk.Enabled = !value;
                txtSimCard.Enabled = !value;
                txtUsedBy.Enabled = !value;
            }
        }

        public Contract Contract
        {
            get
            {
                return (Contract)ViewState["Contract"];
            }
            set
            {
                Clear();

                ViewState["Contract"] = value;
                if (value != null)
                {                    
                    NetworkId = PlanService.GetPlan(value.PlanId).NetworkId;
                    PlanId = value.PlanId;
                    ActivationDate = value.ActivationDate;
                    EndDate = value.EndDate;
                    ContractStatus = value.ContractStatus;
                    ContractNumber = value.ContractNumber;
                    IMEINumber = value.IMEINumber;
                    PhoneNumber1 = value.PhoneNumber1;
                    PhoneNumber2 = value.PhoneNumber2;
                    PhoneNumber3 = value.PhoneNumber3;
                    PhoneNumber4 = value.PhoneNumber4;
                    PhoneUsedBy = value.UsedBy;
                    Pin = value.Pin;
                    Puk = value.Puk;
                    SimCard = value.SimCard;
                    Data = value.Data;
                    MessageBank = value.MessageBank;
                }
            }
        }

        public void Clear()
        {
            chkData.Checked = false;
            chkMessageBank.Checked = false;

            txtActivationDate.Text = DateTime.Today.ToShortDateString();
            txtEndDate.Text = string.Empty;
            txtIMEINumber.Text = string.Empty;
            //txtPhone.Text = string.Empty;
            txtPhoneNumber1.Text = string.Empty;
            txtPhoneNumber2.Text = string.Empty;
            txtPhoneNumber3.Text = string.Empty;
            txtPhoneNumber4.Text = string.Empty;
            txtPin.Text = string.Empty;
            txtPuk.Text = string.Empty;
            txtSimCard.Text = string.Empty;
            txtUsedBy.Text = string.Empty;

            ResetNetworkId();
        }

        public void ResetNetworkId()
        {            
            NetworkId = ddlNetwork.Items.Count > 0 ? int.Parse(ddlNetwork.Items[0].Value) :0;
        }

        public void SetNew()
        {
            lblContractNumber.Text = Server.HtmlEncode("<NEW CONTRACT>");
            ContractStatus = ContractStatus.NewContract;

            Clear();
        }
        protected void NetworkChange(object sender, EventArgs e)
        {            
            var plans = PlanService.GetPlansByNetworkId(NetworkId);

            ddlPlan.DataSource = plans;
            ddlPlan.DataTextField = "Name";
            ddlPlan.DataValueField = "Id";
            ddlPlan.DataBind();

            if (plans != null && plans.Count > 0)
            {
                ConfigureForNewPlan(sender, e);
            }
        }

        public bool NewContract
        {
            get { return (bool)(ViewState["NewContract"] ?? false);  }
            set { ViewState["NewContract"] = value; }
        }
    }
}