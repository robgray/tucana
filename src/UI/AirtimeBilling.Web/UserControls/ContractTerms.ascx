﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ContractTerms.ascx.cs" Inherits="AirtimeBilling.Web.ContractTerms" %>
<%@ Register Assembly="skmValidators" Namespace="skmValidators" TagPrefix="cc1" %>

<asp:Panel ID="pnlSubmit" runat="server" CssClass="Terms">
    <asp:TextBox ID="txtTermsConditions" runat="server" ReadOnly="True" TextMode="MultiLine" Width="500px" Rows="10"></asp:TextBox>   
    <br />
    <asp:CheckBox ID="chkAgree" runat="server"/>    
    <cc1:CheckBoxValidator ID="CheckBoxValidator1" runat="server" ControlToValidate="chkAgree" MustBeChecked="true" 
    ErrorMessage="Contract terms must be accepted" Display="None" />
    <span>I agree to the Terms and Conditions</span>                   
    <div class="form-actions">
        <asp:LinkButton CssClass="btn btn-success" ID="SubmitButton" Text="Submit and Print" Icon="disk" runat="server" OnClick="SubmitNewContract">
            <i class="icon-ok icon-white"></i> Submit and Print
        </asp:LinkButton>
    </div>        
</asp:Panel>