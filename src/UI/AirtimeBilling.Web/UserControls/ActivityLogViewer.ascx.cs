﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class ActivityLogViewerControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindData();
            }
        }

        protected void gvActivityLog_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvActivityLog.PageIndex = e.NewPageIndex;
            BindData();
        }

        public EntityBase Entity
        {
            get
            {
                return ViewState["Entity"] as EntityBase;
            }
            set
            {
                ViewState["Entity"] = value;
                BindData();
            }
        }

        public string Username
        {
            get
            {
                return (string)ViewState["Username"];
            }
            set
            {
                ViewState["Username"] = value;
                BindData();
            }
        }

        /// <summary>
        /// Retrieves the Activity Log data from the DB.
        /// </summary>
        private void BindData()
        {
            IList<Activity> activities = null;
            if (Entity != null)
            {
                activities = ActivityLoggingService.FindLogEntriesForEntity(Entity);
            }
            else if (!string.IsNullOrEmpty(Username))
            {
                activities = ActivityLoggingService.FindLogEntriesForUser(Username);
            }

            gvActivityLog.DataSource = activities;
            gvActivityLog.DataBind();         
        }       
    }
}