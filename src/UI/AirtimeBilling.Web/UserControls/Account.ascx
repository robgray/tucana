﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.AccountControl" Codebehind="Account.ascx.cs" %>
<div class="row">
    <asp:HiddenField ID="hdnAccountId" runat="server" />
    <sharp:FormGroup ID="AccountInformation" runat="server" GroupingText="Account Information" CssClass="span6">    
        <div class="control-group" >
            <label class="control-label" for="<%= lblAccountNumber.ClientID %>">Account Number</label>
            <div class="controls">
                <asp:Label ID="lblAccountNumber" runat="server" Text='<%= Server.HtmlEncode("<SYSTEM GENERATED>") %>'></asp:Label>
                <asp:HiddenField ID="hdnAccountNumber" runat="server" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= txtAccountName.ClientID %>">Account Name</label>
            <div class="controls">
                <asp:Label ID="lblAccountName" runat="server" Text=""></asp:Label>
                <asp:TextBox ID="txtAccountName" runat="server" Width="150px"></asp:TextBox>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="<%= lblContactName.ClientID %>">Contact</label>
            <div class="controls">
                <asp:Label ID="lblContactName" runat="server" Text=""></asp:Label>
                <asp:HiddenField ID="hdnContactName" runat="server" />
            </div>
        </div>
        <asp:Panel ID="pnlCompany" runat="server" CssClass="control-group" Visible="false">
            <label class="control-label" for="<%= lblCompany.ClientID %>">Company</label>
            <div class="controls">
                <asp:Label ID="lblCompany" runat="server" Text=""></asp:Label>
                <asp:HiddenField ID="hdnCompanyName" runat="server" />
            </div>
        </asp:Panel>
        <div class="control-group">
            <label class="control-label" for="<%= txtPassword.ClientID %>">Password</label>
            <div class="controls">
                <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
                <asp:Label ID="lblPassword" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </sharp:FormGroup>
    <sharp:FormGroup ID="AccountBilling" runat="server" GroupingText="Account Billing" CssClass="span6">    
        <asp:Panel ID="InvoiceRootPanel" runat="server">
            <asp:Panel runat="server" ID="OutstandingBalancePanel" Style="border: 1px solid #b0b0b0;">                                    
                <br />
                <table cellpadding="0" cellspacing="0">                
                    <tr>
                        <td style="width:215px;">
                            <div class="control-group">
                                <label class="control-label">Previous Bill</label>
                                <div class="controls">
                                    <asp:Label ID="PreviousBillLabel" runat="server" Text="$0.00"  />
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="control-group">
                                <label class="control-label">Amount Paid</label>
                                <div class="controls">
                                    <asp:TextBox ID="AmountPaidTextBox" runat="server" Width="70px" style="border: 1px solid Green; background:#E6FFDB;" Enabled="false" />
                                </div>
                            </div>                            
                        </td>                    
                    </tr>
                    <tr>
                        <td colspan="2" style="height:30px; text-align: right;">
                            <asp:Image runat="server" ImageUrl="~/images/icons/money_add.png"/> <asp:LinkButton ID="MakupPaymentPopupButton" runat="server" Text="Make Payment" />
                        </td>
                    </tr>
                </table>            
            </asp:Panel>
            <br />
            <table cellpadding="0" cellspacing="0">
                <tr>
                    <td>
                    <div class="control-group">
                        <label class="control-label" for="<%= ddlBillingMethod.ClientID %>">Billing Method</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlBillingMethod" runat="server" Width="100px">
                                <asp:ListItem Text="Credit Card" Value="CreditCard"></asp:ListItem>
                                <asp:ListItem Text="Invoice" Value="Invoice"></asp:ListItem>                    
                            </asp:DropDownList>
                            <asp:Label ID="lblBillingMethod" runat="server" Text=""></asp:Label>
                        </div>                                
                    </div>            
                    <div class="control-group" style="display:none;">
                        <label class="control-label" for="<%= ddlBillingAddressType.ClientID %>">Billing Address</label>
                        <div class="controls">
                            <asp:DropDownList ID="ddlBillingAddressType" runat="server" Width="100px">
                                <asp:ListItem Text="Physical" Value="Physical"></asp:ListItem>
                                <asp:ListItem Text="Postal" Value="Postal"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="lblBillingAddressType" runat="server" Text=""></asp:Label>
                        </div>
                    </div>    
                    </td>        
                    <td>
                    <div class="control-group" style="display:none">                
                        <asp:HiddenField ID="hdnInvoiceRoot" runat="server" />
                        <label class="control-label" for="<%= chkInvoiceRoot.ClientID %>">Invoice Root</label>
                        <div class="controls">
                            <asp:CheckBox ID="chkInvoiceRoot" runat="server" Text="" Enabled="false" />                             
                        </div>
                    </div>    
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div class="control-group">
                        <label class="control-label" for="<%= chkEmailBillDataFile.ClientID %>">Email Data File</label>
                        <div class="controls">
                            <asp:CheckBox ID="chkEmailBillDataFile" runat="server" Text="" Enabled="false" />
                        </div>
                    </div>
                    </td>
                    <td>
                    <div class="control-group">
                        <label class="control-label" for="<%= chkEmailBill.ClientID %>">Paperless Bill</label>
                        <div class="controls">
                            <asp:CheckBox ID="chkEmailBill" runat="server" Text="" Enabled="false" />
                        </div>
                    </div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div class="control-group">
                        <label class="control-label" for="<%= chkPostBill.ClientID %>">Post Bill</label>
                        <div class="controls">
                            <asp:CheckBox ID="chkPostBill" runat="server" Text="" Enabled="false" />
                        </div>
                    </div>
                    </td>
                    <td>
                    <div class="control-group">
                        <label class="control-label" for="<%= chkInternational.ClientID %>">International Customer</label>
                        <div class="controls">
                            <asp:CheckBox ID="chkInternational" runat="server" Text="" Enabled="false" />
                        </div>
                    </div>
                    </td>
                </tr>                
            </table>
        </asp:Panel>
        <asp:Panel ID="NotInvoiceRootPanel" runat="server">
            <div>Account billing details are only available on the main Account for an Invoice.</div>            
        </asp:Panel>    
    </sharp:FormGroup>
</div>
<asp:Panel ID="MakePaymentPopupPanel" runat="server" CssClass="modal" style="display:none;min-height:0px">
        <div class="modal-header"><h3>Make Payment</h3></div>
        <div class="modal-body">
            <div class="alert alert-info">
                If there is an Invoice with Failed Payment, this amount will be paid against that Invoice.
            </div>             
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="<%= PaymentAmountTextBox.ClientID %>">Payment Amount</label>   
                    <div class="controls">
                        <asp:TextBox ID="PaymentAmountTextBox" runat="server" CssClass="Money" />                        
                    </div>         
                </div>                   
            </div>      
        </div>   
        <div class="modal-footer">
            <asp:LinkButton CssClass="btn btn-success" ID="MakePaymentButton" runat="server" Text="Make Payment"  CausesValidation="false" OnClick="MakePayment">
                <i class="icon-ok icon-white"></i> Make Payment
            </asp:LinkButton>                              
            <asp:LinkButton CssClass="btn" ID="CancelPaymentButton" runat="server" Text="Cancel" CausesValidation="false" />                              
        </div>       
</asp:Panel>
 <atk:ModalPopupExtender ID="MakePaymentPopupExtender" runat="server"
    Enabled="True" 
    TargetControlID="MakupPaymentPopupButton"
    CancelControlID="CancelPaymentButton"
    PopupControlID="MakePaymentPopupPanel"
    BackgroundCssClass="modal-backdrop">
</atk:ModalPopupExtender>