﻿using System;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Web
{
    public partial class AccountReadOnlyControl : BaseUserControl 
    {
        protected string BoxHeight
        {
            get { return Users.Current.IsInRole(Roles.BackOffice) ? "150px" : "140px";  }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //chkInvoiceRoot.Disabled = true;            
            OutstandingBalancePanel.Visible = Users.Current.IsInRole(Roles.BackOffice);
        }

        public int? AccountId
        {
            get
            {
                if (hdnAccountId.Value.Length > 0)
                {
                    int id;
                    if (int.TryParse(hdnAccountId.Value, out id))
                    {
                        if (id > 0)
                        {
                            return new int?(id);
                        }
                        return null;
                    }
                    return null;
                }
                return null;
            }
            private set
            {
                if (value != null)
                {
                    if (value.Value > 0)
                    {
                        hdnAccountId.Value = value.Value.ToString();                        
                    }
                    else
                    {
                        hdnAccountId.Value = string.Empty;
                    }
                }
                else
                {
                    hdnAccountId.Value = string.Empty;                   
                }
            }
        }

        protected void CheckLinkToMaintenance()
        {
            if (AccountId.HasValue && !string.IsNullOrEmpty(AccountNumber)) {
               
            }
        }

        public void DisplayLinkToInvoiceRoot(Account invoiceRoot)
        {
            if (invoiceRoot != null) {
                DisplayLinkToInvoiceRoot(invoiceRoot,
                                         "Account Billing details are available only on the main Account for the invoice." +
                                         "<br/><br/>" +
                                         string.Format(
                                             "Click <a href='{0}'>here</a> to navigate to that account",
                                             NavigationController.GetEditAccountLink(invoiceRoot.Id.Value)));
            }
            else {
                DisplayLinkToInvoiceRoot(invoiceRoot, "Account Billing details are available only on the main Account for the invoice.");
            }
        }

        public void DisplayLinkToInvoiceRoot(Account invoiceRoot, string message)
        {
            if (invoiceRoot != null)
            {
                InvoiceRootMessage.SetInformation(message);
                InvoiceRootPanel.Visible = false;
                NotInvoiceRootPanel.Visible = true;
            }
            else
            {
                InvoiceRootMessage.Clear();
                InvoiceRootPanel.Visible = true;
                NotInvoiceRootPanel.Visible = false;
            }
        }

        public string Contact
        {
            get
            {
                // Use a hidden field so javascript can change value
                // and value is posted back.
                return lblContactName.Text;                
            }
            set
            {
                lblContactName.Text = value;                
            }
        }

        public string Company
        {
            get
            {
                // Use a hidden field so javascript can change value
                // and value is posted back.
                return lblCompany.Text;                
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    pnlCompany.Visible = false;
                    lblCompany.Text = string.Empty;                    
                }
                else
                {
                    pnlCompany.Visible = true;
                    lblCompany.Text = value;                    
                }
            }
        }

        public string AccountNumber
        {
            get
            {
                // Use a hidden field so javascript can change value
                // and value is posted back.
                return ActualAccountNumber;            
            }
            private set
            {
                lblAccountNumber.Text = value;
            }
        }

        protected string ActualAccountNumber
        {
            get { return (string) ViewState["AccountNumber"];  }
            set { ViewState["AccountNumber"] = value; }
        }

        public void SetAccountNumberAndId(int accountId, string accountNumber)
        {
            AccountId = accountId;
            
            if (!string.IsNullOrEmpty(accountNumber)) 
            {
                ActualAccountNumber = accountNumber;
                accountNumber = "<a href='" + NavigationController.GetEditAccountLink(accountId) + "'>" + accountNumber + "</a>";
            }
            else {
                ActualAccountNumber = "<SYSTEM GENERATED>";               
                accountNumber = Server.HtmlEncode("<SYSTEM GENERATED>");
            }

            AccountNumber = accountNumber;            
        }

        public string AccountName
        {
            get { return lblAccountName.Text; }
            set
            {
                lblAccountName.Text = value;                
            }
        }

        public decimal AmountPaid
        {
            get { return AmountPaidLabel.Text.ToDecimalCurrency(); }
            set { AmountPaidLabel.Text = value.ToString("c"); }
        }

        public decimal PreviousBill
        {
            get { return PreviousBillLabel.Text.ToDecimalCurrency(); }
            set { PreviousBillLabel.Text = value.ToString("c"); }
        }

        public string ContactRole
        {
            get { return ""; }            
        }

        public BillingMethod BillingMethod
        {
            get
            {                
                var method = Enum<BillingMethod>.GetEnumFromDescription(lblBillingMethod.Text);
                return method;
            }
            set
            {
                var name = value.GetDescription();
                lblBillingMethod.Text = name;             
            }
        }

        public BillingAddressType BillingAddressType
        {
            get
            {                
                var type = Enum<BillingAddressType>.GetEnumFromDescription(lblBillingAddressType.Text);
                return type;
            }
            set
            {                
                var name = value.GetDescription();
                lblBillingAddressType.Text = name;                
            }
        }

        public bool EmailBillDataFile
        {
            get { return true; }
            set 
            {
                EmailBillDataFileImage.ImageUrl = GetImageUrl(value);
            }
        }

        protected string GetImageUrl(bool value)
        {
            return "~/images/icons/"  + (value ? "tick.png" : "cross.png");
        }

        public bool EmailBill
        {
            get { return true; }
            set { PaperlessBillImage.ImageUrl = GetImageUrl(value); }
        }

        public bool PostBill
        {
            get { return true; }
            set { PostBillImage.ImageUrl = GetImageUrl(value); }
        }

        public string Password
        {
            get { return lblPassword.Text; }
            set
            {
                lblPassword.Text = value;
            }
        }

        public bool IsInternational
        {
            get { return true; }
            set { InternationalImage.ImageUrl = GetImageUrl(value); }            
        }

        public bool IsInvoiceRoot
        {
            get
            {
                // Because IE does not postback disabled controls, 
                // we will not know the correct value of this.
                // Thus we need to get the correct value from the hidden control.               
                return false;
            }
            set
            {
                InvoiceRootImage.ImageUrl = GetImageUrl(value);
            }
        }
               
        public bool ReadOnly { get; set; }

        public bool EnableLinkToMaintenance
        {
            get { return (bool) (ViewState["linktomaint"] ?? false); }
            set { ViewState["linktomaint"] = value; }
        }
    }
}