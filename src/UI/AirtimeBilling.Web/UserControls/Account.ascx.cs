﻿using System;
using System.Web.UI;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Logging;

namespace AirtimeBilling.Web
{
    public partial class AccountControl : BaseUserControl 
    {
        private bool _readOnly = false;

        protected string BoxHeight
        {
            get { return Users.Current.IsInRole(Roles.BackOffice) ? "185px" : "150px";  }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //chkInvoiceRoot.Disabled = true;            
            OutstandingBalancePanel.Visible = Users.Current.IsInRole(Roles.BackOffice);
        }

        public int? AccountId
        {
            get
            {
                if (hdnAccountId.Value.Length > 0)
                {
                    int id;
                    if (int.TryParse(hdnAccountId.Value, out id))
                    {
                        if (id > 0)
                        {
                            return new int?(id);
                        }
                        return null;
                    }
                    return null;
                }
                return null;
            }
            set
            {
                if (value != null)
                {
                    if (value.Value > 0)
                    {
                        hdnAccountId.Value = value.Value.ToString();                        
                    }
                    else
                    {
                        hdnAccountId.Value = string.Empty;
                    }
                }
                else
                {
                    hdnAccountId.Value = string.Empty;                   
                }
            }
        }
      
        public string Contact
        {
            get
            {
                // Use a hidden field so javascript can change value
                // and value is posted back.
                lblContactName.Text = hdnContactName.Value;
                return hdnContactName.Value;
            }
            set
            {
                lblContactName.Text = value;
                hdnContactName.Value = value;
            }
        }

        public string Company
        {
            get
            {
                // Use a hidden field so javascript can change value
                // and value is posted back.
                lblCompany.Text = hdnCompanyName.Value;
                return hdnCompanyName.Value;
            }
            set
            {
                pnlCompany.Visible = !string.IsNullOrEmpty(value);                                    
                lblCompany.Text = value;
                hdnCompanyName.Value = value;
            }
        }

        public string AccountNumber
        {
            get
            {
                // Use a hidden field so javascript can change value
                // and value is posted back.
                lblAccountNumber.Text = hdnAccountNumber.Value;
                return hdnAccountNumber.Value;
            }
            set
            {
                lblAccountNumber.Text = string.IsNullOrEmpty(value) ? Server.HtmlEncode("<SYSTEM GENERATED>") : value;
                hdnAccountNumber.Value = value;
            }
        }

        public string AccountName
        {
            get { return txtAccountName.Text; }
            set
            {
                lblAccountName.Text = value;
                txtAccountName.Text = value;
            }
        }

        public decimal AmountPaid
        {
            get { return AmountPaidTextBox.Text.ToDecimalCurrency(); }
            set { AmountPaidTextBox.Text = value.ToString("c"); }
        }

        public string ContactRole
        {
            get { return ""; }            
        }

        public decimal PreviousBill
        {
            get { return PreviousBillLabel.Text.ToDecimalCurrency(); }
            set 
            {
                PreviousBillLabel.Text = value.ToString("c"); 
            }
        }
        
        public BillingMethod BillingMethod
        {
            get
            {                
                var method = Enum<BillingMethod>.GetEnumFromDescription(ddlBillingMethod.SelectedValue);
                return method;
            }
            set
            {
                var name = value.GetDescription();
                lblBillingMethod.Text = name;
                var item = ddlBillingMethod.Items.FindByValue(name);
                if (item != null)
                {
                    ddlBillingMethod.SelectedItem.Selected = false;
                    item.Selected = true;
                }
            }
        }

        public BillingAddressType BillingAddressType
        {
            get
            {                
                var type = Enum<BillingAddressType>.GetEnumFromDescription(ddlBillingAddressType.SelectedValue);
                return type;
            }
            set
            {                
                var name = value.GetDescription();
                lblBillingAddressType.Text = name;
                var item = ddlBillingAddressType.Items.FindByValue(name);
                if (item != null)
                {
                    ddlBillingAddressType.SelectedItem.Selected = false;
                    item.Selected = true;
                }
            }
        }

        public bool EmailBillDataFile
        {
            get { return chkEmailBillDataFile.Checked; }
            set { chkEmailBillDataFile.Checked = value; }
        }

        public bool EmailBill
        {
            get { return chkEmailBill.Checked; }
            set { chkEmailBill.Checked = value; }
        }

        public bool PostBill
        {
            get { return chkPostBill.Checked; }
            set { chkPostBill.Checked = value; }
        }

        public string Password
        {
            get { return txtPassword.Text; }
            set
            {
                lblPassword.Text = value;
                txtPassword.Text = value;
            }
        }

        public bool IsInternational
        {
            get { return chkInternational.Checked; }  
            set { chkInternational.Checked = value; }            
        }

        public bool IsInvoiceRoot
        {
            get
            {
                // Because IE does not postback disabled controls, 
                // we will not know the correct value of this.
                // Thus we need to get the correct value from the hidden control.
                bool root;
                if (bool.TryParse(hdnInvoiceRoot.Value, out root))
                {
                    chkInvoiceRoot.Checked = true;
                    return root;
                }
                chkInvoiceRoot.Checked = false;
                return false;
            }
            set
            {
                chkInvoiceRoot.Checked = value;

                InvoiceRootPanel.Visible = value;
                NotInvoiceRootPanel.Visible = !value;

                // Also need to set the hidden control.
                hdnInvoiceRoot.Value = value.ToString();
            }
        }
        
        public bool ShowBillingDetails
        {
            get
            {
                return true;
            }
        }

        public bool ReadOnly
        {
            get
            {
                return _readOnly;
            }
            set
            {
                _readOnly = value;

                // Modify to show disabled textboxes instead of labels.

                lblBillingAddressType.Visible = false;
                lblBillingMethod.Visible = false;
                lblPassword.Visible = false;
                lblAccountName.Visible = false;

                chkEmailBill.Enabled = !value;
                chkEmailBillDataFile.Enabled = !value;
                chkPostBill.Enabled = !value;
                chkInternational.Enabled = !value;
                ddlBillingAddressType.Enabled = !value;
                ddlBillingMethod.Enabled = !value;
                txtPassword.Enabled = !value;
                txtAccountName.Enabled = !value;
            }
        }

        protected void MakePayment(object sender, EventArgs e)
        {
            try {
                var amount = PaymentAmountTextBox.Text.ToDecimalCurrency();
                
                var response = AccountService.MakePayment(AccountId.Value, amount);
                if (response.IsSuccessful) {
                    AmountPaid = response.Account.AmountPaid;
                    MakePaymentPopupExtender.Hide();
                }                
            } 
            catch (FormatException) {}
            catch (Exception ex) {
                LoggingUtility.LogException(Users.Current.Username, ex);
            }
        }
    }
}