﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.SettingsPage" Codebehind="SystemSettings.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Settings</h1>
<hr />
<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">System Settings</a></li>
        <li><a href="#tab2" data-toggle="tab">User Settings</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <sharp:MessagePanel ID="SystemSettingMessage" runat="server" />                    
            <sharp:FormGroup ID="SystemSettingsPageFG" runat="server" style="float:none;">                        
                <asp:GridView ID="gvReadOnlySettings" runat="server" 
                        AutoGenerateColumns="False">                
                    <Columns>                                       
                        <asp:BoundField DataField="Name" HeaderText="Setting" Visible="True">        
                            <HeaderStyle  Width="400px" />
                            <ItemStyle  Width="150px" Height="18px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Value" HeaderText="Value" Visible="True" >                   
                            <HeaderStyle  Width="150px" />
                            <ItemStyle  Width="150px" />
                        </asp:BoundField>
                    </Columns>        
                </asp:GridView>
            </sharp:FormGroup>
        </div>
        <div class="tab-pane" id="tab2">
            <sharp:MessagePanel ID="UserSettingsMessage" runat="server" />        
            <sharp:FormGroup ID="UserSettingsPageFG" runat="server" style="float:none;">                        
                <asp:GridView ID="gvUserSettings" runat="server"                     
                        AutoGenerateColumns="False"
                        onrowcancelingedit="gvUserSettings_RowCancelingEdit" 
                        onrowediting="gvUserSettings_RowEditing" 
                        onrowupdating="gvUserSettings_RowUpdating"  DataKeyNames="Id">                
                    <Columns>        
                        <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/cross.gif" 
                                    EditImageUrl="~/images/pencil.gif" ShowEditButton="True" 
                                    UpdateImageUrl="~/images/disk.gif" UpdateText="Save" ItemStyle-Width="40px" HeaderStyle-Width="40px" />
                        <asp:BoundField DataField="SystemSettingId" HeaderText="Id" Visible="False" />
                        <asp:TemplateField>
                            <HeaderStyle Width="600px" />              
                            <HeaderTemplate>
                                Setting
                            </HeaderTemplate>  
                            <ItemStyle Width="600px" />                                        
                            <ItemTemplate>
                                <asp:Label Id="lblKeyName" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>                  
                        <asp:BoundField DataField="Value" HeaderText="Value" Visible="True" >                   
                            <HeaderStyle Width="250px" />
                            <ItemStyle Width="250px" />
                        </asp:BoundField>
                    </Columns>        
                </asp:GridView>
            </sharp:FormGroup>        
        </div>
  </div>
</div>
</asp:Content>

