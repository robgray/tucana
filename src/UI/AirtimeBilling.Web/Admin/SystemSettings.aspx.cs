﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.DataAccess.Repositories;

namespace AirtimeBilling.Web
{
    public partial class SettingsPage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            SystemSettingMessage.SetWarning(
                "The following settings are read-only system settings and can only be modified by Software Support", false);


            UserSettingsMessage.SetWarning("User settings can be modified by the user but this task should only be " +
                                            "performed when the results of the modification are known.  " +
                                            "Modifying these settings without fully understanding the results will cause system instability.", false);

            if (!IsPostBack)
            {
                var rep = RepositoryFactory.GetRepository<ISettingRepository>();
                var settings = rep.GetAllSystemSettings();
                gvReadOnlySettings.DataSource = settings;
                gvReadOnlySettings.DataBind();

                PopulateUserSettings();                
            }            
        }

        private void PopulateUserSettings()
        {
            var rep = RepositoryFactory.GetRepository<ISettingRepository>();
            var userSettings = rep.GetAllUserSettings();
            gvUserSettings.DataSource = userSettings;
            gvUserSettings.DataBind();            
        }

        protected void gvUserSettings_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvUserSettings.EditIndex = e.NewEditIndex;
            PopulateUserSettings();
        }

        protected void gvUserSettings_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var value = gvUserSettings.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox;

            if (value == null)
            {
                throw new InvalidOperationException(
                    "Could not find the textbox with this value.  This should not happen.  Ever!");
            }

            if (gvUserSettings.DataKeys[e.RowIndex] == null)
            {
                throw new InvalidOperationException("Could not find DataKey for this row");
            }

            var rep = RepositoryFactory.GetRepository<ISettingRepository>();            
            var settingId = (int) gvUserSettings.DataKeys[e.RowIndex].Value;
            var setting = rep[settingId];
            if (setting != null)
            {
                setting.Value = value.Text;
            }

            rep.Commit(setting);

            gvUserSettings.EditIndex = -1;
            PopulateUserSettings();
        }

        protected void gvUserSettings_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvUserSettings.EditIndex = -1;
            PopulateUserSettings();
        }        
    }    
}