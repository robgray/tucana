﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class LogViewer : BackOfficeBasePage
    {
        public LogEntry SelectedEntry { get; set; }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            UserMessage.Clear();
            NoRecords.Clear();

            if (!IsPostBack) {
                // Load the query controls
                StartDateTextBox.Text = DateTime.Today.ToString("dd/MM/yyyy");
                EndDateTextBox.Text = DateTime.Today.ToString("dd/MM/yyyy");
            }            
        }        

        protected void SearchLog(object sender, EventArgs e)
        {
            var startDate = DateTime.Parse(StartDateTextBox.Text);
            var endDate = DateTime.Parse(EndDateTextBox.Text);

            if (startDate > endDate) {
                DateTime tempDate = endDate;
                endDate = startDate;
                startDate = tempDate;

                StartDateTextBox.Text = startDate.ToString("dd/MM/yyyy");
                EndDateTextBox.Text = endDate.ToString("dd/MM/yyyy");
            }

            endDate = endDate.AddDays(1).AddMilliseconds(-1);
                
            IList<LogEntry> logEntries = new List<LogEntry>();

            switch (CategoryDropDown.SelectedValue) {                
                case "1":
                    logEntries = LogReaderService.Find(startDate, endDate, UserTextBox.Text, LoggingLevels.DebugInfo);
                    break;
                case "2":
                    logEntries = LogReaderService.Find(startDate, endDate, UserTextBox.Text, LoggingLevels.Warning);
                    break;
                case "3":
                    logEntries = LogReaderService.Find(startDate, endDate, UserTextBox.Text, LoggingLevels.Exception);
                    break;
                default:
                    logEntries = LogReaderService.Find(startDate, endDate, UserTextBox.Text);
                    break;
            }

            gvLog.DataSource = LogEntryDisplay.GetList(logEntries);
            gvLog.DataBind();

            if (logEntries.Count == 0) {
                NoRecords.SetInformation("No log entries were found matching your criteria.");
            }

        }

        protected void gvLog_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {            
            var entry = LogReaderService.ReadLogEntry(args.Key);            
            if (entry != null) {                
                CategoryLabel.Text = entry.Level.ToString();
                EntryTimeLabel.Text = entry.Timestamp.ToString("dd MMM yyyy HH:mm:ss");
                UserLabel.Text = entry.Username;
                MethodNameLabel.Text = entry.MethodName;
                ClassLabel.Text = entry.ClassName;
                MessageLabel.Text = entry.Message;
                StackTraceLabel.Text = entry.StackTrace;
                LogDetailPopup.Show();
            }
        }

        protected void ClosePopup(object sender, EventArgs e)
        {
            LogDetailPopup.Hide();
        }
    }

    public class LogEntryDisplay 
    {
        public static IList<LogEntryDisplay> GetList(IList<LogEntry> logEntries)
        {
            IList<LogEntryDisplay> logs = new List<LogEntryDisplay>();
            foreach (var logEntry in logEntries) {
                logs.Add(Create(logEntry));
            }

            return logs;
        }

        public static LogEntryDisplay Create(LogEntry entry)
        {
            var log =  new LogEntryDisplay
                       {
                           Id = entry.Id.Value,
                           Message = entry.Message,
                           Username = entry.Username,
                           Timestamp = entry.Timestamp,
                       };

            if (entry.Level == LoggingLevels.Warning)
                log.TypeImageUrl = "~/images/icons/warning-icon-sml.png";
            else if (entry.Level == LoggingLevels.Exception)
                log.TypeImageUrl = "~/images/icons/error-icon-sml.png";
            else
                log.TypeImageUrl = "~/images/icons/info-icon-small.png";

            return log;
        }


        public int Id { get; set; }

        public DateTime Timestamp { get; set; }

        public string Message { get; set; }

        public string Username { get; set; }

        public string TypeImageUrl { get; set; }
    }
}
