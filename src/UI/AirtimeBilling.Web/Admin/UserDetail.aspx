﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.UserDetailPage" Codebehind="UserDetailPage.aspx.cs" %>
<%@ Register src="UserControls/ManageUsers.ascx" tagname="ManageUsersControl" TagPrefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
    <h1>Manage User Accounts</h1>
    <hr />
    <sharp:ManageUsersControl ID="ManageUsersControl1" CancelNavigateUrl="~/Admin/UserList.aspx" runat="server" />
</asp:Content>

