﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.UserListPage" Codebehind="UserList.aspx.cs" %>
<%@ Register src="~/Admin/UserControls/Users.ascx" tagname="Users" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
    <h1>Manage User Accounts</h1>
    <hr />    
    <sharp:Users ID="Users1" EditNavigateUrl="~/Admin/UserDetail.aspx" runat="server" />            
</asp:Content>

