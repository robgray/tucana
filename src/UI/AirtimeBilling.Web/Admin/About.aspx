﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="AirtimeBilling.Web.About" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>About</h1>
<hr />
<div class="Form">
    <sharp:FormGroup ID="VersionsFG" runat="server" GroupingText="Version Information" CssClass="module">
        <table class="form">
            <tr>
                <th>Software Version</th>
                <td><%= AirtimeBilling.Web.Version.GetVersionNumber()%></td>
            </tr>
            <tr>
                <th>SatCom Specification Version</th>
                <td>Revision 1 (28th March 2008)</td>        
            </tr>
            <tr>
                <th>Vizada Specification Version</th>
                <td>Unknown</td>
            </tr>
        </table>
    </sharp:FormGroup>
</div>
<br class="RowSplitter" />
</asp:Content>
