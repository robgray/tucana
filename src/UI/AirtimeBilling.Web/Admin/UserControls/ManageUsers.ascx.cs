﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Logging;
using System.Web.Security;
using System.Web.Profile;
using System.Web;

namespace AirtimeBilling.Web
{
    public partial class ManageUsersControl : BaseUserControl
    {
        enum Tabs
        {
            Add, 
            Details,
            Password,
            Delete
        }

        private string User = string.Empty;
        private int TabId = 1;
        private string _CancelNavigateUrl;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            TabId = 0;
            if (Request.QueryString["user"] != null)
            {
                User = (string)Request.QueryString["user"];                
            }

            if (Request.QueryString["tabid"] != null)
            {
                TabId = int.Parse(Request.QueryString["tabid"]);
            } else {
                if (!string.IsNullOrEmpty(User)) {
                    TabId = 1;
                }
            }


            //if this is the first visit to the page, bind the data
            if (!Page.IsPostBack)
            {
                switch (TabId)
                {
                    case 0: 
                        SetTab(Tabs.Add);
                        break;
                    case 1:                        
                        SetTab(Tabs.Details);
                        break;
                    case 3:
                        SetTab(Tabs.Password);
                        break;
                    case 4:
                        SetTab(Tabs.Delete);
                        break;
                }

                BindData();

                rbAgent.Checked = false;
                rbCustomer.Checked = false;
                rbBackOffice.Checked = false;
                
                if (Users.IsInRole(User, Roles.Agent)) {
                    rbAgent.Checked = true;
                    SetAssociationDisplay(Roles.Agent);
                    
                    AgentUser agent = (AgentUser)Users.GetUser(User);
                    ListItem item = ddlSelector.Items.FindByValue(agent.AgentId.ToString());
                    if (item != null)
                    {
                        ddlSelector.SelectedItem.Selected = false;
                        item.Selected = true;
                    }
                }
                else if (Users.IsInRole(User, Roles.Customer)) {
                    rbCustomer.Checked = true;
                    SetAssociationDisplay(Roles.Customer);

                    ContactUser contact = (ContactUser)Users.GetUser(User);
                    ListItem item = ddlSelector.Items.FindByValue(contact.ContactId.ToString());
                    if (item != null)
                    {
                        ddlSelector.SelectedItem.Selected = false;
                        item.Selected = true;
                    }
                }
                else {
                    rbBackOffice.Checked = true;
                    SetAssociationDisplay(Roles.BackOffice);
                }                
            }
        }

        private void SetTab(Tabs tab)
        {            
            pnlDeleteUser.Visible = tab == Tabs.Delete;
            pnlUserDetailPages.Visible = tab == Tabs.Details;
            pnlUserPassword.Visible = tab == Tabs.Password;

            DeleteUserButton.Enabled = tab != Tabs.Add;
            UserDetailPagesButton.Enabled = tab != Tabs.Add;
            PasswordButton.Enabled = tab != Tabs.Add;

            cmdUpdate.Visible = tab != Tabs.Add;
            
            pnlAddNewUser.Visible = tab == Tabs.Add;
        }

        /// <summary>
        /// Gets or sets the cancel navigate URL.
        /// </summary>
        /// <value>The cancel navigate URL.</value>
        public string CancelNavigateUrl
        {
            get { return _CancelNavigateUrl; }
            set { _CancelNavigateUrl = value; }
        }

        /// <summary>
        /// Binds the data.
        /// </summary>
        private void BindData()
        {
            if (String.IsNullOrEmpty(User))
            {
                CreateUserWizard1.CancelDestinationPageUrl = _CancelNavigateUrl;
            }
            else
            {
                UserName.Enabled = false;
                User objUser = Users.GetUser(User);

                if (objUser != null)
                {                    
                    UserName.Text = objUser.Username;
                    Email.Text = objUser.Email;
                    Active.Checked = objUser.IsActive;
                }
            }
        }
     

        #region Button Events
        /// <summary>
        /// Handles the Click event of the cmdChangePassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void cmdChangePassword_Click(object sender, EventArgs e)
        {
            lblMessage.Visible = false;
            if (cvPasswords.IsValid)
            {
                User objUser = Users.GetUser(User);
                if (objUser != null)
                {
                    try
                    {
                        Users.ChangePassword(objUser, NewPassword.Text);
                        lblMessage.Visible = true;
                        lblMessage.Text = "Password changed succesfully";
                    }
                    catch (Exception)
                    {
                        //TODO: Log this error
                        lblMessage.Visible = true;
                        lblMessage.Text = "The password could not be changed, please verify that the password is 7 characters or more.";
                    }

                }
            }

        }

        /// <summary>
        /// Handles the Click event of the cmdUpdate control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void cmdUpdate_Click(object sender, EventArgs e)
        {
            try
            {                
                // Guid UserGuid = new System.Guid(User);
                // MembershipUser objUser = UserIT.GetUser(UserGuid);

                // Need to use the MembershipUser and profile classes directly here because 
                // we may need to convert between AgentUser, ContractUserEntity, 
                // User.  The easiest way to do that is to deal directly with those classes
                // Then call Users.GetUser to get the new type of User.
                MembershipUser objUser = Membership.GetUser(User);
                if (objUser != null)
                {
                    WebProfile prof = WebProfile.GetProfile(User);
                    if (prof == null) prof = new WebProfile();

                    objUser.IsApproved = Active.Checked;
                    objUser.UnlockUser();
                    objUser.Email = Email.Text;
                    if (rbAgent.Checked)
                    {
                        prof.AgentId = int.Parse(ddlSelector.SelectedValue);
                        prof.ContactId = 0;
                    }
                    else if (rbCustomer.Checked)
                    {
                        prof.AgentId = 0;
                        prof.ContactId = int.Parse(ddlSelector.SelectedValue);
                    }
                    else
                    {
                        prof.AgentId = 0;
                        prof.ContactId = 0;
                    }
                    Membership.UpdateUser(objUser);
                    prof.Save();

                    Server.Transfer(_CancelNavigateUrl, false);
                }
            }
            catch (Exception ex)
            {
                //TODO:log this error, display a friendly error message
                ErrorMessage.Message = ex.Message;
            }
        }

        /// <summary>
        /// Handles the CreatedUser event of the CreateUserWizard1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void CreateUserWizard1_CreatedUser(object sender, EventArgs e)
        {
            User user = Users.GetUser(CreateUserWizard1.UserName);
            if (user != null)
            {
                string message = "Welcome to the I online phone system<br/><br/>" +
                    "<span style=\"margin-left: 40px\"><table><tr><td>Username</td><td>" + CreateUserWizard1.UserName + "</td></tr>" +
                    "<tr><td>Password</td><td>" + CreateUserWizard1.Password + "</td></tr></table>";
                LoggingUtility.SendEmail(user.Email, "I Online Phone System", message, false);
            }
        }

        /// <summary>
        /// Handles the Click event of the cmdCancel control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(CancelNavigateUrl, true);
        }

        /// <summary>
        /// Handles the Click event of the cmdUnauthorizeAccount control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void cmdUnauthorizeAccount_Click(object sender, EventArgs e)
        {
            try
            {
                //Guid UserGuid = new System.Guid(User);
                //MembershipUser objUser = UserIT.GetUser(UserGuid);
                User objUser = Users.GetUser(User);
                objUser.IsActive = false;
                Users.Update(objUser);
                BindData();
                ManageDetails(this, null);
                cmdUpdate.Visible = true;

            }
            catch (Exception ex)
            {
                ErrorMessage.Message = ex.Message;
            }
        }

        /// <summary>
        /// Handles the Click event of the cmdDeleteUser control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void DeleteUser(object sender, EventArgs e)
        {
            try
            {
                //Guid UserGuid = new System.Guid(User);
                //MembershipUser objUser = UserIT.GetUser(UserGuid);
                User objUser = Users.GetUser(User);
                Users.Delete(objUser.Username);

                Server.Transfer(CancelNavigateUrl, false);
            }
            catch (Exception ex)
            {
                ErrorMessage.Message = ex.Message;
            }
        }

        /// <summary>
        /// Handles the Click event of the cmdDelete control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void DeleteUserTab(object sender, EventArgs e)
        {
            SetTab(Tabs.Delete);
        }

        


        /// <summary>
        /// Handles the Click event of the cmdManageDetails control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void ManageDetails(object sender, EventArgs e)
        {
            SetTab(Tabs.Details);
        }

        /// <summary>
        /// Handles the Click event of the cmdManagePassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void ManagePassword(object sender, EventArgs e)
        {
            SetTab(Tabs.Password);
        }

        /// <summary>
        /// Handles the Click event of the cmdResetPassword control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void cmdResetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                //Guid UserGuid = new System.Guid(User);
                //MembershipUser objUser = UserIT.GetUser(UserGuid);
                User objUser = Users.GetUser(User);
                string password = Users.ResetPassword(objUser.Username);

                if (password != null)
                {
                    LoggingUtility.SendEmail(objUser.Email, "New I Online Password",
                        string.Format("Your password has been reset.  Your new password is {0}", password), true);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Message = ex.Message;
            }
        }
        #endregion

        protected void TypeCheckChanged(object sender, EventArgs e)
        {            
            var role = rbAgent.Checked ? Roles.Agent : rbCustomer.Checked ? Roles.Customer : Roles.BackOffice;
            SetAssociationDisplay(role);
        }

        protected void SetAssociationDisplay(Roles role)
        {
            AssociationPanel.Visible = role == Roles.Agent || role == Roles.Customer;
            if (rbAgent.Checked)
            {
                ddlSelector.DataSource = AgentService.GetAllAgents();
                ddlSelector.DataTextField = "AgentName";
                ddlSelector.DataValueField = "Id";
                ddlSelector.DataBind();
            }
            else if (rbCustomer.Checked)
            {
                ddlSelector.DataSource = ContactService.GetAllContacts();
                ddlSelector.DataTextField = "Name";
                ddlSelector.DataValueField = "Id";
                ddlSelector.DataBind();
            }         
        }
    }
}