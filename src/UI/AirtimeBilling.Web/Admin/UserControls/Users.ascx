﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.UsersControl" Codebehind="Users.ascx.cs" %>
<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>
<div class="formcontent">    
    <div class="well form-inline">
        <label>Search</label>    
        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>         
        <asp:DropDownList ID="SearchField" runat="server" style="vertical-align: middle">
            <asp:ListItem Text="Username" Value="User Name"></asp:ListItem>
            <asp:ListItem Text="Email" Value="Email"></asp:ListItem>        
        </asp:DropDownList>        
        <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" runat="server" Text="Search" Icon="search"  CausesValidation="false" OnClick="SearchUsers">
            <i class="icon-search icon-white"></i> Search
        </asp:LinkButton>
    </div>         
    <br />
    <br />    
    <asp:Panel ID="plLetterSearch" Runat="server" HorizontalAlign="Center">
        <asp:Repeater ID="LetterSearch" runat="server">
            <ItemTemplate>    
		        <asp:linkbutton ID="FilterButton" OnClick="FilterButton_Click" runat="server" CssClass="CommandButton"  CommandArgument="<%# Container.DataItem %>" CommandName="Filter" Text='<%# Container.DataItem %>'>
		        </asp:linkbutton>&nbsp;&nbsp;
	        </ItemTemplate>
        </asp:Repeater>
    </asp:Panel>
    <br />
    <asp:GridView ID="gvUsers" runat="server" Width="100%" 
        OnRowDataBound="gvUsers_RowDataBound" 
        OnRowCommand="gvUsers_RowCommand" 
        AutoGenerateColumns="False" 
        HorizontalAlign="Center"   
        CellSpacing="0" 
        CellPadding="2" 
        ForeColor="#333333" 
        GridLines="None" 
        AllowPaging="True"
        PagerSettings-FirstPageImageUrl="~/images/resultset_first.gif"
        PagerSettings-LastPageImageUrl="~/images/resultset_last.gif"
        PagerSettings-PreviousPageImageUrl="~/images/resultset_previous.gif"
        PagerSettings-Mode="NumericFirstLast"
        PagerSettings-NextPageImageUrl="~/images/resultset_next.gif"
        OnPageIndexChanging="gvUsers_PageIndexChanging"
        PageSize="15"
         HeaderStyle-HorizontalAlign="left"
        PagerStyle-HorizontalAlign="right">
        <HeaderStyle CssClass="gen-header" />
	    <AlternatingRowStyle CssClass="alt" />
        <Columns>
            <asp:TemplateField>
                <HeaderStyle Width="50px" />
                <ItemStyle Width="50px" />
                <ItemTemplate >
                    <asp:ImageButton ID="Edit" runat="server" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Username") %>' AlternateText="Edit User" ImageUrl="~/images/pencil.gif" />&nbsp;
                    <asp:ImageButton ID="Delete" runat="server" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Username") %>' AlternateText="Delete User" ImageUrl="~/images/cross.gif" />&nbsp;                
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UserName" HeaderText="Username" ReadOnly="True" SortExpression="UserName" />
            <asp:BoundField DataField="Email" HeaderText="Email" SortExpression="Email" />  
            <asp:CheckBoxField DataField="IsActive" HeaderText="Active" SortExpression="IsActive" />
        </Columns>
        <EmptyDataTemplate>            
            <sharp:MessagePanel runat="server" ID="NoUsersMessage" MessageType="alert-info" Message="No users matching this selection have been found."  IncludeClose="False"/>                
        </EmptyDataTemplate>
    </asp:GridView>
    <div class="well">    
        <asp:LinkButton CssClass="btn btn-primary" ID="AddUserButton" runat="server" Text="Add User" Icon="adduser"  onclick="AddUser" />
    </div>
</div>