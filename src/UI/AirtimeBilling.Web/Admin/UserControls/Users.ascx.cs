﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Profile;
using System.Web.Security;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class UsersControl : BaseUserControl
    {
        private string _EditNavigateUrl;

        /// <summary>
        /// Handles the Load event of the Page control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CreateLetterSearch();
                BindData(string.Empty);
            }
        }
        /// <summary>
        /// Gets or sets the edit navigate URL.
        /// </summary>
        /// <value>The edit navigate URL.</value>
        public string EditNavigateUrl
        {
            get { return _EditNavigateUrl; }
            set { _EditNavigateUrl = value; }
        }

        /// <summary>
        /// Creates the letter search.
        /// </summary>
        private void CreateLetterSearch()
        {
            string[] Alphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "All", "Unauthorized" };
            LetterSearch.DataSource = Alphabet;
            LetterSearch.DataBind();
        }

        /// <summary>
        /// Handles the RowCommand event of the gvUsers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Web.UI.WebControls.GridViewCommandEventArgs"/> instance containing the event data.</param>
        protected void gvUsers_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Edit":
                    Response.Redirect(_EditNavigateUrl + "?user=" + e.CommandArgument.ToString());
                    break;
                case "ManageRoles":
                    Response.Redirect(_EditNavigateUrl + "?user=" + e.CommandArgument.ToString() + "&tabid=2");
                    break;
                case "Delete":
                    Response.Redirect(_EditNavigateUrl + "?user=" + e.CommandArgument.ToString() + "&tabid=4");
                    break;

            }
        }

        /// <summary>
        /// Gets or sets the search filter.
        /// </summary>
        /// <value>The search filter.</value>
        protected string SearchFilter
        {
            get { return (string)ViewState["SearchFilter"]; }
            set { ViewState["SearchFilter"] = value; }
        }

        /// <summary>
        /// Binds the data.
        /// </summary>
        /// <param name="filter">The filter.</param>
        private void BindData(string filter)
        {
            SearchFilter = filter;
            string SearchText = SearchFilter;
            switch (filter)
            {
                case "All":
                    SearchText = string.Empty;
                    break;
                case "Unauthorized":
                    SearchText = string.Empty;
                    break;
                default:
                    SearchText = filter + "%";
                    break;
            }

            //MembershipUserCollection users;
            IList<User> users = null;
            if (String.IsNullOrEmpty(SearchText))
            {
                users = Users.GetAllUsers();
            }
            else
            {
                users = Users.FindUsersByName(SearchText);
            }

            if (filter == "Unauthorized")
            {
                IList<User> unauthenticatedUsers = new List<User>();
                foreach (User user in users)
                {
                    if (!user.IsActive)
                        unauthenticatedUsers.Add(user);
                }
                users = unauthenticatedUsers;
            }

            gvUsers.DataSource = users;
            gvUsers.DataBind();

        }
        /// <summary>
        /// Filters the URL.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="currentPage">The current page.</param>
        protected string FilterUrl(object filter, string currentPage)
        {
            string f = (string)filter;
            string url = Page.TemplateControl.AppRelativeVirtualPath;
            if (!String.IsNullOrEmpty(f))
            {
                url = string.Format("{0}?Filter={1}", url, f);
            }
            return this.ResolveUrl(url);
        }

        /// <summary>
        /// Handles the Click event of the FilterButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void FilterButton_Click(object sender, EventArgs e)
        {
            LinkButton lb = (LinkButton)sender;
            BindData(lb.CommandArgument.ToString());
        }

        /// <summary>
        /// Handles the RowDataBound event of the gvUsers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Web.UI.WebControls.GridViewRowEventArgs"/> instance containing the event data.</param>
        protected void gvUsers_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

            }
        }

        /// <summary>
        /// Handles the Click event of the AddUser control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.EventArgs"/> instance containing the event data.</param>
        protected void AddUser(object sender, EventArgs e)
        {
            Response.Redirect(_EditNavigateUrl);
        }

        /// <summary>
        /// Handles the PageIndexChanging event of the gvUsers control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="T:System.Web.UI.WebControls.GridViewPageEventArgs"/> instance containing the event data.</param>
        protected void gvUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUsers.PageIndex = e.NewPageIndex;
            BindData(SearchFilter);
        }

        protected void SearchUsers(object sender, EventArgs e)
        {
            IList<User> users = null;
            if (SearchField.SelectedValue == "Email")
            {
                users = Users.FindUsersByEmail(txtSearch.Text + "%");
            }
            else
            {
                users = Users.FindUsersByName(txtSearch.Text + "%");
            }
            gvUsers.DataSource = users;
            gvUsers.DataBind();
        }
    }
}