﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ManageUsersControl" Codebehind="ManageUsers.ascx.cs" %>
<style>    
.input-group label
{
    float:none !important;
    text-align: left !important;    
}
</style>

<div class="well">
    <asp:LinkButton CssClass="btn" ID="UserDetailPagesButton" runat="server" Text="User Details" Icon="user1"  onclick="ManageDetails"  CausesValidation="false" />
    <asp:LinkButton CssClass="btn" ID="PasswordButton" runat="server" Text="User Password" Icon="key"  onclick="ManagePassword"  CausesValidation="false" />
    <asp:LinkButton CssClass="btn btn-danger" ID="DeleteUserButton" runat="server" Text="Delete User" Icon="deleteuser1"  onclick="DeleteUserTab"  CausesValidation="false">
        <i class="icon-trash icon-white"></i> Delete User
    </asp:LinkButton>  
</div>
<sharp:MessagePanel ID="ErrorMessage" runat="server" MessageType="Failure" />
<asp:panel ID="pnlUserDetailPages" runat="server" style="position: relative;">       
<div class="form-horizontal">
    <div class="control-group">
        <label class="control-label">Username</label>
        <div class="controls">
            <asp:TextBox ID="UserName" runat="server" />
        </div>
    </div>               
    <asp:UpdatePanel ID="apnlType" runat="server">  
        <ContentTemplate>            
            <div class="control-group">
                <label class="control-label">Type</label>                                
                <div class="controls form-inline">
                    <asp:RadioButton ID="rbAgent" runat="server" Text="Agent" Checked="true" GroupName="UserType" AutoPostBack="True" oncheckedchanged="TypeCheckChanged" />                    
                    <asp:RadioButton ID="rbCustomer" runat="server" Text="Customer" GroupName="UserType" AutoPostBack="True" CssClass="input-group" oncheckedchanged="TypeCheckChanged" />
                    <asp:RadioButton ID="rbBackOffice" runat="server" Text="Office Staff" GroupName="UserType" AutoPostBack="True" CssClass="input-group" oncheckedchanged="TypeCheckChanged" />                                                    
                </div>
            </div>
            <asp:Panel ID="AssociationPanel" CssClass="control-group" runat="server">
                <label class="control-label">Association:</label>
                <div class="controls">
                    <asp:DropDownList ID="ddlSelector" runat="server" Visible="true"></asp:DropDownList>       
                </div>
            </asp:Panel>            
        </ContentTemplate>  
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="rbAgent" EventName="CheckedChanged" />                    
            <asp:AsyncPostBackTrigger ControlID="rbCustomer" EventName="CheckedChanged" />                    
            <asp:AsyncPostBackTrigger ControlID="rbBackOffice" EventName="CheckedChanged" />                                        
        </Triggers>
    </asp:UpdatePanel>
    <div class="control-group">                  
        <label class="control-label">Email</label>
        <div class="controls">
            <asp:TextBox ID="Email" runat="server" /> 
        </div>
    </div>
    <div class="control-group">        
        <label class="control-label">Authorized:</label>
        <div class="controls">
            <asp:CheckBox ID="Active" runat="server" CssClass="input-group"/>
        </div>
    </div>
</div>  

<div class="form-actions">    
    <asp:LinkButton CssClass="btn btn-success" ID="cmdUpdate" OnClick="cmdUpdate_Click" runat="server">
        <i class="icon-ok icon-white"></i> Save User
    </asp:LinkButton>    
</div>  
</asp:panel>
  
<asp:Panel CssClass="myform" ID="pnlUserPassword" runat="server">
<div class="formcontent" style="padding-left: 20px;">
    <div class="Row">
        <asp:Label ID="Label16" CssClass="FieldName"  Font-Bold="True" Width="180px" runat="server" Text="Password Last Changed:"></asp:Label>
        <asp:Label ID="PasswordLastChanged" runat="server" CssClass="FieldValue" Text=""></asp:Label>
    </div>
    
    <br />
    <span style="font-weight:bold;margin-top:5px;margin-bottom:10px;" class="h1">Change Password</span>
    <br />
    <br />
    <br />
    <asp:Label ID="lblMessage" ForeColor="Red" runat="server" Visible="false">Your password has been changed successfully.</asp:Label>
    <br />
    <table class="form" style="margin-left:100px;width:300px;">
        <tr>
            <th><asp:Label ID="Label11" CssClass="col1" AssociatedControlID="NewPassword" runat="server" Text="New Password:" /></th>
            <td class="field"><asp:TextBox ID="NewPassword" runat="server" TextMode="Password" /></td>
        </tr>
        <tr>
            <th><asp:Label ID="Label12" CssClass="col1" AssociatedControlID="ConfirmPassword" runat="server" Text="Confirm Password:" /></th>
            <td class="field"><asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" /><asp:CompareValidator ID="cvPasswords" runat="server" ControlToValidate="NewPassword" ControlToCompare="ConfirmPassword" Type="String" ErrorMessage="Passwords must match."></asp:CompareValidator></td>
        </tr>
        <tr>
            <td colspan="2">  <asp:LinkButton CssClass="btn btn-priamry" ID="cmdChangePassword"  OnClick="cmdChangePassword_Click" runat="server">Change Password</asp:LinkButton></td>
        </tr>
    </table> 
    <br />
    <br />
    <span style="font-weight:bold;" class="h1">Reset Password</span> 
    <p class="desc">
        <asp:Label ID="Label13" runat="server" Text="You can reset the password for this user. The password will be randomly generated."></asp:Label>
     </p>
             
     <asp:LinkButton CssClass="btn btn-warning" ID="cmdResetPassword" OnClick="cmdResetPassword_Click" runat="server" Text="Reset Password" />     
</div>         
</asp:Panel>
        
<asp:Panel ID="pnlDeleteUser" runat="server" Width="600px" Visible="false">
<div class="formcontent" style="padding-left: 20px;">
    <p class="desc">It is higly recommended that you unauthorize the users account instead of deleting, to maintain the integrity of the AirtimeBilling database.</p>
    <p class="desc">Unauthorizing the account will keep the user information intact but will not allow the user to log into the application. You can re-authorize the account at anytime if necessary from the user details page.</p>
    <p align="center">        
        <asp:LinkButton CssClass="btn btn-warning" ID="cmdUnauthorizeAccount" OnClick="cmdUnauthorizeAccount_Click" runat="server" Text="Unauthorize this Account" />
        &nbsp;                        
        <asp:LinkButton CssClass="btn btn-danger" ID="DeleteUserNowButton" runat="server" OnClientClick="return confirm('Are you sure?');" Text="Delete User" onclick="DeleteUser">
            <i class="icon-trash icon-white"></i> Delete User
        </asp:LinkButton>
    </p>
</div>    
</asp:Panel>
   
<asp:Panel ID="pnlAddNewUser" Visible="false" runat="server">
    <asp:CreateUserWizard ID="CreateUserWizard1"
            runat="server" UserNameLabelText="Username:" 
            ContinueDestinationPageUrl="~/Admin/UserList.aspx"  LoginCreatedUser="false" 
            CancelDestinationPageUrl="~/Admin/UserList.aspx" Font-Names="Verdana"             
            CreateUserButtonType="Link" 
            CreateUserButtonStyle="btn btn-primary"
            CancelButtonType="Link"
            CancelButtonStyle="btn btn-primary"
            DisplayCancelButton="True"
            InstructionText="Enter the details for the user account."               
            OnCreatedUser="CreateUserWizard1_CreatedUser">
            <WizardSteps>
                <asp:CreateUserWizardStep ID="CreateUserWizardStep1"  runat="server">
                  
                </asp:CreateUserWizardStep>
                <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                </asp:CompleteWizardStep>
            </WizardSteps>
            <InstructionTextStyle Height="35px" />
        </asp:CreateUserWizard>
   </asp:Panel>
