﻿<%@ Page Title="Support - Log Viewer" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="LogViewer.aspx.cs" Inherits="AirtimeBilling.Web.LogViewer" %>
<%@ Import Namespace="System.ComponentModel"%>
<%@ Import Namespace="AirtimeBilling.Common.Logging"%>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Log Viewer</h1>
<hr />
<div class="formcontent module">    
<sharp:FormGroup runat="server" GroupingText="Options" CssClass="well">        
    <div class="control-group">
        <label class="control-label">Start Date</label>                
        <div class="controls">
            <asp:TextBox ID="StartDateTextBox" runat="server" Width="80px" />
            <asp:Image ID="StartDateImage" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
            <atk:CalendarExtender ID="StartDateTextBox_CalendarExtender" runat="server" 
                    TargetControlID="StartDateTextBox" 
                    PopupButtonID="StartDateImage"
                    Format="dd/MM/yyyy">
                </atk:CalendarExtender>                    
                <asp:RequiredFieldValidator ID="rfvStateDate" runat="server" 
                    ControlToValidate="StartDateTextBox"  SetFocusOnError="True"
                    ErrorMessage="Missing Start Date" Display="None" />
                <asp:CompareValidator ID="cvStartDate" runat="server" ErrorMessage="Invalid Date" 
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="StartDateTextBox"  
                    Text="" Display="None"></asp:CompareValidator>     
        </div>
    </div>
    <div class="control-group">            
        <label class="control-label">End Date</label>
        <div class="controls"><asp:TextBox ID="EndDateTextBox" runat="server" Width="80px" />
            <asp:Image ID="EndDateTextBoxImage" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
            <atk:CalendarExtender ID="EndDateTextBox_CalendarExtender" runat="server" 
                    TargetControlID="EndDateTextBox" 
                    PopupButtonID="EndDateTextBoxImage"
                    Format="dd/MM/yyyy">
                </atk:CalendarExtender>                    
                <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" 
                    ControlToValidate="EndDateTextBox"  SetFocusOnError="True"
                    ErrorMessage="Missing End Date" Display="None" />
                <asp:CompareValidator ID="cvEndDate" runat="server" ErrorMessage="Invalid Date" 
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="EndDateTextBox"  
                    Text="" Display="None"></asp:CompareValidator>     
        </div>                
    </div>
    <div class="control-group">
        <label class="control-label">Severity</label>
        <div class="controls">
            <asp:DropDownList runat="server" ID="CategoryDropDown" width="110px">
                <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                <asp:ListItem Value="1">Debug</asp:ListItem>
                <asp:ListItem Value="2">Warning</asp:ListItem>
                <asp:ListItem Value="3">Exception</asp:ListItem>
            </asp:DropDownList>
        </div>
`   </div>
    <div class="control-group">
        <label class="control-label">User</label>
        <div class="controls">
            <asp:TextBox ID="UserTextBox" runat="server" Width="150px"></asp:TextBox> Leave blank to show all users.
        </div>
    </div>
    
    <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" runat="server" OnClick="SearchLog">
        <i class="icon-search icon-white"></i> Search log entries
    </asp:LinkButton>
    
</sharp:FormGroup>    
    <sharp:MessagePanel ID="UserMessage" runat="server" />
    <asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" runat="server" ShowSummary="True"  />    
    <sharp:ClickableGridView ID="gvLog" runat="server" 
        AutoGenerateColumns="False" 
        AllowPaging="false"
        DataKeyNames="Id"
        AllowSorting="false" onrowclicked="gvLog_RowClicked">
        <Columns>
            <asp:TemplateField>
                <HeaderStyle Width="30px" Height="25px" />
                <ItemStyle Width="30px" HorizontalAlign="Center" />
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate>   
                    <asp:Image ID="typeImage" runat="server" ImageUrl='<%# Bind("TypeImageUrl") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Width="130px" />
                <ItemStyle Width="130px"  />
                <HeaderTemplate>Time</HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="TimeLabel" runat="server" Text='<%# Bind("Timestamp", "{0:dd/MM/yyyy HH:mm:ss}") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderStyle Width="100px" />
                <ItemStyle Width="100px"  />
                <HeaderTemplate>User</HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="UserLabel" runat="server" Text='<%# Bind("Username") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>Message</HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="MessageLabel" runat="server" Text='<%# Bind("Message") %>' />                    
                </ItemTemplate>
            </asp:TemplateField>            
        </Columns>                
    </sharp:ClickableGridView>
    <sharp:MessagePanel ID="NoRecords" runat="server" />
    
    <asp:Panel ID="LogDetailPanel" runat="server" CssClass="ModalPopup">                      
        <div class="Form">
            <asp:LinkButton ID="ClosePopupButton" runat="server" Text="Close" />
            <sharp:FormGroup ID="LogDetailFG" runat="server" GroupingText="Log Detail" CssClass="full">                
                <table class="form log">
                    <tr>
                        <th valign="top">Category</th>
                        <td><asp:Label ID="CategoryLabel" runat="server" /></td>                
                    </tr>
                    <tr>
                        <th valign="top">Entry Time</th>
                        <td><asp:Label ID="EntryTimeLabel" runat="server" /></td>                
                    </tr>
                    <tr>
                        <th valign="top">User</th>
                        <td><asp:Label ID="UserLabel" runat="server" /></td>                
                    </tr>
                    <tr>
                        <th valign="top">Message</th>
                        <td><asp:Label ID="MessageLabel" runat="server" /></td>                
                    </tr>
                    <tr>
                        <th valign="top">Method Name</th>
                        <td><asp:Label ID="MethodNameLabel" runat="server" /></td>                
                    </tr>
                    <tr>
                        <th valign="top">Class</th>
                        <td><asp:Label ID="ClassLabel" runat="server" /></td>                
                    </tr>
                    <tr>
                        <th valign="top">Stack Trace</th>
                        <td><asp:Label ID="StackTraceLabel" runat="server"  /></td>                
                    </tr>                
                </table>            
            </sharp:FormGroup>
        </div>
    </asp:Panel>
    <atk:ModalPopupExtender ID="LogDetailPopup" runat="server" BackgroundCssClass="modalBackground"
                     PopupControlId="LogDetailPanel"
                     TargetControlId="LogDetailPanel"
                     CancelControlID="ClosePopupButton" 
                     OnCancelScript="$find('LogDetailPopup').hide(); return false;"
                     BehaviorID="LogDetailPopup" />
</div>        
</asp:Content>
