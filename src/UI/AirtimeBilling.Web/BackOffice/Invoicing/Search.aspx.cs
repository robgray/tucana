﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web.I.Invoicing
{
    public partial class InvoiceRunSearch : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);            

            if (!IsPostBack)
            {
                ddlSearchType.Attributes.Add("onchange", "changeSearch(this);");

                var runs = this.InvoicingService.GetAllInvoiceRuns();

                ddlSearchType.Enabled = runs.Count > 0;
                ddlRunNumber.Enabled = runs.Count > 0;

                SearchForInvoicesButton.Enabled = runs.Count > 0;

                ddlRunNumber.DataSource = runs;
                ddlRunNumber.DataValueField = "RunNumber";
                ddlRunNumber.DataTextField = "RunDate";
                ddlRunNumber.DataTextFormatString = "{0:d}";
                ddlRunNumber.DataBind();               
            }
        }
        
        protected void PrintSelectedInvoices(object sender, EventArgs e)
        {
            var invoiceNumbers = new List<string>();
            foreach (GridViewRow row in gvInvoices.Rows)
            {
                var chk = row.Cells[0].Controls[1] as CheckBox;
                if (chk.Checked)
                {
                    var dcf = row.Cells[1] as DataControlFieldCell;
                    if (dcf != null)
                        invoiceNumbers.Add(dcf.Text);
                }
            }

            PrintMessage.SetInformation("Printing Invoices has not been enabled");
        }

        protected void SearchForInvoices(object sender, EventArgs e)
        {
            IList<InvoiceSearchItem> invoices;
            if (ddlSearchType.SelectedValue.Equals("1"))
            {
                var runNumber = int.Parse(ddlRunNumber.SelectedValue);
                invoices = this.InvoicingService.FindInvoicesByRunNumber(runNumber);
            }
            else
            {
                invoices = this.InvoicingService.FindInvoicesByInvoiceNumber(txtInvoiceNumber.Text);
            }

            gvInvoices.DataSource = invoices;
            gvInvoices.DataBind();

            PrintSelectedInvoicesButton.Visible = invoices.Count > 0;
        }

        protected void gvInvoices_PreRender(object sender, EventArgs e)
        {
            // Add js to the controls
            if (gvInvoices.FooterRow != null)
            {
                var cell = gvInvoices.FooterRow.Cells[0];

                var selectAll = cell.FindControl("lnkSelectAll") as HyperLink;
                var selectNone = cell.FindControl("lnkSelectNone") as HyperLink;

                selectAll.Attributes.Add("onclick", "InvoiceRunManager.SelectInvoice('" + gvInvoices.ClientID + "', true)");
                selectNone.Attributes.Add("onclick", "InvoiceRunManager.SelectInvoice('" + gvInvoices.ClientID + "', false)");
            }


        }

        protected void gvInvoices_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Email":

                    this.InvoicingService.EmailInvoice(e.CommandArgument.ToString());

                    break;
            }
        }
    }
}
