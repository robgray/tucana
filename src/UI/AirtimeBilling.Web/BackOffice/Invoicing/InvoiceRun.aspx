﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.InvoiceRunPage" Codebehind="InvoiceRun.aspx.cs" %>
<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/scripts/InvoiceRun.js" />
    </Scripts>
</asp:ScriptManagerProxy>
<h1>Perform Invoicing</h1>    
<hr />
<sharp:MessagePanel ID="UserMessage" runat="server" />
<div id="InvoiceFail" style="display:none">
    <div class="alert alert-danger"><span id="FailMessage"></span></div>
</div>

<div class="alert alert-info">Ensure you have updated the Amount Paid on all Invoice accounts prior to clicking on <i>1. Generate Invoices</i>. <br /> <br />
    Credit Card Accounts will be automatically updated when uploading processing results (Step 3).</div>

<sharp:FormGroup runat="server" GroupingText="Options">       
    <div class="control-group">            
        <label class="control-label">Invoiced up to</label>
        <div class="controls">
            <asp:Label ID="lblLastInvoiceDate" runat="server"></asp:Label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Invoice to date</label>
        <div class="controls">
        <asp:TextBox ID="txtInvoiceDate" runat="server" MaxLength="20" Width="80px"></asp:TextBox>
        <asp:Image ID="imgInvoiceDate" runat="server" ImageUrl="~/images/Calendar.ico" CssClass="CalIcon" />
        <atk:calendarextender ID="txtInvoiceDate_CalendarExtender" runat="server" 
            Enabled="True" 
            PopupButtonID="imgInvoiceDate"
            Format="dd/MM/yyyy"
            TargetControlID="txtInvoiceDate">
        </atk:calendarextender>                   
        <asp:CompareValidator ID="cvtxtInvoiceDate" runat="server" ErrorMessage="Invalid Date" 
                    Type="Date" Operator="DataTypeCheck" ControlToValidate="txtInvoiceDate"
                    Text="" Display="None"></asp:CompareValidator>                                    
        </div>
    </div>                        
</sharp:FormGroup>

<div class="tasks" wiu>
    <h4>Steps</h4>                
    <div class="calldatacheck"><asp:LinkButton ID="ImportCallsButton" runat="server" Text="Click here to check for unimported Calls"></asp:LinkButton></div>
    <ol>
        <li><asp:LinkButton ID="CalculateInvoicesButton" runat="server" Text="Generate Invoices"></asp:LinkButton></li>
        <li><asp:LinkButton ID="ExportCreditCardDataButton" runat="server" 
                Text="Create Credit Card file for processing" onclick="DownloadCreditCardData"></asp:LinkButton></li>
        <li><asp:LinkButton ID="UploadCreditCardButton" runat="server" Text="Upload Credit Card processing results"></asp:LinkButton></li>                
        <li><asp:LinkButton ID="PrintPostOnlyInvoicesButton" runat="server" 
                Text="Print Post-only Invoices" onclick="PrintPostOnlyInvoicesButton_Click"></asp:LinkButton></li> 
        <li><asp:LinkButton ID="SendEmailInvoicesButton" runat="server" 
                Text="Send Email Invoices and Data Files" onclick="SendEmailInvoices"></asp:LinkButton></li>
        <li><asp:LinkButton ID="PrintICopyButton" runat="server" 
                Text="Print I Invoice copies" onclick="PrintIInvoiceCopies"></asp:LinkButton></li>
    </ol>
</div>        

<asp:Panel ID="pnlUpdating" runat="server" CssClass="modal" style="display:none;">            
    <div class="modal-header"><h4>Performing Invoicing.</h4></div>
    <div class="modal-body">
    <table width="100%">
        <tr>
            <td>Please do not close your browser or refresh this page.</td>
            <td width="100"><asp:Image ID="Image3" runat="server" ImageUrl="~/images/ajax-loader-med.gif" /></td>
        </tr>            
    </table>
    </div>
    <div class="modal-footer">
    </div>    
</asp:Panel>
<atk:modalpopupextender 
    ID="mdlPopup" runat="server" TargetControlID="pnlUpdating" 
    PopupControlID="pnlUpdating" BackgroundCssClass="modal-backdrop" 
    BehaviorID="mdlPopup"  />
    
<asp:Panel ID="pnlUpdating2" runat="server" CssClass="modal" style="display:none;">            
    <div class="modal-header"><h4>Scanning for new Calls</h4></div>
    <div class="modal-body">
        <table width="100%">
            <tr>
                <td>Please do not close your browser or refresh this page.</td>
                <td width="100"><asp:Image ID="Image2" runat="server" ImageUrl="~/images/ajax-loader-med.gif" /></td>
            </tr>            
        </table>
    </div>
    <div class="modal-footer">
    </div>
</asp:Panel>
<atk:modalpopupextender 
    ID="mdlPopupCheck" runat="server" TargetControlID="pnlUpdating2" 
    PopupControlID="pnlUpdating2" BackgroundCssClass="modal-backdrop" 
    BehaviorID="mdlPopupCheck"  />
    
<asp:Panel ID="ImportEwayPanel" runat="server" CssClass="ModalPopup" style="display:none;width:600px;">
    <h1>Import Credit Card processing results</h1>    
    <div class="importText">
        <table>
            <tr>
                <td valign="top"><asp:image ID="todo" runat="server" ImageUrl="~/images/icons/todo.png" /></td>
                <td valign="top">
                    Upload a Transaction report from EWAY in CSV format.  
                    This will match transactions to invoices, based on Invoice Number supplied to Eway.<br /><br />
                    <small>This popup will close once the Eway file has been imported.</small>
                </td>
            </tr>
        </table>
    </div>
    <asp:FileUpload ID="CreditCardUpload" runat="server" />
    <div class="crudbarfloat">    
        <sharp:DynamicHoverImageButton ID="UploadCreditCardFileButton" runat="server" Text="Upload and Process" OnClick="ProcessCreditCardTransactions" />
    </div>
</asp:Panel>
<atk:modalpopupextender 
    ID="EwayImportModalPopup" runat="server" TargetControlID="UploadCreditCardButton" 
    PopupControlID="ImportEwayPanel" BackgroundCssClass="modalBackground" 
    BehaviorID="EwayImportModalPopup"  />
</asp:Content>

