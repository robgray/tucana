﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using AirtimeBilling.Logging;
using AirtimeBilling.Services;
using AirtimeBilling.Services.Services;
using AirtimeBilling.Import;

namespace AirtimeBilling.Web
{
    public partial class InvoiceRunPage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            if (!ClientScript.IsStartupScriptRegistered("Popup"))
            {                
                var script = string.Format("<script type=\"text/javascript\">InvoiceRunManager.Init('{0}','{1}','{2}','{3}')</script>", lblLastInvoiceDate.ClientID,
                                       txtInvoiceDate.ClientID, CalculateInvoicesButton.ClientID, ImportCallsButton.ClientID);             

                ClientScript.RegisterStartupScript(GetType(), "Popup", script);
            }

            if (!IsPostBack)
            {          
                if (Request["Code"] != null && Request["Code"] == "SA2342")
                {
                    UserMessage.SetSuccess("Invoicing Run Successful");
                }
            }

            DateTime? lastInvoicing = this.InvoicingService.InvoiceSettings.LastInvoiceRunPerformedDate;
            lblLastInvoiceDate.Text = lastInvoicing.HasValue ? lastInvoicing.Value.ToString("dd/MM/yyyy") : "";
            txtInvoiceDate.Text = DateTime.Today.ToShortDateString();
        }

        [WebMethod]
        public static void CheckForNewCallsToImport()
        {
            var startRunTime = DateTime.Now;

            var importerManager = new ImporterManager();
            importerManager.RescanForContracts();

            // Pause for at least 3 seconds.            
            var runDuration = DateTime.Now - startRunTime;
            if (runDuration.TotalMilliseconds < 3000)
            {
                var sleepTime = 3000 - (int)runDuration.TotalMilliseconds;
                System.Threading.Thread.Sleep(sleepTime);
            }
        }

        [WebMethod]
        public static int PerformInvoiceRun(string lastInvoiceDate, string invoiceDate)
        {
            DateTime startDate;
            var startRunTime = DateTime.Now;

            if (DateTime.TryParse(lastInvoiceDate, out startDate))
            {
                startDate = startDate.AddDays(1);
            }
            else
            {
                throw new Exception("Invalid Last Invoiced Date");
            }

            DateTime endDate;

            if (!DateTime.TryParse(invoiceDate, out endDate))
            {
                throw new Exception("Invalid Invoice Date");
            }

            if (endDate < startDate)
            {
                throw new Exception("Invoice Run Date must be after Last Invoice Date");
            }
            if (endDate > DateTime.Today.AddDays(1))
            {
                throw new Exception("Invoice Date cannot be in the future");
            }

            try
            {
                var service = new InvoicingService();
                var invoiceRunNumber = service.InvoiceRun(endDate);

                // Pause for at least 3 seconds.
                var runDuration = DateTime.Now - startRunTime;
                if (runDuration.TotalMilliseconds < 3000)
                {
                    var sleepTime = 3000 - (int)runDuration.TotalMilliseconds;
                    System.Threading.Thread.Sleep(sleepTime);
                }

                return invoiceRunNumber;
            }
            catch (InvoiceRunFrequencyException)
            {
                throw;
            } 
            catch (Exception) {
                throw new Exception("Internal Invoicing Error.  Contact System Support.");
            }         
        }

        protected void PrintPostOnlyInvoicesButton_Click(object sender, EventArgs e)        
        {

            // Print post only, from last invoice run.
            var invoiceNumbers = new List<string>();            
            var invoicesForRun = InvoicingService.FindInvoicesByRunNumber(InvoicingService.InvoiceSettings.LastInvoiceRunNumber);

            foreach (var invoice in invoicesForRun)
            {                
                invoiceNumbers.Add(invoice.InvoiceNumber);                
            }

            
            UserMessage.SetInformation("Printing invoices is disabled");
            
        }
        
        
        protected void SendEmailInvoices(object sender, EventArgs e)
        {
            var lastRuNumber = InvoicingService.InvoiceSettings.LastInvoiceRunNumber;
            InvoicingService.SendInvoicesInEmail(lastRuNumber);
        }

        protected void PrintIInvoiceCopies(object sender, EventArgs e)
        {
            // Print post only, from last invoice run.
            var invoiceNumbers = new List<string>();
            var invoicesForRun = InvoicingService.FindInvoicesByRunNumber(InvoicingService.InvoiceSettings.LastInvoiceRunNumber);

            foreach (var invoice in invoicesForRun)
            {
                invoiceNumbers.Add(invoice.InvoiceNumber);
            }

            UserMessage.SetInformation("Printing invoices is disabled");
        }


        protected void DownloadCreditCardData(object sender, EventArgs e)
        {
            var runNumber = InvoicingService.InvoiceSettings.LastInvoiceRunNumber;
            var ewayFile = InvoicingService.ExportCreditCardChargeFile(runNumber);

            Response.ReturnCsv(ewayFile);
        }                 

        protected void ProcessCreditCardTransactions(object sender, ImageClickEventArgs e)
        {
            try {                
                if (CreditCardUpload.HasFile) {
                    string csvData = string.Empty;
                    using (var inputStream = new MemoryStream(CreditCardUpload.FileBytes)) {
                        var reader = new StreamReader(inputStream);

                        csvData = reader.ReadToEnd();
                        reader.Dispose();
                    }
                    InvoicingService.ImportCreditCardChargeFile(csvData, CreditCardUpload.FileName);
                }
            }
            catch (Exception ex) {
                LoggingUtility.LogDebug("ProcessCreditCardTransactions", "InvoiceRun.aspx.cs", ex.Message);
                UserMessage.SetFailure(ex.Message);
            }
        }
    }
}