﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="AirtimeBilling.Web.I.Invoicing.InvoiceRunSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
    <Scripts>
        <asp:ScriptReference Path="~/scripts/InvoiceRun.js" />
        <asp:ScriptReference Path="~/scripts/ProgressPopup.js" />
    </Scripts>
</asp:ScriptManagerProxy>
<script type="text/javascript" language="javascript">
    var ModalProgress = '<%= ModalProgress.ClientID %>';         
</script>
<h1>Existing Invoices</h1>

<sharp:FormGroup runat="server" CssClass="well" GroupingText="Search Options">
    <div class="control-group">
        <label class="control-label" for="<%= ddlSearchType.ClientID %>">Search Type</label>
        <div class="controls">
            <asp:DropDownList ID="ddlSearchType" runat="server">
                <asp:ListItem Value="1" Text="Invoice Date" Enabled="true" />
                <asp:ListItem Value="2" Text="Invoice Number" />
            </asp:DropDownList>   
        </div>   
    </div>                 
    <div id="RunNumberSearch" class="control-group">
        <label class="control-label" for="<%= ddlRunNumber.ClientID %>">Invoice Date</label>
        <div class="controls">
            <asp:DropDownList ID="ddlRunNumber" runat="server"></asp:DropDownList> 
        </div>
    </div>         
    <div id="InvoiceNumberSearch" class="control-group" style="display:none">            
        <label class="control-label" for="<%= txtInvoiceNumber.ClientID %>">Invoice Number</label>
        <div class="controls">
            <asp:TextBox ID="txtInvoiceNumber" runat="server" MaxLength="20" Width="80px"></asp:TextBox>                        
        </div>
    </div>  
    <asp:LinkButton CssClass="btn btn-primary" ID="SearchForInvoicesButton" Text="Search" OnClick="SearchForInvoices" Icon="search" CausesValidation="false" runat="server">
        <i class="icon-search icon-white"></i> Search for Invoices
    </asp:LinkButton>                
 </sharp:FormGroup>

<asp:UpdatePanel ID="upnlInvoices" runat="server" UpdateMode="Conditional">
<ContentTemplate>
<div style="margin:10px 0;clear:both;">
    <sharp:MessagePanel ID="PrintMessage" runat="server" />    
    <asp:GridView ID="gvInvoices" runat="server"         
        ShowFooter="true"
        AutoGenerateColumns="False"                      
        onprerender="gvInvoices_PreRender"
            onrowcommand="gvInvoices_RowCommand">   
        <Columns>
            <asp:TemplateField>
                <HeaderStyle Width="40px" />
                <ItemStyle Width="40px" />
                <FooterStyle Width="40px" />
                <ItemTemplate>                    
                    <table cellpadding="0" cellspacing="0" id="invoicegrid">
                        <tr>
                            <td style="border:0"><asp:CheckBox ID="chkSelect" runat="server" Text="" /></td>
                            <td style="border:0"><asp:ImageButton ID="EmailInvoiceButton" runat="server" ImageUrl="~/images/email.gif"
                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "InvoiceNumber") %>' CommandName="Email" /></td>
                        </tr>
                    </table>
                </ItemTemplate>
                <FooterTemplate>
                    <table cellpadding="0" cellspacing="0" id="invoicegrid">
                        <tr>
                            <td style="border:0"><asp:HyperLink ID="lnkSelectAll" runat="server" Text="All" NavigateUrl="javascript:void(0);" /></td>
                            <td style="border:0"><asp:HyperLink ID="lnkSelectNone" runat="server" Text="None" NavigateUrl="javascript:void(0);" /></td>
                        </tr>
                    </table>                    
                </FooterTemplate>
            </asp:TemplateField>            
            <asp:BoundField DataField="InvoiceNumber" HeaderText="Invoice Number" ReadOnly="True" SortExpression="InvoiceNumber" HeaderStyle-Width="40px" ItemStyle-Width="40px" />            
            <asp:BoundField DataField="InvoiceDate" HeaderText="Invoice To Date" ReadOnly="True" SortExpression="InvoiceDate" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="40px" ItemStyle-Width="40px" />                        
            <asp:BoundField DataField="To" HeaderText="To" ReadOnly="True" SortExpression="To" HeaderStyle-Width="250px" ItemStyle-Width="250px" />  
            <asp:BoundField DataField="RunNumber" HeaderText="Run Number" ReadOnly="True" SortExpression="RunNumber" HeaderStyle-Width="40px" ItemStyle-Width="40px" />          
        </Columns>      
    </asp:GridView>
    <div class="form-actions">
        <asp:LinkButton CssClass="btn btn-primary" ID="PrintSelectedInvoicesButton" runat="server" OnClick="PrintSelectedInvoices" Text="Print Selected Invoices" Icon="print" Visible="false">
            <i class="icon-print icon-white"></i> Print Selected Invoices
        </asp:LinkButton>
    </div>    
</div>    
</ContentTemplate>
<Triggers>
    <asp:AsyncPostBackTrigger ControlID="SearchForInvoicesButton" EventName="click" />
</Triggers>
</asp:UpdatePanel>



<asp:Panel ID="UpdateProgressPanel" runat="server">
    <asp:UpdateProgress ID="UpdatePanel1" runat="server" AssociatedUpdatePanelID="upnlInvoices" DisplayAfter="1">
        <ProgressTemplate>  
            <div class="modal">                    
                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/ajax-loader-big.gif" />
            </div>                    
        </ProgressTemplate>
    </asp:UpdateProgress> 
</asp:Panel>
<atk:modalpopupextender 
    ID="ModalProgress" runat="server" TargetControlID="UpdateProgressPanel" 
    PopupControlID="UpdateProgressPanel" BackgroundCssClass="modal-backdrop" />                
        

</asp:Content>
