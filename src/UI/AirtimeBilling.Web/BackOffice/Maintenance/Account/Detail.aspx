﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.AccountMaintenancePage" Codebehind="Detail.aspx.cs" %>
<%@ Register Src="~/UserControls/Account.ascx" TagName="account" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ActivityLogViewer.ascx" TagName="activitylog" TagPrefix="sharp" %>
<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Account Maintenance</h1>
<asp:Panel ID="pnlUserActions" runat="server" CssClass="well submenu">            
    <asp:LinkButton CssClass="btn" ID="lnkDetailsView" runat="server" Text="Details" onclick="lnkDetailsView_Click" />
    <asp:LinkButton CssClass="btn" ID="lnkActivityLog" runat="server" Text="Activity Log" onclick="ActivityLogViewer" />
    <asp:LinkButton CssClass="btn" ID="SubAccountsButton" runat="server" Text="View Sub Accounts" onclick="ViewSubAccounts" />
    <asp:LinkButton CssClass="btn" ID="lnkSearch" runat="server" Text="Search" Icon="search" onclick="GotoSearch">
        <i class="icon-search"></i> Search
    </asp:LinkButton>       
    <asp:LinkButton CssClass="btn btn-danger" ID="ScrubCustomerPopupButton" runat="server" Text="Delete Customer" CausesValidation="false">
        <i class="icon-trash icon-white"></i> Delete Customer
    </asp:LinkButton>
</asp:Panel>
<sharp:MessagePanel ID="UserMessage" runat="server" />
<div class="formcontent">
<asp:Panel ID="AccountDetailsPanel" runat="server">
    <div class="Form">                
        <div class="module">
            <div class="main" style="margin: 0 10px; margin-bottom: 10px;">
            <table>   
            <% if (!IsMaster) { %>
                <% if (Account.AccountId.HasValue && Account.IsInvoiceRoot)
                   {%>            
                    <tr>
                        <td style="padding-right:10px;margin-right:10px;"><img src="/images/icons/sub-account.png" alt="Sub Account" /></td>
                        <td>Click <b><asp:LinkButton ID="SelectMasterAccountButton" runat='server' OnClick="AttachToMasterAccount" Text="here" /></b> to include this a sub-account on the Master Account Invoice.</td>
                    </tr>
                <% }
                   else
                   {%>
                    <tr>
                        <td style="padding-right:10px;margin-right:10px;"><img src="/images/icons/sub-account.png" alt="Sub Account" /></td>
                        <td>Click <b><asp:Hyperlink ID="ShowInvoiceRootAccountButton" runat="server">here</asp:Hyperlink></b> to navigate to the Main account for this Invoice.</td>
                    </tr>
                    <tr>
                        <td style="padding-right:10px;margin-right:10px;"><img src="/images/icons/master-account.png" alt="Master Account" /></td>
                        
                        <td>Click <b><asp:LinkButton ID="MakeInvoiceRootButton" runat='server' OnClick="MakeInvoiceRoot" Text="here" /></b> to move this account to it's own Invoice.</td>
                    </tr>
                <% } %>
            <% } else if (Account.AccountId.HasValue) {%>
                <tr>
                    <td style="padding-right:10px;margin-right:10px;"><img src="/images/icons/sub-account.png" alt="Sub Account" /></td>
                    <td>Click <b><asp:LinkButton ID="CreateNewSubAccountButton" runat='server' OnClick="CreateNewSubAccount" Text="here" /></b> create a sub Account.</td>
                </tr>     
            <% } %> 
            </table>    
            </div>
        </div>        
        
        <asp:HiddenField ID="hdnContactId" runat="server" />
        <asp:HiddenField ID="hdnCompanyId" runat="server" />
        <asp:HiddenField ID="hdnMasterAccountId" runat="server" />    
        <sharp:account ID="Account" runat="server" />        
        <div class="form-actions">                    
            <asp:LinkButton CssClass="btn btn-success" ID="SaveButton" runat="server" Text="Save" Icon="disk"  onclick="SaveAccount">
                <i class="icon-ok icon-white"></i> Save
            </asp:LinkButton>
        </div>
    </div>        
</asp:Panel>
<asp:Panel ID="pnlActivityLog" runat="server">
    <div class="Form">
        <sharp:FormGroup ID="AccountFG0" runat="server" GroupingText="Account" CssClass="module">            
            <div class="Optional">
                <label class="control-label" for="<%= lblAccountNumber.ClientID %>">Account Number</label>
                <asp:Label ID="lblAccountNumber" runat="server" Text="" ></asp:Label>                        
            </div>
            <div class="Optional">
                <label class="control-label" for="<%= lblAccountName.ClientID %>">Account Name</label>
                <asp:Label ID="lblAccountName" runat="server" Text="" ></asp:Label>                        
            </div>
            <div class="clear"></div>
        </sharp:FormGroup>
    </div>
        
    <sharp:activitylog ID="LogViewer" runat="server" />    
    
    <asp:Panel ID="SubAccountsPanel" runat="server">
        <sharp:ClickableGridView ID="SubAccountsGrid" runat="server" 
            AutoGenerateColumns="False" DataKeyNames="Id" AllowPaging="false" 
            onrowclicked="SubAccountsGrid_RowClicked">
            <Columns>
                <asp:TemplateField>
                    <HeaderStyle Width="120px" />
                    <ItemStyle Width="120px" />
                    <HeaderTemplate>Account Number</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="AccountNumberLabel" runat="server" Text='<%# Bind("AccountNumber") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>Account Name</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="AccountNameLabel" runat="server" Text='<%# Bind("AccountName") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </sharp:ClickableGridView>
    </asp:Panel>
</asp:Panel>
    <asp:Panel ID="ScrubCustomerConfirmPanel" runat="server" CssClass="ModalPopup" style="display:none;min-height:0px">
        <h2>Delete Customer</h2>        
        <sharp:Tip Id="ScrubTip" runat="server" Text="This will completely remove the Customer from the system, as if they were never entered." style="width:500px" />        
        <div class="crudbar" style="position:static;">
            <asp:LinkButton CssClass="btn btn-primary" ID="DeleteCustomerButton" runat="server" Text="Delete" CausesValidation="false" OnClick="PurgeCustomer" />                  
            <asp:LinkButton CssClass="btn btn-primary" ID="CancelDeleteCustomerButton" runat="server" Text="Cancel" CausesValidation="false" />                             
        </div>       
    </asp:Panel>
     <atk:ModalPopupExtender ID="ScrubCustomerExtender" runat="server"
        Enabled="True" 
        TargetControlID="ScrubCustomerPopupButton"
        CancelControlID="CancelDeleteCustomerButton"
        PopupControlID="ScrubCustomerConfirmPanel"
        BackgroundCssClass="modalBackground">
    </atk:ModalPopupExtender>
</div>

</asp:Content>

