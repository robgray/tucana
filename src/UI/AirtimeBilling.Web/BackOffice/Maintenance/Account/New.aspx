﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="NewAccount.aspx.cs" Inherits="AirtimeBilling.Web.NewAccount" %>
<%@ Import Namespace="AirtimeBilling.Core"%>
<%@ Register Src="~/UserControls/Account.ascx" TagName="account" TagPrefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Account Maintenance</h1>
<hr />
<sharp:MessagePanel ID="UserMessage" runat="server" />
<asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True"  /> 
<asp:Panel ID="AccountDetailsPanel" runat="server">                                          
    <asp:HiddenField ID="hdnMasterAccountId" runat="server" />    
    <asp:HiddenField ID="hdnContactId" runat="server" />    
    <sharp:account ID="Account" runat="server" ReadOnly="false" />
    <div class="form-actions">                    
        <asp:Panel ID="SavePanel" runat="server">
            <asp:LinkButton CssClass="btn btn-success" ID="SaveButton" runat="server" onclick="SaveAccount">
                <i class="icon-ok icon-white"></i> Save Account
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn" ID="CancelButton" runat="server" Text="Cancel"  onclick="CancelNewAccount" /> 
        </asp:Panel>
        <asp:Panel ID="SavedPanel" runat="Server" Visible="false">
            <asp:LinkButton CssClass="btn" ID="ReturnButton" runat="server" Text="Return to Master Account"  onclick="CancelNewAccount" /> 
        </asp:Panel>
    </div>    
</asp:Panel>
</asp:Content>
