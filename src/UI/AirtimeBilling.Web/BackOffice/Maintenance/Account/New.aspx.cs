﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class NewAccount : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!IsPostBack) {
                if (!string.IsNullOrEmpty(Request.QueryString["master"]))
                {
                    MasterAccountId = Convert.ToInt32(Request.QueryString["master"]);                    
                    var master = AccountService.GetAccountById(MasterAccountId) as MasterAccount;
                    if (master == null) {
                        UserMessage.SetFailure("Could not find the Master Account");
                        Account.Visible = false;
                        return;
                    }

                    PopulateView(master.CreateSubAccount(string.Empty, string.Empty));
                }
            }
        }

        protected int MasterAccountId
        {
            get
            {
                return Convert.ToInt32(hdnMasterAccountId.Value);
            }
            set
            {
                hdnMasterAccountId.Value = value.ToString();
            }
        }

        protected void PopulateView(Account account)
        {            
            if (account != null)
            {                                              
                hdnContactId.Value = account.ContactId.ToString();

                var contact = ContactService.GetContact(account.ContactId);
                
                Account.AccountId = account.Id;
                Account.AccountNumber = null;
                Account.AccountName = account.AccountName;
                Account.Contact = contact.Name;                
                Account.Password = account.Password;
                Account.IsInvoiceRoot = account.IsInvoiceRoot;
                Account.BillingAddressType = account.BillingAddressType;
                Account.BillingMethod = account.BillingMethod;
                Account.EmailBill = account.EmailBill;
                Account.EmailBillDataFile = account.EmailBillDataFile;
                Account.PostBill = account.PostBill;
                Account.IsInternational = account.IsInternational;
                Account.AmountPaid = account.AmountPaid;
                Account.PreviousBill = account.PreviousBill;                
            }           
        }

        protected void CancelNewAccount(object sender, EventArgs e)
        {
            NavigationController.EditAccount(MasterAccountId);
        }

        protected void SaveAccount(object sender, EventArgs e)
        {
            var sub = new SubAccount();            
            sub.MasterAccountId = MasterAccountId;
            
            sub.AccountName = Account.AccountName;            
            sub.BillingAddressType = Account.BillingAddressType;
            sub.BillingMethod = Account.BillingMethod;
            sub.ContactRole = Account.ContactRole;
            sub.EmailBill = Account.EmailBill;
            sub.EmailBillDataFile = Account.EmailBillDataFile;
            sub.Password = Account.Password;
            sub.PostBill = Account.PostBill;
            sub.IsInvoiceRoot = Account.IsInvoiceRoot;
            sub.IsInternational = Account.IsInternational;
            sub.AmountPaid = Account.AmountPaid;

            if (hdnContactId.Value.Length > 0)
            {
                int contactId;
                if (int.TryParse(hdnContactId.Value, out contactId))
                {
                    if (contactId > 0) sub.ContactId = contactId;
                }
            }

            var request = new SaveAccountRequest
            {
                Account = sub,
                User = Users.Current
            };

            var response = AccountService.SaveAccount(request);
            if (response.IsSuccessful)
            {
                UserMessage.SetSuccess("Acount Saved");
                SavedPanel.Visible = true;
                SavePanel.Visible = false;
            }
            else
            {
                LoggingUtility.LogDebug("SaveAccount", GetType().FullName, "Could not save new Sub Account: " + response.Message);
                UserMessage.SetFailure(response.Message);
                SavedPanel.Visible = false;
                SavePanel.Visible = true;
            }
        }

    }
}
