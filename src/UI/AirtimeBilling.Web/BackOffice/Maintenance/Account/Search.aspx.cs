﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Web.I.Maintenance
{
    public partial class AccountSearchPage : BackOfficeBasePage
    {
        protected override void OnPreLoad(EventArgs e)
        {
            base.OnLoad(e);
            EmptyMessage.Clear();

            if (!IsPostBack) {
                SearchType.DataSource = Enum<AccountSearch>.GetAllDescriptions();
                SearchType.DataBind();
            }            
        }

        protected void SearchResultsGrid_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            if (args.HasKey)
                NavigationController.EditAccount(args.Key);                
        }

        protected void SearchForAccounts(object sender, EventArgs e)
        {
            SearchResultsGrid.DataSource = null;                

            AccountSearch searchType = Enum<AccountSearch>.GetEnumFromDescription(SearchType.SelectedValue);
            var accounts = AccountService.FindAccounts(searchType, SearchValue.Text);

            if (accounts.Count > 0)
            {
                // Convert Account to AccountSearchEntity
                var searches = new List<AccountSearchEntity>();                
                foreach (var account in accounts) {
                    searches.Add(AccountSearchEntity.GetAcountSearchEntity(account));
                }
            
                SearchResultsGrid.DataSource = searches;                
            }
            else {
                EmptyMessage.SetInformation("No accounts were found matching your search parameters.");
            }
            SearchResultsGrid.DataBind();
        }        

        protected void NewAccount(object sender, EventArgs e)
        {
            NavigationController.NewAccount();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            AddButton.Visible = ConfigItems.AllowNewAssociatedEntities;
        }
    }
}
