﻿using System;
using System.Threading;
using System.Web.UI;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Logging;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class AccountMaintenancePage : BackOfficeBasePage
    {
        private enum AccountTab
        {
            None,
            Details,            
            ActivityLog,
            SubAccounts,
        } 

        protected bool IsMaster
        {
            get
            {
                return (bool)(ViewState["IsMaster"] ?? false);
            }
            set
            {
                ViewState["IsMaster"] = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            UserMessage.Clear();

            if (!IsPostBack)
            {              
                if (string.IsNullOrEmpty(Request.QueryString["Id"])) {
                    UserMessage.SetFailure("Could not find the Account. Invalid Id");
                    SetTab(AccountTab.None);
                    return;
                }

                PopulateView(Convert.ToInt32(Request.QueryString["Id"]));
                SetTab(AccountTab.Details);
            }    
            else {
                if (!Account.AccountId.HasValue) {
                    UserMessage.SetFailure("Could not find the Account. Invalid Id");
                    SetTab(AccountTab.None);                    
                }
            }
        }

      
        protected void ActivityLogViewer(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Account.AccountNumber)) return;

            SetTab(AccountTab.ActivityLog);
            
            var account = AccountService.GetAccountByAccountNumber(Account.AccountNumber);
            lblAccountName.Text = Account.AccountName;
            lblAccountNumber.Text = Account.AccountNumber;

            LogViewer.Entity = account;
        }

        protected void SaveAccount(object sender, EventArgs e)
        {
            Account account;
            if (IsMaster)
            {
                var master = new MasterAccount(Account.AccountId.Value);
                if (hdnCompanyId.Value.Length > 0)
                {
                    int id;
                    if (int.TryParse(hdnCompanyId.Value, out id))
                    {
                        if (id > 0) master.CompanyId = id;
                    }
                }
                account = master;
            }
            else
            {
                var sub = new SubAccount(Account.AccountId.Value);
                if (hdnMasterAccountId.Value.Length > 0)
                {
                    int id;
                    if (int.TryParse(hdnMasterAccountId.Value, out id))
                    {
                        if (id > 0) sub.MasterAccountId = id;
                    }
                }
                account = sub;
            }

            account.AccountName = Account.AccountName;
            account.AccountNumber = Account.AccountNumber;
            account.BillingAddressType = Account.BillingAddressType;
            account.BillingMethod = Account.BillingMethod;
            account.ContactRole = Account.ContactRole;
            account.EmailBill = Account.EmailBill;
            account.EmailBillDataFile = Account.EmailBillDataFile;
            account.Password = Account.Password;
            account.PostBill = Account.PostBill;
            account.IsInvoiceRoot = Account.IsInvoiceRoot;
            account.IsInternational = Account.IsInternational;
            account.AmountPaid = Account.AmountPaid;
            account.PreviousBill = Account.PreviousBill;
            
            if (hdnContactId.Value.Length > 0)
            {
                int contactId;
                if (int.TryParse(hdnContactId.Value, out contactId))
                {
                    if (contactId > 0) account.ContactId = contactId;
                }
            }

            var request = new SaveAccountRequest
            {
                Account = account,
                User = Users.Current
            };

            var response = AccountService.SaveAccount(request);
            if (response.IsSuccessful)
            {
                UserMessage.SetSuccess("Acount Saved");
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void PopulateView(int accountId)
        {
            var account = AccountService.GetAccountById(accountId);
            if (account != null) {
                if (account is MasterAccount) {
                    var accounts = AccountService.GetSubAccounts((MasterAccount) account);
                    SubAccountsButton.Visible = accounts.Count > 0;
                    IsMaster = true;
                } else {
                    SubAccountsButton.Visible = false;
                    IsMaster = false;
                }

                var canScrub = ScrubbingService.CanScrubCustomer(account);
                if (!canScrub.IsSuccessful) {
                    ScrubTip.Text = canScrub.Message;
                    DeleteCustomerButton.Visible = false;
                }

                SetTab(AccountTab.Details);

                var searchEntity = AccountSearchEntity.GetAcountSearchEntity(account);

                hdnContactId.Value = searchEntity.ContactId.ToString();
                hdnCompanyId.Value = searchEntity.CompanyId.ToString();
                hdnMasterAccountId.Value = searchEntity.MasterAccountId.ToString();

                Account.AccountId = searchEntity.Id;
                Account.AccountNumber = searchEntity.AccountNumber;
                Account.AccountName = searchEntity.AccountName;
                Account.Contact = searchEntity.ContactName;                
                Account.Company = searchEntity.CompanyName;
                Account.Password = searchEntity.Password;
                Account.IsInvoiceRoot = searchEntity.IsInvoiceRoot;                
                Account.BillingAddressType = searchEntity.BillingAddressType;
                Account.BillingMethod = searchEntity.BillingMethod;
                Account.EmailBill = searchEntity.EmailBill;
                Account.EmailBillDataFile = searchEntity.EmailBillDataFile;
                Account.PostBill = searchEntity.PostBill;                    
                Account.IsInternational = searchEntity.IsInternational;
                Account.AmountPaid = searchEntity.AmountPaid;
                Account.PreviousBill = searchEntity.PreviousBill;
                
                if (!searchEntity.IsInvoiceRoot) {
                
                    ShowInvoiceRootAccountButton.NavigateUrl =
                        NavigationController.GetEditAccountLink(searchEntity.MasterAccountId);                    
                }

                Account.ReadOnly = false;
            }           
        }

        private void SetTab(AccountTab tab)
        {           
            AccountDetailsPanel.Visible = tab == AccountTab.Details;
            pnlActivityLog.Visible = (tab == AccountTab.ActivityLog || tab == AccountTab.SubAccounts);
            SubAccountsPanel.Visible = tab == AccountTab.SubAccounts;
            LogViewer.Visible = tab == AccountTab.ActivityLog;            
        }

        protected void lnkDetailsView_Click(object sender, EventArgs e)
        {
            if (!Account.AccountId.HasValue)
            {
                UserMessage.SetFailure("Could not find the Account. Invalid Id");
                SetTab(AccountTab.None);
                return;
            }

            SetTab(AccountTab.Details);

            // This should cause the controls to get updated?
            Account.Contact = Account.Contact;
            Account.Company = Account.Company;
        }

        protected void GotoSearch(object sender, EventArgs e)
        {
            NavigationController.AccountSearch();
        }

        protected void ViewSubAccounts(object sender, EventArgs e)
        {
            lblAccountName.Text = Account.AccountName;
            lblAccountNumber.Text = Account.AccountNumber;

            if (!Account.AccountId.HasValue)
            {
                UserMessage.SetFailure("Could not find the Account. Invalid Id");
                SetTab(AccountTab.None);
                return;
            }

            SetTab(AccountTab.SubAccounts);

            var account = AccountService.GetAccountById(Account.AccountId.Value);
            if (account is MasterAccount) {

                SubAccountsGrid.DataSource = AccountService.GetSubAccounts((MasterAccount) account);
                SubAccountsGrid.DataBind();

            }
            
        }

        protected void PurgeCustomer(object sender, EventArgs e)
        {
            try {
                var account = AccountService.GetAccountById(Account.AccountId.Value);

                var response = ScrubbingService.ScrubCustomer(account);
                if (response.IsSuccessful) {
                    NavigationController.AccountSearch();
                } else {
                    UserMessage.SetFailure(response.Message);
                }
            }
            catch (Exception ex) {
                
                LoggingUtility.LogException(ex);
                UserMessage.SetFailure("Internal Error.  Check Logs");

            }
        }

        protected void SubAccountsGrid_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            NavigationController.EditAccount(args.Key);
        }
        
        protected void MakeInvoiceRoot(object sender, EventArgs e)
        {
            try {
                var response = AccountService.MakeInvoiceRoot(Account.AccountId.Value);
                if (response.IsSuccessful) {
                    NavigationController.EditAccount(Account.AccountId.Value);
                }
                else {
                    UserMessage.SetFailure(response.Message);
                }
           // } catch (ThreadAbortException tex) {

            } catch (Exception ex) {
                LoggingUtility.LogException(ex);
                UserMessage.SetFailure("Internal Error.  Check Logs");
            }
        }

        protected void AttachToMasterAccount(object sender, EventArgs e)
        {            
            var response = AccountService.ReAttachToMasterAccount(Account.AccountId.Value);
            if (response.IsSuccessful) {
                NavigationController.EditAccount(Account.AccountId.Value);
            } else {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void CreateNewSubAccount(object sender, EventArgs e)
        {
            NavigationController.NewAccount(Account.AccountId.Value);
        }
    }
}