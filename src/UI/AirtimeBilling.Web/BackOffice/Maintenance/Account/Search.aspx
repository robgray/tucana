﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="AirtimeBilling.Web.I.Maintenance.AccountSearchPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Account Maintenance</h1>
<div class="well form-inline">
    <table>
        <tbody><tr><td>
            <label>Search By:</label>
            <asp:DropDownList ID="SearchType" runat="server">                        
            </asp:DropDownList>                
            <asp:TextBox ID="SearchValue" runat="server"></asp:TextBox>                                
            <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" Icon="search" Text="Search" runat="server" CausesValidation="false" OnClick="SearchForAccounts">
                <i class="icon-search icon-white"></i> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn" ID="AddButton" Icon="add" Text="New Account" runat="server" CausesValidation="false"  OnClick="NewAccount" />
        </td></tr></tbody>                    
    </table>                
</div>    
<asp:Panel ID="SearchResultsPanel" runat="server">    
    <sharp:ClickableGridView ID="SearchResultsGrid" runat="server"                   
        AutoGenerateColumns="False" 
        DataKeyNames="Id"
        OnRowClicked="SearchResultsGrid_RowClicked">        
    <Columns>                
        <asp:BoundField DataField="AccountNumber" HeaderText="Account Number" ItemStyle-Width="50px" />
        <asp:BoundField DataField="ContactName" HeaderText="Contact" ItemStyle-Width="200px" />         
        <asp:BoundField DataField="AccountName" HeaderText="Account Name" />         
    </Columns>            
    </sharp:ClickableGridView>    
    <sharp:MessagePanel runat="server" ID="EmptyMessage" IncludeClose="False" />
</asp:Panel>     
</asp:Content>
