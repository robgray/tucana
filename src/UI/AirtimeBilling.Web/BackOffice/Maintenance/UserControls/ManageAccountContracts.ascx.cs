﻿using System;
using System.Web.UI;

namespace AirtimeBilling.Web
{
    public partial class ManageAccountContractsControl : BaseUserControl
    {
        public int AccountId
        {
            get { return Convert.ToInt32(hdnAccountId.Value); }
            set
            {
                hdnAccountId.Value = value.ToString();

                // Retrieve all contracts on this and sub accounts.
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}