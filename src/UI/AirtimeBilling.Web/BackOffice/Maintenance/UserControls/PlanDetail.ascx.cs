﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using System.Globalization;

namespace AirtimeBilling.Web
{
    public partial class PlanDetailControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindTariffs(NetworkId);

            }
        }
        

        public int PlanId
        {
            get
            {               
                return (int) (ViewState["PlanDetail_PlanId"] ?? 0);               
            }
            set
            {
                ViewState["PlanDetail_PlanId"] = value;
            }
        }

        public string Name
        {
            get { return txtName.Text; }
            set { txtName.Text = value; }
        }

        public int Duration
        {
            get { return int.Parse(txtDuration.Text); }
            set { txtDuration.Text = value.ToString(); }
        }

        public decimal Flagfall
        {
            get
            {
                return decimal.Parse(txtFlagfall.Text.Trim(), NumberStyles.AllowCurrencySymbol
                | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands);
            }
            set { txtFlagfall.Text = value.ToString("c"); }
        }

        public decimal FreeCallAmount
        {
            get
            {
                return decimal.Parse(txtFreeCallAmount.Text.Trim(), NumberStyles.AllowCurrencySymbol
                    | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands);
            }
            set { txtFreeCallAmount.Text = value.ToString("c"); }
        }

        public decimal PlanAmount
        {
            get
            {
                return decimal.Parse(txtPlanAmount.Text.Trim(), NumberStyles.AllowCurrencySymbol
                    | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands);
            }
            set { txtPlanAmount.Text = value.ToString("c"); }
        }

        public int UnitOfTime
        {
            get
            {
                return int.Parse(txtUnitOfTime.Text.Trim());
            }
            set { txtUnitOfTime.Text = value.ToString(); }
        }

        public int NetworkId
        {
            get
            {
                if (ViewState["PlanDetail_NetworkId"] != null)
                {
                    return int.Parse(ViewState["PlanDetail_NetworkId"].ToString());
                }                
                return 0;                
            }
            set
            {
                if (this.NetworkId != value)
                {
                    ViewState["PlanDetail_NetworkId"] = value;
                    Network network = NetworkService.GetNetwork(value);
                    if (network != null) txtNetwork.Text = network.Name;
                    BindTariffs(value);
                }
            }
        }

        public int DefaultNetworkTariffId
        {
            get
            {
                if (ddlDefaultTariff.SelectedIndex > -1)
                {
                    return int.Parse(ddlDefaultTariff.SelectedValue.ToString());
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                if (ddlDefaultTariff.Items.Count > 0)
                {
                    ListItem item = ddlDefaultTariff.Items.FindByValue(value.ToString());
                    if (item != null)
                    {
                        ddlDefaultTariff.SelectedItem.Selected = false;
                        item.Selected = true;
                    }
                }
            }
        }

        private void BindTariffs(int networkId)
        {
            IList<NetworkTariff> tariffs = NetworkService.GetNetworkTariffs(networkId);

            ddlDefaultTariff.DataSource = tariffs;
            ddlDefaultTariff.DataValueField = "Id";
            ddlDefaultTariff.DataTextField = "Name";
            ddlDefaultTariff.DataBind();
        }

        public void Clear()
        {
            txtDuration.Text = string.Empty;
            txtName.Text = string.Empty;
            txtFreeCallAmount.Text = string.Empty;
            txtPlanAmount.Text = string.Empty;
            txtFlagfall.Text = string.Empty;

            ddlDefaultTariff.SelectedIndex = -1;
        }
    }
}