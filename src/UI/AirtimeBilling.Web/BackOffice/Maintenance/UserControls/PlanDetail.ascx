﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.PlanDetailControl" Codebehind="PlanDetail.ascx.cs" %>
<sharp:FormGroup ID="MaintainPlan" runat="server" GroupingText="Details">    
    <div class="control-group">
        <label class="control-label" for="<%= txtNetwork.ClientID %>">Network</label>
        <div class="controls">
        <asp:TextBox ID="txtNetwork" runat="server" CssClass="EntryBox" ReadOnly="true"></asp:TextBox>            
        </div>
    </div>         
    <div class="control-group">
        <label class="control-label" for="<%= txtName.ClientID %>">Name</label>
        <div class="controls">
        <asp:TextBox ID="txtName" runat="server" CssClass="EntryBox"></asp:TextBox>    
        <asp:RequiredFieldValidator ID="rfvPlanName" runat="server" 
            ErrorMessage="Missing Plan Name" ControlToValidate="txtName" 
            Display="None" ></asp:RequiredFieldValidator>        
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtDuration.ClientID %>">Duration</label>
        <div class="controls">
        <asp:TextBox ID="txtDuration" runat="server" style="width:40px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" 
            ErrorMessage="Missing Duration"
            ControlToValidate="txtDuration" Display="None" />            
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
            ErrorMessage="Plan duration is not a valid number" Text="Not a valid number"
            ControlToValidate="txtDuration"
            ValidationExpression="^\d+" Display="None" />
        <asp:RangeValidator ID="RangeValidator1" runat="server" 
            ErrorMessage="Duration must be between 1 and 36 months" Text="Must be between 1 and 36 months" 
            MaximumValue="36" MinimumValue="1"
            ControlToValidate="txtDuration" Type="Integer" Display="None" />
            </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtFlagfall.ClientID %>">Flagfall</label>
        <div class="controls">
        <asp:TextBox ID="txtFlagfall" runat="server" style="width:70px" Width="70px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
            ErrorMessage="Missing Flagfall" ControlToValidate="txtFlagfall" 
            Display="None"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
            ErrorMessage="Plan Flagfall is not a valid currency" Text="Invalid Currency"
            ControlToValidate="txtFlagFall" 
            ValidationExpression="^\${0,1}\d+(\.\d\d)?$" Display="None" />            
            </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtFreeCallAmount.ClientID %>">Free Call Amount</label>
        <div class="controls">
        <asp:TextBox ID="txtFreeCallAmount" runat="server" style="width:70px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" 
            ErrorMessage="Missing Free Call Amount" Text=" *" 
            ControlToValidate="txtFreeCallAmount" Display="None"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" 
            ErrorMessage="Plan Free Call Amount is not a valid currency" Text="Invalid Currency"
            ControlToValidate="txtFreeCallAmount"
            ValidationExpression="^\${0,1}\d+(\.\d\d)?$" Display="None" />            
            </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtPlanAmount.ClientID %>">Plan Amount</label>
        <div class="controls">
        <asp:TextBox ID="txtPlanAmount" runat="server" style="width:70px"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" 
            ErrorMessage="Missing Plan Amount" Text=" *" ControlToValidate="txtPlanAmount" 
            Display="None"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" 
            ErrorMessage="Plan Amount is not a valid currency" Text="Invalid Currency"
            ControlToValidate="txtPlanAmount"
            ValidationExpression="^\${0,1}\d+(\.\d\d)?$" Display="None" />            
            </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= txtUnitOfTime.ClientID %>">Billing Interval</label>
        <div class="controls">
        <asp:TextBox ID="txtUnitOfTime" runat="server" style="width:35px"></asp:TextBox>&nbsp;seconds
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" 
            ErrorMessage="Missing Plan Amount" Text=" *" ControlToValidate="txtPlanAmount" 
            Display="None"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" 
            ErrorMessage="Unit of Time" Text="Invalid Unit of Time"
            ControlToValidate="txtUnitOfTime"
            ValidationExpression="^\d+$" Display="None" />            
            </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= ddlDefaultTariff.ClientID %>">Default Tariff</label>
        <div class="controls">
        <asp:DropDownList ID="ddlDefaultTariff" runat="server" CssClass="EntryBox">
        </asp:DropDownList>
        </div>
    </div>      
</sharp:FormGroup>