﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ManageAccountContractsControl" Codebehind="ManageAccountContracts.ascx.cs" %>
<div class="clear">
<div>
    <asp:HiddenField ID="hdnAccountId" runat="server" />
    <asp:GridView ID="GridView1" runat="server"
        AutoGenerateColumns="False"         
        CellPadding="2" 
        ForeColor="#333333" 
        GridLines="None" Width="100%">
        <Columns>
            <asp:CheckBoxField HeaderText="Move" >            
                <ItemStyle Width="30px" />
            </asp:CheckBoxField>
            <asp:BoundField DataField="ContractNumber" HeaderText="Contract #">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Plan" HeaderText="Plan">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="ContactName" HeaderText="Contact">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="AccountNumber" HeaderText="Account #">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="ContractId" HeaderText="Contract Id" Visible="False" />
        </Columns>
        <HeaderStyle CssClass="gen-header" />
        <AlternatingRowStyle CssClass="alt" />  
    </asp:GridView>
</div>
<br />
<div style="clear:both; padding-top:20px">
    <label class="control-label" for="<%= ddlToAccount.ClientID %>">Move To Account:</label>
    <asp:DropDownList ID="ddlToAccount" runat="server">
    </asp:DropDownList>
    &nbsp;<asp:LinkButton ID="lnkMove" runat="server">Move</asp:LinkButton>
</div>
    
    
</div>