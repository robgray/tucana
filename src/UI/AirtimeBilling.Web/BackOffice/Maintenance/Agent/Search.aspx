﻿<%@ Page Title="Agent Search" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.AgentSearchPage" Codebehind="Search.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Agent Maintenance</h1>
<div class="well form-inline">
    <table>
        <tbody><tr><td>
            <label>Search By:</label>
            <asp:DropDownList ID="SearchType" runat="server">     
                <asp:ListItem>Agent Name</asp:ListItem>                   
            </asp:DropDownList>                
            <asp:TextBox ID="SearchValue" runat="server"></asp:TextBox>                                
            <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" Icon="search" Text="Search" runat="server" CausesValidation="false" OnClick="SearchForAgents">
                <i class="icon-search icon-white"></i> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" ID="AddButton" Icon="add" Text="New Agent" runat="server" CausesValidation="false" OnClick="NewAgent" />
        </td></tr></tbody>                    
    </table>                
</div>     
<asp:Panel ID="SearchResultsPanel" runat="server">    
    <sharp:ClickableGridView ID="SearchResultsGrid" runat="server"                   
        AutoGenerateColumns="False" 
        DataKeyNames="Id"
        onpageindexchanging="SearchResultsGrid_PageIndexChanging" 
        onrowclicked="SearchResultsGrid_RowClicked">    
    <Columns>                
        <asp:BoundField DataField="AgentName" HeaderText="Agent" />
        <asp:BoundField DataField="Address" HeaderText="Address" />         
    </Columns>            
    </sharp:ClickableGridView>    
    <sharp:MessagePanel runat="server" ID="EmptyMessage" IncludeClose="False" />
</asp:Panel>
</asp:Content>

