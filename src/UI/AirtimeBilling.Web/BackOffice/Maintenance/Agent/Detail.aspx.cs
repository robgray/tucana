﻿using System;
using System.Web.UI;
using AirtimeBilling.Core.Entities;
using System.Web;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class AgentDetailPage : BasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            UserMessage.Clear();

            if (IsPostBack) return;

            int agentId = 0;
            var user = Users.GetUser(HttpContext.Current.User.Identity.Name);
            var agentUser = user as AgentUser;

            if (Request.QueryString["Id"] != null)
            {
                if (!int.TryParse(Request.QueryString["Id"], out agentId))
                {
                    CurrentSession.ExceptionMessage = "Invalid Agent Id";
                    throw new ArgumentOutOfRangeException();
                }
            }
            
            else if (agentUser != null)
            {
                agentId = agentUser.AgentId;
                OptionsPanel.Visible = false;
            }
                        
            var agent = AgentService.GetAgent(agentId);

            if (agent == null && user is AgentUser)
            {                
                
                Response.Redirect(Users.Current.HomePage, true);
                return;
            }

            if (agent != null) {
                // Populate!
                hdnAgentId.Value = agent.Id.ToString();
                txtAgent.Text = agent.AgentName;
                txtCommission.Text = agent.CommissionPercent.ToString();
                txtEmail.Text = agent.Email;
                AgentAddress.Street = agent.Address.Street;
                AgentAddress.Suburb = agent.Address.Suburb;
                AgentAddress.State = agent.Address.State;
                AgentAddress.Postcode = agent.Address.Postcode;
                rfvCommission.Enabled = Users.Current.IsInRole(Roles.BackOffice);
            }
        }
        protected void lnkSave_Click(object sender, EventArgs e)
        {         
            int agentId = 0;
            Agent agent;
            if (int.TryParse(hdnAgentId.Value, out agentId))
            {
                agent = new Agent(agentId);
            }
            else
            {
                agent = new Agent();
            }

            agent.AgentName = txtAgent.Text;
            if (!string.IsNullOrEmpty(txtCommission.Text)) agent.CommissionPercent = decimal.Parse(txtCommission.Text);
            agent.Email = txtEmail.Text;
            agent.Address = new Address
                                {
                                    Street = AgentAddress.Street,
                                    Suburb = AgentAddress.Suburb,
                                    State = AgentAddress.State,
                                    Postcode = AgentAddress.Postcode
                                };

            try
            {
                var response = AgentService.SaveAgent(new SaveAgentRequest { Agent = agent });
                if (response.IsSuccessful)
                {
                    UserMessage.SetSuccess("Agent saved");                    
                }
                else
                {
                    UserMessage.SetFailure("Failed to save Agent: " + response.Message);
                }
            }
            catch (Exception)
            {
                CurrentSession.ExceptionMessage = "Unable to save Agent.";
                throw;
            }
        }

        protected void GotoSearch(object sender, EventArgs e)
        {
            NavigationController.AgentSearch();
        }
    }
}