﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Common;

namespace AirtimeBilling.Web
{
    public enum AgentSearch
    {
        [EnumDescription("Agent Name")]
        AgentName
    }

    public partial class AgentSearchPage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            EmptyMessage.Clear();
        }

        protected void SearchResultsGrid_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {            
            if (args.HasKey)
                NavigationController.EditAgent(args.Key);        
        }

        protected void SearchResultsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
        }
             
        protected void NewAgent(object sender, EventArgs e)
        {
            NavigationController.NewAgent();
        }                

        protected void SearchForAgents(object sender, EventArgs e)
        {
            var agents = AgentService.FindAgentsByName(SearchValue.Text);
            if (agents.Count > 0) {
                SearchResultsGrid.DataSource = agents;
                SearchResultsGrid.DataBind();
            } else {
                EmptyMessage.SetInformation("No agents were found matching your search parameters");
            }
        }
    }
}