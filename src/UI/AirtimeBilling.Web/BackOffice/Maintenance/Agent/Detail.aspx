﻿<%@ Page Title="Agent Details" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.AgentDetailPage" Codebehind="Detail.aspx.cs" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="address" TagPrefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Agent Maintenance</h1>
<asp:Panel runat="server" ID="OptionsPanel" CssClass="well">
    <asp:LinkButton CssClass="btn" ID="SearchButton" runat="server" Text="Search" Icon="search" onclick="GotoSearch" CausesValidation="False">
        <i class="icon-search"></i> Search
    </asp:LinkButton>
</asp:Panel>
<div class="formcontent">
    <sharp:MessagePanel ID="UserMessage" runat="server" />
    <asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" runat="server" ShowSummary="True"  />
        
    <div class="row">
        <asp:HiddenField ID="hdnAgentId" runat="server" />
        <sharp:FormGroup ID="Agent" runat="server" GroupingText="Agent" CssClass="span6">            
            <div class="control-group">
                <label class="control-label" for="<%= txtAgent.ClientID %>">Agent</label>
                <div class="controls">
                    <asp:TextBox ID="txtAgent" runat="server" CssClass="EntryBox" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvAgentName" runat="server" 
                        ErrorMessage="Missing Agent Name" Display="None" 
                        ControlToValidate="txtAgent"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<%=  txtEmail.ClientID %>">Email</label>            
                <div class="controls">
                    <asp:TextBox ID="txtEmail" runat="server" Width="250px" MaxLength="255"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" 
                        ErrorMessage="Missing Email Address" Display="None"
                        ControlToValidate="txtEmail"></asp:RequiredFieldValidator>
                </div>
            </div>       
            <% if (Users.Current.IsInRole(AirtimeBilling.Web.Roles.BackOffice))
               { %>
            <div class="control-group">
                <label class="control-label" for="<%= txtCommission.ClientID %>" style="text-align: right">Commission</label>
                <div class="controls">
                    <asp:TextBox ID="txtCommission" runat="server" CssClass="EntryBox" MaxLength="10" Width="50px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvCommission" runat="server" 
                        ErrorMessage="Missing Commission" Display="None"
                        ControlToValidate="txtCommission"></asp:RequiredFieldValidator>
                </div>
            </div>
            <% } %>
        </sharp:FormGroup>    
        <sharp:Address ID="AgentAddress" runat="server" Text="Address" />
    </div>        
    <div class="form-actions">                    
        <asp:LinkButton CssClass="btn btn-success" ID="lnkSave" runat="server" 
                Text="Save" Icon="disk"  onclick="lnkSave_Click">
            <i class="icon-ok icon-white"></i> Save
        </asp:LinkButton> 
    </div>    
</div>
</asp:Content>

