<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="CustomFees.aspx.cs" Inherits="AirtimeBilling.Web.Admin.CustomFees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Custom Fees</h1>
<div class="formcontent Form">    
    <sharp:MessagePanel ID="UserMessage" runat="server" />       
    <asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True" ValidationGroup="Edit" />
    <asp:ValidationSummary ID="valNewSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True" ValidationGroup="New" />
    <asp:ValidationSummary ID="valNoneSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True" ValidationGroup="None" />
    <div class="alert alert-info">Custom Fees are applied to individual contracts and can be one-off or recurring.
    <br />One fees are invoiced in the month they are applied to the contract and then removed after invoice.
    <br />Recurring fees remain for the life of the Contract, or until manually removed.  A Recurring fee is not applied in the month it is removed.</div>                                                           
    <asp:GridView ID="gvCustomFees" runat="server" AutoGenerateColumns="False" 
        DataKeyNames="Id" onrowcancelingedit="gvCustomFees_RowCancelingEdit" 
        onrowcommand="gvCustomFees_RowCommand" 
        onrowdatabound="gvCustomFees_RowDataBound" 
        onrowdeleting="gvCustomFees_RowDeleting" onrowediting="gvCustomFees_RowEditing" 
        onrowupdating="gvCustomFees_RowUpdating" ShowFooter="True">                
        <Columns>               
            <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/cross.gif" 
                    EditImageUrl="~/images/pencil.gif" ShowEditButton="True" 
                    DeleteImageUrl="~/images/cross.gif" DeleteText = "Delete"
                    UpdateImageUrl="~/images/disk.gif" UpdateText="Save" 
                    ShowDeleteButton="True" HeaderStyle-Width="40px" 
                    ValidationGroup="Edit"
                ItemStyle-Width="40px" >                           
            <HeaderStyle Width="40px" />
            <ItemStyle Width="40px" />
            </asp:CommandField>
            <asp:BoundField DataField="Id" HeaderText="Fee Id" ReadOnly="True" SortExpression="Id" Visible="false" />     
            <asp:TemplateField HeaderText="Description">                                                    
                <ItemStyle Width="400px" />
                <EditItemTemplate>                                                
                    <asp:TextBox ID="EditFeeDescriptionTextBox" runat="server" Text='<%# Bind("Description") %>' Width="300" ValidationGroup="Edit"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" 
                        ErrorMessage="A Custom Fee description must be supplied" Text="*" Display="None"
                        ControlToValidate="EditFeeDescriptionTextBox" ValidationGroup="Edit" />
                </EditItemTemplate> 
                <FooterTemplate>
                    <asp:TextBox ID="NewFeeDescriptionTextBox" runat="server" Text="" Width="300" ValidationGroup="New"></asp:TextBox>   
                    <asp:RequiredFieldValidator ID="rfvNewDescription" runat="server" 
                        ErrorMessage="A Custom Fee description must be supplied" Text="*" Display="None" ValidationGroup="New"
                        ControlToValidate="NewFeeDescriptionTextBox" />                         
                </FooterTemplate>
                <ItemTemplate>
                    <asp:Label ID="FeeDescriptionLabel" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                </ItemTemplate>                    
            </asp:TemplateField>                        
            <asp:TemplateField HeaderText="Amount">                                        
                <ItemStyle Width="170px" />
                <EditItemTemplate>                        
                    <asp:TextBox ID="EditFeeAmountTextBox" runat="server" Text='<%# Bind("Amount", "{0:c}") %>' Width="60px" ValidationGroup="Edit"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvFeeAmount" runat="server" 
                        ErrorMessage="A Custom Fee amount must be supplied" Text="*" Display="None"
                        ControlToValidate="EditFeeAmountTextBox" ValidationGroup="Edit" />
                    <asp:RegularExpressionValidator ID="rgeFeeAmount" runat="server"
                        ErrorMessage="Custom Fee Amount must be a monetary amount" Text="*" Display="None"
                        ControlToValidate="EditFeeAmountTextBox" ValidationExpression="^(-)?\${0,1}\d+(\.\d\d)?$" />                        
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="NewFeeAmountTextBox" runat="server" Text="" Width="60px" ValidationGroup="New"></asp:TextBox>    
                    <asp:RequiredFieldValidator ID="rfvNewFeeAmount" runat="server" 
                        ErrorMessage="A Custom Fee amount must be supplied" Text="*" Display="None"
                        ControlToValidate="NewFeeAmountTextBox" ValidationGroup="New"  />       
                    <asp:RegularExpressionValidator ID="rgeNewFeeAmount" runat="server"
                        ErrorMessage="Custom Fee Amount must be a monetary amount" Text="*" Display="None" ValidationGroup="New"
                        ControlToValidate="NewFeeAmountTextBox" ValidationExpression="^(-)?\${0,1}\d+(\.\d\d)?$" />             
                </FooterTemplate>
                <ItemTemplate>
                    <asp:Label ID="FeeAmountLabel" runat="server" Text='<%# Bind("Amount", "{0:c}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Recurring?">                                        
                <ItemStyle Width="50px" />
                <EditItemTemplate>                        
                    <asp:CheckBox ID="EditFeeRecurringCheckBox" runat="server" Checked='<%# Bind("IsRecurring") %>'></asp:CheckBox>                        
                </EditItemTemplate>
                <FooterTemplate>
                    <asp:CheckBox ID="NewFeeRecurringCheckBox" runat="server" Checked="false"></asp:CheckBox>                        
                </FooterTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="FeeRecurringCheckBox" runat="server" Checked='<%# Bind("IsRecurring") %>' Enabled="false"></asp:CheckBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                    <EditItemTemplate>
                    </EditItemTemplate>
                    <FooterTemplate>
                        <asp:ImageButton runat="server" id="SaveCustomFeeButton" CssClass="icon" ImageUrl="~/Images/disk.gif"
                         CausesValidation="true" CommandName="Insert" ValidationGroup="New" />                        
                    </FooterTemplate>
                </asp:TemplateField>                
        </Columns>        
    </asp:GridView>    
     <asp:PlaceHolder ID="EmptyCustomFeesPlaceholder" runat="server">
        <table class="Grid" cellspacing="0" cellpadding="2" border="0" style="border-collapse:collapse;">
            <tbody>
                <tr>
                    <th scope="col" style="width:40px;"> </th>
                    <th scope="col">Description</th>
                    <th scope="col">Amount</th>
                    <th align="center" scope="col" style="width:50px">Recurring?</th>             
                    <th scope="col"> </th>
                </tr>
                <tr class="Footer">
                    <td style="width:40px;"> </td>
                    <td style="width:400px;">
                        <asp:TextBox ID="NewFeeDescriptionTextBox" runat="server" Width="300px" ValidationGroup="None"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvDescription" runat="server" 
                            ErrorMessage="A Custom Fee description must be supplied" Text="*" Display="None"
                            ControlToValidate="NewFeeDescriptionTextBox" ValidationGroup="None" />
                    </td>
                    <td style="width:170px;">
                        <asp:TextBox ID="NewFeeAmountTextBox" runat="server" Width="60px" ValidationGroup="None"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvNewFeeAmount" runat="server" 
                            ErrorMessage="A Custom Fee amount must be supplied" Text="*" Display="None"
                            ControlToValidate="NewFeeAmountTextBox" ValidationGroup="None"  />       
                        <asp:RegularExpressionValidator ID="rgeNewFeeAmount" runat="server"
                            ErrorMessage="Custom Fee Amount must be a monetary amount" Text="*" Display="None" ValidationGroup="None"
                            ControlToValidate="NewFeeAmountTextBox" ValidationExpression="^(-)?\${0,1}\d+(\.\d\d)?$" />
                    </td>
                    <td align="center" style="width:50px;"><asp:CheckBox ID="NewIsRecurringCheckBox" runat="server" /></td>                            
                    <td><asp:ImageButton runat="server" id="SaveNewFeeButton" CssClass="icon" ImageUrl="~/Images/disk.gif" OnClick="SaveNewFee" ValidationGroup="None"/></td>                            
                </tr>
            </tbody>
        </table>                                                                    
    </asp:PlaceHolder>           
</div>
</asp:Content>
