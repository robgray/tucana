﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web.Admin
{
    public partial class CustomFees : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            UserMessage.Clear();

            if (!IsPostBack) {
                RefreshFeeGrid();
            }
        }

        protected void SaveCustomFee(object sender, ImageClickEventArgs e)
        {
            
        }

        private void RefreshFeeGrid()
        {
            var fees = InvoicingService.GetAllCustomFees();

            gvCustomFees.DataSource = fees;
            gvCustomFees.DataBind();

            EmptyCustomFeesPlaceholder.Visible = fees.Count == 0;
        }

        protected void gvCustomFees_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Insert":                    
                    var descriptionTextBox = (TextBox)gvCustomFees.FooterRow.FindControl("NewFeeDescriptionTextBox");
                    var amountTextBox = (TextBox)gvCustomFees.FooterRow.FindControl("NewFeeAmountTextBox");
                    var recurringCheckBox = (CheckBox)gvCustomFees.FooterRow.FindControl("NewFeeRecurringCheckBox");

                    var fee = new CustomFee()
                                  {
                                      Description = descriptionTextBox.Text,
                                      Amount = amountTextBox.Text.ToDecimalCurrency(),
                                      IsRecurring = recurringCheckBox.Checked
                                  };

                    var response = InvoicingService.SaveCustomFee(fee);
                    if (response.IsSuccessful) {
                        gvCustomFees.EditIndex = -1;
                        RefreshFeeGrid();
                        UserMessage.SetSuccess("Custom Fee added.");
                    }
                    else {
                        UserMessage.SetFailure(response.Message);
                    }

                    break;

                case "Cancel":
                    gvCustomFees.EditIndex = -1;
                    RefreshFeeGrid();
                    break;
            }
        }

        protected void gvCustomFees_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvCustomFees_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void gvCustomFees_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (this.Request["x"] != null && this.Request["y"] != null)
            {
                gvCustomFees.EditIndex = -1;
                RefreshFeeGrid();
            }
            else
            {

                int customFeeId = (int)gvCustomFees.DataKeys[e.RowIndex].Value;

                var fee = new CustomFee(customFeeId);

                var response = InvoicingService.RemoveCustomFee(fee);
                if (response.IsSuccessful)
                {
                    gvCustomFees.EditIndex = -1;
                    RefreshFeeGrid();                    

                    UserMessage.SetSuccess("Custom Fee deleted.");
                }
                else
                {
                    UserMessage.SetFailure(response.Message);
                }
            }
        }

        protected void gvCustomFees_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCustomFees.EditIndex = e.NewEditIndex;
            RefreshFeeGrid();
        }

        protected void gvCustomFees_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var customFeeId = (int)gvCustomFees.DataKeys[e.RowIndex].Value;
            var description = gvCustomFees.Rows[e.RowIndex].Cells[2].Controls[1] as TextBox;
            var amount = gvCustomFees.Rows[e.RowIndex].Cells[3].Controls[1] as TextBox;
            var isRecurring = gvCustomFees.Rows[e.RowIndex].Cells[4].Controls[1] as CheckBox;

            var fee = new CustomFee(customFeeId)
                          {
                              Description = description.Text,
                              Amount = amount.Text.ToDecimalCurrency(),
                              IsRecurring = isRecurring.Checked
                          };

            var response = InvoicingService.SaveCustomFee(fee);
            
            if (response.IsSuccessful)
            {
                gvCustomFees.EditIndex = -1;
                RefreshFeeGrid();
                UserMessage.SetSuccess("Custom Fee updated.");
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void SaveNewFee(object sender, ImageClickEventArgs e)
        {
            var fee = new CustomFee
            {
                Description = NewFeeDescriptionTextBox.Text,
                Amount = NewFeeAmountTextBox.Text.ToDecimalCurrency(),
                IsRecurring = NewIsRecurringCheckBox.Checked
            };

            var response = InvoicingService.SaveCustomFee(fee);
            if (response.IsSuccessful)
            {
                gvCustomFees.EditIndex = -1;
                RefreshFeeGrid();

                UserMessage.SetSuccess("Custom Fee added.");
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }
    }
}
