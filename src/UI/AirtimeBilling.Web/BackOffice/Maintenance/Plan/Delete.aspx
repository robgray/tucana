﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="AirtimeBilling.Web.I.Maintenance.DeletePlan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Delete Plan</h1>
<sharp:MessagePanel ID="UserMessage" runat="server"  />    
<p class="Note">Only Plans that are not used by any currently acitve contracts can be deleted.</p>
<div class="Form">
    <sharp:FormGroup ID="PlanSummaryFG" runat="server" GroupingText="Plan Summary">
        <div class="control-group">
            <label>Plan Name</label>
            <asp:Label ID="PlanNameLabel" runat="server"></asp:Label>
        </div>
        <div class="control-group">
            <label>Network</label>
            <asp:Label ID="NetworkLabel" runat="server"></asp:Label>
        </div>
        <div class="control-group">
            <label>Active Contracts</label>
            <asp:Label ID="ActiveContractsLabel" runat="server"></asp:Label>
        </div>
    </sharp:FormGroup>
</div>
<div class="crudbarfloat">                
    <asp:LinkButton CssClass="btn btn-primary" ID="DeletePlanButton" OnClientClick="return confirm('Are you sure?')" runat="server" Text="Delete" OnClick="DeletePlanAction" />                
</div>        
</asp:Content>
