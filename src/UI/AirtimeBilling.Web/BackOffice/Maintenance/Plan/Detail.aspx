﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="AirtimeBilling.Web.I.Maintenance.Plan" %>
<%@ Register TagPrefix="sharp" Src="~/BackOffice/Maintenance/UserControls/PlanDetail.ascx" TagName="plandetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Plan Maintenance</h1>
<sharp:MessagePanel ID="UserMessage" runat="server" />
<asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True"  /> 
<asp:Panel ID="DetailsTab" runat="server" CssClass="Form">  
    <sharp:plandetail ID="MaintainPlanDetail" runat="server" Text="Maintain Plan" />   
    <br/>    
    <asp:GridView ID="gvTariffs" runat="server"            
        OnRowDataBound="gvTariffs_RowDataBound" 
        AutoGenerateColumns="False"                                     
        OnPageIndexChanging="gvTariffs_PageIndexChanging"                        
        onrowediting="gvTariffs_RowEditing" 
        onrowcancelingedit="gvTariffs_RowCancelingEdit" 
        onrowupdating="gvTariffs_RowUpdating" DataKeyNames="Id">                                                     
        <Columns>
            <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/cross.gif" 
                EditImageUrl="~/images/pencil.gif" ShowEditButton="True" 
                UpdateImageUrl="~/images/disk.gif" UpdateText="Save" ItemStyle-Width="40px" HeaderStyle-Width="40px" />
            <asp:TemplateField HeaderText="Code" SortExpression="Code">
                <HeaderStyle Width="50px" />
                <ItemStyle Width="50px" />                                        
                <EditItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Tariff" SortExpression="Tariff">
                <HeaderStyle Width="200px" />    
                <ItemStyle Width="200px" />                    
                <EditItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Cost $/min" SortExpression="UnitCost">                    
                <HeaderStyle Width="70px" />
                <ItemStyle Width="70px" />
                <EditItemTemplate>
                    <asp:TextBox ID="txtUnitCost" runat="server" Text='<%# Bind("UnitCost", "{0:c}") %>' Width="60"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" 
                        ErrorMessage="Missing Cost $/min" ControlToValidate="txtUnitCost" 
                        Display="None"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" 
                        ErrorMessage="Tariff Cost $/min is not a valid currency" Text="Invalid Currency"
                        ControlToValidate="txtUnitCost" 
                        ValidationExpression="^\${0,1}\d+(\.\d\d)?$" Display="None" />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblUnitCost" runat="server" Text='<%# Eval("UnitCost", "{0:c}") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Has Flagfall" 
                SortExpression="HasFlagfall">                    
                <EditItemTemplate>
                    <asp:CheckBox ID="chkFlagfall" runat="server" 
                        Checked='<%# Bind("HasFlagfall") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1Flagfall" runat="server" 
                        Checked='<%# Bind("HasFlagfall") %>' Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Include in Free Call" 
                SortExpression="IsCountedInFreeCall">
                <EditItemTemplate>
                    <asp:CheckBox ID="chkFreeCall" runat="server" 
                        Checked='<%# Bind("IsCountedInFreeCall") %>' />
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:CheckBox ID="CheckBox1" runat="server" 
                        Checked='<%# Bind("IsCountedInFreeCall") %>' Enabled="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="NetworkTariffId" 
                SortExpression="NetworkTariffId" Visible="False">
                <EditItemTemplate>                            
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("NetworkTariffId") %>'></asp:Label>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("NetworkTariffId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>        
                          
    <div class="form-actions">                    
        <asp:LinkButton CssClass="btn btn-success" ID="lnkSave" runat="server" Text="Save" Icon="disk"  onclick="SavePlan">
            <i class="icon-ok icon-white"></i> Save Plan
        </asp:LinkButton>
    </div>
</asp:Panel> 

<asp:Panel ID="TariffsTab" runat="server" CssClass="Form">  
    <div class="Form">
        <sharp:FormGroup ID="TariffsFG" runat="server" GroupingText="Plan Summary" CssClass="single module">                
            <div class="control-group" >
                <label class="control-label" for="<%= PlanLabel.ClientID %>">Plan</label>
                <asp:Label ID="PlanLabel" runat="server" Text="" ></asp:Label>                        
            </div>
            <div class="control-group" >
                <label class="control-label" for="<%= NetworkLabel.ClientID %>">Network</label>
                <asp:Label ID="NetworkLabel" runat="server" Text="" ></asp:Label>                        
            </div>
            <div class="clear"></div>
        </sharp:FormGroup>
    </div>        
                      
    
</asp:Panel>  
</asp:Content>
