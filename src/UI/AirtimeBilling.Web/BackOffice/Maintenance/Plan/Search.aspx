﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.PlansMaintenancePage" Codebehind="Search.aspx.cs" %>
<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">    
<h1>Plan Maintenance</h1>
<div class="well form-inline">
    <table>
        <tbody><tr><td>
            <label>Network:</label>
                    <asp:DropDownList ID="Networks" runat="server" CssClass="EntryBox" style="vertical-align:middle;"></asp:DropDownList>                
                    <asp:LinkButton CssClass="btn btn-primary" ID="lnkSearch" runat="server" Text="Search" Icon="search"  onclick="Search" CausesValidation="false">
                        <i class="icon-search icon-white"></i> Search
                    </asp:LinkButton>
                    <asp:LinkButton CssClass="btn btn-primary" ID="lnkAddPlan" runat="server" Text="New Plan" Icon="add"  onclick="AddPlan" style="vertical-align:middle;" />		
        </td></tr></tbody>                    
    </table>                
</div> 

<asp:GridView ID="gvPlans" runat="server"    
    OnRowCommand="gvPlans_RowCommand" 
    AutoGenerateColumns="False"         
    OnPageIndexChanging="gvPlans_PageIndexChanging">                                     
    <Columns>
        <asp:TemplateField>
            <HeaderStyle Width="45px" />
            <ItemStyle Width="45px" />
            <ItemTemplate >
                <asp:ImageButton ID="Edit" runat="server" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Id") %>' ImageUrl="~/images/pencil.gif" />&nbsp;
                <asp:ImageButton ID="Delete" runat="server" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem, "Id") %>' ImageUrl="~/images/cross.gif" />&nbsp;                
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Id" HeaderText="Pland Id" ReadOnly="True" SortExpression="Id" Visible="false" />
        <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name">
            <HeaderStyle Width="200px" />
            <ItemStyle Width = "200px" />
        </asp:BoundField>            
        <asp:BoundField DataField="Duration" HeaderText="Duration">
            <HeaderStyle Width="40px" />
            <ItemStyle Width="40px" />
        </asp:BoundField>
        <asp:BoundField DataField="Flagfall" HeaderText="Flagfall" DataFormatString="{0:c}">
            <HeaderStyle Width="50px" />
            <ItemStyle Width="50px" />
        </asp:BoundField>
        <asp:BoundField DataField="FreeCallAmount" HeaderText="Free Call Amount" DataFormatString="{0:c}">
            <HeaderStyle Width="100px" />
            <ItemStyle Width="80px" />
        </asp:BoundField>
        <asp:BoundField DataField="PlanAmount" HeaderText="Plan Amount" DataFormatString="{0:c}"  />                    
    </Columns>
</asp:GridView>            
<sharp:MessagePanel runat="server" ID="EmptyMessage" />
</asp:Content>