﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web.I.Maintenance
{
    public partial class DeletePlan : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);        
            if (Request.QueryString["id"] != null)
            {
                PlanId = int.Parse(Request.QueryString["id"]);

                var plan = PlanService.GetPlan(PlanId);

                if (plan == null) {
                    UserMessage.SetFailure("Could not find a Plan with this ID");
                }
                var response = PlanService.CanDelete(plan);
                if (response.IsDeleteable) {
                    UserMessage.SetWarning(
                        string.Format("Deleting '{0}' is permanent. Please ensure this you wish to delete this plan.",
                                      plan.Name));

                    DeletePlanButton.Visible = true;
                } else {                    
                    UserMessage.SetWarning(string.Format("The '{0}' plan has active contracts and cannot be deleted<br><br>Click <a href='{1}'>here</a> to return",
                                                         plan.Name, GetFullPath(Users.Current.HomePage)));
                    
                    DeletePlanButton.Visible = false;
                }

                var network = NetworkService.GetNetwork(plan.NetworkId);
                
                PlanNameLabel.Text = plan.Name;
                NetworkLabel.Text = network.Name;
                ActiveContractsLabel.Text = response.OpenContractCount.ToString();

            }            
        }

        protected int PlanId
        {
            get
            {
                if (ViewState["planid"] != null)
                {
                    return int.Parse(ViewState["planid"].ToString());
                }
                return 0;
            }

            set
            {
                ViewState["planid"] = value;                               
            }
        }
        
        protected void DeletePlanAction(object sender, EventArgs e)
        {         
            var request = new DeletePlanRequest
            {
                PlanId = this.PlanId,
                User = Users.Current
            };

            var response = PlanService.DeletePlan(request);
            if (response.IsSuccessful)
            {                
                UserMessage.SetSuccess(string.Format("Plan has been successfully deleted."));
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }
    }
}
