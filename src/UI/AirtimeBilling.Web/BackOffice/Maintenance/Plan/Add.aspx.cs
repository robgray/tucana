﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Web;

namespace AirtimeBilling.Web
{
    public partial class AddPlan : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            // If no planid, then display new plan.
            if (Request.QueryString["networkid"] != null)
            {
                NetworkId = int.Parse(Request.QueryString["networkid"].ToString());
                NewPlanDetail.NetworkId = NetworkId;                
            }
        }

        protected int NetworkId
        {
            get
            {
                if (ViewState["NetworkId"] != null)
                {
                    return int.Parse(ViewState["NetworkId"].ToString());
                }
                return 0;
            }
            set
            {
                ViewState["NetworkId"] = value;
            }
        }

        protected void AddNewPlan(object sender, EventArgs e)
        {
            // Add new 
            var plan = new Plan
            {
                Name = NewPlanDetail.Name,
                Duration = NewPlanDetail.Duration,
                FlagFall = NewPlanDetail.Flagfall,
                FreeCallAmount = NewPlanDetail.FreeCallAmount,
                NetworkId = NetworkId,
                PlanAmount = NewPlanDetail.PlanAmount,
                UnitOfTime = NewPlanDetail.UnitOfTime
            };

            var request = new InsertPlanRequest
            {
                Plan = plan,
                DefaultNetworkTariffId = NewPlanDetail.DefaultNetworkTariffId,
                User = Users.Current
            };

            var response = PlanService.InsertPlan(request);
            if (response.IsSuccessful)
            {
                UserMessage.MessageType = "Success";
                UserMessage.Message = "New Plan has been added.";
                NewPlanButton.Visible = false;
            }
            else
            {
                UserMessage.MessageType = "Failure";
                UserMessage.Message = response.Message;
            }
        }

        protected void GotoSearch(object sender, EventArgs e)
        {
            NavigationController.PlanSearch();
        }

    }
}
