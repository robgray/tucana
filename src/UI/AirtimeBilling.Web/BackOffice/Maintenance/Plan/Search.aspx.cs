﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    public partial class PlansMaintenancePage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);            

            if (!IsPostBack)
            {                
                var networks = NetworkService.GetAllNetworks();

                Networks.DataSource = networks;
                Networks.DataValueField = "Id";
                Networks.DataTextField = "Name";
                Networks.DataBind();

                if (Request.QueryString["networkid"] != null)
                {
                    int networkId = int.Parse(Request.QueryString["networkid"].ToString());
                    var item = Networks.Items.FindByValue(networkId.ToString());
                    if (item != null)
                    {
                        Networks.SelectedItem.Selected = false;
                        item.Selected = true;
                    }
                    BindPlans(NetworkId);
                }                
            }

        }
        protected void gvPlans_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvPlans.PageIndex = e.NewPageIndex;
            BindPlans(NetworkId);
        }
        protected void gvPlans_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var planId = Convert.ToInt32(e.CommandArgument);

            switch (e.CommandName)
            {
                case "Edit":                    
                    NavigationController.EditPlan(planId);
                    break;
                case "Delete":                    
                    NavigationController.DeletePlan(planId);
                    break;
            }
        }
        protected int NetworkId
        {
            get
            {
                var networkId = int.Parse(Networks.SelectedValue);
                return networkId;
            }
        }

        private void BindPlans(int networkId)
        {
            var plans = PlanService.GetPlansByNetworkId(networkId);

            gvPlans.DataSource = plans;
            gvPlans.DataBind();

            if (plans.Count == 0) {
                EmptyMessage.SetInformation("No Plans where found matching your search parameters");
            }
            else {
                EmptyMessage.Clear();
            }
        }

        protected void AddPlan(object sender, EventArgs e)
        {
            NavigationController.NewPlan(NetworkId);
        }

        protected void Search(object sender, EventArgs e)
        {
            BindPlans(NetworkId);
        }
    }
}