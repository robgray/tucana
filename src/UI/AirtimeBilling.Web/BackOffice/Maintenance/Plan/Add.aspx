﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="AirtimeBilling.Web.AddPlan" %>
<%@ Register TagPrefix="sharp" Src="~/BackOffice/Maintenance/UserControls/PlanDetail.ascx" TagName="plandetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
    <h1>Add Plan</h1>        
    <div class="formcontent">
        <sharp:MessagePanel ID="UserMessage" MessageType="Success" runat="server" />
        <asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True"  /> 
        <div class="Form">
            <sharp:plandetail ID="NewPlanDetail" runat="server" Text="New Plan" />                 
        </div>
        <div class="form-actions">                    
            <asp:LinkButton CssClass="btn btn-success" ID="NewPlanButton" runat="server" onclick="AddNewPlan">
                <i class="icon-ok icon-white"></i> Save Plan
            </asp:LinkButton>
        </div>
    </div>
</asp:Content>
