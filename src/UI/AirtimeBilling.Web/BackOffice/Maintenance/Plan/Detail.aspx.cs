﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Web;

namespace AirtimeBilling.Web.I.Maintenance
{
    public partial class Plan : BackOfficeBasePage
    {
        private enum PlanTab
        {
            Details,
            Tariffs
        }

        protected int PlanId
        {
            get
            {
                if (ViewState["planid"] != null)
                {
                    return int.Parse(ViewState["planid"].ToString());
                }
                return 0;
            }

            set
            {
                ViewState["planid"] = value;
            }
        }
        protected int TabId
        {
            get
            {
                if (ViewState["tabid"] != null)
                {
                    return int.Parse(ViewState["tabid"].ToString());
                }
                return 0;
            }

            set
            {
                ViewState["tabid"] = value;
            }
        }
        
        protected int NetworkId
        {
            get
            {
                if (ViewState["NetworkId"] != null)
                {
                    return int.Parse(ViewState["NetworkId"].ToString());
                }
                return 0;
            }
            set
            {
                ViewState["NetworkId"] = value;
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);        
            UserMessage.Clear();

            if (!IsPostBack)
            {
                if (Request.QueryString["Id"] == null) {
                    NavigationController.PlanSearch();                    
                }

                PlanId = Convert.ToInt32(Request.QueryString["Id"].ToString());
                ManageDetails(null, null);                            
                
                // Because Web User Controls embedded in this control need to initialise before
                // we set an attributes on them, we need to perform the Load code
                // in the LoadComplete event.  This ensures all child controls have been created
                // and populated.     
                //Page.LoadComplete += new EventHandler(Page_LoadComplete);
            }

            
        }
     
        private void LoadPlan()
        {
            var plan = PlanService.GetPlan(PlanId);

            if (plan != null)
            {
                NetworkId = plan.NetworkId;
                var tariffs = PlanService.GetPlanTariffs(PlanId);
                var defaultTariff = tariffs.Where(t => t.Id.Value == plan.DefaultTariffId).SingleOrDefault();

                PlanLabel.Text = plan.Name;
                NetworkLabel.Text = NetworkService.GetNetwork(plan.NetworkId).Name;
                MaintainPlanDetail.NetworkId = plan.NetworkId;
                MaintainPlanDetail.Duration = plan.Duration;
                MaintainPlanDetail.Flagfall = plan.FlagFall;
                MaintainPlanDetail.FreeCallAmount = plan.FreeCallAmount;
                MaintainPlanDetail.Name = plan.Name;
                MaintainPlanDetail.PlanAmount = plan.PlanAmount;
                MaintainPlanDetail.UnitOfTime = plan.UnitOfTime;
                if (defaultTariff != null) MaintainPlanDetail.DefaultNetworkTariffId = defaultTariff.NetworkTariffId;

                BindPlanTariffs(tariffs);
            }
        }

        private void BindPlanTariffs(IList<PlanTariff> tariffs)
        {
            if (tariffs == null)
            {                
                tariffs = PlanService.GetPlanTariffs(PlanId);
            }

            gvTariffs.DataSource = tariffs;
            gvTariffs.DataBind();
        }

        private void BindPlanTariffs()
        {
            BindPlanTariffs(null);
        }

        protected void gvTariffs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTariffs.PageIndex = e.NewPageIndex;
            BindPlanTariffs();
        }

        protected void gvTariffs_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        protected void gvTariffs_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }
        protected void ManageDetails(object sender, EventArgs e)
        {
            LoadPlan();

            SetTab(PlanTab.Details);      
        }

        protected void ManageTariffs(object sender, EventArgs e)
        {
            SetTab(PlanTab.Tariffs);     
        }       

        private void SetTab(PlanTab tab)
        {
            DetailsTab.Visible = tab == PlanTab.Details;            
            TariffsTab.Visible = tab == PlanTab.Tariffs;            
        }

        protected void SavePlan(object sender, EventArgs e)
        {         
            var plan = new global::AirtimeBilling.Core.Entities.Plan(PlanId)
            {
                Name = MaintainPlanDetail.Name,
                Duration = MaintainPlanDetail.Duration,
                FlagFall = MaintainPlanDetail.Flagfall,
                FreeCallAmount = MaintainPlanDetail.FreeCallAmount,
                NetworkId = NetworkId,
                PlanAmount = MaintainPlanDetail.PlanAmount,
                UnitOfTime = MaintainPlanDetail.UnitOfTime
            };

            // Get the default tariff.
            var tariffs = PlanService.GetPlanTariffs(PlanId);
            var defaultTariff = tariffs.Where(t => t.NetworkTariffId == MaintainPlanDetail.DefaultNetworkTariffId).Single();
            plan.DefaultTariffId = defaultTariff.Id.Value;

            if (PlanService.UpdatePlan(plan))
            {
                UserMessage.SetSuccess("Successfully saved Plan");
            }
            else
            {                
                UserMessage.SetFailure("Failed to Update Plan");
            }
        }
       
        protected void gvTariffs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            UserMessage.Clear();
            gvTariffs.EditIndex = e.NewEditIndex;
            BindPlanTariffs();
        }

        protected void gvTariffs_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var code = gvTariffs.Rows[e.RowIndex].Cells[1].Controls[1] as Label;
            var name = gvTariffs.Rows[e.RowIndex].Cells[2].Controls[1] as Label;
            var unitcost = gvTariffs.Rows[e.RowIndex].Cells[3].Controls[1] as TextBox;
            var hasflagfall = gvTariffs.Rows[e.RowIndex].Cells[4].Controls[1] as CheckBox;
            var infreecall = gvTariffs.Rows[e.RowIndex].Cells[5].Controls[1] as CheckBox;
            var networktariffid = gvTariffs.Rows[e.RowIndex].Cells[6].Controls[1] as Label;
            
            var entity = new PlanTariff((int)gvTariffs.DataKeys[e.RowIndex].Value, int.Parse(networktariffid.Text))
            {
                Code = code.Text,
                Name = name.Text,
                UnitCost = decimal.Parse(unitcost.Text, NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowDecimalPoint, CultureInfo.CurrentCulture),
                HasFlagfall = hasflagfall.Checked,
                IsCountedInFreeCall = infreecall.Checked
            };

            if (PlanService.UpdatePlanTariff(entity))
            {
                UserMessage.SetSuccess("Successfully saved Tariff");
            }
            gvTariffs.EditIndex = -1;
            BindPlanTariffs();
        }
        protected void gvTariffs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvTariffs.EditIndex = -1;
            BindPlanTariffs();
        }

        protected void SearchPlans(object sender, EventArgs e)
        {
            NavigationController.PlanSearch();
        }
    }
}
