﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Web.I.Maintenance
{
    public partial class ContactSearchPage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            EmptyMessage.Clear();
        
            if (!IsPostBack) {
                SearchType.DataSource = Enum<ContactSearch>.GetAllDescriptions();
                SearchType.DataBind();
            }
        }

        protected void SearchResultsGrid_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            if (args.HasKey)
                NavigationController.EditContact(args.Key);
        }

        protected void SearchResultsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            
        }

        protected void SearchForContact(object sender,EventArgs e)
        {
            SearchResultsGrid.DataSource = null;

            var searchType = Enum<ContactSearch>.GetEnumFromDescription(SearchType.SelectedValue);
            var contacts = ContactService.FindContacts(searchType, SearchValue.Text);

            if (contacts.Count > 0) {
                SearchResultsGrid.DataSource = contacts;                
            }
            else {
                EmptyMessage.SetInformation("No contacts were found matching your search parameters");
            }
            SearchResultsGrid.DataBind();
        }

        protected void NewContact(object sender, EventArgs e)
        {
            NavigationController.NewContact();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            AddButton.Visible = ConfigItems.AllowNewAssociatedEntities;
        }
    }
}
