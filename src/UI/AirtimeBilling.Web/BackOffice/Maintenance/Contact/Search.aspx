﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="AirtimeBilling.Web.I.Maintenance.ContactSearchPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Contact Maintenance</h1>
<sharp:MessagePanel ID="UserMessage" runat="server" />
<div class="well form-inline">
    <table>
        <tbody><tr><td>
            <label class="control-label" for="<%= SearchType.ClientID %>">Search By:</label>
            <asp:DropDownList ID="SearchType" runat="server" Width="150px">                              
            </asp:DropDownList>                
            <asp:TextBox ID="SearchValue" runat="server" Width="350px"></asp:TextBox>                                                                            
            <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" Icon="search" Text="Search" runat="server" CausesValidation="false" OnClick="SearchForContact">
                <i class="icon-search icon-white"></i> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" ID="AddButton" Icon="add" Text="New Contact" runat="server" CausesValidation="false" OnClick="NewContact" />
        </td></tr></tbody>                    
    </table>                
</div>  
<asp:Panel ID="SearchResultsPanel" runat="server">    
    <sharp:ClickableGridView ID="SearchResultsGrid" runat="server"                   
        AutoGenerateColumns="False" 
        DataKeyNames="Id"
        onpageindexchanging="SearchResultsGrid_PageIndexChanging" 
        onrowclicked="SearchResultsGrid_RowClicked">    
        <Columns>                
            <asp:BoundField DataField="Name" HeaderText="Name" />        
            <asp:BoundField DataField="HomeAddress" HeaderText="Address" />         
        </Columns>            
    </sharp:ClickableGridView>    
    <sharp:MessagePanel runat="server" ID="EmptyMessage" />
</asp:Panel>          
</asp:Content>
