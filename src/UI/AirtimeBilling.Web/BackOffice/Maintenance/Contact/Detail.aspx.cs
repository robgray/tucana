﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class ContactMaintenancePage : BackOfficeBasePage
    {
        private enum ContactTab
        {
            Details,
            ActivityLog
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            UserMessage.Clear();

            
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);

            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Id"]))
                {
                    var id = int.Parse(Request.QueryString["Id"]);

                    var contact = ContactService.GetContact(id);
                    PopulateContact(contact);

                    SetTab(ContactTab.Details);
                }
            }
        }
    
        protected void lnkSave_Click(object sender, EventArgs e)
        {
            var request = new SaveContactRequest
            {
                Contact = Contact.Contact,
                User = Users.Current
            };

            var response = ContactService.SaveContact(request);
            if (response.IsSuccessful)
            {
                UserMessage.SetSuccess("Contact Saved");
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }

        protected void DetailsView(object sender, EventArgs e)
        {
            SetTab(ContactTab.Details);
        }

        protected void ActivityLogViewer(object sender, EventArgs e)
        {
            if (Contact.ContactId > 0)
            {             
                var contact = ContactService.GetContact(Contact.Contact.Id.Value);

                lblContactName.Text = contact.Name;
                LogViewer.Entity = contact;

                SetTab(ContactTab.ActivityLog);
            }
        }

        private void SetTab(ContactTab tab)
        {
            pnlActivityLog.Visible = tab == ContactTab.ActivityLog;
            pnlDetailsView.Visible = tab == ContactTab.Details; 
        }

        protected void PopulateContact(Contact contact)
        {            
            Contact.Contact = contact;
        }

        protected void GotoSearch(object sender, EventArgs e)
        {
            NavigationController.ContactSearch();
        }
    }
}