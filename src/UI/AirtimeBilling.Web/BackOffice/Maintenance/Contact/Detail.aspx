﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ContactMaintenancePage" Codebehind="Detail.aspx.cs" %>
<%@ Register Src="~/UserControls/ActivityLogViewer.ascx" TagName="activitylog" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Contact.ascx" TagName="contact" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
    <h1>Contact Maintenance</h1>    
    <hr/>
    <asp:Panel ID="pnlUserActions" runat="server" CssClass="well">            
        <asp:LinkButton CssClass="btn" ID="lnkDetailsView" runat="server" Text="Details"  onclick="DetailsView" CausesValidation="false" />
        <asp:LinkButton CssClass="btn" ID="lnkActivityLog" runat="server" Text="Activity Log"  onclick="ActivityLogViewer" CausesValidation="false" />
        <asp:LinkButton CssClass="btn" ID="lnkSearch" runat="server" Text="Search" Icon="search" onclick="GotoSearch" CausesValidation="false">
            <i class="icon-search"></i> Search
        </asp:LinkButton>
    </asp:Panel>
    <sharp:MessagePanel ID="UserMessage" runat="server" />
    <asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" runat="server" ShowSummary="True"  />

    <asp:Panel ID="pnlDetailsView" runat="server">        
        <sharp:contact ID="Contact" runat="server" />
                    
            <div class="form-actions">                    
            <asp:LinkButton CssClass="btn btn-success" ID="lnkSave" runat="server" Text="Save" Icon="disk"  onclick="lnkSave_Click">
                <i class="icon-ok icon-white"></i> Save
            </asp:LinkButton> 
        </div>        
    </asp:Panel>    
    <asp:Panel ID="pnlActivityLog" runat="server" Visible="false">        
        <div class="form-inline">
            <label><b>Contact Name</b></label>            
            <asp:Label ID="lblContactName" runat="server" Text=""></asp:Label>                                        
        </div>                        
        
        <sharp:activitylog ID="LogViewer" runat="server" />           
    </asp:Panel>

</asp:Content>

