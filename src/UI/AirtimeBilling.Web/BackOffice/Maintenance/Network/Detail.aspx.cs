﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class NetworksMaintenancePage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        
            UserMessage.Clear();

            if (!IsPostBack)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Id"]))
                {
                    var id = int.Parse(Request.QueryString["Id"]);
                    UpdateNetworkControls(id);
                } else {
                    pnlNetworkDetail.Visible = true;
                    BindAirtimeProviders();
                }
                Page.LoadComplete += new EventHandler(Page_LoadComplete);                
            }            
        }

        void Page_LoadComplete(object sender, EventArgs e)
        {

        }

        protected void AddNetwork(object sender, EventArgs e)
        {
            pnlNetworkDetail.Visible = true;        
        }
                
        private void UpdateNetworkControls(int networkId)
        {
            var entity = NetworkService.GetNetwork(networkId);
            if (entity != null)
            {
                Clear();
                                                                          
                NetworkId = networkId;
                Name = entity.Name;
                AcceptsNewContracts = entity.AcceptsNewContracts;
                AirtimeProviderId = entity.AirtimeProviderId;
                
                BindTariffs(networkId);

                pnlNetworkDetail.Visible = true;
            }
            else {
                UserMessage.SetFailure("Could not find the Network to edit");
            }
        }
     private void BindTariffs(int networkId)
        {
            var tariffs = NetworkService.GetNetworkTariffs(networkId);

            gvTariffs.DataSource = tariffs;
            gvTariffs.DataBind();
            
            EmptyTariffsHolder.Visible = tariffs.Count == 0;

        }
       
        protected void cmdCancel_Click(object sender, EventArgs e)
        {
            Clear();
            pnlNetworkDetail.Visible = false;                  
        }

        protected void SaveNetwork(object sender, EventArgs e)
        {            
            ResponseBase response;
            bool isNewNetwork = NetworkId == 0;
            if (!isNewNetwork)
            {
                var entity = new Network(NetworkId)
                                 {
                                     Name = Name,
                                     AcceptsNewContracts = AcceptsNewContracts,
                                     AirtimeProviderId = AirtimeProviderId 
                                 };
                var updateRequest = new UpdateNetworkRequest
                {
                    Network = entity,
                    User = Users.Current
                };
                response = NetworkService.UpdateNetwork(updateRequest);
            }
            else
            {
                var entity = new Network
                                 {
                                    Name = Name,
                                    AcceptsNewContracts = AcceptsNewContracts,
                                    AirtimeProviderId = AirtimeProviderId
                                 };
                var insertRequest = new InsertNetworkRequest
                                    {
                                        Network = entity,
                                        User = Users.Current
                                    };
                response = NetworkService.InsertNetwork(insertRequest);
            }


            if (response.IsSuccessful)
            {
                if (isNewNetwork) {
                    NetworkId = ((InsertNetworkResponse)response).NetworkId;                               
                }

                if (EmptyTariffsHolder.Visible) {
                    InsertNetworkTariff(txtNewCode.Text, txtNewTariff.Text, chkNewFreeCall.Checked,chkNewHasFlagfall.Checked, NetworkId);
                }
                
                BindTariffs(NetworkId);

                UserMessage.SetSuccess("Network " + (isNewNetwork ? "Added" : "Updated"));                                                                            
            }
            else
            {
                UserMessage.SetFailure(response.Message);                
            }
        }
                       
        protected void gvTariffs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }
        protected void gvTariffs_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {

        }
        protected void gvTariffs_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Insert":
                    int networkId = int.Parse(e.CommandArgument.ToString());
                    var txtCode = (TextBox)gvTariffs.FooterRow.FindControl("txtNewTariffCode");
                    var txtTariff = (TextBox)gvTariffs.FooterRow.FindControl("txtNewTariff");
                    var chkFlagfall= (CheckBox)gvTariffs.FooterRow.FindControl("chkNewHasFlagfall");
                    var chkFreeCall = (CheckBox)gvTariffs.FooterRow.FindControl("chkNewFreeCall");

                    InsertNetworkTariff(txtCode.Text, txtTariff.Text, chkFreeCall.Checked, chkFlagfall.Checked, networkId);
                    break;

                case "Cancel":
                    gvTariffs.EditIndex = -1;
                    BindTariffs(NetworkId);
                    break;
            }
        }
        private void InsertNetworkTariff(string code, string tariff, bool freeCall, bool hasFlagfall, int networkId)
        {
            var newTariff = new NetworkTariff
            {
                Code = code,
                Name = tariff,
                IsCountedInFreeCall = freeCall,
                HasFlagfall = hasFlagfall,
                NetworkId = networkId
            };

            var request = new InsertNetworkTariffRequest
            {
                Tariff = newTariff,
                User = Users.Current
            };
            
            var response = NetworkService.InsertNetworkTariff(request);
            if (!response.IsSuccessful)            
            {                
                UserMessage.SetFailure(response.Message);                
            }
            else {
                UserMessage.SetSuccess("Tariff added.");                
            }

            BindTariffs(networkId);
        }

        protected void gvTariffs_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvTariffs_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvTariffs.EditIndex = e.NewEditIndex;            
            BindTariffs(NetworkId);
        }
        protected void gvTariffs_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            var code = gvTariffs.Rows[e.RowIndex].Cells[1].Controls[1] as TextBox;
            var name = gvTariffs.Rows[e.RowIndex].Cells[2].Controls[1] as TextBox;
            var hasflagfall = gvTariffs.Rows[e.RowIndex].Cells[3].Controls[1] as CheckBox;
            var infreecall = gvTariffs.Rows[e.RowIndex].Cells[4].Controls[1] as CheckBox;
            

            // (Changed rag 15/02/2009) #001-10
            //Label networkid = gvTariffs.Rows[e.RowIndex].Cells[4].Controls[1] as Label;            
            var networkTariffId = (int)gvTariffs.DataKeys[e.RowIndex].Value;
            
            var entity = new NetworkTariff(networkTariffId)
                                {
                                    Code = code.Text,
                                    Name = name.Text,
                                    IsCountedInFreeCall = infreecall.Checked,
                                    HasFlagfall = hasflagfall.Checked,
                                    NetworkId = NetworkId
                                };

            var request = new UpdateNetworkTariffRequest()
            {
                Tariff = entity,
                User = Users.Current
            };

            var response = NetworkService.UpdateNetworkTariff(request);
            if (response.IsSuccessful)
            {                
                gvTariffs.EditIndex = -1;
                BindTariffs(NetworkId);
                UserMessage.SetSuccess("Tariff updated.");                
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }
        }
        protected void gvTariffs_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (this.Request["x"] != null && this.Request["y"] != null)
            {
                gvTariffs.EditIndex = -1;
                BindTariffs(NetworkId);
            }
            else
            {

                int networkTariffId = (int)gvTariffs.DataKeys[e.RowIndex].Value;
                
                var request = new DeleteNetworkTariffRequest()
                {
                    TariffId = networkTariffId,
                    User = Users.Current
                };

                var response = NetworkService.DeleteNetworkTariff(request);
                if (response.IsSuccessful)
                {                    
                    gvTariffs.EditIndex = -1;
                    BindTariffs(NetworkId);

                    UserMessage.SetSuccess("Tariff deleted.");
                }
                else
                {
                    UserMessage.SetFailure(response.Message);                                    
                }
            }
        }
        protected void dvNetworkTariff_ItemCommand(object sender, DetailsViewCommandEventArgs e)
        {
            switch (e.CommandName)
            {
                case "Insert":
                    var dvTariff = (DetailsView)sender;                    

                    var txtCode = (TextBox)dvTariff.FindControl("txtNewCode");
                    var txtTariff = (TextBox) dvTariff.FindControl("txtNewTariff");
                    var chkHasFlagfall = (CheckBox)dvTariff.FindControl("chkNewHasFlagfall");
                    var chkFreeCall = (CheckBox)dvTariff.FindControl("chkNewFreeCall");

                    var code = txtCode.Text;
                    var tariff = txtTariff.Text;
                    var freeCall = chkFreeCall.Checked;
                    var hasFlagfall = chkHasFlagfall.Checked;
                    var networkId = int.Parse(e.CommandArgument.ToString());

                    InsertNetworkTariff(code, tariff, freeCall, hasFlagfall, networkId);
                    break;
            }
        }

        protected void dvNetworkTariff_ItemInserting(object sender, DetailsViewInsertEventArgs e)
        {        	
        }

        protected void gvNetworks_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            var hidden = args.Row.Cells[0].FindControl("id") as HiddenField;
            if (hidden != null) {
                int networkId = int.Parse(hidden.Value);
                NavigationController.EditNetwork(networkId);
            }
        }
        public int NetworkId
        {
            get
            {
                if (ViewState["NetworkId"] != null)
                {
                    return int.Parse(ViewState["NetworkId"].ToString());
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["NetworkId"] = value;
            }
        }
        public string Name
        {
            get { return txtNetwork.Text; }
            set { txtNetwork.Text = value; }
        }

        public bool AcceptsNewContracts
        {
            get { return chkForNewContracts.Checked; }
            set { chkForNewContracts.Checked = value; }
        }

        public int AirtimeProviderId
        {
            get { return int.Parse(ddlAirtimeProvider.SelectedValue); }
            set
            {
                ListItem item = ddlAirtimeProvider.Items.FindByValue(value.ToString());
                if (item != null)
                {
                    ddlAirtimeProvider.SelectedItem.Selected = false;
                    item.Selected = true;
                }
            }
        }
        private void BindAirtimeProviders()
        {
            IList<AirtimeProvider> providers = AirtimeProviderService.GetAirtimeProviders();

            ddlAirtimeProvider.DataSource = providers;
            ddlAirtimeProvider.DataTextField = "Name";
            ddlAirtimeProvider.DataValueField = "Id";
            ddlAirtimeProvider.DataBind();
        }

        public void Clear()
        {
            ViewState["NetworkId"] = null;
            this.Name = "";
            chkForNewContracts.Checked = false;
            BindAirtimeProviders();
        }

        protected void GotoSearch(object sender, EventArgs e)
        {
            NavigationController.NetworkSearch();
        }

        protected void SaveNewTariff(object sender, ImageClickEventArgs e)
        {
            var code = txtNewCode.Text;
            var tariff = txtNewTariff.Text;
            var freeCall = chkNewFreeCall.Checked;
            var hasFlagfall = chkNewHasFlagfall.Checked;
            
            InsertNetworkTariff(code, tariff, freeCall, hasFlagfall, NetworkId);
        }
    }
}