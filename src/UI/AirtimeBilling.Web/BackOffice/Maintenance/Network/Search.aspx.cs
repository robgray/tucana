﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web.I.Maintenance
{
    public partial class NetworkSearch : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            EmptyMessage.Clear();

            if (!IsPostBack) {
                BindNetworks();
            }
        }

        private void BindNetworks()
        {
            var networks = NetworkService.GetAllNetworks();

            SearchResultsGrid.DataSource = networks;
            SearchResultsGrid.DataBind();
        }

        protected void SearchResultsGrid_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            if (args.HasKey)
                NavigationController.EditNetwork(args.Key);
        }

        protected void SearchResultsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void SearchForNetwork(object sender, EventArgs e)
        {
            var networks = NetworkService.FindByNetworkName(SearchValue.Text);

            SearchResultsGrid.DataSource = networks;
            SearchResultsGrid.DataBind();

            if (networks.Count == 0)
                EmptyMessage.SetInformation("No networks were found matching your search parameters");
            
        }

        protected void NewNetwork(object sender, EventArgs e)
        {
            NavigationController.NewNetwork();
        }
    }
}
