<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.NetworksMaintenancePage" Codebehind="Detail.aspx.cs" %>
<%@ Register src="UserControls/NetworkDetail.ascx" tagname="NetworkDetail" tagprefix="sharp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">    
<h1>Network Maintenance</h1>    
<div class="well">
    <asp:LinkButton CssClass="btn" ID="lnkSearch" runat="server" Text="Search" Icon="search" onclick="GotoSearch" CausesValidation="false">
        <i class="icon-search"></i> Search
    </asp:LinkButton>
</div>
<div class="formcontent">
    <sharp:MessagePanel ID="UserMessage" runat="server" />    
    <asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True" ValidationGroup="Edit" />
    <asp:ValidationSummary ID="valNewSummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True" ValidationGroup="New"  />
    <asp:ValidationSummary ID="valEmptySummary" CssClass="ValidationSummary" HeaderText="Submission Errors" runat="server" ShowSummary="True" ValidationGroup="Empty"  />
    
    <asp:Panel ID="pnlNetworkDetail" runat="server" Visible="false">
                                
        <sharp:FormGroup ID="NetworkFG" runat="server" CssClass="well">                
            <div class="control-group">
                <label class="control-label" for="<%= txtNetwork.ClientID %>">Network</label>
                <div class="controls">
                    <asp:TextBox ID="txtNetwork" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNetwork" runat="server" 
                        ErrorMessage="Specify a Network Name" Display="None" SetFocusOnError="True"
                        ControlToValidate="txtNetwork"></asp:RequiredFieldValidator>                                            
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= chkForNewContracts.ClientID %>">For New Contracts</label>
                <div class="controls">
                    <asp:CheckBox ID="chkForNewContracts" runat="server"></asp:CheckBox>                    
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="<%= ddlAirtimeProvider.ClientID %>">Airtime Provider</label>
                <div class="controls">
                    <asp:DropDownList ID="ddlAirtimeProvider" runat="server"></asp:DropDownList>
                </div>
            </div>
        </sharp:FormGroup>
        
        <asp:Panel ID="pnlTariffs" runat="server">
            <asp:GridView ID="gvTariffs" runat="server"            
                OnRowDataBound="gvTariffs_RowDataBound" 
                OnRowCommand="gvTariffs_RowCommand" 
                AutoGenerateColumns="False"                                  
                onrowediting="gvTariffs_RowEditing" onrowcancelingedit="gvTariffs_RowCancelingEdit" onrowupdating="gvTariffs_RowUpdating" DataKeyNames="Id" ShowFooter="true" onrowdeleting="gvTariffs_RowDeleting">                          
                <Columns>
                    <asp:CommandField ButtonType="Image" CancelImageUrl="~/images/cross.gif" 
                        EditImageUrl="~/images/pencil.gif" ShowEditButton="True" 
                        DeleteImageUrl="~/images/cross.gif" DeleteText = "Delete"
                        UpdateImageUrl="~/images/disk.gif" UpdateText="Save" 
                        ShowDeleteButton="True" HeaderStyle-Width="40px" ItemStyle-Width="40px" 
                        ValidationGroup="Edit" />                                 
                    <asp:TemplateField HeaderText="Code">                                                    
                        <ItemStyle Width="160px" />
                        <EditItemTemplate>                                                
                            <asp:TextBox ID="txtTariffCode" runat="server" Text='<%# Bind("Code") %>' ValidationGroup="Edit"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTariffCode" runat="server" 
                                ErrorMessage="A Tariff Code must be supplied" Text="*" Display="None"
                                ControlToValidate="txtTariffCode" ValidationGroup="Edit" />
                        </EditItemTemplate> 
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewTariffCode" runat="server" Text="" ValidationGroup="New"></asp:TextBox>        
                            <asp:RequiredFieldValidator ID="rfvTariffCode" runat="server" 
                                ErrorMessage="A Tariff Code must be supplied" Text="*" Display="None"
                                ControlToValidate="txtNewTariffCode" ValidationGroup="New" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Code") %>'></asp:Label>
                        </ItemTemplate>                    
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tariff" SortExpression="Tariff">                                        
                        <ItemStyle Width="170px" />
                        <EditItemTemplate>                        
                            <asp:TextBox ID="txtTariffName" runat="server" Text='<%# Bind("Name") %>' ValidationGroup="Edit"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rfvTariffName" runat="server" 
                                ErrorMessage="A Tariff Name must be supplied" Text=" *" Display="None"
                                ControlToValidate="txtTariffName" ValidationGroup="Edit" />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtNewTariff" runat="server" Text="" ValidationGroup="New"></asp:TextBox>         
                            <asp:RequiredFieldValidator ID="rfvTariffName" runat="server" 
                                ErrorMessage="A Tariff Name must be supplied" Text=" *" Display="None"
                                ControlToValidate="txtNewTariff" ValidationGroup="New" />               
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Has Flagfall">                    
                        <HeaderStyle Width="50px" HorizontalAlign="Center" />
                        <ItemStyle Width="50px" HorizontalAlign="Center" />
                        <FooterStyle HorizontalAlign="Center" />
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkHasFlagfall" runat="server" 
                                Checked='<%# Bind("HasFlagfall") %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkNewHasFlagfall" runat="server" 
                                Checked="false" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1Flagfall" runat="server" 
                                Checked='<%# Bind("HasFlagfall") %>' Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Include in Free Call">                    
                        <HeaderStyle Width="65px" HorizontalAlign="Center"/>
                        <ItemStyle Width="65px" HorizontalAlign="Center"/>
                        <FooterStyle HorizontalAlign="Center" />
                        <EditItemTemplate>
                            <asp:CheckBox ID="chkFreeCall" runat="server" 
                                Checked='<%# Bind("IsCountedInFreeCall") %>' />
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:CheckBox ID="chkNewFreeCall" runat="server" 
                                Checked="false" />
                        </FooterTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" 
                                Checked='<%# Bind("IsCountedInFreeCall") %>' Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <EditItemTemplate>
                        </EditItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton runat="server" id="imgSaveNetwork" CssClass="icon" ImageUrl="~/Images/disk.gif"
                             CausesValidation="true" CommandName="Insert" CommandArgument='<%# NetworkId %>' ValidationGroup="New" />                        
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="NetworkId" 
                        SortExpression="NetworkId" Visible="False">
                        <EditItemTemplate>                            
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("NetworkId") %>'></asp:Label>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("NetworkId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>               
            </asp:GridView>                 
            <asp:PlaceHolder ID="EmptyTariffsHolder" runat="server">
                <table class="Grid" cellspacing="0" cellpadding="2" border="0" style="border-collapse:collapse;">
                    <tbody>
                        <tr>
                            <th scope="col" style="width:40px;"> </th>
                            <th scope="col">Code</th>
                            <th scope="col">Tariff</th>
                            <th align="center" scope="col" style="width:50px">Has Flagfall</th>
                            <th align="center" scope="col" style="width:65px">Include in Free Call</th>
                            <th scope="col"> </th>
                        </tr>
                        <tr class="Footer">
                            <td style="width:40px;"> </td>
                            <td style="width:160px;">
                                <asp:TextBox ID="txtNewCode" runat="server" ValidationGroup="Empty"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvTariffCode" runat="server" 
                                    ErrorMessage="A Tariff Code must be supplied" Text="*" Display="None"
                                    ControlToValidate="txtNewCode" ValidationGroup="Empty" />
                            </td>
                            <td style="width:170px;">
                                <asp:TextBox ID="txtNewTariff" runat="server" Width="190px" ValidationGroup="Empty"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvTariffName" runat="server" 
                                    ErrorMessage="A Tariff Name must be supplied" Text=" *" Display="None"
                                    ControlToValidate="txtNewTariff" ValidationGroup="Empty" /> 
                            </td>
                            <td align="center" style="width:50px;"><asp:CheckBox ID="chkNewFreeCall" runat="server" /></td>
                            <td align="center" style="width:65px;"><asp:CheckBox ID="chkNewHasFlagfall" runat="server" /></td>
                            <td><asp:ImageButton runat="server" id="SaveNewTariffButton" CssClass="icon" ImageUrl="~/Images/disk.gif" OnClick="SaveNewTariff" ValidationGroup="Empty" /></td>                            
                        </tr>
                    </tbody>
                </table>                                                                    
            </asp:PlaceHolder>               
            <div class="form-actions">                    
                <asp:LinkButton CssClass="btn btn-success" ID="lnkSave" runat="server" Text="Save" Icon="disk" onclick="SaveNetwork">
                    <i class="icon-ok icon-white"></i> Save Network
                </asp:LinkButton>
            </div>
        </asp:Panel>
    </asp:Panel>
</div>
</asp:Content>

