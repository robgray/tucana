﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class NetworkDetailControl : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindAirtimeProviders();
            }
        }
        public string Text
        {
            get { return NetworkDetailFG.GroupingText; }
            set { NetworkDetailFG.GroupingText = value; }
        }

        public int NetworkId
        {
            get
            {
                if (ViewState["NetworkId"] != null)
                {
                    return int.Parse(ViewState["NetworkId"].ToString());
                }
                else
                {
                    return 0;
                }
            }
            set
            {
                ViewState["NetworkId"] = value;
            }
        }
        public string Name
        {
            get { return txtNetwork.Text; }
            set { txtNetwork.Text = value; }
        }

        public bool AcceptsNewContracts
        {
            get  { return chkForNewContracts.Checked; }
            set { chkForNewContracts.Checked = value; }
        }

        public int AirtimeProviderId
        {
            get { return int.Parse(ddlAirtimeProvider.SelectedValue); }
            set
            {
                ListItem item = ddlAirtimeProvider.Items.FindByValue(value.ToString());
                if (item != null)
                {
                    ddlAirtimeProvider.SelectedItem.Selected = false;
                    item.Selected = true;
                }
            }
        }
        private void BindAirtimeProviders()
        {            
            IList<AirtimeProvider> providers = AirtimeProviderService.GetAirtimeProviders();

            ddlAirtimeProvider.DataSource = providers;
            ddlAirtimeProvider.DataTextField = "Name";
            ddlAirtimeProvider.DataValueField = "Id";
            ddlAirtimeProvider.DataBind();
        }

        public void Clear()
        {
            ViewState["NetworkId"] = null;
            this.Name = "";
            chkForNewContracts.Checked = false;
            BindAirtimeProviders();
        }
    }
}