﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="AirtimeBilling.Web.NetworkDetailControl" Codebehind="NetworkDetail.ascx.cs" %>
<sharp:FormGroup ID="NetworkDetailFG" runat="server" >    
    <div class="control-group">
        <label class="control-label" for="<%= txtNetwork.ClientID %>">Network</label>
        <div class="controls">
            <asp:TextBox ID="txtNetwork" runat="server" CssClass="EntryBox"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvNetwork" runat="server" 
                ErrorMessage="Specify a Network Name" Display="None" 
                ControlToValidate="txtNetwork"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= chkForNewContracts.ClientID %>">For New Contracts
            <asp:CheckBox ID="chkForNewContracts" runat="server"></asp:CheckBox>
        </label>        
    </div>
    <div class="control-group">
        <label class="control-label" for="<%= ddlAirtimeProvider.ClientID %>">Airtime Provider</label>
        <div class="controls">
            <asp:DropDownList ID="ddlAirtimeProvider" runat="server"></asp:DropDownList>
        </div>
    </div>
</sharp:FormGroup>
