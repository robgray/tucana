﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="AirtimeBilling.Web.I.Maintenance.NetworkSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Network Maintenance</h1>
<div class="well form-inline">
    <table>
        <tbody><tr><td>
            <label>Search By:</label>
            <asp:DropDownList ID="SearchType" runat="server">     
                <asp:ListItem>Network Name</asp:ListItem>                   
            </asp:DropDownList>                
            <asp:TextBox ID="SearchValue" runat="server"></asp:TextBox>                                
            <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" Icon="search" Text="Search" runat="server" CausesValidation="false" OnClick="SearchForNetwork">
                <i class="icon-search icon-white"></i> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" ID="AddButton" Icon="add" Text="New Network" runat="server" CausesValidation="false" OnClick="NewNetwork" />
        </td></tr></tbody>                    
    </table>                
</div> 
<asp:Panel ID="SearchResultsPanel" runat="server">    
    <sharp:ClickableGridView ID="SearchResultsGrid" runat="server"                   
        AutoGenerateColumns="False" 
        DataKeyNames="Id"
        onpageindexchanging="SearchResultsGrid_PageIndexChanging" 
        onrowclicked="SearchResultsGrid_RowClicked">    
        <Columns>            
            <asp:BoundField DataField="Name" HeaderText="Name">
                <HeaderStyle Width="200px" />
                <ItemStyle Width="200px" />
            </asp:BoundField>
            <asp:TemplateField HeaderText="Allow New?">
                    <HeaderStyle Width="30px" />
                    <ItemStyle Width="30px" />
                    <ItemTemplate>
                        <asp:CheckBox ID="CheckBox123" runat="server" Checked='<%# Bind("AcceptsNewContracts") %>' Enabled="false"></asp:CheckBox>
                    </ItemTemplate>                    
                </asp:TemplateField>            
            <asp:BoundField DataField="AirtimeProvider" HeaderText="Airtime Provider" >
                <HeaderStyle Width="200px" />            
                <ItemStyle Width="200px" />
            </asp:BoundField>
        </Columns>        
    </sharp:ClickableGridView>    
    <sharp:MessagePanel runat="server" ID="EmptyMessage" MessageType="Information" Message="No Networks were found matching your search parameters" />
</asp:Panel>
</asp:Content>
