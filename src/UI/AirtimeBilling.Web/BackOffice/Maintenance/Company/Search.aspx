﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="AirtimeBilling.Web.I.Maintenance.CompanySearchPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<h1>Company Maintenance</h1>
<div class="well form-inline">
    <table>
        <tbody><tr><td>
            <label>Search By:</label>
            <asp:DropDownList ID="SearchType" runat="server">                        
            </asp:DropDownList>                
            <asp:TextBox ID="SearchValue" runat="server" Width="350px"></asp:TextBox>                                
            <asp:LinkButton CssClass="btn btn-primary" ID="SearchButton" Icon="search" Text="Search" runat="server" CausesValidation="false" OnClick="SearchForCompany">
                <i class="icon-search icon-white"></i> Search
            </asp:LinkButton>
            <asp:LinkButton CssClass="btn btn-primary" ID="AddButton" Icon="add" Text="New Company" runat="server" CausesValidation="false" OnClick="NewCompany" />
        </td></tr></tbody>                    
    </table>                
</div>     
<asp:Panel ID="SearchResultsPanel" runat="server">    
    <sharp:ClickableGridView ID="SearchResultsGrid" runat="server"                   
        AutoGenerateColumns="False" 
        DataKeyNames="Id"
        onpageindexchanging="SearchResultsGrid_PageIndexChanging" 
        onrowclicked="SearchResultsGrid_RowClicked">    
        <Columns>                
            <asp:BoundField DataField="CompanyName" HeaderText="Company" />        
            <asp:BoundField DataField="Address" HeaderText="Address" />         
        </Columns>            
    </sharp:ClickableGridView>    
    <sharp:MessagePanel runat="server" ID="EmptyMessage" IncludeClose="False" />
</asp:Panel>
</asp:Content>
