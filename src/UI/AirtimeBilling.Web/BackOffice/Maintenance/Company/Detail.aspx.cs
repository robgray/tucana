﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;

namespace AirtimeBilling.Web
{
    public partial class CompanyMaintenancePage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);        
            UserMessage.Clear();
            
            if (!IsPostBack) {
                if (!string.IsNullOrEmpty(Request.QueryString["Id"])) {
                    var companyId = int.Parse(Request.QueryString["Id"]);
                    CompanyId = companyId;
                    var company = CompanyService.GetCompany(companyId);
                    PopulateView(company);
                    SetTab(CompanyTab.Details);
                }

                PostalAddress.CopyFromPanel(OfficeAddress, "Same as " + OfficeAddress.Text);                
            }
        }
  
        private int CompanyId
        {
            get
            {                
                return (int)(ViewState["companyid"]);           
            }
            set
            {
                ViewState["companyid"] = value;
            }
        }

        private void PopulateView(Company company)
        {            
            if (company == null) return;

            CompanyInfo1.SetCompany(company);
            CompanyInfo2.SetCompany(company);

            OfficeAddress.SetAddress(company.Address);
            PostalAddress.SetAddress(company.PostalAddress);               
        }

        protected void lnkSave_Click(object sender, EventArgs e)
        {
            Company company;
            if (CompanyId > 0)
            {
                company = new Company(CompanyId);
            }
            else
            {
                company = new Company();
            }

            company.CompanyName = CompanyInfo1.CompanyName;
            company.ABN = CompanyInfo1.ABN;
            company.ACN = CompanyInfo1.ACN;
            company.Address = new Address
                                  {
                                      Street = OfficeAddress.Street,
                                      Street2 = OfficeAddress.Street2,
                                      Suburb = OfficeAddress.Suburb,
                                      State = OfficeAddress.State,
                                      Postcode = OfficeAddress.Postcode
                                  };
            company.PostalAddress = new Address
                                        {
                                            Street = PostalAddress.Street,
                                            Street2 = PostalAddress.Street2,
                                            Suburb = PostalAddress.Suburb,
                                            State = PostalAddress.State,
                                            Postcode = PostalAddress.Postcode
                                        };

            var request = new SaveCompanyRequest
                            {
                                Company = company,
                                User = Users.Current
                            };
            
            var response = CompanyService.SaveCompany(request);
            if (response.IsSuccessful)
            {
                CompanyId = company.Id.Value;
                UserMessage.SetSuccess("Company Saved");               
            }
            else
            {
                UserMessage.SetFailure(response.Message);
            }

        }

        protected void ActivityLogViewer(object sender, EventArgs e)
        {
            // Go to company             
            var company = CompanyService.GetCompany(CompanyId);
            if (company != null)
            {
                SetTab(CompanyTab.ActivtyLog);

                CompanyInfo2.CompanyName = company.CompanyName;
                CompanyInfo2.ABN = company.ABN;
                CompanyInfo2.ACN = company.ACN;
                LogViewer.Entity = company;
            }
        }
        protected void DetailsView(object sender, EventArgs e)
        {
            SetTab(CompanyTab.Details);
        }

        protected void GotoSearch(object sender, EventArgs e)
        {
            NavigationController.CompanySearch();
        }

        private enum CompanyTab
        {
            Details,
            ActivtyLog
        }

        private void SetTab(CompanyTab tab)
        {
            pnlDetails.Visible = tab == CompanyTab.Details;
            pnlActivityLog.Visible = tab == CompanyTab.ActivtyLog;            
        }
    }
}