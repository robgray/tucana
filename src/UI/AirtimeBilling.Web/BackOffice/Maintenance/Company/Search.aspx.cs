﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Web.I.Maintenance
{
    public partial class CompanySearchPage : BackOfficeBasePage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            EmptyMessage.Clear();
            
            if (!IsPostBack) {
                SearchType.DataSource = Enum<CompanySearch>.GetAllDescriptions();
                SearchType.DataBind();                
            }            
        }

        protected void SearchResultsGrid_RowClicked(object sender, GridViewRowClickedEventArgs args)
        {
            if (args.HasKey)
                NavigationController.EditCompany(args.Key);
        }

        protected void SearchResultsGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

        }

        protected void SearchForCompany(object sender, EventArgs e)
        {
            SearchResultsGrid.DataSource = null;

            var searchType = Enum<CompanySearch>.GetEnumFromDescription(SearchType.SelectedValue);
            var companies = CompanyService.FindCompanies(searchType, SearchValue.Text);

            if (companies.Count > 0) {
                SearchResultsGrid.DataSource = companies;                
            } else {
                EmptyMessage.SetInformation("No companies were found matching your search parameters");
            }
            SearchResultsGrid.DataBind();
        }


        protected void NewCompany(object sender, EventArgs e)
        {
            NavigationController.NewCompany();
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            AddButton.Visible = ConfigItems.AllowNewAssociatedEntities;
        }
    }
}
