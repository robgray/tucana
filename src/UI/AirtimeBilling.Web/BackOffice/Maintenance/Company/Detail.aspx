﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.CompanyMaintenancePage" Codebehind="Detail.aspx.cs" %>
<%@ Register Src="~/UserControls/CompanyInfo.ascx" TagName="company" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/CompanyInfoReadOnly.ascx" TagName="companyreadonly" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/Address.ascx" TagName="address" TagPrefix="sharp" %>
<%@ Register Src="~/UserControls/ActivityLogViewer.ascx" TagName="activitylog" TagPrefix="sharp" %>

<%@ Register Assembly="AirtimeBilling.Web" Namespace="AirtimeBilling.Web" TagPrefix="sharp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
<h1>Company Maintenance</h1>
<asp:Panel ID="pnlUserActions" runat="server" CssClass="well">            
    <asp:LinkButton CssClass="btn" ID="lnkDetailsView" runat="server" Text="Details" onclick="DetailsView" CausesValidation="false"  />
    <asp:LinkButton CssClass="btn" ID="lnkActivityLog" runat="server" Text="Activity Log"  onclick="ActivityLogViewer" CausesValidation="false"  />
    <asp:LinkButton CssClass="btn" ID="lnkSearch" runat="server" Icon="search" Text="Search" CausesValidation="false" OnClick="GotoSearch">
        <i class="icon-search"></i> Search
    </asp:LinkButton>   
</asp:Panel>
<sharp:MessagePanel ID="UserMessage" runat="server" />
<asp:ValidationSummary ID="valSummary" CssClass="ValidationSummary" runat="server" ShowSummary="True"  />
<div class="formcontent">
    <asp:Panel ID="pnlDetails" runat="server">
        <div class="row">
            <asp:HiddenField ID="hdnCompanyId" runat="server" />
            <sharp:company ID="CompanyInfo1" runat="server" /> 
        </div>
        <div class="row">            
            <sharp:address ID="OfficeAddress" runat="server" Text="Headquarters Address" />   
            <sharp:address ID="PostalAddress" runat="server" Text="Postal Address" />
        </div>

        <div class="form-actions">                    
            <asp:LinkButton CssClass="btn btn-success" ID="lnkSave" runat="server" Text="Save" Icon="disk" onclick="lnkSave_Click">
                <i class="icon-ok icon-white"></i> Save
            </asp:LinkButton>
        </div>        
    </asp:Panel>
    <asp:Panel ID="pnlActivityLog" runat="server" Visible="false">
        <div class="Form">
            <sharp:companyreadonly ID="CompanyInfo2" runat="server" ReadOnly="true" />        
        </div>
        <sharp:activitylog ID="LogViewer" runat="server" />    
    </asp:Panel>
</div>
</asp:Content>

