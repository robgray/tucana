﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="AirtimeBilling.Web.BackOfficeWelcomePage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<table>
<tr><td>
<div id="guiMenu">    
    <div class="menuItem" onclick="Menu.Click(this);" onmouseover="Menu.Selected(this);" onmouseout="Menu.UnSelected(this);">
        <table>
            <tr>
                <td><a id="A1" href="/Contracts/Search.aspx" runat="server"><img src="/images/icons/search-user.png" alt="View an existing Contract"/></a></td>                
                <td>View an existing Contract</td>
            </tr>
        </table>
    </div>
    
    <div class="menuItem" onclick="Menu.Click(this);" onmouseover="Menu.Selected(this);" onmouseout="Menu.UnSelected(this);">
        <table>
            <tr>
                <td><a id="NewContractLinkWelcome" href="/Contracts/SelectAccount.aspx" runat="server"><img src="/images/icons/call-user.png" alt="New Contract"/></a></td>                
                <td>Sign a Customer up for Airtime</td>
            </tr>
        </table>
    </div>
    
    <div class="menuItem" onclick="Menu.Click(this);" onmouseover="Menu.Selected(this);" onmouseout="Menu.UnSelected(this);">
        <table>
            <tr>
                <td><a href="/BackOffice/Invoicing/InvoiceRun.aspx"><img src="/images/icons/todo.png" alt="Invoice"/></a></td>
                <td>Perform an Invoice Run or View an existing Invoice</td>
            </tr>
        </table>               
    </div>     
</div>
</td>
</tr>
</table>
</asp:Content>
