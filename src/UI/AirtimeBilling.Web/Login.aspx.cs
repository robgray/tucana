﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI; 
using System.Web.UI.WebControls;
using System.Web.Security;
using System.Web.Profile;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public partial class Login : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            User user = Users.GetUser(HttpContext.Current.User.Identity.Name);
            if (user != null)
            {
                Response.Redirect(user.HomePage);                
            }
        }
        

        protected void MainLogin_Authenticate(object sender, AuthenticateEventArgs e)
        {
            if (this.IsValid)
            {                                
                if (Membership.ValidateUser(MainLogin.UserName, MainLogin.Password))
                {
                    User user = Users.GetUser(MainLogin.UserName);
                    
                    FormsAuthentication.SetAuthCookie(MainLogin.UserName, MainLogin.RememberMeSet);                        
                    Response.Redirect(user.HomePage);                                            
                }
            }
        }
    }
}