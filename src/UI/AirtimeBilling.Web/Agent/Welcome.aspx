﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" CodeBehind="Welcome.aspx.cs" Inherits="AirtimeBilling.Web.AgentWelcomePage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" runat="server">
<table>
<tr><td>
<div id="guiMenu">    
    <div class="menuItem" onclick="Menu.Click(this);" onmouseover="Menu.Selected(this);" onmouseout="Menu.UnSelected(this);">
        <table>
            <tr>
                <td><a id="NewContractLinkWelcome" href="/Contracts/SelectAccount.aspx" runat="server"><img src="/images/icons/call-user.png" alt="New Contract"/></a></td>                
                <td>Click here to sign a customer up for I Airtime.</td>
            </tr>
        </table>
    </div>
    <div class="menuItem" onclick="Menu.Click(this);" onmouseover="Menu.Selected(this);" onmouseout="Menu.UnSelected(this);">
        <table>
            <tr>
                <td><a href="/I/Maintenance/Agent/Detail.aspx"><img src="/images/icons/edit-user.png" alt="Edit details"/></a></td>
                <td>If your contact details have changed please update them by clicking here</td>
            </tr>
        </table>               
    </div>     
</div>
</td>
<td id="rightGuiMenu">
<h2>Agents</h2>
</td>
</tr>
</table>
</asp:Content>
