﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    public partial class ErrorDetailPage : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentSession.ExceptionMessage != null)
                {
                    lblDescription.Text = CurrentSession.ExceptionMessage;
                }
                else
                {
                    lblDescription.Visible = false;
                }
            }
        }
    }
}