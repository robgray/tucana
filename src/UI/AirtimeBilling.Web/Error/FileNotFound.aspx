﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.FileNotFoundPage" Codebehind="FileNotFound.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
    <p class="body" style="font-size: 0.8em; text-align:center; margin: 20px; margin-top:80px">
    Whoops!  I think the page got lost in the mail or maybe you typed it wrong.        
    </p>
</asp:Content>

