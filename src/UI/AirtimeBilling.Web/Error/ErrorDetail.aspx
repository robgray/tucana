﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.ErrorDetailPage" Codebehind="ErrorDetail.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
    <p class="body" style="width:auto; text-align: center">
        <br />
        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/exclamation.gif" /><asp:Label ID="Label1" runat="server" Text="An error has occurred" CssClass="ErrorMessage" style="margin-left: 10px;font-size:1.15em"></asp:Label>
        <br />
        <br />
        <asp:Label ID="lblDescription" runat="server" Text=""  style="font-size:1.15em"></asp:Label><br /><br />
        <asp:Label ID="Label2" runat="server" Text="A detailed error message has been sent to program support." CssClass="ErrorMessage" style="margin-left: 10px;font-size:1.15em"></asp:Label>
        <br /><br />
        <span class="body">Click to <asp:HyperLink ID="hlReturn" runat="server" NavigateUrl="~/Login.aspx">here</asp:HyperLink>&nbsp;to continue.</span>
    </p>
</asp:Content>

