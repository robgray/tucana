﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Centered.Master" AutoEventWireup="true" Inherits="AirtimeBilling.Web.NoAccessPage" Codebehind="NoAccess.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="BodyContent" Runat="Server">
    <p class="body" style="font-size: 0.8em; text-align:center; margin: 20px; margin-top:80px">
    Whoops!  It seems you don't have access to this resource.   <br/>
    If you think should have access, please contact the administrator.<br /><br />
    Your IP address and location has been recorded.    
    </p>
</asp:Content>

