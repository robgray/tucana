﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public interface ISession
    {
        bool SearchReadOnly { get; set; }

        string ExceptionMessage { get; set; }

        Account Account { get; set; }
                
        MasterAccount MasterAccount { get; set; }

        void Clear();
    }
}
