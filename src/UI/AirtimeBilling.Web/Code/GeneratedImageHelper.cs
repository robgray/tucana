﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirtimeBilling.Web
{
    public static class GeneratedImageHelper
    {
        public static string GetButtonUrl(string type, string text, string icon)
        {
            IDictionary<string, string> queryDict = new Dictionary<string, string>();

            queryDict.Add("type", SafelyEncode(type));
            queryDict.Add("text", SafelyEncode(text) + ".png");

            if (!String.IsNullOrEmpty(icon)) queryDict.Add("icon", SafelyEncode(icon));

            string query = "?" + Text.ImplodePairs(queryDict);

            return String.Format(@"/GeneratedImages/Button.ashx{0}", query);
        }

        public static string GetButtonUrl(string type, string text)
        {
            return GetButtonUrl(type, text, null);
        }

        public static string GetModuleUrl(string type, string text)
        {

            return String.Format(@"/GeneratedImages/Module.ashx/{0}/{1}.png", SafelyEncode(type),
                SafelyEncode(text));
        }

        public static string SafelyEncode(string toEncode)
        {
            return (HttpUtility.UrlEncode(HttpUtility.UrlEncode(toEncode ?? "")) ?? "").Replace("*", "%252a");
        }


    }
}
