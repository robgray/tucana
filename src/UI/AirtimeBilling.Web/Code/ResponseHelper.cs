﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace AirtimeBilling.Web
{
    public static class ResponseHelper
    {
        public static void Redirect(this HttpResponse response, string url, string target, string windowFeatures)
        {
            HttpContext context = HttpContext.Current;

            if ((String.IsNullOrEmpty(target) ||
                target.Equals("_self", StringComparison.OrdinalIgnoreCase)) &&
                String.IsNullOrEmpty(windowFeatures))
            {

                response.Redirect(url);
            }
            else
            {
                Page page = (Page)HttpContext.Current.Handler;
                if (page == null)
                {
                    throw new InvalidOperationException(
                        "Cannot redirect to new window outside Page context.");
                }
                url = page.ResolveClientUrl(url);

                string script;
                if (!String.IsNullOrEmpty(windowFeatures))
                {
                    script = @"window.open('{0}', '{1}', '{2}');";
                }
                else
                {
                    script = @"window.open('{0}', '{1}');";
                }

                script = String.Format(script, url, target, windowFeatures);
                ScriptManager.RegisterStartupScript(page, typeof(Page), "Redirect", script, true);
            }
        }

        public static void ReturnCsv(this HttpResponse response, string csvText)
        {
            response.Buffer = true;
            response.Clear();
            response.ClearContent();
            response.ClearHeaders();
            response.ContentType = "text/csv";
            response.AddHeader("content-disposition", "attachment; filename=eway.csv");
            response.AddHeader("Pragma", "public");

            response.Write(csvText);

            response.End();
        }
    }
}