﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirtimeBilling.Web
{
    public enum Networks 
    {
        Iridium = 1,
        Inmarsat = 2,
        Thuraya = 3
    };

    public enum Roles 
    {
        Agent = 1,
        Customer = 2,
        BackOffice = 3,
        Support = 4
    };
}