﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using CSSFriendly;

namespace AirtimeBilling.Web
{
    public class SSGridViewAdapter : BaseGridViewAdapter
    {
        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            GridView control = base.Control as GridView;
            if (control != null) {
                if (control.Rows.Count > 0) {
                    base.RenderContents(writer);    
                }
                else if (control.DataSource != null) {
                    var panel = new MessagePanel();
                    panel.MessageType = "Information";
                    panel.Message = !string.IsNullOrEmpty(control.EmptyDataText)
                                        ? control.EmptyDataText
                                        : "Contains no data";
                    panel.RenderControl(writer);
                }
            }
        }
    }
}
