﻿using System;
using System.ComponentModel;
using System.Web.UI;

namespace AirtimeBilling.Web
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DynamicHoverImageButton runat=server></{0}:DynamicHoverImageButton>")]
    public class DynamicHoverImageButton : DynamicImageButton 
    {        
        protected override void OnPreRender(EventArgs e)
        {
            if (IsSelected) {
                base.OnPreRender(e);
                ImageUrl = GetGeneratedImageUrl(ButtonType + "hover");
            } else {
                Attributes.Add("onmouseover", "this.src='" + GetGeneratedImageUrl(ButtonType + "hover") + "';");
                Attributes.Add("onmouseout", "this.src='" + GetGeneratedImageUrl(ButtonType) + "';");
                base.OnPreRender(e);
            }
            
        }

        public bool IsSelected
        {
            get { return (bool)(ViewState["IsSelected"] ?? false); }
            set
            {
                ViewState["IsSelected"] = value;
                if (value) {
                        ImageUrl = GetGeneratedImageUrl(ButtonType + "hover");
                    Attributes.Remove("onmouseover");
                    Attributes.Remove("onmouseout");
                }  else {
                    Attributes.Add("onmouseover", "this.src='" + GetGeneratedImageUrl(ButtonType + "hover") + "';");
                    Attributes.Add("onmouseout", "this.src='" + GetGeneratedImageUrl(ButtonType) + "';");
                }
            }
        }
    }
}
