﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    public enum FormGroupDirection
    {
        Horizontal,
        Inline
    }

    /// <summary>
    /// Custom control to group forms elements together.
    /// Renders either fieldset element or div with fieldset class
    /// </summary>
    public class FormGroup : Panel
    {        
        public override void RenderBeginTag(HtmlTextWriter writer)
        {
            string groupingText = this.GroupingText;
            writer.AddAttribute(HtmlTextWriterAttribute.Class, FlowDirectionString + " " + CssClass);
            AddAttributesToRender(writer);
            writer.RenderBeginTag(HtmlTextWriterTag.Div);
                
            writer.RenderBeginTag(HtmlTextWriterTag.Fieldset);
            if (!string.IsNullOrEmpty(groupingText))
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Legend);
                writer.Write(groupingText);
                writer.RenderEndTag();
            }                      
        }

        private string FlowDirectionString
        {
            get
            {
                switch (FlowDirection)
                {
                    case FormGroupDirection.Inline:
                        return "form-inline";
                    default:
                        return "form-horizontal";
                }
            }
        }

        public FormGroupDirection FlowDirection
        {
            get
            {
                if (ViewState["FlowDirection"] != null)
                    return (FormGroupDirection)ViewState["FlowDirection"];
                return FormGroupDirection.Horizontal;
            }
            set { ViewState["FlowDirection"] = value; }
        }
        
        public override void RenderEndTag(HtmlTextWriter writer)
        {                       
            writer.RenderEndTag();
            writer.RenderEndTag();
        }

        public bool UseFieldset 
        { 
            get
            {
                return ViewState["UseFieldset"] != null && (bool)ViewState["UseFieldset"];
            }
            set 
            { 
                ViewState["UseFieldset"] = value; 
            } 
        }
    }
}
