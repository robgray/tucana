﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    public delegate void GridViewRowClicked(object sender, GridViewRowClickedEventArgs args);

    public class ClickableGridView : GridView
    {
                
        private static readonly object RowClickedEventKey = new object();

        public event GridViewRowClicked RowClicked;

        public string RowCssClass
        {
            get
            {
                string rowClass = (string)ViewState["rowClass"];
                if (!string.IsNullOrEmpty(rowClass))
                    return rowClass;                
                return string.Empty;
            }
            set
            {
                ViewState["rowClass"] = value;
            }
        }

        public string HoverRowCssClass
        {
            get
            {
                string hoverRowClass = (string)ViewState["hoverRowClass"];
                if (!string.IsNullOrEmpty(hoverRowClass))
                    return hoverRowClass;               
                return string.Empty;
            }
            set
            {
                ViewState["hoverRowClass"] = value;
            }
        }

        protected virtual void OnRowClicked(GridViewRowClickedEventArgs e)
        {
            if (RowClicked != null)
                RowClicked(this, e);
        }

        protected override void RaisePostBackEvent(string eventArgument)
        {
            if (eventArgument.StartsWith("rc"))
            {
                var index = Int32.Parse(eventArgument.Substring(2));
                var key = 0;
                if (DataKeyNames.Length > 0) {
                    key = int.Parse(DataKeys[index].Value.ToString());
                }
                var args = new GridViewRowClickedEventArgs(Rows[index], key);
                OnRowClicked(args);
            }
            else
                base.RaisePostBackEvent(eventArgument);
        }

        protected override void PrepareControlHierarchy()
        {
            base.PrepareControlHierarchy();

            for (int i = 0; i < Rows.Count; i++)
            {
                string argsData = "rc" + Rows[i].RowIndex.ToString();
                Rows[i].Attributes.Add("onclick", Page.ClientScript.GetPostBackEventReference(this, argsData));

                if (RowCssClass != string.Empty)
                    Rows[i].Attributes.Add("onmouseout", "this.className='" + RowCssClass + "';");

                if (HoverRowCssClass != string.Empty)
                    Rows[i].Attributes.Add("onmouseover", "this.className='" + HoverRowCssClass + "';");
            }
        }
    }

    public class GridViewRowClickedEventArgs : EventArgs
    {
        private GridViewRow _row;
        private int _key;

        public GridViewRowClickedEventArgs(GridViewRow aRow, int key)
            : base()
        {
            _row = aRow;
            _key = key;
        }

        public GridViewRow Row
        {
            get { return _row; }
        }

        public int Key
        {
            get { return _key;}
        }

        public bool HasKey
        {
            get { return Key > 0;  }
        }
    }    
}