﻿using System;
using System.Collections.Specialized;
using System.Web;

namespace AirtimeBilling.Web
{
    public class TextImageParameters
    {
        public string Icon { get; set; }
        public string Type { get; set; }
        public string Text { get; set; }
        public int? FixedWidth { get; set; }
        readonly string _pathInfo;

        // This character would normally be Url Encoded if it appeared in
        // pathInfo. Since we do not encode this character during Cache Key generation
        // it allows us to distinctly separate the arbitrary button text (which may have contained an encoded version of this character)
        // from other cache key values, without breaking compatability with previously generated images that do not need additional cache key parts
        const string CacheKeySeparator = "=";

        public TextImageParameters(string pathInfo, NameValueCollection queryArgs)
        {
            if (pathInfo.EndsWith(".png"))
                pathInfo = pathInfo.Substring(0, pathInfo.Length - 4);

            _pathInfo = pathInfo;

            // Extract the button Type and Text from our pathInfo (backwards compatability)
            this.Type = queryArgs["type"];            
            this.Text = queryArgs["text"];
            if (this.Text != null && this.Text.EndsWith(".png"))
                this.Text = queryArgs["text"].Substring(0, this.Text.Length - 4);
            
            this.Icon = queryArgs["icon"];

            if (Type == null)
                return;
        }

        /// <summary>
        /// Retrieves a suitable string to use to uniquely identify this parameter configuration
        /// </summary>
        public string GetCacheKey()
        {
            // We use our provided pathInfo for generating a string that contains our Type and Text
            // arguments for backward compatability with the previous implementation, instead of using the Type
            // and Text properties explicitly                 
            var cacheKey = Web.Text.UrlEncode(_pathInfo, new[] { '.' });

            if (!string.IsNullOrEmpty(Icon))
                cacheKey += CreateCacheKeyPair("Icon", Icon);

            return cacheKey;
        }

        /// <summary>
        /// Generates a suitable piece of text to append during cache key creation
        /// that will allow any image to be uniquely identified with the chosen TextImageParameter combination
        /// </summary>
        static string CreateCacheKeyPair(string name, string value)
        {
            return CacheKeySeparator + name + CacheKeySeparator + value;
        }
    }
}
