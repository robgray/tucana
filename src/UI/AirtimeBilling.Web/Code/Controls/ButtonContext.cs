﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace AirtimeBilling.Web
{
    public class ButtonContext : TextImageContext
    {
        public ButtonContext(HttpContext context) : base(context) { }
        
        protected override bool Setup(TextStyle textStyle, TextImageParameters textParams)
        {
            textStyle.Color = Color.Black;
            textStyle.Family = "Mini 7";
            textStyle.Style = FontStyle.Regular;
            textStyle.Size = 10;

            MinHeight = 17;
            // FIXME: -1 on mono. This should be calculated
            // automatically
            YOffset = -2;

            bool shouldBeCached = true;

            switch (textParams.Type)
            {                
                case "small":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/smallbk.png";
                    _leftTextureFile = "/images/templates/buttons/smallleft.png";
                    _rightTextureFile = "/images/templates/buttons/smallright.png";
                    textStyle.Family = "Arial";
                    textStyle.Style = FontStyle.Bold;
                    MinHeight = 16;
                    textStyle.Size = 11;
                    YOffset = -2;

                    // Apply a few different changes if we are showing an icon too
                    if (!string.IsNullOrEmpty(textParams.Icon))
                    {
                        UseDefaultWidth = false;
                        AdditionalIconTextLeftOffset = -4;
                        AdditionalWidth = 7;
                    }
                    break;

                case "large":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/largebk.png";
                    _leftTextureFile = "/images/templates/buttons/largeleft.png";
                    _rightTextureFile = "/images/templates/buttons/largeright.png";
                    textStyle.Family = "Arial";
                    textStyle.Style = FontStyle.Bold;
                    MinHeight = 20;
                    textStyle.Size = 11;
                    YOffset = -3;

                    // Apply a few different changes if we are showing an icon too
                    if (!string.IsNullOrEmpty(textParams.Icon))
                    {
                        UseDefaultWidth = false;
                        AdditionalIconTextLeftOffset = -4;
                        AdditionalWidth = 7;
                    }
                    break;
                case "largewithicon":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/largebk.png";
                    _leftTextureFile = "/images/templates/buttons/largeleft.png";
                    _rightTextureFile = "/images/templates/buttons/largeright.png";
                    textStyle.Family = "Arial";
                    textStyle.Style = FontStyle.Bold;
                    MinHeight = 30;
                    textStyle.Size = 11;
                    YOffset = -3;

                    // Apply a few different changes if we are showing an icon too
                    if (!string.IsNullOrEmpty(textParams.Icon))
                    {
                        UseDefaultWidth = false;
                        AdditionalIconTextLeftOffset = -4;
                        AdditionalWidth = 7;
                    }
                    break;

                case "padded":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/padded-bk.png";
                    _leftTextureFile = "/images/templates/buttons/padded-left.png";
                    _rightTextureFile = "/images/templates/buttons/padded-right.png";
                    textStyle.Family = "Arial";
                    textStyle.Style = FontStyle.Bold;
                    MinHeight = 26;
                    textStyle.Size = 12;
                    YOffset = -6;
                    break;

                case "nonpadded":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/padded-bk.png";
                    _leftTextureFile = "/images/templates/buttons/padded-left.png";
                    _rightTextureFile = "/images/templates/buttons/padded-right.png";
                    textStyle.Family = "Arial";
                    textStyle.Style = FontStyle.Bold;
                    MinHeight = 26;
                    textStyle.Size = 12;
                    YOffset = -6;
                    UseDefaultWidth = false;
                    AdditionalWidth = 4;
                    break;

                case "blue":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/blue-bk.png";
                    _leftTextureFile = "/images/templates/buttons/blue-left.png";
                    _rightTextureFile = "/images/templates/buttons/blue-right.png";
                    textStyle.Family = "Arial";
                    textStyle.Color = Color.White;
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 16;
                    AdditionalIconTextLeftOffset = 4;
                    MinHeight = 31;
                    YOffset = -6;
                    break;

                case "largemenu":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    MinHeight = 0;
                    textStyle.Size = 17;
                    YOffset = 0;
                    UseDefaultWidth = false;
                    Png8 = false;
                    break;

                case "newlargemenu":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    MinHeight = 0;
                    textStyle.Size = 20;
                    YOffset = 0;
                    UseDefaultWidth = false;
                    Png8 = false;
                    break;

                case "voucher":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    MinHeight = 0;
                    textStyle.Size = 24;
                    YOffset = 0;
                    UseDefaultWidth = false;
                    Png8 = false;
                    break;

                case "transparentmodule":
                    _background = Color.FromArgb(0, 255, 255, 255);


                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    textStyle.Style = FontStyle.Regular;

                    textStyle.Size = 24;
                    MinHeight = 41;
                    YOffset = -6;

                    UseDefaultWidth = false;
                    break;

                case "newslettermodule":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    _backgroundTextureFile = "/images/templates/buttons/newsletterbk.png";

                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(255, 255, 255);
                    textStyle.Style = FontStyle.Regular;

                    textStyle.Size = 24;
                    MinHeight = 41;
                    YOffset = -6;

                    UseDefaultWidth = false;
                    break;

                case "largegenre":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    MinHeight = 24;
                    textStyle.Size = 15;
                    YOffset = 0;
                    Png8 = false;
                    break;

                case "submenu":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/COPRGTL.ttf");
                    textStyle.Color = Color.Black;
                    MinHeight = 20;
                    textStyle.Size = 15;
                    YOffset = 1;
                    Png8 = false;
                    break;

                case "submenuhover":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/COPRGTL.ttf");
                    textStyle.Color = Color.FromArgb(255, 153, 0);
                    MinHeight = 20;
                    textStyle.Size = 15;
                    YOffset = 1;
                    Png8 = false;
                    break;

                case "topmenu":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(124, 203, 254);
                    MinHeight = 0;
                    textStyle.Size = 20;
                    YOffset = 0;
                    Png8 = false;
                    break;

                case "topmenuwhite":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    textStyle.Color = Color.FromArgb(255, 255, 255);
                    MinHeight = 0;
                    textStyle.Size = 20;
                    YOffset = 0;
                    Png8 = false;
                    UseDefaultWidth = false;
                    break;

                case "ltblue":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/ltblue-bk.png";
                    _leftTextureFile = "/images/templates/buttons/ltblue-left.png";
                    _rightTextureFile = "/images/templates/buttons/ltblue-right.png";
                    textStyle.Color = Color.White;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;

                case "ltblueleft":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/ltblue-bk.png";
                    _leftTextureFile = "/images/templates/buttons/ltblue-left.png";
                    _rightTextureFile = "/images/templates/buttons/ltblue-leftbk.png";
                    textStyle.Color = Color.White;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;

                case "ltblueright":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/ltblue-bk.png";
                    _leftTextureFile = "/images/templates/buttons/ltblue-rightbk.png";
                    _rightTextureFile = "/images/templates/buttons/ltblue-right.png";
                    textStyle.Color = Color.White;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;

                case "ltbluemiddle":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/ltblue-bk.png";
                    _leftTextureFile = "/images/templates/buttons/ltblue-rightbk.png";
                    _rightTextureFile = "/images/templates/buttons/ltblue-leftbk.png";
                    textStyle.Color = Color.White;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;


                case "silver":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/silver-bk.png";
                    _leftTextureFile = "/images/templates/buttons/silver-left.png";
                    _rightTextureFile = "/images/templates/buttons/silver-right.png";
                    textStyle.Color = Color.Black;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;

                case "silverleft":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/silver-bk.png";
                    _leftTextureFile = "/images/templates/buttons/silver-left.png";
                    _rightTextureFile = "/images/templates/buttons/silver-leftbk.png";
                    textStyle.Color = Color.Black;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;

                case "silverright":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/silver-bk.png";
                    _leftTextureFile = "/images/templates/buttons/silver-rightbk.png";
                    _rightTextureFile = "/images/templates/buttons/silver-right.png";
                    textStyle.Color = Color.Black;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;

                case "silvermiddle":
                    _background = Color.FromArgb(0, 255, 255, 255);
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
                    _backgroundTextureFile = "/images/templates/buttons/silver-bk.png";
                    _leftTextureFile = "/images/templates/buttons/silver-rightbk.png";
                    _rightTextureFile = "/images/templates/buttons/silver-leftbk.png";
                    textStyle.Color = Color.Black;
                    textStyle.Size = 19;
                    MinHeight = 46;
                    YOffset = -12;
                    break;


                #region old styles
                // OLD STYLES - maintained just for CMS
                case "light":
                    _background = Color.FromArgb(0xec, 0xec, 0xec);
                    break;
                case "medium":
                    _background = Color.FromArgb(0xc9, 0xc9, 0xc9);
                    break;
                case "smallheader":
                    _background = Color.FromArgb(0xc9, 0xc9, 0xc9);
                    MinHeight = 15;
                    YOffset = 0;
                    break;
                case "smallheaderhover":
                    _background = Color.FromArgb(0x66, 0x66, 0x66);
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    MinHeight = 15;
                    YOffset = 0;
                    break;
                case "dark":
                    _background = Color.FromArgb(0x66, 0x66, 0x66);
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    break;
                case "normalhover":
                case "highlight":
                    _background = Color.FromArgb(0xff, 0x99, 0x00);
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    break;
                #endregion

                //Medium grey to black colour gradient used on main navigation buttons in header.
                case "darkgradient":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/HeaderButtonBGDark.png";
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 14;
                    MinHeight = 22;
                    break;
                //Medium orange to orange yellow colour gradient used as mouseover affect for darkgradien buttons.			
                case "darkgradienthover":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/HeaderButtonBGDarkOver.png";
                    textStyle.Color = Color.FromArgb(0, 0, 0);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 14;
                    MinHeight = 22;
                    break;
                case "darkgradientmedium":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/darkmediumidoff.png";
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 14;
                    MinHeight = 18;
                    YOffset = -1;
                    break;
                case "darkgradientmediumhover":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/darkmediumidon.png";
                    textStyle.Color = Color.FromArgb(0, 0, 0);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 14;
                    MinHeight = 18;
                    YOffset = -1;
                    break;
                case "darkgradientsmall":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/darksmallmidoff.png";
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 14;
                    MinHeight = 15;
                    YOffset = 1;
                    break;
                case "darkgradientsmallhover":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/darksmallmidon.png";
                    textStyle.Color = Color.FromArgb(0, 0, 0);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 14;
                    MinHeight = 15;
                    YOffset = 1;
                    break;
                case "darkrounded":
                    textStyle.Family = "Arial";
                    _backgroundTextureFile = "/images/Modules/darkroundedmidoff.png";
                    _leftTextureFile = "/images/Modules/darkroundedleftoff.png";
                    _rightTextureFile = "/images/Modules/darkroundedrightoff.png";
                    textStyle.Color = Color.FromArgb(255, 255, 255);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 11;
                    MinHeight = 22;
                    UseDefaultWidth = false;
                    YOffset = -4;
                    break;
                case "darkroundedhover":
                    textStyle.Family = "Arial";
                    _backgroundTextureFile = "/images/Modules/darkroundedmidon.png";
                    _leftTextureFile = "/images/Modules/darkroundedlefton.png";
                    _rightTextureFile = "/images/Modules/darkroundedrighton.png";
                    textStyle.Color = Color.FromArgb(255, 255, 255);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 11;
                    MinHeight = 22;
                    UseDefaultWidth = false;
                    YOffset = -4;
                    break;
                case "darkgradientlarge":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/BigButtonBGDark.png";
                    textStyle.Color = Color.FromArgb(254, 254, 254);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 22;
                    MinHeight = 36;
                    break;
                case "darkgradientlargehover":
                    textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Bold-Condensed.ttf");
                    _backgroundTextureFile = "/images/Modules/BigButtonBGDarkOver.png";
                    textStyle.Color = Color.FromArgb(255, 255, 255);
                    textStyle.Style = FontStyle.Bold;
                    textStyle.Size = 22;
                    MinHeight = 36;
                    break;
                default:
                    _background = Color.Green;
                    textStyle.Color = Color.Purple;
                    shouldBeCached = false;

                    break;
            }

            return shouldBeCached;
        }

        protected override string ClassName
        {
            get
            {
                return "Button";
            }
        }

        protected override Color Background
        {
            get
            {
                return _background;
            }
        }
        private Color _background;

        protected override string BackgroundTextureFile
        {
            get
            {
                return _backgroundTextureFile;
            }
        }
        string _backgroundTextureFile;

        string _leftTextureFile;
        protected override string LeftTextureFile
        {
            get
            {
                return _leftTextureFile;
            }
        }

        string _rightTextureFile;
        protected override string RightTextureFile
        {
            get
            {
                return _rightTextureFile;
            }
        }



    }
}
