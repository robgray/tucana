﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:TabButton runat=server></{0}:TabButton>")]
    public class TabButton : WebControl, ITextControl 
    {
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return HtmlTextWriterTag.Img;
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {

            writer.AddAttribute(HtmlTextWriterAttribute.Id, ID);
            writer.AddAttribute("class", "submenu");

            CssStyleCollection styles = this.Style;
            foreach (HtmlTextWriterStyle styleKey in styles.Keys)
            {
                writer.AddStyleAttribute(styleKey, styles[styleKey]);
            }

            writer.AddAttribute("src", GetGeneratedImageUrl("submenu"));
            writer.AddAttribute("onmouseover", "this.src='" + GetGeneratedImageUrl("submenuhover") + "';");
            writer.AddAttribute("onmouseout", "this.src='" + GetGeneratedImageUrl("submenu") + "';");
        }

        #region ITextControl Members

        public string Text
        {
            get
            {
                return (string)ViewState["Text"] ?? string.Empty;
            }
            set
            {
                ViewState["Text"] = value;
            }
        }

        #endregion

        protected string GetGeneratedImageUrl(string buttonType)
        {
            return string.IsNullOrEmpty(Icon) ?
                GeneratedImageHelper.GetButtonUrl(buttonType, this.Text) :
                GeneratedImageHelper.GetButtonUrl(buttonType, this.Text, this.Icon);
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Icon
        {
            get
            {
                String s = (String)ViewState["Icon"];
                return string.IsNullOrEmpty(s) ? "" : s;
            }

            set
            {
                ViewState["Icon"] = value;
            }
        }

    }
}
