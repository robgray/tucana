﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Services.Description;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AirtimeBilling.Web
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:Tip runat=server></{0}:Tip>")]
    public class Tip : Table
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Text
        {
            get
            {
                var message = ViewState["TipText"] as String;
                return string.IsNullOrEmpty(message) ? "" : message;
            }
            set
            {
                ViewState["TipText"] = value;
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            // Add attributes to div for the class
            var css = string.Format("alert Tip");
            if (!string.IsNullOrEmpty(CssClass)) css += " " + CssClass;
            writer.AddAttribute(HtmlTextWriterAttribute.Class, css);
            if (string.IsNullOrEmpty(Text))
            {
                writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
            writer.Write(string.Format("<tbody><tr><td class=\"Message\">{0}</td></tr></tbody>", Text));
            writer.BeginRender();
        }       
    }
}
