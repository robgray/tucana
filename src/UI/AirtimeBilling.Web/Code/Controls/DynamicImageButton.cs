﻿using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:DynamicImageButton runat=server></{0}:DynamicImageButton>")]
    public class DynamicImageButton : ImageButton
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("default")]
        [Localizable(true)]
        public string ButtonType
        {
            get
            {
                String s = (String)ViewState["ButtonType"];
                return string.IsNullOrEmpty(s) ? "darkrounded" : s;
            }

            set
            {
                ViewState["ButtonType"] = value;                
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public new string Text
        {
            get
            {
                String s = (String)ViewState["Text"];
                return string.IsNullOrEmpty(s) ? "" : s;
            }

            set
            {
                ViewState["Text"] = value;                
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Icon
        {
            get
            {
                String s = (String)ViewState["Icon"];
                return string.IsNullOrEmpty(s) ? "" : s;
            }

            set
            {
                ViewState["Icon"] = value;
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            ImageUrl = GetGeneratedImageUrl();
            base.OnPreRender(e);
        }

        protected string GetGeneratedImageUrl()
        {
            return GetGeneratedImageUrl(ButtonType);
        }

        protected string GetGeneratedImageUrl(string buttonType)
        {
            return string.IsNullOrEmpty(Icon) ?
                GeneratedImageHelper.GetButtonUrl(buttonType, this.Text) :
                GeneratedImageHelper.GetButtonUrl(buttonType, this.Text, this.Icon);
        }
    }
}
