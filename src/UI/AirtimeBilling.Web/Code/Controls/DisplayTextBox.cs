﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AirtimeBilling.Web
{    
    /// <summary>
    /// Generates a TextBox in normal mode or a Label/Span in read only mode.
    /// </summary>
    public class DisplayTextBox : WebControl, ITextControl 
    {
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return ReadOnly ? HtmlTextWriterTag.Span : HtmlTextWriterTag.Input;
            }
        }
                
        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);            
            if (ReadOnly)
                writer.Write(Text);            
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            
            writer.AddAttribute(HtmlTextWriterAttribute.Id, ID);
            if (!string.IsNullOrEmpty(CssClass)) writer.AddAttribute("class", CssClass);

            CssStyleCollection styles = this.Style;
            foreach(HtmlTextWriterStyle styleKey in styles.Keys)
            {
                writer.AddStyleAttribute(styleKey, styles[styleKey]);
            }
            
            if (!ReadOnly)
            {                
                writer.AddAttribute(HtmlTextWriterAttribute.Type, "text");
                writer.AddAttribute(HtmlTextWriterAttribute.Name, UniqueID);
                writer.AddAttribute(HtmlTextWriterAttribute.Value, Text);
            }
        }


        #region ITextControl Members

        public string Text
        {
            get
            {
                return (string)ViewState["Text"] ?? string.Empty;
            }
            set
            {
                ViewState["Text"] = value;
            }
        }

        #endregion

        public bool ReadOnly
        {
            get { return (bool) (ViewState["ReadOnly"] ?? false); }
            set { ViewState["ReadOnly"] = value; }
        }
    }
}
