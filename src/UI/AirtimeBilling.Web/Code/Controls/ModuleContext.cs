﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace AirtimeBilling.Web
{
    public class ModuleContext : TextImageContext
    {
        public ModuleContext(HttpContext context) : base(context) { }

        protected override string BackgroundTextureFile
        {
            get { return "/images/templates/module/head.png"; }
        }

        protected override int DefaultWidthMargin
        {
            get { return 0; }
        }

        protected override bool Setup(TextStyle textStyle, TextImageParameters textParams)
        {
            textStyle.FontFileName = Server.MapPath("/images/headings/Berthold-Akzidenz-Grotesk-BE-Condensed.ttf");
            textStyle.Color = Color.FromArgb(255, 255, 255);
            textStyle.Style = FontStyle.Regular;

            Png8 = false;

            if (textParams.Type == "std")
            {
                textStyle.Size = 24;
                MinHeight = 41;
                YOffset = -6;
            }
            else
            {
                return false;
            }
            return true;
        }

        protected override string ClassName
        {
            get
            {
                return "Module";
            }
        }
    }
}
