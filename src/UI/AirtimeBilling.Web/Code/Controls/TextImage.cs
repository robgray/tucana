﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.IO;
using System.Web;
using System.Web.UI;
using Brush = System.Drawing.Brush;
using Color = System.Drawing.Color;
using Pen = System.Drawing.Pen;

namespace AirtimeBilling.Web
{
    public abstract class TextImage : Page
    {
        protected static int brush_alpha = 150;
        protected static int tip_alpha = 200;
        private bool useDefaultWidth = true;

        protected TextImage()
        {
            IconXOffset = 3;
            Png8 = true;
        }

        protected bool Png8
        {
            get;
            set;
        }

        protected virtual int DefaultWidthMargin
        {
            //default width on buttons
            get { return 10; }
        }

        protected string text;

        //FIXME: This is workaround for a mono bug, where two
        //simulatenous requests  running this code together will fail
        private static readonly object FontLock = new Object();

        protected static void AddText(int x, int y, string text, Graphics graphics, TextStyle cls, StringFormat format)
        {
            if (cls == null)
                throw new ArgumentNullException("cls");

            lock (FontLock)
            {
                Font verFont;
                PrivateFontCollection pfc = new PrivateFontCollection();

                try
                {
                    if (cls.FontFileName != null && cls.FontFileName != string.Empty)
                    {
                        try
                        {
                            pfc.AddFontFile(cls.FontFileName);
                        }
                        catch (ArgumentNullException ex)
                        {
                            throw new Exception(string.Format("Font with filename '{0}' not found", cls.FontFileName), ex);
                        }

                        verFont = new Font(pfc.Families[0], cls.Size, cls.Style, GraphicsUnit.Pixel);
                    }
                    else
                        verFont = new Font(cls.Family, cls.Size, cls.Style, GraphicsUnit.Pixel);

                    using (verFont)
                    {
                        graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                        graphics.DrawString(text, verFont, cls.Brush, x, y, format);
                    }

                }
                finally
                {
                    // due to a bug in the BCL, each fontfamily object in the private font collection has to
                    // be explicitly disposed.  See:
                    // http://connect.microsoft.com/VisualStudio/feedback/ViewFeedback.aspx?FeedbackID=202970
                    // http://support.microsoft.com/kb/901026
                    // http://blog.loseyourmind.com/?m=200710
                    if (pfc.Families.Length > 0)
                    {
                        for (int i = pfc.Families.Length - 1; i >= 0; i--)
                            pfc.Families[i].Dispose();
                        pfc.Dispose();
                    }
                }
            }
        }



        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Convert.ToBoolean(Request["noheaders"]))
            {
                Response.ContentType = "image/png";
                Response.Cache.SetExpires(DateTime.Today.AddHours(6));
                Response.Cache.SetCacheability(HttpCacheability.Public);
            }

            // Read the input
            //var decodedPathInfo = "Title";
            //string pathInfo = BasePage.GetPathInfo(this);
            //if (pathInfo != null && pathInfo.Length > 1)
                //decodedPathInfo = pathInfo.Substring(1);
            //decodedPathInfo = Server.UrlDecode(decodedPathInfo.Trim());            

            NameValueCollection queryParams = new NameValueCollection();
            foreach (string key in Request.QueryString.Keys)
            {                
                queryParams.Add(key, Server.UrlDecode(Request.QueryString[key]));                
            }

            var decodedPathInfo = Text.JoinQueryString(Request.QueryString, "-", "-");

            // Setup text image parameters
            var textParams = new TextImageParameters(decodedPathInfo, queryParams);
            minHeight = 0;
            yOffset = 0;

            var filename = Server.MapPath("/images/generated/") + ClassName + "-" + textParams.GetCacheKey();
            if (!filename.EndsWith(".png"))
                filename += ".png";

            // At this point, we should check for a cached copy on-disk and return that if found
            if (CanServeFromDisk(filename))
            {
                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                using (BinaryReader reader = new BinaryReader(fs))
                {
                    byte[] buffer = new byte[fs.Length];
                    reader.Read(buffer, 0, Convert.ToInt32(fs.Length));
                    Response.BinaryWrite(buffer);
                    return;
                }
            }

            // Process our input
            using (TextStyle textStyle = new TextStyle())
            {
                if (textParams.FixedWidth.HasValue)
                    FixedWidth = textParams.FixedWidth.Value;

                var cacheImage = Setup(textStyle, textParams);

                if (Request.QueryString["color"] != null)
                {
                    string[] parts = Request.QueryString["color"].Split(new char[] { ',' });
                    textStyle.Color = Color.FromArgb(Convert.ToInt32(parts[0]), Convert.ToInt32(parts[1]), Convert.ToInt32(parts[2]));
                }

                if (!string.IsNullOrEmpty(textParams.Icon))
                    IconTextureFile = GetValidIconFilePath(textParams.Icon);

                text = textParams.Text;

                if (textStyle.PostScriptFamily == null)
                    CreateWithSystemDrawing(textStyle, textParams.Text, filename, cacheImage);
                else
                    CreateWithImageMagick(textStyle, textParams.Text, filename, cacheImage);
            }
        }

        string GetValidIconFilePath(string iconName)
        {
            var relativePath = string.Format("/images/templates/buttons/icons/{0}.png", Text.CreateValidFilenameFrom(iconName));
            var mappedPath = Server.MapPath(relativePath);
            if (!File.Exists(mappedPath))
                throw new Exception(string.Format("Invalid icon name: {0}", iconName));

            return relativePath;
        }

        private bool CanServeFromDisk(string filename)
        {
            return !(Convert.ToBoolean(Request["nocache"]) || Convert.ToBoolean(Request["refreshcache"])) && File.Exists(filename);
        }

        private string RgbInHex(Color c)
        {
            return String.Format("#{0:x2}{1:x2}{2:x2}", c.R, c.G, c.B);
        }

        private static object ImageMagickLock = new Object();


        private void CreateWithImageMagick(TextStyle textStyle, string text, string filename, bool cacheImage)
        {

            lock (ImageMagickLock)
            {
                Process imageMagick = new Process();
                imageMagick.StartInfo.CreateNoWindow = true;
                imageMagick.StartInfo.UseShellExecute = false;
                imageMagick.StartInfo.WorkingDirectory = Server.MapPath("/images/headings");
                imageMagick.StartInfo.FileName = imageMagick.StartInfo.WorkingDirectory + @"\\convert.exe";
                imageMagick.StartInfo.Arguments =
                    (BackgroundTextureFile != null ? " -size x" + MinHeight + " " : "") +
                    "-antialias -fill \"" + RgbInHex(textStyle.Color) + "\" -gravity north -font " + textStyle.PostScriptFamily + " -pointsize " + textStyle.Size + " label:\"" + text + "\" " +
                    (BackgroundTextureFile != null ? " -gravity center -tile \"" + Server.MapPath(BackgroundTextureFile) + "\" -draw \"color 0,0 reset\" " : " -background " + RgbInHex(Background) + " ") +
                    (BackgroundTextureFile != null ? " +tile -fill \"" + RgbInHex(textStyle.Color) + "\" -gravity center -annotate +0-" + (Math.Round((double)MinHeight / 10)) + "  \"" + text + "\" " : "") +
                    " \"" + filename + "\"";
                imageMagick.StartInfo.RedirectStandardOutput = true;
                imageMagick.StartInfo.RedirectStandardError = true;
                imageMagick.Start();

                string err = imageMagick.StandardError.ReadToEnd(); // + imageMagick.StandardError.ReadToEnd();
                imageMagick.WaitForExit();
                if (err != null && err != "")
                    throw new Exception("Could not create TextImage: " + err);

                // Read it back
                Bitmap newBitmap = null;
                Graphics newGraphic = null;

                try
                {
                    using (Image image = Image.FromFile(filename))
                    using (Bitmap objBitmap = new Bitmap(image))
                    {

                        int left, width;
                        FindMinimumBitmapWidth(objBitmap, out left, out width);
                        newBitmap = new Bitmap(width, MinHeight);
                        newGraphic = Graphics.FromImage(newBitmap);

                        newGraphic.DrawImage(image, 0, 0, new Rectangle(left, YOffset, width, MinHeight), GraphicsUnit.Pixel);

                        // Let go of the file so we can
                        // overwrite it
                    }

                    // Send  to user
                    MemoryStream io = new MemoryStream();
                    newBitmap.Save(io, ImageFormat.Png);
                    byte[] ba = io.GetBuffer();
                    Response.BinaryWrite(ba);

                    if (cacheImage)
                        SaveFile(filename, ba);
                }
                finally
                {
                    if (newGraphic != null)
                        newGraphic.Dispose();
                    if (newBitmap != null)
                        newBitmap.Dispose();
                }
            }
        }

        private void FindMinimumBitmapWidth(Bitmap objBitmap, out int left, out int width)
        {
            width = objBitmap.Width;
            int height = objBitmap.Height;

            // find min width
            int x, y;
            bool found = false;
            for (x = width - 1; x >= 0; x--)
            {
                for (y = 0; y < height; y++)
                {
                    if (objBitmap.GetPixel(x, y).ToArgb() != Color.White.ToArgb())
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    break;
            }
            width = x + 1;

            // find left offset
            found = false;
            for (x = 0; x < width; x++)
            {
                for (y = 0; y < height; y++)
                {
                    if (objBitmap.GetPixel(x, y).ToArgb() != Color.White.ToArgb())
                    {
                        found = true;
                        break;
                    }
                }
                if (found)
                    break;
            }
            left = x;
            width -= x;
        }

        private void CreateWithSystemDrawing(TextStyle textStyle, string text, string filename, bool cacheImage)
        {
            // Start with a large object, and find the required width and height

            int width = 800, height = 50;
            int top = 0, left = 0, right = 0;
            int actualwidth;
            int x, y;
            int? leftTextureWidth = null;

            using (Bitmap bitmap = new Bitmap(width, height))
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {

                // The calculated width varies due to antialiasing of the
                // font in various colours, so we will always use black when
                // measuring
                using (TextStyle textStyleWithBlackBrush = (TextStyle)textStyle.Clone())
                {
                    textStyleWithBlackBrush.Brush = new SolidBrush(Color.Black);
                    using (SolidBrush whiteBrush = new SolidBrush(Color.White))
                        graphics.FillRectangle(whiteBrush, 0, 0, width, height);
                    AddText(left, top, text, graphics, textStyleWithBlackBrush, Format);
                }

                FindMinimumBitmapWidth(bitmap, out left, out width);
                left = -left;



                // find min height

                bool found = false;
                for (y = height - 1; y >= 0; y--)
                {
                    for (x = 0; x < width; x++)
                    {
                        if (bitmap.GetPixel(x, y).ToArgb() != Color.White.ToArgb())
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        break;
                }
                height = y + 1;

                // find top offset
                found = false;
                for (y = 0; y < height; y++)
                {
                    for (x = 0; x < width; x++)
                    {
                        if (bitmap.GetPixel(x, y).ToArgb() != Color.White.ToArgb())
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        break;
                }
                if (yOffset != 0)
                    y = yOffset;

                top = -y;
                height -= y;

                if (height < minHeight)
                {
                    if (yOffset == 0)
                        top += (minHeight - y) / 2;
                    height = minHeight;
                }

                if (LeftTextureFile != null)
                {
                    using (Bitmap leftTexture = new Bitmap(Server.MapPath(LeftTextureFile)))
                    {
                        width += leftTexture.Width;
                        left += leftTexture.Width;
                        leftTextureWidth = leftTexture.Width;
                    }
                }
                if (RightTextureFile != null)
                {
                    using (Bitmap rightTexture = new Bitmap(Server.MapPath(RightTextureFile)))
                    {
                        width += rightTexture.Width;
                        right += rightTexture.Width;
                    }
                }

                if (IconTextureFile != null)
                {
                    using (var iconTexture = new Bitmap(Server.MapPath(IconTextureFile)))
                        width += iconTexture.Width;
                }

                if (FixedWidth != 0)
                {
                    actualwidth = FixedWidth;
                }
                else if (useDefaultWidth)
                {
                    actualwidth = width + (2 * DefaultWidthMargin);
                }
                else
                {
                    actualwidth = width + AdditionalWidth;
                }
            }


            if (actualwidth < 1) actualwidth = 1;

            // Create our actual image
            using (Bitmap bitmap = new Bitmap(actualwidth, height))
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                using (Brush backgroundBrush = BackgroundBrush)
                    graphics.FillRectangle(backgroundBrush, 0, 0, actualwidth, height);

                // If there is a textured background, fill up out output with it
                if (BackgroundTextureFile != null)
                {
                    using (Bitmap texturedBackground = new Bitmap(Server.MapPath(BackgroundTextureFile)))
                    {
                        for (x = leftTextureWidth ?? left; x < actualwidth - right - texturedBackground.Width; x += texturedBackground.Width)
                            graphics.DrawImage(texturedBackground, x, 0, texturedBackground.Width, height);

                        graphics.DrawImage(texturedBackground, x, 0, actualwidth - right - x, height);
                    }
                }

                // If there are left/right backgrounds, output them
                if (LeftTextureFile != null)
                {
                    using (Bitmap leftTexture = new Bitmap(Server.MapPath(LeftTextureFile)))
                        graphics.DrawImage(leftTexture, 0, 0, leftTexture.Width, height);
                }
                if (RightTextureFile != null)
                {
                    using (Bitmap rightTexture = new Bitmap(Server.MapPath(RightTextureFile)))
                        graphics.DrawImage(rightTexture, actualwidth - rightTexture.Width, 0, rightTexture.Width, height);
                }

                // If we have an IconTexture set, output it on the image
                var textLeftOffset = left;

                if (IconTextureFile != null)
                {
                    using (var iconTexture = new Bitmap(Server.MapPath(IconTextureFile)))
                    {
                        var xOffset = ((actualwidth - width) / 2) + IconXOffset;
                        graphics.DrawImage(iconTexture, xOffset, ((height - iconTexture.Height) / 2) + IconYOffset, iconTexture.Width, iconTexture.Height);
                        textLeftOffset += iconTexture.Width + IconXOffset + AdditionalIconTextLeftOffset;
                    }
                }

                // Output the button text
                AddText(textLeftOffset + ((actualwidth - width) / 2), top, text, graphics, textStyle, Format);

                // Draw border if set
                if (Border != null)
                {
                    graphics.DrawLine(Border, 0, 0, 0, height);
                    graphics.DrawLine(Border, 0, 0, actualwidth, 0);
                    graphics.DrawLine(Border, actualwidth - 1, 0, actualwidth - 1, height);
                    graphics.DrawLine(Border, 0, height - 1, actualwidth - 1, height - 1);
                }

                Correct99AlphaBug(bitmap);

                // Send
                MemoryStream io = new MemoryStream();
                bitmap.Save(io, ImageFormat.Png);

                byte[] ba = io.GetBuffer();

                if (Png8)
                    ba = CreatePng8(ba);

                if (cacheImage)
                    SaveFile(filename, ba);

                Response.BinaryWrite(ba);
                io.Close();
            }
        }

        private byte[] CreatePng8(byte[] ba)
        {
            using (var pngquant = new Process())
            {
                pngquant.StartInfo.CreateNoWindow = true;
                pngquant.StartInfo.UseShellExecute = false;
                pngquant.StartInfo.WorkingDirectory = Server.MapPath("/images/headings");
                pngquant.StartInfo.FileName = pngquant.StartInfo.WorkingDirectory + @"\\pngquant.exe";
                pngquant.StartInfo.Arguments = "256";
                pngquant.StartInfo.RedirectStandardInput = true;
                pngquant.StartInfo.RedirectStandardOutput = true;
                pngquant.StartInfo.RedirectStandardError = true;
                pngquant.Start();

                pngquant.StandardInput.BaseStream.Write(ba, 0, ba.Length);
                pngquant.StandardInput.Close();
                ba = ReadFully(pngquant.StandardOutput.BaseStream);
                string err = pngquant.StandardError.ReadToEnd();
                pngquant.WaitForExit();

                if (err != null && err != "")
                    throw new Exception("Could not create TextImage: " + err);
            }

            return ba;
        }

        /// <summary>
        /// There exists a bug in our (or more likely the BCL's) image stitching routine that sometimes renders a vertical 99% alpha line.
        /// IE6 will render this as fully opaque, so we need to look for any of these lines first and change them to 100% alpha.
        /// </summary>
        /// <param name="bitmap">Complete-as-possible bitmap</param>
        private void Correct99AlphaBug(Bitmap bitmap)
        {
            // search each pixel for 99% alpha. if such a pixel is found, make it 100% alpha. move on.
            for (int x = 0; x < bitmap.Width; x++)
            {
                for (int y = 0; y < bitmap.Height; y++)
                {
                    Color oldC = bitmap.GetPixel(x, y);
                    if (oldC.A.Equals(254))
                    {
                        Color newC = Color.FromArgb(255, oldC);
                        bitmap.SetPixel(x, y, newC);
                    }
                }
            }
        }

        // From http://www.yoda.arachsys.com/csharp/readbinary.html
        private static byte[] ReadFully(Stream stream)
        {
            byte[] buffer = new byte[32768];
            using (MemoryStream ms = new MemoryStream())
            {
                while (true)
                {
                    int read = stream.Read(buffer, 0, buffer.Length);
                    if (read <= 0)
                        return ms.ToArray();
                    ms.Write(buffer, 0, read);
                }
            }
        }


        private void SaveFile(string filename, byte[] ba)
        {

            // Save to cache file
            if (!Convert.ToBoolean(Request["nocache"]))
            {
                FileStream fs = null;
                BinaryWriter writer = null;

                try
                {
                    fs = new FileStream(filename, FileMode.Create);
                    writer = new BinaryWriter(fs);
                    writer.Write(ba);
                }
                catch
                {
                    // Should probably log to Windows application log
                    // or something like that
                }
                finally
                {
                    if (writer != null)
                        writer.Close();
                    if (fs != null)
                        fs.Close();
                }
            }
        }

        protected virtual string LeftTextureFile
        {
            get
            {
                return null;
            }
        }

        protected virtual string RightTextureFile
        {
            get
            {
                return null;
            }
        }

        protected virtual string BackgroundTextureFile
        {
            get
            {
                return null;
            }
        }

        protected virtual Color Background
        {
            get
            {
                return Color.White;
            }
        }

        protected virtual Pen Border
        {
            get
            {
                return null;
            }
        }

        protected Brush BackgroundBrush
        {
            get
            {
                return new SolidBrush(Background);
            }
        }

        /// <summary>
        /// Optional filename of a texture icon to overlay on the generated button
        /// </summary>
        protected string IconTextureFile { get; set; }
        protected int IconXOffset { get; set; }
        protected int IconYOffset { get; set; }
        protected int AdditionalWidth { get; set; }
        protected int AdditionalIconTextLeftOffset { get; set; }

        protected int MinHeight
        {
            get
            {
                return minHeight;
            }
            set
            {
                minHeight = value;
            }
        }

        protected int YOffset
        {
            get
            {
                return yOffset;
            }
            set
            {
                yOffset = value;
            }
        }

        protected virtual int FixedWidth
        {
            get
            {
                return _fixedWidth;
            }
            set
            {
                _fixedWidth = value;
            }
        }

        protected virtual bool UseDefaultWidth
        {
            get
            {
                return useDefaultWidth;
            }
            set
            {
                useDefaultWidth = value;
            }
        }

        /// <summary>
        /// Setup the TextImage properties used to render the image
        /// </summary>
        /// <returns>True if the result can be safely cached to disk, otherwise false if it should not be.</returns>
        protected abstract bool Setup(TextStyle textStyle, TextImageParameters textParams);

        protected abstract string ClassName
        {
            get;
        }

        private StringFormat Format
        {
            get
            {
                return StringFormatContainer.Instance;
            }
        }

        // Lazy load a single instance of StringFormat
        private class StringFormatContainer
        {
            private static StringFormat _drawFormat;

            static StringFormatContainer()
            {
                _drawFormat = new System.Drawing.StringFormat(0);
                _drawFormat.Alignment = StringAlignment.Near;
            }

            public static StringFormat Instance
            {
                get { return _drawFormat; }
            }
        }

        private int minHeight, yOffset, _fixedWidth;
    }
}
