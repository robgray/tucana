﻿using System;
using System.Drawing;

namespace AirtimeBilling.Web
{
    public class TextStyle : IDisposable, ICloneable
    {
        protected string _fontFamily;
        protected string _fontFileName;
        protected float _fontSize;
        protected FontStyle _fontStyle;
        protected StringAlignment _drawAlign;
        protected Color _color;
        protected Brush _brush;
        protected string _postScriptFamily;

        public TextStyle()
        {
            _fontSize = 10;
            _fontStyle = FontStyle.Regular;
        }

        public Color Color
        {
            get
            {
                return _color;
            }
            set
            {
                _color = value;
                Brush = new SolidBrush(value);
            }
        }

        public Brush Brush
        {
            get
            {
                return _brush;
            }
            set
            {
                if (_brush != null)
                    _brush.Dispose();
                _brush = value;
            }
        }

        public string Family
        {
            get
            {
                return _fontFamily;
            }
            set
            {
                _fontFamily = value;
            }
        }

        public string FontFileName
        {
            get
            {
                return _fontFileName;
            }
            set
            {
                _fontFileName = value;
            }
        }

        // Because Windows .NET can not render them properly
        public string PostScriptFamily
        {
            get
            {
                return _postScriptFamily;
            }
            set
            {
                _postScriptFamily = value;
            }
        }

        public float Size
        {
            get
            {
                return _fontSize;
            }

            set
            {
                _fontSize = value;
            }
        }

        public FontStyle Style
        {
            get
            {
                return _fontStyle;
            }

            set
            {
                _fontStyle = value;
            }
        }

        public void Dispose()
        {
            if (_brush != null)
                _brush.Dispose();
        }

        public object Clone()
        {
            TextStyle cloned = new TextStyle();
            cloned.Brush = (Brush)Brush.Clone();
            cloned.Color = Color;
            cloned.Family = Family;
            cloned.FontFileName = FontFileName;
            cloned.PostScriptFamily = PostScriptFamily;
            cloned.Size = Size;
            cloned.Style = Style;
            return cloned;
        }
    }
}
