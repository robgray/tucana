﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace AirtimeBilling.Web
{
    [DefaultProperty("Message")]
    [ToolboxData("<{0}:MessagePanel runat=server></{0}:MessagePanel>")]
    public class MessagePanel : Panel
    {
        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("Success")]
        [ReadOnly(false)]
        [Localizable(true)]
        public string MessageType
        {
            get
            {
                var message = ViewState["MessagePanelMessageType"] as String;
                return string.IsNullOrEmpty(message) ? "alert-success" : message;
            }
            set
            {
                ViewState["MessagePanelMessageType"] = value;
            }
        }

        public bool IncludeClose
        {
            get
            {
                var includeClose = ViewState["MessagePanelIncludeClose"] as String ?? "True";
                return bool.Parse(includeClose);
            }
            set 
            { 
                ViewState["MessagePanelIncludeClose"] = value.ToString(); 
            }
        }


        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("")]
        [Localizable(true)]
        public string Message
        {
            get
            {
                var message = ViewState["MessagePanelMessage"] as String;
                return string.IsNullOrEmpty(message) ? "" : message;
            }
            set
            {
                ViewState["MessagePanelMessage"] = value;
            }
        }

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            base.AddAttributesToRender(writer);
            // Add attributes to div for the class
            var css = string.Format("alert {0}", MessageType);
            if (!string.IsNullOrEmpty(CssClass)) css += " " + CssClass;
            writer.AddAttribute(HtmlTextWriterAttribute.Class, css);            
            if (string.IsNullOrEmpty(Message)) {
                writer.AddStyleAttribute(HtmlTextWriterStyle.Display, "none");
            }
        }

        protected override void RenderContents(HtmlTextWriter writer)
        {
            base.RenderContents(writer);
            if (IncludeClose)
                writer.Write("<a class=\"close\" data-dismiss=\"alert\" href=\"#\">×</a>{0}", Message);
            else
                writer.Write(Message);
            writer.BeginRender();
        }

        public void SetFailure(string message)
        {
            SetFailure(message, true);
        }

        public void SetFailure(string message, bool showCloseButton)
        {
            SetMessage("alert-error", message, showCloseButton);
        }

        public void SetSuccess(string message)
        {
            SetSuccess(message, true);
        }

        public void SetSuccess(string message, bool showCloseButton)
        {
            SetMessage("alert-success", message, showCloseButton);
        }

        public void SetWarning(string message)
        {
            SetWarning(message, true);
        }

        public void SetWarning(string message, bool showCloseButton)
        {
            SetMessage("alert-block", message, showCloseButton);
        }

        public void SetInformation(string message)
        {
            SetInformation(message, true);
        }

        public void SetInformation(string message, bool showCloseButton)
        {
            SetMessage("alert-info", message, showCloseButton);
        }

        protected void SetMessage(string type, string message, bool showCloseButton)
        {
            IncludeClose = showCloseButton;
            MessageType = type;
            Message = message;
        }

        public bool HasError
        {
            get { return MessageType == "alert-error";  }
        }

        public void Clear()
        {
            SetSuccess("");            
        }
    }

}
