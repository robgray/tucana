﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace AirtimeBilling.Web
{
    /// <summary>
    /// Provides helper functions for text/strings
    /// </summary>
	public static class Text
	{
		public static string UrlEncode(string s, char[] ignore)
		{
			StringBuilder sb = new StringBuilder();
			int i, x = s.Length;
			char c;
			for (i = 0; i < x; i++)
			{
				c = s[i];
				if (Char.IsLetterOrDigit(c) || c == '_' || c == '-' || (ignore != null && System.Array.IndexOf(ignore, c) != -1))
					sb.Append(c);
				else if (c == ' ')
					sb.Append('+');
				else
				{
					//string conv = c.ToString();
					//byte[] b = Encoding.ASCII.GetBytes(conv);
					sb.AppendFormat("%{0:X2}", (int)c);
				}
			}

			return sb.ToString();
		}

		public static string CreateValidFilenameFrom(string s)
		{
			// In 2.0, use System.IO.Path.GetInvalidFilenameChars()
			char[] invalid = new char[] { '*', '?', '/', '\\', ':' };

			char[] data = s.ToCharArray();
			for (int idx = 0; idx < data.Length; idx++)
				if (System.Array.IndexOf(invalid, data[idx]) != -1 || System.Array.IndexOf(Path.GetInvalidPathChars(), data[idx]) != -1)
					data[idx] = ' ';

			s = new string(data);

			// Remove consecutative whitespaces
			string result = null;
			while (s != result)
			{
				result = s;
				s = s.Replace("  ", " ");
			}

			return result;
		}

		public static string ImplodePairs(IDictionary<string, string> dictionary)
		{
			return Join(dictionary, "&", "=");
		}

		public static string Join(IDictionary<string, string> dictionary, string joinToken, string keyvalueToken)
		{
			StringBuilder sb = new StringBuilder();
			foreach (var pair in dictionary)
			{
				if (sb.Length > 0) sb.Append(joinToken);
				sb.Append(pair.Key + keyvalueToken + pair.Value);
			}

			return sb.ToString();
		}

		public static string JoinQueryString(NameValueCollection queryString, string joinToken, string keyvalueToken)
		{
			StringBuilder sb = new StringBuilder();
			foreach (string key in queryString.Keys)
			{
				if (sb.Length > 0) sb.Append(joinToken);
				sb.Append(key + keyvalueToken + queryString[key]);
			}

			return sb.ToString();
		}
	}
}
