﻿using System;

namespace AirtimeBilling.Web
{
    /// <summary>
    /// Summary description for BasePage
    /// </summary>
    public class BackOfficeBasePage : BasePage
    {        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            if (!Users.Current.IsInRole(Roles.BackOffice))
            {
                Response.Redirect(Users.Current.HomePage);
            }            
        }
    }
}