﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Text;

namespace AirtimeBilling.Web
{
    /// <summary>
    /// Summary description for ClientSideUtils
    /// </summary>
    public static class ClientSideUtils
    {
        public static void CreateMessageAlert(Page page, string message, string key)
        {
            message = message.EncodeJsString();
            string script = string.Format("<script type=\"text/javascript\">alert('{0}');</script>", message);

            if (!page.ClientScript.IsStartupScriptRegistered(key))
            {
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script);
            }
        }

        /// <summary>
        /// Encodes a string to be represented as a string literal. The format
        /// is essentially a JSON string.
        /// 
        /// The string returned includes outer quotes 
        /// Example Output: "Hello \"Rick\"!\r\nRock on"
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EncodeJsString(this string s, bool includeQuotes)
        {
            StringBuilder sb = new StringBuilder();
            if (includeQuotes) sb.Append("\"");
            foreach (char c in s)
            {
                switch (c)
                {
                    case '\"':
                        sb.Append("\\\"");
                        break;
                    case '\\':
                        sb.Append("\\\\");
                        break;
                    case '\b':
                        sb.Append("\\b");
                        break;
                    case '\f':
                        sb.Append("\\f");
                        break;
                    case '\n':
                        sb.Append("\\n");
                        break;
                    case '\r':
                        sb.Append("\\r");
                        break;
                    case '\t':
                        sb.Append("\\t");
                        break;
                    default:
                        int i = (int)c;
                        if (i < 32 || i > 127)
                        {
                            sb.AppendFormat("\\u{0:X04}", i);
                        }
                        else
                        {
                            sb.Append(c);
                        }
                        break;
                }
            }
            if (includeQuotes) sb.Append("\"");

            return sb.ToString();
        }

        public static string EncodeJsString(this string s)
        {
            return s.EncodeJsString(false);
        }
    }
}