﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Profile;

namespace RGSS.LandwideCMS.Gui
{
    public class ProfileCommon : ProfileBase 
    {        
        public static ProfileCommon Create(string username)
        {
            return (ProfileCommon)ProfileBase.Create(username);
        }

        public int AgentId
        {
            get 
            {
                return (int)GetPropertyValue("AgentId");
                //return (int)HttpContext.Current.Profile.GetPropertyValue("AgentId");
            }
            set
            {
                //HttpContext.Current.Profile.SetPropertyValue("AgentId", value);
                SetPropertyValue("AgentId", value);
            }
        }

        public int ContactId
        {
            get
            {
                //return (int) HttpContext.Current.Profile.GetPropertyValue("ContactId");
                return (int) GetPropertyValue("ContactId");
            }
            set
            {
                //HttpContext.Current.Profile.SetPropertyValue("ContactId", value);
                SetPropertyValue("ContactId", value);
            }
        }

    }
}
