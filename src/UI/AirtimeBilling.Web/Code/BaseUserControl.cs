﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Web
{
    public class BaseUserControl : UserControl
    {
        private ISession _currentSession;

        protected ISession CurrentSession
        {
            get
            {
                if (_currentSession == null)
                    _currentSession = new CurrentSession();

                return _currentSession;
            }
        }

        protected INavigationController NavigationController
        {
            get
            {
                var page = this.Page as BasePage;
                if (page == null) {
                    throw new Exception("Cannot access navigation controller");
                }

                return page.NavigationController;

            }
        }

        public IScrubbingService ScrubbingService
        {
            get { return ServiceFactory.GetService<IScrubbingService>();  }
        }

        protected IContractService ContractService
        {
            get { return ServiceFactory.GetService<IContractService>(); }
        }

        protected INetworkService NetworkService
        {
            get { return ServiceFactory.GetService<INetworkService>(); }
        }

        protected IActivityLoggingService ActivityLoggingService
        {
            get { return ServiceFactory.GetService<IActivityLoggingService>(); }
        }

        protected IAccountService AccountService
        {
            get { return ServiceFactory.GetService<IAccountService>(); }
        }

        protected IAgentService AgentService
        {
            get { return ServiceFactory.GetService<IAgentService>(); }
        }

        protected ILogReaderService LogReaderService
        {
            get { return ServiceFactory.GetService<ILogReaderService>(); }
        }

        protected IAirtimeProviderService AirtimeProviderService
        {
            get { return ServiceFactory.GetService<IAirtimeProviderService>(); }
        }

        protected ICompanyService CompanyService
        {
            get { return ServiceFactory.GetService<ICompanyService>(); }
        }

        protected IContactService ContactService
        {
            get { return ServiceFactory.GetService<IContactService>(); }
        }
        
        protected IPlanService PlanService
        {
            get { return ServiceFactory.GetService<IPlanService>(); }
        }

        protected IUserService UserService
        {
            get { return ServiceFactory.GetService<IUserService>(); }
        }

        protected IInvoicingService InvoicingService
        {
            get { return ServiceFactory.GetService<IInvoicingService>(); }
        }

        protected INoteService NoteService
        {
            get { return ServiceFactory.GetService<INoteService>();  }
        }

        protected string GetFullPath(string virtualPath)
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute(virtualPath);
        }

        protected void ClearInputs(ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls) {
                if (ctrl is TextBox)
                    ((TextBox) ctrl).Text = string.Empty;
                else if (ctrl is DropDownList)
                    ((DropDownList) ctrl).ClearSelection();
                else if (ctrl is CheckBox)
                    ((CheckBox) ctrl).Checked = false;
                else if (ctrl is Label)
                    ((Label) ctrl).Text = string.Empty;

                ClearInputs(ctrl.Controls);
            }
        }
    }
}
