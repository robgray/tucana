﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace AirtimeBilling.Web
{
    public class NavigationController : INavigationController 
    {        
        public void EditAgent(int agentId)
        {
            Response.Redirect(GetEditAgentLink(agentId));
        }

        public void AgentSearch()
        {
            Response.Redirect(GetAgentSearchLink());
        }

        public void AccountSearch()
        {
            Response.Redirect(GetAccountSearchLink(), false);
        }

        public void ContactSearch()
        {
            Response.Redirect(GetContactSearchLink());
        }

        public void PlanSearch()
        {
            Response.Redirect(GetPlanSearchLink());
        }

        public void NetworkSearch()
        {
            Response.Redirect(GetNetworkSearchLink());
        }

        public void ContractSearch()
        {
            Response.Redirect(GetContractSearchLink());
        }

        public void NewAgent()
        {
            Response.Redirect(GetNewAgentLink());
        }

        public void EditCompany(int companyId)
        {
            Response.Redirect(GetEditCompanyLink(companyId));
        }

        public void EditContact(int contactId)
        {
            Response.Redirect(GetEditContactLink(contactId));
        }

        public void EditAccount(int accountId)
        {
            Response.Redirect(GetEditAccountLink(accountId));
        }

        public void EditNetwork(int networkId)
        {
            Response.Redirect(GetEditNetworkLink(networkId));
        }

        public void EditPlan(int planId)
        {
            Response.Redirect(GetEditPlanLink(planId));
        }

        public void CompanySearch()
        {
            Response.Redirect(GetCompanySearchLink());
        }

        public void DeletePlan(int planId)
        {
            Response.Redirect(GetDeletePlanLink(planId));
        }

        public void NewPlan(int networkId)
        {
            Response.Redirect(GetNewPlanLink(networkId));
        }

        public void NewCompany()
        {
            Response.Redirect(GetNewCompanyLink());
        }

        public void NewContact()
        {
            Response.Redirect(GetNewContactLink());
        }

        public void NewNetwork()
        {
            Response.Redirect(GetNewNetworkLink());
        }

        public void NewAccount()
        {
            Response.Redirect(GetNewAccountLink());
        }

        public void NewAccount(int masterAccountId)
        {
            Response.Redirect(GetNewAccountLink(masterAccountId));
        }

        public void InvoiceRun()
        {
            Response.Redirect(GetInvoiceRunLink());
        }

        public void InvoiceSearch()
        {
            Response.Redirect(GetInvoiceSearchLink());
        }

        public void CustomFees()
        {
            Response.Redirect(GetCustomFeesLink());
        }

        public void NewContract()
        {
            Response.Redirect(GetNewContractLink());
        }

        public void Support()
        {
            Response.Redirect(GetSupportLink());
        }

        public void UserList()
        {
            Response.Redirect(GetUserListLink());
        }

        public void SystemSettings()
        {
            Response.Redirect(GetSystemSettingsLink());
        }

        public void LogViewer()
        {
            Response.Redirect(GetLogViewerLink());
        }

        public void About()
        {
            Response.Redirect(GetAboutLink());
        }

        public string GetUserListLink()
        {
            return GetFullPath("~/Admin/UserList.aspx");
        }

        public string GetSystemSettingsLink()
        {
            return GetFullPath("~/Admin/SystemSettings.aspx");
        }

        public string GetLogViewerLink()
        {
            return GetFullPath("~/Admin/LogViewer.aspx");
        }

        public string GetAboutLink()
        {
            return GetFullPath("~/Admin/About.aspx");
        }

        public string GetSupportLink()
        {
            return GetFullPath("~/Support/Support.aspx");
        }

        public string GetNewContractLink()
        {
            return GetFullPath("~/Contracts/SelectAccount.aspx");
        }

        public string GetCustomFeesLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/CustomFees.aspx");
        }

        public string GetInvoiceRunLink()
        {
            return GetFullPath("~/BackOffice/Invoicing/InvoiceRun.aspx");
        }

        public string GetInvoiceSearchLink()
        {
            return GetFullPath("~/BackOffice/Invoicing/Search.aspx");
        }

        public string GetAgentSearchLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Agent/Search.aspx");
        }

        public string GetAccountSearchLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Account/Search.aspx");
        }

        public string GetContactSearchLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Contact/Search.aspx");
        }

        public string GetCompanySearchLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Company/Search.aspx");
        }

        public string GetPlanSearchLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Plan/Search.aspx");
        }

        public string GetNetworkSearchLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Network/Search.aspx");
        }

        public string GetContractSearchLink()
        {
            return GetFullPath("~/Contracts/Search.aspx");
        }

        public string GetEditAgentLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Agent/Detail.aspx");
        }

        public string GetEditAgentLink(int agentId)
        {
            var builder = new UriBuilder(GetEditAgentLink())
            {
                Query = string.Format("Id={0}", agentId)
            };

            return builder.Uri.ToString();
        }

        public string GetEditCompanyLink(int companyId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Company/Detail.aspx"))
            {
                Query = string.Format("Id={0}", companyId)
            };

            return builder.Uri.ToString();
        }

        public string GetEditContactLink(int contactId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Contact/Detail.aspx"))
            {
                Query = string.Format("Id={0}", contactId)
            };

            return builder.Uri.ToString();
        }

        public string GetEditAccountLink(int accountId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Account/Detail.aspx"))
            {
                Query = string.Format("Id={0}", accountId)
            };

            return builder.Uri.ToString();
        }

        public string GetEditNetworkLink(int networkId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Network/Detail.aspx"))
            {
                Query = string.Format("Id={0}", networkId)
            };

            return builder.Uri.ToString();
        }

        public string GetEditPlanLink(int planId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Plan/Detail.aspx"))
            {
                Query = string.Format("Id={0}", planId)
            };

            return builder.Uri.ToString();
        }

        public string GetDeletePlanLink(int planId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Plan/Delete.aspx"))
            {
                Query = string.Format("Id={0}", planId)
            };

            return builder.Uri.ToString();
        }

        public string GetNewAccountLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Account/Detail.aspx");
        }

        public string GetNewAccountLink(int masterAccountId)
        {            
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Account/New.aspx"))
                              {
                                  Query = string.Format("master={0}", masterAccountId)
                              };
            return builder.Uri.ToString();
        }

        public string GetNewAgentLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Agent/Detail.aspx");
        }

        public string GetNewPlanLink(int networkId)
        {
            var builder = new UriBuilder(GetFullPath("~/BackOffice/Maintenance/Plan/Add.aspx"))
            {
                Query = string.Format("networkId={0}", networkId)
            };

            return builder.Uri.ToString();
        }

        public string GetNewCompanyLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Company/Detail.aspx");
        }

        public string GetNewContactLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Contact/Detail.aspx");
        }

        public string GetNewNetworkLink()
        {
            return GetFullPath("~/BackOffice/Maintenance/Network/Detail.aspx");
        }

        protected string GetFullPath(string virtualPath)
        {
            
            var fullPath = Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute(virtualPath);
            /*
            if (!Request.IsLocalNetwork() && ConfigItems.UseSSL) {
                fullPath = fullPath.Replace("HTTP:", "HTTPS:");
                fullPath = fullPath.Replace("http:", "https:");
            } 
            else if (Request.IsLocalNetwork() && fullPath.StartsWith("HTTPS:"))
                fullPath = fullPath.Replace("HTTPS:", "HTTP:");
            else if (Request.IsLocalNetwork() && fullPath.StartsWith("https:"))
                fullPath = fullPath.Replace("https:", "http:");
            */
            return fullPath;
        }

        protected HttpRequest Request
        {
            get { return HttpContext.Current.Request; }
        }

        protected HttpResponse Response
        {
            get  { return HttpContext.Current.Response; }
        }

        public string GetThankyouLink()
        {
            return GetFullPath("~/Contracts/Thankyou.aspx");
        }

        public void Thankyou()
        {
            Response.Redirect(GetThankyouLink());
        }
    }
}
