﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Web
{
    public class BasePage : Page
    {
        private ISession _currentSession;

        protected ISession CurrentSession
        {
            get
            {
                if (_currentSession == null)
                    _currentSession = new CurrentSession();

                return _currentSession;
            }
        }

        protected IScrubbingService ScrubbingService
        {
            get { return ServiceFactory.GetService<IScrubbingService>(); }
        }

        protected INoteService NoteService
        {
            get { return ServiceFactory.GetService<INoteService>();  }
        }

        protected IContractService ContractService
        {
            get { return ServiceFactory.GetService<IContractService>(); }
        }

        protected INetworkService NetworkService
        {
            get { return ServiceFactory.GetService<INetworkService>(); }
        }

        protected IActivityLoggingService ActivityLoggingService
        {
            get { return ServiceFactory.GetService<IActivityLoggingService>(); }
        }

        protected ILogReaderService LogReaderService
        {
            get { return ServiceFactory.GetService<ILogReaderService>();  }
        }

        protected IAccountService AccountService
        {
            get { return ServiceFactory.GetService<IAccountService>(); }
        }

        protected IAgentService AgentService
        {
            get { return ServiceFactory.GetService<IAgentService>(); }
        }

        protected IAirtimeProviderService AirtimeProviderService
        {
            get { return ServiceFactory.GetService<IAirtimeProviderService>(); }
        }

        protected ICompanyService CompanyService
        {
            get { return ServiceFactory.GetService<ICompanyService>(); }
        }

        protected IContactService ContactService
        {
            get { return ServiceFactory.GetService<IContactService>(); }
        }        

        protected IPlanService PlanService
        {
            get { return ServiceFactory.GetService<IPlanService>(); }
        }

        protected IUserService UserService
        {
            get { return ServiceFactory.GetService<IUserService>(); }
        }

        protected IInvoicingService InvoicingService
        {
            get { return ServiceFactory.GetService<IInvoicingService>(); }
        }

        protected string GetFullPath(string virtualPath)
        {
            return Request.Url.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute(virtualPath);
        }

        protected INavigationController _navigationController;
        public INavigationController NavigationController
        {
            get 
            { 
                if (_navigationController == null)
                    _navigationController = new NavigationController();

                return _navigationController;                                    
            }
        }

        protected void ClearInputs(ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                else if (ctrl is DropDownList)
                    ((DropDownList)ctrl).ClearSelection();
                else if (ctrl is CheckBox)
                    ((CheckBox)ctrl).Checked = false;

                ClearInputs(ctrl.Controls);
            }
        }

    }
}
