﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirtimeBilling.Common;

namespace AirtimeBilling.Web
{
    public static class RequestHelper
    {
        /// <summary>
        /// Determines if the request is from a computer on the LAN
        /// </summary>
        public static bool IsLocalNetwork(this HttpRequest request)
        {
            return request.IsLocal || request.UserHostAddress.StartsWith(ConfigItems.LocalNetworkIPRange);
        }
    
    }
}
