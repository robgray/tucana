﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirtimeBilling.Web
{
    /// <summary>
    /// Summary description for Security
    /// </summary>
    public static class Security
    {
        public static void RedirectToWelcomeIfNotInRole(Roles role)
        {
            var user = Users.Current;
            if (!Users.IsInRole(user, role))
            {             
                HttpContext.Current.Response.Redirect(user.HomePage);              
            }
        }
    }
}