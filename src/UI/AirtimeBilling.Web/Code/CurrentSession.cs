﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public class CurrentSession : ISession
    {
        #region ISession Members

        public bool SearchReadOnly
        {
            get
            {
                var searchReadOnly = GetSessionObject("SearchReadOnly");
                if (searchReadOnly == null) return false;
                return (bool) searchReadOnly;
                           
            }
            set
            {
                SetSessionObject("SearchReadOnly", value);
            }
        }

        public string ExceptionMessage
        {
            get
            {
                return GetSessionObject("Error") as string;
            }
            set
            {
                SetSessionObject("Error", value);
            }
        }

        public Account Account
        {
            get
            {
                return GetSessionObject("Account") as Account;
            }
            set
            {
                SetSessionObject("Account", value);
            }
        }
        
        public MasterAccount MasterAccount
        {
            get
            {
                return GetSessionObject("MasterAccount") as MasterAccount;
            }
            set
            {
                SetSessionObject("MasterAccount", value);
            }
        }

        public void Clear()
        {
            HttpContext.Current.Session.Clear();
        }

        #endregion

        private static T GetSessionObject<T>(string key)
        {
            return (T)HttpContext.Current.Session[key];
        }

        private static object GetSessionObject(string key)
        {
            return HttpContext.Current.Session[key];
        }

        private static void SetSessionObject(string key, object value)
        {
            HttpContext.Current.Session[key] = value;
        }
    }
}
