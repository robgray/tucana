﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Web
{
    /// <summary>
    /// We use this class to grab all the things we want to display in 
    /// our search grid, such as contact name, company name.
    /// </summary>
    public class AccountSearchEntity : Account
    {
        private AccountSearchEntity(int accountId)
            : base(accountId)
        {
            ContactName = string.Empty;
            CompanyName = string.Empty;
        }

        public static AccountSearchEntity GetAcountSearchEntity(Account account)
        {
            if (account.Id == null)
            {
                throw new InvalidOperationException("Cannot create from a new Account");
            }

            var entity = new AccountSearchEntity(account.Id.Value)
            {
                AccountNumber = account.AccountNumber,
                AccountName = account.AccountName,
                BillingAddressType = account.BillingAddressType,
                BillingMethod = account.BillingMethod,
                ContactId = account.ContactId,                
                ContactRole = account.ContactRole,
                EmailBill = account.EmailBill,
                EmailBillDataFile = account.EmailBillDataFile,
                HasRequestedInvoicing = false,
                IsInvoiceRoot = account.IsInvoiceRoot,
                Password = account.Password,
                PostBill = account.PostBill,
                IsInternational = account.IsInternational,
                AmountPaid = account.AmountPaid,
                PreviousBill = account.PreviousBill 
            };

            var contact = ServiceFactory.GetService<IContactService>().GetContact(account.ContactId);
            if (contact != null)
            {
                entity.ContactName = contact.Name;
            }

            // Only master accounts can have companies associated with them
            if (account is MasterAccount)
            {
                var master = account as MasterAccount;
                if (master.CompanyId.HasValue)
                {
                    var company = ServiceFactory.GetService<ICompanyService>().GetCompany(master.CompanyId.Value);
                    if (company != null)
                    {
                        entity.CompanyName = company.CompanyName;
                        entity.CompanyId = master.CompanyId.Value;
                    }
                }
            }

            if (account is SubAccount)
            {
                var sub = account as SubAccount;
                entity.MasterAccountId = sub.MasterAccountId;
            }

            return entity;
        }

        public string ContactName { get; set; }

        public string CompanyName { get; set; }

        public int CompanyId { get; set; }

        public int MasterAccountId { get; set; }
    }
}
