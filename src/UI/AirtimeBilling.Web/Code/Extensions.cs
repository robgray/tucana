﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public static class Extensions
    {
        /// <summary>
        /// Takes a camel case string (eg ApplicationPending) and places a string before each 
        /// Capital inside the word (result eg Application Pending).
        /// </summary>
        /// <param name="s"></param>
        /// <param name="?"></param>
        /// <returns></returns>
        public static string SeparateCamelCase(this string s)
        {
            for (int index = 1; index < s.Length; index++)
            {
                string lower = s.Substring(index, 1).ToLower();
                if (!s.Substring(index, 1).Equals(lower) && !s.Substring(index - 1, 1).Equals(" "))
                {
                    s = s.Insert(index, " ");
                }

            }
            return s;
        }

        public static bool IsInRole(this User user, Roles role)
        {
            return Users.IsInRole(user, role);
        }        
    }
}