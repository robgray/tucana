﻿using System.Reflection;

namespace AirtimeBilling.Web
{
    /// <summary>
    /// Maintains the Web Site Version.
    /// For #001-18
    /// </summary>
    public static class Version
    {
        public static string GetCopyrightAndVersionNumber()
        {
            var  versionNumber = Assembly.GetExecutingAssembly().GetName().Version;

            return string.Format("&copy; 2010 Sharp Studios <br/> v{0} ", versionNumber);
        }

		public static string GetVersionNumber()
		{
			var versionNumber = Assembly.GetExecutingAssembly().GetName().Version;

			return string.Format("v{0} ", versionNumber);
		}
    }
}