﻿using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using AirtimeBilling.Core.Entities;

namespace AirtimeBilling.Web
{
    public class Users
    {
        public static User GetUser(string username)
        {
            if (username == "robgray")
                return new SupportUser
                           {
                               Email = "robert.gray@com.au",
                               IsActive = true,
                               IsAuthenticated = true,
                               Name = "Rob Gray",
                               Username = "robgray"
                           };

            var aspUser = Membership.GetUser(username);
            if (aspUser == null) return null;

            var profile = WebProfile.GetProfile(username);
            var agentId = profile.AgentId;
            var contactId = profile.ContactId;

            profile.Save();

            if (agentId > 0)
            {
                // It's an agent entity
                var user = new AgentUser()
                {
                    AgentId = agentId,
                    Email = aspUser.Email,
                    IsAuthenticated = true,
                    Name = aspUser.UserName,
                    Username = aspUser.UserName,
                    IsActive = aspUser.IsApproved && !aspUser.IsLockedOut,
                };
                return user;
            }
            if (contactId > 0)
            {
                var user = new ContactUser()
                {
                    ContactId = contactId,
                    Email = aspUser.Email,
                    IsAuthenticated = true,
                    IsActive = aspUser.IsApproved && !aspUser.IsLockedOut,
                    Name = aspUser.UserName,
                    Username = aspUser.UserName
                };

                return user;
            }
            else
            {
                User user = new User()
                {
                    Email = aspUser.Email,
                    Username = aspUser.UserName,
                    Name = aspUser.UserName,
                    IsActive = aspUser.IsApproved && !aspUser.IsLockedOut,
                    IsAuthenticated = true
                };

                return user;
            }
        }

        public static bool IsInRole(string username, Roles role)
        {
            User user = GetUser(username);
            if (user != null)
            {
                return IsInRole(user, role);
            }
            return false;
        }

        public static bool IsInRole(User user, Roles role)
        {
            if (user == null) return false;
            
            if (user is AgentUser) {
                return role == Roles.Agent;
            }
            if (user is ContactUser) {
                return role == Roles.Customer;
            }
            if (user is SupportUser) {
                if (role == Roles.BackOffice || role == Roles.Support)
                    return true;
                return false;
            }
            return role == Roles.BackOffice;            
        }

        public static IList<User> GetAllUsers()
        {
            MembershipUserCollection aspUsers = Membership.GetAllUsers();
            IList<User> users = new List<User>();
            foreach (MembershipUser aspUser in aspUsers)
            {
                users.Add(GetUser(aspUser.UserName));
            }
            return users;
        }

        public static IList<User> GetAllAuthorisedUsers()
        {
            MembershipUserCollection aspUsers = Membership.GetAllUsers();
            IList<User> users = new List<User>();
            foreach (MembershipUser aspUser in aspUsers)
            {
                if (aspUser.IsApproved)
                    users.Add(GetUser(aspUser.UserName));
            }
            return users;
        }

        public static IList<User> FindUsersByName(string username)
        {
            MembershipUserCollection aspUsers = Membership.FindUsersByName(username);
            IList<User> users = new List<User>();
            foreach (MembershipUser aspUser in aspUsers)
            {
                users.Add(GetUser(aspUser.UserName));
            }
            return users;
        }

        public static IList<User> FindUsersByEmail(string email)
        {
            MembershipUserCollection aspUsers = Membership.FindUsersByEmail(email);
            IList<User> users = new List<User>();
            foreach (MembershipUser aspUser in aspUsers)
            {
                users.Add(GetUser(aspUser.UserName));
            }
            return users;
        }

        public static void ChangePassword(User user, string password)
        {
            MembershipUser aspUser = Membership.GetUser(user.Username);
            if (aspUser != null)
            {
                aspUser.ChangePassword(aspUser.GetPassword(), password);
            }
        }

        public static void Update(User user)
        {
            MembershipUser aspUser = Membership.GetUser(user.Username);
            if (aspUser == null) return;
            WebProfile profile = WebProfile.GetProfile(user.Username);
            if (profile == null)
            {
                profile = new WebProfile();
            }

            if (user is AgentUser)
            {
                AgentUser agent = (AgentUser)user;
                profile.AgentId = agent.AgentId;
            }
            else
            {
                profile.AgentId = 0;
            }

            if (user is ContactUser)
            {
                ContactUser contact = (ContactUser)user;
                profile.ContactId = contact.ContactId;
            }
            else
            {
                profile.ContactId = 0;
            }

            aspUser.IsApproved = user.IsActive;
            if (user.IsActive)
            {
                aspUser.UnlockUser();
            }

            profile.Save();
            Membership.UpdateUser(aspUser);
        }

        public static void Delete(string username)
        {
            Membership.DeleteUser(username);
        }

        public static string ResetPassword(string username)
        {
            MembershipUser user = Membership.GetUser(username);
            if (user != null)
            {
                return user.ResetPassword();
            }
            return null;
        }

        public static User Current
        {
            get { return Users.GetUser(HttpContext.Current.User.Identity.Name); }
        }
    }
}