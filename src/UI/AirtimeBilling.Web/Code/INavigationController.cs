﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AirtimeBilling.Web
{
    public interface INavigationController
    {
        void AgentSearch();

        void AccountSearch();

        void ContactSearch();

        void CompanySearch();

        void PlanSearch();

        void NetworkSearch();

        void ContractSearch();
        
        void EditAgent(int agentId);

        void EditCompany(int companyId);

        void EditContact(int contactId);

        void EditAccount(int accountId);

        void EditNetwork(int networkId);

        void EditPlan(int planId);

        void DeletePlan(int planId);

        void NewAccount();

        void NewAccount(int masterAccountId);

        void NewAgent();

        void NewPlan(int networkId);

        void NewCompany();

        void NewContact();

        void NewNetwork();

        void CustomFees();

        void InvoiceRun();

        void InvoiceSearch();

        void NewContract();

        void Support();

        void UserList();

        void SystemSettings();

        void LogViewer();

        void About();

        void Thankyou();

        string GetUserListLink();

        string GetSystemSettingsLink();

        string GetLogViewerLink();

        string GetAboutLink();

        string GetSupportLink();

        string GetNewContractLink();

        string GetCustomFeesLink();

        string GetInvoiceRunLink();

        string GetInvoiceSearchLink();

        string GetAgentSearchLink();

        string GetAccountSearchLink();

        string GetContactSearchLink();

        string GetCompanySearchLink();

        string GetPlanSearchLink();

        string GetNetworkSearchLink();

        string GetContractSearchLink();

        string GetEditAgentLink();

        string GetEditAgentLink(int agentId);

        string GetEditCompanyLink(int companyId);

        string GetEditContactLink(int contactId);

        string GetEditAccountLink(int accountId);

        string GetEditNetworkLink(int networkId);

        string GetEditPlanLink(int planId);

        string GetDeletePlanLink(int planId);

        string GetNewAccountLink();

        string GetNewAccountLink(int masterAccountId);

        string GetNewAgentLink();

        string GetNewPlanLink(int networkId);

        string GetNewCompanyLink();

        string GetNewContactLink();

        string GetNewNetworkLink();

        string GetThankyouLink();
    }
}
