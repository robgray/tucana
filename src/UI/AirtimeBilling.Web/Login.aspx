﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="AirtimeBilling.Web.Login" Codebehind="Login.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Airtime Billing</title>        
    <link href="style/layout.css" rel="Stylesheet" type="text/css" />  
    <link href="style/login.css" rel="Stylesheet" type="text/css" />    
</head>
<body>        
<table>
    <tbody>
        <tr>
            <td valign="middle">
                <div class="Login">
                    <form id="loginForm" runat="server">
                    <div style="width:100%;text-align:center">
                        <img src="/images/airtimebilling_logo2A.jpg" />
                    </div>
                    <asp:Login ID="MainLogin" runat="server" 
                        onauthenticate="MainLogin_Authenticate" EnableTheming="True" PasswordLabelText="Password"
                        TextLayout="TextOnTop" TitleText="" UserNameLabelText="Username" 
                        VisibleWhenLoggedIn="False" LoginButtonText="Login" 
                        DisplayRememberMe="False">                
                        <TitleTextStyle CssClass="Title" />
                        <LoginButtonStyle CssClass="loginButton" />
                        <LabelStyle CssClass="loginHeading" />
                    </asp:Login>       
                    </form>
                </div> 
            </td>
        </tr>        
    </tbody>
</table>    
</body>
</html>