﻿using System.Collections.Generic;
using NUnit.Framework;
using Moq;

namespace AirtimeBilling.Import.Tests
{
    /// <summary>
    /// Summary description for CallDataImporterTests
    /// </summary>
    [TestFixture]
    public class CallDataImporterTests
    {     
        Mock<ICallRepository> callRepository = new Mock<ICallRepository>();
        Mock<IContractRepository> contractRepository = new Mock<IContractRepository>();
        Mock<ITariffRepository> tariffRepository = new Mock<ITariffRepository>();
        Mock<IVizadaDataRepository> vizadaRepository = new Mock<IVizadaDataRepository>();
        Mock<IAirtimeProviderRepository> airtimeRepository = new Mock<IAirtimeProviderRepository>();
       
        [Test]
        public void GetDataProviderDescripion()
        {
            airtimeRepository
                .Expect(a => a.FindAll())
                .Returns((new List<AirtimeProvider>
                              {
                                  new AirtimeProvider { AirtimeProviderId = 1, ImporterFullName="AirtimeBilling.Import.VizadaCallDataImporter", Name="Vizada"},
                                  new AirtimeProvider { AirtimeProviderId = 2, ImporterFullName="AirtimeBilling.Import.SatComCallDataImporter", Name="SatCom"}
                              }));


            CallDataImporter importer = new VizadaCallDataImporter(callRepository.Object,
                                                                   contractRepository.Object, tariffRepository.Object,
                                                                   vizadaRepository.Object, airtimeRepository.Object);

            var description = importer.GetDataProviderDescription();
            Assert.AreEqual("Vizada", description);
                
        }
    }
}
