﻿using AirtimeBilling.Core.Enums;
using NUnit.Framework;

namespace AirtimeBilling.Import.Tests
{
    /// <summary>
    /// Summary description for UnitOfTimeTests
    /// </summary>
    [TestFixture]
    public class UnitOfTimeTests
    {              
        [Test]
        public void Extra_unit_of_time_when_time_less_than_complete_multiple_of_block()
        {
            int secondsPerBlock = 30;
            double callTimeInDecimalMinutes = 3.25; // (3 minutes 15 seconds)

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(7, (int) units, "Wrong Units of Time");            
        }

        [Test]
        public void Same_unit_of_time_as_number_of_blocks_when_on_time_equals_block_multiple()
        {
            int secondsPerBlock = 15;
            double callTimeInDecimalMinutes = 0.75; // (45 seconds)

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(3, (int) units, "Wrong Units of Time");            
        }

        [Test]
        public void Charges_a_one_unit_of_time_when_time_less_than_block()
        {
            int secondsPerBlock = 30;
            double callTimeInDecimalMinutes = 0.25; // (15 seconds)

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(1, (int) units, "Wrong Units of Time");            
        }

        [Test]
        public void Zero_call_time_calculates_no_units_of_time()
        {
            int secondsPerBlock = 30;
            double callTimeInDecimalMinutes = 0;

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(0, (int) units, "Wrong Units of Time");            
        }

        [Test]
        public void Three_quarters_minute_sixty_seconds_block()
        {
            int secondsPerBlock = 60;
            double callTimeInDecimalMinutes = 14.50; 

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(15, (int) units, "Wrong Units of Time"); 
        }


        //[Test]
        //public void UnitsOfTimeEqualsOneMinute()
        //{
        //    int secondsPerBlock = 60;
        //    double callTimeInDecimalMinutes = 3.25; // (3 minutes 15 seconds)

        //    var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
        //    var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
        //    Assert.AreEqual(4, units, "Wrong Units of Time");            
        //}

        //[Test]
        //public void UnitsOfTimeGreaterThanOneMinute()
        //{
        //    int secondsPerBlock = 120;
        //    double callTimeInDecimalMinutes = 3.25; // (3 minutes 15 seconds)

        //    var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
        //    var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
        //    Assert.AreEqual(2, units, "Wrong Units of Time");            
        //}

        [Test]
        public void One_minute_call_with_ten_second_blocks_gives_6_blocks()
        {
            int secondsPerBlock = 10;
            double callTimeInDecimalMinutes = 1.00; 

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(6, (int) units, "Wrong Units of Time"); 
        }       

        [Test]
        public void Thirty_second_call_with_10_second_interval_gives_3_blocks()
        {
            int secondsPerBlock = 10;
            double callTimeInDecimalMinutes = 0.50;

            var calc = new UnitsOfTimeCalculator(CallVolumeUnit.Time);
            var units = calc.Calculate(secondsPerBlock, callTimeInDecimalMinutes);
            Assert.AreEqual(3, (int) units, "Wrong Units of Time"); 
        }


    }
}
