﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace RGSS.LandwideCMS.Import.Tests
{
    /// <summary>
    /// Summary description for ImportManagerTests
    /// </summary>
    [TestClass]
    public class ImportManagerTests
    {
        public ImportManagerTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void ProcessFile()
        {
            var fileName = @"c:\temp\calldata\myfile.test";

            var manager = new ImporterManager();
            Mock<ICallDataImporter> importer = new Mock<ICallDataImporter>();

            importer
                .Expect(i => i.RegisteredExtensions())
                .Returns(new List<string> { "test" });

            importer
                .Expect(i => i.Import(fileName))
                .Returns(true);

            manager.Register(importer.Object);

            
            var successful = manager.Process(fileName);
            Assert.IsTrue(successful);

        }

        [TestMethod]
        public void ProcessFileNoRegisteredImporter()
        {
            var fileName = @"c:\temp\calldata\myfile.txt";

            var manager = new ImporterManager();
            Mock<ICallDataImporter> importer = new Mock<ICallDataImporter>();

            importer
                .Expect(i => i.RegisteredExtensions())
                .Returns(new List<string> { "test" });

            importer
                .Expect(i => i.Import(fileName))
                .Returns(true);
            
            manager.Register(importer.Object);

            
            var successful = manager.Process(fileName);
            Assert.IsFalse(successful);

        }

        [TestMethod]
        public void ProcessFileNoImporter()
        {
            var fileName = @"c:\temp\calldata\myfile.test";

            var manager = new ImporterManager();

            var successful = manager.Process(fileName);
            Assert.IsFalse(successful);

        }


    }
}
