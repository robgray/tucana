﻿using NUnit.Framework;

namespace AirtimeBilling.Common.Test
{
    /// <summary>
    /// Summary description for StringExtensionsFixture
    /// </summary>
    [TestFixture]
    public class StringExtensionsFixture
    {
        [Test]
        public void ToDecimalCurrency_converts_from_currency_formatted_string()
        {
            const string number = "$1,000.00";

            decimal convertedNumber = number.ToDecimalCurrency();

            Assert.AreEqual(1000.00M, convertedNumber);
        }
    }
}
