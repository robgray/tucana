﻿using NUnit.Framework;

namespace AirtimeBilling.Common.Test
{
    /// <summary>
    /// Summary description for EnumHelperTests
    /// </summary>
    [TestFixture]
    public class EnumHelperTests
    {              
        private enum Tests
        {
            [EnumDescription("Test One")]
            Test1,
            [EnumDescription("Test Two")]
            Test2,
            [EnumDescription("Test Three")]
            Test3,
            [EnumDescription("Test Four")]
            Test4
        }

        [Test]
        public void Get_string_representation_of_enum()
        {
            var value = Enum<Tests>.GetDescription(Tests.Test1);
            Assert.AreEqual("Test One", value);
        }

        [Test]
        public void Get_enum_given_a_description()
        {
            var status = Enum<Tests>.GetEnumFromDescription("Test One");
            Assert.AreEqual(Tests.Test1, status);
        }

        [Test]
        public void Retrieve_descriptions_of_all_members_in_enumeration()
        {
            var value = Enum<Tests>.GetAllDescriptions();
            Assert.AreEqual(4, value.Count);
            Assert.AreEqual("Test One", value[0]);
            Assert.AreEqual("Test Two", value[1]);
            Assert.AreEqual("Test Three", value[2]);
            Assert.AreEqual("Test Four", value[3]);
        }

        [Test]
        public void Convert_string_to_enum_value()
        {
            var value = Enum<Tests>.Parse("Test1");
            Assert.AreEqual(Tests.Test1, value);
        }

        [Test]
        public void Get_description_using_extension_method()
        {
            var value = Tests.Test1.GetDescription();
            Assert.AreEqual("Test One", value);
        }
    }
}
