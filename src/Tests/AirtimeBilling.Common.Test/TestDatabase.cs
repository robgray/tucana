﻿using System.Data;
using System.Data.SqlClient;

namespace AirtimeBilling.Common.Test
{
    public static class TestDatabase
    {
        /// <summary>
        /// Populates the test database
        /// Only use this for repository testing!
        /// Otherwise Repositories should be mocked.
        /// </summary>
        public static void Populate()
        {
            using (SqlConnection conn = new SqlConnection(ConfigItems.ConnectionString))
            {
                string filename = "";
                string sql = System.IO.File.ReadAllText(filename);
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;

                conn.Open();
                cmd.ExecuteNonQuery();
            }

        }
    }
}