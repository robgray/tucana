﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using NUnit.Framework;

namespace AirtimeBilling.Logic.Tests
{
    [TestFixture]
    public class InvoiceTests
    {
        [Test]
        public void HasContractBeenClosedInThisInvoicePeriod_returns_true_if_closed_in_invoice_period()
        {
            var invoice = new Invoice {FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1)};
            var contract = new Contract(1, DateTime.Now.AddMonths(-6))
                                    {
                                        ContractStatus = ContractStatus.Closed,
                                        StatusChangedDate = invoice.FromDate.AddDays(10)
                                    };

            var hasClosed = invoice.HasContractBeenClosedInThisInvoicePeriod(contract);

            Assert.IsTrue(hasClosed);
        }

        [Test]
        public void HasContractBeenClosedInThisInvoicePeriod_returns_false_if_closed_before_invoice_period()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1) };
            Contract contract = new Contract(1, DateTime.Now.AddMonths(-6))
            {
                ContractStatus = ContractStatus.Closed,
                StatusChangedDate = invoice.FromDate.AddDays(-10)
            };

            var hasClosed = invoice.HasContractBeenClosedInThisInvoicePeriod(contract);

            Assert.IsFalse(hasClosed);
        }

        [Test]
        public void HasContractBeenClosedInThisInvoicePeriod_returns_false_if_closed_after_invoice_period()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1) };
            var contract = new Contract(1, DateTime.Now.AddMonths(-6))
            {
                ContractStatus = ContractStatus.Closed,
                StatusChangedDate = invoice.ToDate.AddDays(10)
            };

            var hasClosed = invoice.HasContractBeenClosedInThisInvoicePeriod(contract);

            Assert.IsFalse(hasClosed);
        }

        [Test]
        public void HasContractBeenClosedInThisInvoicePeriod_returns_false_if_not_closed()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1) };
            var contract = new Contract(1, DateTime.Now.AddMonths(-6))
            {
                ContractStatus = ContractStatus.Active,
                StatusChangedDate = invoice.FromDate.AddDays(10)
            };

            var hasClosed = invoice.HasContractBeenClosedInThisInvoicePeriod(contract);

            Assert.IsFalse(hasClosed);
        }

        [Test]
        public void CalculateContractStartProrata_returns_two_days_when_contract_actived_two_days_after_invocie_start_period()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(2) };
            var contract = new Contract(1, DateTime.Now.AddMonths(-6))
                                    {
                                        ContractStatus = ContractStatus.Active,
                                        ActivationDate = invoice.FromDate.AddDays(2),
                                        StatusChangedDate = invoice.FromDate.AddDays(10)
                                    };

            var rate = invoice.CalculateContractStartProrataRate(contract);

            Assert.AreEqual(1-2/(365.25/12), rate);
        }

        [Test]
        public void CalculateContractStartProrata_returns_1_exception_when_activation_outside_invoice_period()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1) };
            var contract = new Contract(1, DateTime.Now.AddMonths(-6))
            {
                ContractStatus = ContractStatus.Active,
                ActivationDate = invoice.ToDate.AddDays(2),
                StatusChangedDate = invoice.FromDate.AddDays(10)
            };

            var rate = invoice.CalculateContractStartProrataRate(contract);

            Assert.AreEqual(1, rate);
        }

        [Test]
        public void CalculateContractStartProrata_returns_one_if_activated_before_invoice_start()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1) };
            var contract = new Contract(1, DateTime.Now.AddMonths(-6))
            {
                ContractStatus = ContractStatus.Active,
                ActivationDate = invoice.FromDate.AddDays(-3),
                StatusChangedDate = invoice.FromDate.AddDays(10)
            };

            var rate = invoice.CalculateContractStartProrataRate(contract);

            Assert.AreEqual(1, rate);
            
        }

        [Test]
        public void Contract_activated_after_invoice_to_date_is_not_invoiced()
        {
            var invoice = new Invoice { FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1) };
            var contract = new Contract(1, DateTime.Now.AddDays(-10))
            {
                ContractStatus = ContractStatus.Active,
                ActivationDate = DateTime.Now.AddDays(-10),
                StatusChangedDate = DateTime.Now.AddDays(-10)
            };

            var isBillable = invoice.IsContractBillable(contract);
            Assert.IsFalse(isBillable);
        }
    }
}
