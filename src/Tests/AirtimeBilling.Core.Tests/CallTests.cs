﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;

namespace SharpStudios.AirtimeBilling.Core.Tests
{
    /// <summary>
    /// Summary description for CallTests
    /// </summary>
    [TestFixture]
    public class CallTests
    {              
        [Test]
        public void Cost_of_zero_time_call_is_zero()
        {
            var zeroCall = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Call();
            zeroCall.Contract = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Contract();
            zeroCall.Contract.Plan = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Plan { UnitOfTime = 20 };
            zeroCall.Tariff = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Tariff { UnitCost = 1.10M };
            zeroCall.Volume = 0;

            Assert.AreEqual(0, zeroCall.Cost, "Cost is not zero");
        }

        [Test]
        public void Cost_of_call_when_billing_interval_is_one_minute_equals_UnitCost()
        {
            var call = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Call();            
            call.Contract = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Contract();
            call.Contract.Plan = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Plan { UnitOfTime = 60 };
            call.Tariff = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Tariff { UnitCost = 1.10M };
            call.UnitsOfTime = 1;

            Assert.AreEqual(1.10M, call.Cost, "Cost is not zero");
        }

        [Test]
        public void Cost_of_call_when_billing_interval_is_thirty_seconds_equals_half_UnitCost()
        {
            var call = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Call();
            call.Contract = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Contract();
            call.Contract.Plan = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Plan { UnitOfTime = 30 };
            call.Tariff = new SharpStudios.AirtimeBilling.DataAccess.Repositories.Tariff { UnitCost = 1.10M };
            call.UnitsOfTime = 1;

            Assert.AreEqual(0.55M, call.Cost, "Cost is not zero");
        }
        
    }
}
