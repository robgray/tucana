﻿using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using NUnit.Framework;

namespace AirtimeBilling.Logic.Tests
{
    [TestFixture]
    public class ProxyHelperTests
    {
        [Test]
        public void GetProxyEntity_returns_correct_entity()
        {
            var entity = ProxyHelper.GetProxyEntity(5, "Contract");

            Assert.IsInstanceOf(typeof(Contract), entity);
        }
    }
}
