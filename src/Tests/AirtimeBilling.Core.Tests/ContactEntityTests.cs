﻿using AirtimeBilling.Core.Entities;
using NUnit.Framework;

namespace AirtimeBilling.Logic.Tests
{
    /// <summary>
    /// Summary description for ContactEntityTests
    /// </summary>
    [TestFixture]
    public class ContactEntityTests
    {             
        [Test]
        public void Name_populates_First_and_last_names()
        {
            var contact = new Contact {Name = "Robert Gray"};

            Assert.AreEqual("Robert", contact.FirstName);
            Assert.AreEqual("Gray", contact.LastName);
        }

        [Test]
        public void Name_populates_Last_name()
        {
            var contact = new Contact {Name = "Gray"};

            Assert.AreEqual("", contact.FirstName);
            Assert.AreEqual("Gray", contact.LastName);
        }

        [Test]
        public void FirstName_LastName_not_filled_new_entity()
        {
            var contact = new Contact();

            Assert.AreEqual("", contact.FirstName);
            Assert.AreEqual("", contact.LastName);
        }
    }
}
