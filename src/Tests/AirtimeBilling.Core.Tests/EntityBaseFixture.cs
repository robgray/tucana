﻿using AirtimeBilling.Core.Entities;
using NUnit.Framework;

namespace AirtimeBilling.Logic.Tests
{
    [TestFixture]
    public class EntityBaseFixture
    {
        [Test]
        public void GetEntityName_returns_name_of_entity()
        {
            var entity = new MyEntity();

            var name = entity.GetEntityName();

            Assert.AreEqual("MyEntity", name);
        }

        private class MyEntity : EntityBase
        {
            
        }
    }    
}
