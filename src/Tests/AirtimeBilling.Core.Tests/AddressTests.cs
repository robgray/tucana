﻿using AirtimeBilling.Core.Entities;
using NUnit.Framework;

namespace AirtimeBilling.Logic.Tests
{
    /// <summary>
    /// Summary description for AddressTests
    /// </summary>
    [TestFixture]
    public class AddressTests
    {             
        [Test]
        public void ToString_of_address_includes_all_address_fields()
        {
            var address = new Address
                                  {Street = "22 Turrbal St", Suburb = "Bellbowrie", State = "QLD", Postcode = "4070"};
            Assert.AreEqual("22 Turrbal St Bellbowrie QLD 4070", address.ToString());
        }

        [Test]
        public void ToString_when_all_null_fields_returns_empty_string()
        {
            var address = new Address();
            Assert.AreEqual("", address.ToString());
        }

        [Test]
        public void ToString_includes_suburb_state_postcode_when_street_not_included()
        {
            var address = new Address {Suburb = "Bellbowrie", State = "QLD", Postcode = "4070"};                       
            Assert.AreEqual("Bellbowrie QLD 4070", address.ToString());
        }
    }
}
