﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using NUnit.Framework;

namespace AirtimeBilling.Logic.Tests
{
    /// <summary>
    /// Summary description for ContractTests
    /// </summary>
    [TestFixture]
    public class ContractTests
    {     
        [Test]
        public void  IsTerminated_returns_true_if_closed_before_end_date()
        {
            Contract contract = new Contract(1, DateTime.Now.AddYears(-1))
                                    {
                                        ActivationDate = DateTime.Now.AddYears(-1),
                                        EndDate = DateTime.Now.AddYears(1),
                                        StatusChangedDate = DateTime.Now,
                                        ContractStatus = ContractStatus.Closed
                                    };

            Assert.IsTrue(contract.IsTerminated);
        }

        [Test]
        public void IsTerminated_returns_false_if_closed_after_end_date()
        {
            Contract contract = new Contract(1, DateTime.Now.AddYears(-1))
            {
                ActivationDate = DateTime.Now.AddYears(-1),
                EndDate = DateTime.Now.AddYears(1),               
                ContractStatus = ContractStatus.Closed
            };

            contract.StatusChangedDate = contract.EndDate.AddDays(1);

            Assert.IsFalse(contract.IsTerminated);
        }

        [Test]
        public void IsTerminated_returns_false_if_closed_on_end_date()
        {
            Contract contract = new Contract(1, DateTime.Now.AddYears(-1))
            {
                ActivationDate = DateTime.Now.AddYears(-1),
                EndDate = DateTime.Now.AddYears(1),
                ContractStatus = ContractStatus.Closed
            };

            contract.StatusChangedDate = contract.EndDate;

            Assert.IsFalse(contract.IsTerminated);
        }

        [Test]
        public void IsTerminated_returns_false_if_contract_not_closed()
        {
            Contract contract = new Contract(1, DateTime.Now.AddYears(-1))
            {
                ActivationDate = DateTime.Now.AddYears(-1),
                EndDate = DateTime.Now.AddYears(1),
                StatusChangedDate = DateTime.Now.AddYears(1),
                ContractStatus = ContractStatus.Active
            };

            Assert.IsFalse(contract.IsTerminated);
        }
      
        
    }
}
