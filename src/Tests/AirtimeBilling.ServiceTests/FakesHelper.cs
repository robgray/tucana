﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;

namespace AirtimeBilling.Services.Tests
{
    public static class FakesHelper
    {
        public static Plan CreatePlan()
        {
            return new Plan(1)
            {
                DefaultTariffId = 1,
                Duration = 24,
                FlagFall = 0.50M,
                FreeCallAmount = 10M,
                Name = "Test Plan",
                NetworkId = 1,
                PlanAmount = 30M
            };
        }

        public static Contact CreateContact()
        {
            var contact = new Contact(1)
            {
                CreditCard = new CreditCard { CardNumber = "123456", NameOnCard = "Test Man", ExpiryDate = "01/10", CreditCardType = CreditCardType.Visa },
                DriversLicense = new DriversLicense
                {
                    LicenseNumber = "12344232",
                    DateOfBirth = DateTime.Parse("1965-July-01"),
                    Expiry = DateTime.Parse("2010-Mar-03")
                },
                Email = "test@test.com",
                HomeAddress = new Address { Street = "123 Home St", Suburb = "Hometown", State = "QLD", Postcode = "4999" },
                PostalAddress = new Address { Street = "123 Postal Av", Suburb = "Postville", State = "QLD", Postcode = "4998" },
                Name = "Test Person",
                MobilePhone = "0400000000"                
            };

            return contact;
        }

        public static Contract CreateContractOnSubAccount(SubAccount _subAccount, Plan plan)
        {
            return new Contract(2, DateTime.Now)
            {
                AccountId = _subAccount.Id.Value,
                ActivationDate = DateTime.Today.AddMonths(-9),
                EndDate = DateTime.Today.AddMonths(2),
                AgentId = null,
                ContractNumber = "LW00002",
                ContractStatus = ContractStatus.Active,
                Data = true,
                MessageBank = true,
                IMEINumber = "42932323123",
                SimCard = "12312312313",
                PhoneNumber1 = "0410000000",
                Pin = "1234",
                Puk = "1234",
                UsedBy = "Test",
                PlanId = plan.Id.Value
            };
        }

        public static MasterAccount CreateRootInvoiceAccount(Contact rootContact)
        {

            var rootAccount = new MasterAccount(1)
            {
                AccountName = "Test Master Account",
                AccountNumber = "LW0001",
                BillingAddressType = BillingAddressType.Postal,
                BillingMethod = BillingMethod.Invoice,
                ContactId = rootContact.Id.Value,
                ContactRole = "CEO",
                EmailBill = false,
                IsInvoiceRoot = true,
                PostBill = true,
                EmailBillDataFile = false,
                HasRequestedInvoicing = false,
                Password = "Password"
            };

            return rootAccount;
        }

        public static MasterAccount CreateInternationalRootInvoiceAccont(Contact rootContact)
        {
            var rootAccount = CreateRootInvoiceAccount(rootContact);
            rootAccount.IsInternational = true;
            return rootAccount;
        }

        public static SubAccount CreateSubAccount(MasterAccount rootAccount)
        {
            var subAccount = new SubAccount(2)
            {
                AccountName = "Test Sub Account",
                AccountNumber = "LW0002",
                BillingAddressType = BillingAddressType.Postal,
                // Doesn't matter for sub accounts.
                BillingMethod = BillingMethod.CreditCard,
                // Doesn't matter for sub accounts.
                ContactId = rootAccount.ContactId,
                ContactRole = "CEO",
                EmailBill = false,
                IsInvoiceRoot = false,
                PostBill = true,
                EmailBillDataFile = false,
                HasRequestedInvoicing = false,
                Password = "Password",
                MasterAccountId = rootAccount.Id.Value
            };

            return subAccount;
        }

        public static Contract CreateActiveContract(Account account, Plan plan)
        {
            return CreateActiveContract(account, plan, DateTime.Parse("2008-10-01"));
        }

        public static Contract CreateActiveContract(Account account, Plan plan, DateTime orderReceivedDate)
        {
            return new Contract(1, orderReceivedDate)
            {
                AccountId = account != null ? account.Id.Value : 0,
                ActivationDate = DateTime.Parse("2008-10-01"),
                ContractNumber = "LW00001",
                AgentId = null,
                ContractStatus = ContractStatus.Active,
                Data = false,
                MessageBank = false,
                EndDate = DateTime.Parse("2010-10-01"),
                IMEINumber = "1234556",
                SimCard = "1234523",
                PhoneNumber1 = "0411331273",
                Pin = "1234",
                Puk = "1234",
                PlanId = plan != null ? plan.Id.Value : 0,
                UsedBy = "Test"
            };
        }

        public static IList<Call> CreateCallListWithOneCall(Contract contract)
        {
            return new List<Call>
                             {
                                 new Call(1)
                                    {                                         
                                        CalledFrom = "SATELLITE", 
                                        ContractId = contract.Id.Value,
                                        Volume = 2.54M,
                                        HasFreeCallTariff = false,
                                        ImportedCallType = "TEST CALL",
                                        NumberCalled = "0400000001",
                                        PhoneNumber = contract.PhoneNumber1,
                                        UnitCost = 1M,
                                        UnitsOfTime = 6,
                                        Cost = 6.5M
                                    }
                             };
        }

        public static IList<Call> CreateCallListWithTwoCalls(Contract contract)
        {
            return new List<Call>
                             {
                                 new Call(1)
                                    {                                         
                                        CalledFrom = "SATELLITE", 
                                        ContractId = contract.Id.Value,
                                        Volume = 2.54M,
                                        HasFreeCallTariff = true,
                                        ImportedCallType = "TEST CALL",
                                        NumberCalled = "0400000001",
                                        PhoneNumber = contract.PhoneNumber1,
                                        UnitCost = 1M,
                                        UnitsOfTime = 6, 
                                        Cost = 6.5M                                        
                                    }, 
                                new Call(2)
                                    {                                        
                                        CalledFrom = "SATELLITE",
                                        ContractId = contract.Id.Value, 
                                        Volume = 5.23M,
                                        HasFreeCallTariff = true,
                                        ImportedCallType = "TEST CALL",
                                        NumberCalled = "0400000002",
                                        PhoneNumber = contract.PhoneNumber1,
                                        UnitCost = 1M,
                                        UnitsOfTime = 11, 
                                        Cost = 11.5M
                                    }          
                             };
        }
    }
}
