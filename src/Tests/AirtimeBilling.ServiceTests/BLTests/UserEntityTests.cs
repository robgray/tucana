﻿using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using NUnit.Framework;
using System.Security.Principal;

namespace AirtimeBilling.Services.Tests.BLTests
{
    /// <summary>
    /// Summary description for UserEntityTests
    /// </summary>
    [TestFixture]
    public class UserEntityTests
    {              
        [Test]
        public void UserEntityGetPrincipal()
        {
            User user = new AgentUser() { Username = "TEST" };
            IPrincipal principal = user.GetUserPrincipal();

            Assert.IsTrue(principal.IsInRole("Agent"));
        }
    }
}
