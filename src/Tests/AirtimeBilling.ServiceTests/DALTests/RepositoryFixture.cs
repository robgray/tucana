﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Common;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Base class for repository tests.
    /// </summary>
    [TestFixture]
    [Ignore]
    public class RepositoryFixture
    {            
        [SetUp]
        public virtual void MyTestInitialize()
        {
            Populate();
        }
        
        /// <summary>
        /// Populates the test database
        /// Only use this for repository testing!
        /// Otherwise Repositories should be mocked.
        /// </summary>
        protected void Populate()
        {
            using (var conn = new SqlConnection(ConfigItems.ConnectionString))
            {
                var filename = Properties.Settings.Default.TestDbInstallScript;
                var sql = System.IO.File.ReadAllText(filename);
                var cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;

                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }

        protected SqlDataReader ExceuteReader(string query)
        {
            using (var conn = new SqlConnection(ConfigItems.ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandText = query;

                var reader = cmd.ExecuteReader();
                return reader;
            }
        }


    }
}
