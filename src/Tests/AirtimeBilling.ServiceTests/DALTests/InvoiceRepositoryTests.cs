﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.Services.Tests;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for InvoiceRepositoryTests
    /// </summary>
    [TestFixture]
    public class InvoiceRepositoryTests : RepositoryFixture
    {       
        private IInvoiceRepository repository;
        
        [SetUp]
        public override void MyTestInitialize()
        {
            base.MyTestInitialize();
            repository = RepositoryFactory.GetRepository<IInvoiceRepository>();
        }

        [Test]
        public void GetInvoiceTest()
        {
            var entity = repository["INV0001"];
            Assert.IsNotNull(entity);
            Assert.AreEqual(14, entity.Lines.Count());

            foreach(var line in entity.Lines)
            {
                Assert.IsNotNull(line.Invoice);
                Assert.AreSame(line.Invoice, entity);
            }
        }
        
        [Test]
        public void InsertInvoiceTest()
        {
            var address = new Address { Street = "22 Turrbal St",
                              Suburb = "Bellbowrie",
                              State = "QLD",
                              Postcode = "4070"
            };

            var invoice = new Invoice
                              {
                                  InvoiceDate = DateTime.Today,
                                  InvoiceRunNumber = 10,
                                  InvoiceNumber = "INV0010",
                                  InvoiceAddress = address,
                                  To = "Robert Gray",
                                  FromDate = DateTime.Today.AddMonths(-1),
                                  ToDate = DateTime.Today,
                                  AmountPaid = 0,
                              };

            invoice.AddAccount(1);
            invoice.AddLine("Service Charge", 1, 30M, null, null);

            Assert.IsNull(invoice.Id);
            repository.InsertInvoice(invoice);
            Assert.IsNotNull(invoice.Id);
        }
    }
}
