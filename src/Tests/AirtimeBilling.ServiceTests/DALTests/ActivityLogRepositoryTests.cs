﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using Contact=SharpStudios.AirtimeBilling.Core.Contact;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for ActivityLogRepositoryTests
    /// </summary>
    [TestFixture]    
    public class ActivityLogRepositoryTests : RepositoryFixture
    {      
        private IActivityLogRepository repository;
        
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            repository = new ActivityLogRepository();
        }

        [Test]
        public void InsertActivityLogTest()
        {
            var log = new Activity(DateTime.Now, "Test", "Test Activity");
                        
            repository.Insert(log, "Contact", 1);
            Assert.IsNotNull(log.Id);            
        }

        [Test]
        public void FindActivityLogByUser()
        {
            var logs = repository.FindActivitiesByUser("admin");
            Assert.AreEqual(2, logs.Count);            
        }

        [Test]
        public void FindActivityLogByEntity()
        {
            var logs = repository.FindActivitiesByEntity(new Contact(1));
            Assert.AreEqual(2, logs.Count);
        }
    }
}
