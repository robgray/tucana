﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using Plan=SharpStudios.AirtimeBilling.Core.Plan;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for PlanRepositoryTests
    /// </summary>
    [TestFixture]
    public class PlanRepositoryTests : RepositoryFixture
    {       
        private IPlanRepository planRepository;
      
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            planRepository = RepositoryFactory.GetRepository<IPlanRepository>();
        }
      
        [Test]
        public void GetPlanTest()
        {
            Plan plan = planRepository.GetPlan(1);

            Assert.IsNotNull(plan);
            Assert.AreEqual(1, plan.Id, "Id");
            Assert.AreEqual("Landwide 30 (12 Months)", plan.Name, "Name");
            Assert.AreEqual(12, plan.Duration, "Duration");
            Assert.AreEqual(0, plan.FlagFall, "FlagFall");
            Assert.AreEqual(10, plan.FreeCallAmount, "FreeCallAmount");
            Assert.AreEqual(30, plan.PlanAmount, "PlanAmount");
            Assert.AreEqual(1, plan.DefaultTariffId, "DefaultTariffId");
        }

        [Test]
        public void ThisGetTest()
        {
            Plan plan = planRepository[1];
            Assert.IsNotNull(plan);
            Assert.AreEqual(1, plan.Id.Value);
        }

        [Test]
        public void GetPlansByNetworkIdTest()
        {
            IList<Plan> plans = planRepository.GetPlansByNetworkId(1);
            Assert.AreEqual(2, plans.Count);
        }

        [Test]
        public void InsertPlanTest()
        {
            Plan plan = new Plan()
            {
                Name = "Test Plan",
                Duration = 12,
                FlagFall = 0,
                FreeCallAmount = 10,
                PlanAmount = 40,
                NetworkId = 1,
                DefaultTariffId = 9
            };

            planRepository.InsertPlan(plan);
            Assert.IsNotNull(plan.Id, "Plan Id");
            Assert.IsTrue(plan.Id.Value > 0, "Plan Id greater 0");
        }

        [Test]
        public void UpdatePlanTest()
        {
            Plan plan = new Plan(1)
            {                
                Name = "Test Plan",
                Duration = 12,
                FlagFall = 0,
                FreeCallAmount = 10,
                PlanAmount = 40,
                NetworkId = 1,
                DefaultTariffId = 9
            };

            bool success = planRepository.UpdatePlan(plan);
            Assert.IsTrue(success);
        }

        [Test]
        public void DeletePlanTest()
        {
            bool success = planRepository.DeletePlan(2);
            Assert.IsTrue(success);                        
        }

        [Test]
        [ExpectedException(typeof(UnknownEntityException))]
        public void DeletePlanUnknownPlanTest()
        {
            bool success = planRepository.DeletePlan(0);         
        }
    }
}
