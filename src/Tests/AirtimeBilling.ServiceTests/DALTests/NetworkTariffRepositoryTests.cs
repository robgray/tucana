﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using System.Data.SqlClient;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using NetworkTariff=SharpStudios.AirtimeBilling.Core.NetworkTariff;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for NetworkTariffRepositoryTests
    /// </summary>
    [TestFixture]
    public class NetworkTariffRepositoryTests : RepositoryFixture
    {      
        INetworkTariffRepository tariffRepository;
      
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            tariffRepository = RepositoryFactory.GetRepository<INetworkTariffRepository>();
        }
     

        [Test]
        public void GetNetworkTariffsTest()
        {
            IList<NetworkTariff> tariffs = tariffRepository.GetNetworkTariffsByNetworkId(1);
            Assert.AreEqual(8, tariffs.Count, "Tariff Count");
        }

        [Test]
        public void GetNetworkTariffTest()
        {
            NetworkTariff tariff = tariffRepository.GetNetworkTariff(4);
            Assert.AreEqual(4, tariff.Id.Value, "Tariff Id");
            Assert.AreEqual("I3", tariff.Code, "Code");
            Assert.AreEqual("Iridium Data 9.6", tariff.Name, "Tariff");
            Assert.AreEqual(false, tariff.IsCountedInFreeCall, "Is Counted In Free Call");
        }

        [Test]
        public void ThisTest()
        {            
            NetworkTariff tariff = tariffRepository[4];
            Assert.AreEqual(4, tariff.Id.Value, "Tariff Id");
            Assert.AreEqual("I3", tariff.Code, "Code");
            Assert.AreEqual("Iridium Data 9.6", tariff.Name, "Tariff");
            Assert.AreEqual(false, tariff.IsCountedInFreeCall, "Is Counted In Free Call");
        }

        [Test]
        public void InsertNetworkTariffTest()
        {
            NetworkTariff tariff = new NetworkTariff()
            {
                Code = "TEST",
                Name = "Test Tariff",
                IsCountedInFreeCall = true,
                NetworkId = 1
            };

            tariffRepository.InsertNetworkTariff(tariff);
            Assert.IsNotNull(tariff.Id);
        }

        [Test]
        public void UpdateNetworkTariffTest()
        {
            NetworkTariff entity = tariffRepository[4];            
            entity.IsCountedInFreeCall = true;
            entity.Name = "Service calls test";
            entity.Code = "SCT";

            bool success = tariffRepository.UpdateNetworkTariff(entity);
            Assert.IsTrue(success);

            NetworkTariff check = tariffRepository[4];
            Assert.AreEqual(true, check.IsCountedInFreeCall, "Free Call");
            Assert.AreEqual("Service calls test", check.Name, "Name");
            Assert.AreEqual("SCT", check.Code, "Code");
        }

        [Test]
        [ExpectedException(typeof(SqlException))]
        public void DeleteNetworkTariffExistingNetworkPlanTest()
        {                   
            bool success = tariffRepository.DeleteNetworkTariff(4);            
        }

        [Test]
        public void DeleteNetworkTariffTest()
        {            
            bool success = tariffRepository.DeleteNetworkTariff(7);
            Assert.IsTrue(success, "Delete");                
        }
    }
}
