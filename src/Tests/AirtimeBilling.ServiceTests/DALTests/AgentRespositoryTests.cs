﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Services.Tests;
using Agent=SharpStudios.AirtimeBilling.Core.Agent;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for AgentTests
    /// </summary>
    [TestFixture]
    public class AgentRespositoryTests : RepositoryFixture
    {      
        private IAgentRepository agentRepository;

        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            agentRepository = RepositoryFactory.GetRepository<IAgentRepository>();
        }
       
        [Test]
        public void ThisGetTest()
        {
            Agent agent = agentRepository[1];
            Assert.IsNotNull(agent);
            Assert.AreEqual(1, agent.Id.Value);
        }

        [Test]
        public void GetAgentTest()
        {
            Agent agent = agentRepository.GetAgent(1);
            Assert.IsNotNull(agent);
            Assert.AreEqual(1, agent.Id.Value);
        }

        [Test]
        public void GetAllAgents()
        {
            IList<Agent> agents = agentRepository.GetAllAgents();
            Assert.AreEqual(10, agents.Count);
        }

        [Test]
        public void UpdateAgentTest()
        {
            var agent = agentRepository[1];
            agent.AgentName = "Britz - Darwin";            
            agent.Address = new Address 
            { 
                Street = "44 Stuart Highway",
                Suburb = agent.Address.Suburb,
                State = agent.Address.State,
                Postcode = agent.Address.Postcode 
            };

            bool success = agentRepository.Update(agent);
            Assert.IsTrue(success);

        }
    }
}
