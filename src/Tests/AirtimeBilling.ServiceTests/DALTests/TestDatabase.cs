﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Rgss.LandwideCms.Common.Configuration;

namespace Rgss.LandwideCms.Services.Tests
{
    public static class TestDatabase
    {
        /// <summary>
        /// Populates the test database
        /// Only use this for repository testing!
        /// Otherwise Repositories should be mocked.
        /// </summary>
        public static void Populate()
        {
            using (var conn = new SqlConnection(ConfigItems.ConnectionString))
            {                
                var filename = Properties.Settings.Default.TestDbInstallScript;
                var sql = System.IO.File.ReadAllText(filename);
                var cmd = conn.CreateCommand();
                cmd.CommandText = sql;
                cmd.CommandType = CommandType.Text;

                conn.Open();
                cmd.ExecuteNonQuery();                
            }            
        }

        public static SqlDataReader ExceuteReader(string query)
        {
            using (var conn = new SqlConnection(ConfigItems.ConnectionString))
            {
                var cmd = conn.CreateCommand();
                cmd.CommandText = query;

                var reader = cmd.ExecuteReader();
                return reader;
            }
        }
    }
}
