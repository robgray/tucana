﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using Account=SharpStudios.AirtimeBilling.Core.Account;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for AccountRepositoryTests
    /// </summary>
    [TestFixture]
    [Ignore]
    public class AccountRepositoryTests : RepositoryFixture     
    {        
        private IAccountRepository accountRepository;
      
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            accountRepository = RepositoryFactory.GetRepository<IAccountRepository>();
        }        

        [Test]
        public void GetMasterAccountTest()
        {
            int accountId = 1;

            MasterAccount account = (MasterAccount)accountRepository.GetAccount(accountId);
            
            Assert.IsNotNull(account, "Account Null test");
            Assert.AreEqual("MasterAccount", account.GetType().Name, "Account Type");
            Assert.AreEqual(1, account.Id.Value, "Account Id");
            Assert.AreEqual("MK0001", account.AccountNumber, "Account Number");
            Assert.AreEqual("Mikel Master Account", account.AccountName, "Account Name");
            Assert.AreEqual(1, account.ContactId, "Contact Id");
            Assert.AreEqual(1, account.CompanyId, "Company Id");            
            Assert.AreEqual("Owner", account.ContactRole, "Contact Role");
            Assert.AreEqual(BillingMethod.CreditCard, account.BillingMethod, "Billing Method");
            Assert.AreEqual(true, account.EmailBillDataFile, "Email Bill Data File");
            Assert.AreEqual(true, account.EmailBill, "Email Bill");
            Assert.AreEqual(true, account.PostBill, "Post Bill");
            Assert.AreEqual(BillingAddressType.Physical, account.BillingAddressType, "Billing Address Type");
            Assert.AreEqual("Account", account.Password, "Password");
            Assert.AreEqual(true, account.IsInvoiceRoot, "Is Invoice Rooot");
            Assert.AreEqual(true, account.HasRequestedInvoicing, "Has Requested Invoicing");
        }

        [Test]
        public void GetSubAccountTest()
        {
            int accountId = 2;

            SubAccount account = accountRepository.GetAccount(accountId) as SubAccount;

            Assert.IsNotNull(account, "Account Null test");
            Assert.AreEqual("SubAccount", account.GetType().Name, "Account Type");
            Assert.AreEqual(2, account.Id.Value, "Account Id");
            Assert.AreEqual("MK0002", account.AccountNumber, "Account Number");
            Assert.AreEqual("Mikel Account 1", account.AccountName, "Account Name");
            Assert.AreEqual(1, account.ContactId, "Contact Id");            
            Assert.AreEqual("Owner", account.ContactRole, "Contact Role");
            Assert.AreEqual(BillingMethod.CreditCard, account.BillingMethod, "Billing Method");
            Assert.AreEqual(false, account.EmailBillDataFile, "Email Bill Data File");
            Assert.AreEqual(false, account.EmailBill, "Email Bill");
            Assert.AreEqual(false, account.PostBill, "Post Bill");
            Assert.AreEqual(BillingAddressType.Physical, account.BillingAddressType, "Billing Address Type");
            Assert.AreEqual("Account", account.Password, "Password");
            Assert.AreEqual(false, account.IsInvoiceRoot, "Is Invoice Rooot");
            Assert.AreEqual(true, account.HasRequestedInvoicing, "Has Requested Invoicing");
        }

        [Test]
        public void UpdateAccountTest()
        {
            MasterAccount account = new MasterAccount(1)
            {                
                AccountNumber = "MK0001",
                AccountName = "Mikel Master Account",
                ContactId = 1,
                CompanyId = 1,
                ContactRole = "Owner/Operator",
                BillingMethod = BillingMethod.CreditCard,
                EmailBillDataFile = true,
                EmailBill = true,
                PostBill = true,
                BillingAddressType = BillingAddressType.Physical,
                Password = "Account",
                IsInvoiceRoot = true,
                HasRequestedInvoicing = true
            };

            bool success = accountRepository.UpdateAccount(account);

            Assert.IsTrue(success);

            AirtimeBillingDataContext db = DbFactory.GetDataContext();
            var saved = db.Accounts.Where(a => a.AccountId == account.Id.Value).SingleOrDefault();
            
            Assert.IsNotNull(saved);
            Assert.AreEqual(account.AccountNumber, saved.AccountNumber, "Account Number");
            Assert.AreEqual(account.AccountName, saved.AccountName, "Account Name");
            Assert.AreEqual(account.ContactId, saved.ContactId, "Contact Id");
            Assert.AreEqual(account.CompanyId, saved.CompanyId, "Company Id");
            Assert.AreEqual(account.ContactRole, saved.ContactRole, "Contact Role");
            Assert.AreEqual((int)account.BillingMethod, saved.BillingMethod, "Billing Method");
            Assert.AreEqual(account.EmailBillDataFile, saved.EmailBillDataFile, "Email Bill Data File");
            Assert.AreEqual(account.EmailBill, saved.EmailBill, "Email Bill");
            Assert.AreEqual(account.PostBill, saved.PostBill, "Post Bill");
            Assert.AreEqual((int)account.BillingAddressType, saved.BillingAddressType, "Billing Address Type");
            Assert.AreEqual(account.Password, saved.Password, "Password");
            Assert.AreEqual(account.IsInvoiceRoot, saved.IsInvoiceRoot, "Is Invoice Root");
            Assert.AreEqual(account.HasRequestedInvoicing, saved.HasRequestedInvoicing, "Has Requested Invoicing");                            
        }

        [Test]
        public void InsertAccountTest()
        {
            SubAccount account = new SubAccount()
            {                
                AccountNumber = "MK0003",
                AccountName = "Mikel Account 2",
                ContactId = 1,                
                ContactRole = "Owner/Operator",
                BillingMethod = BillingMethod.CreditCard,
                EmailBillDataFile = true,
                EmailBill = true,
                PostBill = true,
                BillingAddressType = BillingAddressType.Physical,
                Password = "Account",
                IsInvoiceRoot = false,
                MasterAccountId = 1,
                HasRequestedInvoicing = true
            };

            accountRepository.InsertAccount(account);
            Assert.IsNotNull(account.Id);

            AirtimeBillingDataContext db = DbFactory.GetDataContext();
            var saved = db.Accounts.Where(a => a.AccountId == account.Id.Value).SingleOrDefault();

            Assert.IsNotNull(saved);
            Assert.AreEqual(account.AccountNumber, saved.AccountNumber, "Account Number");
            Assert.AreEqual(account.AccountName, saved.AccountName, "Account Name");
            Assert.AreEqual(account.ContactId, saved.ContactId, "Contact Id");            
            Assert.AreEqual(account.ContactRole, saved.ContactRole, "Contact Role");
            Assert.AreEqual((int)account.BillingMethod, saved.BillingMethod, "Billing Method");
            Assert.AreEqual(account.EmailBillDataFile, saved.EmailBillDataFile, "Email Bill Data File");
            Assert.AreEqual(account.EmailBill, saved.EmailBill, "Email Bill");
            Assert.AreEqual(account.PostBill, saved.PostBill, "Post Bill");
            Assert.AreEqual((int)account.BillingAddressType, saved.BillingAddressType, "Billing Address Type");
            Assert.AreEqual(account.Password, saved.Password, "Password");
            Assert.AreEqual(account.IsInvoiceRoot, saved.IsInvoiceRoot, "Is Invoice Root");
            Assert.AreEqual(account.HasRequestedInvoicing, saved.HasRequestedInvoicing, "Has Requested Invoicing");
        }

        [Test]
        public void ThisGetTest()
        {
            Account account = accountRepository[3];
            Assert.IsNotNull(account);
            Assert.AreEqual(3, account.Id.Value);
        }

        [Test]
        public void GetAllAccountsTest()
        {
            IList<Account> accounts = accountRepository.GetAllAccounts();
            Assert.IsNotNull(accounts);
            Assert.AreEqual(4, accounts.Count);
        }
    }
}
