﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using SharpStudios.AirtimeBilling.Services;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.Services.Tests;
using Contact=SharpStudios.AirtimeBilling.Core.Contact;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for ContactRepositoryTests
    /// </summary>
    [TestFixture]
    public class ContactRepositoryTests : RepositoryFixture
    {     
        private IContactRepository contactRepository;
      
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            contactRepository = RepositoryFactory.GetRepository<IContactRepository>();
        }
      
        [Test]
        public void ThisGetTest()
        {
            Contact contact = contactRepository[1];

            Assert.IsNotNull(contact);
            Assert.AreEqual(1, contact.Id, "Id");
            Assert.AreEqual("Michael Moran", contact.Name);
            Assert.IsNull(contact.MobilePhone, "Mobile Phone");
            Assert.IsNull(contact.WorkPhone, "Work Phone");
            Assert.IsNull(contact.FaxNumber, "Fax Number");
            Assert.AreEqual("mjmoran@internode.on.net", contact.Email, "Email");
            Assert.AreEqual("Ripley Rd", contact.HomeAddress.Street, "Home Street");
            Assert.AreEqual("Ripley", contact.HomeAddress.Suburb, "Home Suburb");
            Assert.AreEqual("QLD", contact.HomeAddress.State, "Home State");
            Assert.AreEqual("4305", contact.HomeAddress.Postcode, "Home Postcode");
            Assert.IsNull(contact.PostalAddress.Street, "Postal Street");
            Assert.IsNull(contact.PostalAddress.Suburb, "Postal Suburb");
            Assert.IsNull(contact.PostalAddress.Postcode, "Postal Postcode");
            Assert.IsNull(contact.PostalAddress.State, "Postal State");
            Assert.AreEqual("32323535", contact.DriversLicense.LicenseNumber, "Drivers License Number");
            Assert.AreEqual(DateTime.Parse("2009-01-01"), contact.DriversLicense.Expiry, "Drivers License Expiry");
            Assert.AreEqual(DateTime.Parse("1966-04-02"), contact.DriversLicense.DateOfBirth, "Drivers License Date Of Birth");
            Assert.AreEqual("M J Moran", contact.CreditCard.NameOnCard, "Credit Card Name");
            Assert.AreEqual("03/11", contact.CreditCard.ExpiryDate, "Credit Card Expiry");
            Assert.AreEqual(CreditCardType.Visa, contact.CreditCard.CreditCardType, "Credit Card Type");
            Assert.AreEqual("23232323042323", contact.CreditCard.CardNumber, "Credit Card Number");              
        }

        [Test]
        public void GetContactTest()
        {
            Contact contact = contactRepository.GetContactEntity(1);

            Assert.IsNotNull(contact);
            Assert.AreEqual(1, contact.Id, "Id");
            Assert.AreEqual("Michael Moran", contact.Name);
            Assert.IsNull(contact.MobilePhone, "Mobile Phone");
            Assert.IsNull(contact.WorkPhone, "Work Phone");
            Assert.IsNull(contact.FaxNumber, "Fax Number");
            Assert.AreEqual("mjmoran@internode.on.net", contact.Email, "Email");
            Assert.AreEqual("Ripley Rd", contact.HomeAddress.Street, "Home Street");
            Assert.AreEqual("Ripley", contact.HomeAddress.Suburb, "Home Suburb");
            Assert.AreEqual("QLD", contact.HomeAddress.State, "Home State");
            Assert.AreEqual("4305", contact.HomeAddress.Postcode, "Home Postcode");
            Assert.IsNull(contact.PostalAddress.Street, "Postal Street");
            Assert.IsNull(contact.PostalAddress.Suburb, "Postal Suburb");
            Assert.IsNull(contact.PostalAddress.Postcode, "Postal Postcode");
            Assert.IsNull(contact.PostalAddress.State, "Postal State");
            Assert.AreEqual("32323535", contact.DriversLicense.LicenseNumber, "Drivers License Number");
            Assert.AreEqual(DateTime.Parse("2009-01-01"), contact.DriversLicense.Expiry, "Drivers License Expiry");
            Assert.AreEqual(DateTime.Parse("1966-04-02"), contact.DriversLicense.DateOfBirth, "Drivers License Date Of Birth");
            Assert.AreEqual("M J Moran", contact.CreditCard.NameOnCard, "Credit Card Name");
            Assert.AreEqual("03/11", contact.CreditCard.ExpiryDate, "Credit Card Expiry");
            Assert.AreEqual(CreditCardType.Visa, contact.CreditCard.CreditCardType, "Credit Card Type");
            Assert.AreEqual("23232323042323", contact.CreditCard.CardNumber, "Credit Card Number");            
        }

        [Test]
        public void GetAllContactsTest()
        {
            IList<Contact> contacts = contactRepository.GetAllContacts();
            Assert.AreEqual(3, contacts.Count);
        }

        [Test]
        public void UpdateContactTest()
        {
            Contact contact = contactRepository[1];
            contact.Name = "Michael James Moran";
            bool success = contactRepository.UpdateContact(contact);
            Assert.IsTrue(success);
        }

        [Test]
        public void InsertContactTest()
        {
            var dl = new DriversLicense
                                    {
                                        LicenseNumber = "12345678",
                                        DateOfBirth = DateTime.Today,
                                        Expiry = DateTime.Today
                                    };
                                                       

            var cc = new CreditCard
                                {
                                    CardNumber = "123123131312",
                                    NameOnCard = "J Bloggs",
                                    ExpiryDate = "02/12",
                                    CreditCardType = CreditCardType.Visa
                                };
                
            var contact = new Contact
            {
                Name = "Joe Bloggs",
                Email = "jb@test.com",
                DriversLicense = dl,
                CreditCard = cc,
            };

            contactRepository.InsertContact(contact);
            Assert.IsNotNull(contact.Id);
        }
    }
}
