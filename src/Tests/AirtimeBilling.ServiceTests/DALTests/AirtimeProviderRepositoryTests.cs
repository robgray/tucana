﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using AirtimeProvider=SharpStudios.AirtimeBilling.Core.AirtimeProvider;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for AirtimeProviderRepositoryTests
    /// </summary>
    [TestFixture]
    public class AirtimeProviderRepositoryTests : RepositoryFixture
    {      
        IAirtimeProviderRepository repository;
       
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            repository = RepositoryFactory.GetRepository<IAirtimeProviderRepository>();
        }

        [Test]
        public void GetAllAirtimeProvidersTest()
        {
            IList<AirtimeProvider> providers = repository.GetAirtimeProviders();
            
            Assert.IsNotNull(providers);
            Assert.AreEqual(1, providers.Count);
        }
    }
}
