﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using Network=SharpStudios.AirtimeBilling.Core.Network;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for NetworkTests
    /// </summary>
    [TestFixture]
    public class NetworkRepositoryTests : RepositoryFixture
    {    
        INetworkRepository networkRepository;

        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            networkRepository = RepositoryFactory.GetRepository<INetworkRepository>();
        }

        [Test]
        public void GetNetworkTest()
        {
            Network network = networkRepository.GetNetwork(1);
            Assert.IsNotNull(network, "Null");
            Assert.AreEqual(1, network.Id.Value, "Id");
            Assert.AreEqual("Iridium", network.Name, "Name");
            Assert.AreEqual(1, network.AirtimeProviderId, "AirtimeProviderId");
            Assert.AreEqual("Vizada", network.AirtimeProvider, "Airtime Provider");
        }

        [Test]
        public void GetAllNetworksTest()
        {
            IList<Network> networks = networkRepository.GetAllNetworks();
            Assert.AreEqual(3, networks.Count);
        }

        [Test]
        public void InsertNetworkTest()
        {
            Network network = new Network()
            {
                Name = "Thuraya",
                AirtimeProviderId = 1
            };

            networkRepository.InsertNetwork(network);
            Assert.IsNotNull(network.Id, "Id Check");
            Assert.AreEqual("Vizada", network.AirtimeProvider, "Airtime Provider");
         }

        [Test]
        public void UpdateNetworkTest()
        {
            Network network = new Network(1)
            {                
                Name = "Iridum v2",
                AirtimeProviderId = 1
            };

            bool success = networkRepository.UpdateNetwork(network);
            Assert.IsTrue(success);
            Assert.AreEqual("Vizada", network.AirtimeProvider, "Network");
        }

        [Test]
        public void DeleteNetworkTest()
        {
            bool success = networkRepository.DeleteNetwork(1);
            Assert.IsTrue(success);
        }
    }
}
