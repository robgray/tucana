﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Common;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for PlanTariffTests
    /// </summary>
    [TestFixture]
    public class PlanTariffTests : RepositoryFixture
    {       
        IPlanTariffRepository tariffRepository;
       
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            tariffRepository = RepositoryFactory.GetRepository<IPlanTariffRepository>();
        }

        [Test]
        public void GetPlanTariffsByPlanTest()
        {
            IList<PlanTariff> tariffs = tariffRepository.GetPlanTariffsByPlan(1);
            Assert.AreEqual(6, tariffs.Count, "Tariff Count");
        }

        [Test]
        public void GetPlanTariffTest()
        {
            PlanTariff tariff = tariffRepository.GetPlanTariff(3);
            Assert.IsNotNull(tariff, "Null check");
            Assert.AreEqual(3, tariff.Id.Value);
            Assert.AreEqual("I2", tariff.Code);
            Assert.AreEqual("Iridium Data 4.8", tariff.Name);            
            Assert.AreEqual(4, tariff.UnitCost);
            Assert.IsTrue(tariff.IsCountedInFreeCall);
            Assert.AreEqual(3, tariff.NetworkTariffId);            
            Assert.AreEqual(1, tariff.PlanId);
        }

        [Test]
        public void UpdatePlanTariffTest()
        {
            PlanTariff tariff = new PlanTariff(1)
            {                
                Code = "I0", 
                Name = "Iridium Free of Charge",
                UnitCost = 5,
                IsCountedInFreeCall = true,                
                PlanId = 1                
            };

            bool success = tariffRepository.UpdatePlanTariff(tariff);
            Assert.IsTrue(success);
        }

        [Test]
        public void InsertPlanTariffTest()
        {
            PlanTariff tariff = new PlanTariff(4)
            {                
                Code = "TEST",
                Name = "Test",
                UnitCost = 500,
                IsCountedInFreeCall = true,
                PlanId = 1
            };

            tariffRepository.InsertPlanTariff(tariff);
            Assert.IsNotNull(tariff.Id);
        }

        [Test]
        public void DeletePlanTariffByNetworkTariffTest()
        {            
            bool success = tariffRepository.DeletePlanTariffsByNetworkTariff(2);
            Assert.IsTrue(success);
        }
       
    }
}
