﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for RepositoryFactoryTests
    /// </summary>
    [TestFixture]
    public class RepositoryFactoryTests : RepositoryFixture
    {     
        [Test]
        public void GetRepositoryTest()
        {
            var repository = RepositoryFactory.GetRepository<IAccountRepository>();
            Assert.IsNotNull(repository);
        }
    }
}
