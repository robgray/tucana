﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using Contract=SharpStudios.AirtimeBilling.Core.Contract;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for ContractRepositoryTests
    /// </summary>
    [TestFixture]
    public class ContractRepositoryTests : RepositoryFixture
    {
        public ContractRepositoryTests()
        {
            //
            // TODO: Add constructor logic here
            //
        }  

        private IContractRepository contractRepository;

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test         
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            contractRepository = RepositoryFactory.GetRepository<IContractRepository>();
        }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [Test]
        public void GetNonInvoicedCallsTest()
        {
            var calls = contractRepository.GetNonInvoicedCallsByContractId(1);
            Assert.AreEqual(3, calls.Count);
            foreach (var call in calls) Assert.AreEqual(1, call.ContractId);
        }


        [Test]
        public void ThisGetTest()
        {
            var contract = contractRepository[1];
            Assert.IsNotNull(contract);

            Assert.AreEqual(1, contract.Id.Value, "Id");
            Assert.AreEqual(1, contract.PlanId, "Plan Id");
            Assert.AreEqual(1, contract.AccountId, "Account Id");
            Assert.AreEqual("CT0001", contract.ContractNumber, "Contract Number");
            Assert.AreEqual(DateTime.Parse("2008-03-13"), contract.ActivationDate, "Activation Date");
            Assert.AreEqual(DateTime.Parse("2009-03-13"), contract.EndDate, "End Date");
            Assert.AreEqual("3902390233", contract.IMEINumber, "IMEI Number");
            Assert.AreEqual("2390239232", contract.SimCard, "Sim Card");
            Assert.AreEqual("0893827894", contract.PhoneNumber1, "Phone Number 1");
            Assert.AreEqual("012345678", contract.PhoneNumber2, "Phone Number 2");
            Assert.AreEqual("345234423", contract.PhoneNumber3, "Phone Number 3");
            Assert.AreEqual("233442356", contract.PhoneNumber4, "Phone Number 4");
            Assert.AreEqual(1, contract.AgentId, "Agent Id");
            Assert.AreEqual(ContractStatus.ApplicationPending, contract.ContractStatus, "Contract Status");
            Assert.IsFalse(contract.Data, "Data");
            Assert.IsFalse(contract.MessageBank, "Message Bank");
            Assert.AreEqual("Mike", contract.UsedBy, "Used By");
            Assert.AreEqual("1234", contract.Pin, "Pin");
            Assert.AreEqual("1234", contract.Puk, "Puk");
        }

        [Test]
        public void GetContractTest()
        {
            var contract = contractRepository.GetContract(1);

            Assert.AreEqual(1, contract.Id.Value, "Id");
            Assert.AreEqual(1, contract.PlanId, "Plan Id");
            Assert.AreEqual(1, contract.AccountId, "Account Id");
            Assert.AreEqual("CT0001", contract.ContractNumber, "Contract Number");
            Assert.AreEqual(DateTime.Parse("2008-03-13"), contract.ActivationDate, "Activation Date");
            Assert.AreEqual(DateTime.Parse("2009-03-13"), contract.EndDate, "End Date");
            Assert.AreEqual("3902390233", contract.IMEINumber, "IMEI Number");
            Assert.AreEqual("2390239232", contract.SimCard, "Sim Card");
            Assert.AreEqual("0893827894", contract.PhoneNumber1, "Phone Number 1");
            Assert.AreEqual("012345678", contract.PhoneNumber2, "Phone Number 2");
            Assert.AreEqual("345234423", contract.PhoneNumber3, "Phone Number 3");
            Assert.AreEqual("233442356", contract.PhoneNumber4, "Phone Number 4");
            Assert.AreEqual(1, contract.AgentId, "Agent Id");
            Assert.AreEqual(ContractStatus.ApplicationPending, contract.ContractStatus, "Contract Status");
            Assert.IsFalse(contract.Data, "Data");
            Assert.IsFalse(contract.MessageBank, "Message Bank");
            Assert.AreEqual("Mike", contract.UsedBy, "Used By");
            Assert.AreEqual("1234", contract.Pin, "Pin");
            Assert.AreEqual("1234", contract.Puk, "Puk");            
        }

        [Test]
        public void UpdateContractTest()
        {
            var contract = new Contract(1, DateTime.Now)
            {                
                PlanId = 1,
                AccountId = 1,
                ContractNumber = "CT0001",
                ActivationDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(24),
                IMEINumber = "3902390233",
                SimCard = "2390239232",
                PhoneNumber1 = "0893827894",
                PhoneNumber2 = "123",
                AgentId = 1,
                ContractStatus = ContractStatus.Suspended,
                Data = false,
                MessageBank = false,
                UsedBy = "Mike",
                Pin = "1234",
                Puk = "1234"
            };

            bool success = contractRepository.UpdateContract(contract);
            Assert.IsTrue(success);
        }

        [Test]
        public void InsertContractTest()
        {
            var contract = new Contract(DateTime.Now)
            {                
                PlanId = 1,
                AccountId = 2,
                ContractNumber = "CT0023",
                ActivationDate = DateTime.Today,
                EndDate = DateTime.Today.AddMonths(24),
                IMEINumber = "3902390233",
                SimCard = "2390239232",
                PhoneNumber1 = "0893827891",                                
                ContractStatus = ContractStatus.ApplicationSubmitted,
                Data = false,
                MessageBank = false,
                UsedBy = "Test",
                Pin = "1234",
                Puk = "1234"
            };

            contractRepository.InsertContract(contract);
            Assert.IsNotNull(contract.Id);
        }

        [Test]
        public void GetContractsByAccountIdTest()
        {
            IList<Contract> contracts = contractRepository.GetContractsByAccountId(1);
            Assert.AreEqual(2, contracts.Count);
        }

        [Test]
        public void FindByContractNumberTest()
        {
            string contractNumber = "CT";
            IList<Contract> contracts = contractRepository.FindByContractNumber(contractNumber);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(14, contracts.Count, "Contract count");

            foreach (Contract contract in contracts)
            {
                Assert.IsTrue(contract.ContractNumber.StartsWith("CT"), "Contract Number match");
            }            
        }

        [Test]
        public void FindByAccountNumberTest()
        {
            string accountNumber = "MK";
            IList<Contract> contracts = contractRepository.FindByAccountNumber(accountNumber);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(5, contracts.Count, "Contract count");            

            // Can't verify account number because account number not part of contract.
            // Should I get the account here to verify??
        }

        [Test]
        public void FindByPhoneNumber1Test()
        {
            string phoneNumber = "0893827894";
            IList<Contract> contracts = contractRepository.FindByPhoneNumber(phoneNumber);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(1, contracts.Count, "Contract Count");
            Assert.AreEqual(phoneNumber, contracts[0].PhoneNumber1);
        }

        [Test]
        public void FindByPhoneNumber2Test()
        {
            string phoneNumber = "012345678";
            IList<Contract> contracts = contractRepository.FindByPhoneNumber(phoneNumber);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(1, contracts.Count, "Contract Count");
            Assert.AreEqual(phoneNumber, contracts[0].PhoneNumber2);
        }

        [Test]
        public void FindByPhoneNumber3Test()
        {
            string phoneNumber = "345234423";
            IList<Contract> contracts = contractRepository.FindByPhoneNumber(phoneNumber);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(1, contracts.Count, "Contract Count");
            Assert.AreEqual(phoneNumber, contracts[0].PhoneNumber3);
        }

        [Test]
        public void FindByPhoneNumber4Test()
        {
            string phoneNumber = "233442356";
            IList<Contract> contracts = contractRepository.FindByPhoneNumber(phoneNumber);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(1, contracts.Count, "Contract Count");
            Assert.AreEqual(phoneNumber, contracts[0].PhoneNumber4);
        }

        [Test] 
        public void FindByContactTest()
        {
            string contactName = "Robert";
            IList<Contract> contracts = contractRepository.FindByContact(contactName);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(6, contracts.Count, "Contract Count");
        }

        [Test]
        public void FindByPlanIdTest()
        {
            int planId = 1;
            IList<Contract> contracts = contractRepository.FindByPlanId(planId);

            Assert.IsNotNull(contracts, "Contracts Collection");
            Assert.AreEqual(11, contracts.Count, "Contract Count");
        }        
    }
}
