﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Services.Tests;
using User=SharpStudios.AirtimeBilling.Core.User;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for UserRepositoryTests
    /// </summary>
    [TestFixture]
    [Ignore]
    public class UserRepositoryTests : RepositoryFixture 
    {      
        IUserRepository userRepository;
       
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            userRepository = RepositoryFactory.GetRepository<IUserRepository>();
        }
       
        [Test]
        public void GetContactUserTest()
        {
            User entity = userRepository.GetUser("rob", "mypassword");

            Assert.IsNotNull(entity, "User");            
            Assert.IsInstanceOf(typeof(ContactUser),entity);
            Assert.IsNotInstanceOf(typeof (AgentUser), entity);
        }

        [Test]
        public void GetLandwideUserTest()
        {
            User entity = userRepository.GetUser("landwide", "mypassword");

            Assert.IsNotNull(entity, "User");
            Assert.IsInstanceOf(typeof(User), entity);
            Assert.IsNotInstanceOf(typeof(AgentUser), entity);
            Assert.IsNotInstanceOf(typeof(ContactUser), entity);
        }

        [Test]
        public void GetAgentUserTest()
        {
            User entity = userRepository.GetUser("britz", "mypassword");

            Assert.IsNotNull(entity, "User");
            Assert.IsInstanceOf(typeof(AgentUser), entity);            
            Assert.IsNotInstanceOf(typeof(ContactUser), entity);
        }
    }
}
