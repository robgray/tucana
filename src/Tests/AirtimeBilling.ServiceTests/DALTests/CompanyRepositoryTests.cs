﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

using SharpStudios.AirtimeBilling.Services;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.DataAccess.Repositories;
using SharpStudios.AirtimeBilling.Core.Repositories;
using SharpStudios.AirtimeBilling.Services.Tests;
using Company=SharpStudios.AirtimeBilling.Core.Company;

namespace SharpStudios.AirtimeBilling.Services.Tests.DALTests
{
    /// <summary>
    /// Summary description for CompanyRepositoryTests
    /// </summary>
    [TestFixture]
    public class CompanyRepositoryTests : RepositoryFixture
    {     
        private ICompanyRepository companyRepository;
     
        [SetUp]
        public override void MyTestInitialize() 
        {
            base.MyTestInitialize();
            companyRepository = RepositoryFactory.GetRepository<ICompanyRepository>();
        }

        [Test]
        public void ThisGetTest()
        {
            Company company = companyRepository[1];
            Assert.IsNotNull(company);
            Assert.AreEqual(1, company.Id.Value);
        }

        [Test]
        public void ThisSetTest()
        {
            Company company = new Company(1) { CompanyName = "Mikel Solutions", ABN = null, ACN = null };
            companyRepository[1] = company;

            Assert.AreEqual("Mikel Solutions",companyRepository[1].CompanyName);
        }

        [Test]
        public void GetCompanyTest()
        {
            Company company = companyRepository.GetCompany(1);
            Assert.IsNotNull(company);
            Assert.AreEqual(1, company.Id.Value);
        }

        [Test]
        public void GetAllCompaniesTest()
        {
            IList<Company> companies = companyRepository.GetAllCompanies();
            Assert.AreEqual(3, companies.Count);
        }

        [Test]
        public void InsertCompanyTest()
        { 
            Company company = new Company()
            {
                CompanyName = "Megacorp",
                ACN = null,
                ABN = null,
            };

            companyRepository.InsertCompany(company);
            Assert.IsNotNull(company.Id);
        }

        [Test]
        public void UpdateCompanyTest()
        {
            Company entity = new Company(1)
            {                
                CompanyName = "Mikel Software"
            };

            bool success = companyRepository.UpdateCompany(entity);
            Assert.IsTrue(success);
        }
    }
}
