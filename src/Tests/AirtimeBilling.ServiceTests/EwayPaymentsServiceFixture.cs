﻿using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using NUnit.Framework;
using Rhino.Mocks;

namespace AirtimeBilling.Services.Tests
{
    [TestFixture]
    public class EwayPaymentsServiceFixture : BaseServiceFixture
    {
        private IInvoiceRepository invoiceRepository;
        private IAccountRepository accountRepository;
        private IContactRepository contactRepository;
        private IEwayProcessedDataRepository ewayProcessedDataRepository;
        private IInvoiceHardcopyRepository invoiceHardcopyRepository;


        private IEwayPaymentsService ewayService;

        [SetUp]
        public void SetUp()
        {
            invoiceRepository = MockRepository.GenerateMock<IInvoiceRepository>();
            accountRepository = MockRepository.GenerateMock<IAccountRepository>();
            contactRepository = MockRepository.GenerateMock<IContactRepository>();
            ewayProcessedDataRepository = MockRepository.GenerateMock<IEwayProcessedDataRepository>();
            invoiceHardcopyRepository = MockRepository.GenerateMock<IInvoiceHardcopyRepository>();

            ewayService = new EwayPaymentsService(invoiceRepository, accountRepository, contactRepository, ewayProcessedDataRepository, invoiceHardcopyRepository);
        }

        [Test]
        public void ExportCreditCardChargeFile_does_not_have_header_fields()
        {
            invoiceRepository.Expect(r => r.FindInvoicesByRunNumber(1)).Return(new List<Invoice>());

            var csv = ewayService.ExportCreditCardChargeFile(1);

            Assert.IsEmpty(csv);
        }       
    }
}
