﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using NUnit.Framework;

namespace AirtimeBilling.Services.Tests
{
    [TestFixture]
    public class EwayLineItemFixture
    {
        [Test]           
        public void Can_create_ewaylineitem()
        {
            var invoice = new Invoice { InvoiceNumber = "10000", FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1), TotalIncGst = 100M };
            var contact = new Contact
                              {
                                  CreditCard =
                                      new CreditCard
                                          {
                                              CardNumber = "1234567890",
                                              CreditCardType = CreditCardType.Visa,
                                              ExpiryDate = "11/12",
                                              NameOnCard = "Test User"
                                          },
                                  Name = "Test User",
                                  Email = "test@user.com",
                                  HomeAddress =
                                      new Address()
                                          {
                                              Street = "123 Here St",
                                              Suburb = "Hereford",
                                              State = "NSW",
                                              Postcode = "2222"
                                          },
                              };

            var line = EwayLineItem.Create(invoice, contact);

            Assert.AreEqual(10000, line.TotalAmount);
            Assert.AreEqual("Test", line.CustomerFirstName);
            Assert.AreEqual("User", line.CustomerLastName);
            Assert.AreEqual("test@user.com", line.CustomerEmail);
            Assert.AreEqual("123 Here St Hereford NSW 2222", line.CustomerAddress);
            Assert.AreEqual("2222", line.CustomerPostCode);
            Assert.AreEqual("I Satellite Airtime", line.CustomerInvoiceDescription);
            Assert.AreEqual("10000", line.CustomerInvoiceRef);
            Assert.AreEqual("Test User", line.CardHoldersName);
            Assert.AreEqual("1234567890", line.CardNumber);
            Assert.AreEqual("12", line.CardExpiryYear);
            Assert.AreEqual("11", line.CardExpiryMonth);
        }

        [Test]
        public void TotalAmount_is_correct_when_fractions_of_cents_is_low()
        {
            var contact = FakesHelper.CreateContact();
            var invoice = new Invoice { InvoiceNumber = "10000", FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1), TotalIncGst = 100.23451M };

            var line = EwayLineItem.Create(invoice, contact);

            Assert.AreEqual(10023, line.TotalAmount);
        }

        [Test]
        public void TotalAmount_is_correct_when_fractions_of_cents_is_high()
        {
            var contact = FakesHelper.CreateContact();
            var invoice = new Invoice { InvoiceNumber = "10000", FromDate = DateTime.Now.AddMonths(-2), ToDate = DateTime.Now.AddMonths(-1), TotalIncGst = 100.23851M };

            var line = EwayLineItem.Create(invoice, contact);

            Assert.AreEqual(10024, line.TotalAmount);
        }
    }
}
