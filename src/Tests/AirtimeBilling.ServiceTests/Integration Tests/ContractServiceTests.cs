﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Services;
using SharpStudios.AirtimeBilling.Core;
using SharpStudios.AirtimeBilling.Common;

namespace SharpStudios.AirtimeBilling.Services.Tests.Integration_Tests
{
    /// <summary>
    /// Summary description for ContractServiceTests
    /// </summary>
    [TestFixture]
    public class ContractServiceTests
    {
        ContractService service;
       
        private UpdateContractRequest updateRequest;
       
        [SetUp]
        public void MyTestInitialize() 
        {
            service = new ContractService();

            updateRequest = new UpdateContractRequest()
            {
                ContractId = 1,
                PlanId = 2,
                ActivationDate = DateTime.Parse("12/11/2008"),
                EndDate = DateTime.Parse("12/11/2009"),
                UsedBy = "Michael",
                Pin = "6666",
                Puk = "6666",
                PhoneNumber1 = "012344394",
                PhoneNumber2 = "430493304",
                PhoneNumber3 = "324433344",
                PhoneNumber4 = "443343433",
            };
        }

        #region ApproveInvoicing Tests

        [Test]
        public void ApproveInvoicingTest()
        {
            var request = new ApproveInvoicingRequest()
            {
                AccountId = 3,
                IsApprovalGranted = true,
                User = new User() { Username = "UNITTEST" }
            };
            var response = service.ApproveInvoicing(request);

            Assert.IsTrue(response.IsSuccessful, response.Message);
        }

        [Test]
        public void ApproveInvoicingInvalidUserTest()
        {
            var request = new ApproveInvoicingRequest()
            {
                AccountId = 3,
                IsApprovalGranted = true,
                User = new AgentUser() { Username = "UNITTEST" }
            };
            var response = service.ApproveInvoicing(request);

            Assert.IsFalse(response.IsSuccessful, response.Message);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message, "Message");
        }

        [Test]
        public void DenyApproveInvoicingTest()
        {            
            var request = new ApproveInvoicingRequest() { AccountId = 3, IsApprovalGranted = false, User = new User() { Username = "UNITTEST" } };
            var response = service.ApproveInvoicing(request);

            Assert.IsTrue(response.IsSuccessful, response.Message);
        }

        [Test]
        public void ApproveInvoicingNoAccountTest()
        {
            var request = new ApproveInvoicingRequest() { AccountId = 4, IsApprovalGranted = true, User = new User() { Username = "UNITTEST" } };
            var response = service.ApproveInvoicing(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Account Id does not exist", response.Message);
        }

        [Test]
        public void ApproveInvoicingAccountAlreadyInvoicingTest()
        {
            var request = new ApproveInvoicingRequest() { AccountId = 3, IsApprovalGranted = true, User = new User() { Username = "UNITTEST" } };
            var response = service.ApproveInvoicing(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Account is already billing by Invoice", response.Message);
        }

        #endregion

        #region SubmitNewContractForAccount Tests

        [Test]
        public void SubmitNewContractForAccountNoAccountTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.PlanId = 1;
            request.ActivationDate = DateTime.Parse("2008-10-15");
            request.EndDate = DateTime.Parse("2010-10-15");
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Account 'LW0001' does not exist.", response.Message);
        }

        [Test]
        public void SubmitNewContractForAccountInvalidPasswordTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Parse("2008-10-15");
            request.EndDate = DateTime.Parse("2010-10-15");
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Supplied Account Password does not match.", response.Message);
        }

        [Test]
        public void SubmitNewContractForAccountEndDateBeforeActivationDateTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Parse("2008-10-15");
            request.EndDate = DateTime.Parse("2008-10-14");
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after contract activation date.", response.Message);
        }

        [Test]
        public void SubmitNewContractForAccountEndDateBeforeTodayTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Today.AddDays(-10);
            request.EndDate = DateTime.Today.AddDays(-9);
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after the current date.", response.Message);
        }

        [Test]
        public void SubmitNewContractForAccountIncompleteIMEINumbersTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Today.AddDays(1);
            request.EndDate = DateTime.Today.AddDays(3);
            request.IMEINumber = "";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Both an IMEI Number and sim card number must be supplied.", response.Message);
        }

        [Test]
        public void SubmitNewContractForAccountMissingPlanTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Today.AddDays(1);
            request.EndDate = DateTime.Today.AddDays(3);
            request.IMEINumber = "12323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 0;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("A plan must be selected for the Contract.", response.Message);
        }


        [Test]
        public void SubmitNewContractForAccountTest()
        {
            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Parse("2008-10-15");
            request.EndDate = DateTime.Parse("2010-10-15");
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { IsAuthenticated = true, Username = "Test" };

            NewContractResponse response = service.SubmitNewContractForAccount(request);

            Assert.IsTrue(response.IsSuccessful);
        }

        #endregion

        #region SubmitNewContract Tests

        [Test]
        public void SubmitNewContractNoNameTest()
        {            
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.Contact.Name = null;

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("No contact name has been supplied.", response.Message);
        }

        [Test]
        public void SubmitNewContractBadPostalAddressTest()
        {
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.Contact.PostalAddress = new Address {
                                                            Street = request.Contact.PostalAddress.Street,
                                                            Suburb = request.Contact.PostalAddress.Suburb,                                                            
                                                            Postcode = request.Contact.PostalAddress.Postcode
            };

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Please ensure postal address details have been filled out.\n" +
                    "Including street, suburb, state, and postcode", response.Message);
        }

        [Test]
        public void SubmitNewContractEndDateBeforeActivationDateTest()
        {
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.ActivationDate = request.EndDate.AddDays(2);

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after contract activation date.", response.Message);
        }

        [Test]
        public void SubmitNewContractEndDateBeforeTodayTest()
        {
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.ActivationDate = DateTime.Today.AddDays(-2);
            request.EndDate = DateTime.Today;

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after the current date.", response.Message);
        }

        [Test]
        public void SubmitNewContractBothIMEIAndSimcardFilledTest()
        {
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.IMEINumber = null;

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Both an IMEI Number and sim card number must be supplied.", response.Message);
        }

        [Test]
        public void SubmitNewContractTest()
        {            
            // Creates an correctly populated NewContractRequest instance
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.User = new User() { Username = "UNITTEST", Email = "test@test.com" };

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsTrue(response.IsSuccessful);
        }

        [Test]
        public void SubmitNewContractAgentUserTest()
        {            
            // Creates an correctly populated NewContractRequest instance
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.User = new AgentUser() { Username = "UNITTESTAGENT", AgentId = 3, Email = "testagent@test.com" };

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsTrue(response.IsSuccessful);
        }

        [Test]
        public void SubmitNewContractContactUserTest()
        {          
            // Creates an correctly populated NewContractRequest instance
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.User = new ContactUser() { Username = "UNITTESTCONTACT", ContactId = 3, Email = "testcontact@test.com" };

            NewContractResponse response = service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);
        }

        private NewContractRequest CreateNewContractRequestFullyPopulated()
        {
            NewContractRequest request = new NewContractRequest();

            request.Contact.Name = "Robert Gray"; 
            request.Contact.PostalAddress = new Address { Street = "22 Turrbal St",
                                                        Suburb = "Bellbowrie",
                                                        State = "QLD",
                                                        Postcode = "4070"};
            request.Contact.HomeAddress = new Address
            {
                Street = "22 Turrbal St",
                Suburb = "Bellbowrie",
                State = "QLD",
                Postcode = "4070"
            };
            request.Contact.MobilePhone = "0411331273";
            request.Contact.CreditCard = new CreditCard
                                             {
                                                 CardNumber = "12345865482343",
                                                 NameOnCard = "Mr Robert A Gray",
                                                 ExpiryDate = "03/10",
                                                 CreditCardType = CreditCardType.Visa
                                             };
            request.Contact.DriversLicense = new DriversLicense
                                                 {
                                                     LicenseNumber = "123482342",
                                                     DateOfBirth = DateTime.Parse("1976-08-21"),
                                                     Expiry = DateTime.Parse("2010-06-01")
                                                 };
            request.BillingMethod = BillingMethod.Invoice;
            request.BillingAddressType = BillingAddressType.Postal;
            request.ActivationDate = DateTime.Parse("2008-12-01");
            request.EndDate = DateTime.Parse("2010-12-01");
            request.Data = false;
            request.MessageBank = false;
            request.IMEINumber = "123232323";
            request.SimCard = "123232323";
            request.UsedBy = "Rob";
            request.IsBusinessContract = false;
            request.PlanId = 1;
            request.User = new User { Username = "UNITTEST" };

            return request;
        }

        #endregion

        #region ViewContract Tests

        [Test]
        public void ViewContactTest()
        {            
            ViewContractRequest request = new ViewContractRequest();
            request.User = new User() { Username = "UNITTEST", Email = "test@test.com" };
            request.ContractId = 13;

            ViewContractResponse response = service.ViewContract(request);
            Assert.IsFalse(response.IsReadOnly, "Read Only");
            Assert.IsNotNull(response.Contract, "Contract");
            Assert.IsNotNull(response.Account, "Account");
            Assert.IsNotNull(response.Company, "Company");
            Assert.IsNotNull(response.Contact.DriversLicense, "Drivers License");
            Assert.IsNotNull(response.Contact.CreditCard, "Credit Card");
            Assert.IsNotNull(response.Contact.HomeAddress, "Home Address");
            Assert.IsNotNull(response.Contact.PostalAddress, "Home Address");

            Assert.AreEqual(13, response.Contract.Id.Value);
            Assert.AreEqual(1, response.Account.Id.Value);
            Assert.AreEqual(3, response.Company.Id.Value);
            Assert.AreEqual(3, response.Contact.Id.Value);
        }

        [Test]
        public void ViewContactAgentUserTest()
        {            
            ViewContractRequest request = new ViewContractRequest();
            request.User = new AgentUser() { Username = "UNITTESTAGENT", Email = "testagent@test.com" };
            request.ContractId = 13;

            ViewContractResponse response = service.ViewContract(request);
            Assert.IsTrue(response.IsReadOnly, "Read Only");
            Assert.IsNotNull(response.Contract, "Contract");
            Assert.IsNotNull(response.Account, "Account");
            Assert.IsNotNull(response.Company, "Company");
            Assert.IsNotNull(response.Contact.DriversLicense, "Drivers License");
            Assert.IsNotNull(response.Contact.CreditCard, "Credit Card");
            Assert.IsNotNull(response.Contact.HomeAddress, "Home Address");
            Assert.IsNotNull(response.Contact.PostalAddress, "Home Address");

            Assert.AreEqual(13, response.Contract.Id.Value);
            Assert.AreEqual(1, response.Account.Id.Value);
            Assert.AreEqual(3, response.Company.Id.Value);
            Assert.AreEqual(3, response.Contact.Id.Value);
        }

        [Test]
        public void ViewContactContactUserTest()
        {            
            ViewContractRequest request = new ViewContractRequest();
            request.User = new ContactUser() { Username = "UNITTESTCONTACT", Email = "testcontact@test.com" };
            request.ContractId = 13;

            ViewContractResponse response = service.ViewContract(request);
            Assert.IsTrue(response.IsReadOnly, "Read Only");
            Assert.IsNotNull(response.Contract, "Contract");
            Assert.IsNotNull(response.Account, "Account");
            Assert.IsNotNull(response.Company, "Company");
            Assert.IsNotNull(response.Contact.DriversLicense, "Drivers License");
            Assert.IsNotNull(response.Contact.CreditCard, "Credit Card");
            Assert.IsNotNull(response.Contact.HomeAddress, "Home Address");
            Assert.IsNotNull(response.Contact.PostalAddress, "Home Address");

            Assert.AreEqual(13, response.Contract.Id.Value);
            Assert.AreEqual(1, response.Account.Id.Value);
            Assert.AreEqual(3, response.Company.Id.Value);
            Assert.AreEqual(3, response.Contact.Id.Value);
        }


        #endregion

        #region FindContracts Tests

        [Test]
        public void FindContractsByContractNumberTest()
        {
            // Setup mock behaviour
            IList<Contract> contracts = new List<Contract>();
            contracts.Add(new Contract(1, DateTime.Now) { AccountId = 1, ContractNumber = "CT9999", ContractStatus = ContractStatus.Active });
       
            User user = new User() { Username = "test", IsActive = true, Email = "test@test.com", IsAuthenticated = true, Name = "Test" };
            ContractSearchRequest request = new ContractSearchRequest()
            {
                SearchValue = "CT",
                SearchType = ContractSearchField.ContractNumber,
                SearchStatus = ContractStatus.Active,
                SearchContractStatusType = SearchContractStatusType.UseStatus,
                User = user
            };
            ContractSearchResponse response = service.FindContracts(request);

            Assert.IsNotNull(response, "Response is null");
            Assert.IsTrue(response.IsSuccessful, "Response is not successful");
            Assert.IsNotNull(response.Contracts, "Contracts is null");
            Assert.AreEqual(1, response.Contracts.Count, "Contacts count");
        }

        [Test]
        public void FindContractsByPhoneNumberTest()
        {
            // Setup mock behaviour
            IList<Contract> contracts = new List<Contract>();
            contracts.Add(new Contract(1, DateTime.Now) { AccountId = 1, ContractNumber = "CT9999", ContractStatus = ContractStatus.Active, PhoneNumber1 = "093233904", PhoneNumber2 = "0339430449" });
         
            User user = new User() { Username = "test", IsActive = true, Email = "test@test.com", IsAuthenticated = true, Name = "Test" };
            ContractSearchRequest request = new ContractSearchRequest()
            {
                SearchValue = "0339430449",
                SearchType = ContractSearchField.PhoneNumber,
                SearchStatus = ContractStatus.Active,
                SearchContractStatusType = SearchContractStatusType.UseStatus,
                User = user
            };
            ContractSearchResponse response = service.FindContracts(request);

            Assert.IsNotNull(response, "Response is null");
            Assert.IsTrue(response.IsSuccessful, "Response is not successful");
            Assert.IsNotNull(response.Contracts, "Contracts is null");
            Assert.AreEqual(1, response.Contracts.Count, "Contacts count");
            Assert.AreEqual("0339430449", response.Contracts[0].PhoneNumber, "Phone Number");
        }


        #endregion

        #region UpdateContract Tests

        [Test]
        public void UpdateContractTest()
        {    
            updateRequest.User = new User() { Username = "UNITTEST", Email = "test@test.com" };
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsTrue(response.IsSuccessful, response.Message);
            Assert.IsNotNull(response.Contract, "Null Contract");
            Assert.AreEqual(response.Contract.ContractStatus, ContractStatus.Active, "Contract Status");
        }

        [Test]
        public void UpdateContractAgentUserTest()
        {
            updateRequest.User = new AgentUser() { Username = "UNITTESTAGENT", Email = "testagent@test.com" };
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);

        }

        [Test]
        public void UpdateContractCustomerUserTest()
        {
            updateRequest.User = new ContactUser() { Username = "UNITTESTCONTACT", Email = "testcontact@test.com" };
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);
        }

        [Test]
        public void UpdateContractNoUserTest()
        {
            updateRequest.User = null;
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);
        }

        [Test]
        public void UpdateContractFailedUpdateDatabaseTest()
        {           
            updateRequest.User = new User() { Username = "UNITTEST", Email = "test@test.com" };
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.INTERNAL_ERROR, response.Message, "Error Message");
        }

        [Test]
        public void UpdateContractCouldNotFindContractTest()
        {
            updateRequest.User = new User() { Username = "UNITTEST", Email = "test@test.com" };
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.INTERNAL_ERROR, response.Message, "Error Message");
        }

        [Test]
        public void UpdateContractVerifyPlanTest()
        {
            updateRequest.User = new User() { Username = "UNITTEST", Email = "test@test.com" };
            UpdateContractResponse response = service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("A Valid Plan must be supplied", response.Message);
        }

        #endregion

        #region Plan Variance Tests

        [Test]
        public void PlanVarianceTest()
        {           
            VaryContractPlanRequest request = new VaryContractPlanRequest()
            {
                ContractId = 4,
                NewPlanId = 3,
                Cost = 0M,
                StartDate = DateTime.Parse("2009-JAN-15"),
                EndDate = DateTime.Parse("2009-MAR-30"),
                User = new User() { Username = "UNITTEST" }
            };

            VaryContractPlanResponse response = service.VaryContractPlan(request);
            Assert.IsTrue(response.IsSuccessful);
        }

        #endregion 
    }
}
