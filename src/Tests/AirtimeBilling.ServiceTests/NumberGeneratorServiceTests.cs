﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using NUnit.Framework;
using Moq;

namespace AirtimeBilling.Services.Tests
{
    /// <summary>
    /// Summary description for NumberGeneratorServiceTests
    /// </summary>
    [TestFixture]
    public class NumberGeneratorServiceTests
    {

        private INumberGeneratorService _service;
        private Mock<ISettingRepository> _settingRepository;

        [SetUp]
        public void MyTestInitialize()
        {
            _settingRepository = new Mock<ISettingRepository>();
            _service = new NumberGeneratorService(_settingRepository.Object);
            
        }
               
        [Test]
        public void NextInvoiceNumber_returns_next_invoice_number()
        {
            _settingRepository.ExpectGet(s => s["NextInvoiceNumber"]).Returns(new Setting(1) {Value = "1"});
            
            var invoiceNumber = _service.NextInvoiceNumber();

            Assert.AreEqual("1", invoiceNumber);
        }

        [Test]
        public void NextInvoiceNumber_saves_next_invoice_number()
        {
            var setting = new Setting(1) {Value = "1"};
            _settingRepository.ExpectGet(s => s["NextInvoiceNumber"]).Returns(setting);
            
            _service.NextInvoiceNumber();
            
            _settingRepository.Verify(s => s.Commit(It.Is<Setting>(v => v.Value == "2")));
        }

        [Test]        
        public void NextInvoiceNumber_missing_next_invoice_number_setting_throw_exception()
        {
            _settingRepository.ExpectGet(s => s["NextInvoiceNumber"]).Returns(It.Is<Setting>(null));
                        
            var exception = Assert.Throws<Exception>(() => _service.NextInvoiceNumber());
            Assert.AreEqual("Unable to find Invoice Number", exception.Message);            
        }

        [Test]
        public void NextInvoiceRunNumber_returns_next_invoice_run_number()
        {
            _settingRepository.ExpectGet(s => s["NextInvoiceRunNumber"]).Returns(new Setting(1) { Value = "1" });

            var invoiceRunNumber = _service.NextInvoiceRunNumber();

            Assert.AreEqual(1, invoiceRunNumber);
        }

        [Test]
        public void NextInvoiceRunNumber_saves_next_invoice_run_number()
        {
            var setting = new Setting(1) { Value = "1" };
            _settingRepository.ExpectGet(s => s["NextInvoiceRunNumber"]).Returns(setting);

            _service.NextInvoiceRunNumber();

            _settingRepository.Verify(s => s.Commit(It.Is<Setting>(v => v.Value == "2")));
        }

        [Test]
        public void NextInvoiceRunNumber_missing_next_invoice__run_number_setting_throw_exception()
        {
            _settingRepository.ExpectGet(s => s["NextInvoiceRunNumber"]).Returns(It.Is<Setting>(null));

            var exception = Assert.Throws<Exception>(() => _service.NextInvoiceRunNumber());
            Assert.AreEqual("Unable to find Invoice Run Number", exception.Message);
        }


        [Test]
        public void NextContractNumber_returns_next_contract_number_using_format_and_prefix()
        {
            _settingRepository.ExpectGet(s => s["NextContractNumber"]).Returns(new Setting(1) { Value = "1" });
            _settingRepository.ExpectGet(s => s["ContractNumberPrefix"]).Returns(new Setting(2) { Value = "CT" });
            _settingRepository.ExpectGet(s => s["ContractNumberFormat"]).Returns(new Setting(3) { Value = "0000" });

            var contractNumber = _service.NextContractNumber();

            Assert.AreEqual("CT0001", contractNumber);
        }

        [Test]
        public void NextContractNumber_saves_next_contract_number()
        {
            var setting = new Setting(1) { Value = "1" };
            _settingRepository.ExpectGet(s => s["NextContractNumber"]).Returns(setting);
            _settingRepository.ExpectGet(s => s["ContractNumberPrefix"]).Returns(new Setting(2) { Value = "CT" });
            _settingRepository.ExpectGet(s => s["ContractNumberFormat"]).Returns(new Setting(3) { Value = "0000" });


            _service.NextContractNumber();

            _settingRepository.Verify(s => s.Commit(It.Is<Setting>(v => v.Value == "2")));
        }

        [Test]
        public void NextContractNumber_throw_exception_on_missing_next_contract_number()
        {
            _settingRepository.ExpectGet(s => s["NextContractNumber"]).Returns(It.Is<Setting>(null));
                        
            var exception = Assert.Throws<Exception>(() => _service.NextContractNumber());
            Assert.AreEqual("Unable to find Contract Number", exception.Message);            
        }

        [Test]
        public void NextContractNumber_throw_exception_on_missing_contract_number_prefix()
        {
            _settingRepository.ExpectGet(s => s["NextContractNumber"]).Returns(new Setting(1) { Value = "1" });            
            _settingRepository.ExpectGet(s => s["ContractNumberPrefix"]).Returns(It.Is<Setting>(null));

            var exception = Assert.Throws<Exception>(() => _service.NextContractNumber());
            Assert.AreEqual("Unable to find Contract Number Prefix", exception.Message);
        }

        [Test]
        public void NextContractNumber_throw_exception_on_missing_contract_number_format()
        {
            _settingRepository.ExpectGet(s => s["NextContractNumber"]).Returns(new Setting(1) { Value = "1" });
            _settingRepository.ExpectGet(s => s["ContractNumberPrefix"]).Returns(new Setting(2) { Value = "CT" });            
            _settingRepository.ExpectGet(s => s["ContractNumberFormat"]).Returns(It.Is<Setting>(null));

            var exception = Assert.Throws<Exception>(() => _service.NextContractNumber());
            Assert.AreEqual("Unable to find Contract Number Format", exception.Message);
        }

        [Test]
        public void NextAccountNumber_returns_next_account_number_using_format_and_prefix()
        {
            _settingRepository.ExpectGet(s => s["NextAccountNumber"]).Returns(new Setting(1) { Value = "1" });
            _settingRepository.ExpectGet(s => s["AccountNumberPrefix"]).Returns(new Setting(2) { Value = "AC" });
            _settingRepository.ExpectGet(s => s["AccountNumberFormat"]).Returns(new Setting(3) { Value = "0000" });
            
            var accountNumber = _service.NextAccountNumber();

            Assert.AreEqual("AC0001", accountNumber);
        }

        [Test]
        public void NextAccountNumber_saves_next_account_number()
        {
            var setting = new Setting(1) { Value = "1" };
            _settingRepository.ExpectGet(s => s["NextAccountNumber"]).Returns(setting);
            _settingRepository.ExpectGet(s => s["AccountNumberPrefix"]).Returns(new Setting(2) { Value = "AC" });
            _settingRepository.ExpectGet(s => s["AccountNumberFormat"]).Returns(new Setting(3) { Value = "0000" });

            _service.NextAccountNumber();

            _settingRepository.Verify(s => s.Commit(It.Is<Setting>(v => v.Value == "2")));
        }

        [Test]
        public void NextAccountNumber_throw_exception_on_missing_next_account_number()
        {
            _settingRepository.ExpectGet(s => s["NextAccountNumber"]).Returns(It.Is<Setting>(null));

            var exception = Assert.Throws<Exception>(() => _service.NextAccountNumber());
            Assert.AreEqual("Unable to find Account Number", exception.Message);
        }

        [Test]
        public void NextAccountNumber_throw_exception_on_missing_account_number_prefix()
        {
            _settingRepository.ExpectGet(s => s["NextAccountNumber"]).Returns(new Setting(1) { Value = "1" });
            _settingRepository.ExpectGet(s => s["AccountNumberPrefix"]).Returns(It.Is<Setting>(null));

            var exception = Assert.Throws<Exception>(() => _service.NextAccountNumber());
            Assert.AreEqual("Unable to find Account Number Prefix", exception.Message);
        }

        [Test]
        public void NextAccountNumber_throw_exception_on_missing_account_number_format()
        {
            _settingRepository.ExpectGet(s => s["NextAccountNumber"]).Returns(new Setting(1) { Value = "1" });
            _settingRepository.ExpectGet(s => s["AccountNumberPrefix"]).Returns(new Setting(2) { Value = "AC" });
            _settingRepository.ExpectGet(s => s["AccountNumberFormat"]).Returns(It.Is<Setting>(null));

            var exception = Assert.Throws<Exception>(() => _service.NextAccountNumber());
            Assert.AreEqual("Unable to find Account Number Format", exception.Message);
        }
    }
}
