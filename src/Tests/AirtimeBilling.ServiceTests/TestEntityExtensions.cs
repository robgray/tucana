﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.Services;

namespace AirtimeBilling.Services.Tests
{
    public static class TestEntityExtensions
    {
        public static InvoiceLine GetCallLine(this Invoice invoice, Call call, int contractId)
        {
            return new InvoiceLine
            {
                ContractId = contractId,
                Call = call,
                Description = "TEST CALL to " + call.NumberCalled,
                Invoice = invoice,
                Quantity = Convert.ToDouble(call.Volume),
                GstAmount = call.Cost / 11M,
                LineAmount = call.Cost
            };
        }

        public static InvoiceLine GetCallLine(this Invoice invoice, Contract contract, Call call)
        {
            return new InvoiceLine
            {
                ContractId = contract.Id.Value,
                Call = call,
                Description = "TEST CALL to " + call.NumberCalled,
                Invoice = invoice,
                Quantity = Convert.ToDouble(call.Volume),
                GstAmount = call.Cost / 11M,
                LineAmount = call.Cost
            };
        }

        public static InvoiceLine GetEnvironmentalLine(this Invoice invoice, IInvoiceSettings settings)
        {
            return new InvoiceLine
            {
                ContractId = null,
                Call = null,
                Description = settings.EnvironmentalLevyDescription,
                Invoice = invoice,
                Quantity = 1.0,
                LineAmount = 2.50M,
                GstAmount = 2.50M / 11
            };
        }

        public static InvoiceLine GetNetworkAccessLine(this Invoice invoice, Contract contract, Plan plan, IInvoiceSettings settings)
        {
            return new InvoiceLine
            {
                Call = null,
                Description = settings.NetworkAccessFeeDescription,
                Invoice = invoice,
                Quantity = 1.0,
                ContractId = contract.Id.Value,
                LineAmount = plan.PlanAmount,
                GstAmount = plan.PlanAmount / 11
            };
        }

        public static InvoiceLine GetContractSuspensionLine(this Invoice invoice, Contract contract, IInvoiceSettings settings)
        {
            return new InvoiceLine
            {
                ContractId = contract.Id.Value,
                Call = null,
                Description = settings.ContractSuspensionDescription,
                Invoice = invoice,
                Quantity = 1,
                LineAmount = 10.00M,
                GstAmount = 10.00M / 11M
            };
        }        

        public static InvoiceLine GetFreeCallComponent(this Invoice invoice, Contract contract, Plan plan, IInvoiceSettings settings)
        {
            return new InvoiceLine
            {
                ContractId = contract.Id.Value,
                Call = null,
                Description = settings.FreeCallComponentDescription,
                Invoice = invoice,
                GstAmount = -plan.FreeCallAmount / 11M,
                LineAmount = -plan.FreeCallAmount
            };
        }

        public static InvoiceLine GetFreeCallComponent(this Invoice invoice, Contract contract, decimal freeCallAmount, IInvoiceSettings settings)
        {

            return new InvoiceLine
            {
                ContractId = contract.Id.Value,
                Call = null,
                Description = settings.FreeCallComponentDescription,
                Invoice = invoice,
                GstAmount = -freeCallAmount / 11M,
                LineAmount = -freeCallAmount
            };
        }
    }

}
