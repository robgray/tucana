﻿using System;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Services.DTO;
using NUnit.Framework;

namespace AirtimeBilling.Services.Tests
{
    /// <summary>
    /// Summary description for BaseServiceTests
    /// </summary>
    [TestFixture]
    public class BaseServiceFixture
    {        
        protected static User CreateIUser()
        {
            return new User
                   {
                       Username = "IUser",
                       Email = "airtime@airtimebilling.com.au",
                       Name = "I Test User",
                       IsAuthenticated = true,
                       IsActive = true
                   };
        }

        protected static AgentUser CreateAgentUser()
        {
            return new AgentUser
                   {
                       AgentId = 1,
                       Username = "PhoneAgent",
                       Email = "agent@agent.com.au",
                       Name = "Phone Agent",
                       IsActive = true,
                       IsAuthenticated = true
                   };
        }

        protected static ContactUser CreateCustomerUser()
        {
            return new ContactUser
                   {
                       Username = "Customer",
                       ContactId = 1,
                       Name = "Test User",
                       Email = "test.customer@myemail.com.au",
                       IsActive = true,
                       IsAuthenticated = true           
                   };
        }

        protected static T CreateIUserRequest<T>() where T : RequestBase
        {
            var request = Activator.CreateInstance<T>();
            request.User = CreateIUser();

            return request;
        }

        protected static T CreateAgentRequest<T>() where T : RequestBase
        {
            var request = Activator.CreateInstance<T>();
            request.User = CreateAgentUser();

            return request;
        }

        protected static T CreateCustomerRequest<T>() where T : RequestBase
        {
            var request = Activator.CreateInstance<T>();
            request.User = CreateCustomerUser();

            return request;
        }
    }
}
