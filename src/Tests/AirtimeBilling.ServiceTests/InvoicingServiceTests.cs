﻿using System;
using System.Collections.Generic;
using System.Linq;
using AirtimeBilling.Common;
using AirtimeBilling.Core;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using Moq;
using NUnit.Framework;

namespace AirtimeBilling.Services.Tests
{
    /// <summary>
    /// Summary description for InvoicingServiceTests
    /// </summary>
    [TestFixture]
    public class InvoicingServiceTests
    {
        private Mock<INumberGeneratorService> _generator;
        private Mock<IInvoiceRepository> _invoiceRepository;
        private Mock<IAccountRepository> _accountRepository;
        private Mock<IPlanRepository> _planRepository;
        private Mock<INetworkRepository> _networkRepository;
        private Mock<IContractRepository> _contractRepository;
        private Mock<IContactRepository> _contactRepository;
        private Mock<ICompanyRepository> _companyRepository;
        private Mock<ISettingRepository> _settingsRepository;
        private Mock<IActivityLoggingService> _activityService;
        private Mock<ICustomFeeRepository> _customFeeRepository;
        private Mock<IInvoiceHardcopyRepository> _hardcopyRepository;
        private Mock<IContractService> _contractService;
        private Mock<IDateTimeFacade> _dateTime;
        private Mock<IEwayPaymentsService> _ewayService;
        
        // Dont' want to mock.  but we mock the supplied ISettingRepository to constructor.
        // needs to be in it's own test suite!
        private IInvoiceSettings _invoiceSettings;

        private IInvoicingService _service;

        private Contact _rootContact;
        private Contract _rootContract;
        private Contract _subContract;
        private MasterAccount _rootAccount;
        private SubAccount _subAccount;
        private Plan _rootContractPlan;

        private InvoiceLineEqualityComparer _lineComparer;

        private InvoiceFactory _factory;

        private const string _invoiceNumber = "10050";
        private DateTime _today;

        [SetUp]
        public void SetUp()
        {
            _lineComparer = new InvoiceLineEqualityComparer();

            _generator = new Mock<INumberGeneratorService>();
            _invoiceRepository = new Mock<IInvoiceRepository>();
            _accountRepository = new Mock<IAccountRepository>();
            _planRepository = new Mock<IPlanRepository>();
            _networkRepository = new Mock<INetworkRepository>();
            _contractRepository = new Mock<IContractRepository>();
            _contactRepository = new Mock<IContactRepository>();
            _companyRepository = new Mock<ICompanyRepository>();
            _settingsRepository = new Mock<ISettingRepository>();
            _contractService = new Mock<IContractService>();
            _customFeeRepository = new Mock<ICustomFeeRepository>();
            _dateTime = new Mock<IDateTimeFacade>();
            _ewayService = new Mock<IEwayPaymentsService>();
            _invoiceSettings = new InvoiceSettings(_settingsRepository.Object);
            _hardcopyRepository = new Mock<IInvoiceHardcopyRepository>();
            

            _activityService = new Mock<IActivityLoggingService>();

            _service = new InvoicingService(_invoiceRepository.Object,
                                            _accountRepository.Object,
                                            _planRepository.Object,
                                            _networkRepository.Object,
                                            _contractRepository.Object,
                                            _contactRepository.Object,
                                            _companyRepository.Object,
                                            _generator.Object,
                                            _settingsRepository.Object,
                                            _customFeeRepository.Object, 
                                            _hardcopyRepository.Object,
                                            _contractService.Object,
                                            _invoiceSettings,
                                            _ewayService.Object,
                                            _dateTime.Object);

            _rootContractPlan = FakesHelper.CreatePlan();
            _rootContact = FakesHelper.CreateContact();            
            _rootAccount = FakesHelper.CreateRootInvoiceAccount(_rootContact);                
            _rootContract = FakesHelper.CreateActiveContract(_rootAccount, _rootContractPlan);
            _subAccount = FakesHelper.CreateSubAccount(_rootAccount);
            _subContract = FakesHelper.CreateContractOnSubAccount(_subAccount, _rootContractPlan);

            _today = DateTime.Parse("2010-08-16");
            _dateTime.ExpectGet(d => d.Today).Returns(_today);
    
            SetSettingsRepositoryExpectations();            
            SetDefaultExpectations();
        }

        private InvoicingService CreateNewService(Mock<INumberGeneratorService> generator)
        {            
            return new InvoicingService(_invoiceRepository.Object,
                                            _accountRepository.Object,
                                            _planRepository.Object,
                                            _networkRepository.Object,
                                            _contractRepository.Object,
                                            _contactRepository.Object,
                                            _companyRepository.Object,
                                            generator.Object,
                                            _settingsRepository.Object,
                                            _customFeeRepository.Object,
                                            _hardcopyRepository.Object,
                                            _contractService.Object,
                                            _invoiceSettings,
                                            _ewayService.Object,
                                            _dateTime.Object);

        }

        private InvoicingService CreateNewService(Mock<ISettingRepository> settingsRepository)
        {
            var invoiceSettings = new InvoiceSettings(settingsRepository.Object);
            
            return new InvoicingService(_invoiceRepository.Object,
                                            _accountRepository.Object,
                                            _planRepository.Object,
                                            _networkRepository.Object,
                                            _contractRepository.Object,
                                            _contactRepository.Object,
                                            _companyRepository.Object,
                                            _generator.Object,
                                            settingsRepository.Object,
                                            _customFeeRepository.Object,
                                            _hardcopyRepository.Object,
                                            _contractService.Object,
                                            invoiceSettings,
                                            _ewayService.Object,
                                            _dateTime.Object);

        }

        private void SetSettingsRepositoryExpectations()
        {
            _settingsRepository.Expect(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoicedDate", Value = "15-SEP-2009" });
            _settingsRepository.Expect(s => s["ContractSuspensionFee"]).Returns(new Setting { Name = "ContractSuspensionFee", Value = "10.00" });
            _settingsRepository.Expect(s => s["EnvironmentalLevy"]).Returns(new Setting { Name = "EnvironmentalLevy", Value = "2.50" });

       }

        private void SetDefaultExpectations()
        {
            _invoiceRepository.Expect(i => i.FindInvoicesByRunNumber(7)).Returns(new List<Invoice>());
            _planRepository.Expect(p => p.GetPlan(_rootContract.PlanId)).Returns(_rootContractPlan);
            _generator.Expect(g => g.NextInvoiceRunNumber()).Returns(7);
            _generator.Expect(g => g.NextInvoiceNumber()).Returns(_invoiceNumber);
            _contractRepository.Expect(c => c.GetAllCustomFeesForContract(_rootContract)).Returns(new List<CustomFee>());
            _accountRepository.Expect(a => a.UpdateAccount(_rootAccount)).Returns(true);
        }

        private void SetFactory(int runNumber, DateTime fromDate, DateTime toDate)
        {
            _factory = new InvoiceFactory(_invoiceSettings, _dateTime.Object, _contractRepository.Object,
                                          _accountRepository.Object, _planRepository.Object,
                                          _networkRepository.Object, _contractService.Object, _companyRepository.Object,
                                          _contactRepository.Object,
                                          _generator.Object, new InvoiceRunContext(runNumber, fromDate, toDate));
        }

        [Test]
        public void Invoice_excludes_calls_made_after_invoice_to_date()
        {
            var callsMadeBefore = DateTime.Parse("2010-08-15");
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);


            var callInFuture = CreateFakeCall(_rootContract, "0400000001");
            callInFuture.CallTime = callsMadeBefore.AddMonths(2);
            
            // Arrange
            SetExpectationsForSingleAccountSingleContract(new List<Call>() { callInFuture});

            var invoice = _factory.GenerateInvoice(_rootAccount);
            var line = _factory.GetCallLine(callInFuture);


            Assert.IsFalse(invoice.Lines.Contains(line, _lineComparer));   
        }

        [Test]
        public void Invoice_includes_suspension_fee_for_contracts_suspended_in_the_last_month()
        {
            var callsMadeBefore = DateTime.Parse("2010-08-15");
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            // Arrange
            SetExpectationsForSingleAccountSingleContract(new List<Call>());
            
            _rootContract.ContractStatus = ContractStatus.Suspended;
            _rootContract.StatusChangedDate = DateTime.Parse("2010-08-01");     // Must change this because the parameter change internally sets it to DateTime.Now (need to fix).
            _rootContract.SuspensionStartDate = DateTime.Parse("2010-08-01");
            _rootContract.SuspendedUntilDate = DateTime.Parse("2010-08-14");

            var invoice = _factory.GenerateInvoice(_rootAccount);
            
            Assert.IsTrue(invoice.Lines.Contains(invoice.GetContractSuspensionLine(_rootContract, _invoiceSettings), _lineComparer));
        }


        [Test]
        public void GetAllInvoiceRuns()
        {
            _invoiceRepository
                .Expect(i => i.GetAllInvoices())
                .Returns(new List<Invoice>
                             {
                                 new Invoice(1) { InvoiceRunNumber = 1, InvoiceDate = DateTime.Parse("2009-Jun-21 12:00:00") }
                             });

            var runs = _service.GetAllInvoiceRuns();
            Assert.AreEqual(1, runs.Count);
        }

        [Test]
        public void FindInvoicesByInvoiceNumber()
        {
            const string invoiceNumber = "INV0001";

            _invoiceRepository
                .Expect(p => p.FindInvoicesByInvoiceNumber(invoiceNumber))
                .Returns(new List<Invoice>()
                             {
                                 new Invoice(1, new List<InvoiceLine>
                                                     {
                                                         new InvoiceLine(1)
                                                             {
                                                                 Description = "Network Access Fees",
                                                                 GstAmount = 2.72M,
                                                                 LineAmount = 27.27M,
                                                                 Quantity = 1,                                                                 
                                                             }
                                                     }, new List<int> {1})
                                     {                                         
                                         AmountPaid = 0,
                                         FromDate = DateTime.Today.AddMonths(-1),
                                         ToDate = DateTime.Today,
                                         InvoiceAddress =
                                             new Address { 
                                                 Street = "22 Turrbal St",
                                                 Suburb = "Bellbowrie",
                                                 State = "QLD",
                                                 Postcode = "4070"
                                             },
                                         InvoiceDate = DateTime.Today,
                                         InvoiceNumber = invoiceNumber,
                                         To = "Robert Gray",                                         
                                         InvoiceRunNumber = 1,                                         
                                     }
                             });

            var items = _service.FindInvoicesByInvoiceNumber(invoiceNumber);
            foreach (var item in items)
            {
                Assert.AreEqual(invoiceNumber, item.InvoiceNumber);
            }
        }

        [Test]
        public void Invoice_includes_environmental_levy_for_account_with_paper_billing()
        {
            var callsMadeBefore = DateTime.Parse("2010-08-15");
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            _rootAccount.PostBill = true;

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.IsTrue(invoice.Lines.Contains(invoice.GetEnvironmentalLine(_invoiceSettings), _lineComparer));             
        }

        [Test]
        public void Invoice_includes_associated_accounts()
        {
            var callsMadeBefore = DateTime.Parse("2010-08-15");
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            _rootAccount.PostBill = true;

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.IsTrue(invoice.AccountIds.Contains(_rootAccount.Id.Value));            
        }

        [Test]
        public void Invoice_address_is_contact_postal_address()
        {
            var callsMadeBefore = DateTime.Parse("2010-08-15");
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            _rootContact.HomeAddress = new Address
                                           {
                                               Street = "Home Street 1",
                                               Street2 = "Home Street 2",
                                               Suburb = "Home Suburb",
                                               State = "Home State",
                                               Postcode = "Home Postcode"
                                           };
            _rootContact.PostalAddress = new Address()
                                             {
                                                 Street = "Postal Street 1",
                                                 Street2 = "Postal Street 2",
                                                 Suburb = "Postal Suburb",
                                                 State = "Postal State",
                                                 Postcode = "Postal Postcode"
                                             };

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.AreEqual("Postal Street 1", invoice.InvoiceAddress.Street);
            Assert.AreEqual("Postal Street 2", invoice.InvoiceAddress.Street2);
            Assert.AreEqual("Postal Suburb", invoice.InvoiceAddress.Suburb);
            Assert.AreEqual("Postal State", invoice.InvoiceAddress.State);
            Assert.AreEqual("Postal Postcode", invoice.InvoiceAddress.Postcode);
        }

        [Test]
        public void Invoice_To_field_is_account_name()
        {
            var callsMadeBefore = DateTime.Parse("2010-08-15");
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            _rootAccount.AccountName = "Invoice Recipient";

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.AreEqual("Invoice Recipient", invoice.To);            
        }

        [Test]
        public void Invoice_includes_network_access_fee_for_account_with_one_contract()
        {
            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            var callsMadeBefore = DateTime.Parse("2010-08-15");  // This is the invoice to date;            
            _settingsRepository.ExpectGet(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoiceDate", Value = "2010-07-14" });
            
            // Only charge environmental levy if Post Bill is indicated.
            _rootAccount.PostBill = false;

            _invoiceRepository
                .Expect(i => i.InsertInvoice(It.Is<Invoice>(ent => ent.Lines.Contains(ent.GetNetworkAccessLine(_rootContract, _rootContractPlan,_invoiceSettings), _lineComparer))))
                .Callback<Invoice>(i => i.Inserted(1));

            var runNumber = _service.InvoiceRun(callsMadeBefore);
            Assert.AreEqual(7, runNumber, "Incorrect Run Number");
        }

        [Test]
        public void Invoice_includes_free_call_component_if_value_of_calls_less_than_free_call_amount()
        {
            // Only charge environmental levy if Post Bill is indicated.
            _rootAccount.PostBill = false;
            _rootContractPlan.FreeCallAmount = 10M;   

            var fakeCall = CreateFakeCall(_rootContract, "0400000001");
            fakeCall.Cost = 6.5M;
            fakeCall.HasFreeCallTariff = true;
            var calls = new List<Call> { fakeCall };
            
            SetExpectationsForSingleAccountSingleContract(calls);

            var callsMadeBefore = DateTime.Parse("2010-08-15");  // This is the invoice to date;            
            _settingsRepository.ExpectGet(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoiceDate", Value = "2010-07-14" });
                       
            _rootContractPlan.FreeCallAmount = 10M;

            _invoiceRepository
                .Expect(i => i.InsertInvoice(It.Is<Invoice>(ent => ent.Lines.Contains(ent.GetFreeCallComponent(_rootContract, fakeCall.Cost, _invoiceSettings), _lineComparer))))
                .Callback<Invoice>(i => i.Inserted(1));

            var runNumber = _service.InvoiceRun(callsMadeBefore);
                       
            Assert.AreEqual(7, runNumber, "Incorrect Run Number");
        }

        [Test]
        public void Invoice_includes_free_call_component_if_value_of_calls_equals_free_call_amount()
        {
            // Only charge environmental levy if Post Bill is indicated.
            _rootAccount.PostBill = false;
            
            _rootContractPlan.FreeCallAmount = 6.5M;
            var fakeCall = CreateFakeCall(_rootContract, "0400000001");
            fakeCall.Cost = _rootContractPlan.FreeCallAmount;
            fakeCall.HasFreeCallTariff = true;
            
            SetExpectationsForSingleAccountSingleContract(new List<Call> { fakeCall });

            var callsMadeBefore = DateTime.Parse("2010-08-15");  // This is the invoice to date;            
            _settingsRepository.ExpectGet(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoiceDate", Value = "2010-07-14" });

            _invoiceRepository
                .Expect(i => i.InsertInvoice(It.Is<Invoice>(ent => ent.Lines.Contains(ent.GetFreeCallComponent(_rootContract, fakeCall.Cost, _invoiceSettings), _lineComparer))))
                .Callback<Invoice>(i => i.Inserted(1));

            var runNumber = _service.InvoiceRun(callsMadeBefore);
            Assert.AreEqual(7, runNumber, "Incorrect Run Number");
        }

        [Test]
        public void Invoice_does_not_include_free_call_component_when_plan_has_no_free_call_amount()
        {          
            SetExpectationsForSingleAccountSingleContract(FakesHelper.CreateCallListWithOneCall(_rootContract));

            // Invoice all calls not yet invoiced and made before 15/01/2009.
            var callsMadeBefore = DateTime.Parse("2010-08-15");  // This is the invoice to date;            
            _settingsRepository.ExpectGet(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoiceDate", Value = "2010-07-14" });

            var fakeCall = CreateFakeCall(_rootContract, "0400000001");

            // No calls for this contract
            _contractRepository
                .Expect(c => c.GetNonInvoicedCallsByContractId(_rootContract.Id.Value))
                .Returns(new List<Call> { fakeCall } );

            // Only charge environmental levy if Post Bill is indicated.
            _rootAccount.PostBill = false;

            _rootContractPlan.FreeCallAmount = 0M;

            // As this is Account had no calls on it, the only line items on the Invoice
            // should be for "Network Access Fee" and "Environmental Levy"
            _invoiceRepository
                .Expect(i => i.InsertInvoice(It.Is<Invoice>(ent => !ent.Lines.Contains(ent.GetFreeCallComponent(_rootContract, fakeCall.Cost, _invoiceSettings), _lineComparer))))
                .Callback<Invoice>(i => i.Inserted(1));

            var runNumber = _service.InvoiceRun(callsMadeBefore);
            Assert.AreEqual(7, runNumber, "Incorrect Run Number");
        }
       
        [Test]
        public void Invoice_includes_total_free_call_component_if_value_of_calls_greater_than_free_call_amount()
        {
            var calls = FakesHelper.CreateCallListWithTwoCalls(_rootContract);
            SetExpectationsForSingleAccountSingleContract(calls);
            
            // Invoice all calls not yet invoiced and made before 15/01/2009.
            var callsMadeBefore = DateTime.Parse("2010-08-15");  // This is the invoice to date;            
            _settingsRepository.ExpectGet(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoiceDate", Value = "2010-07-14" });
                        
            _rootAccount.PostBill = false;
            _rootContractPlan.FreeCallAmount = 10M;

            _invoiceRepository
                .Expect(i => i.InsertInvoice(It.Is<Invoice>(ent => ent.Lines.Contains(ent.GetFreeCallComponent(_rootContract, _rootContractPlan, _invoiceSettings), _lineComparer))))
                .Callback<Invoice>(i => i.Inserted(1));

            var runNumber = _service.InvoiceRun(callsMadeBefore);
            Assert.AreEqual(7, runNumber, "Incorrect Run Number");
        }
          
        [Test]
        public void Contract_terminated_early_in_invoice_period_is_charged_early_termination_fee()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // This tests one account with one contract that is closed early.
            // The contract has no calls made in the the invoice period 
            // and should only charge the Network Access Fee of the month
            // and the Early Termination Fee.
            _rootContract.ContractStatus = ContractStatus.Closed;
            _rootContract.StatusChangedDate = lastInvoiceDate.AddDays(10);
            _rootContract.EndDate = callsMadeBefore.AddMonths(6);

            // If the contract is closed since the start of the invoice period, charge the closed contract amount

            var invoice = _factory.GenerateInvoice(_rootAccount);
            
            Assert.IsTrue(_rootContract.IsTerminated, "Contract not terminated");            
            Assert.IsTrue(invoice.Lines.Contains(new InvoiceLine
                                                  {
                                                      ContractId = _rootContract.Id.Value,
                                                      Call = null,                                                                                               
                                                      Description = "Early contract termination fee for contract '" + _rootContract.ContractNumber +
                                                                        "'.  " + (int)_rootContract.RemainingTimeOnContract.TotalDays + " days early.",
                                                      Invoice = invoice,
                                                      Quantity = 1,
                                                      LineAmount = _rootContractPlan.RemainingContractValue(_rootContract),
                                                      GstAmount = _rootContractPlan.RemainingContractValue(_rootContract)/11
                                                  }, _lineComparer));                
        }

        [Test]
        public void Contract_closed_after_invoice_period_is_invoiced()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // This tests one account with one contract that is closed early.
            // The contract has no calls made in the the invoice period 
            // and should only charge the Network Access Fee of the month
            // and the Early Termination Fee.
            _rootContract.ContractStatus = ContractStatus.Closed;
            _rootContract.StatusChangedDate = callsMadeBefore.AddDays(10);
            _rootContract.EndDate = callsMadeBefore.AddMonths(6);

            // If the contract is closed since the start of the invoice period, charge the closed contract amount

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.IsNotNull(invoice);
        }

        [Test]
        public void Early_terminate_fee_not_charged_when_contract_closed_after_invoice_period()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // This tests one account with one contract that is closed early.
            // The contract has no calls made in the the invoice period 
            // and should only charge the Network Access Fee of the month
            // and the Early Termination Fee.
            _rootContract.ContractStatus = ContractStatus.Closed;
            _rootContract.StatusChangedDate = callsMadeBefore.AddDays(10);
            _rootContract.EndDate = callsMadeBefore.AddMonths(6);

            // If the contract is closed since the start of the invoice period, charge the closed contract amount

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.IsTrue(_rootContract.IsTerminated, "Contract not terminated");
            Assert.IsFalse(invoice.Lines.Contains(new InvoiceLine
            {
                ContractId = _rootContract.Id.Value,
                Call = null,
                Description = "Early contract termination fee for contract '" + _rootContract.ContractNumber +
                                  "'.  " + (int)_rootContract.RemainingTimeOnContract.TotalDays + " days early.",
                Invoice = invoice,
                Quantity = 1,
                LineAmount = _rootContractPlan.RemainingContractValue(_rootContract),
                GstAmount = _rootContractPlan.RemainingContractValue(_rootContract) / 11
            }, _lineComparer));
        }


        [Test]
        public void Contract_not_invoiced_when_already_closed()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // This tests one account with one contract that is closed early.
            // The contract has no calls made in the the invoice period 
            // and should only charge the Network Access Fee of the month
            // and the Early Termination Fee.
            _rootContract.ContractStatus = ContractStatus.Closed;
            _rootContract.StatusChangedDate = lastInvoiceDate.AddDays(-10);
            _rootContract.EndDate = callsMadeBefore.AddMonths(6);

            // If the contract is closed since the start of the invoice period, charge the closed contract amount

            var invoice = _factory.GenerateInvoice(_rootAccount);
            Assert.IsNull(invoice);                        
        }


        [Test]
        public void Invoice_new_charges_equal_total_for_line_items()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // Only charge environmental levy if Post Bill is indicated.
            _rootAccount.PostBill = false;

            // As this is Account had no calls on it, the only line items on the Invoice
            // should be for "Network Access Fee" and "Environmental Levy"
 
            // Act
            var invoice = _factory.GenerateInvoice(_rootAccount);
            
            // Assert
            Assert.AreEqual(invoice.Lines.Sum(l => l.LineAmount), invoice.NewCharges);          
        }

        [Test]
        public void Invoice_previous_bill_equal_account_previous_bill()
        {            
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // Only charge environmental levy if Post Bill is indicated.
            _rootAccount.PostBill = false;

            // Act
            var invoice = _factory.GenerateInvoice(_rootAccount);

            // Assert
            Assert.AreEqual(_rootAccount.PreviousBill, invoice.PreviousBill);          
        }

        [Test]
        public void Invoice_amount_paid_equals_account_amount_paid()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            // As this is Account had no calls on it, the only line items on the Invoice
            // should be for "Network Access Fee" and "Environmental Levy"
            _invoiceRepository
                .Expect(i => i.InsertInvoice(It.Is<Invoice>(ent => ent.AmountPaid == _rootAccount.AmountPaid)))
                .Callback<Invoice>(i => i.Inserted(1));

            // Act
            var invoice = _factory.GenerateInvoice(_rootAccount);

            // Assert
            Assert.AreEqual(_rootAccount.AmountPaid, invoice.AmountPaid);            
        }

        [Test]
        [Ignore("Needs refactoring to be able to test this")]
        public void Master_account_for_invoice_previous_bill_is_current_invoice_after_invoice_created()
        {
          
        }

        [Test]
        public void Master_account_for_invoice_amount_paid_reset_after_invoice_created()
        {
            // Invoice all calls not yet invoiced and made before 15/01/2009.
            _accountRepository.Expect(a => a.GetAllRootAccounts()).Returns(new List<Account> { _rootAccount });
            _accountRepository.Expect(a => a.GetAllSubAccounts(_rootAccount.Id.Value)).Returns(new List<Account> { _subAccount });
            _accountRepository.Expect(a => a.GetAllSubAccounts(_subAccount.Id.Value)).Returns(new List<Account>());
            _contractRepository.Expect(c => c.GetContractsByAccountId(_rootAccount.Id.Value)).Returns(new List<Contract> { _rootContract });
            _contractRepository.Expect(c => c.GetContractsByAccountId(_subAccount.Id.Value)).Returns(new List<Contract> { _subContract });
            _contactRepository.Expect(c => c.GetContactEntity(_rootAccount.ContactId)).Returns(_rootContact);
            _contractRepository.Expect(c => c.GetAllCustomFeesForContract(_subContract)).Returns(new List<CustomFee>());

            // No calls for this contract
            var fakeCall1 = CreateFakeCall(_rootContract, "0400000001");
            var fakeCall2 = CreateFakeCall(_subContract, "0400000002");

            _contractRepository
                .Expect(c => c.GetNonInvoicedCallsByContractId(_rootContract.Id.Value))
                .Returns(new List<Call>
                             {
                                fakeCall1                              
                             });

            _contractRepository
               .Expect(c => c.GetNonInvoicedCallsByContractId(_subContract.Id.Value))
               .Returns(new List<Call>
                             {                                 
                                fakeCall2
                             });

            var callsMadeBefore = DateTime.Parse("2010-08-15");  // This is the invoice to date;            
            _settingsRepository.ExpectGet(s => s["LastInvoicedDate"]).Returns(new Setting { Name = "LastInvoiceDate", Value = "2010-07-14" });            
            _invoiceRepository.Expect(i => i.InsertInvoice(It.IsAny<Invoice>())).Callback<Invoice>(i => i.Inserted(1));

            _accountRepository
                .Expect(a => a.UpdateAccount(It.Is<MasterAccount>(acc => acc.AmountPaid == 0)))
                .Returns(true);

            // Act
            var runNumber = _service.InvoiceRun(callsMadeBefore);

            // Assert
            Assert.AreEqual(7, runNumber, "Incorrect Run Number");
        }


        #region Parameter Tests

        [Test]        
        public void Cannot_run_invoice_run_twice_in_the_same_month()
        {            
            Assert.Throws<InvoiceRunFrequencyException>(
                () => _service.InvoiceRun(DateTime.Parse("2009-SEP-16 16:00:00")));            
        }

        [Test]
        public void GetLastInvoicedDate_can_convert_to_date_time()
        {            
            var settingsRepository = new Mock<ISettingRepository>();
            var service = CreateNewService(settingsRepository);
            
            settingsRepository.ExpectGet(e => e["LastInvoicedDate"]).Returns(new Setting { Value = "2010-08-14"});

            var lastInvoicedDate = service.InvoiceSettings.LastInvoiceRunPerformedDate;

            Assert.AreEqual(DateTime.Parse("2010-08-14"), lastInvoicedDate);
        }

        [Test]
        public void GetContractSuspensionFee_can_be_converted_to_decimal_from_basic_string()
        {
            _settingsRepository.ExpectGet(e => e["ContractSuspensionFee"]).Returns(new Setting { Value = "10" });

            var amount = _service.InvoiceSettings.ContractSuspensionFee;

            Assert.AreEqual(10M, amount);
        }

        [Test]
        public void GetContractSuspensionFee_can_be_converted_to_decimal_from_currency_string()
        {
            _settingsRepository.ExpectGet(e => e["ContractSuspensionFee"]).Returns(new Setting { Value = "$10" });

            var amount = _service.InvoiceSettings.ContractSuspensionFee;

            Assert.AreEqual(10M, amount);
        }

        [Test]
        public void GetContractSuspensionFee_can_be_converted_to_decimal_from_currency_string_with_cents()
        {
            _settingsRepository.ExpectGet(e => e["ContractSuspensionFee"]).Returns(new Setting { Value = "$10.00" });

            var amount = _service.InvoiceSettings.ContractSuspensionFee;

            Assert.AreEqual(10M, amount);
        }

        [Test]
        public void International_accounts_are_not_charged_gst_on_inovice()
        {
            var lastInvoiceDate = DateTime.Parse("2010-07-15");
            var callsMadeBefore = lastInvoiceDate.AddMonths(1);
            SetFactory(7, lastInvoiceDate.AddDays(1), callsMadeBefore);

            SetExpectationsForSingleAccountSingleContract(new List<Call>());

            _rootAccount.IsInternational = true;

            var invoice = _factory.GenerateInvoice(_rootAccount);

            Assert.Greater(invoice.TotalIncGst, 0);
            Assert.AreEqual(0, invoice.TotalGst);
            Assert.AreEqual(invoice.TotalIncGst, invoice.TotalExGst);
        }

        #endregion 
        
        #region Helpers

        public static Call CreateFakeCall(Contract contract, string numberCalled)
        {
            return new Call(1)
            {
                CalledFrom = "SATELLITE",
                ContractId = contract.Id.Value,
                Volume = 2.54M,
                HasFreeCallTariff = false,
                ImportedCallType = "TEST CALL",
                NumberCalled = numberCalled,
                PhoneNumber = contract.PhoneNumber1,
                UnitCost = 1M,
                UnitsOfTime = 6,
                Cost = 6.5M
            };
        }

        protected void SetExpectationsForSingleAccountSingleContract(IList<Call> callsMade)
        {
            _accountRepository.Expect(a => a.GetAllRootAccounts()).Returns(new List<Account> { _rootAccount });
            _accountRepository.Expect(a => a.GetAllSubAccounts(_rootAccount.Id.Value)).Returns(new List<Account>());
            _contractRepository.Expect(c => c.GetContractsByAccountId(_rootAccount.Id.Value)).Returns(new List<Contract> { _rootContract });
            _contactRepository.Expect(c => c.GetContactEntity(_rootAccount.ContactId)).Returns(_rootContact);
            _contractRepository.Expect(c => c.GetNonInvoicedCallsByContractId(_rootContract.Id.Value)).Returns(callsMade);
            _networkRepository.Expect(n => n.GetNetwork(_rootContract.PlanId)).Returns(new Network(1) {Name = "Test Network"});
        }

        #endregion
    }
}


