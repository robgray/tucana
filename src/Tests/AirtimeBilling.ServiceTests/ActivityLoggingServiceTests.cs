﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using SharpStudios.AirtimeBilling.Services;
using SharpStudios.AirtimeBilling.Core;
using Moq;
using SharpStudios.AirtimeBilling.Core.Repositories;

namespace SharpStudios.AirtimeBilling.Services.Tests
{
    /// <summary>
    /// Summary description for ActivityLoggingServiceTests
    /// </summary>
    [TestFixture]
    public class ActivityLoggingServiceTests
    {       
        Mock<IActivityLogRepository> mockRepository;
        ActivityLoggingService service;

     
        [SetUp]
        public void MyTestInitialize() 
        {
            mockRepository = new Mock<IActivityLogRepository>();
            service = new ActivityLoggingService(mockRepository.Object);
        }
     
        [Test]
        public void WriteUserActivity()
        {
            // All other Write methods delegate to this method.
            // So only need to test this.
            mockRepository
                .Expect(r => r.Insert(It.Is<Activity>(activity => activity.Id == null), "Contact", 1))
                .Callback<Activity>(activity => activity.Inserted(1));

            service.WriteUserActivity(new Contact(1), "admin", "Test");
            
            // Doesn't actually do anything... This is where we need Rhino.Mocks :)
            // ... need to comment why, so I don't need to look through the code.
        }

        [Test]
        public void FindUserActivities()
        {
            mockRepository
                .Expect(r => r.FindActivitiesByUser("Rob"))
                .Returns(new List<Activity>() { new Activity(1, DateTime.Now, "Test", "Rob") });

            IList<Activity> activities = service.FindLogEntriesForUser("Rob");
            Assert.AreEqual(1, activities.Count);                
        }
    }
}
