﻿using System;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using Moq;
using NUnit.Framework;

namespace AirtimeBilling.Services.Tests
{
    [TestFixture]
    public class InvoiceFactoryTests 
    {
        private Mock<INumberGeneratorService> _generator;
        private Mock<IInvoiceRepository> _invoiceRepository;
        private Mock<IAccountRepository> _accountRepository;
        private Mock<IPlanRepository> _planRepository;
        private Mock<INetworkRepository> _networkRepository;
        private Mock<IContractRepository> _contractRepository;
        private Mock<IContactRepository> _contactRepository;
        private Mock<ICompanyRepository> _companyRepository;
        private Mock<ISettingRepository> _settingsRepository;
        private Mock<IActivityLoggingService> _activityService;
        private Mock<ICustomFeeRepository> _customFeeRepository;
        private Mock<IInvoiceHardcopyRepository> _hardcopyRepository;
        private Mock<IContractService> _contractService;
        private Mock<IDateTimeFacade> _dateTime;
        private Mock<IEwayPaymentsService> _ewayService;

        // Dont' want to mock.  but we mock the supplied ISettingRepository to constructor.
        // needs to be in it's own test suite!
        private IInvoiceSettings _invoiceSettings;
        
        private InvoiceFactory _factory;

        [SetUp]
        public void SetUp()
        {
            _generator = new Mock<INumberGeneratorService>();
            _invoiceRepository = new Mock<IInvoiceRepository>();
            _accountRepository = new Mock<IAccountRepository>();
            _planRepository = new Mock<IPlanRepository>();
            _networkRepository = new Mock<INetworkRepository>();
            _contractRepository = new Mock<IContractRepository>();
            _contactRepository = new Mock<IContactRepository>();
            _companyRepository = new Mock<ICompanyRepository>();
            _settingsRepository = new Mock<ISettingRepository>();
            _contractService = new Mock<IContractService>();
            _customFeeRepository = new Mock<ICustomFeeRepository>();
            _dateTime = new Mock<IDateTimeFacade>();
            _ewayService = new Mock<IEwayPaymentsService>();
            _invoiceSettings = new InvoiceSettings(_settingsRepository.Object);
            _hardcopyRepository = new Mock<IInvoiceHardcopyRepository>();
            
            
            _activityService = new Mock<IActivityLoggingService>();            
        }

        private void SetFactory(int runNumber, DateTime fromDate, DateTime toDate)
        {
            _factory = new InvoiceFactory(_invoiceSettings, _dateTime.Object, _contractRepository.Object,
                                          _accountRepository.Object, _planRepository.Object,
                                          _networkRepository.Object, _contractService.Object, _companyRepository.Object,
                                          _contactRepository.Object,
                                          _generator.Object, new InvoiceRunContext(runNumber, fromDate, toDate));
        }

       
    }
}
