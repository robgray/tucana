﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Common;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using AirtimeBilling.Services.DTO;
using AirtimeBilling.Services.Interfaces;
using AirtimeBilling.Services.Services;
using NUnit.Framework;
using Moq;

namespace AirtimeBilling.Services.Tests
{    
    [TestFixture]
    public class ContractServiceTests : BaseServiceFixture
    {       
        private ContractService _service;
        private Mock<IAccountRepository> _accountRepository;
        private Mock<IContactRepository> _contactRepository;
        private Mock<IContractRepository> _contractRepository;
        private Mock<ICompanyRepository> _companyRepository;
        private Mock<IPlanRepository> _planRepository;
        private Mock<IAgentRepository> _agentRepository;        
        private Mock<INumberGeneratorService> _generator;
        private Mock<IActivityLoggingService> _activityService;
        private Mock<Import.ICallDataImportManager> _importManager;
        private Mock<ICustomFeeRepository> _customFeeRepository;        
        private Mock<IDateTimeFacade> _dateTime;
        private DateTime _today;

        // Entities to use for the Update Tests
        UpdateContractRequest updateRequest;

        
                
        // Use TestInitialize to run code before running each test 
        [SetUp]
        public void MyTestInitialize() 
        {
            _accountRepository = new Mock<IAccountRepository>();
            _contactRepository = new Mock<IContactRepository>();
            _contractRepository = new Mock<IContractRepository>();
            _planRepository = new Mock<IPlanRepository>();
            _generator = new Mock<INumberGeneratorService>();
            _companyRepository = new Mock<ICompanyRepository>();
            _agentRepository = new Mock<IAgentRepository>();            
            _customFeeRepository = new Mock<ICustomFeeRepository>();
            
            _activityService = new Mock<IActivityLoggingService>();
            _importManager = new Mock<Import.ICallDataImportManager>();
            _dateTime = new Mock<IDateTimeFacade>();
            
            _today = DateTime.Parse("2008-10-11");
            _dateTime.ExpectGet(p => p.Today).Returns(_today);
            
            _service = new ContractService(_contractRepository.Object, _accountRepository.Object, 
                            _planRepository.Object, _contactRepository.Object, _companyRepository.Object, _agentRepository.Object, 
                            _activityService.Object, _generator.Object, _importManager.Object, _customFeeRepository.Object, _dateTime.Object);

            updateRequest = new UpdateContractRequest
                            {
                                ContractId = 1,
                                PlanId = 1,
                                ActivationDate = DateTime.Parse("12/11/2008"),
                                EndDate = DateTime.Parse("12/11/2009"),
                                UsedBy = "Michael",
                                Pin = "6666",
                                Puk = "6666",
                                PhoneNumber1 = "012344394",
                                PhoneNumber2 = "430493304",
                                PhoneNumber3 = "324433344",
                                PhoneNumber4 = "443343433",
                                HomeAddress = GetAddress(),
                                DeliveryAddress = GetAddress(),
                                AccountName = "Test Account", 
                                AccountPassword = ""
                            };
        }

        #region ApproveInvoicing Tests

        [Test]
        public void ApproveInvoicing()
        {
            // set mock behaviour            
            _accountRepository
                .Expect(p => p.GetAccount(3))
                .Returns(new Account(3) {HasRequestedInvoicing = true, BillingMethod = BillingMethod.CreditCard });
            
            _accountRepository                                
                .Expect(p => p.UpdateAccount(It.Is<Account>(a => 
                    (a.BillingMethod == BillingMethod.Invoice && a.HasRequestedInvoicing == false))))              
                .Returns(true);

            var request = new ApproveInvoicingRequest
            { 
                AccountId = 3, 
                IsApprovalGranted = true, 
                User = CreateIUser()
            };
            var response =_service.ApproveInvoicing(request);

            Assert.IsTrue(response.IsSuccessful, response.Message);                  
        }

        [Test]
        public void Invalid_user_cannot_approve_invoicing_payment_method()
        {
            // set mock behaviour            
            _accountRepository
                .Expect(p => p.GetAccount(3))
                .Returns(new Account(3) { HasRequestedInvoicing = true, BillingMethod = BillingMethod.CreditCard });

            _accountRepository
                .Expect(p => p.UpdateAccount(It.Is<Account>(a =>
                    (a.BillingMethod == BillingMethod.Invoice && a.HasRequestedInvoicing == false))))
                .Returns(true);

            ApproveInvoicingRequest request = new ApproveInvoicingRequest()
            {
                AccountId = 3,
                IsApprovalGranted = true,
                User = CreateAgentUser()
            };
            ApproveInvoicingResponse response =_service.ApproveInvoicing(request);

            Assert.IsFalse(response.IsSuccessful, response.Message);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message, "Message");
        }

        [Test]
        public void Deny_invoicing_payment_leaves_payment_method_as_creditcard()
        {
            // set mock behaviour            
            _accountRepository
                .Expect(p => p.GetAccount(3))
                .Returns(new Account(3) { HasRequestedInvoicing = true, BillingMethod = BillingMethod.CreditCard });

            _accountRepository
                .Expect(p => p.UpdateAccount(It.Is<Account>(a => (a.BillingMethod == BillingMethod.CreditCard && a.HasRequestedInvoicing == false))))
                .Returns(true);

            ApproveInvoicingRequest request = new ApproveInvoicingRequest() 
            { AccountId = 3, IsApprovalGranted = false, User = CreateIUser() };
            ApproveInvoicingResponse response =_service.ApproveInvoicing(request);

            Assert.IsTrue(response.IsSuccessful, response.Message);
        }

        [Test]
        public void ApproveInvoicing_AccountNotFound()
        {
            // set mock behaviour
            _accountRepository
                .Expect(p => p.GetAccount(4))
                .Returns<Account>(null);

            ApproveInvoicingRequest request = new ApproveInvoicingRequest() 
            { AccountId = 4, IsApprovalGranted = true, User = new User() { Username = "UNITTEST" } };
            ApproveInvoicingResponse response = _service.ApproveInvoicing(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Account Id does not exist", response.Message);
        }

        [Test]
        public void ApproveInvoicing_AccountAlreadySetToInvoicing()
        {
            _accountRepository
                .Expect(p => p.GetAccount(3))
                .Returns(new Account(3) { BillingMethod = BillingMethod.Invoice });

            ApproveInvoicingRequest request = new ApproveInvoicingRequest() 
            { AccountId = 3, IsApprovalGranted = true, User = CreateIUser() };
            ApproveInvoicingResponse response =_service.ApproveInvoicing(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Account is already billing by Invoice", response.Message);
        }

        #endregion

        #region SubmitNewContractForAccount Tests

        [Test]        
        public void Cannot_add_contract_to_an_account_when_account_doesnt_exist()
        {         
            // Setup mock behaviour
            _planRepository.Expect(p => p.GetPlan(1)).Returns(new Plan(1));
            _accountRepository.Expect(p => p.GetAccountByAccountNumber("LW0001")).Returns<Account>(null);
            _dateTime.ExpectGet(p => p.Today).Returns(DateTime.Parse("2008-10-11"));

            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.PlanId = 1;
            request.ActivationDate = DateTime.Parse("2008-10-11");
            request.EndDate = DateTime.Parse("2010-10-15");
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = _service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Account 'LW0001' does not exist.", response.Message);
        }

        [Test]        
        public void Cannot_add_contract_to_account_when_account_password_invalid()
        {
            // Setup mock behaviour
            _planRepository.Expect(p => p.GetPlan(1)).Returns(new Plan(1));
            _accountRepository.Expect(p => p.GetAccountByAccountNumber("LW0001")).Returns(new Account(1) { AccountNumber = "LW0001", Password = "Bill" });
            
            var request = new NewContractForAccountRequest
                                                       {
                                                           AccountNumber = "LW0001",
                                                           AccountPassword = "turtle",
                                                           ActivationDate = DateTime.Parse("2008-10-11"),
                                                           EndDate = DateTime.Parse("2010-10-15"),
                                                           IMEINumber = "12342323323",
                                                           SimCard = "123232593",
                                                           UsedBy = "Robert",
                                                           PlanId = 1,
                                                           Data = false,
                                                           MessageBank = false,
                                                           User = new User() {Username = "UNITTEST"}
                                                       };

            var response = _service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Supplied Account Password does not match.", response.Message);
        }

        [Test]        
        public void Cannot_add_contract_to_account_when_end_date_before_activation_date()
        {
            // Setup mock behaviour
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            _accountRepository
                .Expect(p => p.GetAccountByAccountNumber("LW0001"))
                .Returns(new Account(1) { AccountNumber = "LW0001", Password = "Bill" });

            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Parse("2008-10-15");
            request.EndDate = DateTime.Parse("2008-10-14");
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = _service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after contract activation date.", response.Message);
        }

        [Test]        
        public void Cannot_add_contract_to_account_when_end_day_in_the_past()
        {
            // Setup mock behaviour
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            _accountRepository
                .Expect(p => p.GetAccountByAccountNumber("LW0001"))
                .Returns(new Account(1) { AccountNumber = "LW0001", Password = "turtle" });

            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = _today.AddDays(-10);
            request.EndDate = _today.AddDays(-9);
            request.IMEINumber = "12342323323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = _service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after the current date.", response.Message);
        }

        [Test]
        public void New_contract_for_account_requires_IMEI_number_and_sim_card()
        {
            // Setup mock behaviour
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1) );

            _accountRepository
                .Expect(p => p.GetAccountByAccountNumber("LW0001"))
                .Returns(new Account(1) { AccountNumber = "LW0001", Password = "Bill" });

            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Today.AddDays(1);
            request.EndDate = DateTime.Today.AddDays(3);
            request.IMEINumber = "";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 1;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = _service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Both an IMEI Number and sim card number must be supplied.", response.Message);
        }

        [Test]
        public void New_contract_for_account_must_be_on_a_plan()
        {
            // Setup mock behaviour
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            _accountRepository
                .Expect(p => p.GetAccountByAccountNumber("LW0001"))
                .Returns(new Account(1) { AccountNumber = "LW0001", Password = "Bill" });

            NewContractForAccountRequest request = new NewContractForAccountRequest();
            request.AccountNumber = "LW0001";
            request.AccountPassword = "turtle";
            request.ActivationDate = DateTime.Today.AddDays(1);
            request.EndDate = DateTime.Today.AddDays(3);
            request.IMEINumber = "12323";
            request.SimCard = "123232593";
            request.UsedBy = "Robert";
            request.PlanId = 0;
            request.Data = false;
            request.MessageBank = false;
            request.User = new User() { Username = "UNITTEST" };

            NewContractResponse response = _service.SubmitNewContractForAccount(request);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("A plan must be selected for the Contract.", response.Message);
        }


        [Test]
        public void Contract_can_be_added_to_existing_account()
        {
            // Setup mock behaviour
            _planRepository.Expect(p => p.GetPlan(1)).Returns(new Plan(1));
            _accountRepository.Expect(p => p.GetAccountByAccountNumber("LW0001")).Returns(new Account(1) { AccountNumber = "LW0001", Password = "turtle" });
            _generator.Expect(p => p.NextContractNumber()).Returns("CT6666");
            _contractRepository.Expect(p => p.InsertContract(It.Is<Contract>(x => x.Id == null))).Callback<Contract>(c => c.Inserted(1));
            
            var request = new NewContractForAccountRequest
                              {
                                  AccountNumber = "LW0001",
                                  AccountPassword = "turtle",
                                  ActivationDate = DateTime.Parse("2008-10-15"),
                                  EndDate = DateTime.Parse("2010-10-15"),
                                  IMEINumber = "12342323323",
                                  SimCard = "123232593",
                                  UsedBy = "Robert",
                                  PlanId = 1,
                                  Data = false,
                                  MessageBank = false,
                                  User = new User() {IsAuthenticated = true, Username = "Test"}
                              };

            var response = _service.SubmitNewContractForAccount(request);

            Assert.IsTrue(response.IsSuccessful);
        }

        #endregion 

        #region SubmitNewContract Tests

        [Test]
        public void Contact_name_for_new_contract_must_be_supplied()
        {
            // Setup Mock Objects
            _planRepository.Expect(p => p.GetPlan(1)).Returns(new Plan(1));

            var request = CreateNewContractRequestFullyPopulated();
            request.Contact.Name = null;

            var response = _service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("No contact name has been supplied.", response.Message);
        }

        [Test]
        public void New_contract_postal_address_must_have_street_suburb_postcode_state()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.Contact.PostalAddress = new Address
            {
                Street = request.Contact.PostalAddress.Street,
                Suburb = request.Contact.PostalAddress.Suburb,
                Postcode = request.Contact.PostalAddress.Postcode
            };

            NewContractResponse response = _service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Please ensure postal address details have been filled out.\n" +
                    "Including street, suburb, state, and postcode", response.Message);
        }

        [Test]        
        public void New_contract_must_have_end_date_after_activiation_Date()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.ActivationDate = request.EndDate.AddDays(2);

            NewContractResponse response = _service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after contract activation date.", response.Message);
        }

        [Test]
        public void New_contract_must_have_end_date_on_or_after_today()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.ActivationDate = DateTime.Today.AddDays(-2);
            request.EndDate = DateTime.Today;

            NewContractResponse response = _service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Contract end date must be after the current date.", response.Message);
        }

        [Test]
        public void New_contract_must_have_imie_number_and_sim_card()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.IMEINumber = null;
            
            NewContractResponse response = _service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Both an IMEI Number and sim card number must be supplied.", response.Message);
        }

        [Test]
        public void Submitting_new_contract_with_invoice_payment_method_changes_to_creditcard()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            _generator
                .Expect(p => p.NextAccountNumber())
                .Returns("AC0001");

            _generator
                .Expect(p => p.NextContractNumber())
                .Returns("CT0001");

            _contactRepository
                .Expect(p => p.InsertContact(It.Is<Contact>(x => (x.Id == null && 
                    x.Name == "Robert Gray" &&
                    x.MobilePhone == "0411331273"))))
                .Callback<Contact>(c => c.Inserted(1));

            // Invoicing is set to CreditCard until invoicing is approved. 
            // even if invoicing is selected.
            _accountRepository
                .Expect(p => p.InsertAccount(It.Is<Account>(x => (x.Id == null && 
                    x.AccountNumber == "AC0001" && 
                    x.HasRequestedInvoicing && 
                    x.BillingMethod == BillingMethod.CreditCard && 
                    x.BillingAddressType == BillingAddressType.Postal && 
                    x.PostBill))))
                .Callback<Account>(a => a.Inserted(1));

            _contractRepository
                .Expect(p => p.InsertContract(It.Is<Contract>(x => x.Id == null)))
                .Callback<Contract>(c => c.Inserted(1));
                   
            // Creates an correctly populated NewContractRequest instance
            var request = CreateNewContractRequestFullyPopulated();
            request.BillingMethod = BillingMethod.Invoice;
                        
            request.User = CreateIUser();

            var response = _service.SubmitNewContract(request);
            Assert.IsTrue(response.IsSuccessful);
        }
       
        [Test]
        public void Agent_submitted_new_contract_is_associated_with_agent()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            _generator
                .Expect(p => p.NextAccountNumber())
                .Returns("AC0001");

            _generator
                .Expect(p => p.NextContractNumber())
                .Returns("CT0001");

            _contactRepository
                .Expect(p => p.InsertContact(It.Is<Contact>(x => x.Id == null)))
                .Callback<Contact>(c => c.Inserted(1));

            _accountRepository
                .Expect(p => p.InsertAccount(It.Is<Account>(x => (x.Id == null && x.HasRequestedInvoicing == true && x.BillingMethod == BillingMethod.CreditCard && x.PostBill ==  true))))
                .Callback<Account>(a => a.Inserted(1));

            _contractRepository
                .Expect(p => p.InsertContract(It.Is<Contract>(x => x.Id == null)))
                .Callback<Contract>(c => c.Inserted(1));

            // Creates an correctly populated NewContractRequest instance
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.User = CreateAgentUser();

            NewContractResponse response = _service.SubmitNewContract(request);
            Assert.IsTrue(response.IsSuccessful);
        }

        [Test]
        public void Normal_user_cannot_create_contracts()
        {
            // Setup Mock Objects
            _planRepository
                .Expect(p => p.GetPlan(1))
                .Returns(new Plan(1));

            _generator
                .Expect(p => p.NextAccountNumber())
                .Returns("AC0001");

            _generator
                .Expect(p => p.NextContractNumber())
                .Returns("CT0001");

            _contactRepository
                .Expect(p => p.InsertContact(It.Is<Contact>(x => x.Id == null)))
                .Callback<Contact>(c => c.Inserted(1));

            _accountRepository
                .Expect(p => p.InsertAccount(It.Is<Account>(x => (x.Id == null && x.HasRequestedInvoicing && x.BillingMethod == BillingMethod.CreditCard && x.PostBill))))
                .Callback<Account>(a => a.Inserted(1));

            _contractRepository
                .Expect(p => p.InsertContract(It.Is<Contract>(x => x.Id == null)))
                .Callback<Contract>(c => c.Inserted(1));

            // Creates an correctly populated NewContractRequest instance
            NewContractRequest request = CreateNewContractRequestFullyPopulated();
            request.User = CreateCustomerUser();

            NewContractResponse response = _service.SubmitNewContract(request);
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);
        }

        #region Helpers

        private NewContractRequest CreateNewContractRequestFullyPopulated()
        {
            var request = new NewContractRequest
                              {
                                  Contact =
                                      {
                                          Name = "Robert Gray",
                                          PostalAddress = new Address
                                                              {
                                                                  Street = "22 Turrbal St",
                                                                  Suburb = "Bellbowrie",
                                                                  State = "QLD",
                                                                  Postcode = "4070"
                                                              },
                                          HomeAddress = new Address
                                                            {
                                                                Street = "22 Turrbal St",
                                                                Suburb = "Bellbowrie",
                                                                State = "QLD",
                                                                Postcode = "4070"
                                                            },
                                          MobilePhone = "0411331273",
                                          CreditCard = new CreditCard
                                                           {
                                                               CardNumber = "12345865482343",
                                                               NameOnCard = "Mr Robert A Gray",
                                                               ExpiryDate = "03/10",
                                                               CreditCardType = CreditCardType.Visa
                                                           },
                                          DriversLicense = new DriversLicense
                                                               {
                                                                   LicenseNumber = "123482342",
                                                                   DateOfBirth = DateTime.Parse("1976-08-21"),
                                                                   Expiry = _today.AddYears(1)
                                                               }
                                      },
                                  BillingMethod = BillingMethod.Invoice,
                                  BillingAddressType = BillingAddressType.Postal,
                                  ActivationDate = _today,
                                  EndDate = _today.AddMonths(24),
                                  Data = false,
                                  MessageBank = false,
                                  IMEINumber = "123232323",
                                  SimCard = "123232323",
                                  UsedBy = "Rob",
                                  IsBusinessContract = false,
                                  PlanId = 1,
                                  PostBill = true,
                                  User = new User {Username = "UNITTEST"}
                              };

            return request;
        }

        #endregion

        #endregion

        #region ViewContract Tests

        [Test]
        public void I_user_can_retrieve_contract_for_viewing()
        {
            _contractRepository
                .Expect(c => c.GetContract(13))
                .Returns(new Contract(13, DateTime.Now)
                         {                             
                             AccountId = 1
                         });

            _accountRepository
                .Expect(a => a.GetAccount(1))
                .Returns(new MasterAccount(1)
                        {                          
                            ContactId = 3,
                            CompanyId = 3
                        });

            _companyRepository
                .Expect(c => c.GetCompany(3))
                .Returns(new Company(3));

            _contactRepository
                .Expect(c => c.GetContactEntity(3))
                .Returns(new Contact(3));
            
            ViewContractRequest request = new ViewContractRequest();
            request.User = CreateIUser();
            request.ContractId = 13;

            ViewContractResponse response = _service.ViewContract(request);
            Assert.IsFalse(response.IsReadOnly, "Read Only");
            Assert.IsNotNull(response.Contract, "Contract");
            Assert.IsNotNull(response.Account, "Account");
            Assert.IsNotNull(response.Company, "Company");
            
            Assert.AreEqual(13, response.Contract.Id.Value);
            Assert.AreEqual(1, response.Account.Id.Value);
            Assert.AreEqual(3, response.Company.Id.Value);
            Assert.AreEqual(3, response.Contact.Id.Value);
        }

        [Test]
        public void Agent_can_retrieve_contract_for_viewing()
        {
            _contractRepository
                .Expect(c => c.GetContract(13))
                .Returns(new Contract(13, DateTime.Now)
                {                    
                    AccountId = 1
                });

            _accountRepository
                .Expect(a => a.GetAccount(1))
                .Returns(new MasterAccount(1)
                {                    
                    ContactId = 3,
                    CompanyId = 3
                });

            _companyRepository
                .Expect(c => c.GetCompany(3))
                .Returns(new Company(3));

            _contactRepository
                .Expect(c => c.GetContactEntity(3))
                .Returns(new Contact(3));

            ViewContractRequest request = new ViewContractRequest();
            request.User = CreateAgentUser();
            request.ContractId = 13;

            ViewContractResponse response = _service.ViewContract(request);
            Assert.IsTrue(response.IsReadOnly, "Read Only");
            Assert.IsNotNull(response.Contract, "Contract");
            Assert.IsNotNull(response.Account, "Account");
            Assert.IsNotNull(response.Company, "Company");

            Assert.AreEqual(13, response.Contract.Id.Value);
            Assert.AreEqual(1, response.Account.Id.Value);
            Assert.AreEqual(3, response.Company.Id.Value);
            Assert.AreEqual(3, response.Contact.Id.Value);
        }

        [Test]
        public void CustomerUser_can_view_contract()
        {
            // Should add security to prevent customer accessing someone elses data.

            _contractRepository
                .Expect(c => c.GetContract(13))
                .Returns(new Contract(13, DateTime.Now)
                {                    
                    AccountId = 1
                });

            _accountRepository
                .Expect(a => a.GetAccount(1))
                .Returns(new MasterAccount(1)
                {                    
                    ContactId = 3,
                    CompanyId = 3
                });

            _companyRepository
                .Expect(c => c.GetCompany(3))
                .Returns(new Company(3));

            _contactRepository
                .Expect(c => c.GetContactEntity(3))
                .Returns(new Contact(3));

            ViewContractRequest request = new ViewContractRequest();
            request.User = CreateCustomerUser();
            request.ContractId = 13;

            ViewContractResponse response = _service.ViewContract(request);
            Assert.IsTrue(response.IsReadOnly, "Read Only");
            Assert.IsNotNull(response.Contract, "Contract");
            Assert.IsNotNull(response.Account, "Account");
            Assert.IsNotNull(response.Company, "Company");

            Assert.AreEqual(13, response.Contract.Id.Value);
            Assert.AreEqual(1, response.Account.Id.Value);
            Assert.AreEqual(3, response.Company.Id.Value);
            Assert.AreEqual(3, response.Contact.Id.Value);
        }


        #endregion

        #region FindContracts Tests

        [Test]
        public void Search_for_contract_using_contract_number()
        {
            // Setup mock behaviour
            IList<Contract> contracts = new List<Contract>();
            contracts.Add(new Contract(1, DateTime.Now) { AccountId = 1, ContractNumber = "CT9999", ContractStatus = ContractStatus.Active });
            _contractRepository
                .Expect(c => c.FindByContractNumber("CT"))
                .Returns(contracts);

            _accountRepository
                .Expect(a => a.GetAccount(1))
                .Returns(new MasterAccount(1) { AccountName = "Test Account", AccountNumber = "TST0001", ContactId = 1, CompanyId = 1 });

            _contactRepository
                .Expect(c => c.GetContactEntity(1))
                .Returns(new Contact(1) { Name = "Test Contact" });

            _companyRepository
                .Expect(c => c.GetCompany(1))
                .Returns(new Company(1) { CompanyName = "Test Company" });

            User user = new User() { Username = "test", IsActive = true, Email = "test@test.com", IsAuthenticated = true, Name = "Test" };
            ContractSearchRequest request = new ContractSearchRequest() 
            { 
                SearchValue = "CT",
                SearchType = ContractSearchField.ContractNumber, 
                SearchStatus = ContractStatus.Active,
                SearchContractStatusType = SearchContractStatusType.UseStatus,
                User = user
            };
            ContractSearchResponse response = _service.FindContracts(request);

            Assert.IsNotNull(response, "Response is null");
            Assert.IsTrue(response.IsSuccessful, "Response is not successful");
            Assert.IsNotNull(response.Contracts, "Contracts is null");
            Assert.AreEqual(1, response.Contracts.Count, "Contacts count");            
        }

        [Test]
        public void Search_for_contract_using_phone_nummber()
        {
            // Setup mock behaviour
            IList<Contract> contracts = new List<Contract>();
            contracts.Add(new Contract(1, DateTime.Now) { AccountId = 1, ContractNumber = "CT9999", ContractStatus = ContractStatus.Active, PhoneNumber1 = "093233904", PhoneNumber2 = "0339430449" });
            _contractRepository
                .Expect(c => c.FindByPhoneNumber("0339430449"))
                .Returns(contracts);

            _accountRepository
                .Expect(a => a.GetAccount(1))
                .Returns(new MasterAccount(1) { AccountName = "Test Account", AccountNumber = "TST0001", ContactId = 1, CompanyId = 1 });

            _contactRepository
                .Expect(c => c.GetContactEntity(1))
                .Returns(new Contact(1) { Name = "Test Contact" });

            _companyRepository
                .Expect(c => c.GetCompany(1))
                .Returns(new Company(1) { CompanyName = "Test Company" });

            User user = new User() { Username = "test", IsActive = true, Email = "test@test.com", IsAuthenticated = true, Name = "Test" };
            ContractSearchRequest request = new ContractSearchRequest()
            {
                SearchValue = "0339430449",
                SearchType = ContractSearchField.PhoneNumber,
                SearchStatus = ContractStatus.Active,
                SearchContractStatusType = SearchContractStatusType.UseStatus,
                User = user
            };
            ContractSearchResponse response = _service.FindContracts(request);

            Assert.IsNotNull(response, "Response is null");
            Assert.IsTrue(response.IsSuccessful, "Response is not successful");
            Assert.IsNotNull(response.Contracts, "Contracts is null");
            Assert.AreEqual(1, response.Contracts.Count, "Contacts count");
            Assert.AreEqual("0339430449", response.Contracts[0].PhoneNumber, "Phone Number");
        }


        #endregion 

        #region UpdateContract Tests

        [Test]
        public void Activitating_contract_sets_contract_status_to_active()
        {
            var plan = GetPlan();
            var contract = GetContract(ContractStatus.ApplicationPending);
            var account = GetAccount();
            var contact = GetContact();

            _planRepository.Expect(p => p.GetPlan(1)).Returns(plan);
            _contractRepository.Expect(c => c.GetContract(1)).Returns(contract);
            _accountRepository.Expect(a => a.GetAccount(1)).Returns(account);
            _contactRepository.Expect(c => c.GetContactEntity(1)).Returns(contact);

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(q => q.Id.HasValue && q.ContractStatus == ContractStatus.Active)))
                .Returns(true);

            _accountRepository.Expect(a => a.UpdateAccount(It.IsAny<Account>())).Returns(true);
            _contactRepository.Expect(c => c.UpdateContact(It.IsAny<Contact>())).Returns(true);
                
            updateRequest = CreateUpdateContractResponse(contract, account, contact);
            var response = _service.UpdateContract(updateRequest);

            Assert.IsTrue(response.IsSuccessful, response.Message, "Update not successful");
            Assert.IsNotNull(response.Contract, "Null Contract");            
        }

        [Test]
        public void Agent_contract_update_cuases_no_permissions_error()
        {
            updateRequest.User = CreateAgentUser();
            UpdateContractResponse response = _service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);

        }

        [Test]
        public void Customer_contract_update_causes_no_permissions_error()
        {
            updateRequest.User = CreateCustomerUser();
            UpdateContractResponse response = _service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);
        }

        [Test]
        public void Cannot_update_contract_when_no_user_supplied()
        {
            updateRequest.User = null;
            UpdateContractResponse response = _service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.NO_PERMISSIONS, response.Message);
        }

        [Test]
        public void Contract_update_failed_database_action_returns_internal_error()
        {
            var plan = GetPlan();
            var contract = GetContract(ContractStatus.ApplicationPending);
            var account = GetAccount();
            var contact = GetContact();

            _planRepository.Expect(p => p.GetPlan(1)).Returns(plan);
            _contractRepository.Expect(c => c.GetContract(1)).Returns(contract);
            _accountRepository.Expect(a => a.GetAccount(1)).Returns(account);
            _contactRepository.Expect(c => c.GetContactEntity(1)).Returns(contact);

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(q => q.Id.HasValue)))
                .Returns(false);

            updateRequest = CreateUpdateContractResponse(contract, account, contact);
            UpdateContractResponse response = _service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.INTERNAL_ERROR, response.Message, "Error Message");
        }

        [Test]
        public void Updating_not_found_contract_returns_internal_error()
        {
            _planRepository
                .Expect(p => p.GetPlan(updateRequest.PlanId))
                .Returns(new Plan(updateRequest.PlanId));

            _contractRepository
                .Expect(c => c.GetContract(updateRequest.ContractId))
                .Returns<Contract>(null);

            updateRequest.User = CreateIUser();
            UpdateContractResponse response = _service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.INTERNAL_ERROR, response.Message, "Error Message");
        }

        [Test]
        public void Cannot_update_contract_when_plan_missing()
        {
            _planRepository
                .Expect(p => p.GetPlan(updateRequest.PlanId))
                .Returns<Plan>(null);

            updateRequest.User = CreateIUser();
            UpdateContractResponse response = _service.UpdateContract(updateRequest);

            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("A Valid Plan must be supplied", response.Message);
        }

        #endregion

        #region Plan Variance Tests

        [Test]
        public void Varying_contract_plan_writes_contract_plan_variance_record()
        {
            var contract = GetContract(ContractStatus.Active);
            var plan = GetPlan();
            var account = GetAccount();
            var contact = GetContact();

            _planRepository.Expect(p => p.GetPlan(1)).Returns(plan);
            _contractRepository.Expect(c => c.GetContract(1)).Returns(contract);
            _accountRepository.Expect(a => a.GetAccount(1)).Returns(account);
            _contactRepository.Expect(c => c.GetContactEntity(1)).Returns(contact);
         

            VaryContractPlanRequest request = new VaryContractPlanRequest()
            {
                ContractId = contract.Id.Value,
                NewPlanId = plan.Id.Value,
                Cost = 0M,
                StartDate = DateTime.Today.AddDays(-10),
                EndDate = DateTime.Today.AddDays(10),
                User = CreateIUser()
            };

            _contractRepository
              .Expect(c => c.InsertContractPlanVariance(It.Is<Contract>(ct => ct.Id.Value == 1),
                  It.Is<Plan>(p => p.Id.Value == 1), request.StartDate, request.EndDate, CreateIUser().Username, 0M))
              .Returns(5);

            VaryContractPlanResponse response = _service.VaryContractPlan(request);
            Assert.IsTrue(response.IsSuccessful);
        }

        #endregion 
    
        #region Close Contract Tests

        [Test]
        public void Contract_can_be_closed_when_expired()
        {
            var contract = new Contract(1, DateTime.Now)
                               {
                                   AccountId = 1,
                                   ActivationDate = DateTime.Parse("2007-01-04"),
                                   EndDate = DateTime.Parse("2009-01-04"),
                                   ContractStatus = ContractStatus.Active
                               };

            // if closed before contract end date "Contract cancelled before contract end date"
            // else "Contract closed".
            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(contract);

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(e => e.StatusChangedDate.Date == DateTime.Today && e.ContractStatus == ContractStatus.Closed)))
                .Returns(true);

            // Send email notifying of closed contract

            // write activity log for email send.    
            _activityService
                .Expect(a => a.WriteActivity(contract, "Contract Closed"))
                .AtMostOnce();

            var request = new CloseContractRequest { ContractId = 1, User = CreateIUser() };
            var response = _service.CloseContract(request);
            Assert.IsTrue(response.IsSuccessful);            
        }

        [Test]
        public void Closing_contract_before_end_date_writes_cancelled_before_end_activity()
        {
            var contract = new Contract(1, DateTime.Now)
            {
                AccountId = 1,
                ActivationDate = DateTime.Parse("2008-01-04"),
                EndDate = DateTime.Parse("2010-01-04"),
                ContractStatus = ContractStatus.Active 
            };
     
            // if closed before contract end date "Contract cancelled before contract end date"
            // else "Contract closed".
            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(contract);

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(e => e.StatusChangedDate.Date == DateTime.Today && e.ContractStatus == ContractStatus.Closed)))
                .Returns(true);

            // Send email notifying of closed contract

            // write activity log for email send.                                       
            _activityService
                .Expect(a => a.WriteActivity(contract, "Contract cancelled before contract end date ('" + contract.EndDate.ToShortDateString() + "')"))
                .AtMostOnce();

            var request = new CloseContractRequest { ContractId = 1, User = CreateIUser() };
            var response = _service.CloseContract(request);
            Assert.IsTrue(response.IsSuccessful);
        }

        #endregion 
    
        #region Suspend Contract Tests

        [Test]
        public void Suspended_contract_writes_activity_log()
        {
            var suspendUntil = DateTime.Today.AddDays(55);

            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(new Contract(1, DateTime.Now)
                            {
                                AccountId = 1,
                                ActivationDate = DateTime.Today.AddMonths(-3),
                                EndDate = DateTime.Today.AddMonths(9),
                                ContractStatus = ContractStatus.Active,  
                                StatusChangedDate = DateTime.Today.AddMonths(-3),                                
                                SuspendedUntilDate = null,
                                ContractNumber = "CT0001",
                                IMEINumber = "3902390233",
                                SimCard = "2390239232",
                                PhoneNumber1 = "0893827894"
                            });

            _activityService
                .Expect(a => a.WriteActivity(new Contract(1, DateTime.Now), string.Format("Suspended Contract until {0}", suspendUntil.ToString("dd/MM/yyyy"))));                

            _contractRepository 
                .Expect(c => c.UpdateContract(It.Is<Contract>(m => m.AccountId == 1 &&
                                                                    m.ActivationDate == DateTime.Today.AddMonths(-3) && 
                                                                    m.EndDate == DateTime.Today.AddMonths(9) && 
                                                                    m.ContractStatus == ContractStatus.Suspended && 
                                                                    m.StatusChangedDate.Date == DateTime.Today && 
                                                                    m.SuspendedUntilDate == suspendUntil && 
                                                                    m.ContractNumber == "CT0001" && 
                                                                    m.IMEINumber == "3902390233" && 
                                                                    m.SimCard == "2390239232" && 
                                                                    m.PhoneNumber1 == "0893827894")))
                .Returns(true);

            var request = new SuspendContractRequest { ContractId = 1, SuspendUntil = suspendUntil, User = CreateIUser() };                                                                      
            var success = _service.SuspendContract(request);
            Assert.IsTrue(success.IsSuccessful);            
        }

        [Test]
        public void Cannot_suspend_not_active_contract()
        {
            var suspendUntil = DateTime.Today.AddDays(55);

            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(new Contract(1, DateTime.Now)
                {
                    AccountId = 1,
                    ActivationDate = DateTime.Today.AddMonths(-3),
                    EndDate = DateTime.Today.AddMonths(9),
                    ContractStatus = ContractStatus.ApplicationPending,
                    StatusChangedDate = DateTime.Today.AddMonths(-3),                    
                    SuspendedUntilDate = null,
                    ContractNumber = "CT0001",
                    IMEINumber = "3902390233",
                    SimCard = "2390239232",
                    PhoneNumber1 = "0893827894"
                });

            _activityService
                .Expect(a => a.WriteActivity(new Contract(1, DateTime.Now), string.Format("Suspended Contract until {0}", suspendUntil.ToString("dd/MM/yyyy"))));

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(m => m.AccountId == 1 &&
                                                                    m.ActivationDate == DateTime.Today.AddMonths(-3) &&
                                                                    m.EndDate == DateTime.Today.AddMonths(9) &&
                                                                    m.ContractStatus == ContractStatus.Suspended &&
                                                                    m.StatusChangedDate.Date == DateTime.Today &&
                                                                    m.SuspendedUntilDate == suspendUntil &&
                                                                    m.ContractNumber == "CT0001" &&
                                                                    m.IMEINumber == "3902390233" &&
                                                                    m.SimCard == "2390239232" &&
                                                                    m.PhoneNumber1 == "0893827894")))
                .Returns(true);

            var request = new SuspendContractRequest { ContractId = 1, SuspendUntil = suspendUntil, User = CreateIUser() };
            var success = _service.SuspendContract(request);
            Assert.IsFalse(success.IsSuccessful);
            Assert.AreEqual(success.Message, "Cannot suspend a contract that is not active.  Contract is " +
                                             ContractStatus.ApplicationPending.ToString());
        }

        [Test]
        public void Cannot_suspend_not_found_contract()
        {
            var suspendUntil = DateTime.Today.AddDays(55);

            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns<Contract>(null);

            _activityService
                .Expect(a => a.WriteActivity(new Contract(1, DateTime.Now), string.Format("Suspended Contract until {0}", suspendUntil.ToString("dd/MM/yyyy"))));

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(m => m.AccountId == 1 &&
                                                                    m.ActivationDate == DateTime.Today.AddMonths(-3) &&
                                                                    m.EndDate == DateTime.Today.AddMonths(9) &&
                                                                    m.ContractStatus == ContractStatus.Suspended &&
                                                                    m.StatusChangedDate.Date == DateTime.Today &&
                                                                    m.SuspendedUntilDate == suspendUntil &&
                                                                    m.ContractNumber == "CT0001" &&
                                                                    m.IMEINumber == "3902390233" &&
                                                                    m.SimCard == "2390239232" &&
                                                                    m.PhoneNumber1 == "0893827894")))
                .Returns(true);

            var request = new SuspendContractRequest { ContractId = 1, SuspendUntil = suspendUntil, User = CreateIUser() };
            var success = _service.SuspendContract(request);
            Assert.IsFalse(success.IsSuccessful);
            Assert.AreEqual(success.Message, "Could not find contract with contractId=1");
        }

        [Test]        
        // TODO - Find out what this tests.
        public void SuspendContract_ContractNotUpdated()
        {
            var suspendUntil = DateTime.Today.AddDays(55);

            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(new Contract(1, DateTime.Now)
                {
                    AccountId = 1,
                    ActivationDate = DateTime.Today.AddMonths(-3),
                    EndDate = DateTime.Today.AddMonths(9),
                    ContractStatus = ContractStatus.Active,
                    StatusChangedDate = DateTime.Today.AddMonths(-3),
                    SuspendedUntilDate = null,
                    ContractNumber = "CT0001",
                    IMEINumber = "3902390233",
                    SimCard = "2390239232",
                    PhoneNumber1 = "0893827894"
                });

            _activityService
                .Expect(a => a.WriteActivity(new Contract(1, DateTime.Now), string.Format("Suspended Contract until {0}", suspendUntil.ToString("dd/MM/yyyy"))));

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(m => m.AccountId == 1 &&
                                                                    m.ActivationDate == DateTime.Today.AddMonths(-3) &&
                                                                    m.EndDate == DateTime.Today.AddMonths(9) &&
                                                                    m.ContractStatus == ContractStatus.Suspended &&
                                                                    m.StatusChangedDate.Date == DateTime.Today &&
                                                                    m.SuspendedUntilDate == suspendUntil &&
                                                                    m.ContractNumber == "CT0001" &&
                                                                    m.IMEINumber == "3902390233" &&
                                                                    m.SimCard == "2390239232" &&
                                                                    m.PhoneNumber1 == "0893827894")))
                .Returns(false);

            var request = new SuspendContractRequest { ContractId = 1, SuspendUntil = suspendUntil, User = CreateIUser() };
            var success = _service.SuspendContract(request);
            Assert.IsFalse(success.IsSuccessful);
            Assert.AreEqual(success.Message, "The suspension could not be completed.");
        }

        [Test]        
        public void Cannot_suspend_contract_until_a_past_date()
        {
            var suspendUntil = DateTime.Today.AddDays(-5);

            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(new Contract(1, DateTime.Now)
                {
                    AccountId = 1,
                    ActivationDate = DateTime.Today.AddMonths(-3),
                    EndDate = DateTime.Today.AddMonths(9),
                    ContractStatus = ContractStatus.Active,
                    StatusChangedDate = DateTime.Today.AddMonths(-3),
                    SuspendedUntilDate = null,
                    ContractNumber = "CT0001",
                    IMEINumber = "3902390233",
                    SimCard = "2390239232",
                    PhoneNumber1 = "0893827894"
                });

            _activityService
                .Expect(a => a.WriteActivity(new Contract(1, DateTime.Now), string.Format("Suspended Contract until {0}", suspendUntil.ToString("dd/MM/yyyy"))));

            _contractRepository
                .Expect(c => c.UpdateContract(It.Is<Contract>(m => m.AccountId == 1 &&
                                                                    m.ActivationDate == DateTime.Today.AddMonths(-3) &&
                                                                    m.EndDate == DateTime.Today.AddMonths(9) &&
                                                                    m.ContractStatus == ContractStatus.Suspended &&
                                                                    m.StatusChangedDate.Date == DateTime.Today &&
                                                                    m.SuspendedUntilDate == suspendUntil &&
                                                                    m.ContractNumber == "CT0001" &&
                                                                    m.IMEINumber == "3902390233" &&
                                                                    m.SimCard == "2390239232" &&
                                                                    m.PhoneNumber1 == "0893827894")))
                .Returns(false);

            var request = new SuspendContractRequest { ContractId = 1, SuspendUntil = suspendUntil, User = CreateIUser() };
            var success = _service.SuspendContract(request);
            Assert.IsFalse(success.IsSuccessful);
            Assert.AreEqual(success.Message, "Cannot suspend a contract until a day in the past");
        }

        #endregion 

        #region Uninvoiced Calls Test

        [Test]
        public void Retrieve_uninvoiced_calls_for_contract()
        {
            // check contract exists
            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(new Contract(1, DateTime.Now));

            // return some calls for the contract
            _contractRepository
                .Expect(c => c.GetNonInvoicedCallsByContractId(1))
                .Returns(new List<Call>
                            {
                                new Call(1)
                                    {
                                        CalledFrom = "Test",
                                        CallTime = DateTime.Now,
                                        ContractId = 1,
                                        Cost = 5m,
                                        Volume = 15.4m,
                                        HasFreeCallTariff = false,                                                            
                                        ImportedCallType = "Test",
                                        NumberCalled = "123923TEST",
                                        PhoneNumber = "MINE"
                                    }
                            });

            var request = new GetUnInvoicedCallsRequest { ContractId = 1, User = CreateIUser() };
            var response = _service.GetUnInvoicedCalls(request);
            Assert.IsTrue(response.IsSuccessful, "Successful");
            Assert.IsTrue(response.UninvoicedCalls.Count == 1, "Count");
        }

        [Test]
        public void GetUnInvoicedCalls_for_contract_with_no_calls_returns_empty_list()
        {
            // check contract exists
            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns(new Contract(1, DateTime.Now));

            // return some calls for the contract
            _contractRepository
                .Expect(c => c.GetNonInvoicedCallsByContractId(1))
                .Returns(new List<Call>());

            var request = new GetUnInvoicedCallsRequest { ContractId = 1, User = CreateIUser() };
            var response = _service.GetUnInvoicedCalls(request);
            Assert.IsTrue(response.IsSuccessful, "Successful");
            Assert.IsTrue(response.UninvoicedCalls.Count == 0, "Count");
        }

        [Test]
        public void GetUnInvoicedCalls_for_non_existent_contract_returns_not_found()
        {
            _contractRepository
                .Expect(c => c.GetContract(1))
                .Returns<Contract>(null);

            var request = new GetUnInvoicedCallsRequest { ContractId = 1, User = CreateIUser() };
            var response = _service.GetUnInvoicedCalls(request);
            
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(response.Message, "Contract 1 not found.");
        }

        #endregion 

        #region Custom Fee Tests

        [Test]
        public void GetAllCustomFees_returns_empty_when_contract_has_no_custom_fees()
        {
            // Arrange
            var contract = new Contract(1, _today);
            _contractRepository.Expect(c => c.GetContract(contract.Id.Value)).Returns(contract);
            _contractRepository.Expect(c => c.GetAllCustomFeesForContract(contract)).Returns(new List<CustomFee>());
            var request = new CustomFeesRequest {ContractId = contract.Id.Value};

            // Act
            var response = _service.GetCustomFeesForContract(request);

            // Assert
            Assert.IsTrue(response.IsSuccessful);
            Assert.AreEqual(0, response.CustomFees.Count);
        }

        [Test]
        public void GetAllCustomFees_returns_custom_fees_for_contract()
        {
            // Arrange
            var contract = new Contract(1, _today);
            _contractRepository.Expect(c => c.GetContract(contract.Id.Value)).Returns(contract);
            _contractRepository.Expect(c => c.GetAllCustomFeesForContract(contract)).Returns(new List<CustomFee> { new CustomFee(123) { Description = "Test Fee", Amount = 30M, IsRecurring = false } });
            var request = new CustomFeesRequest { ContractId = contract.Id.Value };

            // Act
            var response = _service.GetCustomFeesForContract(request);

            // Assert
            Assert.IsTrue(response.IsSuccessful);
            Assert.AreEqual(1, response.CustomFees.Count);
        }

        [Test]
        public void GetAllCustomFees_handles_missing_contract()
        {
            var contract = new Contract(1, _today);
            _contractRepository.Expect(c => c.GetContract(contract.Id.Value)).Returns<Contract>(null);
            _contractRepository.Expect(c => c.GetAllCustomFeesForContract(contract)).Returns(new List<CustomFee> { new CustomFee(123) { Description = "Test Fee", Amount = 30M, IsRecurring = false } });
            var request = new CustomFeesRequest { ContractId = contract.Id.Value };

            // Act
            var response = _service.GetCustomFeesForContract(request);

            // Assert
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual("Could not find Contract with Id=1", response.Message);            
        }

        [Test]
        public void GetAllCustomFees_handles_contract_repository_exception_with_internal_error_message()
        {
            var contract = new Contract(1, _today);
            _contractRepository.Expect(c => c.GetContract(contract.Id.Value)).Throws(new Exception("Fail"));
            _contractRepository.Expect(c => c.GetAllCustomFeesForContract(contract)).Returns(new List<CustomFee> { new CustomFee(123) { Description = "Test Fee", Amount = 30M, IsRecurring = false } });
            var request = new CustomFeesRequest { ContractId = contract.Id.Value };

            // Act
            var response = _service.GetCustomFeesForContract(request);

            // Assert
            Assert.IsFalse(response.IsSuccessful);
            Assert.AreEqual(Constants.Messages.INTERNAL_ERROR, response.Message);
        }
            

        #endregion 

       

        #region Test Helpers

        private static Contract GetContract(ContractStatus status)
        {
            var contract = new Contract(1, DateTime.Today.AddMonths(-1))
            {
                ContractNumber = "CT0001",
                ContractStatus = status,
                AccountId = 1,
                PlanId = 1,
                ActivationDate = DateTime.Today.AddMonths(-1),
                EndDate = DateTime.Today.AddMonths(11),
                PhoneNumber1 = "0411331273",
                Puk = "1234",
                Pin = "1234",
                IMEINumber = "123423232345",
                SimCard = "123123123123"
            };

            return contract;
        }

        private static Plan GetPlan()
        {
            var plan = new Plan(1)
            {
                DefaultTariffId = 1,
                Duration = 12,
                FlagFall = 0.5M,
                FreeCallAmount = 30M,
                Name = "Test Plan",
                NetworkId = 1,
                PlanAmount = 50,
                UnitOfTime = 30
            };

            return plan;
        }

        private static Account GetAccount()
        {
            var account = new Account(1)
            {
                AccountNumber = "TST0001",
                AccountName = "Test Account",
                ContactId = 1,
                EmailBill = true,
                PostBill = true,
                BillingMethod = BillingMethod.CreditCard,
                Password = "TEST111"
            };

            return account;
        }

        private static Contact GetContact()
        {
            var contact = new Contact(1)
            {
                Email = "test@test.com",
                Name = "TEST MAN"
            };

            return contact;
        }

        private static Address GetAddress()
        {
            return new Address
                   {
                       Street = "123 Test Ave",
                       Suburb = "Testtown",
                       State = "QLD",
                       Postcode = "4999"
                   };
        }


        private static UpdateContractRequest CreateUpdateContractResponse(Contract contract, Account account, Contact contact)
        {
            return new UpdateContractRequest
                           {
                               AccountName = account.AccountName,
                               AccountPassword = account.Password,
                               ActivationDate = contract.ActivationDate,
                               EndDate = contract.EndDate,
                               ContactEmail = contact.Email,
                               CreditCardExpiry = contact.CreditCard.ExpiryDate,
                               CreditCardName = contact.CreditCard.NameOnCard,
                               CreditCardNumber = contact.CreditCard.CardNumber,
                               CreditCardType = contact.CreditCard.CreditCardType,
                               BillingAddress = account.BillingAddressType,
                               PhoneNumber1 = contract.PhoneNumber1,
                               PhoneNumber2 = contract.PhoneNumber2,
                               PhoneNumber3 = contract.PhoneNumber3,
                               PhoneNumber4 = contract.PhoneNumber4,
                               ContactName = contact.Name,
                               BillingMethod = account.BillingMethod,
                               ContractId = contact.Id.Value,
                               DateOfBirth = contact.DriversLicense.DateOfBirth,
                               DeliveryAddress = contact.HomeAddress,
                               EmailDataFile = account.EmailBillDataFile,
                               FaxNumber = contact.FaxNumber,
                               LicenseExpiry = contact.DriversLicense.Expiry,
                               HomeAddress = contact.HomeAddress,
                               LicenseNumber = contact.DriversLicense.LicenseNumber,
                               MobilePhoneNumber = contact.MobilePhone,
                               PaperlessBill = account.EmailBill,
                               Pin = contract.Pin,
                               PlanId = contract.PlanId,
                               PostBill = account.PostBill,
                               Puk = contract.Puk,
                               UsedBy = contract.UsedBy,
                               User = CreateIUser(),
                               WorkNumber = contact.WorkPhone
                           };
        }

        #endregion 
    }
}
