﻿using System;
using System.Collections.Generic;
using AirtimeBilling.Core.Entities;
using AirtimeBilling.Core.Enums;
using AirtimeBilling.Core.Repositories;
using NUnit.Framework;
using Moq;

namespace AirtimeBilling.Monitor.Tests
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestFixture]
    public class ContractStatusMonitorTests
    {        
        private Mock<IContractRepository> contractRepository;
        private ScheduledJob monitor;

        private Contract offSuspensionContract;
        private Contract stillSuspendedContract;
        private Contract activeContract;
            
        [SetUp]
        public void MyTestInitialize()
        {
            contractRepository = new Mock<IContractRepository>();
            monitor = new ContractStatusMonitor(contractRepository.Object);

            offSuspensionContract = new Contract(DateTime.Now)
                                        {
                                            AccountId = 1,
                                            ActivationDate = DateTime.Today.AddYears(-1),
                                            EndDate = DateTime.Today.AddYears(1),
                                            ContractNumber = "CT00001",
                                            ContractStatus = ContractStatus.Suspended,
                                            PlanId = 1,
                                            SuspensionStartDate = DateTime.Now.AddDays(-5),
                                            SuspendedUntilDate = DateTime.Now.AddMinutes(-5),
                                            IMEINumber = "1235",
                                            SimCard = "1235",
                                            PhoneNumber1 = "12354",
                                    };


            stillSuspendedContract = new Contract(DateTime.Now)
                                    {
                                        AccountId = 1,
                                        ActivationDate = DateTime.Today.AddYears(-1),
                                        EndDate = DateTime.Today.AddYears(1),
                                        ContractNumber = "CT00002",
                                        ContractStatus = ContractStatus.Suspended,
                                        PlanId = 1,
                                        SuspensionStartDate = DateTime.Now.AddDays(-5),
                                        SuspendedUntilDate = DateTime.Now.AddDays(5),
                                        IMEINumber = "1235",
                                        SimCard = "1235",
                                        PhoneNumber1 = "12354",
                                    };

            activeContract = new Contract(DateTime.Now)
                                    {
                                        AccountId = 1,
                                        ActivationDate = DateTime.Today.AddYears(-1),
                                        EndDate = DateTime.Today.AddYears(1),
                                        ContractNumber = "CT00003",
                                        ContractStatus = ContractStatus.Active,
                                        PlanId = 1,
                                        IMEINumber = "1235",
                                        SimCard = "1235",
                                        PhoneNumber1 = "12354",
                                    };

        }      

        [Test]
        public void ContractStatusMonitorUpdateStatus()
        {
            // return contracts that:
            // 1. should come off suspension
            // 2. is still on suspension
            // 3. not suspended at all.
            contractRepository
                .Expect(c => c.GetAllContracts())
                .Returns(new List<Contract> {offSuspensionContract, stillSuspendedContract, activeContract});

            Assert.IsNull(monitor.LastRunTime, "Job has been run before");
            
            monitor.Execute();

            Assert.IsNotNull(monitor.LastRunTime, "Null Last Run Time");
            Assert.IsTrue(monitor.LastRunTime <= DateTime.Now, "Last Run Time wrong");
            Assert.IsTrue(monitor.NextRunTime >= DateTime.Now && monitor.NextRunTime > monitor.LastRunTime, "Not updated run time");
            
            // check state of contracts
            Assert.AreEqual(ContractStatus.Active, offSuspensionContract.ContractStatus, "Did not make active");
            Assert.AreEqual(ContractStatus.Suspended, stillSuspendedContract.ContractStatus, "Should not have changed suspended status");
            Assert.AreEqual(ContractStatus.Active, activeContract.ContractStatus, "Should not have changed active status");
        }
    }
}
