﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SharpStudios.AirtimeBilling.Logging;

namespace SharpStudios.AirtimeBilling.LoggingService.TestGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoggingUtility.SendEmail(txtToEmail.Text, "Test Mail", txtMessage.Text, true);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoggingUtility.LogDebug("Load", "Form1", "Loading the shiznit");
        }
    }
}
