﻿namespace AirtimeBilling.Common
{
    public static class Constants
    {
        static Constants() { }

        public const string SYSTEM_USER_NAME = "System";

        public const string NAMESPACE_ROOT = "http://www.robertgray.net.au/airtimebilling";
        public const string NAMESPACE_SERVICES = NAMESPACE_ROOT + "/Services";
        public const string NAMESPACE_DATA = NAMESPACE_ROOT + "/Data";

        public const string NAMESPACE_SERVICES_CONTRACT = NAMESPACE_SERVICES + "/Contract";
        public const string NAMESPACE_SERVICES_SECURITY = NAMESPACE_SERVICES + "/Security";
        public const string NAMESPACE_SERVICES_MAINTENANCE = NAMESPACE_SERVICES + "/Maintenance";
        public const string NAMESPACE_SERVICES_REPORTING = NAMESPACE_SERVICES + "/Reporting";

        public const string CONFIG_KEY_FORCE_SSL = "ForceSSL";
        public const bool DEFAULT_FORCE_SSL = false;

        public const string CONFIG_KEY_LOCAL_NETWORK_IP_RANGE = "LocalNetworkIPRange";
        public const string DEFAULT_LOCAL_NETWORK_IP_RANGE = "192.168";

        // Configuration keys and default values
        public const string CONFIG_KEY_DATABASE_CONNECTION = "DatabaseConnection";
        public const string DEFAULT_DATABASE_CONNECTION = "Data Source=.;Initial Catalog=airtimebilling;user id=airtimebilling;password=airtimebilling;";

        // SMTP Host
        public const string CONFIG_KEY_SMTP_HOST = "SmtpHost";
        public const string DEFAULT_SMTP_HOST = "localhost";

        // System Email Address
        public const string CONFIG_KEY_SYSTEM_EMAIL_ADDRESS = "SystemEmailAddress";
        public const string DEFAULT_SYSTEM_EMAIL_ADDRESS = "test@test.com.au";

        // New Contract Email Address
        public const string CONFIG_KEY_NEW_CONTRACT_EMAIL_ADDRESS = "NewContractEmailAddress";
        public const string DEFAULT_NEW_CONTRACT_EMAIL_ADDRESS = "test@test.com.au";

        // Error Email Address
        public const string CONFIG_KEY_ERROR_EMAIL_ADDRESS = "ErrorEmailAddress";
        public const string DEFAULT_ERROR_EMAIL_ADDRESS = "test@test.com.au";

        // Send Error Email
        public const string CONFIG_KEY_SEND_ERROR_EMAIL = "SendErrorEmail";
        public const bool DEFAULT_SEND_ERROR_EMAIL = false;

        // Units of Time 
        public const string CONFIG_KEY_UNITS_OF_TIME_SIZE = "UnitsOfTimeSize";
        public const int DEFAULT_UNITS_OF_TIME_SIZE = 30;

        // Import Folder
        public const string CONFIG_KEY_IMPORT_FOLDER = "ImportFolder";
        public const string DEFAULT_IMPORT_FOLDER = "%TEMP%";

        // Invoices Folder
        public const string CONFIG_KEY_INVOICES_FOLDER = "InvoicesFolder";
        public const string DEFAULT_INVOICES_FOLDER = "%TEMP%";

        // Contract Number Prefix 
        public const string CONFIG_KEY_CONTRACT_NUMBER_PREFIX = "ContractNumberPrefix";
        public const string DEFAULT_CONTRACT_NUMBER_PREFIX = "";

        // Contract Number Format
        public const string CONFIG_KEY_CONTRACT_NUMBER_FORMAT = "ContractNumberFormat";
        public const string DEFAULT_CONTRACT_NUMBER_FORMAT = "0";

        // Account Number Prefix
        public const string CONFIG_KEY_ACCOUNT_NUMBER_PREFIX = "AccountNumberPrefix";
        public const string DEFAULT_ACCOUNT_NUMBER_PREFIX = "";

        // Account Number Format
        public const string CONFIG_KEY_ACCOUNT_NUMBER_FORMAT = "AccountNumberFormat";
        public const string DEFAULT_ACCOUNT_NUMBER_FORMAT = "0";

        // Match Calls before invoicing
        public const string CONFIG_KEY_MATCH_CALLS_BEFORE_INVOICING = "MatchCallsBeforeInvoicing";
        public const bool DEFAULT_MATCH_CALLS_BEFORE_INVOICING = false;

        // Print Closed Contract Letter
        public const string CONFIG_KEY_PRINT_CLOSED_CONTRACT_LETTER = "PrintClosedContractLetter";
        public const bool DEFAULT_PRINT_CLOSED_CONTRACT_LETTER = false;

        // Print Suspended Contract Letter
        public const string CONFIG_KEY_PRINT_SUSPENDED_CONTRACT_LETTER = "PrintSuspendedContractLetter";
        public const bool DEFAULT_PRINT_SUSPENDED_CONTRACT_LETTER = false;

        public const string CONFIG_KEY_ALLOW_NEW_ASSOCIATED_ENTITIES = "AllowNewAssociatedEntities";
        public const bool DEFAULT_ALLOW_NEW_ASSOCIATED_ENTITIES = true;

        public const string CONFIG_KEY_USE_SLL = "UseSSL";
        public const bool DEFAULT_USE_SSL = true;

        public static class Messages
        {
            static Messages() { }

            public const string NO_PERMISSIONS = "This user does not have permission to perform the requested action.";
            public const string NEW_CONTRACT_EMAIL = "A request for airtime has been submitted.  Please open the following link to begin the approval process.<BR/><BR/>Link: {0}.";
            public const string UNKNOWN_FAILURE = "Unkown Failure.  Please contact system support.";
            public const string INTERNAL_ERROR = "An internal error occurred.  System Support has been notified with the details.";
            //public const string REVIEWING_CONTRACT = "Contract '{0}' for {1} has started the activation process";
            public const string REVIEWING_CONTRACT =
                "<br/><br/>Super Satellite Solutions have begun the Contract activation process for contract <b>{0}</b> for <b>{1}</b>.<br/>" +
                "An activation confirmation email will be sent shortly, once the activation has been completed.<br/><br/>" +
                "Kind Regards<br/>Super Satellite Solutions";
        }

        /// <summary>
        /// Contains the table names used in the system
        /// </summary>
        public static class TableNames
        {
            static TableNames() { }

            public const string ACCOUNT = "Acount";
            public const string ACCOUNT_INVOICE = "AccountInvoice";
            public const string ACTIVITY_LOG = "ActivityLog";
            public const string AGENT = "Agent";
            public const string AIRTIME_PROVIDER = "AirtimeProvider";
            public const string AUDIT = "Audit";
            public const string CALL = "Call";
            public const string COMPANY = "Company";
            public const string CONTACT = "Contact";
            public const string CONTRACT = "Contract";
            public const string CONTRACT_PLAN_VARIANCE = "ContractPlanVariance";
            public const string INVOICE_HEADER = "InvoiceHeader";
            public const string INVOICE_LINE_ITEM = "InvoiceLineItem";
            public const string NETWORK = "Network";
            public const string NETWORK_TARIFF = "NetworkTariff";
            public const string PLAN = "Plan";
            public const string TARIFF = "Tariff";
            public const string USER_PROFILE = "UserProfile";
            public const string VIZADA_DATA = "VizadaData";
            public const string VIZADA_MAIN_SERVICE_CODE = "VizadaMainServiceCode";
            public const string VIZADA_SERVICE_CODE = "VizadaServiceCode";
            public const string VIZADA_SUB_SERVICE_CODE = "VizadaSubServiceCode";
            public const string SATCOM_DATA = "SatComData";
            public const string SATCOM_CALL_INDENTIFIER = "SatComCallIdentifier";
        }

        /// <summary>
        /// Contains Keys and Defaults for the system Reports
        /// </summary>           
        public static class Reports
        {
            static Reports() { }

            public const string CONFIG_KEY_REPORTS_FOLDER = "Reports";
            public const string DEFAULT_REPORTS_FOLDER = "Reports";
            public const string CONFIG_KEY_AIRTIME_CONTRACT = "AirtimeContract";
            public const string DEFAULT_AIRTIME_CONTRACT_RPT = "AirtimeContract.rpt";
            public const string CONFIG_KEY_INVOICE = "Invoice";
            public const string DEFAULT_INVOICE_RPT = "AirtimeInvoice.rpt";
        }

        /// <summary>
        /// Contains Keys and Defaults for MSMQ components 
        /// </summary>
        public static class Msmq
        {
            static Msmq() { }

            public const string CONFIG_KEY_MSMQ_EMAIL = "MsmqEmail";
            public const string DEFAULT_MSMQ_EMAIL = "email";
            public const string CONFIG_KEY_MSMQ_ACTIVITY = "MsmqActivity";
            public const string DEFAULT_MSMQ_ACTIVITY = "activity";
            public const string CONFIG_KEY_MSMQ_EXCEPTION = "MsmqException";
            public const string DEFAULT_MSMQ_EXCEPTION = "exception";
            public const string CONFIG_KEY_MSMQ_DEBUG = "MsmqDebug";
            public const string DEFAULT_MSMQ_DEBUG = "debug";
        }
    }
}