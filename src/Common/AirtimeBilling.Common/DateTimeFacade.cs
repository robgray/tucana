﻿using System;

namespace AirtimeBilling.Common
{
    public class DateTimeFacade : IDateTimeFacade
    {        
        public DateTime Now
        {
            get
            {
                return DateTime.Now;
            }
        }        

        public DateTime Today
        {
            get
            {
                return DateTime.Today;
            }
        }
    }
}
