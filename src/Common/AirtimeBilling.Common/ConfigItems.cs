﻿using System;
using System.Configuration;

namespace AirtimeBilling.Common
{
    /// <summary>
    /// Contains all the configuration settings for the application.
    /// </summary>
    public static class ConfigItems
    {
        #region Private Fields

        private readonly static string _connectString = null;       
        private readonly static string _systemEmail = Constants.DEFAULT_SYSTEM_EMAIL_ADDRESS;
        private readonly static string _newContractEmail = Constants.DEFAULT_NEW_CONTRACT_EMAIL_ADDRESS;
        private readonly static string _errorEmail = Constants.DEFAULT_ERROR_EMAIL_ADDRESS;
        private readonly static bool _sendErrorEmails = Constants.DEFAULT_SEND_ERROR_EMAIL;
        private readonly static int _unitsOfTimeSize = Constants.DEFAULT_UNITS_OF_TIME_SIZE;
        private readonly static string _airtimeContract = Constants.Reports.DEFAULT_REPORTS_FOLDER + "/" + Constants.Reports.DEFAULT_AIRTIME_CONTRACT_RPT;
        private readonly static string _invoice = Constants.Reports.DEFAULT_INVOICE_RPT;
        private readonly static string _importFolder = String.Empty;
        private readonly static string _msmqEmail = Constants.Msmq.DEFAULT_MSMQ_EMAIL;
        private readonly static string _msmqException = Constants.Msmq.DEFAULT_MSMQ_EXCEPTION;
        private readonly static string _msmqActivity = Constants.Msmq.DEFAULT_MSMQ_ACTIVITY;
        private readonly static string _msmqDebug = Constants.Msmq.DEFAULT_MSMQ_DEBUG;
        private static readonly string _invoicesFolder = Constants.DEFAULT_INVOICES_FOLDER;
        private static readonly bool _allowNewAssociatedEntities = Constants.DEFAULT_ALLOW_NEW_ASSOCIATED_ENTITIES;
        private static readonly string _localNetworkIPRange = Constants.DEFAULT_LOCAL_NETWORK_IP_RANGE;
        private static readonly bool _useSSL = Constants.DEFAULT_USE_SSL;
        private static readonly bool _forceSSL = Constants.DEFAULT_FORCE_SSL;

        #endregion 

        #region Constructor

        static ConfigItems()
        {                        
            _connectString = ConfigurationManager.ConnectionStrings[Constants.CONFIG_KEY_DATABASE_CONNECTION] != null
            ? ConfigurationManager.ConnectionStrings[Constants.CONFIG_KEY_DATABASE_CONNECTION].ConnectionString 
            : Constants.DEFAULT_DATABASE_CONNECTION;

            _forceSSL = bool.Parse(GetAppSetting(Constants.CONFIG_KEY_FORCE_SSL, Constants.DEFAULT_FORCE_SSL.ToString()));            
            _invoicesFolder = GetAppSetting(Constants.CONFIG_KEY_INVOICES_FOLDER, Constants.DEFAULT_INVOICES_FOLDER);
            _newContractEmail = GetAppSetting(Constants.CONFIG_KEY_NEW_CONTRACT_EMAIL_ADDRESS, Constants.DEFAULT_NEW_CONTRACT_EMAIL_ADDRESS);
            _errorEmail = GetAppSetting(Constants.CONFIG_KEY_ERROR_EMAIL_ADDRESS, Constants.DEFAULT_ERROR_EMAIL_ADDRESS);
            _sendErrorEmails = bool.Parse(GetAppSetting(Constants.CONFIG_KEY_SEND_ERROR_EMAIL, Constants.DEFAULT_SEND_ERROR_EMAIL.ToString()));
            _useSSL = bool.Parse(GetAppSetting(Constants.CONFIG_KEY_USE_SLL, Constants.DEFAULT_USE_SSL.ToString()));
            _unitsOfTimeSize = int.Parse(GetAppSetting(Constants.CONFIG_KEY_UNITS_OF_TIME_SIZE, Constants.DEFAULT_UNITS_OF_TIME_SIZE.ToString()));
            _importFolder = Environment.ExpandEnvironmentVariables(GetAppSetting(Constants.CONFIG_KEY_IMPORT_FOLDER, Constants.DEFAULT_IMPORT_FOLDER));

            _localNetworkIPRange = GetAppSetting(Constants.CONFIG_KEY_LOCAL_NETWORK_IP_RANGE, Constants.DEFAULT_LOCAL_NETWORK_IP_RANGE);
            
            _msmqActivity = GetAppSetting(Constants.Msmq.CONFIG_KEY_MSMQ_ACTIVITY, Constants.Msmq.DEFAULT_MSMQ_ACTIVITY);
            _msmqEmail = GetAppSetting(Constants.Msmq.CONFIG_KEY_MSMQ_EMAIL, Constants.Msmq.DEFAULT_MSMQ_EMAIL);
            _msmqException = GetAppSetting(Constants.Msmq.CONFIG_KEY_MSMQ_EXCEPTION,
                                           Constants.Msmq.DEFAULT_MSMQ_EXCEPTION);
            _msmqDebug = GetAppSetting(Constants.Msmq.CONFIG_KEY_MSMQ_DEBUG, Constants.Msmq.DEFAULT_MSMQ_DEBUG);

            _allowNewAssociatedEntities = bool.Parse(GetAppSetting(Constants.CONFIG_KEY_ALLOW_NEW_ASSOCIATED_ENTITIES,
                                                        Constants.DEFAULT_ALLOW_NEW_ASSOCIATED_ENTITIES.ToString()));

            var folder = GetAppSetting(Constants.Reports.CONFIG_KEY_REPORTS_FOLDER, Constants.Reports.DEFAULT_REPORTS_FOLDER);

            // Special email case.
            // Try getting the mail settings via the <system.net> <smtp> section from the
            // web.config if we are in a web context, else grab it from the EXE config file.
            System.Configuration.Configuration config = null;                        
            if (System.Web.HttpContext.Current != null)
            {
                // Open the current web.config file.
                config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");                               

                _airtimeContract = System.Web.HttpContext.Current.Server.MapPath("~/" + folder + "/" + GetAppSetting(Constants.Reports.CONFIG_KEY_AIRTIME_CONTRACT, Constants.Reports.DEFAULT_AIRTIME_CONTRACT_RPT));
                _invoice = System.Web.HttpContext.Current.Server.MapPath("~/" + folder + "/" + GetAppSetting(Constants.Reports.CONFIG_KEY_INVOICE, Constants.Reports.DEFAULT_INVOICE_RPT));
            }
            else
            {
                // Open the current config file for the standalone app/service.
                config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

                _airtimeContract = folder + @"\" + GetAppSetting(Constants.Reports.CONFIG_KEY_AIRTIME_CONTRACT, Constants.Reports.DEFAULT_AIRTIME_CONTRACT_RPT);
                _invoice = folder + @"\" + GetAppSetting(Constants.Reports.CONFIG_KEY_INVOICE, Constants.Reports.DEFAULT_INVOICE_RPT);
            }
            if (config != null)
            {
                // get the mail settings section from <system.net>
                System.Net.Configuration.NetSectionGroup net = (System.Net.Configuration.NetSectionGroup)config.GetSectionGroup("system.net");
                if (net != null)
                    _systemEmail = net.MailSettings.Smtp.From;
            }
            
            if (string.IsNullOrEmpty(_systemEmail))
            {
                _systemEmail = GetAppSetting(Constants.CONFIG_KEY_SYSTEM_EMAIL_ADDRESS, Constants.DEFAULT_SYSTEM_EMAIL_ADDRESS);
            }
        }

        private static string GetAppSetting(string key, string defaultValue) 
        {            
            return ConfigurationManager.AppSettings[key] ?? defaultValue;
        }   
        
        #endregion 

        public static bool ForceSSL
        {
            get { return _forceSSL; }
        }

        public static string ConnectionString
        {
            get { return _connectString; }
        }
        
        public static string LocalNetworkIPRange
        {
            get { return _localNetworkIPRange;  }
        }

        public static bool UseSSL
        {
            get { return _useSSL;  }
        }

        #region Public Properties
        
        public static string SystemEmailAddress
        {
            get { return _systemEmail; }
        }

        public static string NewContractEmailAddress
        {
            get { return _newContractEmail; }
        }

        public static string ErrorEmailAddress
        {
            get { return _errorEmail; }
        }

        public static bool SendErrorEmails
        {
            get { return _sendErrorEmails; }
        }

        public static int UnitsOfTimeSize
        {
            get { return _unitsOfTimeSize; }
        }

        public static string ImportFolder
        {
            get { return _importFolder; }
        }

        public static string InvoicesFolder
        {
            get { return _invoicesFolder; }
        }

        public static string AirtimeContract
        {
            get { return _airtimeContract; }
        }

        public static string Invoice
        {
            get { return _invoice; }
        }

        public static bool AllowNewAssociatedEntities
        {
            get { return _allowNewAssociatedEntities; }
        }             
        
        public static string EmailMessageQueue
        {
            get { return _msmqEmail;  }
        }

        public static string ExceptionMessageQueue
        {
            get { return _msmqException;  }
        }

        public static string ActivityMessageQueue
        {
            get { return _msmqActivity;  }
        }

        public static string DebugMessageQueue
        {
            get { return _msmqDebug; }
        }

        #endregion
    }
}
