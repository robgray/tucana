﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using RGSS.LandwideCMS.Common.Configuration;


namespace RGSS.LandwideCMS.Common
{
    public static class Email
    {
        private static SmtpClient mailClient;

        static Email()
        {
            mailClient = Email.GetSmtpClientInstance();
        }

        public static SmtpClient GetSmtpClientInstance()
        {
            var client = new SmtpClient
                        {
                            Host = ConfigItems.SmtpHost,
                            UseDefaultCredentials = true
                        };
            
            return client;
        }

        public static void SendMail(string to, string subject, string message, bool highPriority)
        {
            //using (MailMessage email = new MailMessage())
            using (var email = new AsyncEmail())
            {
                email.To.Add(new MailAddress(to));
                email.From = new MailAddress(ConfigItems.SystemEmailAddress, "Landwide");
                email.Subject = subject;
                email.IsBodyHtml = true;
                email.Body = message;
                email.Priority = highPriority ? MailPriority.High : MailPriority.Normal;

                email.SendEmailAsync();
                //mailClient.Send(email);
                //mailClient.SendAsync(email, null);
            }
        }

        public static void SendMail(string to, string subject, string message)
        {
            SendMail(to, subject, message, false);
        }
    }
}
