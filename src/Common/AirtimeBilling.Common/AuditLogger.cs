﻿using System;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Linq;
using System.Reflection;

namespace AirtimeBilling.Common
{
    public static class AuditLoggingUtility
    {
        static readonly SqlConnection cn = new SqlConnection(ConfigItems.ConnectionString);
        static readonly SqlCommand cmd;

        static AuditLoggingUtility()
        {
            cmd = cn.CreateCommand();
            cmd.CommandText = "INSERT INTO Audit (AuditDate, [User], TableName, FieldName, KeyId, OldValue, NewValue) VALUES (@AuditDate, @User, @TableName, @FieldName, @KeyId, @OldValue, @NewValue)";            
            cmd.Parameters.Add(new SqlParameter("@AuditDate", SqlDbType.DateTime));
            cmd.Parameters.Add(new SqlParameter("@User", SqlDbType.NVarChar, 30));
            cmd.Parameters.Add(new SqlParameter("@TableName", SqlDbType.NVarChar, 60));
            cmd.Parameters.Add(new SqlParameter("@FieldName", SqlDbType.NVarChar, 60));
            cmd.Parameters.Add(new SqlParameter("@KeyId", SqlDbType.Int));
            cmd.Parameters.Add(new SqlParameter("@OldValue", SqlDbType.NVarChar, 80));
            cmd.Parameters.Add(new SqlParameter("@NewValue", SqlDbType.NVarChar, 80));
            cmd.CommandType = CommandType.Text;
        }

        public static bool WriteAuditLog(string user, string tableName, string fieldName, int keyId, string oldValue, string newValue)
        {
            cmd.Parameters["@AuditDate"].Value = DateTime.Now;
            cmd.Parameters["@User"].Value = user;
            cmd.Parameters["@TableName"].Value = tableName;
            cmd.Parameters["@FieldName"].Value = fieldName;
            cmd.Parameters["@KeyId"].Value = keyId;
            cmd.Parameters["@OldValue"].Value = oldValue;
            cmd.Parameters["@NewValue"].Value = newValue;

            var rows = 0;
            try
            {
                cn.Open();
                rows = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(ex.Message);
            }
            finally
            {
                cn.Close();
            }
            return rows == 1;
        }
    }

    public static class AuditExtensions 
    {
        public static bool HasAttribute(this Type t, Type attrType)
        {
            return t.GetCustomAttributes(attrType, true) != null;
        }

        public static bool HasAttribute(this PropertyInfo pi, Type attrType)
        {
            return pi.GetCustomAttributes(attrType, false) != null;
        }

        private static string GetPropertyValue(PropertyInfo pi, object input)
        {
            var tmp = pi.GetValue(input, null);
            return (tmp == null) ? string.Empty : tmp.ToString();
        }

        private static string GetPropertyValue(object input)
        {
            return (input == null) ? "<null>" : input.ToString();
        }

        public static void Audit<TEntity>(this DataContext dc, Func<TEntity, int> tableKeySelector) where TEntity : class
        {            
            var table = dc.GetTable<TEntity>();
            string tableName = dc.Mapping.GetTable(typeof(TEntity)).TableName;
            
            // Remove the 'dbo.' prefix
            if (tableName.IndexOf("dbo.") == 0)
            {
                tableName = tableName.Substring(4);
            }

            // Audit the items that have been modified.
            var updates = dc.GetChangeSet().Updates.OfType<TEntity>();
            foreach (var item in updates) 
            {                
                var key = tableKeySelector.Invoke(item);
                var mmi = table.GetModifiedMembers(item);

                foreach (var mi in mmi)
                {
                    var user = System.Threading.Thread.CurrentPrincipal.Identity.Name;
                    AuditLoggingUtility.WriteAuditLog(user, tableName, mi.Member.Name, key, GetPropertyValue(mi.OriginalValue), GetPropertyValue(mi.CurrentValue));
                }
            }
        }
    }    
}
