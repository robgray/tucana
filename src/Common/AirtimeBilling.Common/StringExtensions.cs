﻿using System;
using System.Collections.Specialized;
using System.Globalization;

namespace AirtimeBilling.Common
{
    /// <summary>
    /// Code from Phil Haack to split Pascal and Camel-cased strings.
    /// Modified by me into Extension methods
    /// http://haacked.com/archive/2005/09/24/10334.aspx
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Parses a camel cased or pascal cased string and returns a new 
        /// string with spaces between the words in the string.
        /// </summary>
        /// <example>
        /// The string "PascalCasing" will return an array with two 
        /// elements, "Pascal" and "Casing".
        /// </example>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string SplitUpperCaseToString(this string source)
        {
            return string.Join(" ", SplitUpperCase(source));
        }


        /// <summary>
        /// Parses a camel cased or pascal cased string and returns an array 
        /// of the words within the string.
        /// </summary>
        /// <example>
        /// The string "PascalCasing" will return an array with two 
        /// elements, "Pascal" and "Casing".
        /// </example>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string[] SplitUpperCase(this string source)
        {
            if (source == null)
                return new string[] { }; //Return empty array.

            if (source.Length == 0)
                return new string[] { "" };

            StringCollection words = new StringCollection();
            int wordStartIndex = 0;

            char[] letters = source.ToCharArray();

            // Skip the first letter. we don't care what case it is.
            for (int i = 1; i < letters.Length; i++)
            {
                if (char.IsUpper(letters[i]))
                {
                    //Grab everything before the current index.
                    words.Add(new String(letters, wordStartIndex, i - wordStartIndex));
                    wordStartIndex = i;
                }
            }

            //We need to have the last word.
            words.Add(new String(letters, wordStartIndex, letters.Length - wordStartIndex));

            //Copy to a string array.
            string[] wordArray = new string[words.Count];
            words.CopyTo(wordArray, 0);
            return wordArray;
        }

        /// <summary>
        /// Set the value of the string to be value when the string is empty string.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string WhenEmpty(this string s, string value)
        {
            return s.Equals(string.Empty) ? value : s;                
        }

        public static decimal ToDecimalCurrency(this string s)
        {
            return Decimal.Parse(s,
                          NumberStyles.AllowCurrencySymbol | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign | 
                          NumberStyles.AllowDecimalPoint);
        }
    }
}
