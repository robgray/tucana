﻿using System;
using System.Collections.Generic;

namespace AirtimeBilling.Common
{
    /// <summary>
    /// This class provides helper functions to enumerations.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class Enum<T>
    {        
        public static T Parse(string value)
        {
            return (T) Enum.Parse(typeof (T), value);
        }

        public static T GetEnumFromDescription(string description)
        {
            var names = Enum.GetNames(typeof (T));
            foreach(var value in names)
            {
                if (value == description) return Parse(value);

                var desc = value.ToString();
                var fieldInfo = typeof(T).GetField(desc);
                var attributes =
                    (EnumDescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(EnumDescriptionAttribute), false);
                if (attributes != null && attributes.Length > 0)
                {
                    if (attributes[0].Description == description)
                    {
                        return Parse(value);
                    }
                }
            }
            throw new ArgumentOutOfRangeException("Cannot translate '" + description + "' into a value from '" +
                                                  typeof (T).Name);
        }

        public static string GetDescription(Enum value)
        {
            var description = value.ToString();
            var fieldInfo = value.GetType().GetField(description);
            var attributes =
                (EnumDescriptionAttribute[]) fieldInfo.GetCustomAttributes(typeof (EnumDescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                description = attributes[0].Description;
            }
            return description;
        }

        public static IList<string> GetAllDescriptions()
        {
            IList<string> descs = new List<string>();
            var enumValues = Enum.GetValues(typeof (T));

            foreach (Enum value in enumValues)
            {
                descs.Add(GetDescription(value));
            }

            return descs;
        }
    }

    public static class EnumExtension
    {
        public static string GetDescription(this Enum e)
        {
            var description = e.ToString();
            var fieldInfo = e.GetType().GetField(description);
            var attributes =
                (EnumDescriptionAttribute[])fieldInfo.GetCustomAttributes(typeof(EnumDescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                description = attributes[0].Description;
            }
            return description;
        }
    }
}
