﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using RGSS.LandwideCMS.Common.Configuration;

namespace RGSS.LandwideCMS.Common.Helpers
{
    public static class ReportFactory
    {        
        public static ReportDocument GetAirtimeContract(int contractId)
        {
            var report = GetConfiguredReport(ConfigItems.AirtimeContract);
            if (report != null)
            {
                var paramContractId = report.ParameterFields["ContractId"];
                paramContractId.CurrentValues.AddValue(contractId.ToString());
            }

            return report;
        }

        public static ReportDocument GetInvoice(IList<string> invoiceNumbers)
        {
            return GetInvoice(invoiceNumbers, false);
        }

        public static ReportDocument GetInvoice(IList<string> invoiceNumbers, bool isCopy)
        {
            var report = GetConfiguredReport(ConfigItems.Invoice);
            if (report != null)
            {
                var paramInvoiceNumber = report.ParameterFields["InvoiceNumber"];
                foreach (var invoiceNumber in invoiceNumbers)
                {
                    paramInvoiceNumber.CurrentValues.AddValue(invoiceNumber);
                }

                var paramIsCopy = report.ParameterFields["IsCopy"];
                paramIsCopy.CurrentValues.AddValue(isCopy);
            }

            return report;
        }

        /// <summary>
        /// Helper to set the Connection info for all reports.
        /// </summary>
        /// <param name="reportFilename">full path and filename to the report file</param>
        /// <returns>A ReportDocument configured to access the database for the project</returns>
        public static ReportDocument GetConfiguredReport(string reportFilename)
        {
            var rep = new ReportDocument();
            rep.Load(reportFilename);
            if (rep.IsLoaded)
            {
                ConnectionInfo connInfo = ConnectionInfoFactory.GetFromSqlServerConnectionString(ConfigItems.ConnectionString);
                Tables tables = rep.Database.Tables;
                foreach (Table table in tables)
                {
                    TableLogOnInfo logonInfo = table.LogOnInfo;
                    logonInfo.ConnectionInfo = connInfo;
                    table.ApplyLogOnInfo(logonInfo);
                }

                return rep;
            }
            return null;
        }
    }
}
