﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CrystalDecisions.Shared;

namespace RGSS.LandwideCMS.Common.Helpers
{
    public static class ConnectionInfoFactory
    {
        /// <summary>
        /// Creates a ConnectionInfo instance from a Sql Server Connection String
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static ConnectionInfo GetFromSqlServerConnectionString(string connectionString)
        {
            if (string.IsNullOrEmpty(connectionString)) return null;

            bool minimalValidity = false;
            string[] tokens = connectionString.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            ConnectionInfo info = new ConnectionInfo();
            foreach (string token in tokens)
            {
                string[] fields = token.Split(new string[] { "=" }, StringSplitOptions.RemoveEmptyEntries);
                if (fields[0].Equals("Data Source", StringComparison.CurrentCultureIgnoreCase))
                {
                    info.ServerName = fields[1];
                }
                else if (fields[0].Equals("Initial Catalog", StringComparison.CurrentCultureIgnoreCase))
                {
                    info.DatabaseName = fields[1];
                }
                else if (fields[0].Equals("Integrated Security", StringComparison.CurrentCultureIgnoreCase))
                {
                    info.IntegratedSecurity = bool.Parse(fields[1]);                    
                }
                else if (fields[0].Equals("User Id", StringComparison.CurrentCultureIgnoreCase))
                {
                    info.UserID = fields[1];
                }
                else if (fields[0].Equals("Password", StringComparison.CurrentCultureIgnoreCase))
                {
                    info.Password = fields[1];
                }
            }

            // For the connection info to be valid, the minimum of a Server and Database as well as either
            // Integrated security or user authentication details must be supplied.
            // If not valid, return null
            minimalValidity = ((info.ServerName.Length > 0 && info.DatabaseName.Length > 0) && 
                (info.IntegratedSecurity || (info.UserID.Length > 0 && info.Password.Length > 0)));

            if (minimalValidity)
            {
                // Do not allow user id or password if Integrated Security is on 
                // (should not be a valid connection string anyway)
                if (info.IntegratedSecurity)
                {
                    info.UserID = string.Empty;
                    info.Password = string.Empty;
                }
            }
            else
            {
                return null;
            }
                        
            return info;
        }

        public static ConnectionInfo GetFromSqlServerConnection(System.Data.SqlClient.SqlConnection sqlConnection)
        {
            return ConnectionInfoFactory.GetFromSqlServerConnectionString(sqlConnection.ConnectionString);
        }
    }
}
