﻿using System;

namespace AirtimeBilling.Common
{
    public interface IDateTimeFacade
    {
        DateTime Now { get; }

        DateTime Today { get; }
    }
}
