﻿namespace AirtimeBilling.Common.Logging
{
    /// <summary>
    /// Provides output format for emails
    /// </summary>
    public interface IEmailFormat
    {
        string GetHtml();
        string GetPlainText();
    }
}
