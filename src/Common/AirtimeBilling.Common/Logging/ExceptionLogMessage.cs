﻿using System;
using System.Text;
using System.Runtime.Serialization;

namespace AirtimeBilling.Common.Logging
{
    [Serializable]
    [DataContract]
    public class ExceptionLogMessage : LogMessage 
    {
        public ExceptionLogMessage() { }

        public ExceptionLogMessage(Exception ex) 
        {
            LoggingLevel = LoggingLevels.Exception;
            MethodName = ex.TargetSite == null ? "Unknown" : ex.TargetSite.Name;
            Source = ex.Source;
            Message = ex.Message;
            StackTrace = ex.StackTrace;
        }        
        
        [DataMember]        
        public string StackTrace { get; set; }
        
        public override string ToString()
        {
            var  sb = new StringBuilder();

            sb.Append(base.ToString());
            sb.Append(Environment.NewLine);            
            sb.AppendFormat("Stack Trace: {0}", StackTrace);

            return sb.ToString();
        }
    }
}
