﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Net.Mail;

namespace AirtimeBilling.Common.Logging
{
    /// <summary>
    /// Represents a generic, text only message.
    /// Derive from this class to get specific messages.
    /// </summary>
    [Serializable]
    [DataContract]
    public class EmailMessage
    {
        public EmailMessage()
        {
            FilesToAttach = new List<string>();
        }

        [DataMember(IsRequired = true)]
        public string ToEmailAddress { get; set; }

        [DataMember(IsRequired = true)]
        public string ToName { get; set; }

        [DataMember(IsRequired = true)]
        public string Subject { get; set; }

        [DataMember(IsRequired = true)]
        public bool IsHighPriority { get; set; }

        [DataMember(IsRequired = true)]
        public bool IsBodyHtml { get; set; }

        [DataMember(IsRequired = true)]
        public string Body { get; set;  }

        [DataMember(IsRequired=true)]
        public IList<string> FilesToAttach { get; set; }

        /// <summary>
        /// Provides a MailMessage email object ready for sending.
        /// Default system parameters are set here, such as the from
        /// address and the reply to address.
        /// </summary>
        /// <returns></returns>
        public virtual MailMessage GetMessage()
        {
            var email = new MailMessage();
            if (string.IsNullOrEmpty(ToName))
            {
                email.To.Add(new MailAddress(ToEmailAddress));
            }
            else
            {
                email.To.Add(new MailAddress(ToEmailAddress, ToName));
            }
            email.From = new MailAddress(ConfigItems.SystemEmailAddress, "Airtime Billing");
            email.Subject = Subject;
            email.ReplyToList.Add(email.From);
            email.IsBodyHtml = IsBodyHtml;
            email.Priority = IsHighPriority ? MailPriority.High : MailPriority.Normal;
            email.Body = Body;

            if (FilesToAttach != null)
            {
                foreach (var attach in FilesToAttach)
                {
                    var attachment = new Attachment(attach);
                    email.Attachments.Add(attachment);
                }
            }

            return email;                            
        }
        
    }
}
