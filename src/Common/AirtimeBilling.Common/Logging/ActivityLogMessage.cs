﻿using System;
using System.Runtime.Serialization;

namespace AirtimeBilling.Common.Logging
{
    [Serializable]
    [DataContract]
    public class ActivityLogMessage : BaseLogMessage
    {        
        /// <summary>
        /// The name of the table containing the entity the activity is describing
        /// </summary>
        [DataMember]
        public string TableName { get; set; }

        /// <summary>
        /// Primary Key for the main entity the activity is describing.
        /// Primary Key is in TableName
        /// </summary>
        [DataMember]
        public int TableKeyId { get; set; }

        /// <summary>
        /// The detailed description of the actitivity
        /// </summary>
        [DataMember]
        public string Activity { get; set; }

    }
}
