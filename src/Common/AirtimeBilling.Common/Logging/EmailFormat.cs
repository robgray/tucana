﻿using System.Text;

namespace AirtimeBilling.Common.Logging
{
    /// <summary>
    /// Provides Email message representation, 
    /// Giving the ability to represent an email in Html and Plain Text
    /// and providing the majority of the Html rending.    
    /// </summary>
    public abstract class EmailFormat : IEmailFormat 
    {
        #region IEmailFormat Members

        public string GetHtml()
        {
            var sb = new StringBuilder();
            sb.AppendLine(GetHead());
            sb.AppendLine("<body>");
            sb.AppendLine("<table style='width: 800px'>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td>");
            sb.AppendLine("<img src='http://airtimebilling.robertgray.net.au/images/airtime_logo.jpg' />");
            sb.AppendLine("</td>");
            sb.AppendLine("<td style='text-align: right;'>");
            sb.AppendLine("<b>Super Satellite Solutions Pty Ltd</b><br />");
            sb.AppendLine("1 Demo Street Testplace QLD 0000<br />");            
            sb.AppendLine("Ph: 07 0000 0000<br />");
            sb.AppendLine("Fax: 07 0000 0000<br />");
            sb.AppendLine("Email: <a href='mailto:airtime@test.com.au'>office@airtimebilling.com.au</a><br />");
            sb.AppendLine("Web: <a href='http://www.airtimebilling.com.au'>www.airtimebilling.com.au</a>");
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");
            sb.AppendLine("<tr>");
            sb.AppendLine("<td colspan='2' id='content'>");
            sb.AppendLine(GetBody());
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");            
            sb.AppendLine(GetSignature());
            sb.AppendLine("<tr>");
            sb.AppendLine("<td colspan='2' class='footer'>");
            sb.AppendLine("This is an Airtime Billing System generated email.");
            sb.AppendLine("</td>");
            sb.AppendLine("</table>");
            sb.AppendLine("</body>");
            sb.AppendLine("</html>");

            return sb.ToString();
        }

        public string GetPlainText()
        {
            var sb = new StringBuilder();
            sb.AppendLine("Super Satellite Solutions Pty Ltd");
            sb.AppendLine("1 Demo Street Testplace QLD 0000");            
            sb.AppendLine("Ph: 07 0000 0000");
            sb.AppendLine("Fax: 07 0000 0000");
            sb.AppendLine("Email: office@airtimebilling.com.au");
            sb.AppendLine("Web: http://www.airtimebilling.com.au");
            sb.AppendLine(); 
            sb.AppendLine();
            sb.AppendLine(GetNoHtml());
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine("This is an Airtime Billng System generated email.");

            return sb.ToString();
        }

        protected abstract string GetNoHtml();
        
        protected abstract string GetBody();

        private string GetHead()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.AppendLine("<head>");
            sb.AppendLine("<style type='text/css'>");
            sb.AppendLine("* { margin: 0; padding: 0; }");
            sb.AppendLine("body { padding-left: 20px; font-family: Verdana; font-size: 10pt; color: #666666; }");                        
            sb.AppendLine("#content { padding: 20px 5px 5px 5px; }");
            sb.AppendLine("#signature { padding-top: 30px; }");
            sb.AppendLine(".footer { font-size: 7pt; padding-top: 20px; text-align: left; }");
            sb.AppendLine("a { color: #FF9900; text-decoration: none; } ");
            sb.AppendLine("a:hover { text-decoration: underline; ");
            sb.AppendLine("td { vertical-align: top; ");
            sb.AppendLine(GetCustomStyles());
            sb.AppendLine("</style>");
            sb.AppendLine("</head>");

            return sb.ToString();
        }

        private string GetSignature()
        {
            var sb = new StringBuilder();
            sb.AppendLine("<tr>");
            sb.AppendLine("<td colspan='2' id='signature'>");
            sb.AppendLine("Regards,<br/>");
            sb.AppendLine("Super Satellite Solutions.");
            sb.AppendLine("</td>");
            sb.AppendLine("</tr>");
            return sb.ToString();
        }

        #endregion

        /// <summary>
        /// Override this method in subclasses to provide custom CSS for that particular email.
        /// Return the CSS styles as string.
        /// </summary>        
        protected virtual string GetCustomStyles()
        {
            return "";
        }
    }
}
