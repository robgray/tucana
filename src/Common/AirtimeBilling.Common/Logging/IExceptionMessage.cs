﻿using System.ServiceModel;

namespace AirtimeBilling.Common.Logging
{
    [ServiceContract]
    public interface IExceptionMessage
    {        
        /// <summary>
        /// Log an exception to the LoggingUtility
        /// </summary>
        /// <param name="entry"></param>
        [OperationContract(IsOneWay = true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void DispatchExceptionLogMessage(ExceptionLogMessage entry);
    }
}
