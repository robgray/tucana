﻿using System;
using System.Text;
using System.Runtime.Serialization;

namespace AirtimeBilling.Common.Logging
{
    [Serializable]
    [DataContract]
    public class LogMessage : BaseLogMessage 
    {
        public LogMessage()
        {
            LoggingLevel = LoggingLevels.None;
        }
        
        [DataMember]
        public LoggingLevels LoggingLevel { get; set; }

        [DataMember]
        public string MethodName { get; set; }

        [DataMember]
        public string Source { get; set; }

        [DataMember]
        public string Message { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendFormat("User: {0}", Username);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("Level: {0}", LoggingLevel);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("Method Name: {0}", MethodName);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("Source: {0}", Source);
            sb.Append(Environment.NewLine);
            sb.AppendFormat("Message: {0}", Message);            

            return sb.ToString();
        }
    }
}
