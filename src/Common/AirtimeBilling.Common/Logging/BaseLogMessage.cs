﻿using System;
using System.Runtime.Serialization;

namespace AirtimeBilling.Common.Logging
{
    [Serializable]
    [DataContract]
    public class BaseLogMessage
    {
        public BaseLogMessage()
        {
            Username = System.Threading.Thread.CurrentPrincipal != null ? System.Threading.Thread.CurrentPrincipal.Identity.Name : "{ServiceUser}";
            Timestamp = DateTime.Now;
        }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public DateTime Timestamp { get; set;  }
    }
}
