﻿namespace AirtimeBilling.Common.Logging
{
    public enum LoggingLevels
    {
        None = 0,
        DebugInfo, 
        Warning,
        Exception,
    }
}
