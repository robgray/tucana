﻿using System.ServiceModel;

namespace AirtimeBilling.Common.Logging
{
    [ServiceContract]
    public interface IEmailMessage
    {
        /// <summary>
        /// Send an email to the queue for eventual sending
        /// </summary>        
        [OperationContract(IsOneWay=true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void DispatchEmailMessage(EmailMessage email);
    }
}
