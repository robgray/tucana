﻿using System;
using System.Runtime.Serialization;

namespace AirtimeBilling.Common.Logging
{
    [Serializable]
    [DataContract]
    public class DebugLogMessage : LogMessage 
    {
        public DebugLogMessage()
        {
            LoggingLevel = LoggingLevels.DebugInfo;
        }        
    }
}
