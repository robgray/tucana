﻿using System.ServiceModel;

namespace AirtimeBilling.Common.Logging
{
    [ServiceContract]
    public interface IDebugMessage
    {
        [OperationContract(IsOneWay=true)]
        [TransactionFlow(TransactionFlowOption.Allowed)]
        void DispatchDebugMessage(DebugLogMessage msg);
    }
}
