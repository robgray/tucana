﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGSS.LandwideCMS.Common.Configuration;
using System.Diagnostics;

namespace RGSS.LandwideCMS.Common
{
    public static class Logger
    {
        public static void LogException(Exception ex)
        {            
            var errorMessage = "Method Name: " + ex.TargetSite.Name + "\n" +
                                    "Source: " + ex.Source + "\n" +
                                    "Message: " + ex.Message + "\n" +
                                    "Stack Trace: " + ex.StackTrace;

            // Log - file 
            Trace.WriteLine(errorMessage);

            if (ConfigItems.SendErrorEmails)
            {
                // Log - send email?
                Email.SendMail(ConfigItems.ErrorEmailAddress, "LandwideCMS Error", errorMessage);
            }
        }
    }
}
