﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AirtimeBilling.Common;

namespace AirtimeBilling.LoggingServices
{
    [ServiceBehavior]
    public abstract class BaseDataService
    {
        protected readonly ServicesDataContext db;

        protected BaseDataService()
        {
            db = new ServicesDataContext(ConfigItems.ConnectionString);
        }
    }
}
