﻿using System;
using System.ServiceModel;
using System.Net.Mail;
using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Logging;

namespace AirtimeBilling.LoggingServices
{
    [ServiceBehavior]
    public class EmailService : IEmailMessage
    {                   
        #region IEmailMessage Members

        [OperationBehavior(TransactionScopeRequired = true)]
        public void DispatchEmailMessage(EmailMessage email)
        {
            try
            {
                if (string.IsNullOrEmpty(email.ToEmailAddress))
                {
                    email.ToEmailAddress = ConfigItems.SystemEmailAddress;
                    email.ToName = "AirtimeBilling No Address";
                }

                using (var outgoing = email.GetMessage()) {
                    var mailClient = new SmtpClient();

                    mailClient.Send(outgoing);

                    var emailLogMsg = string.Format("Sent email SMTP Server: {0}, Mail From: {1}, Mail To: {2}", mailClient.Host,
                                                outgoing.From.Address, outgoing.To[0].Address);
                    LoggingUtility.LogDebug("DispatchEmailMessage", "EmailService", emailLogMsg);
                }                               
            }
            catch (Exception ex)
            {
                // Do not send Error email.
                LoggingUtility.LogException(ex, false);
            }
        }

        #endregion
    }
}
