﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;
using System.ServiceModel;
using AirtimeBilling.Logging;

namespace AirtimeBilling.LoggingServices
{
    [ServiceBehavior]
    public class ActivityLoggingService : BaseDataService, IActivityMessage
    {        
        #region IActivityMessage Members

        [OperationBehavior(TransactionScopeRequired = true)]
        //[OperationBehavior(TransactionAutoComplete = true)]
        public void DispatchActivityLogMessage(ActivityLogMessage entry)
        {
            try
            {
                var log = new ActivityLog
                              {
                                  Activity = entry.Activity,
                                  KeyId = entry.TableKeyId,
                                  LogDate = entry.Timestamp,
                                  TableName = entry.TableName,
                                  User = entry.Username
                              };

                db.ActivityLogs.InsertOnSubmit(log);
                db.SubmitChanges();
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(Constants.SYSTEM_USER_NAME, ex);
            }
        }

        #endregion
    }
}
