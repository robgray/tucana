﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using AirtimeBilling.Common.Logging;

namespace AirtimeBilling.LoggingServices
{
    [ServiceBehavior]
    public class ErrorService : BaseDataService, IExceptionMessage 
    {

        #region IExceptionMessage Members

        [OperationBehavior(TransactionScopeRequired = true)]
        public void DispatchExceptionLogMessage(ExceptionLogMessage entry)
        {
            try
            {
                var error = new Log
                                {
                                    Username = entry.Username,
                                    Timestamp = entry.Timestamp,
                                    LoggingLevel = (int) LoggingLevels.Exception,
                                    Message = entry.Message ?? "unknown",
                                    MethodName = entry.MethodName ?? "unknown",
                                    Source = entry.Source ?? "unknown",
                                    StackTrace = (entry.StackTrace ?? "unknown").Replace(@"\r\n", " -- ")
                                };

                db.Logs.InsertOnSubmit(error);
                db.SubmitChanges();                
            }
            catch (Exception ex)
            {
                // GOT NOTHING TO DO HERE
                Console.WriteLine(ex.Message);
            }
        }

        #endregion
    }
}
