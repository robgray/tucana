﻿using System;
using System.ServiceModel;
using AirtimeBilling.Common;
using AirtimeBilling.Common.Logging;
using AirtimeBilling.Logging;

namespace AirtimeBilling.LoggingServices
{    
    [ServiceBehavior]
    public class DebugService : BaseDataService, IDebugMessage
    {        
        #region IDebugMessage Members
           
        [OperationBehavior(TransactionScopeRequired = true)]
        public void DispatchDebugMessage(DebugLogMessage msg)
        {
            try
            {
                var info = new Log
                               {
                                   Username = msg.Username,
                                   Timestamp = msg.Timestamp,
                                   LoggingLevel = (int) msg.LoggingLevel,
                                   Message = msg.Message,
                                   MethodName = msg.MethodName,
                                   Source = msg.Source
                               };

                db.Logs.InsertOnSubmit(info);
                db.SubmitChanges();                
            }
            catch (Exception ex)
            {
                LoggingUtility.LogException(Constants.SYSTEM_USER_NAME, ex);
            }
        }

        #endregion
    }
}
