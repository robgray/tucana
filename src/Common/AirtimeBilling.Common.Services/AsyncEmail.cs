﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Runtime.Remoting.Messaging;

namespace RGSS.LandwideCMS.Common
{
    public class AsyncEmail : MailMessage
    {
        public AsyncEmail() : base() { }

        public AsyncEmail(MailAddress from, MailAddress to) : base(from, to) { }

        public AsyncEmail(string from, string to) : base(from, to) { }

        public AsyncEmail(string from, string to, string subject, string body) : base(from, to, subject, body) { }

        private void SendEmail()
        {
            if (string.IsNullOrEmpty(this.Body)) return;

            try
            {
                var client = Email.GetSmtpClientInstance();
                using (var msg = new MailMessage(this.From, this.To[0]))
                {
                    msg.Subject = this.Subject;
                    msg.Body = this.Body;
                    msg.IsBodyHtml = this.IsBodyHtml;
                    client.Send(msg);
                }
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
        }

        public delegate void SendEmailDelegate();

        public void GetResultsOnCallback(IAsyncResult ar)
        {
            var del = (SendEmailDelegate)((AsyncResult)ar).AsyncDelegate;
            try
            {
                del.EndInvoke(ar);
            }
            catch (Exception ex)
            {
                Logger.LogException(ex);
            }
        }

        public void SendEmailAsync()
        {
            var del = new SendEmailDelegate(this.SendEmail);
            var callBack = new AsyncCallback(this.GetResultsOnCallback);
            var ar = del.BeginInvoke(callBack, null);
        }
    }
}
