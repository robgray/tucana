﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RGSS.LandwideCMS.Common.Logging;

namespace RGSS.Landwide.Common.Services
{
    public class ActivityLoggingService : IActivityLoggingService
    {
        SqlConnection cn = new SqlConnection(ConfigItems.ConnectionString);
        SqlCommand cmd;

        public ActivityLoggingService()
        {
            
        }

        #region IActivityLoggingService Members

        public void WriteUserActivity(string tableName, int keyId, string user, string activity)
        {
            throw new NotImplementedException();
        }

        public void WriteActivity(string tableName, int keyId, string activity)
        {
            throw new NotImplementedException();
        }

        public void WriteUserActivity(DateTime activityDate, string tableName, int keyId, string user, string activity)
        {
            throw new NotImplementedException();
        }

        public void WriteActivity(DateTime activityDate, string tableName, int keyId, string activity)
        {
            throw new NotImplementedException();
        }

        public void WriteSystemActivity(string tableName, int keyId, string activity)
        {
            throw new NotImplementedException();
        }

        public void WriteSystemActivity(DateTime activityDate, string tableName, int keyId, string activity)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
