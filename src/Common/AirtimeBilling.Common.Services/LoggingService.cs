﻿using RGSS.LandwideCMS.Common.Logging;
using RGSS.LandwideCMS.Common.Configuration;

namespace RGSS.Landwide.Common.Services
{
    // NOTE: If you change the class name "Service1" here, you must also update the reference to "Service1" in App.config.
    public class LoggingService : IActivityMessage
    {
        private ServicesDataContext db;

        public LoggingService()
        {
            db = new ServicesDataContext(ConfigItems.ConnectionString);
        }


        #region ILoggingService Members

        public void LogException(ExceptionLogMessage entry)
        {
            
            
            if (ConfigItems.SendErrorEmails)
            {
               
            }
        }

        #endregion
    }
}
